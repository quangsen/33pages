<?php
public function run()
    {
        if (!array_key_exists($this->procedure, $this->validProcedures)) {
            throw new ApiException("Procedure name is not valid", 0);
        } else {
            $query = 'CALL ' . $this->procedure . '(';
            if (isset($this->validProcedures[$this->procedure]['rules'])) {
                $rules     = $this->validProcedures[$this->procedure]['rules'];
                $validator = Validator::make($this->params, $rules);
                if ($validator->fails()) {
                    throw new ApiException(json_encode($validator->messages()), 0);
                }
                $index = 0;
                foreach ($rules as $attr => $expression) {
                    if ($index == count($rules) - 1) {
                        $query .= ":{$attr}";
                    } else {
                        $query .= ":{$attr}, ";
                    }
                    $index++;
                }
            }
            $query .= ')';
            $host   = getenv('DB_HOST');
            $user   = getenv('DB_USERNAME');
            $pass   = getenv('DB_PASSWORD');
            $dbname = getenv('DB_DATABASE');
            try {
                $pdo = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                if ($this->procedure == 'ProcAddNewWO') {
                    $query = "CALL ProcAddNewWO('{$this->params['UserID']}', '{$this->params['LineItemID']}', '{$this->params['PriceEach']}', '{$this->params['DeliveryDate']}', '{$this->params['Reqd']}', '{$this->params['Build']}', '{$this->params['ChangeReason']}', '{$this->params['FAIR']}', @NewWONo)";
                    $stmt  = $pdo->prepare($query);
                } else {
                    $stmt = $pdo->prepare($query);
                    foreach ($this->params as $key => $value) {
                        $stmt->bindValue(':' . $key, $value, PDO::PARAM_INT);
                    }
                }
                $stmt->execute();
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            } catch (\Exception $e) {
                throw new ApiException($e->getMessage(), 1);
            }
        }
    }