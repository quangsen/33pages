(function(app) {

    app.controller('EngineeringEngineeringCtrl', EngineeringEngineeringCtrl);
    EngineeringEngineeringCtrl.$inject = ['$rootScope', '$scope', 'API', 'Notification', '$state', '$stateParams', '$window'];

    function EngineeringEngineeringCtrl($rootScope, $scope, API, Notification, $state, $stateParams, $window) {
        $scope.DeptID = $stateParams.DeptID;
        var EngineeringTemplateID = $stateParams.EngineeringTemplateID;
        var WorkOrderID = $stateParams.WorkOrderID;
        var OperationID = $stateParams.OperationID;
        var OperationTemplateID = $stateParams.OperationTemplateID;
        var hasOperationID = false;

        if (OperationID !== undefined && OperationID !== '' && OperationID !== null) {
            hasOperationID = true;
        }
        if (OperationTemplateID !== undefined && OperationTemplateID !== '' && OperationTemplateID !== null) {
            hasOperationID = true;
        }

        if (!hasOperationID) {
            $rootScope.OpTmpltIntEngNotes = undefined;
        }

        $scope.AddTemplateProcess = function(Note) {
            $rootScope.$broadcast('updateThead');
            var data;
            if (Note !== undefined && Note !== "") {
                data = {
                    ProcessNote: Note
                };
            }
            if (OperationID !== undefined && OperationID !== "") {
                $rootScope.addRootTemplateProcess(data, OperationID);
            }
            if (OperationTemplateID !== undefined && OperationTemplateID !== "") {
                $rootScope.addRootTemplateProcess(data, OperationTemplateID);
            }
            $scope.ProcessNote = undefined;
        };

        $scope.AddOpTmpltIntEngNotes = function(Note) {
            $rootScope.$broadcast('updateThead');
            if ($rootScope.OpTmpltIntEngNotes === undefined) {
                $rootScope.OpTmpltIntEngNotes = [];
            }
            var data;
            if (Note !== undefined && Note !== "") {
                data = {
                    Note: Note
                };
            }
            $rootScope.OpTmpltIntEngNotes.unshift(data);
            $scope.EngNote = undefined;
            if (OperationID !== undefined && OperationID !== "") {
                $rootScope.addRootEngNote(data, OperationID);
            }
            if (OperationTemplateID !== undefined && OperationTemplateID !== "") {
                $rootScope.addRootEngNote(data, OperationTemplateID);
            }
        };

        /**
         * the function working when click Engineering Review button 
         * in P/E Engineering Tab
         */
        $scope.EngineeringToReview = function() {
            var checkExist = $scope.checkExistEngWorkOrder();
            if (checkExist) {
                if (EngineeringTemplateID !== undefined) {
                    API.procedure.post('ProcEngineeringToReview', {
                            UserID: $rootScope.user.getId(),
                            EngineeringTemplateID: EngineeringTemplateID,
                            PartNumber: $scope.TemplateDetail.PartNumber
                        })
                        .then(function(result) {
                            Notification.show('success', 'Engineering Review success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }

                if (WorkOrderID !== undefined) {
                    API.procedure.post('ProcWOEngineeringToReview', {
                            UserID: $rootScope.user.getId(),
                            WorkOrderID: WorkOrderID,
                            PartNumber: $scope.TemplateDetail.PartNumber
                        })
                        .then(function(result) {
                            Notification.show('success', 'Engineering Review success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };

        $scope.$on('AddTmpltProgram', function(event, args) {
            $scope.OpProgramNumber = '';
        });

        $scope.ViewADD = function() {
            API.procedure.post('ProcViewAllTmpltADD', {
                    PartTemplateID: $scope.TemplateDetail.PartTemplateID
                })
                .then(function(result) {
                    if (result.length > 0) {
                        var fileName = result[0].FileName;
                        var file = $rootScope.PathImageUpload + fileName;
                        $window.open(file, 'blank_');
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        };
    }

})(angular.module('app.components.department.engineering-template.engineering', []));
