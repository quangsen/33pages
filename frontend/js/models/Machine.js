var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Machine(options) {
    this.AirSystem = 0;
    this.CoolantFilter = 0;
    this.CoolantLevel = 0;
    this.CoolantType = null;
    this.Designation = '';
    this.Diameter = '';
    this.DifficultyLevel = '';
    this.Fanfilter = 0;
    this.GC = 0;
    this.Grd = 0;
    this.GreaseLevel = 0;
    this.HolderNo = '';
    this.InsertDatumPoint = '';
    this.IntegralLighting = 0;
    this.InterlocksEngaged = 0;
    this.Mil = 0;
    this.MinStickOut = '';
    this.MinfluteLength = '';
    this.Manufacturer = '';
    this.Model = '';
    this.Offset = '';
    this.OilFilter = 0;
    this.SlideOilLevel = 0;
    this.Spindle = '';
    this.StationNo = '';
    this.TargetConcMax = '';
    this.TargetConcMin = '';
    this.Trn = 0;
    this.oth = 0;

    BaseModel.call(this, options);
}

inherits(Machine, BaseModel);

Machine.prototype.setGC = function(value) {
    this.GC = value;
};

Machine.prototype.setGrd = function(value) {
    this.Grd = value;
};

Machine.prototype.setMil = function(value) {
    this.Mil = value;
};

Machine.prototype.setTrn = function(value) {
    this.Trn = value;
};

Machine.prototype.setoth = function(value) {
    this.oth = value;
};

Machine.prototype.getGC = function() {
    return this.GC;
};

Machine.prototype.getGrd = function() {
    return this.Grd;
};

Machine.prototype.getMil = function() {
    return this.Mil;
};

Machine.prototype.getTrn = function() {
    return this.Trn;
};

Machine.prototype.getoth = function() {
    return this.oth;
};

Machine.prototype.getAirSystem = function() {
    return this.AirSystem;
};

Machine.prototype.getCoolantFilter = function() {
    return this.CoolantFilter;
};

Machine.prototype.getCoolantLevel = function() {
    return this.CoolantLevel;
};

Machine.prototype.getCoolantType = function() {
    return this.CoolantType;
};

Machine.prototype.getDesignation = function() {
    return this.Designation;
};

Machine.prototype.getDiameter = function() {
    return this.Diameter;
};

Machine.prototype.getDifficultyLevel = function() {
    return this.DifficultyLevel;
};

Machine.prototype.getFanfilter = function() {
    return this.Fanfilter;
};

Machine.prototype.getGreaseLevel = function() {
    return this.GreaseLevel;
};

Machine.prototype.getHolderNo = function() {
    return this.HolderNo;
};

Machine.prototype.getInsertDatumPoint = function() {
    return this.InsertDatumPoint;
};

Machine.prototype.getIntegralLighting = function() {
    return this.IntegralLighting;
};

Machine.prototype.getInterlocksEngaged = function() {
    return this.InterlocksEngaged;
};

Machine.prototype.getMinStickOut = function() {
    return this.MinStickOut;
};

Machine.prototype.getMinfluteLength = function() {
    return this.MinfluteLength;
};

Machine.prototype.getManufacturer = function() {
    return this.Manufacturer;
};

Machine.prototype.getModel = function() {
    return this.Model;
};

Machine.prototype.getOffset = function() {
    return this.Offset;
};

Machine.prototype.getOilFilter = function() {
    return this.OilFilter;
};

Machine.prototype.getSlideOilLevel = function() {
    return this.SlideOilLevel;
};

Machine.prototype.getSpindle = function() {
    return this.Spindle;
};

Machine.prototype.getStationNo = function() {
    return this.StationNo;
};

Machine.prototype.getTargetConcMax = function() {
    return this.TargetConcMax;
};

Machine.prototype.getTargetConcMin = function() {
    return this.TargetConcMin;
};

module.exports = Machine;
