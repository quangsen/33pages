(function(app) {

    app.controller('EngineeringPeringCtrl', EngineeringPeringCtrl);
    EngineeringPeringCtrl.$inject = ['$rootScope', '$scope', 'API', 'Notification', '$state', '$stateParams'];

    function EngineeringPeringCtrl($rootScope, $scope, API, Notification, $state, $stateParams) {
        var EngineeringTemplateID = $stateParams.EngineeringTemplateID;
        var WorkOrderID = $stateParams.WorkOrderID;

        /**
         * the function working when click Engineering Review button 
         * in P/E Engineering Tab
         */
        $scope.EngineeringToReview = function() {
            var checkExist = $scope.checkExistEngWorkOrder();
            if (checkExist) {
                if (EngineeringTemplateID !== undefined) {
                    API.procedure.post('ProcEngineeringToReview', {
                            UserID: $rootScope.user.getId(),
                            EngineeringTemplateID: EngineeringTemplateID,
                            PartNumber: $scope.TemplateDetail.PartNumber
                        })
                        .then(function(result) {
                            Notification.show('success', 'Engineering Review success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }

                if (WorkOrderID !== undefined) {
                    API.procedure.post('ProcWOEngineeringToReview', {
                            UserID: $rootScope.user.getId(),
                            WorkOrderID: WorkOrderID,
                            PartNumber: $scope.TemplateDetail.PartNumber
                        })
                        .then(function(result) {
                            Notification.show('success', 'Engineering Review success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };
    }

})(angular.module('app.components.department.engineering-template.pering', []));
