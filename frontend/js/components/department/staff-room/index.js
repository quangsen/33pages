var Staff = require('../../../models/Staff');

(function(app) {

    app.controller('StaffRoomCtrl', StaffRoomCtrl);
    StaffRoomCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API', 'Notification', '$window', '$state'];

    function StaffRoomCtrl($rootScope, $scope, $stateParams, API, Notification, $window, $state) {

        /**
         * Use to swap Add & Update button
         */
        $scope.StaffUserID = $stateParams.StaffUserID;

        $scope.StaffSelected = function(staff) {
            var state = 'department.staff-room';
            var params = { StaffUserID: staff.StaffUserID };
            $rootScope.redirectDept(state, params);
        };

        if ($stateParams.StaffUserID !== undefined) {
            getStaffInfo($stateParams.StaffUserID);
        }

        function getStaffInfo(StaffUserID) {

            /**
             * get Staff Detail
             */
            API.procedure.post('ProcGetStaffDetail', {
                    StaffUserID: StaffUserID
                })
                .then(function(result) {
                    if (result.length > 0) {
                        $scope.StaffDetail = new Staff(result[0]);
                        $scope.StaffDetail.FullName = $scope.StaffDetail.FirstName + " " + $scope.StaffDetail.LastName;
                    }
                })
                .catch(function(error) {
                    console.log(error);
                });

            /**
             * get System Roles
             */
            API.procedure.get('ProcSystemRoles')
                .then(function(result) {
                    if (result.length > 0) {
                        result = _.uniqBy(result, 'RoleID');
                        $scope.SystemRoles = result;
                    }
                })
                .catch(function(error) {
                    console.log(error);
                });

            /**
             * get Staff Roles
             */
            API.procedure.post('ProcGetStaffRoles', {
                    StaffUserID: StaffUserID
                })
                .then(function(result) {
                    if (result.length > 0) {
                        result = _.uniqBy(result, 'RoleID');
                        $scope.StaffRoles = result;
                    }
                })
                .catch(function(error) {
                    console.log(error);
                });

            /**
             * add Assigned Role
             */
            $scope.AddAssignedRole = function(AssignedRole) {
                if (AssignedRole.length > 0) {
                    selectedAssignedRole = _.filter(AssignedRole, _.matches({ selected: true }));
                    if (selectedAssignedRole.length > 0) {
                        var saveDB = 0;
                        selectedAssignedRole.forEach(function(value, key) {
                            API.procedure.post('ProcAddAssignedRole', {
                                    UserID: $rootScope.user.getId(),
                                    StaffUserID: StaffUserID,
                                    RoleID: value.RoleID
                                })
                                .then(function(result) {
                                    saveDB++;
                                    if (saveDB === selectedAssignedRole.length) {
                                        $state.reload();
                                    }
                                })
                                .catch(function(error) {
                                    saveDB++;
                                    Notification.show('warning', 'Assigned Role has exitst');
                                    return false;
                                });
                        });
                    } else {
                        Notification.show('warning', 'Please select System Role');
                        return false;
                    }
                }
            };

            /**
             * Remove Assigned Role
             */
            $scope.RmvAssignedRole = function(StaffRoles) {
                var selectedStaffRole = _.filter(StaffRoles, _.matches({ selected: true }));
                if (selectedStaffRole.length > 0) {
                    var saveDB = 0;
                    selectedStaffRole.forEach(function(value, key) {
                        API.procedure.post('ProcRemoveAssignedRole', {
                                UserID: $rootScope.user.getId(),
                                StaffUserID: StaffUserID,
                                RoleID: value.RoleID
                            })
                            .then(function(result) {
                                saveDB++;
                                if (saveDB === selectedStaffRole.length) {
                                    $state.reload();
                                }
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                } else {
                    Notification.show('warning', 'Please select Assigned Role');
                    return false;
                }
            };
        }

        $scope.NewMember = function() {
            $scope.StaffUserID = undefined;
            $scope.StaffDetail = null;
            $scope.SystemRoles = null;
            $scope.StaffRoles = null;
        };

        $scope.SaveOrUpdateStaff = function(StaffDetail) {
            var FullName = StaffDetail.FullName.split(' ');
            if (FullName[0] !== undefined) {
                FirstName = FullName[0];
            } else {
                FirstName = '';
            }
            if (FullName[1] !== undefined) {
                LastName = FullName[1];
            } else {
                LastName = '';
            }

            // Save Staff
            if ($scope.StaffUserID === undefined) {
                var StaffMember = new Staff(StaffDetail);
                API.procedure.post('ProcAddNewStaff', {
                        UserID: $rootScope.user.getId(),
                        FirstName: FirstName,
                        LastName: LastName,
                        Address1: StaffMember.getAddress1(),
                        Address2: StaffMember.getAddress2(),
                        Address3: StaffMember.getAddress3(),
                        Address4: StaffMember.getAddress4(),
                        Postcode: StaffMember.getPostcode(),
                        HomePhone: StaffMember.getHomePhone(),
                        MobilePhone: StaffMember.getMobilePhone(),
                        Email: StaffMember.getEmail(),
                        ChargeRate: StaffMember.getChargeRate(),
                        SkillLevel: StaffMember.getSkillLevel(),
                        Employed: StaffMember.getEmployed(),
                        Extension: StaffMember.getExtension(),
                        ReportsTo: StaffMember.getReportsTo(),
                        POLimit: StaffMember.getPOLimit(),
                        SpendLimit: StaffMember.getSpendLimit(),
                        ForkOperator: StaffMember.getForkOperator(),
                        FirstAid: StaffMember.getFirstAid(),
                        LeaveDays: StaffMember.getLeaveDays(),
                        Salary: StaffMember.getSalary(),
                        Hours: StaffMember.getHours(),
                        Security: StaffMember.getSecurity(),
                    })
                    .then(function(result) {
                        $state.reload();
                        Notification.show('success', 'Add Staff Member success');
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            } else {

                // Update Staff
                API.procedure.post('ProcUpdateStaffDetail', {
                        UserID: $rootScope.user.getId(),
                        StaffUserID: $scope.StaffUserID,
                        FirstName: FirstName,
                        LastName: LastName,
                        Address1: StaffDetail.getAddress1(),
                        Address2: StaffDetail.getAddress2(),
                        Address3: StaffDetail.getAddress3(),
                        Address4: StaffDetail.getAddress4(),
                        Postcode: StaffDetail.getPostcode(),
                        HomePhone: StaffDetail.getHomePhone(),
                        MobilePhone: StaffDetail.getMobilePhone(),
                        Email: StaffDetail.getEmail(),
                        ChargeRate: StaffDetail.getChargeRate(),
                        SkillLevel: StaffDetail.getSkillLevel(),
                        Employed: StaffDetail.getEmployed(),
                        Extension: StaffDetail.getExtension(),
                        ReportsTo: StaffDetail.getReportsTo(),
                        POLimit: StaffDetail.getPOLimit(),
                        SpendLimit: StaffDetail.getSpendLimit(),
                        ForkOperator: StaffDetail.getForkOperator(),
                        FirstAid: StaffDetail.getFirstAid(),
                        LeaveDays: StaffDetail.getLeaveDays(),
                        Salary: StaffDetail.getSalary(),
                        Hours: StaffDetail.getHours(),
                        Security: StaffDetail.getSecurity(),
                    })
                    .then(function(result) {
                        Notification.show('success', 'Update success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.beforeUpload = function(file) {
            if ($scope.StaffUserID === undefined) {
                Notification.show('warning', 'Please select Staff member');
                return false;
            }
            return true;
        };

        $scope.uploadSuccess = function(success) {
            API.procedure.post('ProcAddEmploymentContract', {
                    StaffUserID: $scope.StaffUserID,
                    FileName: success.data
                })
                .then(function(result) {
                    Notification.show('success', 'Drop file success. The file is store at: public' + $rootScope.PathImageUpload + response.data);
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };

    }

})(angular.module('app.components.department.staff-room', []));
