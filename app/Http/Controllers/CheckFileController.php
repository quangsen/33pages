<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

class CheckFileController extends ApiController {
	public function checkFile(Request $request) {
		$data = $request->only(['fileType', 'fileName']);
		if (empty($data)) {
			throw new ApiException("Missing parameters", 1);
		}

		$fileType = $data['fileType'];
		$fileName = $data['fileName'];

		switch ($fileType) {
		case 'image':
			$dirFile = 'resources/uploads/images';
			break;
		case 'excel':
			$dirFile = 'resources/uploads/excels';
			break;
		}
		$file = $dirFile . '/' . $fileName;
		if (file_exists(public_path($file))) {
			return $this->response('File Exist');
		} else {
			throw new ApiException("File not exist", 1);
		}
	}
}
