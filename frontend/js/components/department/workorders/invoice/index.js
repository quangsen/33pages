(function(app) {
    app.controller('WorkOrderInvoiceCtrl', WorkOrderInvoiceCtrl);
    WorkOrderInvoiceCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API', 'Notification', '$state'];

    function WorkOrderInvoiceCtrl($rootScope, $scope, $stateParams, API, Notification, $state) {
        var WorkOrderID = $rootScope.WorkOrderID = $stateParams.WorkOrderID;
        if (WorkOrderID !== undefined && WorkOrderID !== '') {
            if ($scope.WorkOrderDetail === undefined) {
                $scope.getWorkOrderInfo(WorkOrderID);
            }
            var regexDecimal5_2 = /^(\d{1,5}|\d{1,4}\.\d{1}|\d{1,3}\.\d{1,2})$/;
            var regexDecimal7_2 = /^(\d{1,7}|\d{1,6}\.\d{1}|\d{1,5}\.\d{1,2})$/;
            var regexNumber = /^\d+$/;

            /**
             * get table on left botton screen
             */
            API.procedure.get('ProcGetIncompleteInvoicedWOs')
                .then(function(result) {
                    $scope.IncompleteInvoicedWOs = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get main table on top screen
             */
            $scope.getWOOutvoices = function() {
                API.procedure.post('ProcGetWOOutvoices', {
                        WorkOrderID: WorkOrderID
                    })
                    .then(function(result) {
                        result.forEach(function(value) {
                            if (value.Complete === null | value.Complete === undefined) {
                                value.Complete = false;
                            } else {
                                value.Complete = true;
                            }
                        });
                        $scope.WOOutvoices = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };
            $scope.getWOOutvoices();

            $scope.selectInvoicedWOs = function(item) {
                $scope.QtyWOInvoice = item.Qty;

                API.procedure.post('ProcGetWORaisedOutvoices', {
                        WorkOrderID: item.WONo
                    })
                    .then(function(result) {
                        if (result.length > 0) {
                            $scope.WORaisedOutvoices = result;
                        }
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.CreateInvoice = function(IncompleteInvoicedWOs, Qty) {
                var isSelect = false;
                if (Qty === undefined || Qty === null) {
                    Notification.show('warning', 'Please enter Qty!');
                    return false;
                } else {
                    Qty = parseInt(Qty);
                    IncompleteInvoicedWOs.forEach(function(value) {
                        if (value.selected === true && value.selected !== undefined) {
                            isSelect = true;

                            if (Qty > value.Qty) {
                                Notification.show('warning', 'Qty outvoice must be smaller qty invoice!');
                                return false;
                            }

                            /**
                             * Add WorkOrder Outvoice
                             */
                            API.procedure.post('ProcAddWOOutvoice', {
                                    UserID: $rootScope.user.getId(),
                                    WorkOrderID: value.WONo,
                                    Qty: Qty
                                })
                                .then(function(result) {
                                    $state.reload();
                                })
                                .catch(function(error) {
                                    throw error;
                                });
                        }
                    });

                    if (isSelect === false) {
                        Notification.show('warning', 'Please select WOs to invoice!');
                        return false;
                    }
                }
            };

            $scope.countValueGrantTotal = function(WOOutvoices, ValueExVAT) {
                $scope.grantTotal = 0;
                var WOOutvoice = _.find(WOOutvoices, { 'selected': true });
                if (WOOutvoice !== undefined) {
                    if (ValueExVAT === false || ValueExVAT === undefined) {
                        if (regexDecimal7_2.test(WOOutvoice.TotalPrice)) {
                            $scope.ValueExVAT = WOOutvoice.TotalPrice;
                        } else {
                            $scope.ValueExVAT = 0;
                        }
                    } else {
                        $scope.ValueExVAT = ValueExVAT;
                    }
                    if (regexNumber.test(WOOutvoice.VATRate)) {
                        $scope.grantTotal = (parseFloat($scope.ValueExVAT) * parseFloat(WOOutvoice.VATRate) / 100) + parseFloat($scope.ValueExVAT);
                        $scope.grantTotal = Math.round($scope.grantTotal * 100) / 100;
                    } else {
                        $scope.grantTotal = 0;
                    }
                }
            };

            $scope.UpdateWOOutvoice = function(WOOutvoices, ValueExVAT) {
                var WOOutvoice = _.find(WOOutvoices, { 'selected': true });
                if (WOOutvoice !== undefined) {
                    if (regexDecimal7_2.test(ValueExVAT)) {
                        $scope.countValueGrantTotal(WOOutvoices, ValueExVAT);
                        if (regexDecimal5_2.test(WOOutvoice.DeliveryChg)) {
                            if (WOOutvoice.Complete === true) {
                                WOOutvoice.Complete = 0;
                            }
                            API.procedure.post('ProcUpdateWOOutvoice', {
                                    UserID: $rootScope.user.getId(),
                                    InvoiceID: WOOutvoice.InvoiceNo,
                                    DelCharge: parseFloat(WOOutvoice.DeliveryChg),
                                    ValueExVat: ValueExVAT,
                                    WOComplete: WOOutvoice.Complete
                                })
                                .then(function(result) {
                                    WOOutvoice.TotalPrice = ValueExVAT;
                                    console.log(WOOutvoice);
                                    if (WOOutvoice.Complete === 1) {
                                        $scope.getWOOutvoices();
                                    }
                                })
                                .catch(function(error) {
                                    throw error;
                                });
                        }
                    }
                }
            };

            $scope.openBIRT = function() {
                var WORaisedOutvoiceSelected = _.find($scope.WORaisedOutvoices, { 'selected': true });
                if (WORaisedOutvoiceSelected === undefined) {
                    Notification.show('warning', 'Please select Raised invoice');
                    return false;
                }
                var params = {
                    InvoiceNo: WORaisedOutvoiceSelected.InvoiceNo
                };
                $rootScope.openTabBirt('Invoice', params);
            };

            $scope.viewTop = function() {
                var WOOutvoicesSelected = _.find($scope.WOOutvoices, { 'selected': true });
                if (WOOutvoicesSelected === undefined) {
                    Notification.show('warning', 'Please select Invoice table');
                    return false;
                }
                var params = {
                    InvoiceNo: WOOutvoicesSelected.InvoiceNo
                };
                $rootScope.openTabBirt('Invoice', params);
            };

            $scope.markSent = function() {
                var WORaisedOutvoice = _.find($scope.WORaisedOutvoices, { 'selected': true });
                if (WORaisedOutvoice === undefined) {
                    Notification.show('warning', 'Please select Raised invoice');
                    return false;
                }
                API.procedure.post('ProcInvoiceSent', {
                        UserID: $rootScope.user.getId(),
                        InvoiceID: WORaisedOutvoice.InvoiceNo
                    })
                    .then(function(result) {
                        Notification.show('success', result[0].MsgResult);
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

        } else {
            var state = 'department.workorders';
            $rootScope.redirectDept(state);
            return false;
        }

    }
})(angular.module('app.components.department.workorders.invoice', []));
