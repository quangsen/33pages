var Template = require('../../../../models/Template');

(function(app) {

    app.controller('PartDocsCtrl', PartDocsCtrl);
    PartDocsCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function PartDocsCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {
        var PTemplateID = $stateParams.PTemplateID;
        if (PTemplateID !== undefined && PTemplateID !== '') {
            $scope.TemplateInfo = new Template('');

            /**
             * get document drop-down
             */
            API.procedure.get('ProcGetDocumentDropTypes')
                .then(function(result) {
                    $scope.DocumentDropTypes = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Approval list
             */
            API.procedure.get('ProcGetAllApprovalList')
                .then(function(result) {
                    $scope.ApprovalList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Master Part No list
             */
            API.procedure.get('ProcPartNumberList')
                .then(function(result) {
                    $scope.NumberList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get data of table
             */
            API.procedure.post('ProcGetPartDocs', {
                    PTemplateID: PTemplateID,
                })
                .then(function(result) {
                    $scope.PartDocs = result;
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.changePartNo = function(PartNo) {
                $scope.TemplateInfo.DrawingNumber = PartNo;
            };

            $scope.changePartIssue = function(Issue) {
                $scope.TemplateInfo.Issue = Issue;
            };

            $scope.AddTemplate = function(Template) {
                API.procedure.post('ProcAddNewPartTemplate', {
                        UserID: $rootScope.user.getId(),
                        PartNumber: Template.getPartNumber(),
                        PartIssue: Template.getPartIssue(),
                        Description: Template.getDescription(),
                        LeadDays: Template.getLeadDays(),
                        Price: Template.getPrice(),
                        DrawingNumber: Template.getDrawingNumber(),
                        Issue: Template.getIssue(),
                        ApprovalID: Template.getApprovalID(),
                        AssemblyQty: Template.getAssemblyQty(),
                        PartTemplateID: Template.getPartTemplateID()
                    })
                    .then(function(result) {
                        Notification.show('success', 'Add New Part Template success');
                    })
                    .catch(function(error) {
                        // Notification.show('warning', 'Drawing Number and Issue must match the Reference and IssueNumber fields of a drawing already in the Drawing table');
                    });
            };

            $scope.beforeUpload = function(file) {
                if ($scope.AddPartDoc === undefined) {
                    Notification.show('warning', 'Please select Document Drop type');
                    return false;
                }
                if ($scope.AddPartDoc.DocumentDropType === undefined) {
                    Notification.show('warning', 'Please select Document Drop type');
                    return false;
                } else {
                    if (parseInt($scope.AddPartDoc.DocumentDropType.pkDocumentDropTypeID) === 4) {
                        if ($scope.AddPartDoc.Reference === undefined || $scope.AddPartDoc.Reference === '') {
                            Notification.show('warning', 'Please enter Reference');
                            return false;
                        }
                        if ($scope.AddPartDoc.Issue === undefined || $scope.AddPartDoc.Issue === '') {
                            Notification.show('warning', 'Please enter Issue');
                            return false;
                        }
                    } else {
                        $scope.AddPartDoc.Reference = $scope.AddPartDoc.Issue = "";
                    }
                }
                return true;
            };

            $scope.uploadSuccess = function(response) {
                API.procedure.post('ProcAddPartDoc', {
                        PTemplateID: PTemplateID,
                        UserID: $rootScope.user.getId(),
                        DocTypeID: $scope.AddPartDoc.DocumentDropType.pkDocumentDropTypeID,
                        FileName: response.data,
                        Reference: $scope.AddPartDoc.Reference,
                        Issue: $scope.AddPartDoc.Issue
                    })
                    .then(function(result) {
                        Notification.show('success', 'Drop file success.');
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.ViewFileName = function() {
                var selectedPartDocs = _.find($scope.PartDocs, { 'selected': true });
                if (selectedPartDocs === undefined) {
                    Notification.show('warning', 'Please select an item to view');
                    return false;
                }
                var FileName = $rootScope.PathImageUpload + selectedPartDocs.FileName;
                $window.open(FileName, 'blank_');
            };

            $scope.redirectBack = function() {
                $window.history.back();
            };

        } else {
            $state.go('department.part-control.part-list');
        }

    }

})(angular.module('app.components.department.part-control.part-docs', []));
