(function(app) {

    app.controller('InspectionScheduleMainCtrl', InspectionScheduleMainCtrl);
    InspectionScheduleMainCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function InspectionScheduleMainCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {

        /**
         * Sort Table
         */
        $scope.sortTable = [
            { title: 'WO No.', sortBy: 'WorkOrderID' },
            { title: 'CO No.', sortBy: 'OrderID' },
            { title: 'Part Number', sortBy: 'PartTemplateID' },
            { title: 'Qty', sortBy: 'Qty' },
            { title: 'Operation', sortBy: 'Operation' },
            { title: 'DD Start', sortBy: 'DDStartDate' },
            { title: 'Start', sortBy: 'StartDate' },
            { title: 'Finish', sortBy: 'FinishDate' },
            { title: 'DD Finish', sortBy: 'DDFinishDate' },
            { title: '2nd Resource', sortBy: 'Resource' },
            { title: 'Insp. Type', sortBy: 'InspectionType' },
        ];
        $scope.sortBy = $scope.sortTable[0].sortBy;
        $scope.changeSortBy = function(item) {
            $scope.sortBy = item.sortBy;
        };

        API.procedure.get('ProcGetInspectionSchedule')
            .then(function(result) {
                $scope.InspectionSchedule = result;
                if ($scope.InspectionSchedule.length > 0) {
                    $scope.InspectionSchedule.forEach(function(value) {
                        if (value.Qty !== null && value.Qty !== "") {
                            value.Qty = parseFloat(value.Qty);
                        }
                    });
                }
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * get data Goods in table
         */
        API.procedure.get('ProcGetGoodsInList')
            .then(function(result) {
                $scope.GoodsInList = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.redirectInspect = function(Inspection, Goodsin) {
            isInspec = isGoods = false;
            var selectedInspec = _.find(Inspection, ({ 'selected': true }));
            if (selectedInspec !== undefined) {
                isInspec = true;
            }
            var selectedGoodsin = _.find(Goodsin, ({ 'selected': true }));
            if (selectedGoodsin !== undefined) {
                isGoods = true;
            }
            if (isInspec === false && isGoods === false) {
                Notification.show('warning', 'Please select Inspection or GRB');
                return false;
            }
            if (isInspec === true) {
                var state = 'department.inspection-schedule.inspection';
                var params = {
                    OperationID: selectedInspec.OperationID,
                    WorkOrderID: selectedInspec.WorkOrderID,
                    PartNumber: selectedInspec.PartNumber,
                    GRBID: undefined
                };
                $rootScope.redirectDept(state, params);
            } 
            if (isInspec === false) {
                var statePage = 'department.inspection-schedule.inspection';
                param = {
                    OperationID: undefined,
                    WorkOrderID: undefined,
                    PartNumber: undefined,
                    GRBID: selectedGoodsin.GRBID
                };
                $rootScope.redirectDept(statePage, param);
            }
        };

        $scope.redirectWorkOrder = function(WoID) {
            var state = 'department.workorders.main';
            var params = {
                WorkOrderID: WoID
            };
            $rootScope.redirectDept(state, params);
        };

        $scope.redirectCustomerOrder = function(COID) {
            var state = 'department.customer-order';
            var params = {
                CustomerOrderId: COID
            };
            $rootScope.redirectDept(state, params);
        };

        $scope.redirectPartTemplate = function(PartTemplateID) {
            var state = 'department.part-control.part-list';
            var params = {
                PTemplateID: PartTemplateID
            };
            $rootScope.redirectDept(state, params);
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };

    }

})(angular.module('app.components.department.inspection-schedule.main', []));
