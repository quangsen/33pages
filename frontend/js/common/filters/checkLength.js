(function(app) {

    app.filter('checkLength', function() {
        return function(typeCheck, length, str) {
            if (typeCheck !== 'min' || typeCheck !== 'max') {
                return false;
            }

            if (typeCheck === 'min') {
                if (str.length > length) {
                    return false;
                }
            }

            if (typeCheck === 'max') {
                if (str.length < length) {
                    return false;
                }
            }
        };
    });

})(angular.module('app.common.filters.checkLength', []));
