(function(app) {

    app.controller('PurchaseOrderRaiseCtrl', PurchaseOrderRaiseCtrl);
    PurchaseOrderRaiseCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', 'Notification'];

    function PurchaseOrderRaiseCtrl($rootScope, $scope, API, $state, $stateParams, Notification) {
        $rootScope.PurchaseOrderDetail = $stateParams;
        if ($rootScope.PurchaseOrderDetail.RFQNo !== undefined) {

            /**
             * Sort Table
             */
            $scope.sortTable = [
                { title: 'Order date', sortBy: 'OrderDate' },
                { title: 'Supplier', sortBy: 'SupplierName' },
                { title: 'Qty', sortBy: 'Qty' },
                { title: 'Op No', sortBy: 'OpNo' },
                { title: 'Unit', sortBy: 'Unit' },
                { title: 'Description', sortBy: 'Description' },
                { title: 'Size', sortBy: 'Size' },
                { title: 'Shape', sortBy: 'Shape' },
                { title: 'Each', sortBy: 'EachPrice' },
                { title: 'Value', sortBy: 'OrderValue' },
            ];
            $scope.sortBy = $scope.sortTable[0].sortBy;
            $scope.changeSortBy = function(item) {
                $scope.sortBy = item.sortBy;
            };

            if ($scope.PurchaseOrderDetail !== undefined) {
                if ($scope.PurchaseOrderDetail.PartNumber !== undefined) {
                    API.procedure.post('ProcGetPrevPurchases', {
                            PartNumber: $scope.PurchaseOrderDetail.PartNumber
                        })
                        .then(function(result) {
                            $scope.PrevPurchases = result;
                            if ($scope.PrevPurchases.length > 0) {
                                $scope.PrevPurchases.forEach(function(value) {
                                    if (value.Qty !== null && value.Qty !== "") {
                                        value.Qty = parseInt(value.Qty);
                                    }
                                    if (value.PONo !== null && value.PONo !== "") {
                                        value.PONo = parseInt(value.PONo);
                                    }
                                    if (value.Size !== null && value.Size !== "") {
                                        value.Size = parseFloat(value.Size);
                                    }
                                    if (value.EachPrice !== null && value.EachPrice !== "") {
                                        value.EachPrice = parseFloat(value.EachPrice);
                                    }
                                    if (value.OrderValue !== null && value.OrderValue !== "") {
                                        value.OrderValue = parseFloat(value.OrderValue);
                                    }
                                });
                            }
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }

            $scope.CopyPO = function() {
                var selectedPrevPurchase = _.find($scope.PrevPurchases, { 'selected': true });
                if (selectedPrevPurchase !== undefined) {
                    API.procedure.post('ProcCopyPO', {
                            UserID: $rootScope.user.getId(),
                            PONo: selectedPrevPurchase.PONo
                        })
                        .then(function(result) {
                            Notification.show('success', 'Copy PO success');
                            var state = 'department.purchase-order.raise';
                            var params = {
                                RFQNo: result[0].RFQOID
                            };
                            $rootScope.redirectDept(state, params);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                } else {
                    Notification.show('warning', 'Please select Previous purchase');
                    return false;
                }
            };

            /**
             * @param  type enum{'preview', 'raise'}
             * @return {[type]}      [description]
             */
            $scope.openBIRT = function(type) {
                var params = {};
                if (type === 'preview') {
                    var RFQNo = $rootScope.PurchaseOrderDetail.RFQNo;
                    if (RFQNo === undefined || RFQNo === '') {
                        Notification.show('warning', 'Please select RFQNo');
                        return false;
                    }
                    params.RFQOID = RFQNo;
                    $rootScope.openTabBirt('PurchaseOrderPreview', params);
                }
                if (type === 'raise') {
                    $scope.$watch('user', function(user) {
                        if (user !== undefined) {
                            API.procedure.post('ProcRaiseReprintPO', {
                                    UserID: user.getId(),
                                    RFQOID: $rootScope.PurchaseOrderDetail.RFQNo
                                })
                                .then(function(result) {
                                    var PONo = result[0].PONo;
                                    var params = {
                                        PONo: PONo
                                    };
                                    $rootScope.openTabBirt('PurchaseOrder_v5', params);
                                    var state = 'department.purchase-order.raise';
                                    $rootScope.redirectDept(state, params);
                                })
                                .catch(function(error) {
                                    throw error;
                                });
                        }
                    });
                }
            };

        } else {
            var state = 'department.purchase-order';
            $rootScope.redirectDept(state);
        }
    }

})(angular.module('app.components.department.purchase-order.raise', []));
