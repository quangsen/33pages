(function(app) {
    app.factory('GlobalResource', GlobalResource);
    GlobalResource.$inject = ['$q', '$resource', 'ApiUrl'];

    function GlobalResource($q, $resource, ApiUrl) {
        var url = ApiUrl.get('/api/v1/:url');

        var resource = $resource(url, {}, {
            get: {
                method: 'GET'
            },
            post: {
                method: 'POST'
            }
        });

        return resource;
    }
})(angular.module('app.api.globalResource', []));
