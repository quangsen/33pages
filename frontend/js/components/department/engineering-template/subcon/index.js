(function(app) {

    app.controller('EngineeringSubconCtrl', EngineeringSubconCtrl);
    EngineeringSubconCtrl.$inject = ['$rootScope', '$scope', 'API', 'Notification', '$state', '$stateParams', '$window'];

    function EngineeringSubconCtrl($rootScope, $scope, API, Notification, $state, $stateParams, $window) {
        var EngineeringTemplateID = $stateParams.EngineeringTemplateID;
        var WorkOrderID = $stateParams.WorkOrderID;
        var OperationID = $stateParams.OperationID;
        var OperationTemplateID = $stateParams.OperationTemplateID;
        var hasOperationID = false;
        var numberRegex = /^\d+$/;

        if (OperationID !== undefined && OperationID !== '' && OperationID !== null) {
            hasOperationID = true;

            API.procedure.post('ProcGetWOOpTmpltSubconDetail', {
                    OperationID: OperationID
                })
                .then(function(result) {
                    $scope.OpTmpltSubconDetail = result[0];
                    if (!_.isNil($scope.OpTmpltSubconDetail) && !_.isNil($scope.OpTmpltSubconDetail.SupplierName)) {
                        $scope.subconSearchInputModel = $scope.OpTmpltSubconDetail.SupplierName;
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        }
        if (OperationTemplateID !== undefined && OperationTemplateID !== '' && OperationTemplateID !== null) {
            hasOperationID = true;
            API.procedure.post('ProcGetOpTmpltSubconDetail', {
                    OperationTemplateID: OperationTemplateID
                })
                .then(function(result) {
                    $scope.OpTmpltSubconDetail = result[0];
                    if (!_.isNil($scope.OpTmpltSubconDetail) && !_.isNil($scope.OpTmpltSubconDetail.SupplierName)) {
                        $scope.subconSearchInputModel = $scope.OpTmpltSubconDetail.SupplierName;
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        }

        if (!hasOperationID) {
            $rootScope.SCApprovalCommentItems = undefined;
            $rootScope.OpTmpltIntTreatNotes = undefined;
        }

        /**
         * the function when user select Supplier
         */
        $scope.SupplierSelected = function(item) {
            $scope.selectedSupplier = item;
        };

        $scope.PurchaseReview = function() {
            var checkExist = $scope.checkExistEngWorkOrder();
            if (checkExist) {
                if (EngineeringTemplateID !== undefined) {
                    API.procedure.post('ProcPurchToReviewSC', {
                            UserID: $rootScope.user.getId(),
                            PartTemplateID: $scope.TemplateDetail.PartTemplateID,
                            ETemplateID: EngineeringTemplateID,
                            PartNumber: $scope.TemplateDetail.PartNumber
                        })
                        .then(function(result) {
                            Notification.show('success', 'Purchasing Review Subcon success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }

                if (WorkOrderID !== undefined) {
                    API.procedure.post('ProcWOPurchToReviewSC', {
                            UserID: $rootScope.user.getId(),
                            WorkOrderID: WorkOrderID,
                            PartNumber: $scope.TemplateDetail.PartNumber
                        })
                        .then(function(result) {
                            Notification.show('success', 'Purchasing Review Subcon success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };

        /**
         * the function working when user typing approval comment
         */
        API.procedure.post('ProcSCApprovalComments', {
                CommentSearch: ''
            })
            .then(function(result) {
                $scope.SCApprovalComments = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.addSCApprovalComment = function(Comment) {
            $rootScope.$broadcast('updateThead');
            var data;
            if (Comment !== undefined && Comment !== "") {
                data = {
                    Comment: Comment
                };
                $scope.ApprovalComment = undefined;

                if (OperationID !== undefined && OperationID !== "") {
                    $rootScope.addRootApprovalComment(data, OperationID, 'subcon');
                }
                if (OperationTemplateID !== undefined && OperationTemplateID !== "") {
                    $rootScope.addRootApprovalComment(data, OperationTemplateID, 'subcon');
                }
            }
        };

        /**
         * the function working when user press enter key input of Sub-con Notes table
         */
        $scope.AddTreatNote = function(Note) {
            $rootScope.$broadcast('updateThead');
            if ($rootScope.OpTmpltIntTreatNotes === undefined) {
                $rootScope.OpTmpltIntTreatNotes = [];
            }
            var data;
            if (Note !== undefined && Note !== "") {
                data = {
                    Note: Note
                };
            }
            $rootScope.OpTmpltIntTreatNotes.unshift(data);
            $scope.TreatNote = undefined;
            if (OperationID !== undefined && OperationID !== "") {
                $rootScope.addRootTreatNote(data, OperationID);
            }
            if (OperationTemplateID !== undefined && OperationTemplateID !== "") {
                $rootScope.addRootTreatNote(data, OperationTemplateID);
            }
        };

        /**
         * the function working when user click Update button
         */
        $scope.UpdateSubconDetail = function(OpTmpltSubconDetail) {
            if (OpTmpltSubconDetail !== undefined) {
                if (!hasOperationID) {
                    Notification.show('warning', 'Please select Operation');
                    return false;
                }
                if (OpTmpltSubconDetail === undefined || OpTmpltSubconDetail.LeadTime === undefined || OpTmpltSubconDetail.LeadTime === '' || OpTmpltSubconDetail.LeadTime === null) {
                    OpTmpltSubconDetail.LeadTime = 0;
                }
                if (!numberRegex.test(OpTmpltSubconDetail.LeadTime)) {
                    Notification.show('warning', 'Std. lead time must be number');
                    return false;
                }
                if ($scope.selectedSupplier === undefined) {
                    $scope.selectedSupplier = {
                        SupplierID: null
                    };
                }
                if (EngineeringTemplateID !== undefined) {
                    API.procedure.post('ProcUpdateOpTmpltSubconDetail', {
                            UserID: $rootScope.user.getId(),
                            OperationTemplateID: OperationTemplateID,
                            SupplierID: $scope.selectedSupplier.SupplierID,
                            LeadTime: OpTmpltSubconDetail.LeadTime
                        })
                        .then(function(result) {
                            Notification.show('success', 'Update Operation Template Subcon success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
                if (WorkOrderID !== undefined) {
                    API.procedure.post('ProcUpdateWOOpTmpltSubconDetail', {
                            UserID: $rootScope.user.getId(),
                            OperationID: OperationID,
                            SupplierID: $scope.selectedSupplier.SupplierID,
                            LeadTime: OpTmpltSubconDetail.LeadTime
                        })
                        .then(function(result) {
                            Notification.show('success', 'Update Operation Template Subcon success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };

        $scope.mailTo = function() {
            var data = $rootScope.Engineering_MaterialTab_TemplateMaterialInfo;
            var Unit = _.find($scope.UnitList, function(item) {
                return parseInt(item.value) === parseInt(data.UnitID);
            });
            if (!_.isUndefined(Unit)) {
                Unit = Unit.name;
            } else {
                Unit = '';
            }

            var Shape = _.find($scope.MaterialShapes, function(item) {
                return parseInt(item.pkShapeID) === parseInt(data.ShapeID);
            });
            if (!_.isUndefined(Shape)) {
                Shape = Shape.Description;
            } else {
                Shape = '';
            }

            var Approval = _.find($scope.MaterialLists, function(item) {
                return parseInt(item.ApprovalID) === parseInt(data.ApprovalID);
            });
            if (!_.isUndefined(Approval)) {
                Approval = Approval.Description;
            } else {
                Approval = '';
            }

            var SPU = _.find($scope.UOMList, function(item) {
                return parseInt(item.pkUOMID) === parseInt(data.SPUID);
            });
            if (!_.isUndefined(SPU)) {
                SPU = SPU.Description;
            } else {
                SPU = '';
            }
            var firstRow = "Please provide a quotation for the following:";
            var secondRow = data.Description + " @ " + data.Size + Unit + " " + Shape + " " + SPU + ", to " + Approval + " Approval";
            var thirdRow = "Thank you.";
            var formattedBody = firstRow + "\n\n" + secondRow + "\n\n" + thirdRow;
            var mailToLink = "mailto:simbiisltd@gmail.com?body=" + encodeURIComponent(formattedBody);
            $window.location.href = mailToLink;
        };
    }

})(angular.module('app.components.department.engineering-template.subcon', []));
