var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function OurApproval(options) {
    this.Approval = '';
    this.pkApprovalID = null;

    BaseModel.call(this, options);
}

inherits(OurApproval, BaseModel);

OurApproval.prototype.getApproval = function() {
    return this.Approval;
};

OurApproval.prototype.getPkApprovalID = function() {
    return this.pkApprovalID;
};

module.exports = OurApproval;