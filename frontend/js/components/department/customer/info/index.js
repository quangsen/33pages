var Customer = require('../../../../models/Customer.js');
(function(app) {

    app.controller('CustomerInfoCtrl', CustomerInfoCtrl);

    CustomerInfoCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API', '$state', 'Notification'];

    function CustomerInfoCtrl($rootScope, $scope, $stateParams, API, $state, Notification) {
        var customer_id = $stateParams.customer_id;
        if (customer_id !== undefined) {
            $scope.showUpdateButton = true;
        }
        $scope.$watch('CustomerInfo', function(CustomerInfo) {
            if (CustomerInfo === undefined || CustomerInfo === null) {
                $scope.getCustomerInfo(customer_id);
            } else {
                /*
                 * get customer notes
                 */
                API.procedure.post('ProcGetCustomerConformityNotes', {
                        CustomerID: customer_id,
                    })
                    .then(function(result) {
                        $scope.CustomerConformityNotes = result;
                    })
                    .catch(function(error) {
                        throw (error);
                    });
            }
        });

        /**
         * Add Note
         */
        $scope.addConformityNotes = function(Note) {
            if (Note !== undefined && Note !== '') {
                API.procedure.post('ProcAddCustomerConformityNote', {
                        UserID: $rootScope.user.getId(),
                        CustomerID: customer_id,
                        ConformityNote: Note
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            } else {
                Notification.show('warning', 'Please enter Standard Conformity Notes');
                return false;
            }
        };

        /**
         * copy Address
         */
        $scope.copyAddress = function() {
            $scope.CustomerInfo.InvAddress1 = $scope.CustomerInfo.Address1;
            $scope.CustomerInfo.InvAddress2 = $scope.CustomerInfo.Address2;
            $scope.CustomerInfo.InvAddress3 = $scope.CustomerInfo.Address3;
            $scope.CustomerInfo.InvAddress4 = $scope.CustomerInfo.Address4;
            $scope.CustomerInfo.InvPostcode = $scope.CustomerInfo.Postcode;
        };
    }

})(angular.module('app.components.customer.info', ['app.template']));
