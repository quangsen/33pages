(function(app) {

    app.filter('contain', function() {
        return function(array, key, data) {
            if (!_.isUndefined(array) && !_.isUndefined(_(array).head())) {
                if (!_.has(_(array).head(), key) || data === undefined || data === '') {
                    return array;
                } else {
                    return _.filter(array, function(item) {
                        var pattern = new RegExp(data, "i");
                        return pattern.test(_.toString(item[key]));
                    });
                }
            }
        };
    });
    app.controller('MaterialIssueCtrl', MaterialIssueCtrl);
    MaterialIssueCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function MaterialIssueCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {
        var WorkOrderID = $scope.WorkOrderID = $stateParams.WorkOrderID;
        var GRBNo = $scope.GRBNo = $stateParams.GRBNo;

        /**
         * Sort Table
         */
        $scope.sortTable = [
            { title: 'GRB No.', sortBy: 'GRBNo' },
            { title: 'Spec', sortBy: 'Spec' },
            { title: 'Size', sortBy: 'Size' },
            { title: 'units', sortBy: 'Units' },
            { title: 'Shape', sortBy: 'Shape' },
            { title: 'Release', sortBy: 'ReleaseLevel' },
            { title: 'Purch. Lgth', sortBy: 'PurchaseLength' },
            { title: 'Date in', sortBy: 'DateIn' },
            { title: 'Ord. Qty', sortBy: 'OrderQty' },
            { title: 'In Stk', sortBy: 'InStock' },
            { title: 'Locn', sortBy: 'Location' },
        ];
        $scope.sortBy = $scope.sortTable[0].sortBy;
        $scope.changeSortBy = function(item) {
            $scope.sortBy = item.sortBy;
        };

        /**
         * get In stock table
         */
        API.procedure.get('ProcMaterialInStock')
            .then(function(result) {
                if (result.length > 0) {
                    result.forEach(function(value) {
                        if (value.GRBNo !== null && value.GRBNo !== "") {
                            value.GRBNo = parseFloat(value.GRBNo);
                        }
                        if (value.Size !== null && value.Size !== "") {
                            value.Size = parseFloat(value.Size);
                        }
                        if (value.PurchaseLength !== null && value.PurchaseLength !== "") {
                            value.PurchaseLength = parseFloat(value.PurchaseLength);
                        }
                        if (value.OrderQty !== null && value.OrderQty !== "") {
                            value.OrderQty = parseFloat(value.OrderQty);
                        }
                        if (value.InStock !== null && value.InStock !== "") {
                            value.InStock = parseFloat(value.InStock);
                        }
                    });
                    if ($stateParams.GRBNo !== undefined) {
                        var selectMaterial = _.find(result, function(value) {
                            return parseInt(value.GRBNo) === parseInt($stateParams.GRBNo);
                        });
                        if (selectMaterial !== undefined) {
                            selectMaterial.selected = true;
                        }
                    }
                }
                $scope.MaterialInStock = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.getMaterialIssues = function(GRBNo) {
            API.procedure.post('ProcGetMaterialIssues', {
                    GRBNo: GRBNo
                })
                .then(function(result) {
                    $scope.MaterialIssues = result;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * the function working when click a row of In stock table
         */
        $scope.selectMaterialInStock = function(item) {
            if (WorkOrderID === undefined) {
                var state = 'department.material-issue';
                var params = {
                    WorkOrderID: undefined,
                    GRBNo: item.GRBNo
                };
                $rootScope.redirectDept(state, params);
            } else {
                API.procedure.post('ProcGetMaterialIssueDetail', {
                        WorkOrderID: WorkOrderID,
                        GRBID: item.GRBNo
                    })
                    .then(function(result) {
                        $scope.MaterialIssueDetail = result[0];
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.selectedLocation = function() {
            $scope.$watch('MaterialInStock', function(MaterialInStock) {
                if (MaterialInStock !== undefined) {
                    var selectedMaterialInstock = _.find(MaterialInStock, { 'selected': true });
                    if (selectedMaterialInstock !== undefined) {
                        var Location = selectedMaterialInstock.Location;
                        if (Location !== null && Location !== undefined) {
                            var LocationY = Location.slice(-1);
                            Location = Location.slice(0, -1);
                            var LocationX = Location.slice(-1);
                            Location = Location.slice(0, -1);
                            Location = Location.trim();

                            if ($scope.LocationInfo === undefined) {
                                $scope.LocationInfo = {};
                            }
                            $scope.$watch('StoreAreaList', function(StoreAreaList) {
                                if (StoreAreaList !== undefined) {
                                    $scope.LocationInfo.Location = _.find(StoreAreaList, { 'StoreArea': Location });
                                    var StoreAreaID = $scope.LocationInfo.Location.StoreAreaID;
                                    $scope.getXLocationProc(StoreAreaID);
                                    $scope.getYLocationProc(StoreAreaID);
                                }
                            });
                            $scope.$watch('XStoreLocations', function(XStoreLocations) {
                                $scope.LocationInfo.XLocation = _.find(XStoreLocations, { 'XLocation': parseInt(LocationX) });
                            });
                            $scope.$watch('YStoreLocations', function(YStoreLocations) {
                                $scope.LocationInfo.YLocation = _.find(YStoreLocations, { 'YLocation': LocationY });
                            });
                        }
                    }
                }
            });
        };

        if ($stateParams.GRBNo !== undefined) {
            $scope.getMaterialIssues($stateParams.GRBNo);
            getMaterialIssueDetail($stateParams.GRBNo);
            $scope.selectedLocation();
        }

        function getMaterialIssueDetail(GRBNo) {
            API.procedure.post('ProcGetMaterialIssueDetail', {
                    WorkOrderID: WorkOrderID,
                    GRBID: GRBNo
                })
                .then(function(result) {
                    $scope.MaterialIssueDetail = result[0];
                })
                .catch(function(error) {
                    throw error;
                });
        }


        /**
         * get Location drop-down
         */
        API.procedure.get('ProcGetStoreAreaList')
            .then(function(result) {
                $scope.StoreAreaList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * the function working when user select Location drop-down
         */
        $scope.changeLocation = function(Location) {

            /**
             * populate X - Y Location
             */
            $scope.getXLocationProc(Location.StoreAreaID);
            $scope.getYLocationProc(Location.StoreAreaID);
        };

        $scope.getXLocationProc = function(StoreAreaID) {
            API.procedure.post('ProcGetXStoreLocations', {
                    StoreAreaID: StoreAreaID
                })
                .then(function(result) {
                    $scope.XStoreLocations = result;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.getYLocationProc = function(StoreAreaID) {
            API.procedure.post('ProcGetYStoreLocations', {
                    StoreAreaID: StoreAreaID
                })
                .then(function(result) {
                    $scope.YStoreLocations = result;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * the function working when click Update Loc'n button
         */
        $scope.updateLocation = function(LocationInfo) {
            $scope.selectedMaterialInstock = _.find($scope.MaterialInStock, { 'selected': true });
            if ($stateParams.GRBNo) {

                API.procedure.post('ProcUpdateStockLocation', {
                        GRBNo: $stateParams.GRBNo,
                        StoreAreaID: LocationInfo.Location.StoreAreaID,
                        Xlocation: LocationInfo.XLocation.XLocation,
                        Ylocation: LocationInfo.YLocation.YLocation,
                    })
                    .then(function(result) {
                        $state.reload();
                        Notification.show('success', 'Update Location success');
                    })
                    .catch(function(error) {
                        throw error;
                        // Notification.show('warning', 'Location updated');
                    });

            } else {
                Notification.show('warning', 'Please select Material in stock');
                return false;
            }
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };

        if (WorkOrderID !== undefined && WorkOrderID !== '') {
            $scope.BeginMaterialIssue = function() {
                API.procedure.post('ProcBeginMaterialIssue', {
                        UserID: $rootScope.user.getId(),
                        WorkOrderID: WorkOrderID
                    })
                    .then(function(result) {
                        Notification.show('success', 'Begin Material Issue success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.PauseMaterialIssue = function() {
                API.procedure.post('ProcPauseMaterialIssue', {
                        UserID: $rootScope.user.getId(),
                        WorkOrderID: WorkOrderID
                    })
                    .then(function(result) {
                        Notification.show('success', 'Pause Material Issue success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.EndMaterialIssue = function() {
                API.procedure.post('ProcEndMaterialIssue', {
                        UserID: $rootScope.user.getId(),
                        WorkOrderID: WorkOrderID
                    })
                    .then(function(result) {
                        var params = {
                            GRBID: GRBNo
                        };
                        $rootScope.openTabBirt('JIL', params);
                        Notification.show('success', 'End Material Issue success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };
        }
    }

})(angular.module('app.components.department.material-issue', [
    'angular.filter'
]));
