var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require("gulp-concat");
 
module.exports =  function () {
	gulp.src([
		'./frontend/bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
		'./frontend/bower_components/bootstrap/dist/css/bootstrap.min.css',
		'./frontend/bower_components/angular-notify/angular-notify.css',
		'./frontend/css/**/*.css'
		])
		.pipe(cssmin())
		.pipe(rename({suffix: '.min'}))
		.pipe(concat("app.min.css"))
		.pipe(gulp.dest('public/assets/css/'));
};