require('../globalResource');
(function(app) {
    app.service('GlobalService', GlobalService);
    GlobalService.$inject = ['GlobalResource', '$q'];

    function GlobalService(GlobalResource, $q) {
        this.get = function(url) {
            var defferred = $q.defer();
            GlobalResource.get({
                url: url
            }, function(response) {
                defferred.resolve(response.data);
            }, function(error) {
                defferred.reject(error);
            });
            return defferred.promise;
        };

        this.post = function(url, params) {
            var defferred = $q.defer();
            params = params || {};
            GlobalResource.post({
                url: url
            }, params, function(response) {
                defferred.resolve(response.data);
            }, function(error) {
                defferred.reject(error);
            });
            return defferred.promise;
        };
    }
})(angular.module('app.api.global', [
    'app.api.globalResource'
]));
