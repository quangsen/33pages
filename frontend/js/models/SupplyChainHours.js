var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function SupplyChainHours(options) {
    this.Day1Close = null;
    this.Day1Open = null;
    this.Day2Close = null;
    this.Day2Open = null;
    this.Day3Close = null;
    this.Day3Open = null;
    this.Day4Close = null;
    this.Day4Open = null;
    this.Day5Close = null;
    this.Day5Open = null;
    this.Day6Close = null;
    this.Day6Open = null;
    this.Day7Close = null;
    this.Day7Open = null;

    BaseModel.call(this, options);
}

inherits(SupplyChainHours, BaseModel);

module.exports = SupplyChainHours;