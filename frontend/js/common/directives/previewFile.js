(function(app) {

    app.directive('previewFile', function() {
        return {
            restrict: 'AE',
            scope: {
                fileName: '='
            },
            link: function(scope, element, attrs) {
                scope.$watch('fileName', function(fileName) {
                    if (fileName !== undefined) {
                        var embed = '<embed id="embed" class="full-width" src="' + fileName + '">';
                        $(element).html(embed);

                        /**
                         * CSS for image inside embed tab
                         */
                        var innerEmbed = $('#embed').contentDocument;
                        var imageEmbed = innerEmbed.$('image');
                        imageEmbed.css({
                            "width": "100%",
                            "max-width": "100%",
                            "height": "100%",
                            "max-height": "100%"
                        });
                    }
                });
            }
        };
    });

})(angular.module('app.common.directives.previewFile', []));
