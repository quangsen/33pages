(function(app) {

    app.controller('PartListCtrl', PartListCtrl);
    PartListCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function PartListCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {
        var PTemplateID = $stateParams.PTemplateID;

        // Show/hide dialog dropzone
        $scope.showDialog = false;

        // Save value when select search input
        if (PTemplateID !== undefined) {
            API.procedure.post('ProcPartTemplateSearch', {
                    PartNumberSearchString: "",
                    Filter: 2
                })
                .then(function(result) {
                    var selectedPart = _.findLast(result, function(item) {
                        return parseInt(item.PTemplateID) === parseInt(PTemplateID);
                    });
                    $scope.searchInputModel = selectedPart.PartNumber + ' // ' + selectedPart.Issue + ' // ' + selectedPart.COS;
                    $scope.getInfoPart(selectedPart.ETemplateID);
                    $scope.selectedPart = selectedPart;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        $scope.checkSelectedPart = function() {
            if ($scope.PartItem === undefined) {
                Notification.show('warning', 'Please select Part No.');
                return false;
            } else {
                return true;
            }
        };

        $scope.redirectEngineering = function(ETemplateID) {
            var state = 'department.engineering-template.material';
            var params = {
                EngineeringTemplateID: ETemplateID,
                WorkOrderID: undefined
            };
            $rootScope.redirectDept(state, params);
        };

        $scope.ViewTemplate = function() {
            if ($scope.selectedPart === undefined) {
                var check = $scope.checkSelectedPart();
                if (check === true) {
                    if ($scope.selectedPart.ETemplateID !== null && $scope.selectedPart.ETemplateID !== '') {
                        $scope.redirectEngineering($scope.selectedPart.ETemplateID);
                    }
                }
            } else {
                if ($scope.selectedPart.ETemplateID !== null && $scope.selectedPart.ETemplateID !== '') {
                    $scope.redirectEngineering($scope.selectedPart.ETemplateID);
                }
            }
        };

        $scope.addNewEngTemplate = function(PTemplateID, SupplyConditionID) {
            API.procedure.post('ProcAddNewEngTemplate', {
                    UserID: $rootScope.user.getId(),
                    PTemplateID: PTemplateID,
                    SupplyConditionID: SupplyConditionID
                })
                .then(function(result) {
                    Notification.show('success', 'Create Template success');
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.CreateTemplate = function(Supply) {
            if (Supply === undefined) {
                Notification.show('warning', 'Please select COS');
                return false;
            }
            if ($scope.selectedPart === undefined) {
                var check = $scope.checkSelectedPart();
                if (check === true) {
                    $scope.addNewEngTemplate($scope.PartItem.PTemplateID, Supply.SupplyConditionID);
                }
            } else {
                $scope.addNewEngTemplate($scope.selectedPart.PTemplateID, Supply.SupplyConditionID);
            }
        };

        $scope.PhotocopyTemplate = function(PTemplateID, ETemplateID) {
            API.procedure.post('ProcPhotocopyTemplate', {
                    UserID: $rootScope.user.getId(),
                    PTemplateID: PTemplateID,
                    OrigETemplateID: ETemplateID
                })
                .then(function(result) {
                    Notification.show('success', 'Copy Template success');
                })
                .catch(function(error) {
                    Notification.show('warning', 'Originating ETemplateID is null');
                });
        };

        $scope.CopyTemplate = function(OriginPartNo) {
            if (OriginPartNo === undefined) {
                Notification.show('warning', 'Please select Originating Part No.');
                return false;
            }
            if ($scope.selectedPart === undefined) {
                var check = $scope.checkSelectedPart();
                if (check === true) {
                    $scope.PhotocopyTemplate($scope.PartItem.PTemplateID, OriginPartNo.ETemplateID);
                }
            } else {
                $scope.PhotocopyTemplate($scope.selectedPart.PTemplateID, OriginPartNo.ETemplateID);
            }
        };

        $scope.PartTemplateAlert = function(PTemplateID) {
            API.procedure.post('ProcPartTemplateAlert', {
                    UserID: $rootScope.user.getId(),
                    PTemplateID: PTemplateID
                })
                .then(function(result) {
                    Notification.show('success', 'Alert Template success');
                    $scope.isAlert = false;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.TemplateAlert = function() {
            if ($scope.selectedPart === undefined) {
                var check = $scope.checkSelectedPart();
                if (check === true) {
                    $scope.PartTemplateAlert($scope.PartItem.PTemplateID);
                }
            } else {
                $scope.PartTemplateAlert($scope.selectedPart.PTemplateID);
            }
        };

        $scope.PartTemplateAlertOff = function(PTemplateID) {
            API.procedure.post('ProcPartTemplateAlertOff', {
                    UserID: $rootScope.user.getId(),
                    PTemplateID: PTemplateID
                })
                .then(function(result) {
                    Notification.show('success', 'Remove Alert Template success');
                    $scope.isAlert = true;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.TemplateAlertOff = function() {
            if ($scope.selectedPart === undefined) {
                var check = $scope.checkSelectedPart();
                if (check === true) {
                    $scope.PartTemplateAlertOff($scope.PartItem.PTemplateID);
                }
            } else {
                $scope.PartTemplateAlertOff($scope.selectedPart.PTemplateID);
            }
        };

        $scope.PartTemplateLock = function(PTemplateID) {
            API.procedure.post('ProcPartTemplateLock', {
                    UserID: $rootScope.user.getId(),
                    PTemplateID: PTemplateID
                })
                .then(function(result) {
                    Notification.show('success', 'Lock down Template success');
                    $scope.isLocked = false;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.TemplateLock = function() {
            if ($scope.selectedPart === undefined) {
                var check = $scope.checkSelectedPart();
                if (check === true) {
                    $scope.PartTemplateLock($scope.PartItem.PTemplateID);
                }
            } else {
                $scope.PartTemplateLock($scope.selectedPart.PTemplateID);
            }
        };

        $scope.PartTemplateLockOff = function(PTemplateID) {
            API.procedure.post('ProcPartTemplateLockOff', {
                    UserID: $rootScope.user.getId(),
                    PTemplateID: PTemplateID
                })
                .then(function(result) {
                    Notification.show('success', 'Remove Lock Template success');
                    $scope.isLocked = true;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.TemplateLockOff = function() {
            if ($scope.selectedPart === undefined) {
                var check = $scope.checkSelectedPart();
                if (check === true) {
                    $scope.PartTemplateLockOff($scope.PartItem.PTemplateID);
                }
            } else {
                $scope.PartTemplateLockOff($scope.selectedPart.PTemplateID);
            }
        };

        $scope.execredirectPartHistory = function() {

        };

        $scope.redirectPartHistory = function() {
            if ($scope.selectedPart === undefined) {
                Notification.show('warning', 'Please select Part No.');
                return false;
            }

            $state.go('department.part-history', {
                PTemplateID: $scope.selectedPart.PTemplateID,
                PartNumber: $scope.selectedPart.PartNumber,
                COS: $scope.selectedPart.COS,
                Issue: $scope.selectedPart.Issue
            });
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };

        $scope.UpIssue = function(NewIssue) {
            if (NewIssue === undefined | NewIssue === '') {
                Notification.show('warning', 'Please enter New issue');
                return false;
            }
            if ($scope.selectedPart === undefined) {
                var check = $scope.checkSelectedPart();
                if (check === true) {
                    $scope.NewIssue = NewIssue;
                    $scope.showDialog = true;
                }
            } else {
                $scope.NewIssue = NewIssue;
                $scope.showDialog = true;
            }
        };

        $scope.beforeUpload = function(file) {};

        $scope.errorUpload = function(error) {
            if (error.error_code === 1) {
                Notification.show('warning', 'File extension not allow');
                $scope.showDialog = false;
                $window.location.reload();
            }
        };

        $scope.UpIssuePartTemplate = function(PTemplateID, FileHandle) {
            API.procedure.post('ProcUpIssuePartTemplate', {
                    UserID: $rootScope.user.getId(),
                    PTemplateID: PTemplateID,
                    NewIssue: $scope.NewIssue,
                    FileHandle: FileHandle
                })
                .then(function(result) {
                    if (parseInt(result.Result) === -1) {
                        Notification.show('warning', 'Has error in upload processing');
                    } else {
                        Notification.show('success', 'Up issue success');
                    }
                    $scope.showDialog = false;
                    // $window.location.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.uploadSuccess = function(response) {
            if ($scope.selectedPart === undefined) {
                $scope.UpIssuePartTemplate($scope.PartItem.PTemplateID, response.data);
            } else {
                $scope.UpIssuePartTemplate($scope.selectedPart.PTemplateID, response.data);
            }
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };
    }

})(angular.module('app.components.department.part-control.part-list', []));
