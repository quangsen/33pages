(function(app) {
    app.controller('WorkOrderDocumentCtrl', WorkOrderDocumentCtrl);
    WorkOrderDocumentCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API', '$state', 'Notification', '$window'];

    function WorkOrderDocumentCtrl($rootScope, $scope, $stateParams, API, $state, Notification, $window) {
        var WorkOrderID = $rootScope.WorkOrderID = $stateParams.WorkOrderID;
        if (WorkOrderID !== undefined && WorkOrderID !== '') {
            if ($scope.WorkOrderDetail === undefined) {
                $scope.getWorkOrderInfo(WorkOrderID);
            }

            /**
             * get Work Order Documents
             */
            $scope.getWODocuments = function() {
                API.procedure.post('ProcGetWODocuments', {
                        WorkOrderID: WorkOrderID
                    })
                    .then(function(result) {
                        $scope.WODocuments = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };
            $scope.getWODocuments();

            /**
             * get Document drop-down
             */
            API.procedure.get('ProcGetDocumentDropTypes')
                .then(function(result) {
                    $scope.DocumentDropTypes = result;
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.beforeUpload = function(file) {
                if ($scope.DocumentDropType === undefined) {
                    Notification.show('warning', 'Please select Document Type');
                    return false;
                }
            };

            $scope.uploadSuccess = function(response) {
                API.procedure.post('ProcAddWODocument', {
                        UserID: $rootScope.user.getId(),
                        WorkOrderID: WorkOrderID,
                        DropTypeID: $scope.DocumentDropType.pkDocumentDropTypeID,
                        FileName: response.data
                    })
                    .then(function(result) {
                        Notification.show('success', 'Drop Document success');
                        $scope.getWODocuments();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.printImage = function() {
                var selectedWODocuments = _.find($scope.WODocuments, { 'selected': true });
                if (selectedWODocuments === undefined) {
                    Notification.show('warning', 'Please select Document item');
                    return false;
                }
                var fileName = $rootScope.PathImageUpload + selectedWODocuments.FileName;
                $window.open(fileName, 'blank_');
            };

        } else {
            var state = 'department.workorders';
            $rootScope.redirectDept(state);
            return false;
        }

    }
})(angular.module('app.components.department.workorders.document', []));
