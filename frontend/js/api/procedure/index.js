require('../procedureResource');
(function(app) {
    app.service('ProcedureService', ProcedureService);
    ProcedureService.$inject = ['ProcedureResource', '$q'];

    function ProcedureService(ProcedureResource, $q) {
        this.get = function(procedure) {
            var defferred = $q.defer();
            ProcedureResource.get({
                name: procedure
            }, function(response) {
                defferred.resolve(response.data);
            }, function(error) {
                defferred.reject(error);
            });
            return defferred.promise;
        };

        this.post = function(procedure, params) {
            var defferred = $q.defer();
            params = params || {};
            ProcedureResource.post({
                name: procedure
            }, params, function(response) {
                defferred.resolve(response.data);
            }, function(error) {
                defferred.reject(error);
            });
            return defferred.promise;
        };
    }
})(angular.module('app.api.procedure', [
    'app.api.procedureResource'
]));
