(function(app) {

    app.controller('EngineeringOverviewCtrl', EngineeringOverviewCtrl);
    EngineeringOverviewCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API'];

    function EngineeringOverviewCtrl($rootScope, $scope, $stateParams, API) {
        var EngineeringTemplateID = $stateParams.EngineeringTemplateID;
        var WorkOrderID = $stateParams.WorkOrderID;
        if (EngineeringTemplateID !== undefined && EngineeringTemplateID !== '') {
            API.birt.post('RouteCard_2016Template', {
                    ETemplateID: EngineeringTemplateID
                })
                .then(function(result) {
                    $scope.birtRouteCard = result;
                })
                .catch(function(error) {
                    throw error;
                });
        } else {
            API.birt.post('RouteCard_2016', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.birtRouteCard = result;
                })
                .catch(function(error) {
                    throw error;
                });
        }
    }

})(angular.module('app.components.department.engineering-template.overview', []));
