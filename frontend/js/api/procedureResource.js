(function(app) {
    app.factory('ProcedureResource', ProcedureResource);
    ProcedureResource.$inject = ['$q', '$resource', 'ApiUrl'];

    function ProcedureResource($q, $resource, ApiUrl) {
        var url = ApiUrl.get('/api/v1/procedure/:name');

        var resource = $resource(url, {}, {
            get: {
                method: 'GET'
            },
            post: {
                method: 'POST'
            }
        });

        return resource;
    }
})(angular.module('app.api.procedureResource', []));
