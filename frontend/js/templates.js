(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/partials/angular-notify.tpl.html',
    '<div class="cg-notify-message" ng-class="[$classes, \n' +
    '    $position === \'center\' ? \'cg-notify-message-center\' : \'\',\n' +
    '    $position === \'left\' ? \'cg-notify-message-left\' : \'\',\n' +
    '    $position === \'right\' ? \'cg-notify-message-right\' : \'\']"\n' +
    '    ng-style="{\'margin-left\': $centerMargin}">\n' +
    '\n' +
    '    <div ng-show="!$messageTemplate">\n' +
    '        {{$message}}\n' +
    '    </div>\n' +
    '\n' +
    '    <div ng-show="$messageTemplate" class="cg-notify-message-template">\n' +
    '        \n' +
    '    </div>\n' +
    '\n' +
    '    <button type="button" class="cg-notify-close" ng-click="$close()">\n' +
    '        <span aria-hidden="true">&times;</span>\n' +
    '        <span class="cg-notify-sr-only">Close</span>\n' +
    '    </button>\n' +
    '\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/partials/customer.tpl.html',
    '<div class="customer-page">\n' +
    '    <div class="page-body">\n' +
    '        <div class="title-global-page">Customer Center</div>\n' +
    '        <form ng-submit="CustomerForm.$valid && saveCustomer(CustomerInfo, PaymentTerm, PaymentRunFrequency, fkApprovalID)" name="CustomerForm" novalidate>\n' +
    '            <div class="row row-1">\n' +
    '                <div class="area area-1 col-xs-3">\n' +
    '                    <div class="module">\n' +
    '                        <div class="module-title">\n' +
    '                            <label>Please select a Customer:</label>\n' +
    '                            <typeahead source="CustomerList" key="Alias" select-callback="UserSelected"></typeahead>\n' +
    '                        </div>\n' +
    '                        <div class="module-body">\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-6 control-label">\n' +
    '                                    Finance Code\n' +
    '                                </label>\n' +
    '                                <div class="col-xs-6">\n' +
    '                                    <input type="text" ng-model="CustomerInfo.FinanceCode" name="finance" class="input-finance" required maxlength="3" ng-disabled="!CustomerInfo">\n' +
    '                                </div>\n' +
    '                                <div class="error col-xs-12">\n' +
    '                                    <span ng-show="CustomerForm.$submitted && CustomerForm.finance.$error.required">Please insert finance code</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-6 control-label">Credit limit</label>\n' +
    '                                <div class="col-xs-6">\n' +
    '                                    <span class="currency-symbol currency-symbol-irish left">\n' +
    '                                        <input type="text" currency-input ng-model="CustomerInfo.CreditLimit" class="input-credit" maxlength="7" required name="CreditLimit" ng-disabled="!CustomerInfo">\n' +
    '                                    </span>\n' +
    '                                </div>\n' +
    '                                <div class="error col-xs-12">\n' +
    '                                    <span ng-show="CustomerForm.$submitted && CustomerForm.CreditLimit.$error.required">Please insert credit limit</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-6 control-label no-padding-right">Quote markup %</label>\n' +
    '                                <div class="col-xs-6">\n' +
    '                                    <span class="percent-symbol right">\n' +
    '                                    <input type="text" ng-model="CustomerInfo.MarkUpQuote" class="input-quote" name="MarkUpQuote" required ng-pattern="/^\\d+$/" ng-disabled="!CustomerInfo" maxlength="3">\n' +
    '                                </span>\n' +
    '                                </div>\n' +
    '                                <div class="error col-xs-12">\n' +
    '                                    <span ng-show="CustomerForm.$submitted && CustomerForm.MarkUpQuote.$error.required">Please insert quote markup</span>\n' +
    '                                    <span ng-show="CustomerForm.$submitted && CustomerForm.MarkUpQuote.$error.pattern">Please insert number</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-6 control-label">Payment terms</label>\n' +
    '                                <div class="col-xs-6">\n' +
    '                                    <select required ng-model="PaymentTerm" name="PaymentTerm" ng-disabled="!CustomerInfo" ng-options="PaymentTerm as PaymentTerm.name for PaymentTerm in ListPaymentTerms" ng-change="CustomerInfo.setPaymentTerms(PaymentTerm.value)"></select>\n' +
    '                                </div>\n' +
    '                                <div class="error col-xs-12">\n' +
    '                                    <span ng-show="CustomerForm.$submitted && CustomerForm.PaymentTerm.$error.required">Please select payment terms</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-6 control-label">Payment run frequency</label>\n' +
    '                                <div class="col-xs-6">\n' +
    '                                    <select ng-model="PaymentRunFrequency" name="PaymentRunFrequency" ng-disabled="!CustomerInfo" ng-options="PaymentRunFrequency as PaymentRunFrequency.name for PaymentRunFrequency in ListPaymentRunFrequency" ng-model="PaymentRunFrequency" ng-change="CustomerInfo.setPaymentRunFrequency(PaymentRunFrequency.value)" required></select>\n' +
    '                                </div>\n' +
    '                                <div class="error col-xs-12">\n' +
    '                                    <span ng-show="CustomerForm.$submitted && CustomerForm.PaymentRunFrequency.$error.required">Please select payment run frequency</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="area area-2 col-xs-5">\n' +
    '                    <div class="form-group">\n' +
    '                        <label for="" class="col-xs-4 control-label">Account No.</label>\n' +
    '                        <div class="col-xs-7">\n' +
    '                            <label for="" class="control-label" ng-bind="CustomerInfo.pkCustomerID"></label>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group">\n' +
    '                        <label for="" class="col-xs-4 control-label">Name</label>\n' +
    '                        <div class="col-xs-8">\n' +
    '                            <input type="text" ng-model="CustomerInfo.CustomerName" name="CustomerName" class="input-quote" maxlength="50" required ng-disabled="!CustomerInfo">\n' +
    '                        </div>\n' +
    '                        <div class="error col-xs-12">\n' +
    '                            <span ng-show="CustomerForm.$submitted && CustomerForm.CustomerName.$error.required">Please insert customer name</span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group">\n' +
    '                        <label for="" class="col-xs-4 control-label">Alias</label>\n' +
    '                        <div class="col-xs-8">\n' +
    '                            <input type="text" ng-model="CustomerInfo.Alias" class="input-quote" name="Alias" maxlength="30" required ng-disabled="!CustomerInfo">\n' +
    '                        </div>\n' +
    '                        <div class="error col-xs-12">\n' +
    '                            <span ng-show="CustomerForm.$submitted && CustomerForm.Alias.$error.required">Please insert customer alias</span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group margin-top-30">\n' +
    '                        <label for="" class="col-xs-4 control-label">Address line 1</label>\n' +
    '                        <div class="col-xs-8">\n' +
    '                            <input type="text" ng-model="CustomerInfo.Address1" class="input-quote" name="Address1" required maxlength="45" ng-disabled="!CustomerInfo">\n' +
    '                        </div>\n' +
    '                        <div class="error col-xs-12">\n' +
    '                            <span ng-show="CustomerForm.$submitted && CustomerForm.Address1.$error.required">Please insert address line 1</span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group">\n' +
    '                        <label for="" class="col-xs-4 control-label">Address line 2</label>\n' +
    '                        <div class="col-xs-8">\n' +
    '                            <input type="text" ng-model="CustomerInfo.Address2" class="input-quote" name="Address2" maxlength="40" ng-disabled="!CustomerInfo">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group">\n' +
    '                        <label for="" class="col-xs-4 control-label">Address line 3</label>\n' +
    '                        <div class="col-xs-8">\n' +
    '                            <input type="text" ng-model="CustomerInfo.Address3" class="input-quote" name="Address3" maxlength="40" ng-disabled="!CustomerInfo">\n' +
    '                            <div class="error">\n' +
    '                                <span ng-show="CustomerForm.$submitted && CustomerForm.Address3.$error.required">Please insert address line 3</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group">\n' +
    '                        <label for="" class="col-xs-4 control-label">Address line 4</label>\n' +
    '                        <div class="col-xs-6">\n' +
    '                            <input type="text" ng-model="CustomerInfo.Address4" class="input-quote" name="Address4" maxlength="30" ng-disabled="!CustomerInfo">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group">\n' +
    '                        <label for="" class="col-xs-4 control-label">Postcode</label>\n' +
    '                        <div class="col-xs-3">\n' +
    '                            <input type="text" ng-model="CustomerInfo.Postcode" class="input-quote" name="Postcode" required maxlength="8" ng-disabled="!CustomerInfo">\n' +
    '                        </div>\n' +
    '                        <div class="error col-xs-12">\n' +
    '                            <span ng-show="CustomerForm.$submitted && CustomerForm.Postcode.$error.required">Please insert postcode</span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group">\n' +
    '                        <div class="col-xs-offset-4 col-xs-7">\n' +
    '                            <button type="button" class="copy-address float-right" ng-click="copyAddress()">Copy Address</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="area area-3 col-xs-4">\n' +
    '                    <div class="module">\n' +
    '                        <div class="module-title">Invoice address</div>\n' +
    '                        <div class="module-body">\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-2 control-label no-padding-left">Line 1</label>\n' +
    '                                <div class="col-xs-10 no-padding">\n' +
    '                                    <input type="text" ng-model="CustomerInfo.InvAddress1" class="input-quote" name="InvAddress1" required maxlength="45" ng-disabled="!CustomerInfo">\n' +
    '                                </div>\n' +
    '                                <div class="error">\n' +
    '                                    <span ng-show="CustomerForm.$submitted && CustomerForm.InvAddress1.$error.required">Please insert invoice address line 1</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-2 control-label no-padding-left">Line 2</label>\n' +
    '                                <div class="col-xs-10 no-padding">\n' +
    '                                    <input type="text" ng-model="CustomerInfo.InvAddress2" class="input-quote" name="InvAddress2" maxlength="40" ng-disabled="!CustomerInfo">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-2 control-label no-padding-left">Line 3</label>\n' +
    '                                <div class="col-xs-10 no-padding">\n' +
    '                                    <input type="text" ng-model="CustomerInfo.InvAddress3" class="input-quote" name="InvAddress3" maxlength="40" ng-disabled="!CustomerInfo">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-2 control-label no-padding-left">Line 4</label>\n' +
    '                                <div class="col-xs-8 no-padding">\n' +
    '                                    <input type="text" ng-model="CustomerInfo.InvAddress4" class="input-quote" name="InvAddress4" maxlength="30" ng-disabled="!CustomerInfo">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-2 control-label no-padding-left">Pcode</label>\n' +
    '                                <div class="col-xs-3 no-padding">\n' +
    '                                    <input type="text" ng-model="CustomerInfo.InvPostcode" class="input-quote" name="InvPostcode" maxlength="8" required ng-disabled="!CustomerInfo">\n' +
    '                                </div>\n' +
    '                                <div class="error">\n' +
    '                                    <span ng-show="CustomerForm.$submitted && CustomerForm.InvPostcode.$error.required">Please insert invoice postcode</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="form-group my-form-group">\n' +
    '                                <label for="" class="col-xs-6 col-xs-offset-2 control-label">Default approval level</label>\n' +
    '                                <div class="col-xs-3">\n' +
    '                                    <select name="Approval" ng-options="option as option.Description for option in AllApprovalList" ng-model="fkApprovalID" required ng-disabled="!CustomerInfo" ng-change="CustomerInfo.setApprovalLevel(fkApprovalID.pkApprovalID)">\n' +
    '                                    </select>\n' +
    '                                </div>\n' +
    '                                <div class="error col-xs-12 col-xs-offset-2">\n' +
    '                                    <span ng-show="CustomerForm.$submitted && CustomerForm.Approval.$error.required">Please select default approval level</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row row-2">\n' +
    '                <div class="area area-1 col-xs-3">\n' +
    '                    <div class="module">\n' +
    '                        <div class="module-title">\n' +
    '                            <span>VAT rate</span>\n' +
    '                            <input type="text" ng-model="CustomerInfo.VATRate" valid-number ng-disabled="!CustomerInfo"> %\n' +
    '                        </div>\n' +
    '                        <div class="module-body">\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-6 control-label"></label>\n' +
    '                                <div class="col-xs-6">\n' +
    '                                    <div class="col-xs-6">\n' +
    '                                        <label for="">Yes</label>\n' +
    '                                    </div>\n' +
    '                                    <div class="col-xs-6">\n' +
    '                                        <label for="">No</label>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-6 control-label">\n' +
    '                                    MD approval?\n' +
    '                                </label>\n' +
    '                                <div class="col-xs-6">\n' +
    '                                    <div class="col-xs-6">\n' +
    '                                        <input type="radio" ng-model="CustomerInfo.MDApproval" ng-value="1" name="MDApproval" ng-disabled="!CustomerInfo">\n' +
    '                                    </div>\n' +
    '                                    <div class="col-xs-6">\n' +
    '                                        <input type="radio" ng-model="CustomerInfo.MDApproval" ng-value="0" name="MDApproval" ng-disabled="!CustomerInfo">\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="clearfix"></div>\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-6 control-label">\n' +
    '                                    FD approval?\n' +
    '                                </label>\n' +
    '                                <div class="col-xs-6">\n' +
    '                                    <div class="col-xs-6">\n' +
    '                                        <input type="radio" ng-model="CustomerInfo.FDApproval" ng-value="1" name="FDApproval" ng-disabled="!CustomerInfo">\n' +
    '                                    </div>\n' +
    '                                    <div class="col-xs-6">\n' +
    '                                        <input type="radio" ng-model="CustomerInfo.FDApproval" ng-value="0" name="FDApproval" ng-disabled="!CustomerInfo">\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="clearfix"></div>\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-6 control-label">\n' +
    '                                    GM approval?\n' +
    '                                </label>\n' +
    '                                <div class="col-xs-6">\n' +
    '                                    <div class="col-xs-6">\n' +
    '                                        <input type="radio" ng-model="CustomerInfo.GMApproval" ng-value="1" name="GMApproval" ng-disabled="!CustomerInfo">\n' +
    '                                    </div>\n' +
    '                                    <div class="col-xs-6">\n' +
    '                                        <input type="radio" ng-model="CustomerInfo.GMApproval" ng-value="0" name="GMApproval" ng-disabled="!CustomerInfo">\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="area area-2 col-xs-5">\n' +
    '                    <div class="module">\n' +
    '                        <div class="module-title"></div>\n' +
    '                        <div class="module-body">\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-2 no-padding control-label">Website</label>\n' +
    '                                <div class="col-xs-10">\n' +
    '                                    <input type="text" ng-model="CustomerInfo.Website" ng-disabled="!CustomerInfo" class="input-quote" name="Website" maxlength="40" ng-pattern="/^www\\.[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$/">\n' +
    '                                    <div class="error">\n' +
    '                                        <span ng-show="CustomerForm.$submitted && CustomerForm.Website.$error.pattern">Website incorrect formats</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-2 no-padding control-label">Email</label>\n' +
    '                                <div class="col-xs-10">\n' +
    '                                    <input type="email" ng-model="CustomerInfo.Email" name="Email" class="input-quote" maxlength="50" ng-disabled="!CustomerInfo">\n' +
    '                                    <div class="error">\n' +
    '                                        <span ng-show="CustomerForm.$submitted && CustomerForm.Email.$error.email">Please enter a valid email</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="area area-3 col-xs-4">\n' +
    '                    <div class="form-group default-delivery">\n' +
    '                        <label for="" class="col-xs-6 control-label">Default Delivery charge</label>\n' +
    '                        <div class="col-xs-3">\n' +
    '                            <span class="currency-symbol currency-symbol-irish left">\n' +
    '                            <input type="text" ng-model="CustomerInfo.DeliveryCharge" name="DeliveryCharge" required ng-pattern="/^0|[1-9][0-9]*$/" ng-disabled="!CustomerInfo" maxlength="3" />\n' +
    '                        </span>\n' +
    '                        </div>\n' +
    '                        <div class="error col-xs-12">\n' +
    '                            <span ng-show="CustomerForm.$submitted && CustomerForm.DeliveryCharge.$error.required">Please insert default delivery charge</span>\n' +
    '                            <span ng-show="CustomerForm.$submitted && CustomerForm.DeliveryCharge.$error.pattern">Please insert number</span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group my-form-group send-goods">\n' +
    '                        <label for="" class="col-xs-12 control-label">Send goods up to\n' +
    '                            <input type="text" required ng-model="CustomerInfo.DeliveryGrace" name="DeliveryGrace" ng-pattern="/^0|[1-9][0-9]*$/" ng-disabled="!CustomerInfo" maxlength="4"> Days before due date\n' +
    '                        </label>\n' +
    '                        <div class="error col-xs-12">\n' +
    '                            <span ng-show="CustomerForm.$submitted && CustomerForm.DeliveryGrace.$error.required">Please enter send goods up</span>\n' +
    '                            <span ng-show="CustomerForm.$submitted && CustomerForm.DeliveryGrace.$error.pattern">Please insert number</span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row row-3">\n' +
    '                <div class="area area-1 col-xs-5">\n' +
    '                    <div class="module">\n' +
    '                        <div class="module-body">\n' +
    '                            <span>Multi currency: </span>\n' +
    '                            <label for="">Irish &#8356;</label>\n' +
    '                            <input type="checkbox" ng-model="CustomerInfo.IrishCurrency" ng-disabled="!CustomerInfo" ng-true-value="1" ng-false-value="0">\n' +
    '                            <label for="">US $</label>\n' +
    '                            <input type="checkbox" ng-model="CustomerInfo.USCurrency" ng-true-value="1" ng-false-value="0" ng-disabled="!CustomerInfo">\n' +
    '                            <label for="">Euros &#8364;</label>\n' +
    '                            <input type="checkbox" ng-model="CustomerInfo.EuroCurrency" ng-true-value="1" ng-false-value="0" ng-disabled="!CustomerInfo">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="area area-2 col-xs-7">\n' +
    '                    <div class="area-table table-note">\n' +
    '                        <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="float_standard" load-item="CustomerConformityNotes">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th>Standard Conformity Notes</th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="item in CustomerConformityNotes">\n' +
    '                                    <td class="col-xs-12 no-padding" ng-bind="item.ConformityNote"></td>\n' +
    '                                </tr>\n' +
    '                                <tr>\n' +
    '                                    <td class="col-xs-12 no-padding">\n' +
    '                                        <input type="text" ng-model="Note" maxlength="80">\n' +
    '                                    </td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row row-4">\n' +
    '                <div class="col-xs-12 area">\n' +
    '                    <input type="button" class="button add-customer" ng-click="addCustomer()" value="Add New Customer" />\n' +
    '                    <input type="button" class="button export-customer" value="Export Customers" ng-click="exportCustomer()" />\n' +
    '                    <input type="button" class="button new-co" value="New CO" ng-click="redirectNewCO()" />\n' +
    '                    <input type="button" class="button view-order" value="View Order History" ng-click="redirectOrderHistoryPage()" />\n' +
    '                    <input type="button" class="button contact" value="Contacts" ng-click="redirectCustomerContactPage()" />\n' +
    '                    <input type="button" class="button delivery-point" value="Delivery Points" ng-click="redirectDeliveryPointsPage()" />\n' +
    '                    <input type="button" class="button back" value="Back" ng-click="redirectBack()" />\n' +
    '                    <input type="button" class="button update" ng-click="updateCustomer(CustomerInfo)" ng-show="showUpdateButton" value="Update" />\n' +
    '                    <input type="submit" class="button save" value="Save" ng-show="showSaveCancelButton" />\n' +
    '                    <input type="button" class="button cancel" ng-click="cancelAddCustomer()" value="Cancel" ng-show="showSaveCancelButton" />\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </form>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/partials/left-menu.tpl.html',
    '<ul>\n' +
    '    <li ng-repeat="item in LeftMenu">\n' +
    '        <a ui-sref="{{item.state}}">\n' +
    '            <button ng-bind="item.title"></button>\n' +
    '        </a>\n' +
    '    </li>\n' +
    '</ul>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/partials/searchInput.tpl.html',
    '<div class="search-input">\n' +
    '    <input type="text" ng-model="searchInputModel" typeahead-on-select="onSelect($item, $model, $label, $event)" uib-typeahead="item[display] for item in SearchInput($viewValue)" ng-click="clearSearch(searchInputModel)">\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/partials/staff-room.tpl.html',
    '<div class="staff-room-page">\n' +
    '    <div class="title-global-page">Staff Room</div>\n' +
    '    <div class="page-body">\n' +
    '        <form ng-submit="StaffForm.$valid && SaveOrUpdateStaff(StaffDetail)" name="StaffForm" novalidate>\n' +
    '            <div class="row">\n' +
    '                <div class="staff-info col-xs-12">\n' +
    '                    <div class="row">\n' +
    '                        <div class="staff-credit col-xs-3">\n' +
    '                            <div class="row">\n' +
    '                                <div class="select-staff col-xs-12">\n' +
    '                                    <p>Please select staff member:</p>\n' +
    '                                    <search-input procedure="ProcGetStaffList" search-key="SearchString" ng-model-options="{debounce: 500}" display-key="Name" on-select="StaffSelected"></search-input>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="po-limit margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title content-right col-xs-8">\n' +
    '                                            Individual PO limit\n' +
    '                                        </div>\n' +
    '                                        <div class="col-xs-4 content">\n' +
    '                                            <input type="text" name="POLimit" ng-model="StaffDetail.POLimit" required ng-pattern="/^\\d+$/">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.POLimit.$error.required">\n' +
    '                                                Please enter PO limit\n' +
    '                                            </span>\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.POLimit.$error.pattern">\n' +
    '                                                PO limit must be number\n' +
    '                                            </span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="po-limit col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title content-right col-xs-8">\n' +
    '                                            Mthly credit / spend limit\n' +
    '                                        </div>\n' +
    '                                        <div class="col-xs-4 content">\n' +
    '                                            <input type="text" name="SpendLimit" ng-model="StaffDetail.SpendLimit" required ng-pattern="/^\\d+$/">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="error col-xs-12">\n' +
    '                                    <span ng-show="StaffForm.$submitted && StaffForm.SpendLimit.$error.required">Please enter spend limit</span>\n' +
    '                                    <span ng-show="StaffForm.$submitted && StaffForm.SpendLimit.$error.pattern">spend limit must be number</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="fork-first col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="fork-lift col-xs-12">\n' +
    '                                            <div class="row">\n' +
    '                                                <div class="title col-xs-8">&nbsp;</div>\n' +
    '                                                <div class="yes-title col-xs-2">Yes</div>\n' +
    '                                                <div class="no-title col-xs-2">No</div>\n' +
    '                                            </div>\n' +
    '                                            <div class="row">\n' +
    '                                                <div class="title col-xs-8">\n' +
    '                                                    Fork lift Operator\n' +
    '                                                </div>\n' +
    '                                                <div class="col-xs-2 yes-option">\n' +
    '                                                    <input type="radio" name="fork" ng-model="StaffDetail.ForkOperator" ng-checked="StaffDetail.ForkOperator == 1" value="1">\n' +
    '                                                </div>\n' +
    '                                                <div class="col-xs-2 no-option">\n' +
    '                                                    <input type="radio" name="fork" ng-model="StaffDetail.ForkOperator" ng-checked="StaffDetail.ForkOperator == 0" value="0">\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="first-aid col-xs-12">\n' +
    '                                            <div class="row">\n' +
    '                                                <div class="title col-xs-8">&nbsp;</div>\n' +
    '                                                <div class="yes-title col-xs-2">Yes</div>\n' +
    '                                                <div class="no-title col-xs-2">No</div>\n' +
    '                                            </div>\n' +
    '                                            <div class="row">\n' +
    '                                                <div class="title col-xs-8">\n' +
    '                                                    First Aid Rep.\n' +
    '                                                </div>\n' +
    '                                                <div class="col-xs-2 yes-option">\n' +
    '                                                    <input type="radio" name="first" ng-model="StaffDetail.FirstAid" ng-checked="StaffDetail.FirstAid == 1" value="1">\n' +
    '                                                </div>\n' +
    '                                                <div class="col-xs-2 no-option">\n' +
    '                                                    <input type="radio" name="first" ng-model="StaffDetail.FirstAid" ng-checked="StaffDetail.FirstAid == 0" value="0">\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="leave-days margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title content-right col-xs-8">\n' +
    '                                            Leave days / annum\n' +
    '                                        </div>\n' +
    '                                        <div class="col-xs-3 col-xs-offset-1 content">\n' +
    '                                            <input type="text" name="LeaveDays" ng-model="StaffDetail.LeaveDays" required ng-pattern="/^\\d+$/">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="error col-xs-12">\n' +
    '                                    <span ng-show="StaffForm.$submitted && StaffForm.LeaveDays.$error.required">Please enter Leave days / annum</span>\n' +
    '                                    <span ng-show="StaffForm.$submitted && StaffForm.LeaveDays.$error.pattern">Leave days / annum must be number</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="annual-salary margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title content-right col-xs-8">\n' +
    '                                            Annual Salary\n' +
    '                                        </div>\n' +
    '                                        <div class="col-xs-4 content">\n' +
    '                                            <input type="text" ng-model="StaffDetail.Salary" name="Salary" required ng-pattern="/^\\d+$/">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="error col-xs-12">\n' +
    '                                    <span ng-show="StaffForm.$submitted && StaffForm.Salary.$error.required">Please enter Annual Salary</span>\n' +
    '                                    <span ng-show="StaffForm.$submitted && StaffForm.Salary.$error.pattern">Annual Salary must be number</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="currently-employed col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-8">&nbsp;</div>\n' +
    '                                        <div class="yes-title col-xs-2">Yes</div>\n' +
    '                                        <div class="no-title col-xs-2">No</div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-8">\n' +
    '                                            Currently employed?\n' +
    '                                        </div>\n' +
    '                                        <div class="col-xs-2 yes-option">\n' +
    '                                            <input type="radio" ng-model="StaffDetail.Employed" name="Employed" ng-checked="StaffDetail.Employed == 1">\n' +
    '                                        </div>\n' +
    '                                        <div class="col-xs-2 no-option">\n' +
    '                                            <input type="radio" ng-model="StaffDetail.Employed" name="Employed" ng-checked="StaffDetail.Employed == 0">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="staff-address col-xs-6">\n' +
    '                            <div class="row">\n' +
    '                                <div class="payroll margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Payroll No.\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-9">\n' +
    '                                            <span ng-bind="StaffDetail.PayrollNumber"></span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="name margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Name\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-7">\n' +
    '                                            <input type="text" ng-model="StaffDetail.FirstName" name="FirstName" maxlength="25" required>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.FirstName.$error.required">Please enter Name</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="address1 margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Address line 1\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-7">\n' +
    '                                            <input type="text" ng-model="StaffDetail.Address1" name="Address1" maxlength="45" required>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.Address1.$error.required">Please enter Address line 1</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="address2 margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Address line 2\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-7">\n' +
    '                                            <input type="text" ng-model="StaffDetail.Address2" maxlength="40">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="address3 margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Address line 3\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-7">\n' +
    '                                            <input type="text" ng-model="StaffDetail.Address3" maxlength="40">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="address4 margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Address line 4\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-6">\n' +
    '                                            <input type="text" ng-model="StaffDetail.Address4" maxlength="30">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="postcode margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Postcode\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-3">\n' +
    '                                            <input type="text" ng-model="StaffDetail.Postcode" name="Postcode" maxlength="8" required>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.Postcode.$error.required">Please enter Postcode</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="homephone margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Home phone\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-4">\n' +
    '                                            <input type="text" ng-model="StaffDetail.HomePhone" name="HomePhone" maxlength="20" required ng-pattern="/^0|[1-9][0-9]*$/">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.HomePhone.$error.required">Please enter Home phone</span>\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.HomePhone.$error.pattern">Home phone must be number</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="mobile margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Mobile\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-4">\n' +
    '                                            <input type="text" ng-model="StaffDetail.MobilePhone" maxlength="20" name="MobilePhone" required ng-pattern="/^0|[1-9][0-9]*$/">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.MobilePhone.$error.required">Please enter Mobile</span>\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.MobilePhone.$error.pattern">Mobile must be number</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="email margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Email\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-9">\n' +
    '                                            <input type="email" ng-model="StaffDetail.Email" maxlength="50" name="Email" required>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.Email.$error.required">Please enter Email</span>\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.Email.$error.email">Please enter a valid email</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="staff-security col-xs-3">\n' +
    '                            <div class="row">\n' +
    '                                <div class="contracted-hours col-xs-8">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-12 color-dark-blue border bold">\n' +
    '                                            Contracted hours\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-12">\n' +
    '                                            <input type="text" class="hour" ng-model="StaffDetail.Hours" name="Hours" required ng-pattern="/^\\d+$/">\n' +
    '                                            <span>hours per</span>\n' +
    '                                            <select name="" id="" class="hour-type">\n' +
    '                                                <option value="">Week</option>\n' +
    '                                            </select>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="error col-xs-12">\n' +
    '                                    <span ng-show="StaffForm.$submitted && StaffForm.Hours.$error.required">Please enter Hour</span>\n' +
    '                                    <span ng-show="StaffForm.$submitted && StaffForm.Hours.$error.pattern">Hour must be number</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="security-access col-xs-8">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-12 color-dark-blue bold">\n' +
    '                                            Security access\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-12">\n' +
    '                                            <div class="row user">\n' +
    '                                                <div class="col-xs-9">\n' +
    '                                                    <p>User</p>\n' +
    '                                                </div>\n' +
    '                                                <div class="col-xs-3">\n' +
    '                                                    <input type="radio" ng-model="StaffDetail.Security" name="Security" value="1">\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                            <div class="row manager">\n' +
    '                                                <div class="col-xs-9">\n' +
    '                                                    <p>Manager</p>\n' +
    '                                                </div>\n' +
    '                                                <div class="col-xs-3">\n' +
    '                                                    <input type="radio" ng-model="StaffDetail.Security" name="Security" value="2">\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                            <div class="row super-user">\n' +
    '                                                <div class="col-xs-9">\n' +
    '                                                    <p>Super User</p>\n' +
    '                                                </div>\n' +
    '                                                <div class="col-xs-3">\n' +
    '                                                    <input type="radio" ng-model="StaffDetail.Security" name="Security" value="3">\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="system-assigned-contract col-xs-12">\n' +
    '                    <div class="row">\n' +
    '                        <div class="system-assigned col-xs-9">\n' +
    '                            <div class="row">\n' +
    '                                <div class="system-role col-xs-4">\n' +
    '                                    <div class="title col-xs-8 color-dark-blue border bold">\n' +
    '                                        System Role list\n' +
    '                                    </div>\n' +
    '                                    <div class="content clear-fix background-light-yellow color-blue">\n' +
    '                                        <p ng-repeat="item in AssignedRole" ng-click="selectItem(item, \'selected\')" ng-bind="item.Role" ng-class="{\'selected\': item.selected}">\n' +
    '                                        </p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="system-button col-xs-1">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="add col-xs-12 no-padding">\n' +
    '                                            <button type="button" ng-click="AddAssignedRole(AssignedRole)">Add</button> >\n' +
    '                                        </div>\n' +
    '                                        <div class="rmv col-xs-12 no-padding">\n' +
    '                                            < <button type="button" ng-click="RmvAssignedRole(StaffRoles)">Rmv</button>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="assigned-role col-xs-4">\n' +
    '                                    <div class="title col-xs-8 color-dark-blue border bold">\n' +
    '                                        Assigned Roles\n' +
    '                                    </div>\n' +
    '                                    <div class="content clear-fix background-light-yellow color-blue">\n' +
    '                                        <p ng-repeat="item in StaffRoles" ng-click="selectItem(item, \'selected\')" ng-bind="item.Role" ng-class="{\'selected\': item.selected}">\n' +
    '                                        </p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="save-area col-xs-3" ng-hide="isStaff">\n' +
    '                                    <button type="submit">Save</button>\n' +
    '                                </div>\n' +
    '                                <div class="save-area col-xs-3" ng-show="isStaff">\n' +
    '                                    <button type="submit">Update</button>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="contract col-xs-3">\n' +
    '                            <div class="title col-xs-12">\n' +
    '                                Add Contract of Employment:\n' +
    '                            </div>\n' +
    '                            <div class="content col-xs-12">\n' +
    '                                <button>Drop Contract here</button>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="first-role-new-member col-md-9">\n' +
    '                    <div class="row">\n' +
    '                        <div class="first-role content-right col-md-9">\n' +
    '                            First role is primary\n' +
    '                        </div>\n' +
    '                        <div class="new-member col-md-3">\n' +
    '                            <button ng-click="NewMember()">New Member</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="back col-md-3">\n' +
    '                    <button ng-click="redirectBack()">Back</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </form>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/partials/typeahead.tpl.html',
    '<div id="the-basics">\n' +
    '  <input class="typeahead" type="text">\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/department.tpl.html',
    '<div class="page department">\n' +
    '    <div class="left-menu" ng-include="\'/frontend/js/common/partials/left-menu.tpl.html\'"></div>\n' +
    '    <div class="department-content">\n' +
    '    	<img class="home-icon" src="../../../../../assets/images/Kythera_Blackbird_Home_icon.gif" ng-click="redirectHome()">\n' +
    '        <div ui-view></div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/landing-page/landing-page.tpl.html',
    '<div class="login-page department-page">\n' +
    '    <div class="page-content">\n' +
    '        <a ng-repeat="item in user.getDepartment()" ui-sref="department.home({DeptID: item.DeptID})">\n' +
    '            <button ng-bind="item.Description"></button>\n' +
    '        </a>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/login/index.tpl.html',
    '<div id="login" class="login-page">\n' +
    '    <div class="login-wrapper">\n' +
    '        <form method="post" name="LoginForm" ng-submit="LoginForm.$valid && login(user)" novalidate>\n' +
    '            <div class="form-group">\n' +
    '                <label>User</label>\n' +
    '                <input type="text" class="form-control" name="username" ng-model="user.Username" required>\n' +
    '                <div class="error">\n' +
    '                    <span ng-show="LoginForm.$submitted && LoginForm.username.$error.required">Please enter your username</span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="form-group">\n' +
    '                <label>Password</label>\n' +
    '                <input type="password" class="form-control" name="password" ng-model="user.UserPwd" required>\n' +
    '                <div class="error">\n' +
    '                    <span ng-show="LoginForm.$submitted && LoginForm.password.$error.required">Please enter your password</span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="form-group">\n' +
    '                <button type="submit">Login</button>\n' +
    '            </div>\n' +
    '        </form>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/report/report.tpl.html',
    '<div class="page report-page">\n' +
    '    <div class="container">\n' +
    '        <div class="row">\n' +
    '            <div class="report-header col-lg-lg-12">\n' +
    '                <div class="row">\n' +
    '                    <div class="left-header col-lg-3">\n' +
    '                        <div class="col-lg-12">\n' +
    '                            <h3><span class="wo">WO</span> No : 10101</h3></div>\n' +
    '                        <div class="col-lg-4 col-lg-offset-2">\n' +
    '                            <h4>required :</h4></div>\n' +
    '                        <div class="col-lg-2">\n' +
    '                            <h2 class="number-left">45</h2></div>\n' +
    '                        <div class="col-lg-1">\n' +
    '                            <h5 class="number-right">(51)</h5></div>\n' +
    '                    </div>\n' +
    '                    <div class="mid-header col-lg-5">\n' +
    '                        <div class="col-lg-12">\n' +
    '                            <h4>"Flugelbinder assembly"</h4></div>\n' +
    '                        <div class="col-lg-8">\n' +
    '                            <h3 class="part-no">Part No A320-0001</h3></div>\n' +
    '                        <div class="col-lg-4">\n' +
    '                            <h5 class="part-issue">Issue . 002</h5></div>\n' +
    '                        <div class="col-lg-8">\n' +
    '                            <h3 class="drawing">Drawing No 575-40559</h3></div>\n' +
    '                        <div class="col-lg-4">\n' +
    '                            <h6 class="drawing-issue">Issue . 1A</h6></div>\n' +
    '                    </div>\n' +
    '                    <div class="right-header col-lg-3">\n' +
    '                        <div class="col-lg-12">\n' +
    '                            <h3>Route Card</h3></div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <!-- end header -->\n' +
    '            <div class="report-content col-lg-lg-12">\n' +
    '                <div class="row">\n' +
    '                    <div class="header-content col-lg-11">\n' +
    '                        <div class="row">\n' +
    '                            <div class="left col-lg-7">\n' +
    '                                <div class="row">\n' +
    '                                    <div class="top col-lg-12">\n' +
    '                                        <div class="row">\n' +
    '                                            <h4 class="col-lg-2">Operation</h4>\n' +
    '                                            <h4 class="col-lg-6">Processes</h4>\n' +
    '                                            <h4 class="col-lg-4">Notes  </h4>\n' +
    '                                            <hr class="col-lg-12">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="bottom col-lg-12">\n' +
    '                                        <div class="row">\n' +
    '                                            <img class="col-lg-1 img" src="../assets/images/Milling_Icon_Sml.jpg">\n' +
    '                                            <h4 class="col-lg-3 Material">Material Issue</h4>\n' +
    '                                            <div>\n' +
    '                                                <span class="col-lg-2 grbno">GRB No.:</span>\n' +
    '                                                <input class="col-lg-1 grbno-text" type="text" value="000003">\n' +
    '                                            </div>\n' +
    '                                            <div>\n' +
    '                                                <span class="col-lg-2">IRN No.:</span>\n' +
    '                                                <input class="col-lg-1 irnno-text" type="text">\n' +
    '                                            </div>\n' +
    '                                            <h6 class="col-lg-2 notes">Notes about this materail</h6>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="right col-lg-4">\n' +
    '                                <div class="row">\n' +
    '                                    <div class="top col-lg-12">\n' +
    '                                        <div class="row">\n' +
    '                                            <h4 class="col-lg-3">Outcome</h4>\n' +
    '                                            <h4 class="col-lg-5">Inspecto</h4>\n' +
    '                                            <h4 class="col-lg-4">Inspection</h4>\n' +
    '                                            <hr class="col-lg-12">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="bottom col-lg-12">\n' +
    '                                        <div class="row">\n' +
    '                                            <img class="col-lg-1 img" src="../assets/images/SmallTick.png">\n' +
    '                                            <h5 class="col-lg-4 Material">Material Issue</h5>\n' +
    '                                            <h3 class="col-lg-6 bla">Bla Bla ..</h3>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col-lg-10 bottom">\n' +
    '                                <div class="row">\n' +
    '                                    <div class="col-lg-2 logoa">\n' +
    '                                        <div class="row">\n' +
    '                                            <div class="col-md-12">\n' +
    '                                                <span class="span">1</span>\n' +
    '                                                <h3 class="th7">7th nov</h3>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="top col-lg-12">\n' +
    '                                        <div class="row">\n' +
    '                                            <h6 class="col-lg-1">Spec</h6>\n' +
    '                                            <h6 class="col-lg-1">Shape</h6>\n' +
    '                                            <h6 class="col-lg-1">UOM</h6>\n' +
    '                                            <h6 class="col-lg-1">Approval</h6>\n' +
    '                                            <h6 class="col-lg-1">Size</h6>\n' +
    '                                            <h6 class="col-lg-1">City</h6>\n' +
    '                                            <h6 class="col-lg-2">Issue Lgth</h6>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="mid col-lg-9">\n' +
    '                                        <div class="row">\n' +
    '                                            <hr class="col-lg-9 hr">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="bottom col-lg-12">\n' +
    '                                        <h6 class="col-lg-2">S07-1008</h6>\n' +
    '                                        <h6 class="col-lg-1 Diameter">Diameter</h6>\n' +
    '                                        <h6 class="col-lg-1 Litres">Litres</h6>\n' +
    '                                        <h6 class="col-lg-1 ios">IOS9001</h6>\n' +
    '                                        <h6 class="col-lg-1 size">1</h6>\n' +
    '                                        <h6 class="col-lg-1">21</h6>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="mid-content col-lg-11">\n' +
    '                        <div class="row">\n' +
    '                            <div class="left col-lg-8">\n' +
    '                                <div class="row">\n' +
    '                                    <div class="top col-lg-12">\n' +
    '                                        <div class="row">\n' +
    '                                            <div class="col-lg-1 col-lg-offset-4 set">\n' +
    '                                                <input type="text" class="t-set" value="Set">\n' +
    '                                            </div>\n' +
    '                                            <div class="col-lg-1">\n' +
    '                                                <input type="text" class="run" value="Run">\n' +
    '                                            </div>\n' +
    '                                            <div class="col-lg-1">\n' +
    '                                                <input type="text" class="iss" value="Iss.">\n' +
    '                                            </div>\n' +
    '                                            <hr class="hr col-lg-12">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="bottom col-lg-11">\n' +
    '                                        <div class="row">\n' +
    '                                            <img class="col-lg-1 img" src="../assets/images/Milling_Icon_Sml.jpg">\n' +
    '                                            <h4 class="col-lg-3 Material">Manual Turning</h4>\n' +
    '                                            <h6 class="col-lg-3 Mazak">on Mazak VTC800/30SR</h6>\n' +
    '                                            <div class="col-lg-1">\n' +
    '                                                <div class="row">\n' +
    '                                                    <h6 class="col-lg-12 prog">Prog No\'s.</h6>\n' +
    '                                                    <hr class="col-lg-12 hr">\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                            <h6 class="col-lg-4 dia">21.1 dia x 3.5 with 0.2 rads 313.0350.00AS45</h6>\n' +
    '                                            <div class="col-lg-6">\n' +
    '                                                <div class="row text">\n' +
    '                                                    <div class="col-lg-12">\n' +
    '                                                        <p class="hold">hold on mandrel and cut serration as per drawing</p>\n' +
    '                                                    </div>\n' +
    '                                                    <div class="col-lg-12">\n' +
    '                                                        <p class="leave">leave .005 thread diameter for treatment</p>\n' +
    '                                                    </div>\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                            <div class="col-lg-1 number">\n' +
    '                                                <div class="row">\n' +
    '                                                    <div class="col-lg-12">\n' +
    '                                                        <p class="col-lg-12 prog">123789</p>\n' +
    '                                                    </div>\n' +
    '                                                    <div class="col-lg-12">\n' +
    '                                                        <p class="col-lg-12 prog">456321</p>\n' +
    '                                                    </div>\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                            <div class="col-lg-10">\n' +
    '                                                <div class="row">\n' +
    '                                                    <span class="span">2</span>\n' +
    '                                                    <h3 class="th10">10th nov</h3>\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                            <div class="col-lg-12 tooling">\n' +
    '                                                <div class="row">\n' +
    '                                                    <h4 class="col-lg-7">Tooling</h4>\n' +
    '                                                    <h4 class="col-lg-5">Fixtures</h4>\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                            <div class="col-lg-12 two-table">\n' +
    '                                                <div class="row">\n' +
    '                                                    <div class="col-lg-8">\n' +
    '                                                        <table class="table table-bordered grb ">\n' +
    '                                                            <tr>\n' +
    '                                                                <th>GRB No.</th>\n' +
    '                                                                <th>Description</th>\n' +
    '                                                                <th>Type</th>\n' +
    '                                                                <th>Sub type</th>\n' +
    '                                                                <th>Status</th>\n' +
    '                                                            </tr>\n' +
    '                                                            <tr>\n' +
    '                                                                <td>810072</td>\n' +
    '                                                                <td>Capliper</td>\n' +
    '                                                                <td>End millxxxxxxxe</td>\n' +
    '                                                                <td>Bullnose millxxxxxxxe</td>\n' +
    '                                                                <td>x</td>\n' +
    '                                                            </tr>\n' +
    '                                                            <tr>\n' +
    '                                                                <td>810073</td>\n' +
    '                                                                <td>Bigger Capliper</td>\n' +
    '                                                                <td>End millxxxxxxxe</td>\n' +
    '                                                                <td>Bullnose</td>\n' +
    '                                                                <td>x</td>\n' +
    '                                                            </tr>\n' +
    '                                                        </table>\n' +
    '                                                    </div>\n' +
    '                                                    <div class="col-lg-5">\n' +
    '                                                        <table class="table table-bordered fixture">\n' +
    '                                                            <tr>\n' +
    '                                                                <th>GRB No.</th>\n' +
    '                                                                <th>Description</th>\n' +
    '                                                                <th>Status</th>\n' +
    '                                                            </tr>\n' +
    '                                                            <tr>\n' +
    '                                                                <td>910073</td>\n' +
    '                                                                <td>Fluge lbinder base</td>\n' +
    '                                                                <td>x</td>\n' +
    '                                                            </tr>\n' +
    '                                                            <tr>\n' +
    '                                                                <td>101010</td>\n' +
    '                                                                <td>Towerfeild jig</td>\n' +
    '                                                                <td>x</td>\n' +
    '                                                            </tr>\n' +
    '                                                        </table>\n' +
    '                                                    </div>\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="right col-lg-4">\n' +
    '                                <div class="row">\n' +
    '                                    <div class="top col-lg-12">\n' +
    '                                        <div class="row top-right">\n' +
    '                                            <div class="col-lg-1 col-lg-offset-4 set">\n' +
    '                                                <input type="text" class="ins" value="Ins">\n' +
    '                                            </div>\n' +
    '                                            <div class="col-lg-1">\n' +
    '                                                <input type="text" class="s" value="S">\n' +
    '                                            </div>\n' +
    '                                            <div class="col-lg-1">\n' +
    '                                                <input type="text" class="equal" value="=">\n' +
    '                                            </div>\n' +
    '                                            <hr class="hr col-lg-12">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="bottom col-lg-12">\n' +
    '                                        <div class="row">\n' +
    '                                            <img class="col-lg-1 img" src="../assets/images/Milling_Icon_Sml.jpg">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="footer-content col-lg-11">\n' +
    '                        <div class="row">\n' +
    '                            <div class="left col-lg-8">\n' +
    '                                <div class="row">\n' +
    '                                    <div class="top col-lg-12">\n' +
    '                                        <div class="row">\n' +
    '                                            <div class="col-lg-1 col-lg-offset-4 set">\n' +
    '                                                <input type="text" class="t-set" value="Set">\n' +
    '                                            </div>\n' +
    '                                            <div class="col-lg-1">\n' +
    '                                                <input type="text" class="run" value="Run">\n' +
    '                                            </div>\n' +
    '                                            <div class="col-lg-1">\n' +
    '                                                <input type="text" class="made" value="Made.">\n' +
    '                                            </div>\n' +
    '                                            <hr class="hr col-lg-12">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="bottom col-md-11">\n' +
    '                                        <div class="row">\n' +
    '                                            <img class="col-lg-1 img" src="../assets/images/SubconIconSml.jpg">\n' +
    '                                            <h4 class="col-lg-3 Material">Heat Treatment</h4>\n' +
    '                                            <h6 class="note">a super subcom note</h6>\n' +
    '                                            <div class="col-lg-12 number">\n' +
    '                                                <div class="row">\n' +
    '                                                    <span class="span">3</span>\n' +
    '                                                    <h3 class="th21">21st nov</h3>\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                            <div class="col-lg-12 cer">\n' +
    '                                                <span>Certificate No\'s.:</span>\n' +
    '                                                <input type="text">\n' +
    '                                                <input type="text">\n' +
    '                                                <input type="text">\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="right col-lg-4">\n' +
    '                                <div class="row">\n' +
    '                                    <div class="top col-lg-12">\n' +
    '                                        <div class="row top-right">\n' +
    '                                            <div class="col-lg-1 col-lg-offset-4 set">\n' +
    '                                                <input type="text" class="ins" value="Ins">\n' +
    '                                            </div>\n' +
    '                                            <div class="col-lg-1">\n' +
    '                                                <input type="text" class="s" value="S">\n' +
    '                                            </div>\n' +
    '                                            <div class="col-lg-1">\n' +
    '                                                <input type="text" class="equal" value="=">\n' +
    '                                            </div>\n' +
    '                                            <hr class="hr col-lg-12">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="bottom col-lg-12">\n' +
    '                                        <div class="row">\n' +
    '                                            <img class="col-lg-1 img" src="../assets/images/Milling_Icon_Sml.jpg">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <!-- end content -->\n' +
    '            <div class="report-footer col-lg-lg-12">\n' +
    '                <div class="row">\n' +
    '                    <div class="header-footer col-lg-12">\n' +
    '                        <div class="row">\n' +
    '                            <div class="left col-lg-7">\n' +
    '                                <div class="row">\n' +
    '                                    <div class="top col-lg-12">\n' +
    '                                        <div class="row">\n' +
    '                                            <input class="col-lg-2 one" type="text" value="Rcvd">\n' +
    '                                            <input type="text" class="col-lg-6 two">\n' +
    '                                            <input class="col-lg-4 three" type="text">\n' +
    '                                            <hr class="col-lg-12 hr">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="bottom col-lg-12">\n' +
    '                                        <div class="row">\n' +
    '                                            <img class="col-lg-1 img" src="../assets/images/Milling_Icon_Sml.jpg">\n' +
    '                                            <h4 class="col-lg-3 Material">Material Issue</h4>\n' +
    '                                            <div>\n' +
    '                                                <span class="col-lg-2 grbno">GRB No.:</span>\n' +
    '                                            </div>\n' +
    '                                            <div>\n' +
    '                                                <span class="col-lg-2">IRN No.:</span>\n' +
    '                                                <input class="col-lg-1 irnno-text" type="text">\n' +
    '                                            </div>\n' +
    '                                            <div class="top col-lg-12">\n' +
    '                                                <div class="row">\n' +
    '                                                    <h6 class="col-lg-1">Spec</h6>\n' +
    '                                                    <h6 class="col-lg-1">Shape</h6>\n' +
    '                                                    <h6 class="col-lg-1">UOM</h6>\n' +
    '                                                    <h6 class="col-lg-1">Approval</h6>\n' +
    '                                                    <h6 class="col-lg-1">Size</h6>\n' +
    '                                                    <h6 class="col-lg-1">City</h6>\n' +
    '                                                    <h6 class="col-lg-2">Issue Lgth</h6>\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                            <hr class="col-lg-12 hr">\n' +
    '                                            <div class="col-lg-7 in">\n' +
    '                                                <input type="text" class="set" value="Iss">\n' +
    '                                                <input type="text" class="run" value="Run">\n' +
    '                                                <input type="text" class="iss" value="Set">\n' +
    '                                            </div>\n' +
    '                                            <div class="col-lg-11 ky">\n' +
    '                                                <h6>Kythera Route card vl</h6>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="right col-lg-4">\n' +
    '                                <div class="row">\n' +
    '                                    <div class="top col-lg-12">\n' +
    '                                        <div class="row top-right">\n' +
    '                                            <div class="col-lg-1 col-lg-offset-4 set">\n' +
    '                                                <input type="text" class="ins" value="Ins">\n' +
    '                                            </div>\n' +
    '                                            <div class="col-lg-1">\n' +
    '                                                <input type="text" class="s" value="S">\n' +
    '                                            </div>\n' +
    '                                            <div class="col-lg-1">\n' +
    '                                                <input type="text" class="equal" value="=">\n' +
    '                                            </div>\n' +
    '                                            <hr class="hr col-lg-12">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="bottom col-lg-12">\n' +
    '                                        <div class="row">\n' +
    '                                            <img class="col-lg-1 img" src="../assets/images/Milling_Icon_Sml.jpg">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="in col-lg-12">\n' +
    '                                        <input class="ins" type="text" value="Ins">\n' +
    '                                        <input class="s" type="text" value="S">\n' +
    '                                        <input class="equal" type="text" value="=">\n' +
    '                                    </div>\n' +
    '                                    <hr class="col-lg-6 hr">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <!-- end footer -->\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/test/test.tpl.html',
    'asfasdf asdf sfas fs ');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/treatment-admin/treatment-admin.tpl.html',
    '<div class="page treatment-admin">\n' +
    '    <div class="container-fluid">\n' +
    '        <div class="row">\n' +
    '            <div class="header col-md-12">\n' +
    '                <div class="row">\n' +
    '                    <div class="left col-md-4">\n' +
    '                        <div class="text">\n' +
    '                            <p>Treatments list</p>\n' +
    '                            <p>Add new treatment</p>\n' +
    '                            <input type="text">\n' +
    '                            <br>\n' +
    '                            <input class="add" type="submit" value="Add">\n' +
    '                            <br>\n' +
    '                            <p class="de">Delete selected Treatment</p>\n' +
    '                            <input type="submit" class="del" value="Delete">\n' +
    '                        </div>\n' +
    '                        <div class="sl">\n' +
    '                            <select multiple class="form-control" name="" id="">\n' +
    '                                <option value="">Heat Treatment</option>\n' +
    '                                <option value="">Thread cutting</option>\n' +
    '                                <option value="">Copper Strip</option>\n' +
    '                                <option value="">Grinding</option>\n' +
    '                            </select>\n' +
    '                        </div>\n' +
    '                        <div class="im">\n' +
    '                            <img class="col-lg-1 img up" src="../assets/images/Up_Arrow_Sml.png">\n' +
    '                            <br>\n' +
    '                            <p>Sort</p>\n' +
    '                            <img class="col-lg-1 img dow" src="../assets/images/Down_Arrow_Sml.png">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="mid col-md-4">\n' +
    '                        <div class="text">\n' +
    '                            <p>Operations list</p>\n' +
    '                            <p>Add new treatment</p>\n' +
    '                            <input type="text">\n' +
    '                            <br>\n' +
    '                            <input class="add" type="submit" value="Add">\n' +
    '                            <br>\n' +
    '                            <p class="de">Delete selected Operation</p>\n' +
    '                            <input type="text" class="del" value="Delete">\n' +
    '                        </div>\n' +
    '                        <div class="sl">\n' +
    '                            <select multiple class="form-control" name="" id="">\n' +
    '                                <option value="">Heat Treatment</option>\n' +
    '                                <option value="">Thread cutting</option>\n' +
    '                                <option value="">Copper Strip</option>\n' +
    '                                <option value="">Grinding</option>\n' +
    '                            </select>\n' +
    '                        </div>\n' +
    '                        <div class="im">\n' +
    '                            <img class="col-lg-1 img up" src="../assets/images/Up_Arrow_Sml.png">\n' +
    '                            <br>\n' +
    '                            <p>Sort</p>\n' +
    '                            <img class="col-lg-1 img dow" src="../assets/images/Down_Arrow_Sml.png">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="right col-md-4">\n' +
    '                        <div class="top">\n' +
    '                            <div class="text">\n' +
    '                                <p>Sundries list</p>\n' +
    '                                <p>Add new Sundry</p>\n' +
    '                                <input type="text">\n' +
    '                                <br>\n' +
    '                                <input class="add" type="submit" value="Add">\n' +
    '                                <br>\n' +
    '                                <p class="de">Delete selected Sundries</p>\n' +
    '                                <input type="submit" class="del" value="Delete">\n' +
    '                            </div>\n' +
    '                            <div class="sl">\n' +
    '                                <select multiple class="form-control" name="" id="">\n' +
    '                                    <option value="">Heat Treatment</option>\n' +
    '                                    <option value="">Thread cutting</option>\n' +
    '                                    <option value="">Copper Strip</option>\n' +
    '                                    <option value="">Grinding</option>\n' +
    '                                </select>\n' +
    '                            </div>\n' +
    '                            <div class="im">\n' +
    '                                <img class="col-lg-1 img up" src="../assets/images/Up_Arrow_Sml.png">\n' +
    '                                <br>\n' +
    '                                <p>Sort</p>\n' +
    '                                <img class="col-lg-1 img dow" src="../assets/images/Down_Arrow_Sml.png">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="bottom">\n' +
    '                            <div class="text">\n' +
    '                                <p>Sundries list</p>\n' +
    '                                <p>Add new Sundry</p>\n' +
    '                                <input type="text">\n' +
    '                                <br>\n' +
    '                                <input class="add" type="submit" value="Add">\n' +
    '                                <br>\n' +
    '                                <p class="de">Delete selected Sundrie</p>\n' +
    '                                <input type="submit" class="del" value="Delete">\n' +
    '                            </div>\n' +
    '                            <div class="sl">\n' +
    '                                <select multiple class="form-control" name="" id="">\n' +
    '                                    <option value="">Heat Treatment</option>\n' +
    '                                    <option value="">Thread cutting</option>\n' +
    '                                    <option value="">Copper Strip</option>\n' +
    '                                    <option value="">Grinding</option>\n' +
    '                                </select>\n' +
    '                            </div>\n' +
    '                            <div class="im">\n' +
    '                                <img class="col-lg-1 img up" src="../assets/images/Up_Arrow_Sml.png">\n' +
    '                                <br>\n' +
    '                                <p>Sort</p>\n' +
    '                                <img class="col-lg-1 img dow" src="../assets/images/Down_Arrow_Sml.png">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="content col-md-12">\n' +
    '                <div class="row">\n' +
    '                    <div class="left col-md-4">\n' +
    '                        <div class="text">\n' +
    '                            <p>Scheduling contingencies(fat)</p>\n' +
    '                        </div>\n' +
    '                        <div class="tb">\n' +
    '                            <p>days</p>\n' +
    '                            <table class="table table-bordered">\n' +
    '                                <tr>\n' +
    '                                    <td>Between Operation</td>\n' +
    '                                    <td>3</td>\n' +
    '                                </tr>\n' +
    '                                <tr>\n' +
    '                                    <td>Delivery</td>\n' +
    '                                    <td>10</td>\n' +
    '                                </tr>\n' +
    '                                <tr>\n' +
    '                                    <td></td>\n' +
    '                                    <td></td>\n' +
    '                                </tr>\n' +
    '                            </table>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="mid col-md-4">\n' +
    '                        <div class="text">\n' +
    '                            <p>OP. PAuse reasons :</p>\n' +
    '                            <p>Add new reason</p>\n' +
    '                            <input type="text">\n' +
    '                            <br>\n' +
    '                            <input class="add" type="submit" value="Add">\n' +
    '                        </div>\n' +
    '                        <div class="sl">\n' +
    '                            <select multiple class="form-control" name="" id="">\n' +
    '                                <option value="">Heat Treatment</option>\n' +
    '                                <option value="">Thread cutting</option>\n' +
    '                                <option value="">Copper Strip</option>\n' +
    '                                <option value="">Grinding</option>\n' +
    '                            </select>\n' +
    '                        </div>\n' +
    '                        <div class="im">\n' +
    '                            <img class="col-lg-1 img up" src="../assets/images/Up_Arrow_Sml.png">\n' +
    '                            <br>\n' +
    '                            <p>Sort</p>\n' +
    '                            <img class="col-lg-1 img dow" src="../assets/images/Down_Arrow_Sml.png">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="right col-md-4">\n' +
    '                        <input type="submit" class="back" value="Back">\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/admin-reference/admin_reference.tpl.html',
    '<div class="admin-reference-page">\n' +
    '    <div class="page-body">\n' +
    '        <div class="title-global-page">Admin References</div>\n' +
    '        <div class="row row-1">\n' +
    '            <div class="col-md-6 area area-1">\n' +
    '                <div class="col-md-6 module module-our-approval">\n' +
    '                    <div class="module-title">Our Approval levels</div>\n' +
    '                    <div class="module-body">\n' +
    '                        <div class="col-md-6 module-control">\n' +
    '                            <button ng-click="addOurApproval(OurApproval, $event)" class="kythera-small-button" type="submit">Add</button>\n' +
    '                            <input type="text" ng-model="OurApproval" maxlength="15">\n' +
    '                            <button ng-click="deleteOurApproval(OurApprovalList)" class="kythera-small-button">Delete</button>\n' +
    '                        </div>\n' +
    '                        <div class="col-md-6 module-data">\n' +
    '                            <div class="area-table">\n' +
    '                                <table ng-show="OurApprovalList | hasItem">\n' +
    '                                    <tr ng-repeat="item in OurApprovalList" ng-click="selectItem(item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                        <td>\n' +
    '                                            <span ng-bind="item.Approval"></span>\n' +
    '                                        </td>\n' +
    '                                    </tr>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-md-6 module module-deviation-reasons">\n' +
    '                    <div class="module-title">Deviation Reasons</div>\n' +
    '                    <div class="module-body">\n' +
    '                        <div class="col-md-6 module-control">\n' +
    '                            <button class="kythera-small-button" type="submit" ng-click="addDeviation(Deviation)">Add</button>\n' +
    '                            <input type="text" ng-model="Deviation" maxlength="20">\n' +
    '                            <button class="kythera-small-button" ng-click="deleteDeviationReasons(DeviationList)">Delete</button>\n' +
    '                        </div>\n' +
    '                        <div class="col-md-6 module-data">\n' +
    '                            <div class="area-table">\n' +
    '                                <table>\n' +
    '                                    <tr ng-repeat="item in DeviationList" ng-click="selectItem(item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                        <td>\n' +
    '                                            <span ng-bind="item.Deviation"></span>\n' +
    '                                        </td>\n' +
    '                                    </tr>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="col-md-6 area area-2">\n' +
    '                <div class="col-md-6 module module-material-shapes">\n' +
    '                    <div class="module-title">Material Shapes</div>\n' +
    '                    <div class="module-body">\n' +
    '                        <div class="col-md-6 module-control">\n' +
    '                            <button class="kythera-small-button" ng-click="addMaterialShape(MaterialShape, MaterialShapeRequire)">Add</button>\n' +
    '                            <input type="text" ng-model="MaterialShape" maxlength="15">\n' +
    '                            <label>Requires Description </label>\n' +
    '                            <input type="checkbox" ng-model="MaterialShapeRequire" ng-true-value="1" ng-false-value="0">\n' +
    '                            <button class="kythera-small-button" ng-click="delMaterialShape(ShapesList)">Delete</button>\n' +
    '                        </div>\n' +
    '                        <div class="col-md-6 module-data">\n' +
    '                            <div class="area-table">\n' +
    '                                <table ng-show="ShapesList | hasItem">\n' +
    '                                    <tr ng-repeat="item in ShapesList" ng-click="selectItem(item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                        <td><span ng-bind="item.Description"></span></td>\n' +
    '                                        <td>{{item.ReqdDesc == 1 ? "Y" : "N"}}</td>\n' +
    '                                    </tr>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-md-6 module module-units-measure">\n' +
    '                    <div class="module-title">Units of Measure</div>\n' +
    '                    <div class="module-body">\n' +
    '                        <div class="col-md-6 module-control">\n' +
    '                            <button class="kythera-small-button" type="submit" ng-click="addUOM(UOM)">Add</button>\n' +
    '                            <input type="text" ng-model="UOM" maxlength="11">\n' +
    '                            <button class="kythera-small-button" type="submit" ng-click="deleteUOM(ListUOM)">Delete</button>\n' +
    '                        </div>\n' +
    '                        <div class="col-md-6 module-data">\n' +
    '                            <div class="area-table">\n' +
    '                                <table ng-show="ListUOM | hasItem">\n' +
    '                                    <tr ng-repeat="item in ListUOM" ng-click="selectItem(item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                        <td>\n' +
    '                                            <span ng-bind="item.Description"></span>\n' +
    '                                        </td>\n' +
    '                                    </tr>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row row-2">\n' +
    '            <div class="col-md-6 area area-1">\n' +
    '                <div class="col-md-6 module module-all-approval-level">\n' +
    '                    <div class="module-title content-right">All Approval levels</div>\n' +
    '                    <div class="module-body">\n' +
    '                        <div class="col-md-6 module-control">\n' +
    '                            <button class="kythera-small-button" ng-click="addApprovalLevel(ApprovalLevel)">Add</button>\n' +
    '                            <input type="text" ng-model="ApprovalLevel" maxlength="20">\n' +
    '                            <button class="kythera-small-button" type="submit" ng-click="deleteAllApproval(AllApproval)">Delete</button>\n' +
    '                        </div>\n' +
    '                        <div class="col-md-6 module-data">\n' +
    '                            <div class="area-table">\n' +
    '                                <table ng-show="AllApproval | hasItem">\n' +
    '                                    <tr ng-repeat="item in AllApproval" ng-click="selectItem(item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                        <td>\n' +
    '                                            <span ng-bind="item.Description"></span>\n' +
    '                                        </td>\n' +
    '                                    </tr>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-md-6 module module-document-drop-types">\n' +
    '                    <div class="module-title content-right">Document Drop types</div>\n' +
    '                    <div class="module-body">\n' +
    '                        <div class="col-md-6 module-control">\n' +
    '                            <button class="kythera-small-button" ng-click="AddDocumentDropType(DocumentDropType)">Add</button>\n' +
    '                            <input type="text" ng-model="DocumentDropType" maxlength="17">\n' +
    '                            <button class="kythera-small-button" ng-click="DeleteDocumentDropType(ListDropTypes)">Delete</button>\n' +
    '                        </div>\n' +
    '                        <div class="col-md-6 module-data">\n' +
    '                            <div class="area-table">\n' +
    '                                <table ng-show="ListDropTypes | hasItem">\n' +
    '                                    <tr ng-repeat="item in ListDropTypes" ng-click="selectItem(item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                        <td>\n' +
    '                                            <span ng-bind="item.DropType"></span>\n' +
    '                                        </td>\n' +
    '                                    </tr>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="col-md-5 area area-2">\n' +
    '                <div class="col-md-12 module module-bussiness-shutdowns">\n' +
    '                    <div class="module-title">Business shutdowns</div>\n' +
    '                    <div class="module-body">\n' +
    '                        <div class="col-md-6 module-control">\n' +
    '                            <button class="kythera-small-button" type="submit" ng-click="addBusinessShutdown(shutDate, shutReason)">Add</button>\n' +
    '                            <div class="area-input">\n' +
    '                                <input type="text" class="input-date" ng-model="shutDate">\n' +
    '                                <input type="text" class="input-desc" ng-model="shutReason" maxlength="22">\n' +
    '                            </div>\n' +
    '                            <button class="kythera-small-button" type="submit" ng-click="deleteBusinessShutdown(listBusiness)">Delete</button>\n' +
    '                        </div>\n' +
    '                        <div class="col-md-6 module-data">\n' +
    '                            <div class="area-table">\n' +
    '                                <table ng-show="listBusiness | hasItem">\n' +
    '                                    <tr ng-repeat="item in listBusiness" ng-click="selectItem(item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                        <td>\n' +
    '                                            <span ng-bind="item.Shutdate | timeFormat: \'DD/MM\'"></span>\n' +
    '                                        </td>\n' +
    '                                        <td>\n' +
    '                                            <span ng-bind="item.ShutReason"></span>\n' +
    '                                        </td>\n' +
    '                                    </tr>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row row-3">\n' +
    '            <div class="container">\n' +
    '                <div class="col-md-12">\n' +
    '                    <button class="back kythera-large-button" type="button" ng-click="redirectBack()">Back</button>\n' +
    '                    <button type="button" class="export-customer kythera-large-button" ng-click="exportCustomer()">Export all Customers</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/customer/customer.tpl.html',
    '<div ui-view></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/customer-order/customer-order.tpl.html',
    '<div class="customer-order-page">\n' +
    '    <div class="page-body">\n' +
    '        <div class="title-global-page">Customer Order</div>\n' +
    '        <div class="page-top">\n' +
    '            <div class="area area-1 col-xs-4">\n' +
    '                <div class="item">\n' +
    '                    <label for="">Customer</label>\n' +
    '                    <select name="customer" ng-model="customer" ng-options="customer as customer.Alias for customer in customers" ng-change="changeCustomer(customer)">\n' +
    '                    </select>\n' +
    '                </div>\n' +
    '                <div class="item">\n' +
    '                    <label for="">Customer Order Number</label>\n' +
    '                    <input type="text" ng-model="CustomerOrder.CustomerOrderRef" name="CustomerOrderRef" ng-model-option="{debounce : 500}" ng-change="UpdateCO(selectCO, CustomerOrder, ReleaseType, DeliveryPoint)" maxlength="25">\n' +
    '                </div>\n' +
    '                <div class="item">\n' +
    '                    <label for="">Select Customer Order</label>\n' +
    '                    <select ng-model="selectCO" ng-options="option as option.CustomerOrderRef for option in CustomerOrderList" ng-change="selectCustomerOrder(selectCO)" name="selectCO">\n' +
    '                    </select>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="area area-2 col-xs-3">\n' +
    '                <button class="new-order" type="button" ng-click="AddNewCO(customer, CustomerOrder, ReleaseType, DeliveryPoint)">New Order</button>\n' +
    '                <button class="customer-center" type="button" ng-click="redirectCustomerCenter()">Customer Center</button>\n' +
    '            </div>\n' +
    '            <div class="area area-3 col-xs-3 no-padding">\n' +
    '                <div class="item">\n' +
    '                    <label for="">Release type</label>\n' +
    '                    <select name="ReleaseType" ng-model="ReleaseType" ng-options="option as option.Approval for option in ReleaseTypes" name="ReleaseType">\n' +
    '                    </select>\n' +
    '                </div>\n' +
    '                <div class="item">\n' +
    '                    <label for="">Delivery point</label>\n' +
    '                    <select name="DeliveryPoint" ng-model="DeliveryPoint" ng-options="option as option.Address1 for option in DeliveryPoints" name="DeliveryPoint">\n' +
    '                    </select>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="area area-4 col-xs-2 no-padding-right">\n' +
    '                <button type="button" dropzone on-success="uploadSuccess" before-upload="beforeUpload" max-file-name="30">Drop Original Order here</button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="page-content">\n' +
    '            <div class="customer-order">\n' +
    '                <div class="area-table table-2 table-customer-order">\n' +
    '                    <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_LIWO">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <td class="col-xs-1 no-padding">Line No.</td>\n' +
    '                                <td class="col-xs-3">Part number // issue // Template cond.</td>\n' +
    '                                <td class="col-xs-8 no-padding">\n' +
    '                                    <table>\n' +
    '                                        <tr>\n' +
    '                                            <td class="col-xs-1 no-padding">₤ each</td>\n' +
    '                                            <td class="col-xs-2">Delivery date</td>\n' +
    '                                            <td class="col-xs-2 no-padding">\n' +
    '                                                <p class="content-center">Quantity</p>\n' +
    '                                                <p class="text-center">\n' +
    '                                                    <span>Reqd</span>\n' +
    '                                                    <span>Build</span>\n' +
    '                                                </p>\n' +
    '                                            </td>\n' +
    '                                            <td class="col-xs-3 no-padding">Change / Close Reason</td>\n' +
    '                                            <td class="col-xs-1 no-padding">FAIR?</td>\n' +
    '                                            <td class="col-xs-2">W/O No.</td>\n' +
    '                                            <td class="col-xs-1 no-padding"></td>\n' +
    '                                        </tr>\n' +
    '                                    </table>\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="LIWO in LIWOinfo" ng-class="{\'tr-active\': LIWO.chooseLineItem}">\n' +
    '                                <td class="col-xs-1 no-padding" ng-click="selectOneItem(LIWOinfo, LIWO, \'chooseLineItem\')">\n' +
    '                                    <span>Cust Ref</span>\n' +
    '                                    <input type="text" ng-model="LIWO[0].CustRef" maxlength="5" ng-model="{debounce: 500}" ng-change="UpdateLineItem(LIWO[0]); selectOneItem(LIWOinfo, LIWO, \'selected\')">\n' +
    '                                    <span>ESL QuNo.</span>\n' +
    '                                    <input type="text" ng-model="LIWO[0].QuoteNo" maxlength="8" ng-change="selectOneItem(LIWOinfo, LIWO, \'selected\')">\n' +
    '                                </td>\n' +
    '                                <td class="col-xs-3">\n' +
    '                                    <select ng-model="LIWO[0].PTemplateID" ng-options="option as option.DisplayData for option in TemplateLists" ng-change="UpdateLineItem(LIWO[0]); selectOneItem(LIWOinfo, LIWO, \'selected\')"></select>\n' +
    '                                </td>\n' +
    '                                <td class="col-xs-8 no-padding">\n' +
    '                                    <table>\n' +
    '                                        <tr ng-repeat="item in LIWO" ng-if="item.WONo !== null" ng-class="{\'highlight\': item.highlight}" ng-click="selectWO(item, LIWOinfo)">\n' +
    '                                            <td class="col-xs-1 no-padding">\n' +
    '                                                <input type="text" ng-model="item.PriceEach" ng-model-option="{debounce: 500}" ng-change="UpdateWO(item); changeEachPrice(LIWOinfo)" ng-disabled="!LIWO[0].PTemplateID || item.Live != 0">\n' +
    '                                            </td>\n' +
    '                                            <td class="col-xs-2">\n' +
    '                                                <input class="width-90-percent" type="text" ng-disabled="!LIWO[0].PTemplateID || item.Live != 0" ng-model="item.DeliveryDate">\n' +
    '                                            </td>\n' +
    '                                            <td class="col-xs-2 no-padding">\n' +
    '                                                <input class="text-center pull-left width-48-percent" type="text" ng-model="item.Reqd" ng-change="UpdateWO(item); changeEachPrice(LIWOinfo)" ng-disabled="!LIWO[0].PTemplateID || item.Live != 0">\n' +
    '                                                <input class="text-center pull-right width-48-percent" type="text" ng-model="item.Build" ng-change="UpdateWO(item); changeEachPrice(LIWOinfo)" ng-disabled="!LIWO[0].PTemplateID || item.Live != 0">\n' +
    '                                            </td>\n' +
    '                                            <td class="col-xs-3 no-padding">\n' +
    '                                                <input class="width-90-percent" ng-model="item.ChangeReason" maxlength="30" ng-change="selectOneItem(LIWOinfo, LIWO, \'selected\')">\n' +
    '                                            </td>\n' +
    '                                            <td class="col-xs-1 no-padding">\n' +
    '                                                <input type="checkbox" ng-model="item.FAIR" ng-true-value="1" ng-false-value="0" ng-disabled="!LIWO[0].PTemplateID || item.Live != 0" ng-click="UpdateWO(item)">\n' +
    '                                            </td>\n' +
    '                                            <td class="col-xs-2" ng-click="selectOneItem(LIWO, item, \'highlight\')">\n' +
    '                                                <span class="font-large-bold" ng-bind="item.WONo"></span>\n' +
    '                                            </td>\n' +
    '                                            <td class="col-xs-1 no-padding">\n' +
    '                                                <button type="submit" class="button" ng-show="item.Live == 0" ng-click="LaunchWO(item)" ng-disabled="!LIWO[0].PTemplateID || LIWO[0].PTemplateID.PlanningComplete == 0">Launch</button>\n' +
    '                                                <button type="submit" class="button" ng-hide="item.Live == 0" ng-click="CancelWO(LIWO)">Cancel / Close</button>\n' +
    '                                            </td>\n' +
    '                                        </tr>\n' +
    '                                        <tr my-enter="AddNewWO(NewWO)">\n' +
    '                                            <td class="col-xs-1 no-padding">\n' +
    '                                                <input type="text" ng-model="NewWO.PriceEach" ng-change="selectOneItem(LIWOinfo, LIWO, \'selected\')">\n' +
    '                                            </td>\n' +
    '                                            <td class="col-xs-2">\n' +
    '                                                <input class="width-90-percent" type="text" ng-model="NewWO.DeliveryDate" ng-change="selectOneItem(LIWOinfo, LIWO, \'selected\')">\n' +
    '                                            </td>\n' +
    '                                            <td class="col-xs-2 no-padding">\n' +
    '                                                <input class="pull-left width-48-percent" type="text" ng-model="NewWO.Reqd">\n' +
    '                                                <input class="pull-right width-48-percent" type="text" ng-model="NewWO.Build" ng-change="selectOneItem(LIWOinfo, LIWO, \'selected\')">\n' +
    '                                            </td>\n' +
    '                                            <td class="col-xs-3 no-padding">\n' +
    '                                                <input class="width-90-percent" ng-model="NewWO.ChangeReason" maxlength="30" ng-change="selectOneItem(LIWOinfo, LIWO, \'selected\')">\n' +
    '                                            </td>\n' +
    '                                            <td class="col-xs-1 no-padding">\n' +
    '                                                <input type="checkbox" ng-model="NewWO.FAIR" ng-true-value="1" ng-false-value="0" ng-change="selectOneItem(LIWOinfo, LIWO, \'selected\')">\n' +
    '                                            </td>\n' +
    '                                            <td class="col-xs-2">\n' +
    '                                                <input type="text" class="full-width" ng-model="NewWO.WONo">\n' +
    '                                            </td>\n' +
    '                                            <td class="col-xs-1 no-padding"></td>\n' +
    '                                        </tr>\n' +
    '                                    </table>\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                            <tr my-enter="addNewLineItem(NewLineItem)">\n' +
    '                                <td class="col-xs-1 no-padding">\n' +
    '                                    <span>Cust Ref</span>\n' +
    '                                    <input type="text" maxlength="5" ng-model="NewLineItem.CustRef">\n' +
    '                                    <span>ESL QuNo.</span>\n' +
    '                                    <input type="text" ng-model="NewLineItem.Quoteno" maxlength="8">\n' +
    '                                </td>\n' +
    '                                <td class="col-xs-3">\n' +
    '                                    <select ng-model="NewLineItem.TemplateList" ng-options="option as option.DisplayData for option in TemplateLists"></select>\n' +
    '                                </td>\n' +
    '                                <td class="col-xs-8">\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="total-order">\n' +
    '                <div class="area-1 col-xs-4 col-xs-offset-4">\n' +
    '                    <span class="total">&#8356;{{totalPrice}}</span>\n' +
    '                    <span class="total-title">Totals</span>\n' +
    '                    <span class="sum-quantity" ng-bind="totalReqd"></span>\n' +
    '                    <span class="sum-quantity" ng-bind="totalBuild"></span>\n' +
    '                </div>\n' +
    '                <div class="area-2 col-xs-4 no-padding">\n' +
    '                    <button class="view-order" ng-click="ViewWO(LIWOinfo)">View Work Order</button>\n' +
    '                    <button class="view-acknlgmnt" type="button" ng-click="openBIRT()">View Acknlgmnt</button>\n' +
    '                    <button class="cancel" type="button" ng-click="CancelLineItem(LIWOinfo)">Cancel Lineitem</button>\n' +
    '                </div>\n' +
    '                <!-- Modal -->\n' +
    '                <div modal-show modal-visible="showDialog" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">\n' +
    '                    <div class="modal-dialog modal-lg">\n' +
    '                        <div class="modal-content">\n' +
    '                            <div class="modal-body">\n' +
    '                                <div class="col-xs-8">\n' +
    '                                    <div class="form-group modal-title">\n' +
    '                                        <label for="" class="col-xs-12">Customer Order Lineitem Close / Cancel</label>\n' +
    '                                    </div>\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label for="" class="col-xs-5 col-xs-offset-3">\n' +
    '                                            Communication issue?\n' +
    '                                        </label>\n' +
    '                                        <div class="col-xs-1">\n' +
    '                                            <input type="checkbox" ng-model="LineItem.IssueType" ng-true-value="1">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label for="" class="col-xs-5 col-xs-offset-3">\n' +
    '                                            <strong>Stock</strong> issue?\n' +
    '                                        </label>\n' +
    '                                        <div class="col-xs-1">\n' +
    '                                            <input type="checkbox" ng-model="LineItem.IssueType" ng-true-value="2">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="area-table">\n' +
    '                                        <table>\n' +
    '                                            <tr>\n' +
    '                                                <th>Why Cleared / Cancelled</th>\n' +
    '                                            </tr>\n' +
    '                                            <tr>\n' +
    '                                                <td>\n' +
    '                                                    <input type="text" ng-model="LineItem.CancelReason" maxlength="80" my-enter="ExecCancelLineItem(LineItem)">\n' +
    '                                                </td>\n' +
    '                                            </tr>\n' +
    '                                        </table>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="modal-footer">\n' +
    '                                <button data-dismiss="modal">Close</button>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="area-customer-order">\n' +
    '                <div class="area-1 col-xs-8">\n' +
    '                    <div class="area-table">\n' +
    '                        <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_customerorder">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th>Customer Order Notes</th>\n' +
    '                                    <th>Date</th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="item in CustOrderNotes">\n' +
    '                                    <td ng-bind="item.Note"></td>\n' +
    '                                    <td ng-bind="item.NoteDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                                </tr>\n' +
    '                                <tr>\n' +
    '                                    <td>\n' +
    '                                        <input type="text" ng-model="CoNotes.Note" my-enter="addCoNotes(CoNotes)" maxlength="80">\n' +
    '                                    </td>\n' +
    '                                    <td ng-bind="item.NoteDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                    <div class="area-table">\n' +
    '                        <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_acknowledgement">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th>Acknowledgement Note</th>\n' +
    '                                    <th>Date</th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="item in AcknowNotes">\n' +
    '                                    <td ng-bind="item.Note"></td>\n' +
    '                                    <td></td>\n' +
    '                                </tr>\n' +
    '                                <tr>\n' +
    '                                    <td>\n' +
    '                                        <input type="text" ng-model="ACNote.Note" my-enter="addACNote(ACNote)" maxlength="80">\n' +
    '                                    </td>\n' +
    '                                    <td></td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="area-2 col-xs-4 no-padding">\n' +
    '                    <div class="area-commercially">\n' +
    '                        <div class="row">\n' +
    '                            <label for="" class="col-xs-5 col-xs-offset-1 no-padding">&nbsp;</label>\n' +
    '                            <span class="col-xs-1">Yes</span>\n' +
    '                            <span class="col-xs-1">No</span>\n' +
    '                            <div class="col-xs-4"></div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <label for="" class="col-xs-5 col-xs-offset-1 no-padding">Commercially viable?</label>\n' +
    '                            <div class="col-xs-1">\n' +
    '                                <input type="radio" ng-model="CustomerOrder.CommerciallyViable" ng-value="1" ng-change="NewCO.setCommerciallyViable(1); UpdateCO(selectCO, CustomerOrder, ReleaseType, DeliveryPoint)">\n' +
    '                            </div>\n' +
    '                            <div class="col-xs-1">\n' +
    '                                <input type="radio" ng-model="CustomerOrder.CommerciallyViable" ng-value="0" ng-change="NewCO.setCommerciallyViable(0); UpdateCO(selectCO, CustomerOrder, ReleaseType, DeliveryPoint)">\n' +
    '                            </div>\n' +
    '                            <div class="col-xs-4"></div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="area-amendment">\n' +
    '                        <div class="row">\n' +
    '                            <label for="" class="col-xs-5 col-xs-offset-1 no-padding">&nbsp;</label>\n' +
    '                            <span class="col-xs-1">Yes</span>\n' +
    '                            <span class="col-xs-1">No</span>\n' +
    '                            <div class="col-xs-4"></div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <label for="" class="col-xs-5 col-xs-offset-1 no-padding">Amendment reviewed?</label>\n' +
    '                            <div class="col-xs-1">\n' +
    '                                <input type="radio" ng-model="CustomerOrder.AmendmentReviewed" ng-value="1" ng-change="NewCO.setAmendmentReviewed(1); UpdateCO(selectCO, CustomerOrder, ReleaseType, DeliveryPoint)">\n' +
    '                            </div>\n' +
    '                            <div class="col-xs-1">\n' +
    '                                <input type="radio" ng-model="CustomerOrder.AmendmentReviewed" ng-value="0" ng-change="NewCO.setAmendmentReviewed(0); UpdateCO(selectCO, CustomerOrder, ReleaseType, DeliveryPoint)">\n' +
    '                            </div>\n' +
    '                            <div class="col-xs-4"></div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="area-button">\n' +
    '                        <button class="back" type="button" ng-click="redirectBack()">Back</button>\n' +
    '                        <button class="part-control" type="button" ng-click="redirectPartControl()">Part Control</button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/delivery-despatch/delivery-despatch.tpl.html',
    '<div class="delivery-despatch-page">\n' +
    '    <div class="page-body">\n' +
    '        <div class="title-global-page">Delivery Despatch</div>\n' +
    '        <div class="col-md-6 no-padding">\n' +
    '            <div class="work-order-table">\n' +
    '                <div class="table-title">Completed Work Orders</div>\n' +
    '                <div class="area-table">\n' +
    '                    <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_wono">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th>WO No.</th>\n' +
    '                                <th>Customer</th>\n' +
    '                                <th>Qty</th>\n' +
    '                                <th>GRB No.</th>\n' +
    '                                <th>Locn</th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="item in DispatchList" ng-click="selectOneItem(DispatchList, item); GetDispatchDetail(DispatchList, undefined)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                <td ng-bind="item.WorkOrderID"></td>\n' +
    '                                <td ng-bind="item.Customer"></td>\n' +
    '                                <td ng-bind="item.Qty"></td>\n' +
    '                                <td ng-bind="item.GRBNo"></td>\n' +
    '                                <td ng-bind="item.Location"></td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="treatment-table">\n' +
    '                <div class="table-title">Parts for Treatment</div>\n' +
    '                <div class="area-table">\n' +
    '                    <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_pono">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th>PO No.</th>\n' +
    '                                <th>Supplier</th>\n' +
    '                                <th>GRB No.</th>\n' +
    '                                <th>Qty</th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="item in TreatmentDispatchList" ng-click="selectOneItem(TreatmentDispatchList, item); GetDispatchDetail(undefined, TreatmentDispatchList)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                <td ng-bind="item.POnID"></td>\n' +
    '                                <td ng-bind="item.Supplier"></td>\n' +
    '                                <td ng-bind="item.GRBNo"></td>\n' +
    '                                <td ng-bind="item.Qty"></td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-md-6 no-padding">\n' +
    '            <div class="row">\n' +
    '                <div class="col-md-7 col-md-offset-5">\n' +
    '                    <div class="row">\n' +
    '                        <div class="description col-md-12">\n' +
    '                            <label for="" class="col-md-12">Description</label>\n' +
    '                            <div class="col-md-12 description-group">\n' +
    '                                <input type="text" readonly ng-model="DispatchDetail.Description">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="delivery-address col-md-12">\n' +
    '                            <label for="" class="col-md-12">Delivery Address</label>\n' +
    '                            <div class="delivery-address-group col-md-12">\n' +
    '                                <input type="text" readonly ng-model="DispatchDetail.Name">\n' +
    '                                <input type="text">\n' +
    '                                <input type="text" readonly ng-model="DispatchDetail.Address1">\n' +
    '                                <input type="text" readonly ng-model="DispatchDetail.Address2">\n' +
    '                                <input type="text" readonly ng-model="DispatchDetail.Address3">\n' +
    '                                <input type="text" readonly ng-model="DispatchDetail.Address4">\n' +
    '                                <input type="text" readonly ng-model="DispatchDetail.Postcode">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="special-instructions col-md-12">\n' +
    '                            <label for="" class="col-md-12">Special instructions</label>\n' +
    '                            <div class="special-instructions-group col-md-12">\n' +
    '                                <textarea readonly rows="3" ng-model="DispatchDetail.SpecialInstruction"></textarea>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="col-md-5">\n' +
    '                    <div class="row">\n' +
    '                        <div class="carriage-type col-md-12">\n' +
    '                            <label for="" class="col-md-6">Carriage type</label>\n' +
    '                            <select class="col-md-6" ng-model="Carriage" ng-options="option as option.name for option in CarriageList">\n' +
    '                            </select>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="courier col-md-12">\n' +
    '                            <label for="" class="col-md-6">Courier</label>\n' +
    '                            <select class="col-md-6" ng-model="Courier" ng-options="option as option.Name for option in CourierList">\n' +
    '                            </select>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="col-md-12 delivery-button">\n' +
    '                    <button class="back" ng-click="redirectBack()">Back</button>\n' +
    '                    <button class="despatched" ng-click="Dispatched()">Mark as Despatched</button>\n' +
    '                    <button class="print-docs" type="button" ng-click="printDocs()">Print Docs</button>\n' +
    '                    <button class="booked" ng-click="CourierBooked(Carriage, Courier)">Mark as Booked</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/engineering-template/engineering-template.tpl.html',
    '<div class="row" id="engineering-template">\n' +
    '    <div class="container">\n' +
    '        <div class="col-md-3 box-sidebar row no-padding operation-area">\n' +
    '            <h1 class="color-blue">Part No. {{TemplateDetail.PartNumber}}</h1>\n' +
    '            <h2>Issue {{TemplateDetail.IssueNumber}}</h2>\n' +
    '            <h2 ng-show="WorkOrderID">WO {{WorkOrderID}}</h2>\n' +
    '            <div class="col-md-12 button-view">\n' +
    '                <button type="submit" class="text-center pull-right unco">Overview</button>\n' +
    '            </div>\n' +
    '            <div class="area-table table-previous col-md-12">\n' +
    '                <table class="table-operation" andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_operation">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th class="text-center no-padding">Operation</th>\n' +
    '                            <th class="text-center no-padding">Set</th>\n' +
    '                            <th class="text-center no-padding">Run</th>\n' +
    '                            <th class="text-center no-padding">L</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in ListTemplateOps" ng-click="selectOneItem(ListTemplateOps, item, \'selected\'); selectOperation(item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                            <td class="text-center blue-bg-black-cl no-padding" ng-bind="item.Operation"></td>\n' +
    '                            <td class="text-center blue-bg-black-cl no-padding" ng-bind="item.EstimatedSetTime"></td>\n' +
    '                            <td class="text-center blue-bg-black-cl no-padding" ng-bind="item.EstimatedRunTime"></td>\n' +
    '                            <td class="text-center blue-bg-black-cl no-padding">\n' +
    '                                <input type="radio" ng-model="item.Linked" ng-value="1" disabled ng-show="item.OpTypeID == 2">\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                        <tr ng-show="AreaIcon">\n' +
    '                            <td colspan="4">\n' +
    '                                <img class="material-icon" ng-click="showIcon(\'material\')">\n' +
    '                                <img class="machine-icon" ng-click="showIcon(\'machine\')">\n' +
    '                                <img class="subcon-icon" ng-click="showIcon(\'subcon\')">\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                        <tr ng-show="MaterialDropDown">\n' +
    '                            <td class="text-center">\n' +
    '                                <span>Material issue</span>\n' +
    '                            </td>\n' +
    '                            <td class="text-center">\n' +
    '                                <input class="text-center" type="text" ng-model="TemplateOp.SetTime" maxlength="9">\n' +
    '                            </td>\n' +
    '                            <td class="text-center">\n' +
    '                                <input class="text-center" type="text" ng-model="TemplateOp.RunTime" maxlength="9">\n' +
    '                            </td>\n' +
    '                            <td class="text-center">\n' +
    '                                <input class="text-center" type="radio" ng-model="TemplateOp.Linked" ng-value="1">\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                        <tr ng-show="MachineDropDown">\n' +
    '                            <td class="text-center">\n' +
    '                                <select class="input-full" ng-model="TemplateOp.MachiningID" ng-options="option as option.Description for option in MachiningList"></select>\n' +
    '                            </td>\n' +
    '                            <td class="text-center">\n' +
    '                                <input class="text-center" type="text" ng-model="TemplateOp.SetTime" maxlength="9">\n' +
    '                            </td>\n' +
    '                            <td class="text-center">\n' +
    '                                <input class="text-center" type="text" ng-model="TemplateOp.RunTime" maxlength="9">\n' +
    '                            </td>\n' +
    '                            <td class="text-center">\n' +
    '                                <input class="text-center" type="radio" ng-model="TemplateOp.Linked" ng-value="1">\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                        <tr ng-show="SubconDropDown">\n' +
    '                            <td class="text-center">\n' +
    '                                <select class="input-full" ng-model="TemplateOp.TreatmentID" ng-options="option as option.Description for option in SubconList"></select>\n' +
    '                            </td>\n' +
    '                            <td class="text-center">\n' +
    '                                <input class="text-center" type="text" ng-model="TemplateOp.SetTime" maxlength="9">\n' +
    '                            </td>\n' +
    '                            <td class="text-center">\n' +
    '                                <input class="text-center" type="text" ng-model="TemplateOp.RunTime" maxlength="9">\n' +
    '                            </td>\n' +
    '                            <td class="text-center">\n' +
    '                                <input class="text-center" type="radio" ng-model="TemplateOp.Linked" ng-value="1">\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '                <table class="table-partTotal">\n' +
    '                    <tbody>\n' +
    '                        <tr class="total">\n' +
    '                            <td class="text-center no-padding">Part Total:</td>\n' +
    '                            <td class="text-center no-padding" ng-bind="PartTotalSet"></td>\n' +
    '                            <td class="text-center no-padding" ng-bind="PartTotalRun"></td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '                <div class="button-end">\n' +
    '                    <button type="button" class="text-center pull-right" ng-click="RmvTemplateOp(ListTemplateOps)">Rmv Op</button>\n' +
    '                    <button type="button" class="text-center pull-right" ng-click="AddTemplateOp(TemplateOp)">Insert</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-md-9 tab-area">\n' +
    '            <div id="box-content">\n' +
    '                <div class="list-tabs">\n' +
    '                    <ul class="tabs">\n' +
    '                        <li><a ui-sref-active="active" ui-sref="department.engineering-template.material">Material Requirement</a></li>\n' +
    '                        <!-- <li><a ui-sref-active="active" ui-sref="department.engineering-template.pering">P/E Engineering</a></li> -->\n' +
    '                        <li><a ui-sref-active="active" ui-sref="department.engineering-template.engineering">Engineering</a></li>\n' +
    '                        <li><a ui-sref-active="active" ui-sref="department.engineering-template.subcon">Sub-con</a></li>\n' +
    '                        <li><a ui-sref-active="active" ui-sref="department.engineering-template.review">Review</a></li>\n' +
    '                        <li><a ui-sref-active="active" ui-sref="department.engineering-template.overview">Overview</a></li>\n' +
    '                    </ul>\n' +
    '                </div>\n' +
    '                <div class="containerr">\n' +
    '                    <!-- Box-material-requirement -->\n' +
    '                    <div ui-view></div>\n' +
    '                </div>\n' +
    '                <div class="row box-end col-xs-12 no-padding">\n' +
    '                    <div class="col-md-4">\n' +
    '                        <p>Engineering Issue: {{TemplateDetail.EngIssue}}</p>\n' +
    '                    </div>\n' +
    '                    <div class="col-md-4">\n' +
    '                        <p><strong>Planning complete </strong>\n' +
    '                            <input type="radio" ng-model="TemplateDetail.PlanningComplete" ng-value="1" ng-click="UpdateTemplateDetail(TemplateDetail.PlanningComplete)">\n' +
    '                        </p>\n' +
    '                    </div>\n' +
    '                    <div class="col-md-4 no-padding">\n' +
    '                        <button type="button" ng-click="redirectBack()" class="text-center pull-right">Back</button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <img class="img-responsive" ng-src="{{showImage}}">\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/finance-admin/finance-admin.tpl.html',
    '<div class="finance-admin">\n' +
    '    <div class="page-body">\n' +
    '        <div class="row">\n' +
    '            <div class="page-content col-xs-12">\n' +
    '                <div class="col-xs-6 no-padding">\n' +
    '                    <div class="table-website col-xs-10 no-padding">\n' +
    '                        <div class="table-title">Website access</div>\n' +
    '                        <div class="area-table">\n' +
    '                            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_website">\n' +
    '                                <thead>\n' +
    '                                    <tr>\n' +
    '                                        <th>Full site address</th>\n' +
    '                                        <th>Username</th>\n' +
    '                                        <th>Password</th>\n' +
    '                                    </tr>\n' +
    '                                </thead>\n' +
    '                                <tbody>\n' +
    '                                    <tr ng-repeat="item in Websites" ng-click="selectItem(item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                        <td>{{item.Website}}</td>\n' +
    '                                        <td>{{item.UserName}}</td>\n' +
    '                                        <td>{{item.SitePassword}}</td>\n' +
    '                                    </tr>\n' +
    '                                    <tr>\n' +
    '                                        <td>\n' +
    '                                            <input type="text" ng-model="website.Website" maxlength="45">\n' +
    '                                        </td>\n' +
    '                                        <td>\n' +
    '                                            <input type="text" ng-model="website.UserName" maxlength="25">\n' +
    '                                        </td>\n' +
    '                                        <td>\n' +
    '                                            <input type="text" ng-model="website.SitePassword" maxlength="15">\n' +
    '                                        </td>\n' +
    '                                    </tr>\n' +
    '                                </tbody>\n' +
    '                            </table>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="table-control col-xs-2">\n' +
    '                        <div class="table-control-title">&nbsp;</div>\n' +
    '                        <button class="add" type="button" ng-click="AddWebsite(website)">Add</button>\n' +
    '                        <button class="delete" ng-click="DeleteWebsite(Websites)">Delete</button>\n' +
    '                    </div>\n' +
    '                    <div class="vat-charge col-xs-12">\n' +
    '                        <div class="vat-rate col-xs-8">\n' +
    '                            <label for="">VAT rate</label>\n' +
    '                            <input type="text" ng-model="FinanceAdmin.VATRate" class="input-width-short">%\n' +
    '                        </div>\n' +
    '                        <div class="fair-charge col-xs-4">\n' +
    '                            <label for="" class="col-xs-8 no-padding">FAIR charge &#8356;</label>\n' +
    '                            <input type="text" class="input-width-short" ng-model="FinanceAdmin.FAIRCharge">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="exchange-rates-get-rates col-xs-12 no-padding">\n' +
    '                        <div class="exchange-rates col-xs-10">\n' +
    '                            <div class="exchange-title col-xs-12">Exchange rates:</div>\n' +
    '                            <div class="exchange-body col-xs-12 no-padding">\n' +
    '                                <div class="us-currency col-xs-3 col-xs-offset-1 no-padding">\n' +
    '                                    <div class="us-currency-group col-xs-6 no-padding content-right">\n' +
    '                                        <input type="text" class="input-width-short" ng-model="FinanceAdmin.USD" maxlength="8">\n' +
    '                                    </div>\n' +
    '                                    <label for="" class="col-xs-6 no-padding">US Dollars</label>\n' +
    '                                </div>\n' +
    '                                <div class="euro-currency col-xs-4">\n' +
    '                                    <label for="" class="col-xs-6 no-padding content-right">Euros</label>\n' +
    '                                    <div class="euro-currency-group col-xs-6">\n' +
    '                                        <input type="text" class="input-width-short" ng-model="FinanceAdmin.Euros" maxlength="8">\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="irish-currency col-xs-4">\n' +
    '                                    <label for="" class="col-xs-6 no-padding content-right">Irish pounds</label>\n' +
    '                                    <div class="irish-currency-group col-xs-6">\n' +
    '                                        <input type="text" class="input-width-short" ng-model="FinanceAdmin.IrishPounds" maxlength="8">\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-xs-6 no-padding-right">\n' +
    '                    <div class="form-address-branch col-xs-12 no-padding">\n' +
    '                        <form name="BranchForm" ng-submit="BranchForm.$valid && AddAddBranch(addBranch)" novalidate>\n' +
    '                            <div class="form-address col-xs-9 no-padding">\n' +
    '                                <div class="form-element">\n' +
    '                                    <div class="form-content col-xs-10 no-padding">\n' +
    '                                        <div class="element name col-xs-12 no-padding">\n' +
    '                                            <label for="" class="col-xs-4 no-padding-right">Name</label>\n' +
    '                                            <div class="name-group col-xs-8 no-padding">\n' +
    '                                                <input type="text" name="BranchName" ng-model="addBranch.BranchName" maxlength="25" required>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="BranchForm.$submitted && BranchForm.BranchName.$error.required">Please enter Name</span>\n' +
    '                                        </div>\n' +
    '                                        <div class="element address1 col-xs-12 no-padding">\n' +
    '                                            <label for="" class="col-xs-4 no-padding-right">Address 1</label>\n' +
    '                                            <div class="address1-group col-xs-8 no-padding">\n' +
    '                                                <input type="text" ng-model="addBranch.Address1" maxlength="45" required name="Address1">\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="BranchForm.$submitted && BranchForm.Address1.$error.required">Please enter Address 1</span>\n' +
    '                                        </div>\n' +
    '                                        <div class="element address2 col-xs-12 no-padding">\n' +
    '                                            <label for="" class="col-xs-4 no-padding-right">Address 2</label>\n' +
    '                                            <div class="address2-group col-xs-8 no-padding">\n' +
    '                                                <input type="text" ng-model="addBranch.Address2" maxlength="40" required name="Address2">\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="BranchForm.$submitted && BranchForm.Address2.$error.required">Please enter Address 2</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="form-button no-padding col-xs-2">\n' +
    '                                        <div class="add">\n' +
    '                                            <button type="submit">Add</button>\n' +
    '                                        </div>\n' +
    '                                        <div class="delete">\n' +
    '                                            <button type="button" ng-click="DeleteBranch(Branches)">Delete</button>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="form-element">\n' +
    '                                    <div class="form-content col-xs-12 no-padding">\n' +
    '                                        <div class="element address3 col-xs-12 no-padding">\n' +
    '                                            <label for="" class="col-xs-4 no-padding-right">Address 3</label>\n' +
    '                                            <div class="address3-group col-xs-8 no-padding">\n' +
    '                                                <input type="text" ng-model="addBranch.Address3" maxlength="30" name="Address3">\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                        <div class="element address4 col-xs-12 no-padding">\n' +
    '                                            <label for="" class="col-xs-4 no-padding-right">Address 4</label>\n' +
    '                                            <div class="address4-group col-xs-8 no-padding">\n' +
    '                                                <input type="text" ng-model="addBranch.Address4">\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                        <div class="element telephone col-xs-5 col-xs-offset-7 no-padding">\n' +
    '                                            <label class="col-xs-6 no-padding-right" for="">Telephone</label>\n' +
    '                                            <div class="telephone-group col-xs-6 no-padding">\n' +
    '                                                <input type="text" ng-model="addBranch.Phone" name="Phone" maxlength="20" required ng-pattern="/^0|[1-9][0-9]*$/">\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                        <div class="error col-xs-5 col-xs-offset-7 no-padding">\n' +
    '                                            <span ng-show="BranchForm.$submitted && BranchForm.Phone.$error.required">Please enter Telephone</span>\n' +
    '                                            <span ng-show="BranchForm.$submitted && BranchForm.Phone.$error.pattern">Telephone must be number</span>\n' +
    '                                        </div>\n' +
    '                                        <div class="element postcode col-xs-12 no-padding">\n' +
    '                                            <label for="" class="col-xs-4 no-padding-right">Postcode</label>\n' +
    '                                            <div class="postcode-group col-xs-2 no-padding">\n' +
    '                                                <input type="text" ng-model="addBranch.Postcode" maxlength="8" name="Postcode" required>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="BranchForm.$submitted && BranchForm.Postcode.$error.required">Please enter Postcode</span>\n' +
    '                                        </div>\n' +
    '                                        <div class="element facsimile col-xs-5 col-xs-offset-7 no-padding">\n' +
    '                                            <label for="" class="col-xs-6 no-padding-right">Facsimile</label>\n' +
    '                                            <div class="facsimile-group col-xs-6 no-padding">\n' +
    '                                                <input type="text" ng-model="addBranch.Fax" maxlength="20" name="Fax" required ng-pattern="/^0|[1-9][0-9]*$/">\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                        <div class="error col-xs-5 col-xs-offset-7 no-padding">\n' +
    '                                            <span ng-show="BranchForm.$submitted && BranchForm.Fax.$error.required">Please enter Facsimile</span>\n' +
    '                                            <span ng-show="BranchForm.$submitted && BranchForm.Fax.$error.pattern">Facsimile must be number</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="branch col-xs-3 no-padding-right">\n' +
    '                                <div class="table-title">Branches</div>\n' +
    '                                <div class="area-table">\n' +
    '                                    <table ng-show="Branches | hasItem">\n' +
    '                                        <tr ng-repeat="Branche in Branches" ng-click="selectItem(Branche, \'selected\')">\n' +
    '                                            <td>{{Branche.BranchName}} <span ng-show="Branche.selected == true" class="pull-right glyphicon glyphicon-ok"></span></td>\n' +
    '                                        </tr>\n' +
    '                                    </table>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </form>\n' +
    '                    </div>\n' +
    '                    <div class="material-markup col-xs-12">\n' +
    '                        <label for="">Material markup</label>\n' +
    '                        <input type="text" ng-model="FinanceAdmin\n' +
    '.MaterialMarkup" class="input-width-short">%\n' +
    '                    </div>\n' +
    '                    <div class="user-role-back col-xs-12 no-padding">\n' +
    '                        <button class="get-rates" ng-click="getRates\n' +
    '()">Get Rates</button>\n' +
    '                        <button class="user-roles">User Roles</button>\n' +
    '                        <button class="update" type="button" ng-click="UpdateFinanceAdmin(FinanceAdmin)">Update</button>\n' +
    '                        <button class="back" ng-click="redirectBack()">Back</button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/goods-in/goods-in.tpl.html',
    '<div class="goods-in-page">\n' +
    '    <div class="title-global-page">Goods in</div>\n' +
    '    <div class="page-top">\n' +
    '        <div class="search col-xs-2">\n' +
    '            <strong>Search: </strong>\n' +
    '            <div class="grb-no">\n' +
    '                <span class="grb-title">GRB No.</span>\n' +
    '                <input class="grb-value" type="text" ng-model="GRBNo">\n' +
    '            </div>\n' +
    '            <div class="material">\n' +
    '                <span class="material-title">Material</span>\n' +
    '                <input type="text" class="material-value" ng-model="Material" maxlength="6">\n' +
    '            </div>\n' +
    '            <div class="size">\n' +
    '                <span class="size-title">Size</span>\n' +
    '                <input type="text" class="size-value" ng-model="Size">\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="instock-table col-xs-6">\n' +
    '            <div class="table-control">\n' +
    '                <span class="up-control" ng-click="reverse = false"></span>\n' +
    '                <span>sort</span>\n' +
    '                <span class="down-control" ng-click="reverse = true"></span>\n' +
    '            </div>\n' +
    '            <div class="table-title">in stock</div>\n' +
    '            <div class="area-table">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_instock" load-item="MaterialInStock">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th ng-repeat="item in sortTable" ng-bind="item.title" ng-click="selectOneItem(sortTable, item, \'selected\'); changeSortBy(item)" ng-class="{\'th-active\': item.selected}"></th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in MaterialInStock | orderBy: sortBy : reverse | contain:\'GRBNo\':GRBNo | contain: \'Spec\' : Material | contain: \'Size\' : Size | unique:\'GRBNo\'" ng-click="selectOneItem(MaterialInStock, item); selectMaterialInStock(item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                            <td ng-bind="item.GRBNo"></td>\n' +
    '                            <td ng-bind="item.Spec"></td>\n' +
    '                            <td ng-bind="item.Size"></td>\n' +
    '                            <td ng-bind="item.Units"></td>\n' +
    '                            <td ng-bind="item.Shape"></td>\n' +
    '                            <td ng-bind="item.ReleaseLevel"></td>\n' +
    '                            <td ng-bind="item.PurchaseLength"></td>\n' +
    '                            <td ng-bind="item.DateIn | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                            <td ng-bind="item.OrderQty"></td>\n' +
    '                            <td ng-bind="item.InStock"></td>\n' +
    '                            <td ng-bind="item.Location"></td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="issued-table col-xs-4">\n' +
    '            <div class="table-title">issued</div>\n' +
    '            <div class="area-table">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_date">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th>Date</th>\n' +
    '                            <th>Part No.</th>\n' +
    '                            <th>Qty</th>\n' +
    '                            <th>WO</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-model="test" ng-repeat="item in MaterialIssues" ng-click="selectOneItem(MaterialIssues, item); selectMaterialIssue(item)">\n' +
    '                            <td ng-bind="item.DateIssued | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                            <td>\n' +
    '                                <a href="#" ng-bind="item.PartNumber"></a>\n' +
    '                            </td>\n' +
    '                            <td ng-bind="item.Qty"></td>\n' +
    '                            <td>\n' +
    '                                <a href="#" ng-bind="item.WONo"></a>\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="location">\n' +
    '            <form name="LocationForm" ng-submit="LocationForm.$valid && updateLocation(LocationInfo)" novalidate>\n' +
    '                <span class="location-title">Location</span>\n' +
    '                <select ng-model="LocationInfo.Location" ng-options="option as option.StoreArea for option in StoreAreaList" ng-change="changeLocation(LocationInfo.Location); GRB.setStoreAreaID(LocationInfo.Location.StoreAreaID)" name="Location" required>\n' +
    '                </select>\n' +
    '                <select ng-model="LocationInfo.XLocation" ng-options="option as option.XLocation for option in XStoreLocations" ng-change="GRB.setXLocation(LocationInfo.XLocation.XLocation)" name="XLocation" required>\n' +
    '                </select>\n' +
    '                <select ng-model="LocationInfo.YLocation" ng-options="option as option.YLocation for option in YStoreLocations" ng-change="GRB.setYLocation(LocationInfo.YLocation.YLocation)" name="YLocation" required>\n' +
    '                </select>\n' +
    '                <button type="submit">Update Loc\'n</button>\n' +
    '                <div class="error">\n' +
    '                    <p ng-show="LocationForm.$submitted && LocationForm.Location.$error.required">Please select Location</p>\n' +
    '                    <p ng-show="LocationForm.$submitted && LocationForm.XLocation.$error.required">Please select XLocation</p>\n' +
    '                    <p ng-show="LocationForm.$submitted && LocationForm.YLocation.$error.required">Please select YLocation</p>\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="page-body">\n' +
    '        <div class="page-body-title">Receive:</div>\n' +
    '        <div class="page-body-content">\n' +
    '            <form name="GRBForm" ng-submit="GRBForm.$valid && AddGRB(GRB)" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-9 receive-left">\n' +
    '                        <div class="row">\n' +
    '                            <div class="form-group">\n' +
    '                                <div class="col-xs-2 rcvd">\n' +
    '                                    <div class="title">Rcvd Date\n' +
    '                                        <p class="leave-blank"><small>leave blank for today</small></p>\n' +
    '                                    </div>\n' +
    '                                    <input type="text" ng-model="GRB.RcvdDate">\n' +
    '                                </div>\n' +
    '                                <div class="col-xs-2 next-gr">\n' +
    '                                    <button ng-click="generateNextGR()" type="button">Generate next GR</button>\n' +
    '                                </div>\n' +
    '                                <div class="col-xs-4 grb">\n' +
    '                                    <span class="title">GRB:</span>\n' +
    '                                    <input type="text" readonly ng-model="GRB.nextGR">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row" ng-show="GRType.MaterialType == 1">\n' +
    '                            <div class="form-group">\n' +
    '                                <div class="rejects col-xs-3 col-xs-offset-9">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="rejects-content">\n' +
    '                                            <span class="col-xs-4 no-padding">Rejects</span>\n' +
    '                                            <div class="col-xs-8">\n' +
    '                                                <select ng-model="Reject" ng-options="option as option.RejectNoteID for option in RejectsList" ng-change="GRB.setRejectNoteID(Reject.RejectNoteID)"></select>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="form-group">\n' +
    '                                <div class="gr-type col-xs-10">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="gr-type-content">\n' +
    '                                            <label for="" class="col-xs-1 no-padding">GR type</label>\n' +
    '                                            <div class="col-xs-3">\n' +
    '                                                <select ng-model="GRType" ng-options="option as option.Type for option in MaterialTypeList" ng-change="changeGRType(GRType); GRB.setMaterialTypeID(GRType.MaterialType)" required name="GRType">\n' +
    '                                                </select>\n' +
    '                                            </div>\n' +
    '                                            <div class="col-xs-6 gr-type-content-group" ng-show="GRType.MaterialType == 8">\n' +
    '                                                <select ng-model="FIMaterial" ng-options="option as option.DisplayData for option in PartTemplateList" ng-change="changeFIMaterial(FIMaterial); GRB.setETemplateID(FIMaterial.ETemplateID)">\n' +
    '                                                </select>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row error">\n' +
    '                                        <p ng-show="GRBForm.$submitted && GRBForm.GRType.$error.required">Please select GR type</p>\n' +
    '                                    </div>\n' +
    '                                    <div class="row" ng-show="GRType.MaterialType == 8">\n' +
    '                                        <div class="customer-order col-xs-12 no-padding">\n' +
    '                                            <label for="" class="col-xs-4 no-padding content-right">Customer Order No.</label>\n' +
    '                                            <div class="customer-order-group col-xs-3">\n' +
    '                                                <select ng-model="CustomerOrder" ng-options="option as option.CustomerOrderRef for option in MatchingCOList" ng-change="changeCustomerOrder(CustomerOrder); GRB.setOrderID(CustomerOrder.OrderID)">\n' +
    '                                                </select>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                        <div class="work-order col-xs-12 no-padding">\n' +
    '                                            <label for="" class="col-xs-4 content-right no-padding">Work Order No.</label>\n' +
    '                                            <div class="work-order-group col-xs-2">\n' +
    '                                                <select ng-model="LineItem" ng-options="option as option.WorkOrderID for option in MatchingLineItems" ng-change="GRB.setLineItemID(LineItem.LineItemID)">\n' +
    '                                                </select>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="col-xs-2 gr-type-button" ng-show="isFastTrack">\n' +
    '                                    <button>Fast Track</button>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row" ng-hide="GRType.MaterialType == 7 || GRType.MaterialType == 8">\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="" class="col-xs-2 no-padding po-number">PO number</label>\n' +
    '                                <div class="col-xs-1 po-number-group">\n' +
    '                                    <select ng-model="PONumber" ng-options="option as option.POnID for option in POList" ng-change="changePOList(PONumber); GRB.setPOnID(PONumber.POnID)">\n' +
    '                                    </select>\n' +
    '                                </div>\n' +
    '                                <label for="" class="col-xs-1 item">Item</label>\n' +
    '                                <div class="item-group col-xs-3">\n' +
    '                                    <select ng-model="POItem" ng-options="option as option.Description for option in POItemsList" ng-change="changePOItem(POItem); GRB.setItemID(POItem.ItemID)">\n' +
    '                                    </select>\n' +
    '                                </div>\n' +
    '                                <label for="" class="col-xs-2 no-padding order-by" ng-hide="GRType.MaterialType == 1">Ordered by</label>\n' +
    '                                <div class="order-by-group col-xs-1" ng-hide="GRType.MaterialType == 1">\n' +
    '                                    <input type="text" ng-model="PONumber.OrderedBy">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-3 receive-right">\n' +
    '                        <div class="col-xs-4 receive-right-content">\n' +
    '                            <div class="dn">\n' +
    '                                <input type="radio" ng-model="DocTypeID" ng-value="1"> DN\n' +
    '                            </div>\n' +
    '                            <div class="CoC">\n' +
    '                                <input type="radio" ng-model="DocTypeID" ng-value="2"> CoC\n' +
    '                            </div>\n' +
    '                            <div class="inv">\n' +
    '                                <input type="radio" ng-model="DocTypeID" ng-value="3"> Inv\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="col-xs-8 drop-button">\n' +
    '                            <button type="button" dropzone on-success="uploadSuccess" before-upload="beforeUpload" max-file-name="30">Drop Delivery Docs here</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row" ng-if="GRType.MaterialType != 8">\n' +
    '                    <div class="form-group">\n' +
    '                        <div class="work-order col-xs-3 col-xs-offset-6">\n' +
    '                            <label for="" class="col-xs-6 no-padding">Work Order No.</label>\n' +
    '                            <div class="work-order-group col-xs-6">\n' +
    '                                <input type="text" ng-model="GRB.WorkOrderNo" ng-disabled="GRType.MaterialType == 1">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="release-note col-xs-3">\n' +
    '                            <label for="" class="col-xs-6 no-padding">Release Note No.</label>\n' +
    '                            <div class="release-note-group col-xs-6">\n' +
    '                                <input type="text" name="ReleaseNoteNo" ng-model="GRB.ReleaseNoteNo" maxlength="15">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row" ng-hide="GRType.MaterialType == 1 || GRType.MaterialType == 7 || GRType.MaterialType == 8">\n' +
    '                    <div class="form-group">\n' +
    '                        <div class="supplier">\n' +
    '                            <label for="" class="col-xs-2 no-padding">Supplier / Customer</label>\n' +
    '                            <div class="supplier-group col-xs-1">\n' +
    '                                <select ng-model="CustomerSupplier" ng-options="option as option.Alias for option in CustomerSupplierList" ng-change="GRB.setID(CustomerSupplier.ID); GRB.setType(CustomerSupplier.Type)">\n' +
    '                                </select>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row" ng-show="GRType.MaterialType == 8">\n' +
    '                    <div class="form-group">\n' +
    '                        <div class="customer">\n' +
    '                            <label for="" class="col-xs-2 no-padding">Customer</label>\n' +
    '                            <div class="customer-group col-xs-3">\n' +
    '                                <input type="text" ng-model="GRB.Customer">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row" ng-show="GRType.MaterialType == 1">\n' +
    '                    <div class="form-group">\n' +
    '                        <div class="supplier">\n' +
    '                            <label for="" class="col-xs-2 no-padding">Supplier</label>\n' +
    '                            <div class="supplier-group col-xs-3">\n' +
    '                                <input type="text" ng-model="GRB.SupplierName" ng-disabled="GRType.MaterialType == 1">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row" ng-hide="GRType.MaterialType == 7 || GRType.MaterialType == 8">\n' +
    '                    <div class="form-group">\n' +
    '                        <div class="description">\n' +
    '                            <label for="" class="col-xs-2 no-padding">Description</label>\n' +
    '                            <div class="description-group col-xs-6">\n' +
    '                                <textarea ng-model="GRB.Description" ng-disabled="GRType.MaterialType == 1" maxlength="80"></textarea>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row" ng-if="GRType.MaterialType != 8">\n' +
    '                    <div class="form-group">\n' +
    '                        <div class="delivery-note col-xs-3 col-xs-offset-9">\n' +
    '                            <label for="" class="col-xs-6 no-padding">Delivery Note No.</label>\n' +
    '                            <div class="delivery-note-group col-xs-6">\n' +
    '                                <input type="text" ng-model="GRB.DeliveryNoteNo" maxlength="20" name="DeliveryNoteNo">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="form-group">\n' +
    '                        <div class="qty-receive col-xs-3 no-padding">\n' +
    '                            <label for="" class="col-xs-8 no-padding">Qty Received</label>\n' +
    '                            <div class="qty-receive-group col-xs-4">\n' +
    '                                <input type="text" ng-model="GRB.QtyRcvd" maxlength="8" name="QtyRcvd" ng-pattern="/^\\d+$/">\n' +
    '                            </div>\n' +
    '                            <div class="error">\n' +
    '                                <p ng-show="GRBForm.$submitted && GRBForm.QtyRcvd.$error.pattern">Qty Received must be number</p>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="col-xs-4 uom">\n' +
    '                            <div class="col-xs-3 no-padding uom-title" ng-if="GRType.MaterialType == 1">\n' +
    '                                <div class="uom-p col-xs-6 no-padding">\n' +
    '                                    <p>UOM</p>\n' +
    '                                </div>\n' +
    '                                <div class="uom-input col-xs-6 no-padding">\n' +
    '                                    <input type="text" ng-model="GRB.Size" name="Size" maxlength="8" ng-pattern="/(^\\d{1,5}$)|(^\\d{1,4}\\.\\d{1}$)|(^\\d{1,3}\\.\\d{1,2}$)|(^\\d{1,2}\\.\\d{1,3}$)/">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col-xs-3 no-padding-right" ng-if="GRType.MaterialType == 1">\n' +
    '                                <select ng-model="Unit" ng-options="option as option.name for option in Units" ng-change="changeUnit(Unit)"></select>\n' +
    '                            </div>\n' +
    '                            <div class="col-xs-6 shape no-padding-right">\n' +
    '                                <div class="col-xs-4 no-padding shape-title" ng-show="GRType.MaterialType == 1">\n' +
    '                                    <span>Shape</span>\n' +
    '                                </div>\n' +
    '                                <div class="col-xs-8 no-padding shape-select" ng-show="GRType.MaterialType == 1">\n' +
    '                                    <select ng-model="Shape" ng-options="option as option.Description for option in MaterialShapes" ng-change="GRB.setShapeID(Shape.pkShapeID)"></select>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="error col-xs-12 no-padding">\n' +
    '                                <p ng-if="GRBForm.$submitted && GRBForm.Size.$error.pattern">Size must be decimal (5,3)</p>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="col-xs-1 no-padding-right uom-select" ng-if="GRType.MaterialType == 1">\n' +
    '                            <select ng-model="UOM" ng-options="option as option.Description for option in UOMList" ng-change="GRB.setUOMID(UOM.pkUOMID)"></select>\n' +
    '                        </div>\n' +
    '                        <div class="approval-error col-xs-2">\n' +
    '                            <p ng-show="hasApprovalMismatch">Approval mismatch. Hand docs to Front Office..</p>\n' +
    '                        </div>\n' +
    '                        <div class="part-number col-xs-3" ng-if="GRType.MaterialType != 8">\n' +
    '                            <label for="" class="col-xs-8">Part Number</label>\n' +
    '                            <div class="part-number-group col-xs-4">\n' +
    '                                <input type="text" ng-model="GRB.PartNumber" maxlength="25" name="PartNumber">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row" ng-if="GRType.MaterialType == 8">\n' +
    '                    <div class="form-group">\n' +
    '                        <div class="qty-muanufacture col-xs-3 no-padding">\n' +
    '                            <label for="" class="col-xs-8 no-padding">Qty to Manufacture</label>\n' +
    '                            <div class="qty-muanufacture-group col-xs-4">\n' +
    '                                <input type="text" ng-model="GRB.QtyManu" maxlength="8" name="QtyManu" ng-pattern="/^\\d+$/">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="error col-xs-12">\n' +
    '                            <p ng-if="GRBForm.$submitted && GRBForm.QtyManu.$error.pattern">Qty to Manufacture must be number</p>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="form-group">\n' +
    '                        <div class="location col-xs-12 no-padding">\n' +
    '                            <label for="" class="col-xs-2 no-padding">Location / Issued to</label>\n' +
    '                            <div class="location-group col-xs-10">\n' +
    '                                <select ng-model="GRB.Location" ng-options="option as option.StoreArea for option in StoreAreaList" ng-change="changeLocation(GRB.Location); GRB.setStoreAreaID(GRB.Location.StoreAreaID)" name="Location" required>\n' +
    '                                </select>\n' +
    '                                <select ng-model="XLocation" ng-options="option as option.XLocation for option in XStoreLocations" ng-change="GRB.setXLocation(XLocation.XLocation)" name="XLocation" required>\n' +
    '                                </select>\n' +
    '                                <select ng-model="YLocation" ng-options="option as option.YLocation for option in YStoreLocations" ng-change="GRB.setYLocation(YLocation.YLocation)" name="YLocation" required>\n' +
    '                                </select>\n' +
    '                                <span class="tag"> / </span>\n' +
    '                                <button>Direct issue</button>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="error">\n' +
    '                            <p ng-show="GRBForm.$submitted && GRBForm.Location.$error.required">Please select Location</p>\n' +
    '                            <p ng-show="GRBForm.$submitted && GRBForm.XLocation.$error.required">Please select XLocation</p>\n' +
    '                            <p ng-show="GRBForm.$submitted && GRBForm.YLocation.$error.required">Please select YLocation</p>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="approval-type col-xs-3 no-padding">\n' +
    '                        <label for="" class="col-xs-8 no-padding">Approval type</label>\n' +
    '                        <div class="approval-type-group col-xs-4">\n' +
    '                            <select ng-model="Approval" ng-options="option as option.Description for option in ApprovalList" ng-change="changeApproval(Approval); GRB.setApprovalTypeID(Approval.pkApprovalID)">\n' +
    '                            </select>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="approval-button col-xs-6 col-xs-offset-3">\n' +
    '                        <button class="back" type="button" ng-click="redirectBack()">Back</button>\n' +
    '                        <button class="due-in-report" type="button" ng-click="openBIRT(\'DueIn\')">\'Due in\' Report</button>\n' +
    '                        <button class="view-po" type="button" ng-click="openBIRT(\'PurchaseOrder_v5\')">View PO</button>\n' +
    '                        <button class="add" type="button">Add</button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/home/home.tpl.html',
    '<div class="page-wrapper">\n' +
    '    <div class="page-content">\n' +
    '        <div class="title-global-page">Home</div>\n' +
    '        <div class="welcome">Welcome, <span ng-bind="user.getUsername()"></span></div>\n' +
    '        <div class="page-body">\n' +
    '            <div class="row">\n' +
    '                <div class="col-md-12">\n' +
    '                    <div class="human-resource col-md-4 no-padding">\n' +
    '                        <div class="title col-md-12 no-padding">Human Resources</div>\n' +
    '                        <div class="action col-md-3 no-padding">\n' +
    '                            <small>Book your Holiday & sick periods from here</small>\n' +
    '                            <button class="hr">H.R.</button>\n' +
    '                        </div>\n' +
    '                        <div class="image col-md-5">\n' +
    '                            <img src="../../../../../assets/images/Human_Resources.png" alt="Human Resource" />\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="system-help col-md-8 no-padding">\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-md-12">\n' +
    '                                <div class="system-overview col-md-6">\n' +
    '                                    <div class="title col-md-12 no-padding">System overview</div>\n' +
    '                                    <div class="image col-md-5">\n' +
    '                                        <img src="../../../../../assets/images/overview.jpg" alt="System overview">\n' +
    '                                    </div>\n' +
    '                                    <div class="action col-md-3 no-padding">\n' +
    '                                        <small>Learn about the functions</small>\n' +
    '                                        <button class="overview">System overview</button>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="help col-md-6">\n' +
    '                                    <div class="image col-md-5">\n' +
    '                                        <img src="../../../../../assets/images/help.jpg" alt="Help">\n' +
    '                                    </div>\n' +
    '                                    <div class="content col-md-7 no-padding">\n' +
    '                                        <div class="title col-md-12">Get Help</div>\n' +
    '                                        <div class="action col-md-12">\n' +
    '                                            <button>Help</button>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-md-12">\n' +
    '                                <div class="search">\n' +
    '                                    <div class="input-search col-md-6">\n' +
    '                                        <input type="text">\n' +
    '                                    </div>\n' +
    '                                    <div class="title col-md-6">\n' +
    '                                        System wide search\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="col-md-12">\n' +
    '                    <div class="exit-blackbird col-md-4 no-padding">\n' +
    '                        <div class="title col-md-12 no-padding">Exit Blackbird</div>\n' +
    '                        <div class="action col-md-3 no-padding">\n' +
    '                            <button class="exit" ng-click="logout()">Exit</button>\n' +
    '                        </div>\n' +
    '                        <div class="image col-md-5">\n' +
    '                            <img src="../../../../../assets/images/human_exit.jpg" alt="Exit">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="task-table col-md-8">\n' +
    '                        <div class="table-control col-md-1">\n' +
    '                            <span class="up-control" ng-click="reverse = false"></span>\n' +
    '                            <span>sort</span>\n' +
    '                            <span class="down-control" ng-click="reverse = true"></span>\n' +
    '                        </div>\n' +
    '                        <div class="table-content col-md-11">\n' +
    '                            <div class="table-title">Tasks requiring your attention:</div>\n' +
    '                            <div class="area-table">\n' +
    '                                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_home" load-item="TodoList">\n' +
    '                                    <thead>\n' +
    '                                        <tr>\n' +
    '                                            <th ng-repeat="item in sortTable" ng-bind="item.title" ng-click="selectOneItem(sortTable, item, \'selected\'); changeSortBy(item)" ng-class="{\'th-active\': item.selected}" class="{{item.class}}"></th>\n' +
    '                                        </tr>\n' +
    '                                    </thead>\n' +
    '                                    <tbody>\n' +
    '                                        <tr ng-repeat="item in TodoList | orderBy : sortBy : reverse" ng-click="selectOneItem(TodoList, item, \'selected\')" ng-dblclick="redirectPage(item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                            <td ng-bind="item.Age"></td>\n' +
    '                                            <td>\n' +
    '                                                <a class="hasTooltip">{{item.ActionType}}\n' +
    '                                                    <span>{{item.Description}}</span>\n' +
    '                                                </a>\n' +
    '                                            </td>\n' +
    '                                            <td ng-bind="item.CustomerName"></td>\n' +
    '                                            <td ng-bind="item.Qty"></td>\n' +
    '                                            <td ng-bind="item.WONo"></td>\n' +
    '                                            <td ng-bind="item.PartNo"></td>\n' +
    '                                            <td align="center">\n' +
    '                                                <input type="radio" ng-click="CompleteTodo(item)">\n' +
    '                                            </td>\n' +
    '                                        </tr>\n' +
    '                                    </tbody>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/inspection-schedule/inspection-schedule.tpl.html',
    '<div ui-view></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/machine-control/machine-control.tpl.html',
    '<div class="machine-control">\n' +
    '    <div class="container-fluid">\n' +
    '        <div class="row">\n' +
    '            <div class="top col-xs-12">\n' +
    '                <div class="row">\n' +
    '                    <div class="title-global-page">Machine Control</div>\n' +
    '                    <div class="left col-xs-6">\n' +
    '                        <div class="table-control">\n' +
    '                            <span class="up-control" ng-click="reverse = false"></span>\n' +
    '                            <span>sort</span>\n' +
    '                            <span class="down-control" ng-click="reverse = true"></span>\n' +
    '                        </div>\n' +
    '                        <div class="area-table">\n' +
    '                            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_machinelist" load-item="MachineListDetails">\n' +
    '                                <thead>\n' +
    '                                    <tr>\n' +
    '                                        <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[0], \'selected\'); changeSortBy(sortTable[0])" ng-class="{\'th-active\': sortTable[0].selected}">Mcn ID</th>\n' +
    '                                        <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[1], \'selected\'); changeSortBy(sortTable[1])" ng-class="{\'th-active\': sortTable[1].selected}">Manufacturer</th>\n' +
    '                                        <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[2], \'selected\'); changeSortBy(sortTable[2])" ng-class="{\'th-active\': sortTable[2].selected}">Model</th>\n' +
    '                                        <th colspan="2">Service Dates</th>\n' +
    '                                    </tr>\n' +
    '                                    <tr>\n' +
    '                                        <th ng-click="selectOneItem(sortTable, sortTable[3], \'selected\'); changeSortBy(sortTable[3])" ng-class="{\'th-active\': sortTable[3].selected}">Start</th>\n' +
    '                                        <th ng-click="selectOneItem(sortTable, sortTable[4], \'selected\'); changeSortBy(sortTable[4])" ng-class="{\'th-active\': sortTable[4].selected}">End</th>\n' +
    '                                    </tr>\n' +
    '                                </thead>\n' +
    '                                <tbody>\n' +
    '                                    <tr ng-repeat="item in MachineListDetails | orderBy : sortBy : reverse" ng-click="selectOneItem(MachineListDetails, item); selectMachine(item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                        <td ng-bind="item.MachineID"></td>\n' +
    '                                        <td ng-bind="item.Manufacturer"></td>\n' +
    '                                        <td ng-bind="item.Model"></td>\n' +
    '                                        <td>\n' +
    '                                            <input class="col-xs-10 no-padding" ng-model="item.ServiceStart" ng-change="AddMachine(item)"></input>\n' +
    '                                        </td>\n' +
    '                                        <td>\n' +
    '                                            <input class="col-xs-10 no-padding" ng-model="item.ServiceEnd" ng-change="AddMachine(item)"></input>\n' +
    '                                        </td>\n' +
    '                                    </tr>\n' +
    '                                </tbody>\n' +
    '                            </table>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="right col-xs-6">\n' +
    '                        <div class="row">\n' +
    '                            <div class="left col-xs-6">\n' +
    '                                <p>Dugard</p>\n' +
    '                                <p class="eag">Eagle 200</p>\n' +
    '                                <img class="img" src="../../assets/images/Dugard_Eagle_200.jpg" alt="">\n' +
    '                                <br>\n' +
    '                                <span>Difficullty level : </span>\n' +
    '                                <input type="text" value="{{MachineStats.DifficultyLevel}}">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="bottom col-xs-12">\n' +
    '                <div class="row">\n' +
    '                    <div class="left col-xs-5">\n' +
    '                        <div class="top">\n' +
    '                            <p>Lubricant / coolant:</p>\n' +
    '                            <div class="ar">\n' +
    '                                <span>Concentration check : </span>\n' +
    '                                <input type="text" value="{{MachineStats.ConcCheck | timeFormat: \'DD/MM/YYYY\'}}"><span class="by">by</span>\n' +
    '                                <input type="text" class="dave" value="{{MachineStats.ConcUser}}">\n' +
    '                                <br>\n' +
    '                                <span>Lubricant change :</span>\n' +
    '                                <input type="text" value="{{MachineStats.LubeCheck | timeFormat: \'DD/MM/YYYY\'}}"><span class="by">by</span>\n' +
    '                                <input type="text" value="{{MachineStats.LubeUser}}">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="bottom">\n' +
    '                            <p>Hydraulics :</p>\n' +
    '                            <div class="ar">\n' +
    '                                <span>Oil level check : </span>\n' +
    '                                <input type="text" value="{{MachineStats.LevelCheck | timeFormat: \'DD/MM/YYYY\'}}"><span class="by">by</span>\n' +
    '                                <input type="text" class="dave" value="{{MachineStats.ConcUser}}">\n' +
    '                                <br>\n' +
    '                                <span>Oil pressure chk :</span>\n' +
    '                                <input type="text" value="{{MachineStats.PressureCheck | timeFormat: \'DD/MM/YYYY\'}}"><span class="by">by</span>\n' +
    '                                <input type="text" value="{{MachineStats.PressureUser}}">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="right col-xs-7">\n' +
    '                        <div class="tooling-table">\n' +
    '                            <div class="area-table">\n' +
    '                                <table>\n' +
    '                                    <tr>\n' +
    '                                        <th rowspan="2" class="cl-brown">Tooling</th>\n' +
    '                                        <th colspan="2" class="cl-light-blue">Month</th>\n' +
    '                                        <th colspan="2" class="cl-light-blue">Quarter</th>\n' +
    '                                        <th colspan="2" class="cl-light-blue">Year</th>\n' +
    '                                    </tr>\n' +
    '                                    <tr>\n' +
    '                                        <th class="cl-blue">This</th>\n' +
    '                                        <th>Last</th>\n' +
    '                                        <th class="cl-blue">This</th>\n' +
    '                                        <th>Last</th>\n' +
    '                                        <th class="cl-blue">This</th>\n' +
    '                                        <th>Last</th>\n' +
    '                                    </tr>\n' +
    '                                    <tr>\n' +
    '                                        <td class="bg-brown-cl-white">Tools used</td>\n' +
    '                                        <td class="cl-blue" ng-bind="MachineStats.ThisMonth"></td>\n' +
    '                                        <td ng-bind="MachineStats.LastMonth"></td>\n' +
    '                                        <td class="cl-blue" ng-bind="MachineStats.ThisQuarter"></td>\n' +
    '                                        <td ng-bind="MachineStats.LastQuarter"></td>\n' +
    '                                        <td class="cl-blue" ng-bind="MachineStats.ThisYear"></td>\n' +
    '                                        <td ng-bind="MachineStats.LastYear"></td>\n' +
    '                                    </tr>\n' +
    '                                    <tr>\n' +
    '                                        <td class="bg-brown-cl-white">Tooling cost</td>\n' +
    '                                        <td class="cl-blue" ng-bind="MachineStats.ThisMonth"></td>\n' +
    '                                        <td ng-bind="MachineStats.LastMonth"></td>\n' +
    '                                        <td class="cl-blue" ng-bind="MachineStats.ThisQuarter"></td>\n' +
    '                                        <td ng-bind="MachineStats.LastQuarter"></td>\n' +
    '                                        <td class="cl-blue" ng-bind="MachineStats.ThisYear"></td>\n' +
    '                                        <td ng-bind="MachineStats.LastYear"></td>\n' +
    '                                    </tr>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="maintenance-table">\n' +
    '                            <div class="area-table">\n' +
    '                                <table>\n' +
    '                                    <tr>\n' +
    '                                        <th rowspan="2" class="cl-brown">Maintenance</th>\n' +
    '                                        <th class="cl-light-blue">Date</th>\n' +
    '                                        <th class="cl-light-blue">Issue</th>\n' +
    '                                        <th class="cl-light-blue">Resolution</th>\n' +
    '                                    </tr>\n' +
    '                                    <tr>\n' +
    '                                        <th ng-bind="MaintenanceStats.CreateDate | timeFormat: \'DD/MM/YYYY\'"></th>\n' +
    '                                        <th ng-bind="MaintenanceStats.Issue"></th>\n' +
    '                                        <th ng-bind="MaintenanceStats.Resolution"></th>\n' +
    '                                    </tr>\n' +
    '                                    <tr>\n' +
    '                                        <td class="bg-brown-cl-white">&nbsp;</td>\n' +
    '                                        <td>&nbsp;</td>\n' +
    '                                        <td>&nbsp;</td>\n' +
    '                                        <td>&nbsp;</td>\n' +
    '                                    </tr>\n' +
    '                                    <tr>\n' +
    '                                        <td class="bg-brown-cl-white">&nbsp;</td>\n' +
    '                                        <td>&nbsp;</td>\n' +
    '                                        <td>&nbsp;</td>\n' +
    '                                        <td>&nbsp;</td>\n' +
    '                                    </tr>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="area-button">\n' +
    '                            <button ng-click="redirectBack()" type="button">Back</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/machine-list/machine-list.tpl.html',
    '<div class="machines-list-page">\n' +
    '    <div class="title-global-page">Machine List</div>\n' +
    '    <form name="MachineForm" ng-submit="MachineForm.$valid && AddOrUpdateMachine(MachineParams)" novalidate>\n' +
    '        <div class="page-color-top">\n' +
    '            <div class="content">\n' +
    '                <div class="content-title">\n' +
    '                    <label>Display:</label>\n' +
    '                </div>\n' +
    '                <div class="content-color">\n' +
    '                    <div class="color-box trn" ng-click="filterMachine(\'Trn\')" ng-class="{\'color-select\': MachineType === \'Trn\'}">Trn</div>\n' +
    '                    <div class="color-box grd" ng-click="filterMachine(\'Grd\')" ng-class="{\'color-select\': MachineType === \'Grd\'}">Grd</div>\n' +
    '                    <div class="color-box mil" ng-click="filterMachine(\'Mil\')" ng-class="{\'color-select\': MachineType === \'Mil\'}">Mil</div>\n' +
    '                    <div class="color-box gc" ng-click="filterMachine(\'GC\')" ng-class="{\'color-select\': MachineType === \'GC\'}">GC</div>\n' +
    '                    <div class="color-box oth" ng-click="filterMachine(\'Oth\')" ng-class="{\'color-select\': MachineType === \'Oth\'}">Oth</div>\n' +
    '                    <div class="color-box active" ng-click="filterMachine(\'Active\')" ng-class="{\'color-select\': MachineType === \'Active\'}">Active</div>\n' +
    '                    <div class="color-box all" ng-click="filterMachine(\'ALL\')" ng-class="{\'color-select\': MachineType === \'ALL\'}">ALL</div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="page-body">\n' +
    '            <div class="table-content col-xs-5">\n' +
    '                <div class="col-xs-1 sort-control no-padding">\n' +
    '                    <span class="up-control" ng-click="reverse = false"></span>\n' +
    '                    <span>sort</span>\n' +
    '                    <span class="down-control" ng-click="reverse = true"></span>\n' +
    '                </div>\n' +
    '                <div class="col-xs-11 area-table">\n' +
    '                    <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_machinelist" load-item="MachineMasterList">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th ng-repeat="item in sortTable" ng-bind="item.title" ng-click="selectOneItem(sortTable, item, \'selected\'); changeSortBy(item)" ng-class="{\'th-active\': item.selected}"></th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="item in MachineMasterList | orderBy: sortBy : reverse" ng-click="selectOneItem(MachineMasterList, item); selectMachine(item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                <td ng-bind="item.MachineID"></td>\n' +
    '                                <td ng-bind="item.Manufacturer"></td>\n' +
    '                                <td ng-bind="item.Model"></td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="page-content col-xs-7 no-padding-right">\n' +
    '                <div class="row margin-bot-3">\n' +
    '                    <div class="button-mainternance col-xs-12">\n' +
    '                        <div class="machine-action col-xs-4 no-padding">\n' +
    '                            <button class="remove" type="button" ng-click="RemoveMachine(MachineMasterList)">Remove from service</button>\n' +
    '                            <button type="button" class="add float-right" ng-click="AddNew()">Add New</button>\n' +
    '                        </div>\n' +
    '                        <div class="maintenance col-xs-3 col-xs-offset-5 content-right">Maintenance Parameters</div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="make-coolant-type col-xs-12 margin-bot-10">\n' +
    '                        <div class="make-model col-xs-4 no-padding-right">\n' +
    '                            <div class="make col-xs-12 no-padding">\n' +
    '                                <label for="" class="col-xs-12 no-padding">Make</label>\n' +
    '                                <div class="col-xs-12 no-padding make-group">\n' +
    '                                    <input type="text" ng-model="MachineParams.Manufacturer" maxlength="15">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="model col-xs-12 no-padding">\n' +
    '                                <label for="" class="col-xs-12 no-padding">Model</label>\n' +
    '                                <div class="col-xs-12 no-padding model-group">\n' +
    '                                    <input type="text" ng-model="MachineParams.Model" maxlength="25">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="coolant-target col-xs-4 col-xs-offset-4 no-padding">\n' +
    '                            <div class="coolant-type col-xs-12 no-padding content-right margin-bot-3">\n' +
    '                                <label for="" class="col-xs-6 color-light-blue no-padding">Coolant Type</label>\n' +
    '                                <div class="coolant-type-group col-xs-6 no-padding">\n' +
    '                                    <input type="text" ng-model="MachineParams.CoolantType">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="target col-xs-12 no-padding content-right margin-bot-3">\n' +
    '                                <label for="" class="col-xs-7 color-light-blue no-padding">Target concentration</label>\n' +
    '                                <div class="coolant-type-group col-xs-5 no-padding">\n' +
    '                                    <input type="text" class="from" ng-model="MachineParams.TargetConcMin" name="TargetConcMin" ng-pattern="/^\\d+$/">\n' +
    '                                    <span>--</span>\n' +
    '                                    <input type="text" class="to" ng-model="MachineParams.TargetConcMax" name="TargetConcMax" ng-pattern="/^\\d+$/">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="error">\n' +
    '                                <span ng-show="MachineForm.$submitted && MachineForm.TargetConcMin.$error.pattern">Target min must be number</span>\n' +
    '                            </div>\n' +
    '                            <div class="error">\n' +
    '                                <span ng-show="MachineForm.$submitted && MachineForm.TargetConcMax.$error.pattern">Target max must be number</span>\n' +
    '                            </div>\n' +
    '                            <div class="coolant-type col-xs-12 no-padding content-right">\n' +
    '                                <label for="" class="col-xs-11 color-light-blue no-padding">Slide Oil Level</label>\n' +
    '                                <div class="coolant-type-group col-xs-1 no-padding">\n' +
    '                                    <input type="checkbox" ng-model="MachineParams.SlideOilLevel" ng-true-value="1" ng-false-value="0">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="color-coolant-level col-xs-12">\n' +
    '                        <div class="color col-xs-7 no-padding-right">\n' +
    '                            <div class="color-box trn" ng-show="isIcon" ng-click="selectTrn()" ng-class="{\'color-select\': MachineParams.Trn == 1}">Trn</div>\n' +
    '                            <div class="color-box grd" ng-show="isIcon" ng-click="selectGrd()" ng-class="{\'color-select\': MachineParams.Grd == 1}">Grd</div>\n' +
    '                            <div class="color-box mil" ng-show="isIcon" ng-click="selectMil()" ng-class="{\'color-select\': MachineParams.Mil == 1}">Mil</div>\n' +
    '                            <div class="color-box gc" ng-show="isIcon" ng-click="selectGC()" ng-class="{\'color-select\': MachineParams.GC == 1}">GC</div>\n' +
    '                            <div class="color-box oth" ng-show="isIcon" ng-click="selectoth()" ng-class="{\'color-select\': MachineParams.oth == 1}">Oth</div>\n' +
    '                        </div>\n' +
    '                        <div class="coolant-grease col-xs-5 no-padding">\n' +
    '                            <div class="coolant-level col-xs-12 no-padding content-right margin-bot-10">\n' +
    '                                <label for="" class="col-xs-11 color-light-blue no-padding">Coolant level</label>\n' +
    '                                <div class="coolant-level-group col-xs-1 no-padding">\n' +
    '                                    <input type="checkbox" ng-model="MachineParams.CoolantLevel" ng-true-value="1" ng-false-value="0">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="grease-level col-xs-12 no-padding content-right margin-bot-10">\n' +
    '                                <label for="" class="col-xs-11 color-light-blue no-padding">Grease level</label>\n' +
    '                                <div class="grease-level-group col-xs-1 no-padding">\n' +
    '                                    <input type="checkbox" ng-model="MachineParams.GreaseLevel" ng-true-value="1" ng-false-value="0">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="optional-fan-filter col-xs-12 no-padding">\n' +
    '                        <div class="optional no-padding col-xs-8">\n' +
    '                            <label for="" class="col-xs-12 color-light-blue">Optional parameters:</label>\n' +
    '                            <div class="optional-group col-xs-12">\n' +
    '                                <div class="col-xs-5">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="offset col-xs-5 col-xs-offset-7 no-padding margin-bot-3">\n' +
    '                                            <input type="text" name="Offset" ng-model="MachineParams.Offset" ng-pattern="/^\\d+$/">Offset\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="col-xs-12 error">\n' +
    '                                            <span ng-show="MachineForm.$submitted && MachineForm.Offset.$error.pattern">Offset must be number</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="station col-xs-12 no-padding margin-bot-3">\n' +
    '                                            <label for="" class="col-xs-7 no-padding-left content-right">Station No.</label>\n' +
    '                                            <div class="station-group col-xs-5 no-padding">\n' +
    '                                                <input type="text" ng-model="MachineParams.StationNo" name="StationNo" ng-pattern="/^\\d+$/">\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="col-xs-12 error">\n' +
    '                                            <span ng-show="MachineForm.$submitted && MachineForm.StationNo.$error.pattern">Station No. must be number</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="designation col-xs-12 no-padding margin-bot-3">\n' +
    '                                            <label for="" class="col-xs-7 no-padding-left content-right">Designation</label>\n' +
    '                                            <div class="designation-group col-xs-5 no-padding">\n' +
    '                                                <input type="text" ng-model="MachineParams.Designation">\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="diameter col-xs-12 no-padding margin-bot-3">\n' +
    '                                            <label for="" class="col-xs-7 no-padding-left content-right">Diameter</label>\n' +
    '                                            <div class="diameter-group col-xs-5 no-padding">\n' +
    '                                                <input type="text" ng-model="MachineParams.Diameter" name="Diameter" ng-pattern="/^\\d+$/">\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="MachineForm.$submitted && MachineForm.Diameter.$error.pattern">Diameter must be number</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="flute-length col-xs-12 no-padding margin-bot-3">\n' +
    '                                            <label for="" class="col-xs-7 no-padding-left content-right">Min. flute length</label>\n' +
    '                                            <div class="flute-length-group col-xs-5 no-padding">\n' +
    '                                                <input type="text" ng-model="MachineParams.MinfluteLength" name="MinfluteLength" ng-pattern="/^\\d+$/">\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="MachineForm.$submitted && MachineForm.MinfluteLength.$error.pattern">Min. flute must be number</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="col-xs-7 no-padding">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="stick-out col-xs-12 no-padding margin-bot-3">\n' +
    '                                            <label for="" class="col-xs-6 no-padding content-right">Min. stick out</label>\n' +
    '                                            <div class="stick-out-group col-xs-5">\n' +
    '                                                <input type="text" ng-model="MachineParams.MinStickOut" name="MinStickOut" ng-pattern="/^\\d+$/">\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="MachineForm.$submitted && MachineForm.MinStickOut.$error.pattern">Min. stick must be number</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="holder col-xs-12 no-padding margin-bot-3">\n' +
    '                                            <label for="" class="col-xs-6 no-padding content-right">Holder No.</label>\n' +
    '                                            <div class="holder-group col-xs-5">\n' +
    '                                                <input type="text" ng-model="MachineParams.HolderNo">\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="datum-point col-xs-12 no-padding margin-bot-3">\n' +
    '                                            <label for="" class="col-xs-6 no-padding content-right">Insert Datum point</label>\n' +
    '                                            <div class="datum-point-group col-xs-5">\n' +
    '                                                <input type="text" ng-model="MachineParams.InsertDatumPoint" maxlength="15">\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="spindle col-xs-12 no-padding margin-bot-3">\n' +
    '                                            <label for="" class="col-xs-6 no-padding content-right">Spindle</label>\n' +
    '                                            <div class="spindle-group col-xs-5">\n' +
    '                                                <input type="text" ng-model="MachineParams.Spindle">\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="fan-oil-filter col-xs-4">\n' +
    '                            <div class="fan-filter col-xs-12 no-padding margin-bot-10">\n' +
    '                                <label for="" class="col-xs-11 color-light-blue no-padding-left content-right">Fan filter</label>\n' +
    '                                <div class="fan-filter-group col-xs-1 no-padding content-right">\n' +
    '                                    <input type="checkbox" ng-model="MachineParams.Fanfilter" ng-true-value="1" ng-false-value="0">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="oil-filter col-xs-12 no-padding margin-bot-10">\n' +
    '                                <label for="" class="col-xs-11 color-light-blue no-padding-left content-right">Oil filter</label>\n' +
    '                                <div class="oil-filter-group col-xs-1 no-padding content-right">\n' +
    '                                    <input type="checkbox" ng-model="MachineParams.OilFilter" ng-true-value="1" ng-false-value="0">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="coolant-filter col-xs-12 no-padding margin-bot-10">\n' +
    '                                <label for="" class="col-xs-11 color-light-blue no-padding-left content-right">Coolant filter</label>\n' +
    '                                <div class="coolant-filter-group col-xs-1 no-padding content-right">\n' +
    '                                    <input type="checkbox" ng-model="MachineParams.CoolantFilter" ng-true-value="1" ng-false-value="0">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="air-system col-xs-12 no-padding margin-bot-10">\n' +
    '                                <label for="" class="col-xs-11 color-light-blue no-padding-left content-right">Air System</label>\n' +
    '                                <div class="air-system-group col-xs-1 no-padding content-right">\n' +
    '                                    <input type="checkbox" ng-model="MachineParams.AirSystem" ng-true-value="1" ng-false-value="0">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="integral col-xs-12 no-padding margin-bot-10">\n' +
    '                                <label for="" class="col-xs-11 color-light-blue no-padding-left content-right">Integral lighting</label>\n' +
    '                                <div class="integral-group col-xs-1 no-padding content-right">\n' +
    '                                    <input type="checkbox" ng-model="MachineParams.IntegralLighting" ng-true-value="1" ng-false-value="0">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="interlocks col-xs-12 no-padding margin-bot-10">\n' +
    '                                <label for="" class="col-xs-11 color-light-blue no-padding-left content-right">Interlocks engaged</label>\n' +
    '                                <div class="interlocks-group col-xs-1 no-padding content-right">\n' +
    '                                    <input type="checkbox" ng-model="MachineParams.InterlocksEngaged" ng-true-value="1" ng-false-value="0">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="other col-xs-12 no-padding margin-bot-10">\n' +
    '                                <div class="other-group col-xs-1 col-xs-offset-11 no-padding content-right">\n' +
    '                                    <input type="checkbox">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="difficulty-level col-xs-4 no-padding">\n' +
    '                        <label for="" class="col-xs-5 no-padding">Difficulty Level</label>\n' +
    '                        <div class="difficulty-level-group col-xs-7">\n' +
    '                            <input type="text" ng-model="MachineParams.DifficultyLevel" name="DifficultyLevel" ng-pattern="/^\\d+$/">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="error">\n' +
    '                        <span ng-show="MachineForm.$submitted && MachineForm.DifficultyLevel.$error.pattern">Difficulty Level must be number</span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="page-button">\n' +
    '            <button class="back" ng-click="redirectBack()">Back</button>\n' +
    '            <button class="save">Save</button>\n' +
    '        </div>\n' +
    '    </form>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/material-issue/material-issue.tpl.html',
    '<div class="material-issue-page">\n' +
    '    <div class="title-global-page">Material Issue</div>\n' +
    '    <div class="page-top">\n' +
    '        <div class="search col-xs-2">\n' +
    '            <strong>Search: </strong>\n' +
    '            <div class="grb-no">\n' +
    '                <span class="grb-title">GRB No.</span>\n' +
    '                <input class="grb-value" type="text" ng-model="GRBNo">\n' +
    '            </div>\n' +
    '            <div class="material">\n' +
    '                <span class="material-title">Material</span>\n' +
    '                <input type="text" class="material-value" ng-model="Material" maxlength="6">\n' +
    '            </div>\n' +
    '            <div class="size">\n' +
    '                <span class="size-title">Size</span>\n' +
    '                <input type="text" class="size-value" ng-model="Size">\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="instock-table col-xs-6">\n' +
    '            <div class="table-control">\n' +
    '                <span class="up-control" ng-click="reverse = false"></span>\n' +
    '                <span>sort</span>\n' +
    '                <span class="down-control" ng-click="reverse = true"></span>\n' +
    '            </div>\n' +
    '            <div class="table-title">in stock</div>\n' +
    '            <div class="area-table">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_instock" load-item="MaterialInStock">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th ng-repeat="item in sortTable" ng-bind="item.title" ng-click="selectOneItem(sortTable, item, \'selected\'); changeSortBy(item)" ng-class="{\'th-active\': item.selected}"></th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in MaterialInStock | orderBy: sortBy : reverse | contain:\'GRBNo\':GRBNo | contain: \'Spec\' : Material | contain: \'Size\' : Size | unique:\'GRBNo\'" ng-click="selectOneItem(MaterialInStock, item); selectMaterialInStock(item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                            <td ng-bind="item.GRBNo"></td>\n' +
    '                            <td ng-bind="item.Spec"></td>\n' +
    '                            <td ng-bind="item.Size"></td>\n' +
    '                            <td ng-bind="item.Units"></td>\n' +
    '                            <td ng-bind="item.Shape"></td>\n' +
    '                            <td ng-bind="item.ReleaseLevel"></td>\n' +
    '                            <td ng-bind="item.PurchaseLength"></td>\n' +
    '                            <td ng-bind="item.DateIn | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                            <td ng-bind="item.OrderQty"></td>\n' +
    '                            <td ng-bind="item.InStock"></td>\n' +
    '                            <td ng-bind="item.Location"></td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="issued-table col-xs-4">\n' +
    '            <div class="table-title">issued</div>\n' +
    '            <div class="area-table">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_date">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th>Date</th>\n' +
    '                            <th>Part No.</th>\n' +
    '                            <th>Qty</th>\n' +
    '                            <th>WO</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-model="test" ng-repeat="item in MaterialIssues" ng-click="selectOneItem(MaterialIssues, item); selectMaterialIssue(item)">\n' +
    '                            <td ng-bind="item.DateIssued | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                            <td>\n' +
    '                                <a href="#" ng-bind="item.PartNumber"></a>\n' +
    '                            </td>\n' +
    '                            <td ng-bind="item.Qty"></td>\n' +
    '                            <td>\n' +
    '                                <a href="#" ng-bind="item.WONo"></a>\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="location">\n' +
    '            <form name="LocationForm" ng-submit="LocationForm.$valid && updateLocation(LocationInfo)" novalidate>\n' +
    '                <span class="location-title">Location</span>\n' +
    '                <select ng-model="LocationInfo.Location" ng-options="option as option.StoreArea for option in StoreAreaList" ng-change="changeLocation(LocationInfo.Location); GRB.setStoreAreaID(LocationInfo.Location.StoreAreaID)" name="Location" required>\n' +
    '                </select>\n' +
    '                <select ng-model="LocationInfo.XLocation" ng-options="option as option.XLocation for option in XStoreLocations" ng-change="GRB.setXLocation(LocationInfo.XLocation.XLocation)" name="XLocation" required>\n' +
    '                </select>\n' +
    '                <select ng-model="LocationInfo.YLocation" ng-options="option as option.YLocation for option in YStoreLocations" ng-change="GRB.setYLocation(LocationInfo.YLocation.YLocation)" name="YLocation" required>\n' +
    '                </select>\n' +
    '                <button type="submit">Update Loc\'n</button>\n' +
    '                <div class="error">\n' +
    '                    <p ng-show="LocationForm.$submitted && LocationForm.Location.$error.required">Please select Location</p>\n' +
    '                    <p ng-show="LocationForm.$submitted && LocationForm.XLocation.$error.required">Please select XLocation</p>\n' +
    '                    <p ng-show="LocationForm.$submitted && LocationForm.YLocation.$error.required">Please select YLocation</p>\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="page-body-issue">\n' +
    '        <div class="page-body-title">Issue:</div>\n' +
    '        <div class="page-body-content">\n' +
    '            <div class="row">\n' +
    '                <div class="form-group">\n' +
    '                    <div class="col-xs-2 grb">\n' +
    '                        <label for="" class="col-xs-9 col-xs-offset-3 no-padding">GRB:</label>\n' +
    '                        <div class="grb-group col-xs-9 col-xs-offset-3">\n' +
    '                            <input type="text" ng-model="MaterialIssueDetail.GRBNo" readonly>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="location col-xs-2">\n' +
    '                        <label for="" class="col-xs-12">Location</label>\n' +
    '                        <div class="location-group col-xs-12">\n' +
    '                            <input type="text" class="border-blue color-orange" ng-model="MaterialIssueDetail.Location" readonly>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="length col-xs-2 col-xs-offset-1">\n' +
    '                        <label for="" class="col-xs-4 no-padding">Length</label>\n' +
    '                        <div class="length-group col-xs-8 no-padding">\n' +
    '                            <input type="text" ng-model="MaterialIssueDetail.Length" readonly>\n' +
    '                            <span ng-bind="MaterialIssueDetail.Units"></span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="work-order col-xs-3 col-xs-offset-2">\n' +
    '                        <p>Work Order {{WorkOrderID}}</p>\n' +
    '                        <p>Part No. {{MaterialIssueDetail.PartNumber}}</p>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="form-group">\n' +
    '                    <div class="qty col-xs-2 col-xs-offset-5">\n' +
    '                        <label for="" class="col-xs-4">Qty</label>\n' +
    '                        <div class="qty-group col-xs-8 no-padding">\n' +
    '                            <input type="text" ng-model="MaterialIssueDetail.Qty" readonly>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="form-group">\n' +
    '                    <div class="material col-xs-2 col-xs-offset-1">\n' +
    '                        <label for="" class="col-xs-12">Material</label>\n' +
    '                        <div class="material-group col-xs-12">\n' +
    '                            <input type="text" class="border-blue color-orange" ng-model="MaterialIssueDetail.MaterialSpec" readonly>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="size col-xs-1 no-padding">\n' +
    '                        <label for="" class="col-xs-12 no-padding">Size</label>\n' +
    '                        <div class="size-group col-xs-12">\n' +
    '                            <input type="text" class="border-blue color-orange" ng-model="MaterialIssueDetail.Size" ng-model="MaterialIssueDetail.Size" readonly>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="unit col-xs-1 no-padding-left">\n' +
    '                        <label for="" class="col-xs-12 no-padding">Units</label>\n' +
    '                        <div class="col-xs-12 unit-group no-padding">\n' +
    '                            <input type="text" class="border-blue color-orange" ng-model="MaterialIssueDetail.Units" readonly>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="shape col-xs-1 no-padding">\n' +
    '                        <label for="" class="col-xs-12 no-padding">Shape</label>\n' +
    '                        <div class="shape-group col-xs-12 no-padding">\n' +
    '                            <input type="text" class="border-blue color-orange" ng-model="MaterialIssueDetail.Shape" readonly>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="release col-xs-1 no-padding-right">\n' +
    '                        <label for="" class="col-xs-12 no-padding">Release</label>\n' +
    '                        <div class="release-group col-xs-12 no-padding">\n' +
    '                            <input type="text" class="border-blue color-orange" ng-model="MaterialIssueDetail.ReleaseType" readonly>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="form-group">\n' +
    '                    <div class="description col-xs-4 col-xs-offset-8">\n' +
    '                        <strong class="col-xs-5 no-padding-left">Description</strong>\n' +
    '                        <div class="description-group col-xs-7 no-padding">\n' +
    '                            <input type="text" ng-model="MaterialIssueDetail.Description" readonly>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="form-group">\n' +
    '                    <div class="issue-instruction col-xs-4 col-xs-offset-8">\n' +
    '                        <strong class="col-xs-5 no-padding-left">Issue Instruction</strong>\n' +
    '                        <div class="issue-instruction-group col-xs-7 no-padding">\n' +
    '                            <input type="text" ng-model="MaterialIssueDetail.Instruction" readonly>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="form-group">\n' +
    '                    <div class="designated-machine"><strong>Designated Machine</strong> {{MaterialIssueDetail.Machine}}</div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="form-group pos-relative">\n' +
    '                    <div class="cutting-material col-xs-1 col-xs-offset-1">\n' +
    '                        Begin issuing / cutting material:\n' +
    '                    </div>\n' +
    '                    <div class="material-control col-xs-6">\n' +
    '                        <div class="begin control-element col-xs-4" ng-click="BeginMaterialIssue()">\n' +
    '                            <div class="icon"></div>\n' +
    '                            <div class="title">Begin</div>\n' +
    '                        </div>\n' +
    '                        <div class="pause control-element col-xs-4" ng-click="PauseMaterialIssue()">\n' +
    '                            <div class="icon"></div>\n' +
    '                            <div class="title">Pause</div>\n' +
    '                        </div>\n' +
    '                        <div class="complete control-element col-xs-4" ng-click="EndMaterialIssue()">\n' +
    '                            <div class="icon"></div>\n' +
    '                            <div class="title">Complete</div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-1 col-xs-offset-3 material-button">\n' +
    '                        <button type="button" ng-click="redirectBack()">Back</button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/operations-treatment-admin/operations-treatment-admin.tpl.html',
    '<div id="operations-treatment-admin">\n' +
    '    <div class="col-md-4 box">\n' +
    '        <div class="col-md-5">\n' +
    '            <p class="text-right">Treatments list:</p>\n' +
    '            <br>\n' +
    '            <div class="table-title text-right">Add new treatment</div>\n' +
    '            <p>\n' +
    '                <input type="text" ng-model="Treatment" maxlength="20">\n' +
    '            </p>\n' +
    '            <div class="sample-box space-bottom">\n' +
    '                <button type="button" class="pull-right" ng-click="AddTreatment(Treatment)">Add</button>\n' +
    '            </div>\n' +
    '            <div class="table-title text-right">Delete selected Treatment</div>\n' +
    '            <div class="sample-box space-bottom">\n' +
    '                <button type="button" class="pull-right space-bottom" ng-click="DeteleTreatment(TreatmentsList)">Delete</button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-md-6">\n' +
    '            <div class="area-table table-previous height-big">\n' +
    '                <table ng-show="TreatmentsList | hasItem">\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in TreatmentsList | orderBy: \'Description\' : reverseTreatment" ng-click="selectItem(item, \'selected\')">\n' +
    '                            <td>\n' +
    '                            	<span ng-bind="item.Description"></span>\n' +
    '                            	<span ng-show="item.selected == true" class="pull-right glyphicon glyphicon-ok"></span>\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="table-control col-md-1">\n' +
    '            <span class="up-control" ng-click="reverseTreatment = false"></span>\n' +
    '            <span>sort</span>\n' +
    '            <span class="down-control" ng-click="reverseTreatment = true"></span>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col-md-4 box">\n' +
    '        <div class="col-md-5">\n' +
    '            <p class="text-right">Operations list:</p>\n' +
    '            <br>\n' +
    '            <div class="table-title text-right">Add new operation</div>\n' +
    '            <p>\n' +
    '                <input type="text" ng-model="Operation" maxlength="25">\n' +
    '            </p>\n' +
    '            <div class="sample-box space-bottom">\n' +
    '                <button type="button" class="pull-right" ng-click="AddOperation(Operation)">Add</button>\n' +
    '            </div>\n' +
    '            <div class="table-title text-right">Delete selected Operation</div>\n' +
    '            <div class="sample-box space-bottom">\n' +
    '                <button type="button" class="pull-right space-bottom" ng-click="DeleteOperation(MachiningTypeList)">Delete</button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-md-6">\n' +
    '            <div class="area-table table-previous height-big">\n' +
    '                <table ng-show="MachiningTypeList | hasItem">\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in MachiningTypeList | orderBy: \'Description\' : reverseOperation" ng-click="selectItem(item, \'selected\')">\n' +
    '                            <td>\n' +
    '                                <span ng-bind="item.Description"></span>\n' +
    '                                <span ng-show="item.selected == true" class="pull-right glyphicon glyphicon-ok"></span>\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="table-control col-md-1">\n' +
    '            <span class="up-control" ng-click="reverseOperation = false"></span>\n' +
    '            <span>sort</span>\n' +
    '            <span class="down-control" ng-click="reverseOperation = true"></span>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col-md-4 box">\n' +
    '        <div class="row">\n' +
    '            <div class="col-md-5">\n' +
    '                <p class="text-right">Sundries list:</p>\n' +
    '                <br>\n' +
    '                <div class="table-title text-right">Add new Sundry</div>\n' +
    '                <p>\n' +
    '                    <input type="text" ng-model="Sundry" maxlength="20">\n' +
    '                </p>\n' +
    '                <div class="sample-box space-bottom">\n' +
    '                    <button type="button" class="pull-right" ng-click="AddSundry(Sundry)">Add</button>\n' +
    '                </div>\n' +
    '                <div class="table-title text-right">Delete selected Sundries</div>\n' +
    '                <div class="sample-box space-bottom">\n' +
    '                    <button type="submit" class="pull-right space-bottom" ng-click="DeleteSundries(SundriesList)">Delete</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="col-md-6">\n' +
    '                <div class="area-table table-previous">\n' +
    '                    <table ng-show="SundriesList | hasItem">\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="item in SundriesList | orderBy: \'Description\' : reverseSundry" ng-click="selectItem(item, \'selected\')">\n' +
    '                                <td>\n' +
    '                                    <span ng-bind="item.Description"></span>\n' +
    '                                    <span ng-show="item.selected == true" class="pull-right glyphicon glyphicon-ok"></span>\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="table-control col-md-1">\n' +
    '                <span class="up-control" ng-click="reverseSundry = false"></span>\n' +
    '                <span>sort</span>\n' +
    '                <span class="down-control" ng-click="reverseSundry = true"></span>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <br>\n' +
    '        <div class="row">\n' +
    '            <div class="col-md-5">\n' +
    '                <p class="text-right">Condition of Supply:</p>\n' +
    '                <br>\n' +
    '                <div class="table-title text-right">Add new condition</div>\n' +
    '                <p>\n' +
    '                    <input type="text" ng-model="Condition" maxlength="20">\n' +
    '                </p>\n' +
    '                <div class="sample-box space-bottom">\n' +
    '                    <button type="button" class="pull-right" ng-click="AddCondition(Condition)">Add</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="col-md-6">\n' +
    '                <div class="area-table table-previous">\n' +
    '                    <table ng-show="SupplyConditionList | hasItem">\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="item in SupplyConditionList | orderBy: \'Description\' : reverseCondition">\n' +
    '                                <td ng-bind="item.Description"></td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="table-control col-md-1">\n' +
    '                <span class="up-control" ng-click="reverseCondition = false"></span>\n' +
    '                <span>sort</span>\n' +
    '                <span class="down-control" ng-click="reverseCondition = true"></span>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-4 box">\n' +
    '            <div class="col-md-5">\n' +
    '                <p class="text-right">Scheduling contingencies (Fat)</p>\n' +
    '            </div>\n' +
    '            <div class="col-md-6">\n' +
    '                <div class="area-table table-previous">\n' +
    '            		<p class="text-right">days</p>\n' +
    '                    <table ng-show="GetFat | hasItem">\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="item in GetFat">\n' +
    '                                <td ng-bind="item.FATType"></td>\n' +
    '                                <td>\n' +
    '                                    <input ng-model="item.DelDays" type="text" ng-blur="UpdateFat(item)">\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-md-4 box">\n' +
    '	        <div class="col-md-5">\n' +
    '	            <p class="text-right">Op. Pause reasons:</p>\n' +
    '	            <br>\n' +
    '	            <div class="table-title text-right">Add new reason</div>\n' +
    '	            <p>\n' +
    '	                <input type="text" ng-model="Reason" maxlength="10">\n' +
    '	            </p>\n' +
    '	            <div class="sample-box space-bottom">\n' +
    '	                <button type="submit" class="pull-right" ng-click="AddReason(Reason)">Add</button>\n' +
    '	            </div>\n' +
    '	        </div>\n' +
    '	        <div class="col-md-6">\n' +
    '	            <div class="area-table table-previous">\n' +
    '	                <table ng-show="PauseReasons | hasItem">\n' +
    '	                    <tbody>\n' +
    '	                        <tr ng-repeat="item in PauseReasons | orderBy: \'Reason\' : reversePauseReason">\n' +
    '	                            <td ng-bind="item.Reason"></td>\n' +
    '	                        </tr>\n' +
    '	                    </tbody>\n' +
    '	                </table>\n' +
    '	            </div>\n' +
    '	        </div>\n' +
    '	        <div class="table-control col-md-1">\n' +
    '	            <span class="up-control" ng-click="reversePauseReason = false"></span>\n' +
    '	            <span>sort</span>\n' +
    '	            <span class="down-control" ng-click="reversePauseReason = true"></span>\n' +
    '	        </div>\n' +
    '	    </div>\n' +
    '    </div>\n' +
    '    <div class="row">\n' +
    '    	<button type="button" class="back-btn text-center pull-right button-large" ng-click="redirectBack()">Back</button>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/overview/overview.tpl.html',
    '<script>\n' +
    '// Select all tabs\n' +
    '$(\'.nav-tabs a\').click(function() {\n' +
    '    $(this).tab(\'show\');\n' +
    '})\n' +
    '\n' +
    '// Select tab by name\n' +
    '$(\'.nav-tabs a[href="#home"]\').tab(\'show\')\n' +
    '\n' +
    '// Select first tab\n' +
    '$(\'.nav-tabs a:first\').tab(\'show\')\n' +
    '\n' +
    '// Select last tab\n' +
    '$(\'.nav-tabs a:last\').tab(\'show\')\n' +
    '\n' +
    '// Select fourth tab (zero-based)\n' +
    '$(\'.nav-tabs li:eq(3) a\').tab(\'show\')\n' +
    '</script>\n' +
    '<div class="overview">\n' +
    '    <div class="row">\n' +
    '        <div class="col-xs-12 me">\n' +
    '            <div class="col-xs-12">\n' +
    '                <ul class="nav nav-tabs">\n' +
    '                    <li>\n' +
    '                        <a href="">Material reqiurement</a>\n' +
    '                    </li>\n' +
    '                    <li class="active">\n' +
    '                        <a href="">P/E Engineering</a>\n' +
    '                    </li>\n' +
    '                    <li>\n' +
    '                        <a href="">Engineering</a>\n' +
    '                    </li>\n' +
    '                    <li>\n' +
    '                        <a href="">Sub-con</a>\n' +
    '                    </li>\n' +
    '                    <li>\n' +
    '                        <a href="">Review</a>\n' +
    '                    </li>\n' +
    '                    <li>\n' +
    '                        <a href="">Overview</a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-xs-2">\n' +
    '            <p class="text">Operation</p>\n' +
    '        </div>\n' +
    '        <div class="col-xs-7">\n' +
    '            <p class="text">Processes</p>\n' +
    '        </div>\n' +
    '        <div class="col-xs-3">\n' +
    '            <p class="text">Notes</p>\n' +
    '        </div>\n' +
    '        <div class="col-xs-10 col-xs-offset-2">\n' +
    '            <hr>\n' +
    '        </div>\n' +
    '        <div class="col-xs-12">\n' +
    '            <div class="col-xs-2">\n' +
    '                <img src="/assets/images/Milling_Icon_Sml.jpg" alt="">\n' +
    '            </div>\n' +
    '            <div class="col-xs-2">\n' +
    '                <p class="textb">Material Issue</p>\n' +
    '            </div>\n' +
    '            <div class="col-xs-8">\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-4">\n' +
    '                        <span>GRB NO.</span><span><input type="text" value="0003"></span>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-4">\n' +
    '                        <span>IRN NO</span><span><input type="text"></span>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-4">\n' +
    '                        <p>Note about this material</p>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-xs-12 date">\n' +
    '            <div class="row">\n' +
    '                <div class="col-xs-4">\n' +
    '                    <p class="month">7th Nov</p>\n' +
    '                    <p class="day">1</p>\n' +
    '                </div>\n' +
    '                <div class="col-xs-8">\n' +
    '                    <div class="row">\n' +
    '                        <div class="pink col-xs-2">Spec</div>\n' +
    '                        <div class="pink col-xs-1">Shape</div>\n' +
    '                        <div class="pink col-xs-2">UOM</div>\n' +
    '                        <div class="pink col-xs-2">Aappvoral</div>\n' +
    '                        <div class="pink col-xs-2">Size</div>\n' +
    '                        <div class="pink col-xs-1">Qty</div>\n' +
    '                        <div class="pink col-xs-2">Issue Lgth</div>\n' +
    '                        <div class="pink col-xs-12">\n' +
    '                            <hr>\n' +
    '                        </div>\n' +
    '                        <div class="col-xs-2">S07-108</div>\n' +
    '                        <div class="col-xs-1">Diamater</div>\n' +
    '                        <div class="col-xs-2">Lister</div>\n' +
    '                        <div class="col-xs-2">ISO9001</div>\n' +
    '                        <div class="col-xs-1">1</div>\n' +
    '                        <div class="col-xs-2">21</div>\n' +
    '                        <div class="col-xs-2"></div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-xs-5 col-xs-offset-7">\n' +
    '            <div class="row">\n' +
    '                <div class="col-xs-12">\n' +
    '                    <div class="row">\n' +
    '                        <input class="col-xs-4" type="text" value="test">\n' +
    '                        <input class="col-xs-4" type="text" value="test">\n' +
    '                        <input class="col-xs-4" type="text" value="test">\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col-xs-10 col-xs-offset-2">\n' +
    '        <hr>\n' +
    '    </div>\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="col-xs-2">\n' +
    '            <img src="/assets/images/Milling_Icon_Sml.jpg" alt="">\n' +
    '        </div>\n' +
    '        <div class="col-xs-4">\n' +
    '            <p>Manual Turning on Mazak VTC800/30SR</p>\n' +
    '            <p>hold on manddrel and cut serration as per drawing</p>\n' +
    '            <p>leave .005 thread diameter for treatment</p>\n' +
    '        </div>\n' +
    '        <div class="col-xs-2">\n' +
    '            <p>Prog No\'s</p>\n' +
    '            <p>123456789</p>\n' +
    '            <p>45621</p>\n' +
    '        </div>\n' +
    '        <div class="col-xs-4">\n' +
    '            <p>2.1.1 dia 3x5 with 0.2 rada 313.0305.00AS45</p>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="row">\n' +
    '            <div class="col-xs-3">\n' +
    '                <p class="month">7th Nov</p>\n' +
    '                <p class="day">1</p>\n' +
    '            </div>\n' +
    '            <div class="col-xs-9">\n' +
    '                <div class="col-xs-7">\n' +
    '                    <p class="tit">Tooling</p>\n' +
    '                </div>\n' +
    '                <div class="col-xs-5">\n' +
    '                    <p class="tit">Fixtures</p>\n' +
    '                </div>\n' +
    '                <div class="col-xs-6">\n' +
    '                    <table class="table table-bordered">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th>GRB no.</th>\n' +
    '                                <th>Description</th>\n' +
    '                                <th>Type</th>\n' +
    '                                <th>Sub type</th>\n' +
    '                                <th>Status</th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr>\n' +
    '                                <td>810072</td>\n' +
    '                                <td>Caliper</td>\n' +
    '                                <td>Type Milk xxx</td>\n' +
    '                                <td>Bullnose xxxx</td>\n' +
    '                                <td>x</td>\n' +
    '                            </tr>\n' +
    '                            <tr>\n' +
    '                                <td>810072</td>\n' +
    '                                <td>Caliper</td>\n' +
    '                                <td>Type Milk xxx</td>\n' +
    '                                <td>Bullnose xxxx</td>\n' +
    '                                <td>x</td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '                <div class="col-xs-5 col-xs-offset-1">\n' +
    '                    <table class="table table-bordered">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th>GRB no.</th>\n' +
    '                                <th>Description</th>\n' +
    '                                <th>Status</th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr>\n' +
    '                                <td>910073</td>\n' +
    '                                <td>Fluge lbinder basw</td>\n' +
    '                                <td>x</td>\n' +
    '                            </tr>\n' +
    '                            <tr>\n' +
    '                                <td>910073</td>\n' +
    '                                <td>Fluge lbinder basw</td>\n' +
    '                                <td>x</td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col-xs-5 col-xs-offset-7">\n' +
    '        <div class="row">\n' +
    '            <div class="col-xs-12">\n' +
    '                <div class="row">\n' +
    '                    <input class="col-xs-4" type="text" value="test">\n' +
    '                    <input class="col-xs-4" type="text" value="test">\n' +
    '                    <input class="col-xs-4" type="text" value="test">\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col-xs-10 col-xs-offset-2">\n' +
    '        <hr>\n' +
    '    </div>\n' +
    '    <div class="col-xs-2 col-xs-offset-10">\n' +
    '        <button>Print</button>\n' +
    '    </div>\n' +
    '</div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/part-control/part-control.tpl.html',
    '<div class="part-control">\n' +
    '    <div class="row">\n' +
    '        <div class="col-xs-12 page-tab">\n' +
    '            <ul class="nav nav-tabs">\n' +
    '                <li ui-sref-active="active"><a ui-sref="department.part-control.part-list">Parts List</a></li>\n' +
    '                <li ui-sref-active="active"><a ui-sref="department.part-control.assemblies({PTemplateID: PTemplateID})">Assemblies</a></li>\n' +
    '                <li ui-sref-active="active"><a ui-sref="department.part-control.notes({PTemplateID: PTemplateID})">Notes</a></li>\n' +
    '                <li ui-sref-active="active"><a ui-sref="department.part-control.review-tasks({PTemplateID: PTemplateID})">Review tasks</a></li>\n' +
    '                <li ui-sref-active="active"><a ui-sref="department.part-control.docs-newpart({PTemplateID: PTemplateID})">Docs / new Part</a></li>\n' +
    '            </ul>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row">\n' +
    '        <div ui-view></div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/part-history/part-history.tpl.html',
    '<div class="part-history">\n' +
    '    <div class="row">\n' +
    '        <div class="col-xs-12">\n' +
    '            <div class="title-global-page">Part History</div>\n' +
    '        </div>\n' +
    '        <div class="col-xs-7 col-xs-offset-1 left">\n' +
    '            <div class="row">\n' +
    '                <div class="col-xs-10">\n' +
    '                    <div class="area-table">\n' +
    '                        <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_partwo" load-item="PartWOList">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th ng-repeat="item in sortTable" ng-bind="item.title" ng-click="selectOneItem(sortTable, item, \'selected\'); changeSortBy(item)" ng-class="{\'th-active\': item.selected}"></th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="item in PartWOList | orderBy: sortBy | reverse" ng-click="selectOneItem(PartWOList, item, \'selected\'); PartWOSelected(item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                    <td ng-bind="item.CustomerName"></td>\n' +
    '                                    <td ng-bind="item.WONo"></td>\n' +
    '                                    <td ng-bind="item.OrderDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                                    <td ng-bind="item.Completed"></td>\n' +
    '                                    <td ng-bind="item.Qty"></td>\n' +
    '                                    <td ng-bind="item.PriceEach"></td>\n' +
    '                                    <td ng-bind="item.Value"></td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-xs-2">\n' +
    '                    <span class="up-control" ng-click="reverse = false"></span>\n' +
    '                    <span>sort</span>\n' +
    '                    <span class="down-control" ng-click="reverse = true"></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-xs-4 right">\n' +
    '            <div class="row">\n' +
    '                <div class="col-xs-5">\n' +
    '                    <p>Part No.</p>\n' +
    '                    <input class="color-blue border" type="text" ng-model="PartNumber">\n' +
    '                </div>\n' +
    '                <div class="col-xs-3">\n' +
    '                    <p>Issue</p>\n' +
    '                    <input class="issue font-weight content-right" type="text" ng-model="Issue">\n' +
    '                </div>\n' +
    '                <div class="col-xs-4">\n' +
    '                    <input class="color-blue border font-large font-weight content-right" type="text" required ng-model="TimesMade">\n' +
    '                    <h6>Times made</h6>\n' +
    '                </div>\n' +
    '                <div class="col-xs-7 top bot">\n' +
    '                    <input class="color-blue border" type="text" ng-model="COS">\n' +
    '                </div>\n' +
    '                <div class="col-xs-12">\n' +
    '                    <button ng-click="redirectWorkOrder()">View WO</button>\n' +
    '                </div>\n' +
    '                <div class="col-xs-12 top">\n' +
    '                    <p class="algin">Grand total recevired for this part</p>\n' +
    '                    <p class="algin font-weight">₤{{GrandTotal}}</p>\n' +
    '                </div>\n' +
    '                <div class="col-xs-6 algin">Length : </div>\n' +
    '                <div class="col-xs-6 bot help-block" ng-bind="PartWO.Length"></div>\n' +
    '                <div class="col-xs-6 algin">Width</div>\n' +
    '                <div class="col-xs-6 bot help-block" ng-bind="PartWO.Width"></div>\n' +
    '                <div class="col-xs-6 algin">Height</div>\n' +
    '                <div class="col-xs-6 bot help-block" ng-bind="PartWO.Height"></div>\n' +
    '                <div class="col-xs-6 algin">Weight</div>\n' +
    '                <div class="col-xs-6 bot help-block" ng-bind="PartWO.Weight"></div>\n' +
    '                <div class="col-xs-6 algin">Appoval</div>\n' +
    '                <div class="col-xs-6 bot help-block" ng-bind="PartWO.Approval"></div>\n' +
    '                <div class="col-xs-6 content-right">Finish</div>\n' +
    '                <div class="col-xs-6 bot help-block" ng-bind="PartWO.Finish"></div>\n' +
    '                <div class="col-xs-6 algin">Time</div>\n' +
    '                <div class="col-xs-6 bot help-block" ng-bind="PartWO.Time"></div>\n' +
    '                <div class="col-xs-12">\n' +
    '                    <button ng-click="redirectBack()">Back</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/production-schedule/production-schedule.tpl.html',
    '<div class="production-page">\n' +
    '    <div class="page-body">\n' +
    '        <div class="row box content">\n' +
    '            <div class="title-global-page">Production Schedule</div>\n' +
    '            <div class="list-tabs">\n' +
    '                <ul class="tabs">\n' +
    '                    <li>\n' +
    '                        <a ui-sref-active="active" ui-sref="department.production-schedule.material-issue">\n' +
    '                            Material Issue\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                    <li ng-repeat="value in MachineTab">\n' +
    '                        <a ui-sref="department.production-schedule.machine({machine: value.MachineAlias})" ng-class="{\'active\': machineParams == value.MachineAlias}">\n' +
    '                            {{value.Machine}}\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                    <li>\n' +
    '                        <a ui-sref-active="active" ui-sref="department.production-schedule.subcon">\n' +
    '                            Sub-con\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '            <div class="tab_container">\n' +
    '                <div ui-view></div>\n' +
    '                <div class="col-sm-12 button-active no-padding">\n' +
    '                    <div class="area-BPCButton">\n' +
    '                        <div class="area-radio">\n' +
    '                            <div class="set-radio">\n' +
    '                                set\n' +
    '                                <input type="radio" ng-model="OperationStatus.SetRun" ng-value="0" disabled></input>\n' +
    '                            </div>\n' +
    '                            <div class="run-radio">\n' +
    '                                run\n' +
    '                                <input type="radio" ng-model="OperationStatus.SetRun" ng-value="1" disabled></input>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div ng-show="showBegin" class="button-begin button-main custom-width">\n' +
    '                            <div>\n' +
    '                                <p ng-click="BeginOperator(OperationStatus.SetRun)">\n' +
    '                                    <a href=""><img src="../../../../../../assets/images/Right_Arrow.png"></a>\n' +
    '                                </p>\n' +
    '                                <p>Begin</p>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div ng-show="showBeginHighlight" class="button-begin button-main custom-width">\n' +
    '                            <div>\n' +
    '                                <p ng-click="BeginOperator(OperationStatus.SetRun)">\n' +
    '                                    <a href=""><img src="../../../../../../assets/images/PlayButtonHighlighted.JPG"></a>\n' +
    '                                </p>\n' +
    '                                <p>Begin</p>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div ng-show="showPause" class="button-pause button-main custom-width">\n' +
    '                            <div>\n' +
    '                                <select ng-model="Reason" ng-options="option as option.name for option in Reasons">\n' +
    '                                </select>\n' +
    '                                <p class="pause-icon" ng-click="PauseOperator(Reason)">\n' +
    '                                    <a href=""><img src="../../../../../../assets/images/Pause.png"></a>\n' +
    '                                </p>\n' +
    '                                <p>Pause</p>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div ng-show="showPauseHighlight" class="button-pause button-main custom-width">\n' +
    '                            <div>\n' +
    '                                <select ng-model="Reason" ng-options="option as option.name for option in Reasons">\n' +
    '                                </select>\n' +
    '                                <p class="pause-icon" ng-click="PauseOperator(Reason)">\n' +
    '                                    <a href=""><img src="../../../../../../assets/images/PauseButtonHighLighted.JPG"></a>\n' +
    '                                </p>\n' +
    '                                <p>Pause</p>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div ng-show="showComplete" class="button-complete button-main custom-width">\n' +
    '                            <p ng-click="CompleteOperator()">\n' +
    '                                <a href=""><img src="../../../../../../assets/images/stopsmall.PNG"></a>\n' +
    '                            </p>\n' +
    '                            <p>Complete</p>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="allocate-area">\n' +
    '                        <div class="selected text-right custom-width">\n' +
    '                            <p>Allocate secondary resource to selected job</p>\n' +
    '                            <select ng-model="ResourceUser" ng-options="option as option.UName for option in ResourceList">\n' +
    '                            </select>\n' +
    '                        </div>\n' +
    '                        <div class="bg-img add-resource">\n' +
    '                            <p class="buttun-first"><a href="javascript:;" ng-click="addResource(ResourceUser)">Add Resource</a></p>\n' +
    '                            <p><a class="line" href="javascript:;" ng-click="refreshProduction()">Refresh</a></p>\n' +
    '                        </div>\n' +
    '                        <div class="bg-img late-report lonely">\n' +
    '                            <p></p>\n' +
    '                            <p><a href="javascript:;" ng-click="openBIRT()">\'Late\' Report</a></p>\n' +
    '                        </div>\n' +
    '                        <div class="bg-img view-wo lonely">\n' +
    '                            <p></p>\n' +
    '                            <p><a href="javascript:;" ng-click="ViewWO()">View WO Detail</a></p>\n' +
    '                        </div>\n' +
    '                        <div class="bg-img back lonely">\n' +
    '                            <p> </p>\n' +
    '                            <p><a class="line" ng-click="redirectBack()">Back</a></p>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/purchase-order/purchase-order.tpl.html',
    '<div class="purchase-order">\n' +
    '    <div class="page-body">\n' +
    '        <div class="title-global-page">Purchase Order</div>\n' +
    '        <div class="page-top">\n' +
    '            <div class="top-item col-md-11">\n' +
    '                <div class="row row-1">\n' +
    '                    <div class="col-md-10 area">\n' +
    '                        <div class="area-1 col-md-6">\n' +
    '                            <div class="area-title col-md-4 no-padding">\n' +
    '                                Part No. search\n' +
    '                            </div>\n' +
    '                            <div class="area-input col-md-6 no-padding">\n' +
    '                                <search-input search-input-model="searchInputModel" procedure="ProcPartSearchList" search-key="SearchField" ng-model-options="{debounce: 500}" display-key="PartNumber | Issue | Qty | Description | PONo | Raised | RFQNo" on-select="PartNoSelected"></search-input>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="area-2 col-md-3">\n' +
    '                            <div class="area-title col-md-6 no-padding">\n' +
    '                                PO search\n' +
    '                            </div>\n' +
    '                            <div class="area-input col-md-6 no-padding">\n' +
    '                                <search-input search-input-model="searchPOModel" procedure="ProcPOSearchList" search-key="POSearchField" ng-model-options="{debounce: 500}" display-key="PONo" on-select="PONoSelected"></search-input>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="area-3 col-md-3 no-padding">\n' +
    '                            <div class="area-title col-md-6 no-padding-left">\n' +
    '                                RFQ search\n' +
    '                            </div>\n' +
    '                            <div class="area-input col-md-6 no-padding-left">\n' +
    '                                <search-input search-input-model="searchRFQModel" procedure="ProcRFQSearchList" search-key="RFQSearchField" ng-model-options="{debounce: 500}" display-key="RFQNo" on-select="RFQNoSelected"></search-input>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row row-3">\n' +
    '                    <div class="area">\n' +
    '                        <span class="area-1">\n' +
    '                                <label for="">selected RFQ</label>\n' +
    '                                <input type="text" ng-model="PurchaseOrderDetail.RFQNo" readonly>\n' +
    '                            </span>\n' +
    '                        <span class="area-2">\n' +
    '                                <label for="">selected PO</label>\n' +
    '                                <input type="text" ng-model="PurchaseOrderDetail.PONo" readonly>\n' +
    '                            </span>\n' +
    '                        <span class="area-3">\n' +
    '                                <label for="">Raised</label>\n' +
    '                                <input type="text" ng-model="PurchaseOrderDetail.Raised">\n' +
    '                            </span>\n' +
    '                        <span class="area-4">\n' +
    '                                <label for="">Complete</label>\n' +
    '                                <input type="checkbox" value="1" ng-model="PurchaseOrderDetail.Complete" ng-checked="PurchaseOrderDetail.Complete == 1" ng-click="POComplete()">\n' +
    '                            </span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="top-button col-md-1">\n' +
    '                <button class="new-purchase" ng-click="AddNewRFQ()">New</button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="page-tab">\n' +
    '            <ul class="nav nav-tabs">\n' +
    '                <li ui-sref-active="active"><a ui-sref="department.purchase-order.details({RFQNo: PurchaseOrderDetail.RFQNo})">Details</a></li>\n' +
    '                <li ui-sref-active="active"><a ui-sref="department.purchase-order.instructions({RFQNo: PurchaseOrderDetail.RFQNo, PONo: PurchaseOrderDetail.PONo, PartNumber: PurchaseOrderDetail.PartNumber, Issue: PurchaseOrderDetail.Issue, Qty: PurchaseOrderDetail.Qty, Raised: PurchaseOrderDetail.Raised, Description: PurchaseOrderDetail.Description})">Instructions / Comments</a></li>\n' +
    '                <li ui-sref-active="active"><a ui-sref="department.purchase-order.supplier({RFQNo: PurchaseOrderDetail.RFQNo, PONo: PurchaseOrderDetail.PONo, PartNumber: PurchaseOrderDetail.PartNumber, Issue: PurchaseOrderDetail.Issue, Qty: PurchaseOrderDetail.Qty, Raised: PurchaseOrderDetail.Raised, Description: PurchaseOrderDetail.Description})">Supplier & Approval</a></li>\n' +
    '                <li ui-sref-active="active"><a ui-sref="department.purchase-order.rfq({RFQNo: PurchaseOrderDetail.RFQNo, PONo: PurchaseOrderDetail.PONo, PartNumber: PurchaseOrderDetail.PartNumber, Issue: PurchaseOrderDetail.Issue, Qty: PurchaseOrderDetail.Qty, Raised: PurchaseOrderDetail.Raised, Description: PurchaseOrderDetail.Description})">R.F.Q.</a></li>\n' +
    '                <li ui-sref-active="active"><a ui-sref="department.purchase-order.raise({RFQNo: PurchaseOrderDetail.RFQNo, PONo: PurchaseOrderDetail.PONo, PartNumber: PurchaseOrderDetail.PartNumber, Issue: PurchaseOrderDetail.Issue, Qty: PurchaseOrderDetail.Qty, Raised: PurchaseOrderDetail.Raised, Description: PurchaseOrderDetail.Description})">Raise/Copy PO</a></li>\n' +
    '                <li ui-sref-active="active"><a ui-sref="department.purchase-order.notes({RFQNo: PurchaseOrderDetail.RFQNo, PONo: PurchaseOrderDetail.PONo, PartNumber: PurchaseOrderDetail.PartNumber, Issue: PurchaseOrderDetail.Issue, Qty: PurchaseOrderDetail.Qty, Raised: PurchaseOrderDetail.Raised, Description: PurchaseOrderDetail.Description})">Notes and follow up</a></li>\n' +
    '                <li ui-sref-active="active"><a ui-sref="department.purchase-order.material-search({RFQNo: PurchaseOrderDetail.RFQNo, PONo: PurchaseOrderDetail.PONo, PartNumber: PurchaseOrderDetail.PartNumber, Issue: PurchaseOrderDetail.Issue, Qty: PurchaseOrderDetail.Qty, Raised: PurchaseOrderDetail.Raised, Description: PurchaseOrderDetail.Description})">Material search</a></li>\n' +
    '                <li ui-sref-active="active"><a ui-sref="department.purchase-order.invoice({RFQNo: PurchaseOrderDetail.RFQNo, PONo: PurchaseOrderDetail.PONo, PartNumber: PurchaseOrderDetail.PartNumber, Issue: PurchaseOrderDetail.Issue, Qty: PurchaseOrderDetail.Qty, Raised: PurchaseOrderDetail.Raised, Description: PurchaseOrderDetail.Description})">Invoice</a></li>\n' +
    '                <li ui-sref-active="active"><a ui-sref="department.purchase-order.quality({RFQNo: PurchaseOrderDetail.RFQNo, PONo: PurchaseOrderDetail.PONo, PartNumber: PurchaseOrderDetail.PartNumber, Issue: PurchaseOrderDetail.Issue, Qty: PurchaseOrderDetail.Qty, Raised: PurchaseOrderDetail.Raised, Description: PurchaseOrderDetail.Description})">Quality</a></li>\n' +
    '            </ul>\n' +
    '            <div class="tab-content">\n' +
    '                <div ui-view></div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/remarks-comments/remarks-comments.tpl.html',
    '<div class="remark-comment-page">\n' +
    '    <div class="title-global-page">Remarks Comments</div>\n' +
    '    <div class="row">\n' +
    '        <div class="content col-md-12">\n' +
    '            <div class="remarks col-md-6">\n' +
    '                <div class="title-button">\n' +
    '                    <span class="title">Standard Remarks</span>\n' +
    '                    <button ng-click="AddRemark(Remark)">Add</button>\n' +
    '                </div>\n' +
    '                <div class="search">\n' +
    '                    <input type="text" ng-model="Remark" maxlength="100">\n' +
    '                </div>\n' +
    '                <div class="area-table">\n' +
    '                    <table ng-show="Remarks | hasItem">\n' +
    '                        <tr ng-repeat="item in Remarks" ng-click="selectItem(item, \'selected\')">\n' +
    '                            <td>\n' +
    '                                <span>{{item.Remark}}</span>\n' +
    '                                <span ng-show="item.selected == true" class="pull-right glyphicon glyphicon-ok"></span>\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '                <div class="delete">\n' +
    '                    <button type="submit" ng-click="RemoveRemark(Remarks)">Delete</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="comments col-md-6">\n' +
    '                <div class="title-button">\n' +
    '                    <span class="title">Standard comments</span>\n' +
    '                    <button ng-click="AddComment(Comment)">Add</button>\n' +
    '                </div>\n' +
    '                <div class="search">\n' +
    '                    <input type="text" ng-model="Comment" maxlength="100">\n' +
    '                </div>\n' +
    '                <div class="area-table">\n' +
    '                    <table ng-show="Comments | hasItem">\n' +
    '                        <tr ng-repeat="item in Comments" ng-click="selectItem(item, \'selected\')">\n' +
    '                            <td>\n' +
    '                                <span>{{item.Comment}}</span>\n' +
    '                                <span ng-show="item.selected == true" class="pull-right glyphicon glyphicon-ok"></span>\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '                <div class="delete">\n' +
    '                    <button type="submit" ng-click="RemoveComment(Comments)">Delete</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="page-footer col-md-12">\n' +
    '            <button type="submit" ng-click="redirectBack()">Back</button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/staff-room/staff-room.tpl.html',
    '<div class="staff-room-page">\n' +
    '    <div class="title-global-page">Staff Room</div>\n' +
    '    <div class="page-body">\n' +
    '        <form ng-submit="StaffForm.$valid && SaveOrUpdateStaff(StaffDetail)" name="StaffForm" novalidate>\n' +
    '            <div class="row">\n' +
    '                <div class="staff-info col-xs-12">\n' +
    '                    <div class="row">\n' +
    '                        <div class="staff-credit col-xs-3">\n' +
    '                            <div class="row">\n' +
    '                                <div class="select-staff col-xs-12">\n' +
    '                                    <p>Please select staff member:</p>\n' +
    '                                    <search-input search-input-model="searchInputModel" procedure="ProcGetStaffList" search-key="SearchString" ng-model-options="{debounce: 500}" display-key="Name" on-select="StaffSelected"></search-input>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="po-limit margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title content-right col-xs-8">\n' +
    '                                            Individual PO limit\n' +
    '                                        </div>\n' +
    '                                        <div class="col-xs-4 content">\n' +
    '                                            <input type="text" name="POLimit" ng-model="StaffDetail.POLimit" required ng-pattern="/^\\d+$/">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.POLimit.$error.required">\n' +
    '                                                Please enter PO limit\n' +
    '                                            </span>\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.POLimit.$error.pattern">\n' +
    '                                                PO limit must be number\n' +
    '                                            </span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="po-limit col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title content-right col-xs-8">\n' +
    '                                            Mthly credit / spend limit\n' +
    '                                        </div>\n' +
    '                                        <div class="col-xs-4 content">\n' +
    '                                            <input type="text" name="SpendLimit" ng-model="StaffDetail.SpendLimit" required ng-pattern="/^\\d+$/">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="error col-xs-12">\n' +
    '                                    <span ng-show="StaffForm.$submitted && StaffForm.SpendLimit.$error.required">Please enter spend limit</span>\n' +
    '                                    <span ng-show="StaffForm.$submitted && StaffForm.SpendLimit.$error.pattern">spend limit must be number</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="fork-first col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="fork-lift col-xs-12">\n' +
    '                                            <div class="row">\n' +
    '                                                <div class="title col-xs-8">&nbsp;</div>\n' +
    '                                                <div class="yes-title col-xs-2">Yes</div>\n' +
    '                                                <div class="no-title col-xs-2">No</div>\n' +
    '                                            </div>\n' +
    '                                            <div class="row">\n' +
    '                                                <div class="title col-xs-8">\n' +
    '                                                    Fork lift Operator\n' +
    '                                                </div>\n' +
    '                                                <div class="col-xs-2 yes-option">\n' +
    '                                                    <input type="radio" name="fork" ng-model="StaffDetail.ForkOperator" ng-checked="StaffDetail.ForkOperator == 1" value="1">\n' +
    '                                                </div>\n' +
    '                                                <div class="col-xs-2 no-option">\n' +
    '                                                    <input type="radio" name="fork" ng-model="StaffDetail.ForkOperator" ng-checked="StaffDetail.ForkOperator == 0" value="0">\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="first-aid col-xs-12">\n' +
    '                                            <div class="row">\n' +
    '                                                <div class="title col-xs-8">&nbsp;</div>\n' +
    '                                                <div class="yes-title col-xs-2">Yes</div>\n' +
    '                                                <div class="no-title col-xs-2">No</div>\n' +
    '                                            </div>\n' +
    '                                            <div class="row">\n' +
    '                                                <div class="title col-xs-8">\n' +
    '                                                    First Aid Rep.\n' +
    '                                                </div>\n' +
    '                                                <div class="col-xs-2 yes-option">\n' +
    '                                                    <input type="radio" name="first" ng-model="StaffDetail.FirstAid" ng-checked="StaffDetail.FirstAid == 1" value="1">\n' +
    '                                                </div>\n' +
    '                                                <div class="col-xs-2 no-option">\n' +
    '                                                    <input type="radio" name="first" ng-model="StaffDetail.FirstAid" ng-checked="StaffDetail.FirstAid == 0" value="0">\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="leave-days margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title content-right col-xs-8">\n' +
    '                                            Leave days / annum\n' +
    '                                        </div>\n' +
    '                                        <div class="col-xs-3 col-xs-offset-1 content">\n' +
    '                                            <input type="text" name="LeaveDays" ng-model="StaffDetail.LeaveDays" required ng-pattern="/^\\d+$/">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="error col-xs-12">\n' +
    '                                    <span ng-show="StaffForm.$submitted && StaffForm.LeaveDays.$error.required">Please enter Leave days / annum</span>\n' +
    '                                    <span ng-show="StaffForm.$submitted && StaffForm.LeaveDays.$error.pattern">Leave days / annum must be number</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="annual-salary margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title content-right col-xs-8">\n' +
    '                                            Annual Salary\n' +
    '                                        </div>\n' +
    '                                        <div class="col-xs-4 content">\n' +
    '                                            <input type="text" ng-model="StaffDetail.Salary" name="Salary" required ng-pattern="/^\\d+$/">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="error col-xs-12">\n' +
    '                                    <span ng-show="StaffForm.$submitted && StaffForm.Salary.$error.required">Please enter Annual Salary</span>\n' +
    '                                    <span ng-show="StaffForm.$submitted && StaffForm.Salary.$error.pattern">Annual Salary must be number</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="currently-employed col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-8">&nbsp;</div>\n' +
    '                                        <div class="yes-title col-xs-2">Yes</div>\n' +
    '                                        <div class="no-title col-xs-2">No</div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-8">\n' +
    '                                            Currently employed?\n' +
    '                                        </div>\n' +
    '                                        <div class="col-xs-2 yes-option">\n' +
    '                                            <input type="radio" ng-model="StaffDetail.Employed" name="Employed" ng-checked="StaffDetail.Employed == 1">\n' +
    '                                        </div>\n' +
    '                                        <div class="col-xs-2 no-option">\n' +
    '                                            <input type="radio" ng-model="StaffDetail.Employed" name="Employed" ng-checked="StaffDetail.Employed == 0">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="staff-address col-xs-6">\n' +
    '                            <div class="row">\n' +
    '                                <div class="payroll margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Payroll No.\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-9">\n' +
    '                                            <span ng-bind="StaffDetail.PayrollNumber"></span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="name margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Name\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-7">\n' +
    '                                            <input type="text" ng-model="StaffDetail.FullName" name="FullName" maxlength="25" required>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.FullName.$error.required">Please enter Name</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="address1 margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Address line 1\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-7">\n' +
    '                                            <input type="text" ng-model="StaffDetail.Address1" name="Address1" maxlength="45" required>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.Address1.$error.required">Please enter Address line 1</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="address2 margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Address line 2\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-7">\n' +
    '                                            <input type="text" ng-model="StaffDetail.Address2" maxlength="40">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="address3 margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Address line 3\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-7">\n' +
    '                                            <input type="text" ng-model="StaffDetail.Address3" maxlength="40">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="address4 margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Address line 4\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-6">\n' +
    '                                            <input type="text" ng-model="StaffDetail.Address4" maxlength="30">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="postcode margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Postcode\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-3">\n' +
    '                                            <input type="text" ng-model="StaffDetail.Postcode" name="Postcode" maxlength="8" required>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.Postcode.$error.required">Please enter Postcode</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="homephone margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Home phone\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-4">\n' +
    '                                            <input type="text" ng-model="StaffDetail.HomePhone" name="HomePhone" maxlength="20" required ng-pattern="/^0|[1-9][0-9]*$/">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.HomePhone.$error.required">Please enter Home phone</span>\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.HomePhone.$error.pattern">Home phone must be number</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="mobile margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Mobile\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-4">\n' +
    '                                            <input type="text" ng-model="StaffDetail.MobilePhone" maxlength="20" name="MobilePhone" required ng-pattern="/^0|[1-9][0-9]*$/">\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.MobilePhone.$error.required">Please enter Mobile</span>\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.MobilePhone.$error.pattern">Mobile must be number</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="email margin-bottom-10 col-xs-12">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-3">\n' +
    '                                            Email\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-9">\n' +
    '                                            <input type="email" ng-model="StaffDetail.Email" maxlength="50" name="Email" required>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="error col-xs-12">\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.Email.$error.required">Please enter Email</span>\n' +
    '                                            <span ng-show="StaffForm.$submitted && StaffForm.Email.$error.email">Please enter a valid email</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="staff-security col-xs-3">\n' +
    '                            <div class="row">\n' +
    '                                <div class="contracted-hours col-xs-8">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-12 color-dark-blue border bold">\n' +
    '                                            Contracted hours\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-12">\n' +
    '                                            <input type="text" class="hour" ng-model="StaffDetail.Hours" name="Hours" required ng-pattern="/^\\d+$/">\n' +
    '                                            <span>hours per</span>\n' +
    '                                            <select name="" id="" class="hour-type">\n' +
    '                                                <option value="">Week</option>\n' +
    '                                            </select>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="error col-xs-12">\n' +
    '                                    <span ng-show="StaffForm.$submitted && StaffForm.Hours.$error.required">Please enter Hour</span>\n' +
    '                                    <span ng-show="StaffForm.$submitted && StaffForm.Hours.$error.pattern">Hour must be number</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="security-access col-xs-8">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="title col-xs-12 color-dark-blue bold">\n' +
    '                                            Security access\n' +
    '                                        </div>\n' +
    '                                        <div class="content col-xs-12">\n' +
    '                                            <div class="row user">\n' +
    '                                                <div class="col-xs-9">\n' +
    '                                                    <p>User</p>\n' +
    '                                                </div>\n' +
    '                                                <div class="col-xs-3">\n' +
    '                                                    <input type="radio" ng-model="StaffDetail.Security" name="Security" value="1">\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                            <div class="row manager">\n' +
    '                                                <div class="col-xs-9">\n' +
    '                                                    <p>Manager</p>\n' +
    '                                                </div>\n' +
    '                                                <div class="col-xs-3">\n' +
    '                                                    <input type="radio" ng-model="StaffDetail.Security" name="Security" value="2">\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                            <div class="row super-user">\n' +
    '                                                <div class="col-xs-9">\n' +
    '                                                    <p>Super User</p>\n' +
    '                                                </div>\n' +
    '                                                <div class="col-xs-3">\n' +
    '                                                    <input type="radio" ng-model="StaffDetail.Security" name="Security" value="3">\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="system-assigned-contract col-xs-12">\n' +
    '                    <div class="row">\n' +
    '                        <div class="system-assigned col-xs-9">\n' +
    '                            <div class="row">\n' +
    '                                <div class="system-role col-xs-4">\n' +
    '                                    <div class="title col-xs-8 color-dark-blue border bold">\n' +
    '                                        System Role list\n' +
    '                                    </div>\n' +
    '                                    <div class="content clear-fix background-light-yellow color-blue">\n' +
    '                                        <p ng-repeat="item in SystemRoles" ng-click="selectItem(item, \'selected\')" ng-bind="item.Role" ng-class="{\'selected\': item.selected}">\n' +
    '                                        </p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="system-button col-xs-1">\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="add col-xs-12 no-padding">\n' +
    '                                            <button type="button" ng-click="AddAssignedRole(SystemRoles)">Add</button> >\n' +
    '                                        </div>\n' +
    '                                        <div class="rmv col-xs-12 no-padding">\n' +
    '                                            < <button type="button" ng-click="RmvAssignedRole(StaffRoles)">Rmv</button>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="assigned-role col-xs-4">\n' +
    '                                    <div class="title col-xs-8 color-dark-blue border bold">\n' +
    '                                        Assigned Roles\n' +
    '                                    </div>\n' +
    '                                    <div class="content clear-fix background-light-yellow color-blue">\n' +
    '                                        <p ng-repeat="item in StaffRoles" ng-click="selectItem(item, \'selected\')" ng-bind="item.Role" ng-class="{\'selected\': item.selected}">\n' +
    '                                        </p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="save-area col-xs-3" ng-hide="StaffUserID">\n' +
    '                                    <button type="submit">Save</button>\n' +
    '                                </div>\n' +
    '                                <div class="save-area col-xs-3" ng-show="StaffUserID">\n' +
    '                                    <button type="submit">Update</button>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="contract col-xs-3">\n' +
    '                            <div class="title col-xs-12">\n' +
    '                                Add Contract of Employment:\n' +
    '                            </div>\n' +
    '                            <div class="content col-xs-12">\n' +
    '                                <button dropzone on-success="uploadSuccess" before-upload="beforeUpload" type="button" max-file-name="30">Drop Contract here</button>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="first-role-new-member col-md-9">\n' +
    '                    <div class="row">\n' +
    '                        <div class="first-role content-right col-md-9">\n' +
    '                            First role is primary\n' +
    '                        </div>\n' +
    '                        <div class="new-member col-md-3">\n' +
    '                            <button type="button" ng-click="NewMember()">New Member</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="back col-md-3">\n' +
    '                    <button ng-click="redirectBack()">Back</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </form>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/store-admin/store-admin.tpl.html',
    '<div class="admin-page">\n' +
    '    <div class="title-global-page">Store Admin</div>\n' +
    '    <div class="page-body">\n' +
    '        <div class="store-locations col-md-4 no-padding-left">\n' +
    '            <div class="store-locations-table">\n' +
    '                <div class="table-control col-md-5 no-padding-left">\n' +
    '                    <p class="control-title right-content">Add new</p>\n' +
    '                    <input type="text" class="col-md-12 no-padding" ng-model="StoreArea" maxlength="15">\n' +
    '                    <div class="button-control col-md-12 no-padding">\n' +
    '                        <div class="add right-content">\n' +
    '                            <button type="button" ng-click="AddStoreArea(StoreArea)">Add</button>\n' +
    '                        </div>\n' +
    '                        <div class="delete right-content">\n' +
    '                            <button type="button" ng-click="RemoveStoreArea(StoreAreaList)">Delete</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="table-content col-md-7">\n' +
    '                    <div class="row">\n' +
    '                        <div class="table-title col-md-10 right-content">Store Locations</div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="area-table col-md-10 no-padding">\n' +
    '                            <table ng-show="StoreAreaList | hasItem">\n' +
    '                                <tr ng-repeat="item in StoreAreaList" ng-click="selectOneItem(StoreAreaList, item); selectStoreArea(item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                    <td ng-bind="item.StoreArea"></td>\n' +
    '                                </tr>\n' +
    '                            </table>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="couriers-table">\n' +
    '                <div class="table-control col-md-5 no-padding-left">\n' +
    '                    <p class="control-title right-content">Couriers</p>\n' +
    '                    <p class="control-title right-content">Add new</p>\n' +
    '                    <input type="text" class="col-md-12 no-padding" ng-model="Courier" maxlength="25">\n' +
    '                    <div class="button-control col-md-12 no-padding">\n' +
    '                        <div class="add right-content">\n' +
    '                            <button ng-click="AddCourier(Courier)">Add</button>\n' +
    '                        </div>\n' +
    '                        <div class="delete right-content">\n' +
    '                            <button ng-click="RemoveCourier(CourierList)">Delete</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="table-content col-md-7">\n' +
    '                    <div class="row">\n' +
    '                        <div class="area-table col-md-10 no-padding">\n' +
    '                            <table ng-show="CourierList | hasItem">\n' +
    '                                <tr ng-repeat="item in CourierList | orderBy: \'Name\' : reverse" ng-click="selectItem(item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                    <td ng-bind="item.Name"></td>\n' +
    '                                </tr>\n' +
    '                            </table>\n' +
    '                        </div>\n' +
    '                        <div class="area-sort col-md-2">\n' +
    '                            <span class="up-control" ng-click="reverse = false"></span>\n' +
    '                            <span>sort</span>\n' +
    '                            <span class="down-control" ng-click="reverse = true"></span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="x-co-ordinate col-md-4">\n' +
    '            <div class="table-control col-md-5 no-padding-left">\n' +
    '                <p class="control-title right-content">Add new</p>\n' +
    '                <input type="text" class="col-md-12 no-padding" ng-model="XLocation">\n' +
    '                <div class="button-control col-md-12 no-padding">\n' +
    '                    <div class="add right-content">\n' +
    '                        <button ng-click="AddXLocation(StoreAreaList, XLocation)">Add</button>\n' +
    '                    </div>\n' +
    '                    <div class="delete right-content">\n' +
    '                        <button ng-click="RemoveXLocation(StoreAreaList, XStoreLocations)">Delete</button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="table-content col-md-7">\n' +
    '                <div class="row">\n' +
    '                    <div class="table-title col-md-10 right-content">X co-ordinate</div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="area-table col-md-10 no-padding">\n' +
    '                        <table ng-show="XStoreLocations | hasItem">\n' +
    '                            <tr ng-repeat="item in XStoreLocations" ng-click="selectItem(item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                <td>\n' +
    '                                    <span ng-bind="item.XLocation"></span>\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="y-co-ordinate col-md-4">\n' +
    '            <div class="table-control col-md-5 no-padding-left">\n' +
    '                <p class="control-title right-content">Add new</p>\n' +
    '                <input type="text" class="col-md-12 no-padding" ng-model="YLocation">\n' +
    '                <div class="button-control col-md-12 no-padding">\n' +
    '                    <div class="add right-content">\n' +
    '                        <button type="button" ng-click="AddYLocation(StoreAreaList, YLocation)">Add</button>\n' +
    '                    </div>\n' +
    '                    <div class="delete right-content">\n' +
    '                        <button type="button" ng-click="RemoveYLocation(StoreAreaList, YStoreLocations)">Delete</button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="table-content col-md-7">\n' +
    '                <div class="row">\n' +
    '                    <div class="table-title col-md-6 no-padding right-content">Y co-ordinate</div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="area-table col-md-6 no-padding">\n' +
    '                        <table ng-show="YStoreLocations | hasItem">\n' +
    '                            <tr ng-repeat="item in YStoreLocations" ng-click="selectItem(item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                <td ng-bind="item.YLocation"></td>\n' +
    '                            </tr>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="button-footer">\n' +
    '            <button type="button" class="back" ng-click="redirectBack()">Back</button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/supply-chain/supply-chain.tpl.html',
    '<div ui-view></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/wo-detail/wo-detail.tpl.html',
    '<div class="row" id="wo-detail">\n' +
    '    <div class="title-global-page">Work Order Detail</div>\n' +
    '    <div class="col-xs-3">\n' +
    '        <h1 class="title color-blue">Part No. {{WODetail.PartNumber}}</h1>\n' +
    '    </div>\n' +
    '    <div class="col-xs-3 text-center color-blue">Issue {{WODetail.Issue}}</div>\n' +
    '    <div class="col-xs-3 text-center view-drawing">\n' +
    '        <button type="button" ng-click="ViewDrawing()" class="text-center top-button">View Drawing</button>\n' +
    '    </div>\n' +
    '    <div class="col-xs-3 content-right">\n' +
    '        <p><strong>Drawing No:</strong>{{WODetail.DrawingNumber}}</p>\n' +
    '    </div>\n' +
    '    <div class="row machine no-margin">\n' +
    '        <div class="col-xs-4 col-xs-offset-1 left">\n' +
    '            <p class="col-xs-12 no-padding"><span class="blue col-xs-5 content-right">Machine::</span> <span class="col-xs-7">{{WODetail.Machine}}</span></p>\n' +
    '            <p class="col-xs-12 no-padding"><span class="blue col-xs-5 content-right">Batch No:</span> <span class="zen col-xs-3 content-right">{{WODetail.GRBID}}</span></p>\n' +
    '            <p class="col-xs-12 no-padding"><span class="blue col-xs-5 content-right">Customer Order:</span><span class="zen col-xs-3 content-right" ng-bind="WODetail.OrderID"></span></p>\n' +
    '        </div>\n' +
    '        <div class="area-table fo-notes-table col-xs-7 ">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_front">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th>Front Office Notes</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in WOFONotes">\n' +
    '                        <td ng-bind="item.Note" align="center"></td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row work-order no-margin">\n' +
    '        <div class="table-title col-xs-6 no-padding content-right">\n' +
    '            <strong>Work Order No.: </strong>{{WorkOrderID}}\n' +
    '        </div>\n' +
    '        <div class="area-table operation-table col-xs-6">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_operation">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th align="center">Operation</th>\n' +
    '                        <th align="center">Set</th>\n' +
    '                        <th align="center">Run</th>\n' +
    '                        <th align="center">Operator</th>\n' +
    '                        <th align="center">Bypass</th>\n' +
    '                        <th align="center">Subcon</th>\n' +
    '                        <th align="center">Comp</th>\n' +
    '                        <th align="center">Swap?</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in WOOpsDetail" ng-click="selectOneItem(WOOpsDetail, item, \'selected\'); SelectedOperation(item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                        <td align="center" ng-bind="item.Operation"></td>\n' +
    '                        <td align="center" ng-bind="item.ActualSetTime"></td>\n' +
    '                        <td align="center" ng-bind="item.ActualRunTime"></td>\n' +
    '                        <td align="center" ng-bind="item.Operator"></td>\n' +
    '                        <td align="center">\n' +
    '                            <input type="checkbox" ng-model="item.Bypass" ng-true-value="1" ng-false-value="0" ng-show="item.OperationTypeID == 2 || item.OperationTypeID == 3" ng-click="UpdateOperation(item)" />\n' +
    '                        </td>\n' +
    '                        <td align="center">\n' +
    '                            <input type="checkbox" ng-model="item.Subcon" ng-true-value="1" ng-false-value="0" ng-show="item.OperationTypeID == 2" ng-click="UpdateOperation(item)" />\n' +
    '                        </td>\n' +
    '                        <td align="center" align="center">\n' +
    '                            <img ng-show="item.Complete == 1" class="img-responsive" src="../../assets/images/SmallTick.png">\n' +
    '                        </td>\n' +
    '                        <td align="center">\n' +
    '                            <input type="checkbox" ng-model="item.Swap" ng-true-value="1" ng-false-value="0" ng-show="item.OperationTypeID == 2" ng-click="SwapOp(WOOpsDetail)" />\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '        <div class="swap-repeat col-xs-2">\n' +
    '            <div class="swap col-xs-12 no-padding">\n' +
    '                <button type="button">Swap Ops</button>\n' +
    '            </div>\n' +
    '            <div class="repeat col-xs-12 no-padding">\n' +
    '                <button type="button">Repeat</button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="table-title col-xs-4 no-padding content-right">\n' +
    '            <strong>&nbsp;</strong>\n' +
    '        </div>\n' +
    '        <div class="area-table process-notes-table col-xs-4">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_processes">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th>Processes</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in OpProcesses">\n' +
    '                        <td ng-bind="item.ProcessNote"></td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '        <div class="col-xs-6 ft">\n' +
    '            <div class="col-xs-4 zen-left">\n' +
    '                <p>{{WODetail.QtyOff}} <span>off</span></p>\n' +
    '            </div>\n' +
    '            <div class="col-xs-4 zen-center">\n' +
    '                <p ng-show="WODetail.FastTrack == 1"><span>FT</span></p>\n' +
    '            </div>\n' +
    '            <div class="area-table deliveries-table col-xs-4 no-padding">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_deliveries">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th>Deliveries</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in WODeliveries">\n' +
    '                            <td ng-bind="item.DispatchDate | timeFormat: \'DD/MM/YYYY\'" align="center"></td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row operator no-margin">\n' +
    '        <div class="area-table operator-table col-xs-5 col-xs-offset-1">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_operatornotes">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th>Operator Notes</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in OpOperatorNotes">\n' +
    '                        <td ng-bind="item.Note" align="center"></td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '        <div class="col-xs-6 zen-right">\n' +
    '            <button class="view-orig" type="button" ng-click="ViewOriginTemplate()">View Orig template</button>\n' +
    '            <button class="back" type="button" ng-click="redirectBack()">Back</button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/workorders/workorders.tpl.html',
    '<div class="workorders-page">\n' +
    '    <div class="title-global-page">Work Order</div>\n' +
    '    <div class="page-body">\n' +
    '        <div class="row box first">\n' +
    '            <form id="id-header">\n' +
    '                <div class="titles-orders">\n' +
    '                    <div class="col-xs-11 titles-orders-first">\n' +
    '                        <div class="orders-check-box row">\n' +
    '                            <span>Search:</span>\n' +
    '                            <div class="order-item">\n' +
    '                                <label>Live Order</label>\n' +
    '                                <input type="radio" name="isLiveOrder" value="1" ng-model="isLiveOrder">\n' +
    '                            </div>\n' +
    '                            <div class="order-item">\n' +
    '                                <label>All</label>\n' +
    '                                <input type="radio" name="isLiveOrder" value="2" ng-model="isLiveOrder">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="orders-input row">\n' +
    '                            <div class="col-sm-offset-1 col-sm-2">\n' +
    '                                <label>WO No.</label>\n' +
    '                                <search-input search-input-model="searchInputModel" procedure="ProcWorkOrderSearch" search-key="WOSearchField" display-key="pkWorkOrderID" other-field="{Live: isLiveOrder}" on-select="WorkOrderNoSelected"></search-input>\n' +
    '                            </div>\n' +
    '                            <div class="col-sm-2 ">\n' +
    '                                <label>Part No.</label>\n' +
    '                                <input class="full-width" type="text" name="partno" maxlength="25" class="input-quote" list="PONoList" ng-model="PONo" value="{{PONo}}" ng-keyup="ChoosePONo($event)" ng-model-options="{debounce: 500}" ng-change="changeSelectedPO(PONo)" />\n' +
    '                                <datalist id="PONoList">\n' +
    '                                    <option value="{{item.PartNumber}}" ng-repeat="item in PONoList"></option>\n' +
    '                                </datalist>\n' +
    '                            </div>\n' +
    '                            <div class="col-sm-2 ">\n' +
    '                                <label class="left-none">Matching Work Order:</label>\n' +
    '                                <select ng-model="WorkOrdersForPart" ng-options="option as option.pkWorkOrderID for option in WorkOrdersForParts" ng-change="changeWorkOrdersForPart(WorkOrdersForPart)">\n' +
    '                                </select>\n' +
    '                            </div>\n' +
    '                            <div class="col-sm-5 ">\n' +
    '                                <label>Part of Customer order, Ref: </label>\n' +
    '                                <input type="text" name="customerorder" ng-model="WorkOrderDetail.CustomerOrderRef" class="input-quote" ng-disabled="!WorkOrderDetail" />\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="orders-text row">\n' +
    '                            <div class="col-sm-3">\n' +
    '                                <label>Selected WO:</label>\n' +
    '                                <input type="text" ng-model="WorkOrderID" readonly>\n' +
    '                            </div>\n' +
    '                            <div class="col-sm-2">\n' +
    '                                <label>Due Date:</label>\n' +
    '                                <input class="full-width" name="duedate" ng-model="WorkOrderDetail.DueDate" class="input-quote" ng-disabled="!WorkOrderDetail" />\n' +
    '                            </div>\n' +
    '                            <div class="col-sm-2 quantity">\n' +
    '                                <label>Quantity:</label>\n' +
    '                                <input type="text" name="quantity" ng-model="WorkOrderDetail.Quantity" class="input-quote" ng-disabled="!WorkOrderDetail" />\n' +
    '                            </div>\n' +
    '                            <div class="col-sm-3 description">\n' +
    '                                <label>Description:</label>\n' +
    '                                <input type="text" name="description" ng-model="WorkOrderDetail.Description" class="input-quote" ng-disabled="!WorkOrderDetail" />\n' +
    '                            </div>\n' +
    '                            <div class="col-sm-2">\n' +
    '                                <label>Part No:</label>\n' +
    '                                <a href="javascript:;" ng-bind="WorkOrderDetail.PartNumber" ng-click="redirectPartControl(WorkOrderDetail.pkPartTemplateID)" class="col-xs-8 no-border a-part-no"></a>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="orders-information"></div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-1 titles-orders-after">\n' +
    '                        <button type="submit" class="copy-address float-right" ng-click="redirectBack()">Back</button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '        <div class="row box content">\n' +
    '            <div class="list-tabs">\n' +
    '                <ul class="tabs">\n' +
    '                    <li><a ui-sref-active="active" ui-sref="department.workorders.main({WorkOrderID: WorkOrderID})">Main</a></li>\n' +
    '                    <li><a ui-sref-active="active" ui-sref="department.workorders.cocs({WorkOrderID: WorkOrderID})">CoCs</a></li>\n' +
    '                    <li><a ui-sref-active="active" ui-sref="department.workorders.pos({WorkOrderID: WorkOrderID})">POs</a></li>\n' +
    '                    <li><a ui-sref-active="active" ui-sref="department.workorders.costing({WorkOrderID: WorkOrderID})">Costings</a></li>\n' +
    '                    <li><a ui-sref-active="active" ui-sref="department.workorders.invoice({WorkOrderID: WorkOrderID})">Invoices</a></li>\n' +
    '                    <li><a ui-sref-active="active" ui-sref="department.workorders.route-card({WorkOrderID: WorkOrderID})">Route card</a></li>\n' +
    '                    <li><a ui-sref-active="active" ui-sref="department.workorders.material-issue({WorkOrderID: WorkOrderID})">Material Issue</a></li>\n' +
    '                    <li><a ui-sref-active="active" ui-sref="department.workorders.document({WorkOrderID: WorkOrderID})">Documents</a></li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '            <div class="tab_container">\n' +
    '                <div ui-view></div>\n' +
    '            </div>\n' +
    '            <!--end tab_container-->\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/customer/contact/contact.tpl.html',
    '<div class="contact-page">\n' +
    '    <div class="title-global-page">Customer Contact</div>\n' +
    '    <div class="contact-header">\n' +
    '        <div class="contact-element contact-id">\n' +
    '            <span>Customer Account No.</span> <span ng-bind="customer_id"></span>\n' +
    '        </div>\n' +
    '        <div class="contact-element contact-company col-md-12">\n' +
    '            <div class="company col-md-6">\n' +
    '                <span>Name</span>: <span ng-bind="CustomerInfo.CustomerName"></span>\n' +
    '            </div>\n' +
    '            <div class="phone col-md-6">\n' +
    '                <span class="key">Telephone</span>\n' +
    '                <input type="text" ng-model="CustomerInfo.Phone" maxlength="20">\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="contact-element contact-alias col-md-12">\n' +
    '            <div class="alias col-md-6">\n' +
    '                <span class="key">Alias</span>: <span class="value" ng-bind="CustomerInfo.Alias"></span>\n' +
    '            </div>\n' +
    '            <div class="facsimile col-md-6">\n' +
    '                <span class="key">Facsimile</span>\n' +
    '                <input type="text" ng-model="CustomerInfo.Facsimile" maxlength="20">\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="clearfix"></div>\n' +
    '    <div class="page-body">\n' +
    '        <div class="module module-1 col-md-5">\n' +
    '            <div class="row no-margin">\n' +
    '                <div class="module-hour">\n' +
    '                    <div class="module-title">\n' +
    '                        Opening hours\n' +
    '                    </div>\n' +
    '                    <div class="module-body">\n' +
    '                        <form>\n' +
    '                            <div class="area-table">\n' +
    '                                <table>\n' +
    '                                    <thead>\n' +
    '                                        <tr>\n' +
    '                                            <th>Day</th>\n' +
    '                                            <th>Open</th>\n' +
    '                                            <th>Close</th>\n' +
    '                                        </tr>\n' +
    '                                    </thead>\n' +
    '                                    <tbody>\n' +
    '                                        <tr ng-repeat="day in days">\n' +
    '                                            <td ng-bind="day.name"></td>\n' +
    '                                            <td>\n' +
    '                                                <input type="text" ng-model="CustomerInfo[\'Day\' + day.key + \'Open\']" maxlength="4">\n' +
    '                                            </td>\n' +
    '                                            <td>\n' +
    '                                                <input type="text" ng-model="CustomerInfo[\'Day\' + day.key + \'Close\']" maxlength="4">\n' +
    '                                            </td>\n' +
    '                                        </tr>\n' +
    '                                    </tbody>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                        </form>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row no-margin">\n' +
    '                <div class="module-additional">\n' +
    '                    <div class="row">\n' +
    '                        <div class="module-body col-xs-12">\n' +
    '                            <div class="row">\n' +
    '                                <form action="">\n' +
    '                                    <div class="area-table table-additional col-xs-9">\n' +
    '                                        <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_additional" load-item="CustomerAdditional">\n' +
    '                                            <thead>\n' +
    '                                                <tr>\n' +
    '                                                    <th>Additional</th>\n' +
    '                                                    <th>Start</th>\n' +
    '                                                    <th>End</th>\n' +
    '                                                </tr>\n' +
    '                                            </thead>\n' +
    '                                            <tbody>\n' +
    '                                                <tr ng-repeat="item in CustomerAdditional" ng-click="selectItem(item, \'selected\')">\n' +
    '                                                    <td>\n' +
    '                                                        <span ng-bind="item.Type"></span>\n' +
    '                                                        <span ng-show="item.selected == true" class="pull-right glyphicon glyphicon-ok"></span>\n' +
    '                                                    </td>\n' +
    '                                                    <td ng-bind="item.StartPoint"></td>\n' +
    '                                                    <td ng-bind="item.EndPoint"></td>\n' +
    '                                                </tr>\n' +
    '                                                <tr>\n' +
    '                                                    <td>\n' +
    '                                                        <select ng-model="Additional.TimeType">\n' +
    '                                                            <option value="1">Shutdown</option>\n' +
    '                                                            <option value="2">Collection</option>\n' +
    '                                                            <option value="3">Other</option>\n' +
    '                                                        </select>\n' +
    '                                                    </td>\n' +
    '                                                    <td>\n' +
    '                                                        <input type="text" ng-model="Additional.StartPoint" maxlength="9">\n' +
    '                                                    </td>\n' +
    '                                                    <td>\n' +
    '                                                        <input type="text" ng-model="Additional.EndPoint" maxlength="9">\n' +
    '                                                    </td>\n' +
    '                                                </tr>\n' +
    '                                            </tbody>\n' +
    '                                        </table>\n' +
    '                                    </div>\n' +
    '                                    <div class="area-button col-xs-3 no-padding">\n' +
    '                                        <button type="button" class="remove-additional" ng-click="RmvAdditional(CustomerAdditional)">Rmv</button>\n' +
    '                                    </div>\n' +
    '                                </form>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="module module-2 col-md-7">\n' +
    '            <div class="module-contact">\n' +
    '                <div class="row">\n' +
    '                    <div class="module-title col-xs-2 bold border color-title">Contacts</div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="module-body col-xs-12">\n' +
    '                        <div class="row">\n' +
    '                            <div class="body-title content-right">\n' +
    '                                \'AC\' -Acknowledgement contact\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="area-table col-xs-10 no-padding">\n' +
    '                                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_contact">\n' +
    '                                    <thead>\n' +
    '                                        <tr>\n' +
    '                                            <th>Name</th>\n' +
    '                                            <th>Phone</th>\n' +
    '                                            <th>Ext</th>\n' +
    '                                            <th>Position</th>\n' +
    '                                            <th>Email</th>\n' +
    '                                            <th>Pr</th>\n' +
    '                                            <th>AC</th>\n' +
    '                                        </tr>\n' +
    '                                    </thead>\n' +
    '                                    <tbody>\n' +
    '                                        <tr ng-repeat="item in CustomerContacts" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                            <td ng-click="selectItem(item, \'selected\')">\n' +
    '                                                <input class="pull-left" type="text" ng-model="item.ContactName" maxlength="45">\n' +
    '                                            </td>\n' +
    '                                            <td>\n' +
    '                                                <input type="text" ng-model="item.Phone" maxlength="13">\n' +
    '                                            </td>\n' +
    '                                            <td>\n' +
    '                                                <input type="text" ng-model="item.Extension">\n' +
    '                                            </td>\n' +
    '                                            <td>\n' +
    '                                                <input type="text" ng-model="item.Position" maxlength="25">\n' +
    '                                            </td>\n' +
    '                                            <td>\n' +
    '                                                <input type="text" ng-model="item.Email" maxlength="50">\n' +
    '                                            </td>\n' +
    '                                            <td>\n' +
    '                                                <input type="checkbox" ng-model="item.PrimaryContact" ng-true-value="1" ng-false-value="0">\n' +
    '                                            </td>\n' +
    '                                            <td>\n' +
    '                                                <input type="checkbox" ng-model="item.AcknowContact" ng-true-value="1" ng-false-value="0">\n' +
    '                                            </td>\n' +
    '                                        </tr>\n' +
    '                                        <tr my-enter="AddContact(CustomerContact)">\n' +
    '                                            <td>\n' +
    '                                                <input class="pull-left" type="text" ng-model="CustomerContact.ContactName" name="ContactName" maxlength="45">\n' +
    '                                            </td>\n' +
    '                                            <td>\n' +
    '                                                <input type="text" ng-model="CustomerContact.Phone" name="Phone" maxlength="13">\n' +
    '                                            </td>\n' +
    '                                            <td>\n' +
    '                                                <input type="text" ng-model="CustomerContact.Extension">\n' +
    '                                            </td>\n' +
    '                                            <td>\n' +
    '                                                <input type="text" ng-model="CustomerContact.Position" name="Position" maxlength="25">\n' +
    '                                            </td>\n' +
    '                                            <td>\n' +
    '                                                <input type="text" ng-model="CustomerContact.Email" name="Email" required maxlength="50">\n' +
    '                                            </td>\n' +
    '                                            <td>\n' +
    '                                                <input type="checkbox" ng-model="CustomerContact.PrimaryContact">\n' +
    '                                            </td>\n' +
    '                                            <td>\n' +
    '                                                <input type="checkbox" ng-model="CustomerContact.AcknowContact">\n' +
    '                                            </td>\n' +
    '                                        </tr>\n' +
    '                                    </tbody>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                            <div class="area-button col-xs-2">\n' +
    '                                <button class="update-contact" type="button" ng-click="UpdateContact(CustomerContacts)">Update Contact</button>\n' +
    '                                <button class="remove-contact" type="button" ng-click="RmvContact(CustomerContacts)">Rmv Contact</button>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="clearfix"></div>\n' +
    '            <div class="module-freeform">\n' +
    '                <span>Freeform Notes</span>\n' +
    '                <div class="clearfix"></div>\n' +
    '                <div class="module-body">\n' +
    '                    <div class="area-button">\n' +
    '                        <button type="button" class="remove-freeform" ng-click="RmvNote(CustomerNotes)">Rmv</button>\n' +
    '                    </div>\n' +
    '                    <div class="area-table">\n' +
    '                        <table>\n' +
    '                            <tr ng-repeat="item in CustomerNotes" ng-click="selectItem(item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                <td>\n' +
    '                                    <span ng-bind="item.Note"></span>\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                            <tr my-enter="AddNote(Note)">\n' +
    '                                <td>\n' +
    '                                    <input type="text" ng-model="Note" maxlength="100">\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="page-footer">\n' +
    '        <div class="col-md-12">\n' +
    '            <button ng-click="redirectBack()">Back</button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/customer/delivery-points/delivery-points.tpl.html',
    '<div class="delivery-point-page">\n' +
    '    <div class="page-content">\n' +
    '        <div class="page-title">Delivery points</div>\n' +
    '        <div class="page-body">\n' +
    '            <div class="area-delivery" ng-repeat="item in DeliveryPoints">\n' +
    '                <div class="delivery-stt" ng-click="selectItem(item, \'selected\')">\n' +
    '                    <span ng-bind="$index + 1"></span>\n' +
    '                    <span ng-show="item.selected == true" class="glyphicon glyphicon-ok"></span>\n' +
    '                </div>\n' +
    '                <div class="col-md-6 area area-1">\n' +
    '                    <div class="area-body">\n' +
    '                        <div class="item">\n' +
    '                            <label>Line 1</label>\n' +
    '                            <div>\n' +
    '                                <input type="text" ng-model="item.Address1" maxlength="45">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="item">\n' +
    '                            <label>Line 2</label>\n' +
    '                            <div>\n' +
    '                                <input type="text" ng-model="item.Address2" maxlength="40">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="item">\n' +
    '                            <label>Line 3</label>\n' +
    '                            <div>\n' +
    '                                <input type="text" ng-model="item.Address3" maxlength="40">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="item">\n' +
    '                            <label>Line 4</label>\n' +
    '                            <div>\n' +
    '                                <input type="text" ng-model="item.Address4" maxlength="30">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="item">\n' +
    '                            <label>Pcode</label>\n' +
    '                            <div>\n' +
    '                                <input type="text" ng-model="item.Postcode" maxlength="8">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="item">\n' +
    '                            <div class="button">\n' +
    '                                <button ng-click="UpdateCustDeliveryPoint(item.DPID, item)">Update</button>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-md-5 area area-2">\n' +
    '                    <div class="area-control">\n' +
    '                        <button class="add" ng-click="AddCustomerDeliveryContact(DeliveryPointContact, item.DPID)">Add</button>\n' +
    '                        <button class="remove" ng-click="RmvCustomerDeliveryContact(item.contact)">Rmv</button>\n' +
    '                    </div>\n' +
    '                    <div class="area-body">\n' +
    '                        <div class="area-table">\n' +
    '                            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_contact">\n' +
    '                                <thead>\n' +
    '                                    <tr>\n' +
    '                                        <th class="col-xs-9 no-padding">Contact name</th>\n' +
    '                                        <th class="col-xs-3 no-padding">Phone</th>\n' +
    '                                    </tr>\n' +
    '                                </thead>\n' +
    '                                <tbody>\n' +
    '                                    <tr ng-repeat="contact in item.contact" ng-click="selectItem(contact, \'selected\')" ng-class="{\'tr-active\': contact.selected}">\n' +
    '                                        <td class="col-xs-9 no-padding">\n' +
    '                                            <span ng-bind="contact.ContactName"></span>\n' +
    '                                        </td>\n' +
    '                                        <td class="col-xs-3 no-padding" ng-bind="contact.Phone"></td>\n' +
    '                                    </tr>\n' +
    '                                    <tr>\n' +
    '                                        <td class="col-xs-9 no-padding">\n' +
    '                                            <input type="text" ng-model="DeliveryPointContact.ContactName" maxlength="45">\n' +
    '                                        </td>\n' +
    '                                        <td class="col-xs-3 no-padding">\n' +
    '                                            <input type="text" ng-model="DeliveryPointContact.Phone" maxlength="13">\n' +
    '                                        </td>\n' +
    '                                    </tr>\n' +
    '                                </tbody>\n' +
    '                            </table>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="area-delivery">\n' +
    '                <div class="delivery-stt"></div>\n' +
    '                <div class="col-md-6 area area-1">\n' +
    '                    <div class="area-body">\n' +
    '                        <div class="item">\n' +
    '                            <label>Line 1</label>\n' +
    '                            <div>\n' +
    '                                <input type="text" ng-model="DeliveryPoint.Address1" maxlength="45">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="item">\n' +
    '                            <label>Line 2</label>\n' +
    '                            <div>\n' +
    '                                <input type="text" ng-model="DeliveryPoint.Address2" maxlength="40">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="item">\n' +
    '                            <label>Line 3</label>\n' +
    '                            <div>\n' +
    '                                <input type="text" ng-model="DeliveryPoint.Address3" maxlength="40">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="item">\n' +
    '                            <label>Line 4</label>\n' +
    '                            <div>\n' +
    '                                <input type="text" ng-model="DeliveryPoint.Address4" maxlength="30">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="item">\n' +
    '                            <label>Pcode</label>\n' +
    '                            <div>\n' +
    '                                <input type="text" ng-model="DeliveryPoint.Postcode" maxlength="8">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="page-footer">\n' +
    '            <button class="back" ng-click="redirectBack()">Back</button>\n' +
    '            <button class="add-address" ng-click="AddCustomerDeliveryPoint(DeliveryPoint)">Add address</button>\n' +
    '            <button class="remove-address" ng-click="RemoveCustomerDeliveryPoint(DeliveryPoints)">Rmv address</button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/customer/info/info.tpl.html',
    '<div ng-include="\'/frontend/js/common/partials/customer.tpl.html\'"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/customer/main/main.tpl.html',
    '<div ng-include="\'/frontend/js/common/partials/customer.tpl.html\'"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/customer/order-history/order-history.tpl.html',
    '<div class="order-history-page">\n' +
    '    <div class="page-body">\n' +
    '        <div class="title-global-page">Order History</div>\n' +
    '        <form name="OrderHistoryForm">\n' +
    '            <div class="col-md-5 left no-padding-left">\n' +
    '                <div class="area area-1">\n' +
    '                    <div class="area-title">\n' +
    '                        <label>Select by Part No. (optional)</label>\n' +
    '                        <select ng-model="PartNo" ng-options="option as option.PartNumber for option in PartNos" ng-change="changePartNo(PartNo)"></select>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="clearfix"></div>\n' +
    '                <div class="area area-2">\n' +
    '                    <div class="area-title">Select an order</div>\n' +
    '                    <div class="area-body">\n' +
    '                        <div class="area-table">\n' +
    '                            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_raise" load-item="CustomerOrderHistory">\n' +
    '                                <thead>\n' +
    '                                    <tr>\n' +
    '                                        <th class="col-xs-5 no-padding">Raised</th>\n' +
    '                                        <th class="col-xs-4 no-padding">Completed</th>\n' +
    '                                        <th class="col-xs-3 no-padding">CO No.</th>\n' +
    '                                    </tr>\n' +
    '                                </thead>\n' +
    '                                <tbody>\n' +
    '                                    <tr ng-repeat="item in CustomerOrderHistory | unique:\'CONo\'" ng-click="selectOneItem(CustomerOrderHistory, item, \'selected\', selectOrder(item))" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                        <td class="col-xs-4 no-padding" ng-bind="item.Raised | timeFormat:\'DD-MM-YYYY\'"></td>\n' +
    '                                        <td class="col-xs-4 no-padding" ng-bind="item.Completed | timeFormat:\'DD-MM-YYYY\'"></td>\n' +
    '                                        <td class="col-xs-4 no-padding" ng-bind="item.CONo"></td>\n' +
    '                                    </tr>\n' +
    '                                </tbody>\n' +
    '                            </table>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="col-md-7 right no-padding-right">\n' +
    '                <div class="area area-3">\n' +
    '                    <div class="area-body">\n' +
    '                        <div class="area-table">\n' +
    '                            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_wono" load-item="WorkOrderHistory">\n' +
    '                                <thead>\n' +
    '                                    <tr>\n' +
    '                                        <th class="col-xs-4 no-padding">WO No.</th>\n' +
    '                                        <th class="col-xs-4 no-padding">Part No.</th>\n' +
    '                                        <th class="col-xs-1 no-padding">Qty</th>\n' +
    '                                        <th class="col-xs-3 no-padding">Days</th>\n' +
    '                                    </tr>\n' +
    '                                </thead>\n' +
    '                                <tbody>\n' +
    '                                    <tr ng-repeat="item in WorkOrderHistory" ng-dblclick="gotoWorkOrder(item)" ng-click="selectOneItem(WorkOrderHistory, item, \'selected\'); WONoSelected(item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                        <td class="col-xs-4 no-padding" ng-bind="item.WONo"></td>\n' +
    '                                        <td class="col-xs-4 no-padding" ng-bind="item.PartNo"></td>\n' +
    '                                        <td class="col-xs-1 no-padding" ng-bind="item.Qty"></td>\n' +
    '                                        <td class="col-xs-3 no-padding" ng-bind="item.Days"></td>\n' +
    '                                    </tr>\n' +
    '                                </tbody>\n' +
    '                            </table>\n' +
    '                        </div>\n' +
    '                        <div class="area-desc">double-click to view</div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="area area-4">\n' +
    '                    <div class="area-body">\n' +
    '                        <div class="area-table">\n' +
    '                            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_ordernotes" load-item="OrderNotesHistory">\n' +
    '                                <thead>\n' +
    '                                    <tr>\n' +
    '                                        <th class="col-xs-12 no-padding">Order notes</th>\n' +
    '                                    </tr>\n' +
    '                                </thead>\n' +
    '                                <tbody>\n' +
    '                                    <tr ng-repeat="item in OrderNotesHistory">\n' +
    '                                        <td class="col-xs-12 no-padding" ng-bind="item.OrderNote"></td>\n' +
    '                                    </tr>\n' +
    '                                </tbody>\n' +
    '                            </table>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="area area-5">\n' +
    '                    <div class="area-body">\n' +
    '                        <div class="area-table">\n' +
    '                            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_external" load-item="OrderRejectHistory">\n' +
    '                                <thead>\n' +
    '                                    <tr>\n' +
    '                                        <th class="col-xs-8 no-padding">External Reject Preventive action</th>\n' +
    '                                        <th class="col-xs-4 no-padding">Reject Reason</th>\n' +
    '                                    </tr>\n' +
    '                                </thead>\n' +
    '                                <tbody>\n' +
    '                                    <tr ng-repeat="item in OrderRejectHistory">\n' +
    '                                        <td class="col-xs-8 no-padding" ng-bind="item.RejectAction"></td>\n' +
    '                                        <td class="col-xs-4 no-padding" ng-bind="item.RejectReason"></td>\n' +
    '                                    </tr>\n' +
    '                                </tbody>\n' +
    '                            </table>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="area area-6">\n' +
    '                    <div class="area-body">\n' +
    '                        <div class="col-md-2">\n' +
    '                            <button class="enter-feedback">Enter feedback</button>\n' +
    '                        </div>\n' +
    '                        <div class="repeat-title col-md-2">\n' +
    '                            <label class="text-right">Repeat order Quantity</label>\n' +
    '                            <input type="number" ng-model="RepeatWorkOrder">\n' +
    '                        </div>\n' +
    '                        <div class="col-md-3">\n' +
    '                            <button class="repeate-order" ng-click="RepeatOrder(WorkOrderHistory)">Repeat Order</button>\n' +
    '                        </div>\n' +
    '                        <div class="col-md-2">\n' +
    '                            <button class="cancel">Cancel</button>\n' +
    '                        </div>\n' +
    '                        <div class="col-md-2">\n' +
    '                            <button class="back" ng-click="redirectBack()">Back</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </form>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/engineering-template/engineering/engineering.tpl.html',
    '<div id="engineering" class="client-readmore tab-center">\n' +
    '    <div ng-show="DeptID === \'6\'" class="engineering-base-container">\n' +
    '        <div class="row process-machine-program">\n' +
    '            <div class="col-xs-12 box-table space-top">\n' +
    '                <div class="col-xs-5">\n' +
    '                    <div class="area-table no-padding table-previous col-xs-12 table-operation">\n' +
    '                        <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_processes">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th>Processes for selected Operation</th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr>\n' +
    '                                    <td>\n' +
    '                                        <input type="text" ng-model="ProcessNote" my-enter="AddTemplateProcess(ProcessNote)" maxlength="60">\n' +
    '                                    </td>\n' +
    '                                </tr>\n' +
    '                                <tr ng-repeat="item in TemplateProcesses | orderBy:\'-ProcessID\'">\n' +
    '                                <td>{{item.ProcessNote}} <i ng-click="deleteNoteComment(item, TemplateProcesses)" class="icon-remove glyphicon glyphicon-remove"></i></td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-xs-3 text-center">\n' +
    '                    <label class="title-color-blue">Designated Machine</label>\n' +
    '                    <select ng-model="Engineering_machine" ng-options="option as option.Machine for option in MachineList" ng-change="changeDesignMachine(Engineering_machine)">\n' +
    '                    </select>\n' +
    '                </div>\n' +
    '                <div class="col-xs-2">\n' +
    '                    <div class="area-table table-previous table-program col-xs-12 no-padding">\n' +
    '                        <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_program">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th>Program No\'s</th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="item in MachineOpPrograms">\n' +
    '                                    <td ng-bind="item.ProgramNumber"></td>\n' +
    '                                </tr>\n' +
    '                                <tr>\n' +
    '                                    <td>\n' +
    '                                        <input type="text" ng-model="OpProgramNumber" my-enter="AddMachineOpProgram(OpProgramNumber, Engineering_machine)" maxlength="8">\n' +
    '                                    </td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row type-viewADD">\n' +
    '            <div class="col-xs-12 space-top">\n' +
    '                <div class="col-xs-offset-6 col-xs-4 no-padding">\n' +
    '                    <div class="area-table table-previous table-type-autoprint col-xs-12">\n' +
    '                        <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_addlist">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th class="text-center col-xs-8">Type</th>\n' +
    '                                    <th class="text-center col-xs-4">Auto print</th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="item in ADDList">\n' +
    '                                    <td class="text-center" ng-bind="item.DocType"></td>\n' +
    '                                    <td class="text-center">\n' +
    '                                        <input class="width-initial" type="checkbox" ng-model="item.AutoPrint" ng-checked="item.AutoPrint == 1" ng-click="UpdateADD(item)">\n' +
    '                                    </td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-xs-2">\n' +
    '                    <button type="submit" class="pull-right" ng-click="ViewADD()">View A.D.D</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row internal-risk">\n' +
    '            <div class="col-xs-8 row space-top internal-engineering-notes">\n' +
    '                <div class="area-table table-previous table-engineering col-xs-12">\n' +
    '                    <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_internal">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th class="text-center">Internal Engineering Notes</th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr>\n' +
    '                                <td>\n' +
    '                                    <input type="text" ng-model="EngNote" my-enter="AddOpTmpltIntEngNotes(EngNote)" maxlength="80">\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                            <tr ng-repeat="item in OpTmpltIntEngNotes | orderBy:\'-NoteID\'">\n' +
    '                                <td>{{item.Note}} <i ng-click="deleteNoteComment(item, OpTmpltIntEngNotes)" class="icon-remove glyphicon glyphicon-remove"></i></td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="col-xs-4 row space-top risk-statement">\n' +
    '                <label class="title-color-blue pull-right">Risk statement</label>\n' +
    '                <textarea class="full-width" cols="36" rows="3" ng-bind="TemplateEngineering.RiskStatement"></textarea>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div ng-show="DeptID !== \'6\'" class="engineering-container">\n' +
    '        <div class="col-md-11 box-table space-top">\n' +
    '            <div class="col-sm-7 row">\n' +
    '                <div class="area-table table-previous table-operation col-md-12">\n' +
    '                    <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_template">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th>Processes for selected Operation</th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="item in TemplateProcesses">\n' +
    '                                <td ng-bind="item.ProcessNote"></td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="col-sm-3 row text-center">\n' +
    '                <label class="title-color-blue">Designated Machine</label>\n' +
    '                <select class="input-full" ng-model="Engineering_machine" ng-options="option as option.Machine for option in MachineList" ng-change="changeDesignMachine(Engineering_machine)">\n' +
    '                </select>\n' +
    '            </div>\n' +
    '            <div class="col-sm-3 row">\n' +
    '                <div class="area-table table-previous table-program col-md-12">\n' +
    '                    <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_machine">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th>Program No\'s</th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="item in MachineOpPrograms">\n' +
    '                                <td ng-bind="item.ProgramNumber"></td>\n' +
    '                            </tr>\n' +
    '                            <tr>\n' +
    '                                <td>\n' +
    '                                    <input type="text" ng-model="OpProgramNumber" my-enter="AddMachineOpProgram(OpProgramNumber, Engineering_machine)" maxlength="8">\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-md-12 space-top engineering-ask">\n' +
    '            <button type="button" class="pull-left" ng-click="EngineeringToReview()">Engineering Review</button>\n' +
    '            <div class="col-md-2">\n' +
    '                <p>Ask purchasing Dept to Review this Template</p>\n' +
    '            </div>\n' +
    '            <button type="submit" class="pull-right" ng-click="ViewADD()">View A.D.D</button>\n' +
    '        </div>\n' +
    '        <div class="col-md-offset-8 col-md-4 risk-statement">\n' +
    '            <p class="title-color-blue content-right">Risk statement</p>\n' +
    '            <textarea cols="40" rows="3" ng-bind="TemplateEngineering.RiskStatement"></textarea>\n' +
    '        </div>\n' +
    '        <div class="col-md-9 row space-top">\n' +
    '            <div class="area-table table-previous table-engineering col-md-12">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_internal">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th class="text-center">Internal Engineering Notes</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr>\n' +
    '                            <td>\n' +
    '                                <input type="text" ng-model="EngNote" my-enter="AddOpTmpltIntEngNotes(EngNote)" maxlength="80">\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                        <tr ng-repeat="item in OpTmpltIntEngNotes | orderBy:\'-NoteID\'">\n' +
    '                        <td>{{item.Note}} <i ng-click="deleteNoteComment(item, OpTmpltIntEngNotes)" class="icon-remove glyphicon glyphicon-remove"></i></td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-md-3 row space-top engineering-complete">\n' +
    '            <div class="col-md-6 row">\n' +
    '                <p class="text-right"><strong>Engineering complete</strong></p>\n' +
    '            </div>\n' +
    '            <div class="col-md-6 row no-padding pull-right" ng-show="TemplateEngineering.EngComplete == 1">\n' +
    '                <p class="text-left"> <img src="/assets/images/SmallTick.png" alt="Small Tick"></p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/engineering-template/material-requirement/material-requirement.tpl.html',
    '<div id="material" class="client-readmore tab-center">\n' +
    '    <form name="TemplateMaterialForm" ng-submit="TemplateMaterialForm.$valid && addTemplateMaterial(TemplateMaterialInfo)">\n' +
    '        <div class="box-choose-main row no-margin">\n' +
    '            <div class="box free-issue">\n' +
    '                <p class="text-center">Free Issue</p>\n' +
    '                <p class="text-center">\n' +
    '                    <input type="radio" ng-model="TemplateMaterialInfo.FreeIssue" ng-checked="TemplateMaterialInfo.FreeIssue == 1">\n' +
    '                </p>\n' +
    '            </div>\n' +
    '            <div class="box box-2 description-size">\n' +
    '                <div class="col-md-7 description">\n' +
    '                    <p>Description</p>\n' +
    '                    <p>\n' +
    '                        <search-input search-input-model="TemplateMaterialInfo.Description" procedure="ProcSearchMaterialSpec" search-key="MaterialSpec" display-key="MaterialSpec" on-select="DescriptionSelected"></search-input>\n' +
    '                    </p>\n' +
    '                </div>\n' +
    '                <div class="col-md-5 size">\n' +
    '                    <p>Size</p>\n' +
    '                    <p>\n' +
    '                        <input type="text" ng-model="TemplateMaterialInfo.Size" name="Size" ng-pattern="/^(\\d{1,7})$|^(\\d{1,6}\\.\\d{1})$|^(\\d{1,5}\\.\\d{1,2})$|^(\\d{1,4}\\.\\d{1,3})$/" ng-change="changeTemplateMaterialInfo()"></input>\n' +
    '                    </p>\n' +
    '                </div>\n' +
    '                <div class="col-md-12 spec">\n' +
    '                    <p>\n' +
    '                        <input class="max-width-100" type="text" ng-model="TemplateMaterialInfo.Spec">\n' +
    '                    </p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="box unit">\n' +
    '                <p class="text-left unit-title">Units</p>\n' +
    '                <p class="text-right unit-body">\n' +
    '                    <select ng-model="Unit" ng-options="option as option.name for option in UnitList" name="Unit" required ng-change="TemplateMaterialInfo.setUnitID(Unit.value); changeTemplateMaterialInfo()">\n' +
    '                    </select>\n' +
    '                </p>\n' +
    '            </div>\n' +
    '            <div class="box shape">\n' +
    '                <p class="text-left shape-title">Shape</p>\n' +
    '                <p class="text-right shape-body">\n' +
    '                    <select ng-model="Shape" ng-options="option as option.Description for option in MaterialShapes" name="Shape" required ng-change="TemplateMaterialInfo.setShapeID(Shape.pkShapeID); changeTemplateMaterialInfo()">\n' +
    '                    </select>\n' +
    '                </p>\n' +
    '            </div>\n' +
    '            <div class="box part-yield">\n' +
    '                <p class="part-title">Part length</p>\n' +
    '                <p class="part-body">\n' +
    '                    <input type="text" ng-model="TemplateMaterialInfo.PartLength" ng-pattern="/^(\\d{1,8})$|^(\\d{1,7}\\.\\d{1})$|^(\\d{1,6}\\.\\d{1,2})$/" name="PartLength"><span>mm</span>\n' +
    '                </p>\n' +
    '                <p class="yield-title">Yield\n' +
    '                    <input type="text" ng-model="TemplateMaterialInfo.Yield" name="Yield" maxlength="8" ng-pattern="/^\\d+$/">\n' +
    '                </p>\n' +
    '            </div>\n' +
    '            <div class="box approval">\n' +
    '                <p class="approval-title">Approval</p>\n' +
    '                <p class="approval-body">\n' +
    '                    <select ng-model="Approval" ng-options="option as option.Description for option in MaterialLists" name="Approval" required ng-change="TemplateMaterialInfo.setApprovalID(Approval.ApprovalID); changeTemplateMaterialInfo()">\n' +
    '                    </select>\n' +
    '                </p>\n' +
    '            </div>\n' +
    '            <div class="box spu">\n' +
    '                <p class="spu-title">SPU</p>\n' +
    '                <p class="spu-body">\n' +
    '                    <select ng-model="SPU" ng-options="option as option.Description for option in UOMList" name="SPU" required ng-change="TemplateMaterialInfo.setSPUID(SPU.pkUOMID); changeTemplateMaterialInfo()">\n' +
    '                    </select>\n' +
    '                </p>\n' +
    '            </div>\n' +
    '            <div class="box email-quote">\n' +
    '                <a href="javascript:;" ng-click="mailTo(TemplateMaterialInfo)" class="text-center pull-right mail-to">email quote</a>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="box-chose-alias row no-margin test-pieces-standard-delivery">\n' +
    '            <div class="col-md-4 test-pieces">\n' +
    '                <div class="test-pieces-title col-md-9 no-padding-left">\n' +
    '                    <p>Test pieces required?</p>\n' +
    '                </div>\n' +
    '                <div class="test-pieces-body col-xs-3 no-padding">\n' +
    '                    <input class="input-full" type="text" ng-model="TemplateMaterialInfo.TestPieces" name="TestPieces" maxlength="8" ng-pattern="/^\\d+$/">\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="col-md-4 standard-delivery no-padding-left">\n' +
    '                <div class="col-md-9 no-padding standard-delivery-title">\n' +
    '                    <p class="text-right">Set standard Delivery days:</p>\n' +
    '                </div>\n' +
    '                <div class="col-md-3 no-padding-left standard-delivery-body">\n' +
    '                    <input class="input-full" type="text" ng-model="TemplateMaterialInfo.StdDeliveryDays" name="StdDeliveryDays" maxlength="8" ng-pattern="/^\\d+$/">\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="box-chose-alias row no-margin issue-instruction-standard-supplier">\n' +
    '            <div class="col-md-8 edit-box issue-instruction">\n' +
    '                <div class="col-md-4 issue-instruction-title no-padding">\n' +
    '                    <p class="text-right col-xs-12"><strong>Issue Instruction</strong></p>\n' +
    '                </div>\n' +
    '                <div class="col-md-8 issue-instruction-body">\n' +
    '                    <input type="text" class="input-full" ng-model="TemplateMaterialInfo.IssueInstruction" maxlength="50">\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="col-md-4 no-padding standard-supplier">\n' +
    '                <div class="col-md-6 no-padding standard-supplier-title">\n' +
    '                    <p> Set Standard Supplier:</p>\n' +
    '                </div>\n' +
    '                <div class="col-md-6 standard-supplier-body no-padding">\n' +
    '                    <select ng-model="Supplier" ng-options="option as option.Alias for option in StdSuppliers" name="Supplier" ng-change="TemplateMaterialInfo.setStdSupplierID(Supplier.StdSupplierID)"></select>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="box-content row no-margin comments-notes">\n' +
    '            <div class="col-md-8">\n' +
    '                <div class="table-previous non-overflow col-md-12 approval-comments">\n' +
    '                    <p>Please select standard / approval comments</p>\n' +
    '                    <input class="full-width" type="text" list="SearchApprovalComments" ng-model="ApprovalComment" maxlength="80" my-enter="addApprovalComment(ApprovalComment)">\n' +
    '                    <datalist id="SearchApprovalComments">\n' +
    '                        <option value="{{item.Comments}}" ng-repeat="item in SearchApprovalComments"></option>\n' +
    '                    </datalist>\n' +
    '                    <div class="area-table">\n' +
    '                        <table ng-show="ApprovalComments | hasItem">\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="item in ApprovalComments | orderBy:\'-CommentID\'">\n' +
    '                                    <td>{{item.Comment}} <i ng-click="deleteNoteComment(item, ApprovalComments, \'approvalcomment\')" class="icon-remove glyphicon glyphicon-remove"></i></td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="table-previous col-md-12 po-comments">\n' +
    '                    <p>Please select standard PO comments</p>\n' +
    '                    <input class="full-width" type="text" list="SearchPOComments" ng-model="POComment" maxlength="80" my-enter="addPOComment(POComment)">\n' +
    '                    <datalist id="SearchPOComments">\n' +
    '                        <option value="{{item.Comments}}" ng-repeat="item in SearchPOComments"></option>\n' +
    '                    </datalist>\n' +
    '                    <div class="area-table">\n' +
    '                        <table ng-show="POComments | hasItem">\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="item in POComments | orderBy:\'-CommentID\'">\n' +
    '                                    <td>{{item.Comment}} <i ng-click="deleteNoteComment(item, POComments, \'pocomment\')" class="icon-remove glyphicon glyphicon-remove"></i></td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="area-table table-previous col-md-11 conformity-notes">\n' +
    '                    <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_conformity">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th>Conformity Notes</th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr>\n' +
    '                                <td>\n' +
    '                                    <input type="text" ng-model="ConformityNote" maxlength="80" my-enter="AddConformityNote(ConformityNote)">\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                            <tr ng-repeat="item in ConformityNotes | orderBy:\'-NoteID\'">\n' +
    '                                <td>{{item.Note}} <i ng-click="deleteNoteComment(item, ConformityNotes)" class="icon-remove glyphicon glyphicon-remove"></i></td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '                <div class="area-table table-previous col-md-11 internal-material-notes">\n' +
    '                    <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_material">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th>Internal Material Notes</th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr>\n' +
    '                                <td>\n' +
    '                                    <input type="text" ng-model="MaterialNote" maxlength="80" my-enter="AddMaterialNote(MaterialNote)">\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                            <tr ng-repeat="item in MaterialNotes | orderBy:\'-NoteID\'">\n' +
    '                                <td>{{item.Note}} <i ng-click="deleteNoteComment(item, MaterialNotes)" class="icon-remove glyphicon glyphicon-remove"></i></td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="col-md-4 sidebar-right">\n' +
    '                <div class="area-table table-previous table-type col-md-12 no-padding">\n' +
    '                    <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_addlist">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th>Type</th>\n' +
    '                                <th>Auto Print</th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="item in ADDList" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                <td class="text-center" ng-click="selectOneItem(ADDList, item, \'selected\')" ng-bind="item.DocType"></td>\n' +
    '                                <td class="text-center">\n' +
    '                                    <input class="width-initial" type="checkbox" ng-model="item.AutoPrint" ng-checked="item.AutoPrint == 1" ng-click="UpdateADD(item)">\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '                <button type="button" class="text-center pull-right viewADD" ng-click="ViewADD()">View A.D.D</button>\n' +
    '                <div class="col-md-12 button-last">\n' +
    '                    <div class="row">\n' +
    '                        <p class="col-xs-6 no-padding ask-purchasing">Ask purchasing Dept to Review this Template</p>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="col-md-6 review-btn">\n' +
    '                            <button type="button" class="text-center pull-left" ng-click="PurchaseReview()">Purchasing Review</button>\n' +
    '                        </div>\n' +
    '                        <div class="col-md-6 save-btn no-padding">\n' +
    '                            <button type="submit">Save</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row no-margin">\n' +
    '            <div class="error col-xs-12">\n' +
    '                <p ng-show="TemplateMaterialForm.$submitted && TemplateMaterialForm.Size.$error.pattern">Size must be decimal(7,3)</p>\n' +
    '                <p ng-show="TemplateMaterialForm.$submitted && TemplateMaterialForm.PartLength.$error.pattern">Part length must be decimal (8,2)</p>\n' +
    '                <p ng-show="TemplateMaterialForm.$submitted && TemplateMaterialForm.TestPieces.$error.pattern">Test pieces must be number</p>\n' +
    '                <p ng-show="TemplateMaterialForm.$submitted && TemplateMaterialForm.StdDeliveryDays.$error.pattern">Delivery days must be number</p>\n' +
    '                <p ng-show="TemplateMaterialForm.$submitted && TemplateMaterialForm.Unit.$error.required">Please select Unit</p>\n' +
    '                <p ng-show="TemplateMaterialForm.$submitted && TemplateMaterialForm.Shape.$error.required">Please select Shape</p>\n' +
    '                <p ng-show="TemplateMaterialForm.$submitted && TemplateMaterialForm.Approval.$error.required">Please select Approval</p>\n' +
    '                <p ng-show="TemplateMaterialForm.$submitted && TemplateMaterialForm.SPU.$error.required">Please select SPU</p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </form>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/engineering-template/overview/overview.tpl.html',
    '<div id="overview" class="client-readmore tab-center">\n' +
    '    <div class="full-width" ng-bind-html="birtRouteCard | css"></div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/engineering-template/pering/pering.tpl.html',
    '<div id="pering" class="client-readmore tab-center">\n' +
    '    <div class="col-md-11 box-table space-top">\n' +
    '        <div class="col-sm-7 row">\n' +
    '            <div class="area-table table-previous table-operation col-md-12">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_processes">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th>Processes for selected Operation</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in TemplateProcesses">\n' +
    '                            <td ng-bind="item.ProcessNote"></td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-sm-3 row text-center">\n' +
    '            <label class="title-color-blue">Designated Machine</label>\n' +
    '            <select class="input-full" ng-model="Machine" ng-options="option as option.Machine for option in MachineList" ng-change="changeDesignMachine(Machine)">\n' +
    '            </select>\n' +
    '        </div>\n' +
    '        <div class="col-sm-3 row">\n' +
    '            <div class="area-table table-previous table-program col-md-12">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_program">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th>Program No\'s</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in MachineOpPrograms">\n' +
    '                            <td ng-bind="item.ProgramNumber"></td>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <td>\n' +
    '                                <input type="text" ng-model="ProgramNumber" my-enter="AddMachineOpProgram(ProgramNumber)" maxlength="8">\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col-md-12 row space-top engineering-ask">\n' +
    '        <button type="button" class="pull-left" ng-click="EngineeringToReview()">Engineering Review</button>\n' +
    '        <div class="col-md-2">\n' +
    '            <p>Ask purchasing Dept to Review this Template</p>\n' +
    '        </div>\n' +
    '        <button type="submit" class="pull-right" ng-click="ViewADD()">View A.D.D</button>\n' +
    '    </div>\n' +
    '    <div class="col-md-offset-8 col-md-4 row no-padding risk-statement">\n' +
    '        <p class="title-color-blue content-right">Risk statement</p>\n' +
    '        <textarea cols="40" rows="3" ng-bind="TemplateEngineering.RiskStatement"></textarea>\n' +
    '    </div>\n' +
    '    <div class="col-md-9 row space-top">\n' +
    '        <div class="area-table table-previous table-engineering col-md-12">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_internal">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th class="text-center">Internal Engineering Notes</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in OpTmpltIntEngNotes">\n' +
    '                        <td ng-bind="item.Note"></td>\n' +
    '                    </tr>\n' +
    '                    <tr>\n' +
    '                        <td>\n' +
    '                            <input type="text" ng-model="EngNote" my-enter="AddOpTmpltIntEngNotes(EngNote)" maxlength="80">\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col-md-3 row space-top">\n' +
    '        <div class="col-md-6 row">\n' +
    '            <p class="text-right"><strong>Engineering complete</strong></p>\n' +
    '        </div>\n' +
    '        <div class="col-md-6 row no-padding pull-right" ng-show="TemplateEngineering.EngComplete == 1">\n' +
    '            <p class="text-left"> <img src="/assets/images/SmallTick.png" alt="Small Tick"></p>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/engineering-template/review/review.tpl.html',
    '<div id="review" class="client-readmore tab-center">\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-3 text-center list-button">\n' +
    '            <h2 class="title-color-blue"><strong>Review status</strong></h2>\n' +
    '            <button ng-repeat="item in listReview" type="button" class="{{item.bgclass}} {{item.colorclass}}" ng-click="selectOneItem(listReview, item, \'selected\'); setReview(item.value)" ng-class="{\'col-white\': item.selected}">{{item.name}}</button>\n' +
    '        </div>\n' +
    '        <div class="col-md-9 space-top">\n' +
    '            <div class="row space-top">\n' +
    '                <div class="col-md-8 text-right"><strong></strong></div>\n' +
    '                <div class="col-md-2 text-left"><strong>No</strong></div>\n' +
    '                <div class="col-md-2 text-right"><strong>Yes</strong></div>\n' +
    '            </div>\n' +
    '            <div class="row space-top">\n' +
    '                <div class="col-md-8 text-right"><strong>Do we have the tooling?</strong></div>\n' +
    '                <div class="col-md-2 text-left">\n' +
    '                    <input type="radio" name="Tooling" ng-model="OpTmpltReviewDetail.Tooling" ng-value="0" ng-click="UpdateOpReview(OpTmpltReviewDetail)">\n' +
    '                </div>\n' +
    '                <div class="col-md-2 text-right">\n' +
    '                    <input type="radio" name="Tooling" ng-model="OpTmpltReviewDetail.Tooling" ng-value="1" ng-click="UpdateOpReview(OpTmpltReviewDetail)">\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row space-top">\n' +
    '                <div class="col-md-8 text-right"><strong>Do we have the equipment?</strong></div>\n' +
    '                <div class="col-md-2 text-left">\n' +
    '                    <input type="radio" name="QAEquip" ng-model="OpTmpltReviewDetail.QAEquip" ng-value="0" ng-click="UpdateOpReview(OpTmpltReviewDetail)">\n' +
    '                </div>\n' +
    '                <div class="col-md-2 text-right">\n' +
    '                    <input type="radio" ng-model="OpTmpltReviewDetail.QAEquip" ng-value="1" ng-click="UpdateOpReview(OpTmpltReviewDetail)">\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row space-top">\n' +
    '                <div class="col-md-8 text-right"><strong>Correct approval on all RFQs?</strong></div>\n' +
    '                <div class="col-md-2 text-left">\n' +
    '                    <input type="radio" name="CorrectApproval" ng-model="OpTmpltReviewDetail.CorrectApproval" ng-value="0" ng-click="UpdateOpReview(OpTmpltReviewDetail)">\n' +
    '                </div>\n' +
    '                <div class="col-md-2 text-right">\n' +
    '                    <input type="radio" name="CorrectApproval" ng-model="OpTmpltReviewDetail.CorrectApproval" ng-value="1" ng-click="UpdateOpReview(OpTmpltReviewDetail)">\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-3 text-center list-button">\n' +
    '            <h2 class="title-color-blue"><strong>Risk assessment</strong></h2>\n' +
    '            <button ng-repeat="item in listRisk" type="button" class="{{item.bgclass}} {{item.colorclass}}" ng-click="selectOneItem(listRisk, item, \'selected\'); setRisk(item.value)" ng-class="{\'col-yellow\': item.selected}">{{item.name}}</button>\n' +
    '        </div>\n' +
    '        <div class="col-md-9">\n' +
    '            <div class="col-md-offset-6 col-md-6">\n' +
    '                <button type="button" class="pull-left" ng-click="QualityToReview()">QA Review</button>\n' +
    '                <div class="col-md-7">\n' +
    '                    <p>Ask Quality Dept. to Review this template</p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <p class="text-right title-color-blue" ng-show="OpTmpltReviewDetail.ReviewStatus == 1">Declined quote Generated</p>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row no-margin">\n' +
    '        <div class="area-table table-previous row space-top internal-qa-notes">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_internal">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th class="text-center">Internal QA Notes</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr>\n' +
    '                        <td>\n' +
    '                            <input type="text" ng-model="ReviewNote" my-enter="AddReviewNote(ReviewNote)" maxlength="80">\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                    <tr ng-repeat="item in OpTmpltIntReviewNotes track by $index">\n' +
    '                        <td ng-bind="item.Note"></td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/engineering-template/subcon/subcon.tpl.html',
    '<div id="sub-con" class="client-readmore tab-center">\n' +
    '    <h4 class="color-blue col-xs-12">Heat treatment</h4>\n' +
    '    <div class="col-xs-4">\n' +
    '        <span class="text-center">Set standard Supplier:</span>\n' +
    '        <search-input search-input-model="subconSearchInputModel" procedure="ProcGetSupplierList" search-key="SearchString" display-key="Supplier" on-select="SupplierSelected"></search-input>\n' +
    '    </div>\n' +
    '    <div class="col-xs-4">\n' +
    '        <button type="button" class="pull-left" ng-click="PurchaseReview()">Purchasing Review</button>\n' +
    '        <div class="col-xs-7">\n' +
    '            <p>Ask purchasing Dept to Review this Template</p>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col-xs-4">\n' +
    '        <button type="submit" class="pull-left small" ng-click="ViewADD()">View A.D.D</button>\n' +
    '        <div class="col-xs-8 area-table table-personal no-padding-right">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_addlist">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th class="text-center">Type</th>\n' +
    '                        <th class="text-center">Auto print</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in ADDList" ng-class="{\'tr-active\': item.selected}">\n' +
    '                        <td class="text-center" ng-click="selectOneItem(ADDList, item, \'selected\')" ng-bind="item.DocType"></td>\n' +
    '                        <td class="text-center">\n' +
    '                            <input class="width-initial" type="checkbox" ng-model="item.AutoPrint" ng-checked="item.AutoPrint == 1" ng-click="UpdateADD(item)">\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col-xs-8">\n' +
    '        <div class="area-table table-previous row space-top non-overflow no-margin">\n' +
    '            <p>Please select a standard / approval comment</p>\n' +
    '            <input class="full-width" type="text" list="SCApprovalComments" ng-model="ApprovalComment" maxlength="80" my-enter="addSCApprovalComment(ApprovalComment)">\n' +
    '            <datalist id="SCApprovalComments">\n' +
    '                <option value="{{item.Comments}}" ng-repeat="item in SCApprovalComments"></option>\n' +
    '            </datalist>\n' +
    '            <div class="area-table">\n' +
    '                <table ng-show="SCApprovalCommentItems | hasItem">\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in SCApprovalCommentItems | orderBy:\'-CommentID\'">\n' +
    '                        <td>{{item.Comment}} <i ng-click="deleteNoteComment(item, SCApprovalCommentItems, \'approvalcomment\')" class="icon-remove glyphicon glyphicon-remove"></i></td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="subcon-leadtime col-xs-12 no-padding">\n' +
    '        <div class="col-xs-7 space-top">\n' +
    '            <div class="area-table table-previous table-internal row no-margin">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_internal">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th class="text-center">Internal Sub-con Notes</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr>\n' +
    '                            <td>\n' +
    '                                <input type="text" ng-model="TreatNote" my-enter="AddTreatNote(TreatNote)" maxlength="80">\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                        <tr ng-repeat="item in OpTmpltIntTreatNotes | orderBy:\'-NoteID\'">\n' +
    '                            <td>{{item.Note}} <i ng-click="deleteNoteComment(item, OpTmpltIntTreatNotes, \'pocomment\')" class="icon-remove glyphicon glyphicon-remove"></i></td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-xs-5 no-padding-right">\n' +
    '            <p class="text-right">Std. lead time</p>\n' +
    '            <p class="text-right">\n' +
    '                <input class="width-input-number" type="text" ng-model="OpTmpltSubconDetail.LeadTime" maxlength="8"><span>day</span></p>\n' +
    '            <a href="javascript:;" ng-click="mailTo()" class="text-center pull-right mail-to">email quote</a>\n' +
    '            <button class="update-btn" type="button" ng-click="UpdateSubconDetail(OpTmpltSubconDetail)">Update</button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/inspection-schedule/inspection/inspection.tpl.html',
    '<div class="inspection-page">\n' +
    '    <div class="title-global-page">Inspection</div>\n' +
    '    <div class="inspection-action col-md-5">\n' +
    '        <div class="area-table col-md-12">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" float-thead-enabled="testFloatThead" ng-model="thead_operations" andy-float-thead>\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th>Operations list</th>\n' +
    '                        <th>M</th>\n' +
    '                        <th>I</th>\n' +
    '                        <th>S</th>\n' +
    '                        <th>F</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in InspectOpList" ng-click="selectOneItem(InspectOpList, item, \'selected\'); SelectedOperation(item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                        <td ng-bind="item.Operation"></td>\n' +
    '                        <td ng-bind="item.Made"></td>\n' +
    '                        <td ng-bind="item.Inspected"></td>\n' +
    '                        <td ng-bind="item.Scrapped"></td>\n' +
    '                        <td ng-bind="item.Forwarded"></td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '        <div class="inspection-control col-md-12">\n' +
    '            <div class="department" ng-show="OperationID">\n' +
    '                <select ng-model="Staff" ng-options="option as option.name for option in Staffs">\n' +
    '                </select>\n' +
    '            </div>\n' +
    '            <div class="control">\n' +
    '                <div class="begin control-element col-md-4">\n' +
    '                    <div class="icon" ng-click="BeginInspection()"></div>\n' +
    '                    <div class="title">Begin</div>\n' +
    '                </div>\n' +
    '                <div class="pause control-element col-md-4">\n' +
    '                    <select ng-model="PauseReason" ng-options="option as option.name for option in Reasons"></select>\n' +
    '                    <div class="icon" ng-click="PauseInspection()"></div>\n' +
    '                    <div class="title">Pause</div>\n' +
    '                </div>\n' +
    '                <div class="complete control-element col-md-4">\n' +
    '                    <div class="icon" ng-click="CompleteInspection()"></div>\n' +
    '                    <div class="title">Complete</div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="material-stock col-md-12">\n' +
    '            <div class="content">\n' +
    '                <div class="title">\n' +
    '                    <span>Material / stock inspection</span>\n' +
    '                </div>\n' +
    '                <div class="body">\n' +
    '                    <div class="row">\n' +
    '                        <div class="approval-type col-md-4">\n' +
    '                            <div class="module-title">Approval type</div>\n' +
    '                            <select ng-model="InspectionDetail.ApprovalTypeID" ng-options="option as option.Description for option in ApprovalList">\n' +
    '                            </select>\n' +
    '                        </div>\n' +
    '                        <div class="spec col-md-4">\n' +
    '                            <div class="module-title">Spec.</div>\n' +
    '                            <input type="text" ng-model="InspectionDetail.Spec">\n' +
    '                        </div>\n' +
    '                        <div class="release-note col-md-4">\n' +
    '                            <div class="module-title">Release Note No.</div>\n' +
    '                            <input type="text" ng-model="InspectionDetail.ReleaseNoteNo">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="all-ok col-md-3">\n' +
    '                            <div class="module-title">All OK</div>\n' +
    '                            <input type="radio" ng-model="InspectionDetail.AllOK" ng-value="1" ng-change="UpdateInspectionDetail(InspectionDetail, \'allok\')">\n' +
    '                        </div>\n' +
    '                        <div class="rejected col-md-3">\n' +
    '                            <div class="module-title">Rejected</div>\n' +
    '                            <input type="text" ng-model="InspectionDetail.QtyRejected" my-enter="UpdateInspectionDetail(InspectionDetail, \'rejected\')">\n' +
    '                        </div>\n' +
    '                        <div class="all-rejected col-md-3">\n' +
    '                            <div class="module-title">All Rejected</div>\n' +
    '                            <input type="radio" ng-model="InspectionDetail.AllOK" ng-value="2" ng-change="UpdateInspectionDetail(InspectionDetail, \'allrejected\')">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="batch-no col-md-5">\n' +
    '                            <div class="module-title">\n' +
    '                                <strong>Batch No.</strong>\n' +
    '                            </div>\n' +
    '                            <div class="value">\n' +
    '                                <strong ng-bind="GRBID"></strong>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="reject-reason col-md-4">\n' +
    '                            <div class="module-title">Reject reason</div>\n' +
    '                            <select ng-model="InspectionDetail.RejectReasonID" ng-options="option as option.name for option in RejectReasons" ng-change="UpdateInspectionDetail(InspectionDetail, \'rejectReason\')">\n' +
    '                            </select>\n' +
    '                        </div>\n' +
    '                        <div class="create-reject col-md-3">\n' +
    '                            <button type="button" ng-click="openBIRT()">Create Reject Note</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="inspection-information col-md-7">\n' +
    '        <div class="row">\n' +
    '            <div class="area-table table-previous col-md-12">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_previous">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th>Previous Internal Scrap information</th>\n' +
    '                            <th>Scrap Reason</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in OpInspectionPISI">\n' +
    '                            <td ng-bind="item.PreviousScrapInfo"></td>\n' +
    '                            <td ng-bind="item.PreviousScrapReason"></td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row">\n' +
    '            <div class="area-table table-external col-md-12">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_external">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th>External Reject Preventive action</th>\n' +
    '                            <th>Reject Reason</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in OpInspectionERPA">\n' +
    '                            <td ng-bind="item.ExtRejectAction"></td>\n' +
    '                            <td ng-bind="item.RejectReason"></td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row">\n' +
    '            <div class="area-table table-equipment col-md-5">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_checking">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th>Checking equipment</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in OpInspectionCE">\n' +
    '                            <td ng-bind="item.CheckEquip"></td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '            <div class="area-table table-operator col-md-5">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_operatornotes">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th>Operator Notes</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in OpInspectionON">\n' +
    '                            <td ng-bind="item.OperatorNote"></td>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <td>\n' +
    '                                <input class="col-xs-12 no-padding" type="text" ng-model="OperatorNote" maxlength="50"></input>\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '            <div class="table-control col-md-2">\n' +
    '                <button type="button" ng-click="addOperatorNote(OperatorNote)">Add note</button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row">\n' +
    '            <div class="area-table table-process col-md-8">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_processes">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th>Processes</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in OpInspectionProcesses">\n' +
    '                            <td ng-bind="item.ProcessNote"></td>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <td>\n' +
    '                                <input class="col-xs-12 no-padding" type="text" ng-model="ProcessNote" maxlength="60"></input>\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '            <div class="table-control col-md-2">\n' +
    '                <button type="button" ng-click="addProcessNote(ProcessNote)">Add Process</button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row inspection-info">\n' +
    '            <div class="col-md-12">\n' +
    '                <span>Inspection info</span>\n' +
    '                <input type="text" maxlength="50" ng-model="OpInspectionDetail.InspectInfo" ng-model-options="{debounce: 500}" ng-change="UpdateOperationDetail(OpInspectionDetail)">\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row work-order">\n' +
    '            <div class="col-md-12">\n' +
    '                <div class="qty-inspected">\n' +
    '                    <div class="title">Qty inspected</div>\n' +
    '                    <input type="text" ng-model="OpInspectionDetail.QtyInspected" ng-model-options="{debounce: 500}" ng-change="UpdateOperationDetail(OpInspectionDetail)">\n' +
    '                </div>\n' +
    '                <div class="qty-scrapped">\n' +
    '                    <div class="title">Qty scrapped</div>\n' +
    '                    <input type="text" ng-model="OpInspectionDetail.QtyScrapped" ng-model-options="{debounce: 500}" ng-change="UpdateOperationDetail(OpInspectionDetail)">\n' +
    '                </div>\n' +
    '                <div class="reason">\n' +
    '                    <div class="title">Reason</div>\n' +
    '                    <select ng-model="OpInspectionDetail.ReasonID" ng-options="option as option.name for option in ScrappedReasons" ng-change="UpdateOperationDetail(OpInspectionDetail)">\n' +
    '                    </select>\n' +
    '                </div>\n' +
    '                <div class="work-order-part">\n' +
    '                    <div class="work-order-no">\n' +
    '                        <div class="title">Work Order No.</div>\n' +
    '                        <div class="value" ng-bind="WorkOrderID"></div>\n' +
    '                    </div>\n' +
    '                    <div class="part-no">\n' +
    '                        <div class="title">Part No.</div>\n' +
    '                        <div class="value" ng-bind="PartNumber"></div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row inspection-button">\n' +
    '            <div class="col-md-12">\n' +
    '                <strong class="po-qty">Sub-con PO Qty</strong>\n' +
    '                <input type="text" maxlength="8" ng-model="OpInspectionDetail.SubConQty" class="input-po-qty">\n' +
    '                <button class="raise-po" type="button" ng-click="RaisePO(OpInspectionDetail.SubConQty)">Raise PO</button>\n' +
    '                <button class="view-add" type="button" ng-click="ViewADD()">View ADD</button>\n' +
    '                <button class="print-coc" type="submit">Print COCs</button>\n' +
    '                <button class="back" type="button" ng-click="redirectBack()">Back</button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <embed class="max-width-100percent" ng-src="{{showImage}}">\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/inspection-schedule/main/main.tpl.html',
    '<div class="inspection-schedule-page">\n' +
    '    <div class="page-body">\n' +
    '        <div class="title-global-page">Inspection Schedule</div>\n' +
    '        <div class="row">\n' +
    '            <div class="area-table table-inspection col-xs-11">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="float_inspection" load-item="InspectionSchedule">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[0], \'selected\'); changeSortBy(sortTable[0])" ng-class="{\'th-active\': sortTable[0].selected}">WO No.</th>\n' +
    '                            <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[1], \'selected\'); changeSortBy(sortTable[1])" ng-class="{\'th-active\': sortTable[1].selected}">CO No.</th>\n' +
    '                            <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[2], \'selected\'); changeSortBy(sortTable[2])" ng-class="{\'th-active\': sortTable[2].selected}">Part Number</th>\n' +
    '                            <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[3], \'selected\'); changeSortBy(sortTable[3])" ng-class="{\'th-active\': sortTable[3].selected}">Qty</th>\n' +
    '                            <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[4], \'selected\'); changeSortBy(sortTable[4])" ng-class="{\'th-active\': sortTable[4].selected}">Operation</th>\n' +
    '                            <th colspan="4">Dates</th>\n' +
    '                            <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[9], \'selected\'); changeSortBy(sortTable[9])" ng-class="{\'th-active\': sortTable[9].selected}">2nd Resource</th>\n' +
    '                            <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[10], \'selected\'); changeSortBy(sortTable[10])" ng-class="{\'th-active\': sortTable[10].selected}">Insp. Type</th>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <th ng-click="selectOneItem(sortTable, sortTable[5], \'selected\'); changeSortBy(sortTable[5])" ng-class="{\'th-active\': sortTable[5].selected}">DD Start</th>\n' +
    '                            <th ng-click="selectOneItem(sortTable, sortTable[6], \'selected\'); changeSortBy(sortTable[6])" ng-class="{\'th-active\': sortTable[6].selected}">&nbsp;&nbsp;&nbsp;Start</th>\n' +
    '                            <th ng-click="selectOneItem(sortTable, sortTable[7], \'selected\'); changeSortBy(sortTable[7])" ng-class="{\'th-active\': sortTable[7].selected}">&nbsp;&nbsp;&nbsp;Finish</th>\n' +
    '                            <th ng-click="selectOneItem(sortTable, sortTable[8], \'selected\'); changeSortBy(sortTable[8])" ng-class="{\'th-active\': sortTable[8].selected}">DD Finish</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in InspectionSchedule | orderBy : sortBy : reverse" ng-click="selectOneItem(InspectionSchedule, item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                            <td>\n' +
    '                                <a href="javascript:;" ng-click="redirectWorkOrder(item.WorkOrderID)" ng-bind="item.WorkOrderID"></a>\n' +
    '                            </td>\n' +
    '                            <td>\n' +
    '                                <a href="javascript:;" ng-click="redirectCustomerOrder(item.OrderID)" ng-bind="item.OrderID"></a>\n' +
    '                            </td>\n' +
    '                            <td>\n' +
    '                                <a href="javascript:;" ng-click="redirectPartTemplate(item.PartTemplateID)" ng-bind="item.PartNumber"></a>\n' +
    '                            </td>\n' +
    '                            <td ng-bind="item.Qty"></td>\n' +
    '                            <td ng-bind="item.Operation"></td>\n' +
    '                            <td ng-bind="item.DDStartDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                            <td ng-bind="item.StartDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                            <td ng-bind="item.FinishDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                            <td ng-bind="item.DDFinishDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                            <td ng-bind="item.Resource"></td>\n' +
    '                            <td ng-bind="item.InspectionType"></td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '            <div class="table-control col-xs-1">\n' +
    '                <span class="up-control" ng-click="reverse = false"></span>\n' +
    '                <span>sort</span>\n' +
    '                <span class="down-control" ng-click="reverse = true"></span>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row">\n' +
    '            <div class="area-table table-goods-in col-xs-2">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_grb">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th>GRB No.</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in GoodsInList" ng-click="selectOneItem(GoodsInList, item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                            <td ng-bind="item.GRBID"></td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="page-footer">\n' +
    '        <button class="back" type="button" ng-click="redirectBack()">Back</button>\n' +
    '        <button class="inspect" ng-click="redirectInspect(InspectionSchedule, GoodsInList)">Inspect</button>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/part-control/docs/docs.tpl.html',
    '<div class="col-xs-12 docs-tab">\n' +
    '    <div class="row">\n' +
    '        <div class="col-xs-5 col-xs-offset-1 user-type-table">\n' +
    '            <div class="area-table">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_partdocs">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th class="font-weight color-blue">User</th>\n' +
    '                            <th class="font-weight color-blue">Type</th>\n' +
    '                            <th class="font-weight color-blue">Auto print</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in PartDocs" ng-click="selectOneItem(PartDocs, item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                            <td ng-bind="item.Uname"></td>\n' +
    '                            <td ng-bind="item.DropType"></td>\n' +
    '                            <td>\n' +
    '                                <input type="checkbox">\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="view col-xs-1 no-padding">\n' +
    '            <button type="button" ng-click="ViewFileName()">View</button>\n' +
    '            <img class="img-responsive" ng-src="{{showImage}}">\n' +
    '        </div>\n' +
    '        <div class="approval-list col-xs-2 col-xs-offset-1">\n' +
    '            <div class="document-drop-type col-xs-12 no-padding">\n' +
    '                <select ng-model="AddPartDoc.DocumentDropType" ng-options="option as option.DropType for option in DocumentDropTypes" ng-change="changeDROP(AddPartDoc.DocumentDropType)"></select>\n' +
    '            </div>\n' +
    '            <div class="reference col-xs-12 no-padding" ng-show="AddPartDoc.DocumentDropType.pkDocumentDropTypeID == 4">\n' +
    '                <div class="reference-title col-xs-12 no-padding">\n' +
    '                    <p>Reference</p>\n' +
    '                </div>\n' +
    '                <div class="reference-content col-xs-12 no-padding">\n' +
    '                    <input type="text" maxlength="25" ng-model="AddPartDoc.Reference">\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="issue col-xs-12 no-padding" ng-show="AddPartDoc.DocumentDropType.pkDocumentDropTypeID == 4">\n' +
    '                <div class="issue-title col-xs-6 no-padding-left">\n' +
    '                    <p class="content-right">Issue</p>\n' +
    '                </div>\n' +
    '                <div class="issue-content col-xs-6 no-padding">\n' +
    '                    <input type="text" maxlength="5" ng-model="AddPartDoc.Issue">\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-xs-2 drop-button">\n' +
    '            <button dropzone max-file-name="30" on-success="uploadSuccess" before-upload="beforeUpload">Drop associated Part Docs here</button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <form name="TemplateForm" ng-submit="TemplateForm.$valid && AddTemplate(TemplateInfo)" novalidate>\n' +
    '        <div class="row">\n' +
    '            <div class="part-template col-xs-3">\n' +
    '                <div class="part-template-title border-bottom color-blue large-font font-weight">Add a New Part Template</div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row">\n' +
    '            <div class="part-issue col-xs-12">\n' +
    '                <div class="part-no col-xs-2">\n' +
    '                    <p class="part-no-title">Part No.</p>\n' +
    '                    <input type="text" maxlength="25" ng-model="TemplateInfo.PartNumber" ng-change="changePartNo(TemplateInfo.PartNumber)" name="PartNumber" required />\n' +
    '                </div>\n' +
    '                <div class="issue col-xs-1">\n' +
    '                    <p class="issue-title">Issue</p>\n' +
    '                    <input type="text" class="border content-center font-weight" maxlength="5" ng-model="TemplateInfo.PartIssue" ng-change="changePartIssue(TemplateInfo.PartIssue)" name="PartIssue" required />\n' +
    '                </div>\n' +
    '                <div class="desc col-xs-2">\n' +
    '                    <p class="desc-title">Description</p>\n' +
    '                    <input type="text" maxlength="40" ng-model="TemplateInfo.Description">\n' +
    '                </div>\n' +
    '                <div class="std-lead-days col-xs-2">\n' +
    '                    <span class="std-lead-days-title">Std Lead days</span>\n' +
    '                    <input type="text" ng-model="TemplateInfo.LeadDays" name="LeadDays" ng-pattern="/^\\d+$/" />\n' +
    '                </div>\n' +
    '                <div class="std-price col-xs-2">\n' +
    '                    <span class="std-price-title">Std Price</span>\n' +
    '                    <span class="currency-symbol currency-symbol-irish left">\n' +
    '                    <input class="input-credit" type="text" ng-model="TemplateInfo.Price" name="Price" ng-pattern="/^\\d{0,5}(\\.\\d{1,2})?$/" />\n' +
    '                </span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row">\n' +
    '            <div class="drawing-assemblies col-xs-12">\n' +
    '                <div class="drawing-issue col-xs-3">\n' +
    '                    <div class="row">\n' +
    '                        <div class="drawing col-xs-8">\n' +
    '                            <p class="drawing-title">Drawing No.</p>\n' +
    '                            <input type="text" maxlength="25" ng-model="TemplateInfo.DrawingNumber" name="DrawingNumber" required />\n' +
    '                        </div>\n' +
    '                        <div class="issue col-xs-4">\n' +
    '                            <p class="issue-title content-right">Issue</p>\n' +
    '                            <input class="content-center" type="text" maxlength="5" ng-model="TemplateInfo.Issue" name="Issue" required />\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="approval col-xs-12">\n' +
    '                            <div class="approval-title col-xs-6 no-padding content-right">Approval</div>\n' +
    '                            <div class="approval-content col-xs-6 no-padding">\n' +
    '                                <select ng-model="Approval" ng-options="option as option.Description for option in ApprovalList" name="ApprovalID" required ng-change="TemplateInfo.setApprovalID(Approval.pkApprovalID)"></select>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-xs-3 col-xs-offset-1 assemblies-qty">\n' +
    '                    <div class="row">\n' +
    '                        <div class="assemblies-title content-center border-bottom color-blue font-weight">Assemblies</div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="qty-per col-xs-12">\n' +
    '                            <div class="qty col-xs-2 no-padding">\n' +
    '                                <p class="qty-title content-right">Qty</p>\n' +
    '                                <input type="text" ng-model="TemplateInfo.AssemblyQty" ng-pattern="/^\\d+$/" name="AssemblyQty" />\n' +
    '                            </div>\n' +
    '                            <div class="per col-xs-2">\n' +
    '                                <p class="per-title">&nbsp;</p>\n' +
    '                                <p class="per-content font-weight">per</p>\n' +
    '                            </div>\n' +
    '                            <div class="master-part col-xs-8 no-padding">\n' +
    '                                <p class="master-part-title">Master Part No.</p>\n' +
    '                                <select ng-model="MasterPartNo" ng-options="option as option.PartNumber for option in NumberList" name="PartTemplateID" ng-change="TemplateInfo.setPartTemplateID(MasterPartNo.PartTemplateID)"></select>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="create-new col-xs-1 col-xs-offset-2 no-padding">\n' +
    '                    <button type="submit">Create New</button>\n' +
    '                </div>\n' +
    '                <div class="back col-xs-2">\n' +
    '                    <button class="pull-right" ng-click="redirectBack()">Back</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row">\n' +
    '            <div class="error col-xs-12">\n' +
    '                <p class="col-xs-12" ng-show="TemplateForm.$submitted && TemplateForm.PartNumber.$error.required">Please enter Part No.</p>\n' +
    '                <p class="col-xs-12" ng-show="TemplateForm.$submitted && TemplateForm.PartIssue.$error.required">Please enter Part Issue</p>\n' +
    '                <p class="col-xs-12" ng-show="TemplateForm.$submitted && TemplateForm.DrawingNumber.$error.required">Please enter Drawing No.</p>\n' +
    '                <p class="col-xs-12" ng-show="TemplateForm.$submitted && TemplateForm.Issue.$error.required">Please enter Issue</p>\n' +
    '                <p class="col-xs-12" ng-show="TemplateForm.$submitted && TemplateForm.LeadDays.$error.pattern">Std Lead days must be number</p>\n' +
    '                <p class="col-xs-12" ng-show="TemplateForm.$submitted && TemplateForm.LeadDays.$error.pattern">Std Lead days must be number</p>\n' +
    '                <p class="col-xs-12" ng-show="TemplateForm.$submitted && TemplateForm.Price.$error.pattern">Std Price must be decimal(7,2)</p>\n' +
    '                <p class="col-xs-12" ng-show="TemplateForm.$submitted && TemplateForm.ApprovalID.$error.required">Please select Approval</p>\n' +
    '                <p class="col-xs-12" ng-show="TemplateForm.$submitted && TemplateForm.AssemblyQty.$error.pattern">Qty must be number</p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </form>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/part-control/assemblies/assemblies.tpl.html',
    '<div class="assemblies-tab col-xs-12">\n' +
    '    <div class="row">\n' +
    '        <div class="work-no col-xs-11 col-xs-offset-1 font-weight font-large" ng-bind="PartItem.PartNumber"></div>\n' +
    '    </div>\n' +
    '    <div class="row">\n' +
    '        <div class="assemblies col-xs-offset-1 col-xs-4">\n' +
    '            <div class="row">\n' +
    '                <div class="assemblies-title content-center color-blue border-bottom col-xs-8">Assemblies</div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="assemblies-content col-xs-12 no-padding">\n' +
    '                    <div class="qty-master col-xs-8 no-padding">\n' +
    '                        <div class="qty col-xs-3">\n' +
    '                            <div class="col-xs-12 qty-title">Qty</div>\n' +
    '                            <input type="text" ng-model="Qty">\n' +
    '                        </div>\n' +
    '                        <div class="per col-xs-1 no-padding">\n' +
    '                            <div class="per-title">&nbsp;</div>\n' +
    '                            <span class="font-weight">per</span>\n' +
    '                        </div>\n' +
    '                        <div class="master-partno col-xs-8">\n' +
    '                            <div class="master-partno-title">Master Part No.</div>\n' +
    '                            <select ng-model="MasterPartNo" ng-options="option as option.PartNumber for option in PartNumberList"></select>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="update col-xs-4">\n' +
    '                        <button class="pull-right" ng-click="PartLinkToMaster(Qty, MasterPartNo)">Update</button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="child-partno col-xs-2 col-xs-offset-5">\n' +
    '            <div class="child-partno-title color-blue">Child Part No\'s.</div>\n' +
    '            <div class="area-table">\n' +
    '                <table ng-show="PartAssemblies | hasItem">\n' +
    '                    <tr ng-repeat="item in PartAssemblies">\n' +
    '                        <td ng-bind="item.PartNumber"></td>\n' +
    '                    </tr>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/part-control/notes/notes.tpl.html',
    '<div class="col-xs-12 notes-tab">\n' +
    '    <div class="row">\n' +
    '        <div class="esl-notes col-xs-12">\n' +
    '            <div class="row">\n' +
    '                <div class="notes-table col-xs-11">\n' +
    '                    <div class="area-table">\n' +
    '                        <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_template">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th>\n' +
    '                                        ESL internal Notes\n' +
    '                                    </th>\n' +
    '                                    <th>Date</th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="item in PartTemplateNotes">\n' +
    '                                    <td ng-bind="item.PartNote"></td>\n' +
    '                                    <td ng-bind="item.NoteDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                                </tr>\n' +
    '                                <tr>\n' +
    '                                    <td>\n' +
    '                                        <input type="text" maxlength="80" ng-model="Note">\n' +
    '                                    </td>\n' +
    '                                    <td></td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="notes-add col-xs-1 no-padding">\n' +
    '                    <button ng-click="AddNote(Note)">Add</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row">\n' +
    '        <div class="part-remarks col-xs-12">\n' +
    '            <div class="row">\n' +
    '                <div class="remarks-table col-xs-11">\n' +
    '                    <div class="area-table">\n' +
    '                        <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_remarks">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th>\n' +
    '                                        Part Remarks\n' +
    '                                    </th>\n' +
    '                                    <th>Date</th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="item in PartTemplateRemarks">\n' +
    '                                    <td ng-bind="item.PartRemark"></td>\n' +
    '                                    <td ng-bind="item.RemarkDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                                </tr>\n' +
    '                                <tr>\n' +
    '                                    <td>\n' +
    '                                        <input type="text" maxlength="45" ng-model="Remark">\n' +
    '                                    </td>\n' +
    '                                    <td></td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="remarks-add col-xs-1 no-padding">\n' +
    '                    <button ng-click="AddRemark(Remark)">Add</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/part-control/part-list/part-list.tpl.html',
    '<div class="part-list-tab col-xs-12">\n' +
    '    <div class="row">\n' +
    '        <div class="part-filter col-xs-12">\n' +
    '            <div class="row">\n' +
    '                <div class="filter col-xs-2 col-xs-offset-1">\n' +
    '                    <div class="title">&nbsp;</div>\n' +
    '                    <div class="content">\n' +
    '                        Part Filter:\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="all-issue col-xs-1">\n' +
    '                    <div class="title">All</div>\n' +
    '                    <div class="content">\n' +
    '                        <input type="radio" ng-model="PartFilter" name="PartFilter" value="2" ng-selected="PartFilter == 2">\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="active-issue col-xs-1 no-padding">\n' +
    '                    <div class="title">Active Issue</div>\n' +
    '                    <div class="content">\n' +
    '                        <input type="radio" ng-model="PartFilter" name="PartFilter" value="1">\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row">\n' +
    '        <div class="col-xs-8 search-button">\n' +
    '            <div class="row">\n' +
    '                <div class="col-xs-12 search-template">\n' +
    '                    <div class="row">\n' +
    '                        <div class="search col-xs-5 col-xs-offset-1">\n' +
    '                            <search-input search-input-model="searchInputModel" procedure="ProcPartTemplateSearch" search-key="PartNumberSearchString" display-key="PartNumber // Issue // COS" other-field="{Filter: PartFilter}" on-select="PartNoSelected"></search-input>\n' +
    '                        </div>\n' +
    '                        <div class="template-release col-xs-6">\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-8 view-template">\n' +
    '                                    <button type="button" ng-click="ViewTemplate()">View Teamplate</button>\n' +
    '                                </div>\n' +
    '                                <div class="col-xs-4 release-type">\n' +
    '                                    <p>Release type</p>\n' +
    '                                    <select ng-model="PartDetail[0].ReleaseTypeID" ng-options="option as option.Description for option in AllApprovalList">\n' +
    '                                    </select>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="flugel-up-issue col-xs-12">\n' +
    '                    <div class="row">\n' +
    '                        <div class="flugel col-xs-6">\n' +
    '                            <div class="title pull-right color-blue large-font" ng-bind="PartDetail[0].PartDescription"></div>\n' +
    '                            <div class="content col-xs-3 col-xs-offset-9 pull-right">\n' +
    '                                <p class="content-title"><small>New issue</small></p>\n' +
    '                                <input type="text" maxlength="5" ng-model="NewIssue">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="col-xs-6 up-issue">\n' +
    '                            <div class="title large-font">&nbsp;</div>\n' +
    '                            <div class="content">\n' +
    '                                <button type="button" ng-click="UpIssue(NewIssue)">Up Issue</button>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <!-- Modal -->\n' +
    '                        <div modal-show modal-visible="showDialog" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">\n' +
    '                            <div class="modal-dialog modal-lg">\n' +
    '                                <div class="modal-content">\n' +
    '                                    <div class="modal-body">\n' +
    '                                        <div dropzone on-success="uploadSuccess" on-error="errorUpload" before-upload="beforeUpload" class="drop-zone" max-file-name="45">\n' +
    '                                            <span>Drop file here</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="modal-footer">\n' +
    '                                        <button data-dismiss="modal">Close</button>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="cos-create-template col-xs-12">\n' +
    '                    <div class="row">\n' +
    '                        <div class="cos col-xs-6">\n' +
    '                            <p class="content-right">COS</p>\n' +
    '                            <div class="content">\n' +
    '                                <select class="pull-right" ng-model="PartDetail[0].COSID" ng-options="option as option.Description for option in SupplyConditionList">\n' +
    '                                </select>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="create-templete col-xs-6">\n' +
    '                            <button ng-click="CreateTemplate(PartDetail[0].COSID)">Create Teamplate</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="originating-photocopy col-xs-12">\n' +
    '                    <div class="row">\n' +
    '                        <div class="originating col-xs-6">\n' +
    '                            <p class="content-right">Originating Part No.</p>\n' +
    '                            <select class="pull-right" ng-model="OriginPartNo" ng-options="option as option.showData for option in PartTemplateList"></select>\n' +
    '                        </div>\n' +
    '                        <div class="photocopy col-xs-6">\n' +
    '                            <button ng-click="CopyTemplate(OriginPartNo)">photocopy Teamplate</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-xs-4 live-job">\n' +
    '            <div class="row">\n' +
    '                <div class="col-xs-12">\n' +
    '                    <div class="row">\n' +
    '                        <div class="col-xs-10">\n' +
    '                            <p>Live jobs using this Part and issue</p>\n' +
    '                            <div class="area-table">\n' +
    '                                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_partdetail">\n' +
    '                                    <thead>\n' +
    '                                        <tr>\n' +
    '                                            <th>WO No.</th>\n' +
    '                                            <th>Customer</th>\n' +
    '                                        </tr>\n' +
    '                                    </thead>\n' +
    '                                    <tbody>\n' +
    '                                        <tr ng-repeat="item in PartDetail">\n' +
    '                                            <td ng-bind="item.WorkOrderID"></td>\n' +
    '                                            <td ng-bind="item.CustomerName"></td>\n' +
    '                                        </tr>\n' +
    '                                    </tbody>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row">\n' +
    '        <div class="col-xs-12 alert-lockdown">\n' +
    '            <div class="alert-btn col-xs-2">\n' +
    '                <button class="color-yellow" ng-show="isAlert" ng-click="TemplateAlert()">Alert</button>\n' +
    '                <button class="color-yellow" ng-hide="isAlert" ng-click="TemplateAlertOff()">Remove Alert</button>\n' +
    '            </div>\n' +
    '            <div class="lockdown-btn col-xs-2">\n' +
    '                <button class="color-red" ng-show="isLocked" ng-click="TemplateLock()">Lock down</button>\n' +
    '                <button class="color-red" ng-hide="isLocked" ng-click="TemplateLockOff()">Remove Lock</button>\n' +
    '            </div>\n' +
    '            <div class="part-history-btn col-xs-2">\n' +
    '                <button ng-click="redirectPartHistory()">Part History</button>\n' +
    '            </div>\n' +
    '            <div class="back-btn col-xs-6">\n' +
    '                <button class="pull-right" ng-click="redirectBack()">Back</button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/part-control/review-tasks/review-tasks.tpl.html',
    '<div class="col-xs-12 review-tasks-tab">\n' +
    '    <div class="row">\n' +
    '        <div class="review-table-button col-xs-11">\n' +
    '            <div class="review-table col-xs-10">\n' +
    '                <div class="area-table">\n' +
    '                    <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_review">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th>\n' +
    '                                    Review tasks for this Part\n' +
    '                                </th>\n' +
    '                                <th>Task Type</th>\n' +
    '                                <th>Date</th>\n' +
    '                                <th>\n' +
    '                                    <p>Requested depts</p>\n' +
    '                                    <p>FO Pu PE En Qu SP St Fi</p>\n' +
    '                                </th>\n' +
    '                                <th>Closed</th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="item in PartReviewTasks" ng-click="selectOneItem(PartReviewTasks, item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                <td ng-bind="item.Description"></td>\n' +
    '                                <td>\n' +
    '                                    <strong class="color-black" ng-bind="item.TaskType"></strong>\n' +
    '                                </td>\n' +
    '                                <td ng-bind="item.CreateDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                                <td>\n' +
    '                                    <input type="checkbox" ng-model="item.FO" ng-true-value="1" ng-false-value="0">\n' +
    '                                    <input type="checkbox" ng-model="item.Pu" ng-true-value="1" ng-false-value="0">\n' +
    '                                    <input type="checkbox" ng-model="item.PE" ng-true-value="1" ng-false-value="0">\n' +
    '                                    <input type="checkbox" ng-model="item.En" ng-true-value="1" ng-false-value="0">\n' +
    '                                    <input type="checkbox" ng-model="item.Qu" ng-true-value="1" ng-false-value="0">\n' +
    '                                    <input type="checkbox" ng-model="item.SP" ng-true-value="1" ng-false-value="0">\n' +
    '                                    <input type="checkbox" ng-model="item.St" ng-true-value="1" ng-false-value="0">\n' +
    '                                    <input type="checkbox" ng-model="item.Fi" ng-true-value="1" ng-false-value="0">\n' +
    '                                </td>\n' +
    '                                <td ng-bind="item.Closed"></td>\n' +
    '                            </tr>\n' +
    '                            <tr>\n' +
    '                                <td>\n' +
    '                                    <input type="text" maxlength="30" ng-model="Review.Description">\n' +
    '                                </td>\n' +
    '                                <td>\n' +
    '                                    <select ng-model="Review.Type" ng-options="option as option.ActionType for option in TypeList"></select>\n' +
    '                                </td>\n' +
    '                                <td></td>\n' +
    '                                <td>\n' +
    '                                    <input type="checkbox" ng-model="Review.FO" ng-true-value="1" ng-false-value="0">\n' +
    '                                    <input type="checkbox" ng-model="Review.Pu" ng-true-value="1" ng-false-value="0">\n' +
    '                                    <input type="checkbox" ng-model="Review.PE" ng-true-value="1" ng-false-value="0">\n' +
    '                                    <input type="checkbox" ng-model="Review.En" ng-true-value="1" ng-false-value="0">\n' +
    '                                    <input type="checkbox" ng-model="Review.Qu" ng-true-value="1" ng-false-value="0">\n' +
    '                                    <input type="checkbox" ng-model="Review.SP" ng-true-value="1" ng-false-value="0">\n' +
    '                                    <input type="checkbox" ng-model="Review.St" ng-true-value="1" ng-false-value="0">\n' +
    '                                    <input type="checkbox" ng-model="Review.Fi" ng-true-value="1" ng-false-value="0">\n' +
    '                                </td>\n' +
    '                                <td></td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="review-button col-xs-2">\n' +
    '                <button class="create-btn" ng-click="CreateReview(Review)">Create</button>\n' +
    '                <p>close selected task</p>\n' +
    '                <button class="close-btn" ng-click="CloseReview(PartReviewTasks)">Close</button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/production-schedule/machine/machine.tpl.html',
    '<div id="puma" class="tab_content">\n' +
    '    <div class="area-table">\n' +
    '        <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_ownermachine" load-item="OwnerMachine">\n' +
    '            <thead>\n' +
    '                <tr>\n' +
    '                    <th rowspan="2" class="space"></th>\n' +
    '                    <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[0], \'selected\'); changeSortBy(sortTable[0])" ng-class="{\'th-active\': sortTable[0].selected}">WO No.</th>\n' +
    '                    <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[1], \'selected\'); changeSortBy(sortTable[1])" ng-class="{\'th-active\': sortTable[1].selected}">CO No.</th>\n' +
    '                    <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[2], \'selected\'); changeSortBy(sortTable[2])" ng-class="{\'th-active\': sortTable[2].selected}">Part Number</th>\n' +
    '                    <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[3], \'selected\'); changeSortBy(sortTable[3])" ng-class="{\'th-active\': sortTable[3].selected}">Qty</th>\n' +
    '                    <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[4], \'selected\'); changeSortBy(sortTable[4])" ng-class="{\'th-active\': sortTable[4].selected}">Operation</th>\n' +
    '                    <th colspan="4">Dates</th>\n' +
    '                    <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[9], \'selected\'); changeSortBy(sortTable[9])" ng-class="{\'th-active\': sortTable[9].selected}">Resource</th>\n' +
    '                    <th rowspan="2" colspan="2" ng-click="selectOneItem(sortTable, sortTable[10], \'selected\'); changeSortBy(sortTable[10])" ng-class="{\'th-active\': sortTable[10].selected}">Progress</th>\n' +
    '                </tr>\n' +
    '                <tr>\n' +
    '                    <th ng-click="selectOneItem(sortTable, sortTable[5], \'selected\'); changeSortBy(sortTable[5])" ng-class="{\'th-active\': sortTable[5].selected}">DD Start</th>\n' +
    '                    <th ng-click="selectOneItem(sortTable, sortTable[6], \'selected\'); changeSortBy(sortTable[6])" ng-class="{\'th-active\': sortTable[6].selected}">Start</th>\n' +
    '                    <th ng-click="selectOneItem(sortTable, sortTable[7], \'selected\'); changeSortBy(sortTable[7])" ng-class="{\'th-active\': sortTable[7].selected}">Finish</th>\n' +
    '                    <th ng-click="selectOneItem(sortTable, sortTable[8], \'selected\'); changeSortBy(sortTable[8])" ng-class="{\'th-active\': sortTable[8].selected}">DD Finish</th>\n' +
    '                </tr>\n' +
    '            </thead>\n' +
    '            <tbody>\n' +
    '                <tr ng-repeat="item in OwnerMachine | orderBy : sortBy : reverse" ng-click="OperationSelected(item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                    <td ng-click="selectOneItem(OwnerMachine, item, \'selected\')"></td>\n' +
    '                    <td class="text-center underline color">\n' +
    '                        <a ui-sref="department.workorders.main({DeptID: 7, WorkOrderID: item.WorkOrderID})" ng-bind="item.WorkOrderID"></a>\n' +
    '                    </td>\n' +
    '                    <td class="text-center underline color">\n' +
    '                        <a ui-sref="department.customer-order({DeptID: 7, CustomerOrderId: item.OrderID})" ng-bind="item.OrderID"></a>\n' +
    '                    </td>\n' +
    '                    <td class="text-right underline color">\n' +
    '                        <a ui-sref="department.part-control.part-list({DeptID: 6, PTemplateID: item.PartTemplateID})" ng-bind="item.PartNumber"></a>\n' +
    '                    </td>\n' +
    '                    <td class="text-right" ng-bind="item.Qty"></td>\n' +
    '                    <td ng-bind="item.Operation"></td>\n' +
    '                    <td class="text-center" ng-bind="item.DDStart | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                    <td class="text-center" ng-bind="item.StartDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                    <td class="text-center" ng-bind="item.Finish | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                    <td class="text-center" ng-bind="item.DDFinish | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                    <td ng-bind="item.Resource"></td>\n' +
    '                    <td class="text-center font-big">\n' +
    '                        <a ng-click="NextProgress(OwnerMachine, item)" class="underline" href="javascript:;" ng-click="selectItemByOpNo(item.OpNo, OwnerMachine)" ng-bind="item.OpNo"></a>\n' +
    '                    </td>\n' +
    '                    <td class="text-center font-big" ng-bind="item.LastOp"></td>\n' +
    '                </tr>\n' +
    '            </tbody>\n' +
    '        </table>\n' +
    '    </div>\n' +
    '    <div class="table-control">\n' +
    '        <span class="up-control" ng-click="reverse = false"></span>\n' +
    '        <span>sort</span>\n' +
    '        <span class="down-control" ng-click="reverse = true"></span>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/production-schedule/material-issue/material-issue.tpl.html',
    '<div id="material-issue" class="tab_content">\n' +
    '    <div class="row">\n' +
    '        <div class="area-table">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_proMI" load-item="ProductionScheduleMI">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th rowspan="2" class="space"></th>\n' +
    '                        <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[0], \'selected\'); changeSortBy(sortTable[0])" ng-class="{\'th-active\': sortTable[0].selected}">WO No.</th>\n' +
    '                        <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[1], \'selected\'); changeSortBy(sortTable[1])" ng-class="{\'th-active\': sortTable[1].selected}">CO No.</th>\n' +
    '                        <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[2], \'selected\'); changeSortBy(sortTable[2])" ng-class="{\'th-active\': sortTable[2].selected}">Part Number</th>\n' +
    '                        <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[3], \'selected\'); changeSortBy(sortTable[3])" ng-class="{\'th-active\': sortTable[3].selected}">Qty</th>\n' +
    '                        <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[4], \'selected\'); changeSortBy(sortTable[4])" ng-class="{\'th-active\': sortTable[4].selected}">Operation</th>\n' +
    '                        <th colspan="4">Dates</th>\n' +
    '                        <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[9], \'selected\'); changeSortBy(sortTable[9])" ng-class="{\'th-active\': sortTable[9].selected}">Supplier</th>\n' +
    '                        <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[10], \'selected\'); changeSortBy(sortTable[10])" ng-class="{\'th-active\': sortTable[10].selected}">Ops</th>\n' +
    '                    </tr>\n' +
    '                    <tr>\n' +
    '                        <th ng-click="selectOneItem(sortTable, sortTable[5], \'selected\'); changeSortBy(sortTable[5])" ng-class="{\'th-active\': sortTable[5].selected}">DD Start</th>\n' +
    '                        <th ng-click="selectOneItem(sortTable, sortTable[6], \'selected\'); changeSortBy(sortTable[6])" ng-class="{\'th-active\': sortTable[6].selected}">Start</th>\n' +
    '                        <th ng-click="selectOneItem(sortTable, sortTable[7], \'selected\'); changeSortBy(sortTable[7])" ng-class="{\'th-active\': sortTable[7].selected}">Finish</th>\n' +
    '                        <th ng-click="selectOneItem(sortTable, sortTable[8], \'selected\'); changeSortBy(sortTable[8])" ng-class="{\'th-active\': sortTable[8].selected}">DD Finish</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in ProductionScheduleMI | orderBy: sortBy : reverse" ng-class="{\'tr-active\': item.selected}">\n' +
    '                        <td ng-click="selectOneItem(ProductionScheduleMI, item, \'selected\')"></td>\n' +
    '                        <td class="text-center underline color" ng-bind="item.WorkOrderID"></td>\n' +
    '                        <td class="text-center underline color" ng-bind="item.OrderID"></td>\n' +
    '                        <td class="text-right underline color" ng-bind="item.PartNumber"></td>\n' +
    '                        <td class="text-right" ng-bind="item.Qty"></td>\n' +
    '                        <td ng-bind="item.Operation"></td>\n' +
    '                        <td class="text-center" ng-bind="item.DDStart | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                        <td class="text-center" ng-bind="item.StartDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                        <td class="text-center" ng-bind="item.Finish | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                        <td class="text-center" ng-bind="item.DDFinish | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                        <td ng-bind="item.Supplier"></td>\n' +
    '                        <td class="text-center font-big" ng-bind="item.OpNo"></td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '        <div class="table-control">\n' +
    '            <span class="up-control" ng-click="reverse = false"></span>\n' +
    '            <span>sort</span>\n' +
    '            <span class="down-control" ng-click="reverse = true"></span>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/production-schedule/subcon/subcon.tpl.html',
    '<div id="subcon" class="tab_content">\n' +
    '    <div class="row">\n' +
    '        <div class="area-table">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_prosc" load-item="ProductionScheduleSC">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th rowspan="2" class="space"></th>\n' +
    '                        <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[0], \'selected\'); changeSortBy(sortTable[0])" ng-class="{\'th-active\': sortTable[0].selected}">WO No.</th>\n' +
    '                        <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[1], \'selected\'); changeSortBy(sortTable[1])" ng-class="{\'th-active\': sortTable[1].selected}">CO No.</th>\n' +
    '                        <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[2], \'selected\'); changeSortBy(sortTable[2])" ng-class="{\'th-active\': sortTable[2].selected}">Part Number</th>\n' +
    '                        <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[3], \'selected\'); changeSortBy(sortTable[3])" ng-class="{\'th-active\': sortTable[3].selected}">Qty</th>\n' +
    '                        <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[4], \'selected\'); changeSortBy(sortTable[4])" ng-class="{\'th-active\': sortTable[4].selected}">Operation</th>\n' +
    '                        <th colspan="4">Dates</th>\n' +
    '                        <th rowspan="2" ng-click="selectOneItem(sortTable, sortTable[9], \'selected\'); changeSortBy(sortTable[9])" ng-class="{\'th-active\': sortTable[9].selected}">Supplier</th>\n' +
    '                        <th rowspan="2" colspan="2" ng-click="selectOneItem(sortTable, sortTable[10], \'selected\'); changeSortBy(sortTable[10])" ng-class="{\'th-active\': sortTable[10].selected}">Progress</th>\n' +
    '                    </tr>\n' +
    '                    <tr>\n' +
    '                        <th ng-click="selectOneItem(sortTable, sortTable[5], \'selected\'); changeSortBy(sortTable[5])" ng-class="{\'th-active\': sortTable[5].selected}">DD Start</th>\n' +
    '                        <th ng-click="selectOneItem(sortTable, sortTable[6], \'selected\'); changeSortBy(sortTable[6])" ng-class="{\'th-active\': sortTable[6].selected}">Start</th>\n' +
    '                        <th ng-click="selectOneItem(sortTable, sortTable[7], \'selected\'); changeSortBy(sortTable[7])" ng-class="{\'th-active\': sortTable[7].selected}">Finish</th>\n' +
    '                        <th ng-click="selectOneItem(sortTable, sortTable[8], \'selected\'); changeSortBy(sortTable[8])" ng-class="{\'th-active\': sortTable[8].selected}">DD Finish</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in ProductionScheduleSC | orderBy: sortBy : reverse" ng-class="{\'tr-active\': item.selected}">\n' +
    '                        <td ng-click="selectOneItem(ProductionScheduleSC, item, \'selected\')"></td>\n' +
    '                        <td class="text-center underline color" ng-bind="item.WorkOrderID"></td>\n' +
    '                        <td class="text-center underline color" ng-bind="item.OrderID"></td>\n' +
    '                        <td class="text-right underline color" ng-bind="item.PartNumber"></td>\n' +
    '                        <td class="text-right" ng-bind="item.Qty"></td>\n' +
    '                        <td ng-bind="item.Operation"></td>\n' +
    '                        <td class="text-center" ng-bind="item.DDStart | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                        <td class="text-center" ng-bind="item.StartDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                        <td class="text-center" ng-bind="item.Finish | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                        <td class="text-center" ng-bind="item.DDFinish | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                        <td ng-bind="item.Supplier"></td>\n' +
    '                        <td class="text-center font-big">\n' +
    '                            <a ng-click="NextProgress(ProductionScheduleSC, item)" class="underline" href="javascript:;" ng-bind="item.OpNo"></a>\n' +
    '                        </td>\n' +
    '                        <td class="text-center font-big" ng-bind="item.LastOp"></td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '        <div class="table-control">\n' +
    '            <span class="up-control" ng-click="reverse = false"></span>\n' +
    '            <span>sort</span>\n' +
    '            <span class="down-control" ng-click="reverse = true"></span>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/purchase-order/details/details.tpl.html',
    '<div class="tab-pane details-tab">\n' +
    '    <div class="row">\n' +
    '        <div class="col-md-12 area">\n' +
    '            <div class="area-1">\n' +
    '                <div class="area-quote">\n' +
    '                    <label for="">Quote raised</label>\n' +
    '                    <input type="text" ng-model="RFQPODetail.QuoteRaised">\n' +
    '                </div>\n' +
    '                <div class="area-po">\n' +
    '                    <label for="">PO type: </label>\n' +
    '                    <select ng-model="POItemDetail.POType" ng-options="option as option.name for option in POTypes" ng-change="changePOType(POItemDetail.POType)">\n' +
    '                    </select>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="area-2">\n' +
    '                <div class="table-workorder">\n' +
    '                    <div class="table-title">Work Orders</div>\n' +
    '                    <div class="area-table">\n' +
    '                        <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_workorder" load-item="SelectedWODetail">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th ng-repeat="item in sortTable" ng-bind="item.title" ng-click="selectOneItem(sortTable, item, \'selected\'); changeSortBy(item)" ng-class="{\'th-active\': item.selected}"></th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-show="WODetail === undefined">\n' +
    '                                    <td>\n' +
    '                                        <select ng-model="WorkOrder" ng-options="option as option.WONo for option in WOList" ng-change="changeWONo(WorkOrder)">\n' +
    '                                        </select>\n' +
    '                                    </td>\n' +
    '                                    <td></td>\n' +
    '                                    <td></td>\n' +
    '                                </tr>\n' +
    '                                <tr ng-hide="WODetail === undefined" ng-repeat="item in SelectedWODetail | orderBy: sortBy : reverse | unique:\'PartNumber\'">\n' +
    '                                    <td>\n' +
    '                                        <select ng-model="item.WONo" ng-options="option as option.WONo for option in WOList" ng-change="changeWONo(item.WONo)">\n' +
    '                                        </select>\n' +
    '                                    </td>\n' +
    '                                    <td ng-bind="item.PartNumber"></td>\n' +
    '                                    <td ng-bind="item.Qty"></td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                    <div class="table-control">\n' +
    '                        <span class="up-control" ng-click="reverse = false"></span>\n' +
    '                        <span>sort</span>\n' +
    '                        <span class="down-control" ng-click="reverse = true"></span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="area-options">\n' +
    '                    <div class="row">\n' +
    '                        <div class="operation col-xs-2">\n' +
    '                            <label>Operation</label>\n' +
    '                            <input class="color-blue border currentOperation" type="text" ng-model="WODetail.OperationNo" maxlength="3"></input>\n' +
    '                            <label class="color-blue">of {{MaxOperation.OperationNo}}</label>\n' +
    '                        </div>\n' +
    '                        <div class="material col-xs-2">\n' +
    '                            <label for="" ng-bind="WODetail.OpDesc"></label>\n' +
    '                        </div>\n' +
    '                        <div class="link-btn col-xs-1 col-xs-offset-1">\n' +
    '                            <button type="button" ng-click="LinkToPOItem()">Link</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row no-margin">\n' +
    '                        <div class="options">\n' +
    '                            <div class="element element-1">\n' +
    '                                <div class="row">\n' +
    '                                    <div class="col-xs-2 item-number">\n' +
    '                                        <label>Item Number</label>\n' +
    '                                        <label class="color-blue" ng-bind="CurrentItem"></label>\n' +
    '                                    </div>\n' +
    '                                    <div class="col-xs-2 item-control">\n' +
    '                                        <span class="left-control" ng-click="getPOItemDetail(\'prev\')"></span>\n' +
    '                                        <span class="right-control" ng-click="addNewItem()"></span>\n' +
    '                                        <span class="color-blue font-weight">of {{POItemDetail.TotalItems}}</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="border">\n' +
    '                                <form name="OrderForm" ng-submit="OrderForm.$valid && UpdateOrder(POItemDetail)" novalidate>\n' +
    '                                    <div class="element element-2">\n' +
    '                                        <label class="label-quantity">Quantity</label>\n' +
    '                                        <input type="text" class="input-quantity" ng-model="POItemDetail.Qty" name="Qty" ng-change="totalPrice(PriceType, POItemDetail)" ng-pattern="/^\\d+$/" maxlength="8" />\n' +
    '                                        <select ng-model="UOM" ng-options="option as option.name for option in UOMs" ng-show="POTypeOther1">\n' +
    '                                            <option value=""></option>\n' +
    '                                        </select>\n' +
    '                                        <label class="label-spu" ng-hide="POTypeOther1">SPU</label>\n' +
    '                                        <input ng-hide="POTypeOther1" type="text" class="input-spu" ng-model="POItemDetail.SPU" name="SPU" ng-pattern="/^\\d+$/" maxlength="8" />\n' +
    '                                        <select ng-hide="POTypeOther1" ng-model="SPUDetail" ng-options="option as option.Description for option in UOMList" ng-change="POItemDetail.setSPUID(SPUDetail.pkUOMID)" name="SPUDetail">\n' +
    '                                        </select>\n' +
    '                                        <button class="save" type="submit">Save</button>\n' +
    '                                    </div>\n' +
    '                                    <div class="element element-3" ng-hide="POTypeOther1">\n' +
    '                                        <label>Size</label>\n' +
    '                                        <label>Units</label>\n' +
    '                                        <label>Shape</label>\n' +
    '                                        <div>\n' +
    '                                            <input type="text" class="size" ng-model="POItemDetail.Size" name="Size" maxlength="8" ng-pattern="/(^\\d{1,7}$)|(^\\d{1,6}\\.\\d{1}$)|(^\\d{1,5}\\.\\d{1,2}$)|(^\\d{1,4}\\.\\d{1,3}$)/">\n' +
    '                                            <select ng-model="UnitDetail" class="units" ng-options="option as option.name for option in UnitList" ng-change="POItemDetail.setUnitID(UnitDetail.value)" name="UnitDetail">\n' +
    '                                            </select>\n' +
    '                                            <select class="shape" ng-model="ShapeDetail" ng-options="option as option.Shape for option in ShapeList" ng-change="POItemDetail.setShapeID(ShapeDetail.ShapeID)" name="ShapeDetail">\n' +
    '                                            </select>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="element">\n' +
    '                                        <label for="" class="label-description">Description</label>\n' +
    '                                        <input type="text" class="input-description" ng-model="POItemDetail.PartDescription" maxlength="45">\n' +
    '                                    </div>\n' +
    '                                    <div class="element">\n' +
    '                                        <label for="" class="label-comment">Comment</label>\n' +
    '                                        <input type="text" class="comment" ng-model="POItemDetail.Comment" maxlength="45">\n' +
    '                                    </div>\n' +
    '                                    <div class="element no-margin">\n' +
    '                                        <label for="" class="label-price">Price</label>\n' +
    '                                        <input type="text" class="input-price" ng-change="totalPrice(PriceType, POItemDetail)" ng-model="POItemDetail.Price" name="Price" maxlength="8" ng-pattern="/(^\\d{1,7}$)|(^\\d{1,6}\\.\\d{1}$)|(^\\d{1,5}\\.\\d{1,2}$)/">\n' +
    '                                        <select ng-model="PriceType" ng-change="totalPrice(PriceType, POItemDetail); POItemDetail.setPer(PriceType.value)" class="price-type" ng-options="option as option.name for option in PriceList" name="PriceType">\n' +
    '                                        </select>\n' +
    '                                        <label for="" class="label-total-price">Total Price</label>\n' +
    '                                        <span class="span-total-price currency-symbol currency-symbol-irish right">\n' +
    '                                                <input type="text" readonly ng-model="TotalPrice" ng-change="totalPrice(PriceType, POItemDetail)">\n' +
    '                                            </span>\n' +
    '                                    </div>\n' +
    '                                </form>\n' +
    '                            </div>\n' +
    '                            <button class="back" type="button" ng-click="redirectBack()">Back</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/purchase-order/instructions/instructions.tpl.html',
    '<div id="instructions" class="tab-pane comment-tab">\n' +
    '    <div class="tab-title">PO comment</div>\n' +
    '    <div class="tab-body">\n' +
    '        <div class="comment-table">\n' +
    '            <div class="area-table">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_pocomment">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th>PO comment</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in OrderComments">\n' +
    '                            <td ng-bind="item.Comment"></td>\n' +
    '                        </tr>\n' +
    '                        <tr>\n' +
    '                            <td>\n' +
    '                                <input type="text" list="CommentList" ng-model="Comment" ng-change="changeComment(Comment)" maxlength="100" ng-keyup="addComment($event, Comment)" ng-keydown="addComment($event, Comment)">\n' +
    '                                <datalist id="CommentList">\n' +
    '                                    <option value="{{item.Comment}}" ng-repeat="item in CommentList"></option>\n' +
    '                                </datalist>\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="drop-date">\n' +
    '            <div class="area-table">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_dropdate">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th>Drop date</th>\n' +
    '                            <th>Type</th>\n' +
    '                            <th>Auto print</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in OrderDocuments | unique:\'DocType\'">\n' +
    '                            <td ng-bind="item.DropDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                            <td ng-bind="item.DocType"></td>\n' +
    '                            <td>\n' +
    '                                <input type="checkbox" ng-model="item.AutoPrint" ng-true-value="1" ng-false-value="0" ng-click="UpdateOrderDocument(item)">\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/purchase-order/invoice/invoice.tpl.html',
    '<div id="invoice" class="tab-pane invoice-tab">\n' +
    '    <div class="col-md-12 tab-content">\n' +
    '        <div class="grb-area col-md-12">\n' +
    '            <div class="grb col-md-12">\n' +
    '                <div class="area-table col-md-7">\n' +
    '                    <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_link">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th>Link</th>\n' +
    '                                <th>GRB No.</th>\n' +
    '                                <th>Item</th>\n' +
    '                                <th>Qty</th>\n' +
    '                                <th>Date in</th>\n' +
    '                                <th>Description</th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="item in UnpaidPurchases" ng-click="selectOneItem(UnpaidPurchases, item, \'selected\'); selectedUnpaidPurchases(item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                <td>\n' +
    '                                    <input type="checkbox" ng-model="item.Link" ng-true-value="1" ng-false-value="0" ng-click="selectItem(item, \'selected\'); calculateInvoiceAndAddGRB(UnpaidPurchases)">\n' +
    '                                </td>\n' +
    '                                <td>\n' +
    '                                    <a href="javascript:;" ng-bind="item.fkGRBID" ng-click="redirectGoodsIn(item.fkGRBID)"></a>\n' +
    '                                </td>\n' +
    '                                <td ng-bind="item.ItemNo"></td>\n' +
    '                                <td ng-bind="item.Qty"></td>\n' +
    '                                <td ng-bind="item.DateIn | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                                <td ng-bind="item.Description"></td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '                <div class="drop-invoice col-md-2 col-md-offset-3">\n' +
    '                    <div class="drop">\n' +
    '                        <button type="button" dropzone on-success="uploadSuccess" before-upload="beforeUpload" max-file-name="20">Drop Invoice here</button>\n' +
    '                    </div>\n' +
    '                    <div class="invoice-no">\n' +
    '                        <span>Invoice No.</span>\n' +
    '                        <input type="text" maxlength="25" ng-model="DropInvoice.InvoiceNo">\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="invoice-total col-md-7 col-md-offset-5 no-padding">\n' +
    '                Estimated invoice total\n' +
    '                <input class="total" type="text" value="&#8356;{{InvoiceTotal}}"> actual total\n' +
    '                <input type="text" class="actual-total" ng-model="DropInvoice.ActualTotal"> invoice ok\n' +
    '                <input type="checkbox" ng-model="DropInvoice.InvoiceOK" ng-true-value="1" ng-false-value="0" ng-click="RmvGRBInvoice(DropInvoice.InvoiceOK)">\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="invoice-table col-md-12">\n' +
    '            <div class="raise-adjustment col-md-7">\n' +
    '                <div class="col-md-5 adjustment-reject">\n' +
    '                    <div class="adjustment-title">Raise an adjustment / reject</div>\n' +
    '                    <div class="adjustment-no">\n' +
    '                        <input type="text" ng-model="NextRejectNo">RN\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-md-6 raise-note no-padding">\n' +
    '                    <div class="select-invoice">\n' +
    '                        <span>Select Invoice</span>\n' +
    '                        <select ng-model="RaiseNote.Invoice" ng-options="option as option.InvoiceRef for option in AllInvoiceList">\n' +
    '                        </select>\n' +
    '                    </div>\n' +
    '                    <div class="debit-amount">\n' +
    '                        <span>Debit amount</span>\n' +
    '                        <span class="currency-symbol currency-symbol-irish left">\n' +
    '                            <input type="text" ng-model="RaiseNote.DebitAmount">\n' +
    '                        </span>\n' +
    '                    </div>\n' +
    '                    <div class="reason">\n' +
    '                        <span>Reason</span>\n' +
    '                        <select ng-model="RaiseNote.Reason">\n' +
    '                            <option value="1">Incorrect Invoice price</option>\n' +
    '                            <option value="2">Credit not rcvd</option>\n' +
    '                            <option value="3">Goods tainted</option>\n' +
    '                        </select>\n' +
    '                    </div>\n' +
    '                    <div class="button">\n' +
    '                        <button class="btn-reject" ng-click="RaiseReject(RaiseNote)">Raise Reject note</button>\n' +
    '                        <button class="btn-adjustment" ng-click="RaiseAdjustment(RaiseNote)">Raise Adjustmnt</button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="raise-table col-md-4 col-md-offset-1">\n' +
    '                <div class="area-table">\n' +
    '                    <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_invoice">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th>Invoice No.</th>\n' +
    '                                <th>Expected price</th>\n' +
    '                                <th>Actual price</th>\n' +
    '                                <th>Added by</th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="item in POInvoices">\n' +
    '                                <td ng-bind="item.InvoiceNo"></td>\n' +
    '                                <td>&#8356;{{item.ExpectedPrice}}</td>\n' +
    '                                <td>&#8356;{{item.ActualPrice}}</td>\n' +
    '                                <td ng-bind="item.User"></td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/purchase-order/material-search/material-search.tpl.html',
    '<div id="material-search" class="tab-pane material-tab">\n' +
    '    <div class="col-md-12 tab-content">\n' +
    '        <div class="tab-search">\n' +
    '            <div class="tab-title">\n' +
    '                <span>Enter spec</span>\n' +
    '                <typeahead source="SearchMaterialSpec" key="MaterialSpec" select-callback="MaterialSelected"></typeahead>\n' +
    '            </div>\n' +
    '            <div class="tab-button">\n' +
    '                <button ng-click="SearchMaterial()">Search</button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="tab-body">\n' +
    '            <div class="area-table">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_date">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th>Date</th>\n' +
    '                            <th>PO number</th>\n' +
    '                            <th>Supplier</th>\n' +
    '                            <th>Qty</th>\n' +
    '                            <th>UOM</th>\n' +
    '                            <th>Price paid</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in MatchingSpecPOs">\n' +
    '                            <td ng-bind="item.OrderDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                            <td ng-bind="item.PONo"></td>\n' +
    '                            <td ng-bind="item.SupplierName"></td>\n' +
    '                            <td ng-bind="item.Qty"></td>\n' +
    '                            <td ng-bind="item.UOM"></td>\n' +
    '                            <td ng-bind="item.Price"></td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/purchase-order/notes/notes.tpl.html',
    '<div id="notes" class="tab-pane notes-tab">\n' +
    '    <div class="col-md-12 tab-content">\n' +
    '        <div class="tab-title">Progress: </div>\n' +
    '        <div class="tab-body">\n' +
    '            <div class="area-table">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_staff">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th>Staff member</th>\n' +
    '                            <th>Contact type</th>\n' +
    '                            <th>Progress Notes</th>\n' +
    '                            <th>Date</th>\n' +
    '                            <th>Chase</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in OrderProgress">\n' +
    '                            <td ng-bind="item.StaffMember"></td>\n' +
    '                            <td>\n' +
    '                                <select ng-model="item.ContactTypeID" ng-options="option as option.name for option in ContactTypes">\n' +
    '                                </select>\n' +
    '                            </td>\n' +
    '                            <td ng-bind="item.Progress"></td>\n' +
    '                            <td ng-bind="item.CreateDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                            <td>{{item.Chase == 1 ? \'Y\' : \'N\'}}</td>\n' +
    '                        </tr>\n' +
    '                        <tr my-enter="AddOrderProgress(OrderProgressItem)">\n' +
    '                            <td></td>\n' +
    '                            <td>\n' +
    '                                <select ng-model="OrderProgressItem.ContactType" ng-options="option as option.name for option in ContactTypes">\n' +
    '                                </select>\n' +
    '                            </td>\n' +
    '                            <td>\n' +
    '                                <input type="text" ng-model="OrderProgressItem.Progress" maxlength="50">\n' +
    '                            </td>\n' +
    '                            <td></td>\n' +
    '                            <td>\n' +
    '                                <input type="checkbox" ng-model="OrderProgressItem.Chase" ng-true-value="1" ng-false-value="0">\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/purchase-order/quality/quality.tpl.html',
    '<div id="quality" class="tab-pane quality-tab">\n' +
    '    <div class="quality-body col-xs-10">\n' +
    '        <div class="row">\n' +
    '            <div class="reject-note col-xs-5">Reject note</div>\n' +
    '            <div class="reject-quanlity col-xs-3">\n' +
    '                <div class="open-grb">\n' +
    '                    <span>Open GRBs</span>\n' +
    '                    <select ng-model="POReject.GRBID" ng-options="option as option.GRBID for option in POOpenGRBList">\n' +
    '                    </select>\n' +
    '                </div>\n' +
    '                <div class="reject-qty">\n' +
    '                    <span>Rejected Qty</span>\n' +
    '                    <input type="text" ng-model="POReject.RejectQty">\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="reject-note-no col-xs-3">\n' +
    '                <span>Reject Note No.</span>\n' +
    '                <input type="text" ng-model="NextRejectNo" readonly>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row">\n' +
    '            <div class="deviation col-xs-8">\n' +
    '                <span>Deviation</span>\n' +
    '                <textarea ng-model="POReject.Deviation" cols="30" rows="3" maxlength="200"></textarea>\n' +
    '            </div>\n' +
    '            <div class="reject-clause col-xs-3 no-padding">\n' +
    '                <span>Reject clause</span>\n' +
    '                <select class="pull-right" ng-model="POReject.RejectClauseID">\n' +
    '                    <option value="1">Rectification</option>\n' +
    '                    <option value="2">Replacement</option>\n' +
    '                    <option value="3">Credit only</option>\n' +
    '                    <option value="4">Information only</option>\n' +
    '                </select>\n' +
    '                <div class="create-reject">\n' +
    '                    <button ng-click="AddPOReject(POReject)">Create Reject</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row">\n' +
    '            <div class="reject-close col-xs-12">\n' +
    '                Reject Closed (Quality only)\n' +
    '                <input type="checkbox" ng-model="POReject.Closed">\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="drop-quality col-xs-2">\n' +
    '        <button type="button" dropzone on-success="uploadSuccess" before-upload="beforeUpload" max-file-name="30">Drop Non-Conformance Reports</button>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/purchase-order/raise/raise.tpl.html',
    '<div id="raise" class="tab-pane raise-tab">\n' +
    '    <div class="button">\n' +
    '        <button class="preview" ng-click="openBIRT(\'preview\')">Preview PO</button>\n' +
    '        <button class="raise" ng-click="openBIRT(\'raise\')">Raise / reprint PO</button>\n' +
    '                <button class="copy" ng-click="CopyPO()">C\'opy PO</button>\n' +
    '    </div>\n' +
    '    <div class="table-content">\n' +
    '        <div class="table-title">Previous purchase</div>\n' +
    '        <div class="area-table">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_order" load-item="PrevPurchases">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th ng-repeat="item in sortTable" ng-bind="item.title" ng-click="selectOneItem(sortTable, item, \'selected\'); changeSortBy(item)" ng-class="{\'th-active\': item.selected}" class="{{item.class}}"></th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in PrevPurchases | orderBy : sortBy : reverse" ng-click="selectOneItem(PrevPurchases, item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                        <td ng-bind="item.OrderDate | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                        <td ng-bind="item.SupplierName"></td>\n' +
    '                        <td ng-bind="item.Qty"></td>\n' +
    '                        <td ng-bind="item.OpNo"></td>\n' +
    '                        <td ng-bind="item.Unit"></td>\n' +
    '                        <td ng-bind="item.Description"></td>\n' +
    '                        <td ng-bind="item.Size"></td>\n' +
    '                        <td ng-bind="item.Shape"></td>\n' +
    '                        <td>&#8356;{{item.EachPrice}}</td>\n' +
    '                        <td>&#8356;{{item.OrderValue}}</td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '        <div class="table-control">\n' +
    '            <span class="up-control" ng-click="reverse = false"></span>\n' +
    '            <span>sort</span>\n' +
    '            <span class="down-control" ng-click="reverse = true"></span>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/purchase-order/rfq/rfq.tpl.html',
    '<div id="rfq" class="tab-pane rfq-tab">\n' +
    '    <div class="tab-title">\n' +
    '        <span>ESL Contact</span>\n' +
    '        <select ng-model="Contact" ng-options="option as option.UName for option in ContactList"></select>\n' +
    '    </div>\n' +
    '    <div class="tab-body">\n' +
    '        <div class="area-table">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_supplier" load-item="SelectedSuppliers">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th ng-repeat="item in sortTable" ng-bind="item.title" ng-click="selectOneItem(sortTable, item, \'selected\'); changeSortBy(item)" ng-class="{\'th-active\': item.selected}"></th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in SelectedSuppliers | orderBy : sortBy : reverse" ng-click="selectOneItem(SelectedSuppliers, item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                        <td>\n' +
    '                            <select ng-model="item.SupplierID" ng-options="option as option.SupplierName for option in SupplierList">\n' +
    '                            </select>\n' +
    '                        </td>\n' +
    '                        <td>\n' +
    '                            <input type="text" ng-model="item.TotalCost">\n' +
    '                        </td>\n' +
    '                        <td>\n' +
    '                            <input type="text" ng-model="item.DeliveryPrice">\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                    <tr my-enter="AddRFQ(itemAddRFQ)">\n' +
    '                        <td>\n' +
    '                            <select ng-model="itemAddRFQ.SupplierID" ng-options="option as option.SupplierName for option in SupplierList">\n' +
    '                            </select>\n' +
    '                        </td>\n' +
    '                        <td>\n' +
    '                            <input type="text" ng-model="itemAddRFQ.TotalCost">\n' +
    '                        </td>\n' +
    '                        <td>\n' +
    '                            <input type="text" ng-model="itemAddRFQ.DeliveryPrice">\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '        <div class="table-control">\n' +
    '            <span class="up-control" ng-click="reverse = false"></span>\n' +
    '            <span>sort</span>\n' +
    '            <span class="down-control" ng-click="reverse = true"></span>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="tab-footer">\n' +
    '        <span ng-bind="SelectedSuppliers | currentIndex"></span>\n' +
    '        <span class="prev-control" ng-click="selectPrevItem(SelectedSuppliers)"></span>\n' +
    '        <span class="next-control" ng-click="selectNextItem(SelectedSuppliers)"></span>\n' +
    '        <span ng-bind="SelectedSuppliers.length"></span>\n' +
    '        <button type="button" ng-click="RaiseRFQ()">Raise RFQ</button>\n' +
    '        <button type="button" ng-click="generateEmail()" class="generate-email">Generate email</button>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/purchase-order/supplier/supplier.tpl.html',
    '<div id="supplier" class="tab-pane supplier-tab">\n' +
    '    <div class="area supplier">\n' +
    '        <span>Supplier</span>\n' +
    '        <select ng-model="POSupplierDetail.SupplierID" ng-options="option as option.SupplierName for option in SupplierList" ng-change="SelectSupplier(POSupplierDetail.SupplierID); UpdatePOSupplier(POSupplierDetail)">\n' +
    '        </select>\n' +
    '    </div>\n' +
    '    <div class="area supplier-ref">\n' +
    '        <span>Supplier Ref</span>\n' +
    '        <input type="text" ng-model="POSupplierDetail.SupplierRef" maxlength="15" ng-change="UpdatePOSupplier(POSupplierDetail)" />\n' +
    '    </div>\n' +
    '    <div class="area client-contact">\n' +
    '        <span>Client Contact</span>\n' +
    '        <select ng-model="POSupplierDetail.ClientContactID" ng-options="option as option.ContactName for option in SupplierContacts" ng-change="UpdatePOSupplier(POSupplierDetail)">\n' +
    '        </select>\n' +
    '    </div>\n' +
    '    <div class="area delivery-charge">\n' +
    '        <span>Delivery Charge</span>\n' +
    '        <input type="text" ng-model="POSupplierDetail.DeliveryCharge" ng-change="TotalNetOrder(POSupplierDetail); UpdatePOSupplier(POSupplierDetail)" maxlength="6" />\n' +
    '    </div>\n' +
    '    <div class="area certification">\n' +
    '        <span class="span-certification">Certification</span>\n' +
    '        <input type="text" class="input-certification" maxlength="6" ng-model="POSupplierDetail.CertCharge" ng-change="TotalNetOrder(POSupplierDetail); UpdatePOSupplier(POSupplierDetail)" />\n' +
    '        <span class="total-cost">Total Net Order Cost</span>\n' +
    '        <span class="value-total-cost">\n' +
    '            <input type="text" placeholder="&#8356;" ng-model="POSupplierDetail.TotalNetCost" readonly />\n' +
    '        </span>\n' +
    '    </div>\n' +
    '    <div class="area approval">\n' +
    '        <span>Approval</span>\n' +
    '        <input type="text" ng-model="POSupplierDetail.ApprovalComment" ng-change="UpdatePOSupplier(POSupplierDetail)" maxlength="45" />\n' +
    '    </div>\n' +
    '    <div class="area delivery-days">\n' +
    '        <span>Delivery Days</span>\n' +
    '        <input type="text" ng-model="POSupplierDetail.DeliveryDays" ng-change="UpdatePOSupplier(POSupplierDetail)" maxlength="6" />\n' +
    '    </div>\n' +
    '    <div class="area delivery-date">\n' +
    '        <span>Delivery Date</span>\n' +
    '        <input type="text" ng-model="POSupplierDetail.DeliveryDate" ng-change="UpdatePOSupplier(POSupplierDetail)" />\n' +
    '    </div>\n' +
    '    <div class="area del-comments">\n' +
    '        <span>Del Comments</span>\n' +
    '        <select ng-model="POSupplierDetail.DeliveryCommentID" ng-options="option as option.DeliveryComment for option in DeliveryCommentsList" ng-change="UpdatePOSupplier(POSupplierDetail)">\n' +
    '        </select>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/supply-chain/main/main.tpl.html',
    '<div class="supply-chain-page">\n' +
    '    <div class="title-global-page">Supply Chain</div>\n' +
    '    <div class="page-body">\n' +
    '        <form ng-submit="SupplyChain.$valid && SaveOrUpdateSupplier(SupplierDetail)" name="SupplyChain" novalidate>\n' +
    '            <div class="supply-chain-info">\n' +
    '                <div class="col-md-3 no-padding">\n' +
    '                    <div class="select-supplier col-md-12 no-padding">\n' +
    '                        <div class="title col-md-12 no-padding">Please select a Supplier:</div>\n' +
    '                        <div class="select-supplier-group col-md-12">\n' +
    '                            <label for="">Look for..</label>\n' +
    '                            <search-input search-input-model="searchInputModel" procedure="ProcGetSupplierList" search-key="SearchString" ng-model-options="{debounce: 500}" display-key="Supplier" on-select="SupplierSelected"></search-input>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="supplier-type col-md-12 no-padding">\n' +
    '                        <div class="title col-md-12 no-padding">Supplier type:</div>\n' +
    '                        <div class="supplier-type-group col-md-12">\n' +
    '                            <div class="yes-no-supplier col-md-12">\n' +
    '                                <label for="" class="col-md-6">&nbsp;</label>\n' +
    '                                <div class="yes-no-supplier-group col-md-6">\n' +
    '                                    <div class="yes-select col-md-6">\n' +
    '                                        <label for="">Yes</label>\n' +
    '                                    </div>\n' +
    '                                    <div class="no-select col-md-6">\n' +
    '                                        <label for="">No</label>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="raw-material col-md-12 no-padding">\n' +
    '                                <label for="" class="col-md-6 no-padding">Raw Material?</label>\n' +
    '                                <div class="raw-material-group col-md-6">\n' +
    '                                    <div class="yes-select col-md-6">\n' +
    '                                        <input type="radio" name="raw-material" ng-model="SupplierDetail.MaterialSupplier" ng-value="1">\n' +
    '                                    </div>\n' +
    '                                    <div class="no-select col-md-6">\n' +
    '                                        <input type="radio" name="raw-material" ng-model="SupplierDetail.MaterialSupplier" ng-value="0">\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="treatments col-md-12 no-padding">\n' +
    '                                <label for="" class="col-md-6 no-padding">Treatments?</label>\n' +
    '                                <div class="treatments-group col-md-6">\n' +
    '                                    <div class="yes-select col-md-6">\n' +
    '                                        <input type="radio" ng-model="SupplierDetail.TreatmentSupplier" ng-value="1">\n' +
    '                                    </div>\n' +
    '                                    <div class="no-select col-md-6">\n' +
    '                                        <input type="radio" ng-model="SupplierDetail.TreatmentSupplier" ng-value="0">\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="tooling col-md-12 no-padding">\n' +
    '                                <label for="" class="col-md-6 no-padding">Tooling?</label>\n' +
    '                                <div class="tooling-group col-md-6">\n' +
    '                                    <div class="yes-select col-md-6">\n' +
    '                                        <input type="radio" name="tooling" ng-model="SupplierDetail.ToolingSupplier" ng-value="1">\n' +
    '                                    </div>\n' +
    '                                    <div class="no-select col-md-6">\n' +
    '                                        <input type="radio" name="tooling" ng-model="SupplierDetail.ToolingSupplier" ng-value="0">\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="calibration col-md-12 no-padding">\n' +
    '                                <label for="" class="col-md-6 no-padding">Calibration?</label>\n' +
    '                                <div class="calibration-group col-md-6">\n' +
    '                                    <div class="yes-select col-md-6">\n' +
    '                                        <input type="radio" name="calibration" ng-model="SupplierDetail.CalibrationSupplier" ng-value="1">\n' +
    '                                    </div>\n' +
    '                                    <div class="no-select col-md-6">\n' +
    '                                        <input type="radio" name="calibration" ng-model="SupplierDetail.CalibrationSupplier" ng-value="0">\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="miscellaneous col-md-12 no-padding">\n' +
    '                                <label for="" class="col-md-6 no-padding">Miscellaneous?</label>\n' +
    '                                <div class="miscellaneous-group col-md-6">\n' +
    '                                    <div class="yes-select col-md-6">\n' +
    '                                        <input type="radio" name="miscellaneous" ng-model="SupplierDetail.MiscSupplier" ng-value="1">\n' +
    '                                    </div>\n' +
    '                                    <div class="no-select col-md-6">\n' +
    '                                        <input type="radio" name="miscellaneous" ng-model="SupplierDetail.MiscSupplier" ng-value="0">\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="minimum-order col-md-10 no-padding-right">\n' +
    '                        <label for="" class="col-md-8">Minimum order charge:</label>\n' +
    '                        <div class="minimum-order-group col-md-4">\n' +
    '                            <input type="text" ng-model="SupplierDetail.MinOrderCharge">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="payment-terms col-md-10 no-padding-right">\n' +
    '                        <label for="" class="col-md-8 content-right">Payment terms</label>\n' +
    '                        <div class="payment-terms-group col-md-4 no-padding">\n' +
    '                            <select ng-model="PaymentTerms" ng-options="option as option.name for option in ListPaymentTerms" ng-change="SupplierDetail.setPaymentTerms(PaymentTerms.value)" name="paymentterms" required>\n' +
    '                            </select>\n' +
    '                        </div>\n' +
    '                        <div class="error">\n' +
    '                            <span ng-show="SupplyChain.$submitted && SupplyChain.paymentterms.$error.required">Please select Payment terms</span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="current-supplier col-md-10 no-padding-right">\n' +
    '                        <label for="" class="col-md-11 content-right">Current Supplier</label>\n' +
    '                        <div class="current-supplier-group col-xs-1 no-padding content-right">\n' +
    '                            <input type="checkbox" ng-model="SupplierDetail.Current" ng-true-value="1" ng-false-value="0">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-md-6 no-padding">\n' +
    '                    <div class="element account col-md-12">\n' +
    '                        <div class="element-content col-md-12">\n' +
    '                            <div class="element-account element-group col-md-12">\n' +
    '                                <label for="" class="title col-md-4">Account No.</label>\n' +
    '                                <div class="element-account-group col-md-8">\n' +
    '                                    <label ng-bind="SupplierInfo.SupplierID"></label>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="element name-alias col-md-12">\n' +
    '                        <div class="element-content col-md-10">\n' +
    '                            <div class="element-name element-group col-md-12">\n' +
    '                                <label for="" class="title col-md-4">Name</label>\n' +
    '                                <div class="element-name-group col-md-8">\n' +
    '                                    <input type="text" name="name" ng-model="SupplierDetail.Supplier" maxlength="50" required>\n' +
    '                                    <div class="error">\n' +
    '                                        <span ng-show="SupplyChain.$submitted && SupplyChain.name.$error.required">Please enter Name</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="element-alias element-group col-md-12">\n' +
    '                                <label for="" class="title col-md-4">Alias</label>\n' +
    '                                <div class="element-alias-group col-md-7">\n' +
    '                                    <input type="text" name="alias" ng-model="SupplierDetail.Alias" maxlength="40" required>\n' +
    '                                    <div class="error">\n' +
    '                                        <span ng-show="SupplyChain.$submitted && SupplyChain.alias.$error.required">Please enter Alias</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="element-button col-md-2">\n' +
    '                            <button class="remove" type="button" ng-click="RmvSupplierApproval(SupplierApprovals)">Rmv</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="element address col-md-12">\n' +
    '                        <div class="element-content col-md-10">\n' +
    '                            <div class="element-address-1 element-group col-md-12">\n' +
    '                                <label for="" class="title col-md-4">Address line 1</label>\n' +
    '                                <div class="element-address-1-group col-md-8">\n' +
    '                                    <input type="text" name="address1" ng-model="SupplierDetail.Address1" maxlength="45" required>\n' +
    '                                    <div class="error">\n' +
    '                                        <span ng-show="SupplyChain.$submitted && SupplyChain.address1.$error.required">Please enter Address 1</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="element-address-2 element-group col-md-12">\n' +
    '                                <label for="" class="title col-md-4">Address line 2</label>\n' +
    '                                <div class="element-address-2-group col-md-8">\n' +
    '                                    <input type="text" name="address2" ng-model="SupplierDetail.Address2" maxlength="40">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="element-address-3 element-group col-md-12">\n' +
    '                                <label for="" class="title col-md-4">Address line 3</label>\n' +
    '                                <div class="element-address-3-group col-md-8">\n' +
    '                                    <input type="text" name="address3" ng-model="SupplierDetail.Address3" maxlength="40">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="element-address-4 element-group col-md-12">\n' +
    '                                <label for="" class="title col-md-4">Address line 4</label>\n' +
    '                                <div class="element-address-4-group col-md-7">\n' +
    '                                    <input type="text" name="address4" ng-model="SupplierDetail.Address4" maxlength="30">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="element-button col-md-2">\n' +
    '                            <button class="check-site" ng-click="Checksite()">check site</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="element post-code col-md-12">\n' +
    '                        <div class="element-content col-md-10">\n' +
    '                            <div class="element-post-code element-group col-md-12">\n' +
    '                                <label for="" class="title col-md-4">Postcode</label>\n' +
    '                                <div class="element-post-code-group col-md-5">\n' +
    '                                    <input type="text" name="postcode" ng-model="SupplierDetail.Postcode" maxlength="8" required ng-pattern="/^\\d+$/">\n' +
    '                                    <div class="error">\n' +
    '                                        <span ng-show="SupplyChain.$submitted && SupplyChain.postcode.$error.required">Please enter Postcode</span>\n' +
    '                                        <span ng-show="SupplyChain.$submitted && SupplyChain.postcode.$error.pattern">Postcode must be number</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="element-button col-md-2">\n' +
    '                            <button class="remove" type="button" ng-click="RmvSupplierNADCAP(SupplierNADCAPs)">Rmv</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="element telephone-facsimile col-md-12">\n' +
    '                        <div class="element-content col-md-10">\n' +
    '                            <div class="element-telephone element-group col-md-12">\n' +
    '                                <label for="" class="title col-md-4">Telephone</label>\n' +
    '                                <div class="element-telephone-group col-md-6">\n' +
    '                                    <input type="text" name="phone" required maxlength="20" ng-model="SupplierDetail.Phone" ng-pattern="/^\\d+[ ]?\\d*$/">\n' +
    '                                    <div class="error">\n' +
    '                                        <span ng-show="SupplyChain.$submitted && SupplyChain.phone.$error.required">Please enter Telephone</span>\n' +
    '                                        <span ng-show="SupplyChain.$submitted && SupplyChain.phone.$error.pattern">Telephone must be number</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="element-facsimile element-group col-md-12">\n' +
    '                                <label for="" class="title col-md-4">Facsimile</label>\n' +
    '                                <div class="element-facsimile-group col-md-6">\n' +
    '                                    <input type="text" name="facsimile" required maxlength="20" ng-model="SupplierDetail.Facsimile" ng-pattern="/^\\d+[ ]?\\d*$/">\n' +
    '                                    <div class="error">\n' +
    '                                        <span ng-show="SupplyChain.$submitted && SupplyChain.facsimile.$error.required">Please enter Facsimile</span>\n' +
    '                                        <span ng-show="SupplyChain.$submitted && SupplyChain.facsimile.$error.pattern">Facsimile must be number</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="element-button col-md-2">\n' +
    '                            <button class="check-site" ng-click="Checksite()">check site</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="element website-email col-md-12">\n' +
    '                        <div class="element-content col-md-12">\n' +
    '                            <div class="element-website element-group col-md-12">\n' +
    '                                <label for="" class="title col-md-4">Website</label>\n' +
    '                                <div class="element-website-group col-md-6">\n' +
    '                                    <input type="text" name="website" ng-model="SupplierDetail.Website" maxlength="50" ng-pattern="/^www\\.[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$/">\n' +
    '                                    <div class="error">\n' +
    '                                        <span ng-show="SupplyChain.$submitted && SupplyChain.website.$error.pattern">Website incorrect formats</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="element-email element-group col-md-12">\n' +
    '                                <label for="" class="title col-md-4">Email</label>\n' +
    '                                <div class="element-email-group col-md-8">\n' +
    '                                    <div>\n' +
    '                                        <input type="text" name="email" ng-model="SupplierDetail.Email" maxlength="50">\n' +
    '                                        <div class="error">\n' +
    '                                            <span ng-show="SupplyChain.$submitted && CustomerForm.email.$error.email">Please enter a valid email</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-md-3 no-padding">\n' +
    '                    <div class="iso-table">\n' +
    '                        <div class="area-table">\n' +
    '                            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_iso">\n' +
    '                                <thead>\n' +
    '                                    <tr>\n' +
    '                                        <th>ISO Approvals</th>\n' +
    '                                        <th>Expiry</th>\n' +
    '                                    </tr>\n' +
    '                                </thead>\n' +
    '                                <tbody>\n' +
    '                                    <tr ng-repeat="item in SupplierApprovals" ng-click="selectOneItem(SupplierApprovals, item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                        <td ng-bind="item.ISOApproval"></td>\n' +
    '                                        <td ng-bind="item.Expiry | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                                    </tr>\n' +
    '                                    <tr my-enter="AddSupplierApproval(SupplierApproval)">\n' +
    '                                        <td>\n' +
    '                                            <select ng-model="SupplierApproval.ApprovalID" ng-options="option as option.Description for option in ApprovalList">\n' +
    '                                            </select>\n' +
    '                                        </td>\n' +
    '                                        <td>\n' +
    '                                            <input type="text" ng-model="SupplierApproval.Expiry">\n' +
    '                                        </td>\n' +
    '                                    </tr>\n' +
    '                                </tbody>\n' +
    '                            </table>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="nadcap-table">\n' +
    '                        <div class="area-table">\n' +
    '                            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_nadcap">\n' +
    '                                <thead>\n' +
    '                                    <tr>\n' +
    '                                        <th>NADCAP Approvals</th>\n' +
    '                                        <th>Expiry</th>\n' +
    '                                    </tr>\n' +
    '                                </thead>\n' +
    '                                <tbody>\n' +
    '                                    <tr ng-repeat="item in SupplierNADCAPs" ng-click="selectOneItem(SupplierNADCAPs, item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                        <td ng-bind="item.NADCAPApproval"></td>\n' +
    '                                        <td ng-bind="item.Expiry | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                                    </tr>\n' +
    '                                    <tr my-enter="AddSupplierNADCAP(SupplierNADCAP)">\n' +
    '                                        <td>\n' +
    '                                            <select ng-model="SupplierNADCAP.ApprovalID" ng-options="option as option.NADCAPApproval for option in NADCAPList">\n' +
    '                                            </select>\n' +
    '                                        </td>\n' +
    '                                        <td>\n' +
    '                                            <input type="text" ng-model="SupplierNADCAP.Expiry">\n' +
    '                                        </td>\n' +
    '                                    </tr>\n' +
    '                                </tbody>\n' +
    '                            </table>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="contact">\n' +
    '                        <label for="" class="contact-title col-md-8 no-padding">View Contacts and Opening hours etc.</label>\n' +
    '                        <div class="contact-group col-md-4 no-padding-right">\n' +
    '                            <button type="button" ng-click="redirectContact()">Contacts</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="supply-chain-action">\n' +
    '                <div class="col-md-5">\n' +
    '                    <div class="drop-supplier col-md-8 no-padding-left">\n' +
    '                        <button type="button" dropzone on-success="uploadSuccess" before-upload="beforeUpload" max-file-name="30">Drop Supplier Questionnaire here</button>\n' +
    '                    </div>\n' +
    '                    <div class="view-completed col-md-4 no-padding">\n' +
    '                        <button class="view-sq" type="button" ng-click="ViewSQ()">View S.Q.</button>\n' +
    '                        <div class="completed">\n' +
    '                            <label for="" class="title col-md-12 no-padding">Completed by.</label>\n' +
    '                            <div class="completed-group col-md-12 no-padding">\n' +
    '                                <input type="text" ng-model="CompleteBy" maxlength="25">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col-md-4">\n' +
    '                    <button class="new-supplier" type="button" ng-click="NewSupplier()">New Supplier</button>\n' +
    '                    <button class="order-history" type="button" ng-click="redirectOrderHistory()">Order History</button>\n' +
    '                    <button class="performance" type="button" ng-click="redirectPerformance()">Performance</button>\n' +
    '                </div>\n' +
    '                <div class="col-md-3 no-padding">\n' +
    '                    <button class="back" type="button" ng-click="redirectBack()">Back</button>\n' +
    '                    <button class="update" type="submit" ng-show="swapUpdateSave">Update</button>\n' +
    '                    <button class="update" type="submit" ng-hide="swapUpdateSave">Save</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </form>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/supply-chain/supply-contact/supply-contact.tpl.html',
    '<div class="supply-contact-page">\n' +
    '    <div class="title-global-page">Supply Chain Contact</div>\n' +
    '    <div class="contact-header">\n' +
    '        <div class="contact-element contact-id">\n' +
    '            <span>Supplier Account No.</span> <span ng-bind="SupplierID"></span>\n' +
    '        </div>\n' +
    '        <div class="contact-element contact-company col-xs-12">\n' +
    '            <div class="company col-xs-6">\n' +
    '                <span>Name </span><span ng-bind="SupplierDetail.Supplier"></span>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="contact-element contact-alias col-xs-12">\n' +
    '            <div class="alias col-xs-6">\n' +
    '                <span class="key">Alias </span><span class="value" ng-bind="SupplierDetail.Alias"></span>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="clearfix"></div>\n' +
    '    <div class="page-body">\n' +
    '        <div class="module module-1 col-xs-4">\n' +
    '            <div class="row">\n' +
    '                <div class="module-hour col-xs-12">\n' +
    '                    <div class="row">\n' +
    '                        <div class="module-title col-xs-12">\n' +
    '                            Opening hours\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="module-body col-xs-12">\n' +
    '                            <form>\n' +
    '                                <div class="row">\n' +
    '                                    <div class="area-opening-hour col-xs-12">\n' +
    '                                        <div class="row">\n' +
    '                                            <div class="area-table col-xs-10">\n' +
    '                                                <table>\n' +
    '                                                    <tr>\n' +
    '                                                        <th>Day</th>\n' +
    '                                                        <th>Open</th>\n' +
    '                                                        <th>Close</th>\n' +
    '                                                    </tr>\n' +
    '                                                    <tr ng-repeat="day in days">\n' +
    '                                                        <td ng-bind="day.name"></td>\n' +
    '                                                        <td>\n' +
    '                                                            <input type="text" ng-model="OpenHours[\'Day\' + day.key + \'Open\']" maxlength="4">\n' +
    '                                                        </td>\n' +
    '                                                        <td>\n' +
    '                                                            <input type="text" ng-model="OpenHours[\'Day\' + day.key + \'Close\']" maxlength="4">\n' +
    '                                                        </td>\n' +
    '                                                    </tr>\n' +
    '                                                </table>\n' +
    '                                            </div>\n' +
    '                                            <div class="area-button col-xs-2 no-padding">\n' +
    '                                                <button ng-click="UpdateOpenHours(OpenHours)">Update</button>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </form>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="module-additional">\n' +
    '                <div class="row">\n' +
    '                    <div class="module-body col-xs-12">\n' +
    '                        <div class="row">\n' +
    '                            <form action="">\n' +
    '                                <div class="area-table col-xs-11">\n' +
    '                                    <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_additional">\n' +
    '                                        <thead>\n' +
    '                                            <tr>\n' +
    '                                                <th>Additional</th>\n' +
    '                                                <th>Start</th>\n' +
    '                                                <th>End</th>\n' +
    '                                            </tr>\n' +
    '                                        </thead>\n' +
    '                                        <tbody>\n' +
    '                                            <tr ng-repeat="item in SupplierAdditional" ng-click="selectItem(item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                                <td ng-bind="item.Additional"></td>\n' +
    '                                                <td ng-bind="item.StartPoint"></td>\n' +
    '                                                <td ng-bind="item.EndPoint"></td>\n' +
    '                                            </tr>\n' +
    '                                            <tr my-enter="AddSupplierAdditional(Additional)">\n' +
    '                                                <td>\n' +
    '                                                    <select ng-model="Additional.AdditionalTypeID" ng-options="option as option.name for option in ListAdditionalContact"></select>\n' +
    '                                                </td>\n' +
    '                                                <td>\n' +
    '                                                    <input type="text" ng-model="Additional.StartPoint" maxlength="9">\n' +
    '                                                </td>\n' +
    '                                                <td>\n' +
    '                                                    <input type="text" ng-model="Additional.EndPoint" maxlength="9">\n' +
    '                                                </td>\n' +
    '                                            </tr>\n' +
    '                                        </tbody>\n' +
    '                                    </table>\n' +
    '                                </div>\n' +
    '                                <div class="area-button col-xs-1">\n' +
    '                                    <button type="button" class="remove-additional" ng-click="RmvAdditional(SupplierAdditional)">Rmv</button>\n' +
    '                                </div>\n' +
    '                            </form>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="module module-personnel col-xs-2">\n' +
    '            <div class="module-body">\n' +
    '                <form>\n' +
    '                    <div class="area-table">\n' +
    '                        <table>\n' +
    '                            <tr>\n' +
    '                                <th>Personnel</th>\n' +
    '                                <th>No.</th>\n' +
    '                            </tr>\n' +
    '                            <tr>\n' +
    '                                <td>Manu. (Direct)</td>\n' +
    '                                <td>\n' +
    '                                    <input type="text" ng-model="SupplierPersonnel.Type1" ng-blur="UpdatePersonnel(SupplierPersonnel)" valid-number>\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                            <tr>\n' +
    '                                <td>Manu. (Indirect)</td>\n' +
    '                                <td>\n' +
    '                                    <input type="text" ng-model="SupplierPersonnel.Type2" ng-blur="UpdatePersonnel(SupplierPersonnel)" valid-number>\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                            <tr>\n' +
    '                                <td>Quality</td>\n' +
    '                                <td>\n' +
    '                                    <input type="text" ng-model="SupplierPersonnel.Type3" ng-blur="UpdatePersonnel(SupplierPersonnel)" valid-number>\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                            <tr>\n' +
    '                                <td>Engineering</td>\n' +
    '                                <td>\n' +
    '                                    <input type="text" ng-model="SupplierPersonnel.Type4" ng-blur="UpdatePersonnel(SupplierPersonnel)" valid-number>\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                            <tr>\n' +
    '                                <td>Other</td>\n' +
    '                                <td>\n' +
    '                                    <input type="text" ng-model="SupplierPersonnel.Type5" ng-blur="UpdatePersonnel(SupplierPersonnel)" valid-number>\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                            <tr>\n' +
    '                                <td><b>Total:</b></td>\n' +
    '                                <td><b ng-bind="Total"></b></td>\n' +
    '                            </tr>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </form>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="module module-2 col-xs-6">\n' +
    '            <div class="module-contact">\n' +
    '                <div class="row">\n' +
    '                    <div class="module-title col-xs-2 bold border color-title">Contacts</div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="module-body col-xs-12">\n' +
    '                        <div class="row">\n' +
    '                            <div class="area-opening-hour col-xs-12"></div>\n' +
    '                            <div class="area-table table-contact col-xs-10 no-padding">\n' +
    '                                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_contact">\n' +
    '                                    <thead>\n' +
    '                                        <tr>\n' +
    '                                            <th>Name</th>\n' +
    '                                            <th>Phone</th>\n' +
    '                                            <th>Email</th>\n' +
    '                                            <th>Prime</th>\n' +
    '                                        </tr>\n' +
    '                                    </thead>\n' +
    '                                    <tbody>\n' +
    '                                        <tr ng-repeat="item in SupplierContacts" ng-click="selectItem(item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                            <td ng-bind="item.ContactName">\n' +
    '                                            </td>\n' +
    '                                            <td ng-bind="item.Phone"></td>\n' +
    '                                            <td ng-bind="item.Email"></td>\n' +
    '                                            <td>\n' +
    '                                                <input type="checkbox" ng-bind="item.Prime" ng-checked="item.Prime == 1">\n' +
    '                                            </td>\n' +
    '                                        </tr>\n' +
    '                                        <tr my-enter="AddSupplierContact(SupplierContact)">\n' +
    '                                            <td>\n' +
    '                                                <input type="text" ng-model="SupplierContact.ContactName" name="ContactName" maxlength="45">\n' +
    '                                            </td>\n' +
    '                                            <td>\n' +
    '                                                <input type="text" ng-model="SupplierContact.Phone" name="Phone" maxlength="13">\n' +
    '                                            </td>\n' +
    '                                            <td>\n' +
    '                                                <input type="text" ng-model="SupplierContact.Email" name="Email" maxlength="50">\n' +
    '                                            </td>\n' +
    '                                            <td>\n' +
    '                                                <input type="checkbox" ng-model="SupplierContact.Prime">\n' +
    '                                            </td>\n' +
    '                                        </tr>\n' +
    '                                    </tbody>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                            <div class="area-button col-xs-2">\n' +
    '                                <button type="button" class="remove-contact" ng-click="RmvContact(SupplierContacts)">Rmv Contact</button>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="clearfix"></div>\n' +
    '            <div class="module-freeform">\n' +
    '                <span>Freeform Notes</span>\n' +
    '                <div class="clearfix"></div>\n' +
    '                <div class="module-body">\n' +
    '                    <div class="area-table">\n' +
    '                        <table>\n' +
    '                            <tr ng-repeat="item in SupplierNotes">\n' +
    '                                <td ng-bind="item.Note"></td>\n' +
    '                            </tr>\n' +
    '                            <tr my-enter="AddNote(Note)">\n' +
    '                                <td>\n' +
    '                                    <input type="text" ng-model="Note" maxlength="100">\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                        </table>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="page-footer">\n' +
    '        <div class="col-xs-12">\n' +
    '            <button ng-click="redirectCustomer()">Back</button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/supply-chain/supply-order-history/supply-order-history.tpl.html',
    '<div class="supply-order-history-page">\n' +
    '    <div class="title-global-page">Supply Chain Order History</div>\n' +
    '    <div class="row">\n' +
    '        <div class="page-body col-xs-12">\n' +
    '            <div class="row">\n' +
    '                <div class="supply-title col-xs-2">\n' +
    '                    Wheelabrator\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="order-po-linked col-xs-12">\n' +
    '                    <div class="select-order col-xs-3">\n' +
    '                        <div class="row">\n' +
    '                            <div class="title col-xs-12">\n' +
    '                                Select an order\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="area-table col-xs-12">\n' +
    '                                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_order">\n' +
    '                                    <thead>\n' +
    '                                        <tr>\n' +
    '                                            <th>Completed</th>\n' +
    '                                            <th>PO No.</th>\n' +
    '                                            <th>Added by</th>\n' +
    '                                        </tr>\n' +
    '                                    </thead>\n' +
    '                                    <tbody>\n' +
    '                                        <tr ng-repeat="item in SupplierOrderHistory" ng-click="selectOneItem(SupplierOrderHistory, item); selectOrder(item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                            <td ng-bind="item.Completed"></td>\n' +
    '                                            <td ng-bind="item.PONo"></td>\n' +
    '                                            <td ng-bind="item.AddedBy"></td>\n' +
    '                                        </tr>\n' +
    '                                    </tbody>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="po-linked col-xs-3">\n' +
    '                        <div class="row">\n' +
    '                            <div class="title col-xs-12">\n' +
    '                                PO linked to:\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="area-table col-xs-12">\n' +
    '                                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_wono">\n' +
    '                                    <thead>\n' +
    '                                        <tr>\n' +
    '                                            <th>WO No.</th>\n' +
    '                                            <th>Part No.</th>\n' +
    '                                            <th>Qty</th>\n' +
    '                                        </tr>\n' +
    '                                    </thead>\n' +
    '                                    <tbody>\n' +
    '                                        <tr ng-repeat="item in SupplierOrderHistoryWO" ng-click="selectOneItem(SupplierOrderHistory, item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                            <td ng-bind="item.WONo"></td>\n' +
    '                                            <td ng-bind="item.PartNo"></td>\n' +
    '                                            <td ng-bind="item.Qty"></td>\n' +
    '                                        </tr>\n' +
    '                                    </tbody>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="button col-xs-12">\n' +
    '                    <button ng-click="redirectBack()">Back</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/supply-chain/supply-performance/supply-performance.tpl.html',
    '<div class="supplier-performance">\n' +
    '    <div class="container">\n' +
    '        <div class="row">\n' +
    '            <div class="header col-xs-8">\n' +
    '                <div class="display">Display : </div>\n' +
    '                <div ng-repeat="item in sortHeader" class="{{item.class}}" ng-class="{\'active\': item.selected}" ng-bind="item.title" ng-click="selectOneItem(sortHeader, item, \'selected\'); chooseSortHeader(item)"></div>\n' +
    '            </div>\n' +
    '            <div class="content col-xs-12">\n' +
    '                <div class="row">\n' +
    '                    <div class="supplier col-xs-12">\n' +
    '                        <div class="col-xs-1">\n' +
    '                            <span class="up-control" ng-click="reverse = false"></span>\n' +
    '                            <span class="down-control" ng-click="reverse = true"></span>\n' +
    '                        </div>\n' +
    '                        <div class="area-table col-xs-11">\n' +
    '                            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_performance" load-item="SupplierPerformance">\n' +
    '                                <thead>\n' +
    '                                    <tr>\n' +
    '                                        <th ng-repeat="item in tableSupplier" ng-bind="item.title" ng-click="selectOneItem(tableSupplier, item, \'selected\'); changeSortBy(item)" ng-class="{\'th-active\': item.selected}"></th>\n' +
    '                                    </tr>\n' +
    '                                </thead>\n' +
    '                                <tbody>\n' +
    '                                    <tr ng-repeat="item in SupplierPerformance | orderBy: sortSupplier : reverse" ng-click="selectOneItem(SupplierPerformance, item, \'selected\')" ng-class="{\'tr-active\':item.selected}">\n' +
    '                                        <td ng-bind="item.Alias"></td>\n' +
    '                                        <td ng-bind="item.TotalOrders"></td>\n' +
    '                                        <td ng-bind="item.Outstanding"></td>\n' +
    '                                        <td ng-bind="item.Delivered"></td>\n' +
    '                                        <td ng-bind="item.Cancelled"></td>\n' +
    '                                        <td ng-bind="item.AvLeadDays"></td>\n' +
    '                                        <td ng-bind="item.ActualDays"></td>\n' +
    '                                        <td ng-bind="item.EarlyLate"></td>\n' +
    '                                        <td ng-bind="item.Early" ng-class="{\'bg-green-cl-black\' : item.Early > 0}"></td>\n' +
    '                                        <td ng-class="{\'bg-darkgreen-cl-white\' : item.OnTime > 0}">{{item.OnTime}}%</td>\n' +
    '                                        <td ng-bind="item.Late" ng-class="{\'bg-red-cl-white\' : item.Late > 0}"></td>\n' +
    '                                        <td ng-bind="item.Rejects" ng-class="{\'bg-red-cl-white\' : item.Rejects > 0}"></td>\n' +
    '                                        <td ng-bind="item.concessions"></td>\n' +
    '                                        <td ng-class="{\'bg-darkgreen-cl-white\' : item.QRating == 100, \'bg-green-cl-black\' : item.QRating >= 95 && item.QRating <= 99, \'bg-red-cl-white\' : item.QRating < 95}">{{item.QRating}}%</td>\n' +
    '                                        <td ng-class="{\'bg-darkgreen-cl-white\' : item.VRating >= 95, \'bg-yellow-cl-black\' : item.VRating >= 65 && item.VRating <= 95, \'bg-green-cl-black\' : item.VRating >= 33 && item.VRating < 65, \'bg-red-cl-white\' : item.VRating < 33}">{{item.VRating}}%</td>\n' +
    '                                    </tr>\n' +
    '                                </tbody>\n' +
    '                            </table>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="performance col-xs-5 col-xs-offset-1">\n' +
    '                            <div class="area-table">\n' +
    '                                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_total">\n' +
    '                                    <thead>\n' +
    '                                        <tr>\n' +
    '                                            <th>Performance</th>\n' +
    '                                            <th>Material</th>\n' +
    '                                            <th>Treatments</th>\n' +
    '                                            <th>Miscellaneous</th>\n' +
    '                                        </tr>\n' +
    '                                    </thead>\n' +
    '                                    <tbody>\n' +
    '                                        <tr ng-repeat="item in SupplierPerformanceTotals">\n' +
    '                                            <td ng-bind="item.PerformanceGroup" ng-class="{\'bg-darkgreen-cl-white\' : item.PerformanceGroup == \'1st class\', \'bg-yellow-cl-black\' : item.PerformanceGroup == \'Standard\', \'bg-orange-cl-black\' : item.PerformanceGroup == \'Sub standard\', \'bg-red-cl-white\' : item.PerformanceGroup == \'Unacceptable\'}"></td>\n' +
    '                                            <td ng-class="{\'bg-lightgreen-cl-black\' : item.PerformanceGroup == \'1st class\', \'\' : item.PerformanceGroup == \'Standard\', \'bg-orange-cl-black\' : item.PerformanceGroup == \'Sub standard\', \'bg-pink-cl-black\' : item.PerformanceGroup == \'Unacceptable\', \'bg-blue-cl-black\' : item.PerformanceGroup == \'Totals\'}">{{item.Material}}%</td>\n' +
    '                                            <td ng-class="{\'bg-lightgreen-cl-black\' : item.PerformanceGroup == \'1st class\', \'\' : item.PerformanceGroup == \'Standard\', \'bg-orange-cl-black\' : item.PerformanceGroup == \'Sub standard\', \'bg-pink-cl-black\' : item.PerformanceGroup == \'Unacceptable\', \'bg-violet-cl-black\' : item.PerformanceGroup == \'Totals\'}">{{item.Treatments}}%</td>\n' +
    '                                            <td ng-class="{\'bg-lightgreen-cl-black\' : item.PerformanceGroup == \'1st class\', \'\' : item.PerformanceGroup == \'Standard\', \'bg-orange-cl-black\' : item.PerformanceGroup == \'Sub standard\' || item.PerformanceGroup == \'Totals\', \'bg-pink-cl-black\' : item.PerformanceGroup == \'Unacceptable\'}">{{item.Miscellaneous}}%</td>\n' +
    '                                        </tr>\n' +
    '                                    </tbody>\n' +
    '                                </table>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="left company-wide col-xs-2 col-xs-offset-2">\n' +
    '                            <p class="content-right">Company-wide</p>\n' +
    '                            <input class="pers full-width" type="text" ng-model="CompanyWide">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <input class="back" type="button" value="Back" ng-click="redirectBack()">\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/workorders/cocs/cocs.tpl.html',
    '<div id="tab-cocs" class="tab_content">\n' +
    '    <div class="content-header">\n' +
    '        <div class="col-sm-1">\n' +
    '            <label>\n' +
    '                <p>Invoice</p>\n' +
    '                <input name="credit" ng-model="Credit" value="0" type="radio" id="radio-invoice" ng-model="showCredit" ng-checked="true" checked="checked" />\n' +
    '            </label>\n' +
    '            <label>\n' +
    '                <p>Credit</p>\n' +
    '                <input name="credit" ng-model="Credit" value="1" type="radio" id="radio-credit" ng-model="showCredit" />\n' +
    '            </label>\n' +
    '        </div>\n' +
    '        <div class="col-sm-3 input-invoice">\n' +
    '            <div>\n' +
    '                <p> </p>\n' +
    '                <input type="text" class="input-quote date" value="{{nowDate}}" ng-dblclick="getNowDate()">\n' +
    '            </div>\n' +
    '            <div>\n' +
    '                <label>Invoice</label>\n' +
    '                <input type="text" ng-model="NextOutvoiceNo" class="input-quote text">\n' +
    '                <p class="quantity-title">Quantity</p>\n' +
    '                <input type="text" class="quantity">\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-sm-4 no-padding">\n' +
    '            <form>\n' +
    '                <div class="area-table">\n' +
    '                    <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_grb" class="grb-table">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th>GRB</th>\n' +
    '                                <th>Incoming release</th>\n' +
    '                                <th>Display?</th>\n' +
    '                                <th>Print</th>\n' +
    '                                <th>Update</th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="item in OrderCoCInfo" ng-class="{\'tr-active\': item.selected}">\n' +
    '                                <td ng-click="selectItem(item, \'selected\')">\n' +
    '                                    <span ng-bind="item.Deviation"></span>\n' +
    '                                    <span>{{item.GRB}}</span>\n' +
    '                                </td>\n' +
    '                                <td>\n' +
    '                                    <input type="text" value="{{item.IRNoteNumber}}" readonly>\n' +
    '                                </td>\n' +
    '                                <td>\n' +
    '                                    <input type="checkbox" ng-model="item.Display" ng-checked="item.Display">\n' +
    '                                </td>\n' +
    '                                <td>\n' +
    '                                    <input type="checkbox" ng-model="item.Print" ng-checked="item.Print == 1" ng-checked="item.Print">\n' +
    '                                </td>\n' +
    '                                <td>\n' +
    '                                    <button ng-click="updateIRList(item)">Update</button>\n' +
    '                                </td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '        <div class="col-sm-4 submit">\n' +
    '            <p>\n' +
    '                CoC Pagerwork complete\n' +
    '                <input type="checkbox" ng-model="PaperworkComplete" ng-click="updateWOCoCInfo(PaperworkComplete)" />\n' +
    '            </p>\n' +
    '            <p>\n' +
    '                <button type="button" class="copy-address float-right" ng-click="PrintCoC(OrderCoCInfo)">Print</button>\n' +
    '                <span>Print selected docs</span>\n' +
    '            </p>\n' +
    '            <p>\n' +
    '                <button type="button" ng-click="CreateWOOutvoice(NextOutvoiceNo)">Save</button>\n' +
    '            </p>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="content-main">\n' +
    '        <div class="col-sm-3">\n' +
    '            <div ng-show="Credit == 1" class="box-credit col-xs-12">\n' +
    '                <span class="credit-title">Credit</span>\n' +
    '                <p class="title">\n' +
    '                    Return Note:\n' +
    '                </p>\n' +
    '                <input class="number" type="text" ng-model="NextCreditNote" readonly />\n' +
    '                <div class="col-sm-12 select">\n' +
    '                    Invoice\n' +
    '                    <select ng-model="OutvoiceCredit.InvoiceNo" ng-options="option as option.InvoiceNo for option in WOOutvoiceList">\n' +
    '                    </select>\n' +
    '                </div>\n' +
    '                <div class="col-sm-6 no-padding">\n' +
    '                    coC Reqd?\n' +
    '                    <input type="checkbox" ng-model="OutvoiceCredit.CoCReqd" />\n' +
    '                </div>\n' +
    '                <div class="col-sm-6 quantity no-padding">\n' +
    '                    Quantity\n' +
    '                    <input type="text" ng-model="OutvoiceCredit.Quantity" />\n' +
    '                </div>\n' +
    '                <div class="col-sm-12 identification">\n' +
    '                    <p>Identification marks</p>\n' +
    '                    <input type="text" ng-model="OutvoiceCredit.IdentMarks" maxlength="20" />\n' +
    '                </div>\n' +
    '                <div class="col-sm-12">\n' +
    '                    <button type="button" class="copy-address float-right" ng-click="CreateOutvoiceCredit(OutvoiceCredit)">Save</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-sm-offset-1 col-sm-8 list-text">\n' +
    '            <div class="area-table material-notes">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_material">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th class="col-xs-12 no-padding">Material Notes</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in WOMaterialNotes">\n' +
    '                            <td class="col-xs-12 no-padding">\n' +
    '                                <input type="text" ng-model="item.MaterialNote">\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '            <div class="area-table conformity-notes">\n' +
    '                <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_conformity">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th class="col-xs-12 no-padding">Conformity Notes</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in WOConformityNotes">\n' +
    '                            <td class="col-xs-12 no-padding">\n' +
    '                                <input type="text" ng-model="item.ConformityNote">\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/workorders/costing/costing.tpl.html',
    '<div id="tab-costings" class="tab_content">\n' +
    '    <div class="col-sm-2 box-total">\n' +
    '        <p>\n' +
    '            <span class="name">Materials</span>\n' +
    '            <span class="text content-right">₤{{WorkOrderLabour[0].MaterialCost}}</span>\n' +
    '        </p>\n' +
    '        <p>\n' +
    '            <span class="name">Treatments</span>\n' +
    '            <span class="text content-right">₤{{TotalTreatments}}</span>\n' +
    '        </p>\n' +
    '        <p>\n' +
    '            <span class="name">Labour</span>\n' +
    '            <span class="text content-right">₤{{WOCost}}</span>\n' +
    '        </p>\n' +
    '        <p class="total">\n' +
    '            <span class="name">Total p/part</span>\n' +
    '            <span class="text">₤{{PerPartCost}}</span>\n' +
    '        </p>\n' +
    '    </div>\n' +
    '    <div class="col-sm-5">\n' +
    '        <div class="tabs-content table-operation">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_operation">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th class="col-xs-6 no-padding">Operation</th>\n' +
    '                        <th class="col-xs-3 no-padding">Set</th>\n' +
    '                        <th class="col-xs-3 no-padding">Run</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in WorkOrderLabour">\n' +
    '                        <td class="col-xs-6 no-padding" ng-bind="item.Operation"></td>\n' +
    '                        <td class="col-xs-3 no-padding" ng-bind="item.SetTime"></td>\n' +
    '                        <td class="col-xs-3 no-padding" ng-bind="item.RunTime"></td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '        <div class="tabs-content table-part-totals">\n' +
    '            <table>\n' +
    '                <tbody>\n' +
    '                    <tr class="total">\n' +
    '                        <td>Part Totals:</td>\n' +
    '                        <td ng-bind="PartTotalSet"></td>\n' +
    '                        <td ng-bind="PartTotalRun"></td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col-sm-5">\n' +
    '        <div class="table-right tabs-content table-time">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_time">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th class="col-xs-4 no-padding">Time</th>\n' +
    '                        <th class="col-xs-4 no-padding">Cost</th>\n' +
    '                        <th class="col-xs-4 no-padding">Qty</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in WorkOrderLabour">\n' +
    '                        <td class="col-xs-4 no-padding" ng-bind="item.DisplayTimeTaken"></td>\n' +
    '                        <td class="col-xs-4 no-padding content-right">₤{{item.Cost}}</td>\n' +
    '                        <td class="col-xs-4 no-padding content-right" ng-bind="item.Qty"></td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '        <div class="table-right tabs-content table-grand-total">\n' +
    '            <table class="grand-total">\n' +
    '                <tr>\n' +
    '                    <td>Grand Totals:</td>\n' +
    '                    <td ng-bind="GrandTime"></td>\n' +
    '                    <td class="content-right">₤{{GrandCost}}</td>\n' +
    '                    <td ng-bind="GrandQty"></td>\n' +
    '                </tr>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '        <div class="table-right tabs-content table-per-part">\n' +
    '            <table class="per-part">\n' +
    '                <tr>\n' +
    '                    <td>per part:</td>\n' +
    '                    <td ng-bind="PerPartTime"></td>\n' +
    '                    <td>₤{{PerPartCost}}</td>\n' +
    '                </tr>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '        <div class="table-right tabs-content table-wo-totals">\n' +
    '            <table class="wo-totals">\n' +
    '                <tr>\n' +
    '                    <td>WO Totals:</td>\n' +
    '                    <td ng-bind="WOTime"></td>\n' +
    '                    <td class="content-right">₤{{WOCost}}</td>\n' +
    '                    <td ng-bind="WOQty"></td>\n' +
    '                </tr>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/workorders/document/document.tpl.html',
    '<div id="tab-documents" class="tab_content">\n' +
    '    <div class="row">\n' +
    '        <div class="col-sm-offset-2 col-sm-4 dcm-table">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_desc">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th>Description</th>\n' +
    '                        <th>Print</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody class="floatThead-wrapper">\n' +
    '                    <tr ng-repeat="item in WODocuments" ng-click="selectOneItem(WODocuments, item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                        <td ng-bind="item.description"></td>\n' +
    '                        <td>\n' +
    '                            <input type="checkbox" ng-bind="item.Print" ng-checked="item.Print == 1" />\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '        <div class="col-sm-2 dcm-select">\n' +
    '            Select Document Types\n' +
    '            <select ng-model="DocumentDropType" ng-options="option as option.DropType for option in DocumentDropTypes">\n' +
    '            </select>\n' +
    '        </div>\n' +
    '        <div class="col-sm-3 dcm-drop">\n' +
    '            <button type="button" dropzone on-success="uploadSuccess" before-upload="beforeUpload" max-file-name="30">Drop Documents here</button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row">\n' +
    '        <div class="col-sm-offset-6 col-sm-3 dcm-button">\n' +
    '            <span>Print selected docs</span>\n' +
    '            <button type="button" ng-click="printImage()" class="copy-address float-right">Print</button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/workorders/invoice/invoice.tpl.html',
    '<div id="tab-invoices" class="tab_content">\n' +
    '    <div class="col-sm-8 content-height-left">\n' +
    '        <div class="area-table table-main">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_date">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th class="col-xs-2 no-padding">Date</th>\n' +
    '                        <th class="col-xs-1 no-padding">Number</th>\n' +
    '                        <th class="col-xs-1 no-padding">Quantity</th>\n' +
    '                        <th class="col-xs-2 no-padding">Price Each</th>\n' +
    '                        <th class="col-xs-1 no-padding">Del. Charge</th>\n' +
    '                        <th class="col-xs-1 no-padding">Total Price</th>\n' +
    '                        <th class="col-xs-2 no-padding">Certificate No.</th>\n' +
    '                        <th class="col-xs-2 no-padding">WO Complete</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in WOOutvoices" ng-click="countValueGrantTotal(WOOutvoices, false)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                        <td class="col-xs-2 no-padding" ng-click="selectOneItem(WOOutvoices, item)" align="center" ng-bind="item.CreateDate | timeFormat:\'DD-MM-YYYY\'"></td>\n' +
    '                        <td class="col-xs-1 no-padding">\n' +
    '                            <a href="javascript:;" ng-bind="item.InvoiceNo" ng-click="redirectFinance(item.InvoiceNo)"></a>\n' +
    '                        </td>\n' +
    '                        <td class="col-xs-1 no-padding" align="right" ng-bind="item.Qty"></td>\n' +
    '                        <td class="col-xs-2 no-padding" align="right">₤{{item.EachPrice}}</td>\n' +
    '                        <td class="col-xs-1 no-padding" align="center">\n' +
    '                            <input type="text" maxlength="6" ng-model="item.DeliveryChg" ng-change="UpdateWOOutvoice(WOOutvoices, ValueExVAT)">\n' +
    '                        </td>\n' +
    '                        <td class="col-xs-1 no-padding" align="right">₤{{item.TotalPrice}}</td>\n' +
    '                        <td class="col-xs-2 no-padding" align="center" ng-bind="item.CertificateNo"></td>\n' +
    '                        <td class="col-xs-2 no-padding" align="center">\n' +
    '                            <input type="checkbox" ng-model="item.Complete" ng-true-value="1" ng-false-value="0" ng-change="UpdateWOOutvoice(WOOutvoices, ValueExVAT)">\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="content-height-right col-sm-4">\n' +
    '        <p class="title">Invoice details</p>\n' +
    '        <div class="col-sm-7 prince">\n' +
    '            <p>\n' +
    '                Value ex VAT\n' +
    '                <input type="text" maxlength="8" ng-model="ValueExVAT" ng-change="UpdateWOOutvoice(WOOutvoices, ValueExVAT)" />\n' +
    '            </p>\n' +
    '            <p>\n' +
    '                Grand total\n' +
    '                <input type="text" ng-model="grantTotal" />\n' +
    '            </p>\n' +
    '        </div>\n' +
    '        <div class="col-sm-offset-1 col-sm-4 button">\n' +
    '            <button type="button" ng-click="viewTop()" class="copy-address float-right">View</button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row"></div>\n' +
    '    <div class="content-button left col-sm-4">\n' +
    '        <p class="title-main">WOs to invoice</p>\n' +
    '        <div class="area-table table-wo">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_wono">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th class="col-xs-3 no-padding">WO No.</th>\n' +
    '                        <th class="col-xs-4 no-padding">Customer</th>\n' +
    '                        <th class="col-xs-1 no-padding">Qty</th>\n' +
    '                        <th class="col-xs-4 no-padding">Created</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in IncompleteInvoicedWOs" ng-click="selectOneItem(IncompleteInvoicedWOs, item); selectInvoicedWOs(item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                        <td class="col-xs-3 no-padding" ng-bind="item.WONo"></td>\n' +
    '                        <td class="col-xs-4 no-padding" ng-bind="item.CustomerName"></td>\n' +
    '                        <td class="col-xs-1 no-padding" ng-bind="item.Qty"></td>\n' +
    '                        <td class="col-xs-4 no-padding" ng-bind="item.Created | timeFormat:\'DD-MM-YYYY\'"></td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="content-button center  col-sm-2">\n' +
    '        <div class="center-child">\n' +
    '            <p class="title">Qty left to invoice</p>\n' +
    '            <input type="text" class="left" ng-model="QtyWOInvoice" />\n' +
    '            <hr class="left" />\n' +
    '            <input type="text" class="right" ng-model="QtyWOOutvoice" />\n' +
    '            <hr class="right" />\n' +
    '            <button ng-click="CreateInvoice(IncompleteInvoicedWOs, QtyWOOutvoice)" class="copy-address float-right">Create invoice</button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="content-button right col-sm-3">\n' +
    '        <p class="title-main">Raised invoices</p>\n' +
    '        <div class="area-table table-raised-invoice">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_raised">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th class="col-xs-4 no-padding">Number</th>\n' +
    '                        <th class="col-xs-3 no-padding">Qty</th>\n' +
    '                        <th class="col-xs-5 no-padding">Created</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in WORaisedOutvoices" ng-click="selectOneItem(WORaisedOutvoices, item, \'selected\')" ng-class="{\'tr-active\': item.selected}">\n' +
    '                        <td class="col-xs-4 no-padding" ng-bind="item.InvoiceNo"></td>\n' +
    '                        <td class="col-xs-3 no-padding" ng-bind="item.Qty"></td>\n' +
    '                        <td class="col-xs-5 no-padding" ng-bind="item.CreateDate | timeFormat:\'DD-MM-YYYY\'"></td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="content-button button col-sm-1">\n' +
    '        <button type="button" ng-click="openBIRT()" class="copy-address float-right">View</button>\n' +
    '        <button type="button" ng-click="markSent()" class="copy-address float-right">Mark as Sent</button>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/workorders/main/main.tpl.html',
    '<div id="tab-main" class="tab_content">\n' +
    '    <div class="col-xs-2 submit-button">\n' +
    '        <div class="col-xs-12">\n' +
    '            <button type="button" class="viewco" ng-click="redirectCOpage(WorkOrderDetail.OrderID, WorkOrderDetail.CustomerOrderRef)">View CO</button>\n' +
    '        </div>\n' +
    '        <div class="col-xs-12">\n' +
    '            <button type="button" class="engineering" ng-click="redirectEngineering()">Engineering</button>\n' +
    '        </div>\n' +
    '        <div class="col-xs-12">\n' +
    '            <button type="button" class="part" ng-click="redirectPartControl(WorkOrderDetail.pkPartTemplateID)">Part</button>\n' +
    '        </div>\n' +
    '        <div class="col-xs-12">\n' +
    '            <button type="button" class="schedule" ng-click="redirectPSpage()">Schedule</button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col-xs-4 information-orders">\n' +
    '        <div class="form-group">\n' +
    '            <label class="col-xs-4">Part No.</label>\n' +
    '            <a href="javascript:;" ng-bind="WorkOrderDetail.PartNumber" ng-click="redirectPartControl(WorkOrderDetail.pkPartTemplateID)" class="col-xs-8 no-border"></a>\n' +
    '        </div>\n' +
    '        <div class="form-group">\n' +
    '            <label class="col-xs-4">Order No.</label>\n' +
    '            <div class="col-xs-8 blue">\n' +
    '                <a href="javascript:;" ng-bind="WorkOrderDetail.CustomerOrderRef" ng-click="redirectCOpage(WorkOrderDetail.CustomerOrderRef)"></a>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="form-group">\n' +
    '            <label class="col-xs-4">Quantity</label>\n' +
    '            <div class="col-xs-8">\n' +
    '                <input type="text" class="input-quote" ng-model="WorkOrderDetail.Quantity" ng-disabled="!WorkOrderDetail">\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="form-group">\n' +
    '            <label class="col-xs-4">Due</label>\n' +
    '            <div class="col-xs-8">\n' +
    '                <input type="text" class="input-quote" ng-model="WorkOrderDetail.DueDate" ng-disabled="!WorkOrderDetail">\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="form-group">\n' +
    '            <label class="col-xs-4">Est. Finish Date</label>\n' +
    '            <div class="col-xs-8">\n' +
    '                <input type="text" class="input-quote" ng-model="WorkOrderDetail.EstDate" ng-disabled="!WorkOrderDetail">\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="form-group">\n' +
    '            <label class="col-xs-4">Where</label>\n' +
    '            <div class="col-xs-8 blue">\n' +
    '                <a href="javascript:;" ng-bind="WorkOrderDetail.WhereMachine" ng-click="redirectMachineList(WorkOrderDetail.WhereMachine)"></a>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="form-group">\n' +
    '            <label class="col-xs-4">Issue</label>\n' +
    '            <div class="col-xs-8">\n' +
    '                <input type="text" class="input-quote" ng-model="WorkOrderDetail.Issue" ng-disabled="!WorkOrderDetail">\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col-xs-6 modules-list-text">\n' +
    '        <div class="area-table">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_notes">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th class="col-xs-12 no-padding">Notes</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in WorkOrderNotes">\n' +
    '                        <td class="col-xs-12 no-padding">\n' +
    '                            <span ng-bind="item.Note"></span>\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                    <tr>\n' +
    '                        <td class="col-xs-12 no-padding">\n' +
    '                            <input type="text" ng-model="OrderNotes" maxlength="80" my-enter="AddOrderNotes(OrderNotes)" ng-disabled="!WorkOrderDetail">\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col-xs-offset-6 col-xs-6 close-cancal">\n' +
    '        <div class="col-xs-6 no-padding">\n' +
    '            <label>Close / Cancal this work order</label>\n' +
    '            <p>This cannot be reversed</p>\n' +
    '        </div>\n' +
    '        <div class="col-xs-6">\n' +
    '            <input type="radio" name="CloseCancal" ng-click="WOCancel()" />\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/workorders/material-issue/material-issue.tpl.html',
    '<div id="tab-materialIssue" class="tab_content">\n' +
    '    <div class="option-search">\n' +
    '        <p class="title-top">\n' +
    '            Search:\n' +
    '        </p>\n' +
    '        <div class="col-xs-offset-2 col-xs-1">\n' +
    '            <p class="title">\n' +
    '                Required:.\n' +
    '            </p>\n' +
    '        </div>\n' +
    '        <div class="col-xs-1 src-description input">\n' +
    '            <p>\n' +
    '                Description\n' +
    '            </p>\n' +
    '            <input type="text" ng-model="WOMaterialDetail.Description" />\n' +
    '        </div>\n' +
    '        <div class="col-xs-1 src-size input">\n' +
    '            <p>\n' +
    '                Size\n' +
    '            </p>\n' +
    '            <input type="text" ng-model="WOMaterialDetail.Size" />\n' +
    '        </div>\n' +
    '        <div class="col-xs-1 src-units input">\n' +
    '            <p>\n' +
    '                Units\n' +
    '            </p>\n' +
    '            <select ng-model="WOMaterialDetail.UnitID" ng-options="option as option.name for option in Units">\n' +
    '            </select>\n' +
    '        </div>\n' +
    '        <div class="col-xs-1 src-shape select">\n' +
    '            <p>\n' +
    '                Shape\n' +
    '            </p>\n' +
    '            <select ng-model="WOMaterialDetail.fkShapeID" ng-options="option as option.Description for option in MaterialShapes">\n' +
    '            </select>\n' +
    '        </div>\n' +
    '        <div class="col-xs-1 src-partlength input">\n' +
    '            <p>\n' +
    '                Part length\n' +
    '            </p>\n' +
    '            <input type="text" ng-model="WOMaterialDetail.PartLength" />\n' +
    '            <span>mm</span>\n' +
    '        </div>\n' +
    '        <div class="col-xs-2 src-approval select">\n' +
    '            <p>\n' +
    '                Approval\n' +
    '            </p>\n' +
    '            <select ng-model="WOMaterialDetail.ApprovalID" ng-options="option as option.Description for option in AllApprovalList">\n' +
    '            </select>\n' +
    '        </div>\n' +
    '        <div class="col-xs-2 src-sup input select">\n' +
    '            <p>\n' +
    '                SPU\n' +
    '            </p>\n' +
    '            <input type="text" ng-model="WOMaterialDetail.SPUNo" />\n' +
    '            <select ng-model="WOMaterialDetail.SPUID" ng-options="option as option.Description for option in UOMList"></select>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col-xs-12">\n' +
    '    </div>\n' +
    '    <div class="content-material col-xs-2">\n' +
    '        <div class="col-xs-6 text-right no-padding">\n' +
    '            <p>GRB No.</p>\n' +
    '            <p>Material</p>\n' +
    '            <p>Size</p>\n' +
    '        </div>\n' +
    '        <div class="col-xs-6 text-left">\n' +
    '            <p>\n' +
    '                <input class="grp" type="text" ng-model="GRB" ng-change="SearchMaterial(GRB, Spec, Size)" />\n' +
    '            </p>\n' +
    '            <p>\n' +
    '                <input class="material" type="text" maxlength="6" ng-model="Spec" ng-change="SearchMaterial(GRB, Spec, Size)" />\n' +
    '            </p>\n' +
    '            <p>\n' +
    '                <input class="size" type="text" ng-model="Size" ng-change="SearchMaterial(GRB, Spec, Size)" />\n' +
    '            </p>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="content-table col-xs-7 no-padding-right">\n' +
    '        <div class="table-control">\n' +
    '            <span class="up-control" ng-click="reverse = false"></span>\n' +
    '            <span>sort</span>\n' +
    '            <span class="down-control" ng-click="reverse = true"></span>\n' +
    '        </div>\n' +
    '        <div class="area-table table-main">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_grb" load-item="MaterialInStock">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th ng-repeat="item in sortTable" ng-bind="item.title" ng-click="selectOneItem(sortTable, item, \'selected\'); changeSortBy(item)" ng-class="{\'th-active\': item.selected}"></th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in MaterialInStock | orderBy : sortBy : reverse" ng-click="selectOneItem(MaterialInStock, item)" ng-class="{\'tr-active\': item.selected}">\n' +
    '                        <td class="col-xs-1 no-padding" ng-bind="item.GRBNo"></td>\n' +
    '                        <td class="col-xs-1 no-padding" ng-bind="item.Spec"></td>\n' +
    '                        <td class="col-xs-1 no-padding" ng-bind="item.Size"></td>\n' +
    '                        <td class="col-xs-1 no-padding" ng-bind="item.Units"></td>\n' +
    '                        <td class="col-xs-1 no-padding" ng-bind="item.Shape"></td>\n' +
    '                        <td class="col-xs-1 no-padding" ng-bind="item.ReleaseLevel"></td>\n' +
    '                        <td class="col-xs-1 no-padding" ng-bind="item.PurchaseLength"></td>\n' +
    '                        <td class="col-xs-2 no-padding" ng-bind="item.DateIn | timeFormat: \'DD/MM/YYYY\'"></td>\n' +
    '                        <td class="col-xs-1 no-padding" ng-bind="item.OrderQty"></td>\n' +
    '                        <td class="col-xs-1 no-padding" ng-bind="item.InStock"></td>\n' +
    '                        <td class="col-xs-1 no-padding" ng-bind="item.Location"></td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="content-button col-xs-3">\n' +
    '        <button ng-click="ReserveMaterial(MaterialInStock, QtyReserve)" class="copy-address float-right">Reserve</button>\n' +
    '        <div class="col-xs-12 space"></div>\n' +
    '        <div class="text-right col-xs-3">Qty to reserve</div>\n' +
    '        <div class="text-left col-xs-9">\n' +
    '            <input type="text" class="text-right" ng-model="QtyReserve" />\n' +
    '        </div>\n' +
    '        <div class="col-xs-12" ng-show="checkQty">\n' +
    '            <p class="warning">This falls shoft off the required qty. Please create Another WO for the shoftfall</p>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col-xs-offset-4 col-xs-5 text-description">\n' +
    '        <p>Description<span ng-bind="WOMaterialDetail.Description"></span></p>\n' +
    '        <p>Designated Machine <span ng-bind="WOMaterialDetail.Machine"></span></p>\n' +
    '    </div>\n' +
    '    <div class="col-xs-3 button-description">\n' +
    '        <button ng-click="IssueMaterialForWO()">Issue</button>\n' +
    '    </div>\n' +
    '    <div class="col-xs-12 box-bottom">\n' +
    '        <div class="col-xs-6 bottom-table">\n' +
    '            <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="thead_notes">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th>Notes</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="item in IntMaterialNotes">\n' +
    '                        <td ng-bind="item.Note"></td>\n' +
    '                    </tr>\n' +
    '                    <tr my-enter="AddMaterialNote(MaterialNote)">\n' +
    '                        <td>\n' +
    '                            <input type="text" maxlength="80" ng-model="MaterialNote" />\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '        <div class="col-xs-offset-1 col-xs-4 bottom-text">\n' +
    '            <p ng-show="isStore">Store will be informed to being cuting / issuing the selected material, one the \'issue\' button is clicked</p>\n' +
    '        </div>\n' +
    '        <div class="col-xs-1 bottom-button">\n' +
    '            <button type="submit" ng-click="redirectPurchaseOrderPage()">Add PO</button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/workorders/pos/pos.tpl.html',
    '<div id="tab-pos" class="tab_content">\n' +
    '    <p>\n' +
    '        <span>RFQ</span> >\n' +
    '        <span>PO</span> >\n' +
    '        <span>GRP</span> >\n' +
    '        <span>Invoice list</span>\n' +
    '    </p>\n' +
    '    <div class="area-table">\n' +
    '        <table andy-float-thead="floatTheadOptions" change-ui-router="changeUIRouter" ng-model="grb">\n' +
    '            <thead>\n' +
    '                <tr>\n' +
    '                    <td>GRB number</td>\n' +
    '                    <td>PO number</td>\n' +
    '                    <td>RFQ number</td>\n' +
    '                    <td>invoice NO</td>\n' +
    '                    <td>OP number</td>\n' +
    '                    <td>OP type</td>\n' +
    '                    <td>Supplier</td>\n' +
    '                </tr>\n' +
    '            </thead>\n' +
    '            <tbody>\n' +
    '                <tr ng-repeat="item in WorkOrderGRBs">\n' +
    '                    <td>\n' +
    '                        <a href="javascript:;" ng-click="redirectGoodsIn(item.GRB)" ng-bind="item.GRB"></a>\n' +
    '                    </td>\n' +
    '                    <td>\n' +
    '                        <a href="javascript:;" ng-click="redirectPurchaseOrder(item.PO)" ng-bind="item.PO"></a>\n' +
    '                    </td>\n' +
    '                    <td ng-bind="item.RFQ"></td>\n' +
    '                    <td ng-bind="item.Invoice"></td>\n' +
    '                    <td ng-bind="item.OpNumber"></td>\n' +
    '                    <td ng-bind="item.OpType"></td>\n' +
    '                    <td ng-bind="item.Supplier"></td>\n' +
    '                </tr>\n' +
    '            </tbody>\n' +
    '        </table>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/department/workorders/route-card/route-card.tpl.html',
    '<div id="tab-routeCard" class="tab_content">\n' +
    '    <div ng-bind-html="birtRouteCard | css"></div>\n' +
    '</div>\n' +
    '');
}]);
})();
