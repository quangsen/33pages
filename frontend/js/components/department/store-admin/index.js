(function(app) {

    app.controller('StoreAdminCtrl', StoreAdminCtrl);
    StoreAdminCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function StoreAdminCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {
        var StoreAreaID = $stateParams.StoreAreaID;

        API.procedure.get('ProcGetStoreAreaList')
            .then(function(result) {
                $scope.StoreAreaList = result;

                if (StoreAreaID !== undefined) {
                    var selectedStoreArea = _.find($scope.StoreAreaList, function(value) {
                        return parseInt(value.StoreAreaID) === parseInt(StoreAreaID);
                    });
                    selectedStoreArea.selected = true;
                }
            })
            .catch(function(error) {
                throw error;
            });

        $scope.selectStoreArea = function(item) {
            var state = 'department.store-admin';
            var params = {
                StoreAreaID: item.StoreAreaID
            };
            $rootScope.redirectDept(state, params);
        };

        if (StoreAreaID !== undefined) {

            /**
             * get X Store Locations
             */
            API.procedure.post('ProcGetXStoreLocations', {
                    StoreAreaID: StoreAreaID
                })
                .then(function(result) {
                    $scope.XStoreLocations = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Y Store Locations
             */
            API.procedure.post('ProcGetYStoreLocations', {
                    StoreAreaID: StoreAreaID
                })
                .then(function(result) {
                    $scope.YStoreLocations = result;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        /**
         * Add Store Area
         */
        $scope.AddStoreArea = function(StoreArea) {
            if (StoreArea !== undefined && StoreArea !== null) {
                API.procedure.post('ProcAddStoreArea', {
                        StoreArea: StoreArea
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            } else {
                Notification.show('warning', 'Please enter Store Location');
                return false;
            }
        };

        $scope.checkSelectStoreArea = function(StoreAreaList) {
            var selectedStoreArea = _.find($scope.StoreAreaList, { 'selected': true });
            if (selectedStoreArea === undefined) {
                Notification.show('warning', 'Please select Store Locations');
                return false;
            }
            return selectedStoreArea;
        };

        $scope.RemoveStoreArea = function(StoreAreaList) {
            var selectedStoreArea = $scope.checkSelectStoreArea(StoreAreaList);
            if (selectedStoreArea) {
                API.procedure.post('ProcRemoveStoreArea', {
                        StoreAreaID: StoreAreaID
                    })
                    .then(function(result) {
                        var state = 'department.store-admin';
                        var params = {
                            StoreAreaID: undefined
                        };
                        $rootScope.redirectDept(state, params);
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.AddXLocation = function(StoreAreaList, XLocation) {
            if (XLocation === undefined || XLocation === '') {
                Notification.show('warning', 'Please enter X co-ordinate');
                return false;
            }
            var regInteger = /^\d+$/;
            if (!regInteger.test(XLocation)) {
                Notification.show('warning', 'X co-ordinate must be number');
                return false;
            }
            if (parseInt(XLocation) > 255) {
                Notification.show('warning', 'X co-ordinate not be larger 255');
                return false;
            }
            var selectedStoreArea = $scope.checkSelectStoreArea(StoreAreaList);
            if (selectedStoreArea) {
                API.procedure.post('ProcAddXLocation', {
                        StoreAreaID: selectedStoreArea.StoreAreaID,
                        XCoord: XLocation
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.RemoveXLocation = function(StoreAreaList, XStoreLocations) {
            var sameXLocation = [];
            var selectedStoreArea = $scope.checkSelectStoreArea(StoreAreaList);
            if (selectedStoreArea) {
                var selectedXLocation = _.filter(XStoreLocations, _.matches({ 'selected': true }));
                if (selectedXLocation.length > 0) {

                    /**
                     * get duplicate XLocation item
                     */
                    selectedXLocation.forEach(function(v1) {
                        XStoreLocations.forEach(function(v2) {
                            if (v2.XLocation === v1.XLocation) {
                                sameXLocation.push(v2);
                            }
                        });

                    });
                    sameXLocation.forEach(function(value) {
                        API.procedure.post('ProcRemoveXLocation', {
                                StoreAreaID: selectedStoreArea.StoreAreaID,
                                XCoord: value.XLocation
                            })
                            .then(function(result) {
                                _.remove(XStoreLocations, value);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                } else {
                    Notification.show('warning', 'Please select X co-ordinate');
                    return false;
                }
            }
        };

        $scope.AddYLocation = function(StoreAreaList, YLocation) {
            if (YLocation === undefined || YLocation === '') {
                Notification.show('warning', 'Please enter Y co-ordinate');
                return false;
            }
            var regAlpha = /^[a-zA-Z\s]+$/;
            if (!regAlpha.test(YLocation)) {
                Notification.show('warning', 'Y co-ordinate must be alpha character');
                return false;
            }
            if (YLocation.length > 1) {
                Notification.show('warning', 'Y co-ordinate not be larger 1 character');
                return false;
            }
            var selectedStoreArea = $scope.checkSelectStoreArea(StoreAreaList);
            if (selectedStoreArea) {
                API.procedure.post('ProcAddYLocation', {
                        StoreAreaID: selectedStoreArea.StoreAreaID,
                        YCoord: YLocation
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.RemoveYLocation = function(StoreAreaList, YStoreLocations) {
            var sameYLocation = [];
            var selectedStoreArea = $scope.checkSelectStoreArea(StoreAreaList);
            if (selectedStoreArea) {
                var selectedYLocation = _.filter(YStoreLocations, _.matches({ 'selected': true }));
                if (selectedYLocation.length > 0) {

                    /**
                     * get duplicate XLocation item
                     */
                    selectedYLocation.forEach(function(v1) {
                        YStoreLocations.forEach(function(v2) {
                            if (v2.YLocation === v1.YLocation) {
                                sameYLocation.push(v2);
                            }
                        });
                    });

                    sameYLocation.forEach(function(value) {
                        API.procedure.post('ProcRemoveYLocation', {
                                StoreAreaID: selectedStoreArea.StoreAreaID,
                                YCoord: value.YLocation
                            })
                            .then(function(result) {
                                _.remove(YStoreLocations, value);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                } else {
                    Notification.show('warning', 'Please select Y co-ordinate');
                    return false;
                }
            }
        };

        /**
         * get Courier table
         */
        API.procedure.get('ProcGetCourierList')
            .then(function(result) {
                $scope.CourierList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * Add Courier
         */
        $scope.AddCourier = function(Courier) {
            if (Courier !== undefined && Courier !== null) {
                API.procedure.post('ProcAddCourier', {
                        CourierName: Courier
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            } else {
                Notification.show('warning', 'Please enter Courier');
                return false;
            }
        };

        /**
         * Remove Courier
         */
        $scope.RemoveCourier = function(CourierList) {
            var selectedCourier = _.filter(CourierList, _.matches({ 'selected': true }));
            if (selectedCourier.length > 0) {
                selectedCourier.forEach(function(value) {
                    API.procedure.post('ProcRemoveCourier', {
                            CourierID: value.CourierID
                        })
                        .then(function(result) {
                            _.remove(CourierList, value);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                });
            } else {
                Notification.show('warning', 'Please select Courier');
                return false;
            }
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };
    }

})(angular.module('app.components.department.store-admin', []));
