var TemplateMaterial = require('../../../models/TemplateMaterial');

require('./material-requirement/');
require('./engineering/');
require('./pering/');
require('./subcon/');
require('./review/');
require('./overview/');

(function(app) {

    app.controller('EngineeringTemplateCtrl', EngineeringTemplateCtrl);
    EngineeringTemplateCtrl.$inject = ['$rootScope', '$scope', 'API', 'Notification', '$state', '$stateParams', '$window'];

    function EngineeringTemplateCtrl($rootScope, $scope, API, Notification, $state, $stateParams, $window) {
        var EngineeringTemplateID = $scope.EngineeringTemplateID = $stateParams.EngineeringTemplateID;
        var WorkOrderID = $scope.WorkOrderID = $stateParams.WorkOrderID;
        var OperationID = $stateParams.OperationID;
        var OperationTemplateID = $stateParams.OperationTemplateID;
        $scope.hasEngineeringTemplateID = false;
        $scope.hasWorkOrderID = false;
        $scope.hasOperationID = false;

        if (EngineeringTemplateID !== undefined && EngineeringTemplateID !== '' && EngineeringTemplateID !== null) {
            $scope.hasEngineeringTemplateID = true;
        }
        if (WorkOrderID !== undefined && WorkOrderID !== '' && WorkOrderID !== null) {
            $scope.hasWorkOrderID = true;
        }
        if (OperationID !== undefined && OperationID !== '' && OperationID !== null) {
            $scope.hasOperationID = true;
        }
        if (OperationTemplateID !== undefined && OperationTemplateID !== '' && OperationTemplateID !== null) {
            $scope.hasOperationID = true;
        }

        $scope.checkExistEngWorkOrder = function() {
            if (!$scope.hasEngineeringTemplateID && !$scope.hasWorkOrderID) {
                Notification.show('warning', 'Please enter EngineeringTemplateID or WorkOrderID');
                return false;
            }
            return true;
        };

        $scope.DeptID = $stateParams.DeptID;

        /**
         * The screen is call from 2 screen
         *
         * Screen 1: Part List (EngineeringTemplateID not null)
         * Screen 2: Work Order (WorkOrderID not null)
         */

        $scope.TemplateMaterialInfo = new TemplateMaterial();

        /**
         * get data of Approval drop-down
         */
        API.procedure.get('ProcMaterialLists')
            .then(function(result) {
                $scope.MaterialLists = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * get Shape drop-down
         */
        API.procedure.get('ProcGetMaterialShapes')
            .then(function(result) {
                $scope.MaterialShapes = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * get SPU drop-down
         */
        API.procedure.get('ProcGetUOMList')
            .then(function(result) {
                $scope.UOMList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * get Supplier drop-down
         */
        API.procedure.get('ProcGetStdSuppliers')
            .then(function(result) {
                $scope.StdSuppliers = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * get Units drop-down
         */
        $scope.UnitList = [{
            name: 'inches',
            value: 1
        }, {
            name: 'mm',
            value: 2
        }];

        /**
         * Use to show/hide drop-down when user click icon
         */
        $scope.AreaIcon = true;
        $scope.MaterialDropDown = false;
        $scope.MachineDropDown = false;
        $scope.SubconDropDown = false;

        /**
         * get Machining drop-down
         * Machining drop-down display when click Machining icon
         */
        API.procedure.get('ProcMachiningList')
            .then(function(result) {
                $scope.MachiningList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * get Subcon drop-down
         * Subcon drop-down display when click Subcon icon
         */
        API.procedure.get('ProcSubconList')
            .then(function(result) {
                $scope.SubconList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * P/E Engineering - Engineering Tab
         * get Designated Machine drop-down
         */
        API.procedure.get('ProcMachineList')
            .then(function(result) {
                $scope.MachineList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * Subcon Tab
         * get Supplier drop-down
         */
        API.procedure.get('ProcSupplierList')
            .then(function(result) {
                $scope.SupplierList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * Material - Engineering - Subcon Tab
         * the function working when user click Auto Print
         */
        $scope.UpdateADD = function(AddItem) {
            API.procedure.post('ProcUpdateADD', {
                    ADDID: AddItem.ADDID
                })
                .then(function(result) {})
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.ViewADD = function() {
            if (WorkOrderID !== undefined) {
                API.procedure.post('ProcViewADD', {
                        WorkOrderID: $stateParams.WorkOrderID
                    })
                    .then(function(result) {
                        if (result.length > 0) {
                            var fileName = result[0].FileName;
                            $scope.ViewTmpltADD = $rootScope.PathImageUpload + fileName;
                        } else {
                            Notification.show('warning', 'Image of Work Order is not avaiable');
                        }
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
            if (EngineeringTemplateID !== undefined) {
                var ADDItem = _.find($scope.ADDList, { 'selected': true });
                if (ADDItem !== undefined) {
                    API.procedure.post('ProcViewTmpltADD', {
                            ADDID: ADDItem.ADDID
                        })
                        .then(function(result) {
                            if (result.length > 0) {
                                var fileName = result[0].FileName;
                                var file = $rootScope.PathImageUpload + fileName;
                                $window.open(file, 'blank_');
                            }
                        })
                        .catch(function(error) {
                            throw error;
                        });
                } else {
                    Notification.show('warning', 'Please select A.D.D item');
                    return false;
                }
            }
        };

        $scope.getOperationTable = function(array) {
            var PartTotalSet = 0;
            var PartTotalRun = 0;
            var EstimatedSetTime;
            var EstimatedRunTime;

            array.forEach(function(value, key) {

                /**
                 * Convert hh:mm:ss to h then add to PartTotalSet
                 */
                if (value.EstimatedSetTime) {
                    EstimatedSetTime = $rootScope.timeFormat(value.EstimatedSetTime, 's', -1);
                    PartTotalSet += parseFloat(EstimatedSetTime);
                }

                /**
                 * Convert hh:mm:ss to s then add to PartTotalRun
                 */
                if (value.EstimatedRunTime) {
                    EstimatedRunTime = $rootScope.timeFormat(value.EstimatedRunTime, 's', -1);
                    PartTotalRun += parseFloat(EstimatedRunTime);
                }
            });
            $scope.PartTotalSet = $rootScope.convertNumberToHHMMSS(PartTotalSet / 3600);
            $scope.PartTotalRun = $rootScope.convertNumberToHHMMSS(PartTotalRun / 3600);

            if ($scope.hasOperationID) {
                var selectedOperation;
                if (EngineeringTemplateID !== undefined) {
                    selectedOperation = _.find(array, function(item) {
                        return parseInt(OperationTemplateID) === parseInt(item.OpTemplateID);
                    });
                    if (selectedOperation !== undefined) {
                        selectedOperation.selected = true;
                    }
                }
                if (WorkOrderID !== undefined) {
                    selectedOperation = _.find(array, function(item) {
                        return parseInt(OperationID) === parseInt(item.OpTemplateID);
                    });
                    if (selectedOperation !== undefined) {
                        selectedOperation.selected = true;
                    }
                }
            }
            $scope.ListTemplateOps = _.map(array, function(item) {
                if (item.OpTemplateID !== null) {
                    item.OpTemplateID = parseInt(item.OpTemplateID);
                }
                if (item.OpTypeID !== null) {
                    item.OpTypeID = parseInt(item.OpTypeID);
                }
                if (item.Linked !== null) {
                    item.Linked = parseInt(item.Linked);
                }
                return item;
            });
        };

        if (EngineeringTemplateID !== undefined) {

            /**
             * Engineering Page
             * show Part number & Issue in top screen
             * show Engineering Issue & Planning complete in bottom screen
             */
            API.procedure.post('ProcGetTemplateDetail', {
                    EngineeringTemplateID: EngineeringTemplateID
                })
                .then(function(result) {
                    $scope.TemplateDetail = result[0];
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get data Operation table
             */
            API.procedure.post('ProcListTemplateOps', {
                    EngineeringTemplateID: EngineeringTemplateID
                })
                .then(function(result) {
                    if (result.length > 0) {
                        $scope.getOperationTable(result);
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        }

        /**
         * the screen is redirect to WorkOrder screen
         */
        if (WorkOrderID !== undefined) {

            /**
             * get Overview data
             */
            API.procedure.post('ProcGetWOTemplateDetail', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.TemplateDetail = result[0];
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Operation table
             */
            API.procedure.post('ProcListWOTemplateOps', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    if (result.length > 0) {
                        $scope.getOperationTable(result);
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        }

        /**
         * Engineering Page
         * use to show/hide area icon when user click icon in Operation table
         * top left screen
         */
        $scope.showIcon = function(TypeIcon) {
            $scope.AreaIcon = false;
            if (TypeIcon === 'material') {
                $scope.MaterialDropDown = true;
                $scope.OpTypeID = 1;
                $scope.TreatmentID = true;
            }
            if (TypeIcon === 'machine') {
                $scope.MachineDropDown = true;
                $scope.OpTypeID = 2;
                $scope.TreatmentID = true;
            }
            if (TypeIcon === 'subcon') {
                $scope.SubconDropDown = true;
                $scope.OpTypeID = 3;
                $scope.TreatmentID = false;
            }
        };

        $scope.selectedDropDownMaterialTab = function(array) {
            $rootScope.$broadcast('EngineeringSelectedUnit', array.UnitID);
            $rootScope.$broadcast('EngineeringSelectedShape', array.ShapeID);
            $rootScope.$broadcast('EngineeringSelectedApproval', array.ApprovalID);
            $rootScope.$broadcast('EngineeringSelectedSPU', array.SPUID);
            $rootScope.$broadcast('EngineeringSelectedSupplier', array.StdSupplierID);
        };

        $rootScope.addRootConformityNote = function(ConformityNotes, OperationID) {
            $scope.checkAddConformityNote = false;
            var countItem = 0;
            if ($scope.hasEngineeringTemplateID) {
                if (ConformityNotes.length !== undefined) {
                    ConformityNotes = _.reverse(ConformityNotes);
                    ConformityNotes.forEach(function(value) {
                        API.procedure.post('ProcAddTmpltConformityNote', {
                                UserID: $rootScope.user.getId(),
                                OperationTemplateID: OperationID,
                                Note: value.Note
                            })
                            .then(function(result) {
                                countItem++;
                                if (countItem === ConformityNotes.length) {
                                    $scope.checkAddConformityNote = true;
                                }
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                } else {
                    API.procedure.post('ProcAddTmpltConformityNote', {
                            UserID: $rootScope.user.getId(),
                            OperationTemplateID: OperationID,
                            Note: ConformityNotes.Note
                        })
                        .then(function(result) {
                            $scope.checkAddConformityNote = true;
                            $scope.getTmpltConformityNotes(OperationTemplateID);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
            if ($scope.hasWorkOrderID) {
                if (ConformityNotes.length !== undefined) {
                    ConformityNotes = _.reverse(ConformityNotes);
                    ConformityNotes.forEach(function(value) {
                        API.procedure.post('ProcAddWOTmpltConformityNote', {
                                UserID: $rootScope.user.getId(),
                                OperationID: OperationID,
                                Note: value.Note
                            })
                            .then(function(result) {
                                countItem++;
                                if (countItem === ConformityNotes.length) {
                                    $scope.checkAddConformityNote = true;
                                }
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                } else {
                    API.procedure.post('ProcAddWOTmpltConformityNote', {
                            UserID: $rootScope.user.getId(),
                            OperationID: OperationID,
                            Note: ConformityNotes.Note
                        })
                        .then(function(result) {
                            $scope.checkAddConformityNote = true;
                            $scope.getWOTmpltConformityNotes(OperationID);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };

        $rootScope.addRootMaterialNote = function(MaterialNotes, OperationID) {
            $scope.checkAddMaterialNote = false;
            var countItem = 0;
            if ($scope.hasEngineeringTemplateID) {
                if (MaterialNotes.length !== undefined) {
                    MaterialNotes = _.reverse(MaterialNotes);
                    MaterialNotes.forEach(function(value) {
                        API.procedure.post('ProcAddOpTmpltIntMaterialNotes', {
                                UserID: $rootScope.user.getId(),
                                OperationTemplateID: OperationID,
                                Note: value.Note
                            })
                            .then(function(result) {
                                countItem++;
                                if (countItem === MaterialNotes.length) {
                                    $scope.checkAddMaterialNote = true;
                                }
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                } else {
                    API.procedure.post('ProcAddOpTmpltIntMaterialNotes', {
                            UserID: $rootScope.user.getId(),
                            OperationTemplateID: OperationID,
                            Note: MaterialNotes.Note
                        })
                        .then(function(result) {
                            $scope.checkAddMaterialNote = true;
                            $scope.getOpTmpltIntMaterialNotes(OperationTemplateID);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
            if ($scope.hasWorkOrderID) {
                if (MaterialNotes.length !== undefined) {
                    MaterialNotes = _.reverse(MaterialNotes);
                    MaterialNotes.forEach(function(value) {
                        API.procedure.post('ProcAddOpWOTmpltIntMaterialNotes', {
                                UserID: $rootScope.user.getId(),
                                OperationID: OperationID,
                                Note: value.Note
                            })
                            .then(function(result) {
                                countItem++;
                                if (countItem === MaterialNotes.length) {
                                    $scope.checkAddMaterialNote = true;
                                }
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                } else {
                    API.procedure.post('ProcAddOpWOTmpltIntMaterialNotes', {
                            UserID: $rootScope.user.getId(),
                            OperationID: OperationID,
                            Note: MaterialNotes.Note
                        })
                        .then(function(result) {
                            $scope.checkAddMaterialNote = true;
                            $scope.getOpWOTmpltIntMaterialNotes(OperationID);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };

        $rootScope.addRootTreatNote = function(TreatNotes, OperationID) {
            $scope.checkAddTreatNote = false;
            var countItem = 0;
            if ($scope.hasEngineeringTemplateID) {
                if (TreatNotes.length !== undefined) {
                    TreatNotes = _.reverse(TreatNotes);
                    TreatNotes.forEach(function(value) {
                        API.procedure.post('ProcAddOpTmpltIntTreatNotes', {
                                UserID: $rootScope.user.getId(),
                                OperationTemplateID: OperationID,
                                Note: value.Note
                            })
                            .then(function(result) {
                                countItem++;
                                if (countItem === TreatNotes.length) {
                                    $scope.checkAddTreatNote = true;
                                }
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                } else {
                    API.procedure.post('ProcAddOpTmpltIntTreatNotes', {
                            UserID: $rootScope.user.getId(),
                            OperationTemplateID: OperationID,
                            Note: TreatNotes.Note
                        })
                        .then(function(result) {
                            $scope.checkAddTreatNote = true;
                            $scope.getOpTmpltIntTreatNotes(OperationTemplateID);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
            if ($scope.hasWorkOrderID) {
                if (TreatNotes.length !== undefined) {
                    TreatNotes = _.reverse(TreatNotes);
                    TreatNotes.forEach(function(value) {
                        API.procedure.post('ProcAddWOOpTmpltIntTreatNotes', {
                                UserID: $rootScope.user.getId(),
                                OperationID: OperationID,
                                Note: value.Note
                            })
                            .then(function(result) {
                                countItem++;
                                if (countItem === TreatNotes.length) {
                                    $scope.checkAddTreatNote = true;
                                }
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                } else {
                    API.procedure.post('ProcAddWOOpTmpltIntTreatNotes', {
                            UserID: $rootScope.user.getId(),
                            OperationID: OperationID,
                            Note: TreatNotes.Note
                        })
                        .then(function(result) {
                            $scope.checkAddTreatNote = true;
                            $scope.getWOOpTmpltIntTreatNotes(OperationID);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };

        $rootScope.addRootTemplateProcess = function(ProcessNotes, OperationID) {
            $scope.checkAddProcessNote = false;
            var countItem = 0;
            if ($scope.hasEngineeringTemplateID) {
                if (ProcessNotes.length !== undefined) {
                    ProcessNotes.forEach(function(value) {
                        API.procedure.post('ProcAddTemplateProcess', {
                                OperationTemplateID: OperationID,
                                ProcessNote: value.ProcessNote
                            })
                            .then(function(result) {
                                countItem++;
                                if (countItem === ProcessNotes.length) {
                                    $scope.checkAddProcessNote = true;
                                }
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                } else {
                    API.procedure.post('ProcAddTemplateProcess', {
                            OperationTemplateID: OperationID,
                            ProcessNote: ProcessNotes.ProcessNote
                        })
                        .then(function(result) {
                            $scope.checkAddProcessNote = true;
                            $scope.getTemplateProcesses(OperationTemplateID);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
            if ($scope.hasWorkOrderID) {
                if (ProcessNotes.length !== undefined) {
                    ProcessNotes.forEach(function(value) {
                        API.procedure.post('ProcAddWOTemplateProcess', {
                                OperationID: OperationID,
                                ProcessNote: value.ProcessNote
                            })
                            .then(function(result) {
                                countItem++;
                                if (countItem === ProcessNotes.length) {
                                    $scope.checkAddProcessNote = true;
                                }
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                } else {
                    API.procedure.post('ProcAddWOTemplateProcess', {
                            OperationID: OperationID,
                            ProcessNote: ProcessNotes.ProcessNote
                        })
                        .then(function(result) {
                            $scope.checkAddProcessNote = true;
                            $scope.getWOTemplateProcesses(OperationID);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };

        $rootScope.addRootEngNote = function(EngNotes, OperationID) {
            $scope.checkAddEngNote = false;
            var countItem = 0;
            if ($scope.hasEngineeringTemplateID) {
                if (EngNotes.length !== undefined) {
                    EngNotes = _.reverse(EngNotes);
                    EngNotes.forEach(function(value) {
                        API.procedure.post('ProcAddOpTmpltIntEngNotes', {
                                UserID: $rootScope.user.getId(),
                                OperationTemplateID: OperationID,
                                Note: value.Note
                            })
                            .then(function(result) {
                                countItem++;
                                if (countItem === EngNotes.length) {
                                    $scope.checkAddEngNote = true;
                                }
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                } else {
                    API.procedure.post('ProcAddOpTmpltIntEngNotes', {
                            UserID: $rootScope.user.getId(),
                            OperationTemplateID: OperationID,
                            Note: EngNotes.Note
                        })
                        .then(function(result) {
                            $scope.checkAddEngNote = true;
                            $scope.getOpTmpltIntEngNotes(OperationTemplateID);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
            if ($scope.hasWorkOrderID) {
                if (EngNotes.length !== undefined) {
                    EngNotes = _.reverse(EngNotes);
                    EngNotes.forEach(function(value) {
                        API.procedure.post('ProcAddWOOpTmpltIntEngNotes', {
                                UserID: $rootScope.user.getId(),
                                OperationID: OperationID,
                                Note: value.Note
                            })
                            .then(function(result) {
                                countItem++;
                                if (countItem === EngNotes.length) {
                                    $scope.checkAddEngNote = true;
                                }
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                } else {
                    API.procedure.post('ProcAddWOOpTmpltIntEngNotes', {
                            UserID: $rootScope.user.getId(),
                            OperationID: OperationID,
                            Note: EngNotes.Note
                        })
                        .then(function(result) {
                            $scope.checkAddEngNote = true;
                            $scope.getWOOpTmpltIntEngNotes(OperationID);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };

        $rootScope.addRootApprovalComment = function(ApprovalComments, OperationID, Type) {
            if (Type === 'material') {
                $scope.checkAddApprovalComment = false;
            }
            if (Type === 'subcon') {
                $scope.checkSCAddApprovalCommentItem = false;
            }
            var Operation = _.find($scope.ListTemplateOps, { 'OpTemplateID': parseInt(OperationID) });
            if (Operation === undefined) {
                Notification.show('warning', "Don't exists Operation");
                return false;
            }
            if ((Type === 'subcon' && parseInt(Operation.OpTypeID) === 3) || (Type === 'material') && parseInt(Operation.OpTypeID) === 1) {
                var countItem = 0;
                if ($scope.hasEngineeringTemplateID) {
                    if (ApprovalComments.length !== undefined) {
                        ApprovalComments = _.reverse(ApprovalComments);
                        ApprovalComments.forEach(function(value) {
                            API.procedure.post('ProcAddTmpltApprovalComment', {
                                    UserID: $rootScope.user.getId(),
                                    OperationTemplateID: OperationID,
                                    Comment: value.Comment
                                })
                                .then(function(result) {
                                    countItem++;
                                    if (countItem === ApprovalComments.length) {
                                        if (Type === 'material') {
                                            $scope.checkAddApprovalComment = true;
                                        }
                                        if (Type === 'subcon') {
                                            $scope.checkSCAddApprovalCommentItem = true;
                                        }
                                    }
                                })
                                .catch(function(error) {
                                    throw error;
                                });
                        });
                    } else {
                        API.procedure.post('ProcAddTmpltApprovalComment', {
                                UserID: $rootScope.user.getId(),
                                OperationTemplateID: OperationID,
                                Comment: ApprovalComments.Comment
                            })
                            .then(function(result) {
                                if (Type === 'material') {
                                    $scope.checkAddApprovalComment = true;
                                }
                                if (Type === 'subcon') {
                                    $scope.checkSCAddApprovalCommentItem = true;
                                }
                                $scope.getTmpltApprovalComments(OperationTemplateID);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    }
                }
                if ($scope.hasWorkOrderID) {
                    if (ApprovalComments.length !== undefined) {
                        ApprovalComments = _.reverse(ApprovalComments);
                        ApprovalComments.forEach(function(value) {
                            API.procedure.post('ProcAddWOTmpltApprovalComment', {
                                    UserID: $rootScope.user.getId(),
                                    OperationID: parseInt(OperationID),
                                    Comment: value.Comment
                                })
                                .then(function(result) {
                                    countItem++;
                                    if (countItem === ApprovalComments.length) {
                                        if (Type === 'material') {
                                            $scope.checkAddApprovalComment = true;
                                        }
                                        if (Type === 'subcon') {
                                            $scope.checkSCAddApprovalCommentItem = true;
                                        }
                                    }
                                })
                                .catch(function(error) {
                                    throw error;
                                });
                        });
                    } else {
                        API.procedure.post('ProcAddWOTmpltApprovalComment', {
                                UserID: $rootScope.user.getId(),
                                OperationID: OperationID,
                                Comment: ApprovalComments.Comment
                            })
                            .then(function(result) {
                                if (Type === 'material') {
                                    $scope.checkAddApprovalComment = true;
                                }
                                if (Type === 'subcon') {
                                    $scope.checkSCAddApprovalCommentItem = true;
                                }
                                $scope.getWOTmpltApprovalComments(OperationID);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    }
                }
            } else {
                $scope.checkAddApprovalComment = true;
                $scope.checkSCAddApprovalCommentItem = true;
            }
        };

        $rootScope.addRootPOComment = function(POComments, OperationID) {
            $scope.checkAddPOComment = false;
            var countItem = 0;
            if ($scope.hasEngineeringTemplateID) {
                if (POComments.length !== undefined) {
                    POComments = _.reverse(POComments);
                    POComments.forEach(function(value) {
                        API.procedure.post('ProcAddTmpltPOComment', {
                                UserID: $rootScope.user.getId(),
                                OperationTemplateID: OperationID,
                                Comment: value.Comment
                            })
                            .then(function(result) {
                                countItem++;
                                if (countItem === POComments.length) {
                                    $scope.checkAddPOComment = true;
                                }
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                } else {
                    API.procedure.post('ProcAddTmpltPOComment', {
                            UserID: $rootScope.user.getId(),
                            OperationTemplateID: OperationID,
                            Comment: POComments.Comment
                        })
                        .then(function(result) {
                            $scope.checkAddPOComment = true;
                            $scope.getTmpltPOComments(OperationTemplateID);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
            if ($scope.hasWorkOrderID) {
                if (POComments.length !== undefined) {
                    POComments = _.reverse(POComments);
                    POComments.forEach(function(value) {
                        API.procedure.post('ProcAddWOTmpltPOComment', {
                                UserID: $rootScope.user.getId(),
                                OperationID: OperationID,
                                Comment: value.Comment
                            })
                            .then(function(result) {
                                countItem++;
                                if (countItem === POComments.length) {
                                    $scope.checkAddPOComment = true;
                                }
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });
                } else {
                    API.procedure.post('ProcAddWOTmpltPOComment', {
                            UserID: $rootScope.user.getId(),
                            OperationID: OperationID,
                            Comment: POComments.Comment
                        })
                        .then(function(result) {
                            $scope.checkAddPOComment = true;
                            $scope.getWOTmpltPOComments(OperationID);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };

        /**
         * the function working when user select one item in Operation table
         */
        $scope.selectOperation = function(Operation) {
            $scope.selectedOperation = Operation;
            var OperationTemplateID = Operation.OpTemplateID;

            var state;
            var params = {};
            if (EngineeringTemplateID !== undefined) {
                params.OperationTemplateID = Operation.OpTemplateID;
            }
            if (WorkOrderID !== undefined) {
                params.OperationID = Operation.OpTemplateID;
            }
            if (parseInt(Operation.OpTypeID) === 1) {
                state = 'department.engineering-template.material';
            }
            if (parseInt(Operation.OpTypeID) === 2) {
                state = 'department.engineering-template.engineering';
            }
            if (parseInt(Operation.OpTypeID) === 3) {
                state = 'department.engineering-template.subcon';
            }

            if (!$scope.hasOperationID) {
                if ($rootScope.ApprovalComments !== undefined) {
                    $rootScope.addRootApprovalComment($rootScope.ApprovalComments, Operation.OpTemplateID, 'material');
                } else {
                    $scope.checkAddApprovalComment = true;
                }
                if ($rootScope.POComments !== undefined) {
                    $rootScope.addRootPOComment($rootScope.POComments, Operation.OpTemplateID, 'material');
                } else {
                    $scope.checkAddPOComment = true;
                }
                if ($rootScope.ConformityNotes !== undefined) {
                    $rootScope.addRootConformityNote($rootScope.ConformityNotes, Operation.OpTemplateID, 'material');
                } else {
                    $scope.checkAddConformityNote = true;
                }
                if ($rootScope.MaterialNotes !== undefined) {
                    $rootScope.addRootMaterialNote($rootScope.MaterialNotes, Operation.OpTemplateID, 'material');
                } else {
                    $scope.checkAddMaterialNote = true;
                }
                if ($rootScope.SCApprovalCommentItems !== undefined) {
                    $rootScope.addRootApprovalComment($rootScope.SCApprovalCommentItems, Operation.OpTemplateID, 'subcon');
                } else {
                    $scope.checkSCAddApprovalCommentItem = true;
                }
                if ($rootScope.OpTmpltIntTreatNotes !== undefined) {
                    $rootScope.addRootTreatNote($rootScope.OpTmpltIntTreatNotes, Operation.OpTemplateID);
                } else {
                    $scope.checkAddTreatNote = true;
                }
                if ($rootScope.OpTmpltIntEngNotes !== undefined) {
                    $rootScope.addRootEngNote($rootScope.OpTmpltIntEngNotes, Operation.OpTemplateID);
                } else {
                    $scope.checkAddEngNote = true;
                }
                if ($rootScope.TemplateProcesses !== undefined) {
                    $rootScope.addRootTemplateProcess($rootScope.TemplateProcesses, Operation.OpTemplateID);
                } else {
                    $scope.checkAddProcessNote = true;
                }
            } else {
                $scope.checkAddApprovalComment = true;
                $scope.checkAddPOComment = true;
                $scope.checkAddConformityNote = true;
                $scope.checkAddMaterialNote = true;
                $scope.checkSCAddApprovalCommentItem = true;
                $scope.checkAddTreatNote = true;
                $scope.checkAddEngNote = true;
                $scope.checkAddProcessNote = true;
            }

            $scope.$watchGroup([
                'checkAddApprovalComment',
                'checkAddPOComment',
                'checkAddConformityNote',
                'checkAddMaterialNote',
                'checkSCAddApprovalCommentItem',
                'checkAddTreatNote',
                'checkAddEngNote',
                'checkAddProcessNote'
            ], function(newValues, oldValues, scope) {
                var checkFalse = newValues.indexOf(false);
                if (checkFalse === -1) {

                    /**
                     * Click Material Operation show Material Tab
                     * Click Machine Operation show Engineering Tab
                     * Click Subcon Operation show Subcon Tab
                     */
                    if (parseInt(Operation.OpTypeID) === 1) {
                        state = 'department.engineering-template.material';
                    }
                    if (parseInt(Operation.OpTypeID) === 2) {
                        state = 'department.engineering-template.engineering';
                    }
                    if (parseInt(Operation.OpTypeID) === 3) {
                        state = 'department.engineering-template.subcon';
                    }
                    $rootScope.redirectDept(state, params);
                }
            });
        };
        // End select Operation

        $rootScope.deleteNoteComment = function(item, array, comment_type) {
            if (_.isNil(OperationTemplateID) && _.isNil(OperationID)) {
                Notification.show('warning', 'Please select an Operation');
                return false;
            }
            if (!_.isNil(comment_type) && comment_type !== '') {
                comment_type = comment_type.toLowerCase();
            }
            var CommentID = null;
            var NoteID = null;
            var ProcessID = null;
            var ID = null;
            if (comment_type === 'approvalcomment') {
                ID = 1;
            } else if (comment_type === 'pocomment') {
                ID = 2;
            } else {
                ID = null;
            }
            if ('CommentID' in item) {
                CommentID = parseInt(item.CommentID);
            }
            if ('NoteID' in item) {
                NoteID = parseInt(item.NoteID);
            }
            if ('ProcessID' in item) {
                ProcessID = parseInt(item.ProcessID);
            }
            if (!_.isNil(OperationTemplateID)) {
                API.procedure.post('ProcDeleteTmpltNoteComment', {
                        CommentID: CommentID,
                        NoteID: NoteID,
                        ProcessID: ProcessID,
                        ID: ID,
                    })
                    .then(function(response) {
                        _.remove(array, item);
                    }, function(error) {
                        throw error;
                    });
            }
            if (!_.isNil(OperationID)) {
                API.procedure.post('ProcDeleteOpTmpltNoteComment', {
                        CommentID: CommentID,
                        NoteID: NoteID,
                        ProcessID: ProcessID,
                        ID: ID,
                    })
                    .then(function(response) {
                        _.remove(array, item);
                    }, function(error) {
                        throw error;
                    });
            }
        };

        if ($scope.hasOperationID) {
            $scope.getTEngineering = function(TEngineering) {
                $scope.TemplateEngineering = TEngineering;

                // Selected Designated Machine
                $scope.$watch('MachineList', function(MachineList) {
                    if (MachineList !== undefined) {
                        $scope.Engineering_machine = _.find(MachineList, function(item) {
                            return parseInt(item.MachineID) === parseInt($scope.TemplateEngineering.MachineID);
                        });
                    }
                });
                $scope.populateMachineProgram(OperationTemplateID, $scope.TemplateEngineering.MachineID);
            };

            if (WorkOrderID !== undefined) {
                $scope.getWOTmpltConformityNotes = function(OperationID) {
                    API.procedure.post('ProcGetWOTmpltConformityNotes', {
                            OperationID: OperationID
                        })
                        .then(function(result) {
                            if (result.length > 0) {
                                result.forEach(function(value) {
                                    value.NoteID = parseFloat(value.NoteID);
                                });
                            }
                            $rootScope.ConformityNotes = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getWOTmpltConformityNotes(OperationID);

                $scope.getOpWOTmpltIntMaterialNotes = function(OperationID) {
                    API.procedure.post('ProcGetOpWOTmpltIntMaterialNotes', {
                            OperationID: OperationID
                        })
                        .then(function(result) {
                            if (result.length > 0) {
                                result.forEach(function(value) {
                                    value.NoteID = parseFloat(value.NoteID);
                                });
                            }
                            $rootScope.MaterialNotes = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getOpWOTmpltIntMaterialNotes(OperationID);

                $scope.getWOTemplateMaterial = function(OperationID) {
                    API.procedure.post('ProcGetWOTemplateMaterial', {
                            OperationID: OperationID
                        })
                        .then(function(result) {
                            $scope.TemplateMaterialInfo = new TemplateMaterial(result[0]);

                            /**
                             * Material tab
                             * Selected Unit - Shape - Approval - SPU - Supplier drop-down
                             */
                            $scope.selectedDropDownMaterialTab($scope.TemplateMaterialInfo);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getWOTemplateMaterial(OperationID);

                $scope.getWOADDList = function(OperationID) {
                    API.procedure.post('ProcGetWOADDList', {
                            OperationID: OperationID
                        })
                        .then(function(result) {
                            $scope.ADDList = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getWOADDList(OperationID);

                $scope.getWOTmpltApprovalComments = function(OperationID) {
                    API.procedure.post('ProcGetWOTmpltApprovalComments', {
                            OperationID: OperationID
                        })
                        .then(function(result) {
                            if (result.length > 0) {
                                result.forEach(function(value) {
                                    value.CommentID = parseFloat(value.CommentID);
                                });
                            }
                            $rootScope.ApprovalComments = result;
                            $rootScope.SCApprovalCommentItems = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getWOTmpltApprovalComments(OperationID);

                $scope.getWOTmpltPOComments = function(OperationID) {
                    API.procedure.post('ProcGetWOTmpltPOComments', {
                            OperationID: OperationID
                        })
                        .then(function(result) {
                            $rootScope.POComments = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getWOTmpltPOComments(OperationID);

                $scope.getWOTemplateProcesses = function(OperationID) {
                    API.procedure.post('ProcGetWOTemplateProcesses', {
                            OperationID: OperationID
                        })
                        .then(function(result) {
                            $rootScope.TemplateProcesses = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getWOTemplateProcesses(OperationID);

                $scope.getWOTemplateEngineering = function(OperationID) {
                    API.procedure.post('ProcGetWOTemplateEngineering', {
                            OperationID: OperationID
                        })
                        .then(function(result) {
                            if (result.length > 0) {
                                $scope.getTEngineering(result[0]);
                            }
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getWOTemplateEngineering(OperationID);

                $scope.getWOOpTmpltIntEngNotes = function(OperationID) {
                    API.procedure.post('ProcGetWOOpTmpltIntEngNotes', {
                            OperationID: OperationID
                        })
                        .then(function(result) {
                            $rootScope.OpTmpltIntEngNotes = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getWOOpTmpltIntEngNotes(OperationID);

                $scope.getWOOpTmpltIntTreatNotes = function(OperationID) {
                    API.procedure.post('ProcGetWOOpTmpltIntTreatNotes', {
                            OperationID: OperationID
                        })
                        .then(function(result) {
                            $rootScope.OpTmpltIntTreatNotes = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getWOOpTmpltIntTreatNotes(OperationID);
            }

            if (EngineeringTemplateID !== undefined) {

                /**
                 * Material Tab
                 * get Conformity Notes table
                 */
                $scope.getTmpltConformityNotes = function(OperationTemplateID) {
                    API.procedure.post('ProcGetTmpltConformityNotes', {
                            OperationTemplateID: OperationTemplateID
                        })
                        .then(function(result) {
                            if (result.length > 0) {
                                result.forEach(function(value) {
                                    value.NoteID = parseFloat(value.NoteID);
                                });
                            }
                            $rootScope.ConformityNotes = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getTmpltConformityNotes(OperationTemplateID);

                /**
                 * Material Tab
                 * get Internal Material Notes table
                 */
                $scope.getOpTmpltIntMaterialNotes = function(OperationTemplateID) {
                    API.procedure.post('ProcGetOpTmpltIntMaterialNotes', {
                            OperationTemplateID: OperationTemplateID
                        })
                        .then(function(result) {
                            if (result.length > 0) {
                                result.forEach(function(value) {
                                    value.NoteID = parseFloat(value.NoteID);
                                });
                            }
                            $rootScope.MaterialNotes = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getOpTmpltIntMaterialNotes(OperationTemplateID);

                /**
                 * Material Tab
                 * get Overview data of Operation
                 */
                $scope.getTemplateMaterial = function(OperationTemplateID) {
                    API.procedure.post('ProcGetTemplateMaterial', {
                            OperationTemplateID: OperationTemplateID
                        })
                        .then(function(result) {
                            console.log(result[0]);
                            $scope.TemplateMaterialInfo = new TemplateMaterial(result[0]);
                            if (!_.isNil($scope.TemplateMaterialInfo.Description) && $scope.TemplateMaterialInfo.Description !== '') {
                                API.procedure.post('ProcSearchMaterialSpec', {
                                        MaterialSpec: $scope.TemplateMaterialInfo.Description
                                    }).then(function(result) {
                                        $rootScope.$broadcast('Engineering_PopulateSpecID', result[0]);
                                    })
                                    .catch(function(error) {
                                        throw error;
                                    });
                            }

                            /**
                             * Material tab
                             * Selected Unit - Shape - Approval - SPU - Supplier drop-down
                             */
                            $scope.selectedDropDownMaterialTab($scope.TemplateMaterialInfo);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getTemplateMaterial(OperationTemplateID);

                /**
                 * Material - Engineering - Subcon Tab
                 * get Type Auto-print table
                 */
                $scope.ProcGetADDList = function(OperationTemplateID) {
                    API.procedure.post('ProcGetADDList', {
                            OperationTemplateID: OperationTemplateID
                        })
                        .then(function(result) {
                            $scope.ADDList = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.ProcGetADDList(OperationTemplateID);

                /**
                 * P/E Engineering - Engineering Tab
                 * get Internal Material Notes table
                 */
                $scope.getOpTmpltIntEngNotes = function(OperationTemplateID) {
                    API.procedure.post('ProcGetOpTmpltIntEngNotes', {
                            OperationTemplateID: OperationTemplateID
                        })
                        .then(function(result) {
                            $rootScope.OpTmpltIntEngNotes = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getOpTmpltIntEngNotes(OperationTemplateID);

                /**
                 * Material Tab
                 * get data of standard approval comment table
                 */
                $scope.getTmpltApprovalComments = function(OperationTemplateID) {
                    API.procedure.post('ProcGetTmpltApprovalComments', {
                            OperationTemplateID: OperationTemplateID
                        })
                        .then(function(result) {
                            if (result.length > 0) {
                                result.forEach(function(value) {
                                    value.CommentID = parseFloat(value.CommentID);
                                });
                            }
                            $rootScope.ApprovalComments = result;
                            $rootScope.SCApprovalCommentItems = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getTmpltApprovalComments(OperationTemplateID);

                /**
                 * Material Tab
                 * get data of po comment table
                 */
                $scope.getTmpltPOComments = function(OperationTemplateID) {
                    API.procedure.post('ProcGetTmpltPOComments', {
                            OperationTemplateID: OperationTemplateID
                        })
                        .then(function(result) {
                            $rootScope.POComments = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getTmpltPOComments(OperationTemplateID);

                /**
                 * P/E Engineering - Engineering Tab
                 * get Operation table
                 */
                $scope.getTemplateProcesses = function(OperationTemplateID) {
                    API.procedure.post('ProcGetTemplateProcesses', {
                            OperationTemplateID: OperationTemplateID
                        })
                        .then(function(result) {
                            $rootScope.TemplateProcesses = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getTemplateProcesses(OperationTemplateID);

                /**
                 * Subcon Tab
                 * get Internal Sub-con Notes table
                 */
                $scope.getOpTmpltIntTreatNotes = function(OperationTemplateID) {
                    API.procedure.post('ProcGetOpTmpltIntTreatNotes', {
                            OperationTemplateID: OperationTemplateID
                        })
                        .then(function(result) {
                            $rootScope.OpTmpltIntTreatNotes = result;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getOpTmpltIntTreatNotes(OperationTemplateID);

                /**
                 * P/E Engineering Tab
                 * get Overview info of Tab
                 */
                $scope.getTemplateEngineering = function(OperationTemplateID) {
                    API.procedure.post('ProcGetTemplateEngineering', {
                            OperationTemplateID: OperationTemplateID
                        })
                        .then(function(result) {
                            if (result.length > 0) {
                                $scope.Engineering_machine = _.find($scope.MachineList, function(item) {
                                    return parseInt(item.MachineID) === parseInt(result[0].MachineID);
                                });
                                $scope.populateMachineProgram(OperationTemplateID, $scope.Engineering_machine.MachineID);
                            }
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
                $scope.getTemplateEngineering(OperationTemplateID);
            }
        }

        $scope.UpdateTemplateDetail = function(PlanningComplete) {
            API.procedure.post('ProcUpdateTemplateDetail', {
                    UserID: $rootScope.user.getId(),
                    EngineeringTemplateID: EngineeringTemplateID,
                    PlanningComplete: PlanningComplete
                })
                .then(function(result) {
                    Notification.show('success', 'PlanningComplete success');
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * Material Tab
         * Subcon Tab
         */

        $scope.PurchaseReview = function() {
            var checkExist = $scope.checkExistEngWorkOrder();
            if (checkExist) {
                if (EngineeringTemplateID !== undefined) {
                    API.procedure.post('ProcPurchToReview', {
                            UserID: $rootScope.user.getId(),
                            PartTemplateID: $scope.TemplateDetail.PartTemplateID,
                            ETemplateID: EngineeringTemplateID,
                            PartNumber: $scope.TemplateDetail.PartNumber
                        })
                        .then(function(result) {
                            Notification.show('success', 'Purchasing Review success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }

                if (WorkOrderID !== undefined) {
                    API.procedure.post('ProcWOPurchToReview', {
                            UserID: $rootScope.user.getId(),
                            WorkOrderID: WorkOrderID,
                            PartNumber: $scope.TemplateDetail.PartNumber
                        })
                        .then(function(result) {
                            Notification.show('success', 'Purchasing Review success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };

        /**
         * P/E Engineering - Engineering Tab
         * the function working when change select machine drop-down
         */
        $scope.changeDesignMachine = function(Machine) {
            $scope.Engineering_machine = Machine;

            if ($scope.hasOperationID) {
                var MachineID = parseInt(Machine.MachineID);

                if (EngineeringTemplateID !== undefined) {
                    API.procedure.post('ProcSelectTemplateMachine', {
                            UserID: $rootScope.user.getId(),
                            OperationTemplateID: OperationTemplateID,
                            MachineID: MachineID
                        })
                        .then(function(result) {})
                        .catch(function(error) {
                            throw error;
                        });
                }

                if (WorkOrderID !== undefined) {
                    API.procedure.post('ProcSelectWOTemplateMachine', {
                            UserID: $rootScope.user.getId(),
                            OperationID: OperationID,
                            MachineID: MachineID
                        })
                        .then(function(result) {})
                        .catch(function(error) {
                            throw error;
                        });
                }
                $scope.populateMachineProgram(OperationTemplateID, MachineID);
            }
        };

        $scope.populateMachineProgram = function(OperationTemplateID, MachineID) {

            /**
             * populate program table when change machine
             */
            if (EngineeringTemplateID !== undefined) {
                API.procedure.post('ProcGetMachineOpPrograms', {
                        OperationTemplateID: OperationTemplateID,
                        MachineID: MachineID
                    })
                    .then(function(result) {
                        $scope.MachineOpPrograms = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }

            if (WorkOrderID !== undefined) {
                API.procedure.post('ProcGetWOMachineOpPrograms', {
                        OperationID: OperationID,
                        MachineID: MachineID
                    })
                    .then(function(result) {
                        $scope.MachineOpPrograms = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        /**
         * P/E Engineering - Engineering Tab
         * the function working when user enter input of program table
         */
        $scope.AddMachineOpProgram = function(ProgramNumber, Engineering_machine) {
            if (!$scope.hasOperationID) {
                Notification.show('warning', 'Please select Operation');
                return false;
            }
            if (Engineering_machine === undefined) {
                Notification.show('warning', 'Please select Designated Machine');
                return false;
            }
            var regexNumber = /^\d+$/;
            if (!regexNumber.test(ProgramNumber)) {
                Notification.show('warning', 'Program No must be number');
                return false;
            }

            OperationTemplateID = parseInt(OperationTemplateID);
            OperationID = parseInt(OperationID);
            Engineering_machine.MachineID = parseInt(Engineering_machine.MachineID);
            ProgramNumber = parseInt(ProgramNumber);
            if (EngineeringTemplateID !== undefined) {
                API.procedure.post('ProcAddTmpltProgram', {
                        UserID: $rootScope.user.getId(),
                        OperationTemplateID: OperationTemplateID,
                        MachineID: Engineering_machine.MachineID,
                        ProgNumber: ProgramNumber
                    })
                    .then(function(result) {
                        $scope.populateMachineProgram(OperationTemplateID, Engineering_machine.MachineID);
                        $rootScope.$broadcast('AddTmpltProgram');
                        Notification.show('success', 'Add Program success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }

            if (WorkOrderID !== undefined) {
                API.procedure.post('ProcAddWOTmpltProgram', {
                        UserID: $rootScope.user.getId(),
                        OperationID: OperationID,
                        MachineID: Engineering_machine.MachineID,
                        ProgramNumber: ProgramNumber
                    })
                    .then(function(result) {
                        $scope.populateMachineProgram(OperationID, Engineering_machine.MachineID);
                        $rootScope.$broadcast('AddTmpltProgram');
                        Notification.show('success', 'Add Program success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        /**
         * Engineering Page
         * use to show/hide area icon when user click icon in Operation table
         * top left screen
         */
        $scope.showIcon = function(TypeIcon) {
            $scope.AreaIcon = false;
            if (TypeIcon === 'material') {
                $scope.MaterialDropDown = true;
                $scope.OpTypeID = 1;
            }
            if (TypeIcon === 'machine') {
                $scope.MachineDropDown = true;
                $scope.OpTypeID = 2;
            }
            if (TypeIcon === 'subcon') {
                $scope.SubconDropDown = true;
                $scope.OpTypeID = 3;
            }
        };

        /**
         * Engineering Page
         * add New item in Operation table (top left screen)
         */
        $scope.AddTemplateOp = function(TemplateOp) {
            var checkExist = $scope.checkExistEngWorkOrder();
            if (checkExist) {
                var regexSetRunTime = /^\d+$/;
                if (TemplateOp === undefined) {
                    Notification.show('warning', 'Please enter full information');
                    return false;
                }
                if (TemplateOp.MachiningID === undefined) {
                    TemplateOp.MachiningID = null;
                } else {
                    TemplateOp.MachiningID = TemplateOp.MachiningID.MachiningListID;
                }
                if (TemplateOp.TreatmentID === undefined) {
                    TemplateOp.TreatmentID = null;
                } else {
                    TemplateOp.TreatmentID = TemplateOp.TreatmentID.TreatmentID;
                }
                if (!regexSetRunTime.test(TemplateOp.SetTime)) {
                    Notification.show('warning', 'format of Set Time must be number');
                    return false;
                }
                if (!regexSetRunTime.test(TemplateOp.RunTime)) {
                    Notification.show('warning', 'format of Set Time must be number');
                    return false;
                }

                if (TemplateOp.SetTime) {
                    var hourSetTime = String(TemplateOp.SetTime.slice(0, -4));
                    if (parseInt(hourSetTime) > 838) {
                        Notification.show('warning', 'hour of SetTime not be larger 838');
                        return false;
                    }
                }
                if (TemplateOp.RunTime) {
                    var hourRunTime = String(TemplateOp.RunTime.slice(0, -4));
                    if (parseInt(hourRunTime) > 838) {
                        Notification.show('warning', 'hour of RunTime not be larger 838');
                        return false;
                    }
                }
                if (TemplateOp.Linked === undefined) {
                    TemplateOp.Linked = 0;
                }
                if (EngineeringTemplateID !== undefined) {
                    API.procedure.post('ProcAddTemplateOp', {
                            UserID: $rootScope.user.getId(),
                            EngineeringTemplateID: EngineeringTemplateID,
                            OpTypeID: $scope.OpTypeID,
                            MachiningID: TemplateOp.MachiningID,
                            TreatmentID: TemplateOp.TreatmentID,
                            SetTime: TemplateOp.SetTime,
                            RunTime: TemplateOp.RunTime,
                            Linked: TemplateOp.Linked
                        })
                        .then(function(result) {
                            $state.reload();
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }

                if (WorkOrderID !== undefined) {
                    API.procedure.post('ProcAddWOTemplateOp', {
                            UserID: $rootScope.user.getId(),
                            WorkOrderID: WorkOrderID,
                            OpTypeID: $scope.OpTypeID,
                            MachiningID: TemplateOp.MachiningID,
                            TreatmentID: TemplateOp.TreatmentID,
                            SetTime: TemplateOp.SetTime,
                            RunTime: TemplateOp.RunTime,
                            Linked: TemplateOp.Linked
                        })
                        .then(function(result) {
                            $state.reload();
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };

        /**
         * Engineering Page
         * remove item in Operation table (top left screen)
         */
        $scope.RmvTemplateOp = function(ListTemplateOps) {
            var selectedTemplateOp = _.filter(ListTemplateOps, _.matches({ selected: true }));
            if (selectedTemplateOp.length > 0) {
                selectedTemplateOp.forEach(function(value) {
                    if (EngineeringTemplateID !== undefined) {
                        API.procedure.post('ProcRmvTemplateOp', {
                                UserID: $rootScope.user.getId(),
                                OperationTemplateID: value.OpTemplateID
                            })
                            .then(function(result) {
                                $state.reload();
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    }

                    if (WorkOrderID !== undefined) {
                        API.procedure.post('ProcRmvWOTemplateOp', {
                                UserID: $rootScope.user.getId(),
                                OperationID: value.OpTemplateID
                            })
                            .then(function(result) {
                                $state.reload();
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    }
                });
            } else {
                Notification.show('warning', 'Please select Operation');
                return false;
            }
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };

    }

})(angular.module('app.components.department.engineering-template', [
    'app.components.department.engineering-template.material',
    'app.components.department.engineering-template.pering',
    'app.components.department.engineering-template.engineering',
    'app.components.department.engineering-template.subcon',
    'app.components.department.engineering-template.review',
    'app.components.department.engineering-template.overview'
]));
