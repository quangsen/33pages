(function(app) {
    app.controller('WorkOrderPosCtrl', WorkOrderPosCtrl);
    WorkOrderPosCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API'];

    function WorkOrderPosCtrl($rootScope, $scope, $stateParams, API) {
        var WorkOrderID = $rootScope.WorkOrderID = $stateParams.WorkOrderID;
        if (WorkOrderID !== undefined && WorkOrderID !== '') {
            if ($scope.WorkOrderDetail === undefined) {
                $scope.getWorkOrderInfo(WorkOrderID);
            }

            /**
             * get Work Order GRBs
             */
            API.procedure.post('ProcGetWorkOrderGRBs', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.WorkOrderGRBs = result;
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.redirectGoodsIn = function(GRB) {
                var state = 'department.goods-in';
                var params = {
                    WOGRBNo: GRB
                };
                $rootScope.redirectDept(state, params);
            };

            $scope.redirectPurchaseOrder = function(PO) {
                var state = 'department.purchase-order.details';
                var params = {
                    RFQNo: undefined,
                    PONo: PO,
                    PartNumber: undefined,
                    Issue: undefined,
                    Qty: undefined,
                    Raised: undefined,
                    Description: undefined
                };
                $rootScope.redirectDept(state, params);
            };

        } else {
            var state = 'department.workorders';
            $rootScope.redirectDept(state);
            return false;
        }
    }
})(angular.module('app.components.department.workorders.pos', []));
