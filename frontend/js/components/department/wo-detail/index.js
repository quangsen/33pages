(function(app) {

    app.controller('WoDetailCtrl', WoDetailCtrl);
    WoDetailCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', '$window', 'Notification'];

    function WoDetailCtrl($rootScope, $scope, API, $state, $stateParams, $window, Notification) {
        var WorkOrderID = $scope.WorkOrderID = $stateParams.WorkOrderID;
        if (WorkOrderID !== undefined && WorkOrderID !== '') {

            /**
             * get data overview of page
             */
            API.procedure.post('ProcGetWODetail', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.WODetail = result[0];
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * the function working when select a row of Operation table
             */
            $scope.SelectedOperation = function(Operation) {
                var OperationID = Operation.OperationID;

                /**
                 * get Processes table
                 */
                API.procedure.post('ProcGetOpProcesses', {
                        OperationID: OperationID
                    })
                    .then(function(result) {
                        $scope.OpProcesses = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });

                /**
                 * get Operator Notes table
                 */
                API.procedure.post('ProcGetOpOperatorNotes', {
                        OperationID: OperationID
                    })
                    .then(function(result) {
                        $scope.OpOperatorNotes = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * get Operation table
             */
            API.procedure.post('ProcGetWOOpsDetail', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.WOOpsDetail = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Front Office table
             */
            API.procedure.post('ProcGetWOFONotes', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.WOFONotes = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Delivery table
             */
            API.procedure.post('ProcGetWODeliveries', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.WODeliveries = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * the function working when click Swap button
             */
            $scope.SwapOp = function(WOOpsDetail) {
                var selectedSwap = _.filter(WOOpsDetail, _.matches({ 'Swap': 1 }));
                if (selectedSwap.length === 2) {

                    API.procedure.post('ProcSwapOps', {
                            UserID: $rootScope.user.getId(),
                            OperationID: selectedSwap[0].OperationID,
                            OperationID2: selectedSwap[1].OperationID
                        })
                        .then(function(result) {
                            Notification.show('success', 'Swap Operation success');
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            };

            $scope.ViewDrawing = function() {
                var WOOpsDetailSelected = _.find($scope.WOOpsDetail, { 'selected': true });
                if (WOOpsDetailSelected === undefined) {
                    Notification.show('warning', 'Please select Operation');
                    return false;
                }
                API.procedure.post('ProcViewDrawing', {
                        OperationID: WOOpsDetailSelected.OperationID
                    })
                    .then(function(result) {
                        $scope.Drawing = result[0].FileName;
                        if ($scope.Drawing === null || $scope.Drawing === '' || $scope.Drawing === undefined) {
                            Notification.show('warning', "File doesn't exist");
                        } else {
                            var FileName = $rootScope.PathImageUpload + $scope.Drawing;
                            $window.open(FileName, 'blank_');
                        }
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.UpdateOperation = function(Operation) {
                if (Operation.Bypass === null) {
                    Operation.Bypass = 0;
                }
                if (Operation.Subcon === null) {
                    Operation.Subcon = 0;
                }

                API.procedure.post('ProcUpdateOpDetail', {
                        UserID: $rootScope.user.getId(),
                        OperationID: Operation.OperationID,
                        Bypass: Operation.Bypass,
                        Subcon: Operation.Subcon
                    })
                    .then(function(result) {})
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.ViewOriginTemplate = function() {
                var state = 'department.engineering-template.material';
                var params = {
                    WorkOrderID: WorkOrderID
                };
                $rootScope.redirectDept(state, params);
            };

            $scope.redirectBack = function() {
                $window.history.back();
            };

        } else {
            $window.history.back();
        }
    }

})(angular.module('app.components.department.wo-detail', []));
