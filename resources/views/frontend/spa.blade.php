<!DOCTYPE html>
<html ng-app="app">

<head>
    <title>Kythera Project</title>
    <meta http-equiv="X-UA-Compatible" content="IE=10; IE=9; IE=8; IE=7; IE=EDGE" />
    <script type="text/javascript">
    var BASE_URL = "{{url('')}}";
    var BIRT_ENV = "{{getenv('BIRT_ENV')}}";
    </script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/css/app.min.css') }}">
</head>

<body>
	<div style="width: 1366px; height:  768px; overflow: auto;" ui-view></div>
</body>

</html>
