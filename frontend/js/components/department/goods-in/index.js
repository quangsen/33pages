var moment = require('moment');
var GoodsIn = require('../../../models/GoodsIn');
(function(app) {

    app.controller('GoodsInCtrl', GoodsInCtrl);

    app.filter('contain', function() {
        return function(array, key, data) {
            if (!_.isUndefined(array) && !_.isUndefined(_(array).head())) {
                if (!_.has(_(array).head(), key) || data === undefined || data === '') {
                    return array;
                } else {
                    return _.filter(array, function(item) {
                        var pattern = new RegExp(data, "i");
                        return pattern.test(_.toString(item[key]));
                    });
                }
            }
        };
    });
    GoodsInCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function GoodsInCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {
        var WorkOrderID = $scope.WorkOrderID = $stateParams.WorkOrderID;

        /**
         * Goods in have 2 sub screen is Receive & Material Issue
         * if WordOrderID not exists then execute Receive Screen
         * else execute Material Screen
         */

        /**
         * Sort Table
         */
        $scope.sortTable = [
            { title: 'GRB No.', sortBy: 'GRBNo' },
            { title: 'Spec', sortBy: 'Spec' },
            { title: 'Size', sortBy: 'Size' },
            { title: 'units', sortBy: 'Units' },
            { title: 'Shape', sortBy: 'Shape' },
            { title: 'Release', sortBy: 'ReleaseLevel' },
            { title: 'Purch. Lgth', sortBy: 'PurchaseLength' },
            { title: 'Date in', sortBy: 'DateIn' },
            { title: 'Ord. Qty', sortBy: 'OrderQty' },
            { title: 'In Stk', sortBy: 'InStock' },
            { title: 'Locn', sortBy: 'Location' },
        ];
        $scope.sortBy = $scope.sortTable[0].sortBy;
        $scope.changeSortBy = function(item) {
            $scope.sortBy = item.sortBy;
        };

        $scope.Units = [{
            id: 1,
            name: 'inches'
        }, {
            id: 2,
            name: 'mm'
        }];

        /**
         * get In stock table
         */
        API.procedure.get('ProcMaterialInStock')
            .then(function(result) {
                if (result.length > 0) {
                    result.forEach(function(value) {
                        if (value.GRBNo !== null && value.GRBNo !== "") {
                            value.GRBNo = parseFloat(value.GRBNo);
                        }
                        if (value.Size !== null && value.Size !== "") {
                            value.Size = parseFloat(value.Size);
                        }
                        if (value.OrderQty !== null && value.OrderQty !== "") {
                            value.OrderQty = parseFloat(value.OrderQty);
                        }
                        if (value.InStock !== null && value.InStock !== "") {
                            value.InStock = parseFloat(value.InStock);
                        }
                    });
                    if ($stateParams.GRBNo !== undefined) {
                        var selectMaterial = _.find(result, function(value) {
                            return parseInt(value.GRBNo) === parseInt($stateParams.GRBNo);
                        });
                        if (selectMaterial !== undefined) {
                            selectMaterial.selected = true;
                        }
                    }
                }
                $scope.MaterialInStock = result;

                /**
                 * Params pass to WorkOrder - POs tab
                 */
                if ($stateParams.WOGRBNo !== undefined) {
                    $scope.MaterialInStock = _.filter(result, _.matches({ 'GRBNo': parseInt($stateParams.WOGRBNo) }));
                    $scope.MaterialInStock[0].selected = true;
                    $scope.selectedLocation();
                }
            })
            .catch(function(error) {
                throw error;
            });

        $scope.getMaterialIssues = function(GRBNo) {
            API.procedure.post('ProcGetMaterialIssues', {
                    GRBNo: GRBNo
                })
                .then(function(result) {
                    $scope.MaterialIssues = result;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.selectedLocation = function() {
            $scope.$watch('MaterialInStock', function(MaterialInStock) {
                if (MaterialInStock !== undefined) {
                    var selectedMaterialInstock = _.find(MaterialInStock, { 'selected': true });
                    if (selectedMaterialInstock !== undefined) {
                        var Location = selectedMaterialInstock.Location;
                        if (Location !== null && Location !== undefined) {
                            var LocationY = Location.slice(-1);
                            Location = Location.slice(0, -1);
                            var LocationX = Location.slice(-1);
                            Location = Location.slice(0, -1);
                            Location = Location.trim();

                            if ($scope.LocationInfo === undefined) {
                                $scope.LocationInfo = {};
                            }
                            $scope.$watch('StoreAreaList', function(StoreAreaList) {
                                if (StoreAreaList !== undefined) {
                                    $scope.LocationInfo.Location = _.find(StoreAreaList, { 'StoreArea': Location });
                                    var StoreAreaID = $scope.LocationInfo.Location.StoreAreaID;
                                    $scope.getXLocationProc(StoreAreaID);
                                    $scope.getYLocationProc(StoreAreaID);
                                }
                            });
                            $scope.$watch('XStoreLocations', function(XStoreLocations) {
                                $scope.LocationInfo.XLocation = _.find(XStoreLocations, { 'XLocation': parseInt(LocationX) });
                            });
                            $scope.$watch('YStoreLocations', function(YStoreLocations) {
                                $scope.LocationInfo.YLocation = _.find(YStoreLocations, { 'YLocation': LocationY });
                            });
                        }
                    }
                }
            });
        };

        if ($stateParams.GRBNo !== undefined) {
            $scope.getMaterialIssues($stateParams.GRBNo);
            $scope.selectedLocation();
        }

        /**
         * the function working when click a row of In stock table
         */
        $scope.selectMaterialInStock = function(item) {
            if (WorkOrderID === undefined) {

                var state = 'department.goods-in';
                var params = {
                    WorkOrderID: undefined,
                    GRBNo: item.GRBNo
                };
                $rootScope.redirectDept(state, params);
            } else {
                API.procedure.post('ProcGetMaterialIssueDetail', {
                        WorkOrderID: WorkOrderID,
                        GRBID: item.GRBNo
                    })
                    .then(function(result) {
                        $scope.MaterialIssueDetail = result[0];
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        /**
         * get Location drop-down
         */
        API.procedure.get('ProcGetStoreAreaList')
            .then(function(result) {
                $scope.StoreAreaList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * the function working when user select Location drop-down
         */
        $scope.changeLocation = function(Location) {

            /**
             * populate X - Y Location
             */
            $scope.getXLocationProc(Location.StoreAreaID);
            $scope.getYLocationProc(Location.StoreAreaID);
        };

        $scope.getXLocationProc = function(StoreAreaID) {
            API.procedure.post('ProcGetXStoreLocations', {
                    StoreAreaID: StoreAreaID
                })
                .then(function(result) {
                    $scope.XStoreLocations = result;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.getYLocationProc = function(StoreAreaID) {
            API.procedure.post('ProcGetYStoreLocations', {
                    StoreAreaID: StoreAreaID
                })
                .then(function(result) {
                    $scope.YStoreLocations = result;
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * the function working when click Update Loc'n button
         */
        $scope.updateLocation = function(LocationInfo) {
            $scope.selectedMaterialInstock = _.find($scope.MaterialInStock, { 'selected': true });
            if ($scope.selectedMaterialInstock !== undefined) {

                API.procedure.post('ProcUpdateStockLocation', {
                        GRBNo: $scope.selectedMaterialInstock.GRBNo,
                        StoreAreaID: LocationInfo.Location.StoreAreaID,
                        Xlocation: LocationInfo.XLocation.XLocation,
                        Ylocation: LocationInfo.YLocation.YLocation,
                    })
                    .then(function(result) {
                        $state.reload();
                        Notification.show('success', 'Update Location success');
                    })
                    .catch(function(error) {
                        throw error;
                        // Notification.show('warning', 'Location updated');
                    });

            } else {
                Notification.show('warning', 'Please select Material in stock');
                return false;
            }
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };

        /**
         * Receive Screen
         */
        if (WorkOrderID === undefined) {

            // Use to show/hide Fast Track button
            $scope.isFastTrack = false;

            // Use to show/hide Approval mismatch
            $scope.hasApprovalMismatch = false;

            /**
             * Data to add new GRB
             */
            $scope.GRB = new GoodsIn();

            /**
             * get Approval type drop-down
             */
            API.procedure.get('ProcGetApprovalList')
                .then(function(result) {
                    $scope.ApprovalList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Supplier / Customer drop-down
             */
            API.procedure.get('ProcGetCustomerSupplierList')
                .then(function(result) {
                    $scope.CustomerSupplierList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get UOM drop-down
             */
            API.procedure.get('ProcGetUOMList')
                .then(function(result) {
                    $scope.UOMList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Shape drop-down
             */
            API.procedure.get('ProcGetMaterialShapes')
                .then(function(result) {
                    $scope.MaterialShapes = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Rejects drop-down
             */
            API.procedure.get('ProcGetRejectsList')
                .then(function(result) {
                    $scope.RejectsList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get PO number drop-down
             */
            API.procedure.get('ProcGetPOList')
                .then(function(result) {
                    $scope.POList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.changeGRType = function(GRType) {
                $scope.showFastTrack();
            };

            $scope.showFastTrack = function() {
                if ($scope.GRType !== undefined && $scope.FIMaterial !== undefined) {
                    if (parseInt($scope.GRType.MaterialType) === 8 && parseInt($scope.FIMaterial.Status) === 1) {
                        $scope.isFastTrack = true;
                    } else {
                        $scope.isFastTrack = false;
                    }
                }
            };

            $scope.openBIRT = function(type) {
                switch (type) {
                    case 'DueIn':
                        $rootScope.openTabBirt(type, params);
                        break;
                    case 'PurchaseOrder_v5':
                        if ($scope.PONumber === undefined) {
                            Notification.show('warning', 'Please select PO number');
                            return false;
                        }
                        var params = {
                            PONo: $scope.PONumber.POnID
                        };
                        $rootScope.openTabBirt(type, params);
                        break;
                }
            };

            /**
             * the function working when select PO number drop-down
             */
            $scope.changePOList = function(PO) {

                /**
                 * populate Item drop-down
                 */
                API.procedure.post('ProcGetPOItemsList', {
                        POnID: PO.POnID
                    })
                    .then(function(result) {
                        $scope.POItemsList = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * the function working when select Item drop-down
             */
            $scope.changePOItem = function(POItem) {
                console.log(POItem);
                if (!_.isNil(POItem)) {

                    /**
                     * populate PODetail
                     */
                    API.procedure.post('ProcGetPOItemStoresDetail', {
                            ItemID: POItem.ItemID
                        })
                        .then(function(result) {
                            $scope.POItemStoresDetail = result[0];
                            console.log(result[0]);

                            $scope.Unit = _.find($scope.Units, function(item) {
                                return item.id === parseInt(result[0].UnitID);
                            });

                            $scope.GRB = $scope.GRB.setOptions(result[0]);
                            $scope.GRB.QtyRcvd = parseInt($scope.GRB.Quantity);

                            // Selected Approval drop-down
                            $scope.Approval = _.find($scope.ApprovalList, function(item) {
                                return item.pkApprovalID === $scope.POItemStoresDetail.ApprovalTypeID;
                            });

                            // Selected UOM drop-down
                            $scope.UOM = _.find($scope.UOMList, function(item) {
                                return item.pkUOMID === $scope.POItemStoresDetail.UOMID;
                            });

                            // Selected Shape drop-down
                            $scope.Shape = _.find($scope.MaterialShapes, function(item) {
                                return item.pkShapeID === $scope.POItemStoresDetail.ShapeID;
                            });

                            // Bind data to Work Order No.
                            $scope.GRB.WorkOrderNo = $scope.POItemStoresDetail.WONo;

                            // Bind data to Description
                            $scope.GRB.Description = $scope.POItemStoresDetail.Description;
                            $scope.GRB.Size = $scope.POItemStoresDetail.Size;
                            $scope.GRB.SupplierName = $scope.POItemStoresDetail.SupplierName;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            };

            /**
             * the function working when click 'Generate next GR' button
             */
            $scope.generateNextGR = function() {
                API.procedure.post('ProcGenerateNewGRB', {
                        UserID: $rootScope.user.getId()
                    })
                    .then(function(result) {
                        $scope.GRB.nextGR = result[0].GRBNo;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * get GR type drop-down
             */
            API.procedure.get('ProcGetMaterialTypeList')
                .then(function(result) {
                    $scope.MaterialTypeList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get unknow drop-down when GR type drop-down = FI Material
             */
            API.procedure.get('ProcPartTemplateList')
                .then(function(result) {
                    if (result.length > 0) {
                        result.forEach(function(value, key) {
                            result[key].DisplayData = value.PartNumber + ' // ' + value.Issue + ' // ' + value.COS;
                        });
                        $scope.PartTemplateList = result;
                    }
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * the function working when select GR type drop-down
             */
            $scope.changeFIMaterial = function(FIMaterial) {
                $scope.showFastTrack();

                /**
                 * populate Customer Order No. drop-down
                 */
                API.procedure.post('ProcGetMatchingCOList', {
                        PartTemplateID: FIMaterial.PTemplateID
                    })
                    .then(function(result) {
                        $scope.MatchingCOList = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });

                if (parseInt(FIMaterial.Status) === 1) {
                    $scope.isFastTrack = true;
                } else {
                    $scope.isFastTrack = false;
                }
            };

            /**
             * the function working when select Customer Order drop-down
             */
            $scope.changeCustomerOrder = function(CustomerOrder) {
                $scope.GRB.FastTrack = parseInt(CustomerOrder.FastTrack);

                /**
                 * populate Word Order No. drop-down
                 */
                API.procedure.post('ProcGetMatchingLineItems', {
                        OrderID: CustomerOrder.OrderID
                    })
                    .then(function(result) {
                        $scope.MatchingLineItems = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * the function working when select Approval drop-down
             */
            $scope.changeApproval = function(Approval) {
                if ($scope.POItemStoresDetail !== undefined) {
                    if (Approval.pkApprovalID !== $scope.POItemStoresDetail.ApprovalTypeID) {
                        $scope.hasApprovalMismatch = true;
                    } else {
                        $scope.hasApprovalMismatch = false;
                    }
                }
            };

            $scope.changeUnit = function(Unit) {
                $scope.Unit = Unit;
            };

            /**
             * the function working when click 'Add' button
             */
            $scope.AddGRB = function(GRB) {
                if (GRB.getRcvdDate() === null) {
                    GRB.RcvdDate = moment().format('YYYY-MM-DD');
                }
                if (GRB.getReleaseNoteNo() === undefined) {
                    GRB.ReleaseNoteNo = '';
                }
                var UnitID;
                if (_.isNil($scope.Unit) || $scope.GRType.MaterialType != 1) {
                    UnitID = null;
                } else {
                    UnitID = $scope.Unit.id;
                }
                API.procedure.post('ProcAddGRB', {
                        UserID: $rootScope.user.getId(),
                        RcvdDate: GRB.getRcvdDate(),
                        MaterialTypeID: GRB.getMaterialTypeID(),
                        OrderID: GRB.getOrderID(),
                        LineItemID: GRB.getLineItemID(),
                        POnID: GRB.getPOnID(),
                        ItemID: GRB.getItemID(),
                        ETemplateID: GRB.getETemplateID(),
                        WorkOrderNo: GRB.getWorkOrderNo(),
                        ReleaseNoteNo: GRB.getReleaseNoteNo(),
                        ID: GRB.getID(),
                        Type: GRB.getType(),
                        Description: GRB.getDescription(),
                        DeliveryNoteNo: GRB.getDeliveryNoteNo(),
                        PartNumber: GRB.getPartNumber(),
                        QtyRcvd: GRB.getQtyRcvd(),
                        QtyManu: GRB.getQtyManu(),
                        Size: GRB.getSize(),
                        UOMID: GRB.getUOMID(),
                        ShapeID: GRB.getShapeID(),
                        StoreAreaID: GRB.getStoreAreaID(),
                        XLocation: GRB.getYLocation(),
                        YLocation: GRB.getXLocation(),
                        DirectIssue: GRB.getDirectIssue(),
                        ApprovalTypeID: GRB.getApprovalTypeID(),
                        RejectNoteID: GRB.getRejectNoteID(),
                        FastTrack: $scope.isFastTrack,
                        UnitID: UnitID
                    })
                    .then(function(result) {
                        Notification.show('success', 'Add GRB success');
                        // $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.beforeUpload = function(file) {
                if (!$scope.GRB || !$scope.GRB.nextGR) {
                    Notification.show('warning', 'Please click "Generate next GR" button');
                    return false;
                }
                if ($scope.DocTypeID === undefined) {
                    Notification.show('warning', 'Please select DocType');
                    return false;
                }
            };

            $scope.uploadSuccess = function(response) {
                API.procedure.post('ProcDropDeliveryDocument', {
                        UserID: $rootScope.user.getId(),
                        GRBNo: $scope.GRB.nextGR,
                        DocTypeID: $scope.DocTypeID,
                        FileName: response.data
                    })
                    .then(function(result) {
                        Notification.show('success', 'Drop Delivery Doc success');
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * Material Issue screen
             */
        } else {

            $scope.BeginMaterialIssue = function() {
                API.procedure.post('ProcBeginMaterialIssue', {
                        UserID: $rootScope.user.getId(),
                        WorkOrderID: WorkOrderID
                    })
                    .then(function(result) {
                        Notification.show('success', 'Begin Material Issue success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.PauseMaterialIssue = function() {
                API.procedure.post('ProcPauseMaterialIssue', {
                        UserID: $rootScope.user.getId(),
                        WorkOrderID: WorkOrderID
                    })
                    .then(function(result) {
                        Notification.show('success', 'Pause Material Issue success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.EndMaterialIssue = function() {
                API.procedure.post('ProcEndMaterialIssue', {
                        UserID: $rootScope.user.getId(),
                        WorkOrderID: WorkOrderID
                    })
                    .then(function(result) {
                        Notification.show('success', 'End Material Issue success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };
        }

    }

})(angular.module('app.components.department.goods-in', [
    'angular.filter'
]));
