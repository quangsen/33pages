var CustomerOpenHours = require('../../../../models/CustomerOpenHours');
var CustomerContact = require('../../../../models/CustomerContact');

(function(app) {
    app.controller('CustomerContactCtrl', CustomerContactCtrl);

    CustomerContactCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API', '$state', 'Notification', '$window'];

    function CustomerContactCtrl($rootScope, $scope, $stateParams, API, $state, Notification, $window) {

        var customer_id = $stateParams.customer_id;
        $scope.customer_id = customer_id;

        $scope.$watch('CustomerInfo', function(CustomerInfo) {
            if (CustomerInfo === undefined | $scope.days === undefined) {
                $scope.getCustomerInfo(customer_id);
            } else {
                /*
                 * get additional
                 */
                API.procedure.post('ProcGetCustomerAdditional', {
                        CustomerID: customer_id,
                    })
                    .then(function(result) {
                        $scope.CustomerAdditional = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });

                /*
                 * get customer contact
                 */
                API.procedure.post('ProcGetCustomerContacts', {
                        CustomerID: customer_id,
                    })
                    .then(function(result) {
                        if (result.length > 0) {
                            $scope.CustomerContacts = result;
                        }
                    })
                    .catch(function(error) {
                        throw error;
                    });

                /**
                 * get Customer Notes
                 */
                API.procedure.post('ProcGetCustomerNotes', {
                        CustomerID: customer_id,
                    })
                    .then(function(result) {
                        $scope.CustomerNotes = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });

                /**
                 * Add Note
                 */
                $scope.AddNote = function(Note) {
                    if (Note === undefined || Note === '') {
                        Notification.show('warning', 'Please enter Freeform Notes');
                        return false;
                    }
                    API.procedure.post('ProcAddCustomerNote', {
                            UserID: $rootScope.user.getId(),
                            CustomerID: customer_id,
                            Note: Note
                        })
                        .then(function(result) {
                            $state.reload();
                        })
                        .catch(function(error) {
                            throw error;
                        });
                };
            }
        });

        function validateContact(Customer) {
            var regexExtension = /^\d+$/;
            var regexPhone = /^\d+[ ]?\d*$/;
            var regexEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
            if (Customer.ContactName === '') {
                Notification.show('warning', 'Please enter Name');
                return false;
            }
            if (Customer.Extension !== '') {
                if (!regexExtension.test(Customer.Extension)) {
                    Notification.show('warning', 'Extension must be number');
                    return false;
                }
            }
            if (Customer.Phone === '') {
                Notification.show('warning', 'Please enter Phone');
                return false;
            } else {
                if (!regexPhone.test(Customer.Phone)) {
                    Notification.show('warning', 'Phone must be number');
                    return false;
                }
            }
            if (Customer.Email === '') {
                Notification.show('warning', 'Please enter Email');
                return false;
            } else {
                if (!regexEmail.test(Customer.Email)) {
                    Notification.show('warning', 'Email is not valid');
                    return false;
                }
            }
            return true;
        }

        $scope.AddContact = function(Customer) {
            if (Customer !== undefined) {
                Customer = new CustomerContact(Customer);
                var validate = validateContact(Customer);
                if (validate === true) {
                    API.procedure.post('ProcAddCustomerContact', {
                            UserID: $rootScope.user.getId(),
                            CustomerID: customer_id,
                            ContactName: Customer.getContactName(),
                            Position: Customer.getPosition(),
                            Phone: Customer.getPhone(),
                            Extension: Customer.getExtension(),
                            Email: Customer.getEmail(),
                            AcknowContact: Customer.getAcknowContact(),
                            PrimaryContact: Customer.getPrimaryContact()
                        }).then(function(result) {
                            $state.reload();
                        })
                        .catch(function(error) {
                            throw error;
                        });

                }
            } else {
                Notification.show('warning', 'Please enter Customer Contact');
                return false;
            }
        };

        $scope.UpdateContact = function(CustomerContacts) {
            if (CustomerContacts.length > 0) {
                var selectedContacts = _.filter(CustomerContacts, _.matches({ 'selected': true }));
                if (selectedContacts.length > 0) {
                    selectedContacts.forEach(function(value) {
                        value = new CustomerContact(value);
                        var validate = validateContact(value);
                        if (validate === true) {
                            API.procedure.post('ProcUpdateCustomerContact', {
                                    UserID: $rootScope.user.getId(),
                                    ContactID: value.ContactID,
                                    ContactName: value.getContactName(),
                                    Position: value.getPosition(),
                                    Phone: value.getPhone(),
                                    Extension: value.getExtension(),
                                    Email: value.getEmail(),
                                    AcknowContact: value.getAcknowContact(),
                                    PrimaryContact: value.getPrimaryContact()
                                })
                                .then(function(result) {
                                    Notification.show('success', 'Update success');
                                })
                                .catch(function(error) {
                                    throw error;
                                });
                        }
                    });
                } else {
                    Notification.show('warning', 'Please select Customer Contact');
                    return false;
                }
            }
        };

        $scope.RmvContact = function(CustomerContacts) {
            if (CustomerContacts.length > 0) {
                var selectedContacts = _.filter(CustomerContacts, _.matches({ 'selected': true }));
                if (selectedContacts.length > 0) {
                    selectedContacts.forEach(function(value) {

                        API.procedure.post('ProcDeleteCustomerContact', {
                                UserID: $rootScope.user.getId(),
                                ContactID: value.ContactID
                            })
                            .then(function(result) {
                                _.remove(CustomerContacts, value);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });

                } else {
                    Notification.show('warning', 'Please select Customer Contact');
                    return false;
                }
            }
        };

        $scope.RmvNote = function(CustomerNotes) {
            console.log(CustomerNotes);
            if (CustomerNotes.length > 0) {
                var selectedNotes = _.filter(CustomerNotes, _.matches({ 'selected': true }));
                if (selectedNotes.length > 0) {
                    selectedNotes.forEach(function(value) {

                        API.procedure.post('ProcDeleteCustomerNote', {
                                UserID: $rootScope.user.getId(),
                                NoteID: value.NoteID
                            })
                            .then(function(result) {
                                _.remove(CustomerNotes, value);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });

                } else {
                    Notification.show('warning', 'Please select Customer Note');
                    return false;
                }
            }
        };

        $scope.RmvAdditional = function(CustomerAdditional) {
            if (CustomerAdditional.length > 0) {
                var selectedAdditional = _.filter(CustomerAdditional, _.matches({ 'selected': true }));
                if (selectedAdditional.length > 0) {
                    selectedAdditional.forEach(function(value) {

                        API.procedure.post('ProcDeleteCustomerAdditional', {
                                UserID: $rootScope.user.getId(),
                                AdditionaID: value.AdditionalID
                            })
                            .then(function(result) {
                                _.remove(CustomerAdditional, value);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    });

                } else {
                    Notification.show('warning', 'Please select Additional');
                    return false;
                }
            }
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };
    }
})(angular.module('app.components.customer.contact', []));
