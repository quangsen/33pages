-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 24, 2016 at 08:53 AM
-- Server version: 5.5.47
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kythera`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`david`@`localhost` PROCEDURE `AddOperation` (IN `OperationID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `AddOpTmpltSubconDetail` (IN `UserID` SMALLINT UNSIGNED, `OperationTemplateID` MEDIUMINT UNSIGNED, `TreatmentID` TINYINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `LeadTime` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

INSERT INTO			TemplateTreatment
					(pkOperationTemplateID, fkTreatmentID, LeadTime, StandardSupplierID, AddedByfkUserID)
VALUES				(OperationTemplateID, TreatmentID, LeadTime, SupplierID, UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `AddProgram` (IN `OperationID` INT, IN `MachineID` INT, IN `ProgramNumber` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

INSERT INTO 	Kythera.OpMachineProgram  
				(fkProgramNumber, fkOperationID, fkMachineID) 
VALUES			(ProgramNumber, OperationID, MachineID);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `AddWOOpTmpltSubconDetail` (IN `UserID` SMALLINT UNSIGNED, `OperationID` MEDIUMINT UNSIGNED, `TreatmentID` TINYINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `LeadTime` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

INSERT INTO			OperationTreatment
					(pkOperationID, fkTreatmentID, LeadTime, AddedByfkUserID)
VALUES				(OperationID, TreatmentID, LeadTime, UserID);

SELECT			Row_Count() INTO Result;

INSERT INTO			SupplierTreatment
					(fkSupplierID, fkOperationID)
VALUES				(SupplierID, OperationID);

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `BeginInspection` (IN `UserID` SMALLINT UNSIGNED, `GRBID` INT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO			Inspection
					(fkUserID, fkTypeID, InspectStart)
VALUES				(UserID, 1, now());

INSERT INTO			MaterialInspect
					(fkMaterialID, fkInspectionID)
VALUES				(GRBID, LAST_INSERT_ID());

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `BeginInspection2` (IN `UserID` SMALLINT UNSIGNED, `GRBID` INT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO			Inspection
					(fkUserID, fkTypeID, InspectStart)
VALUES				(UserID, 1, now());

INSERT INTO			MaterialInspect
					(fkMaterialID, fkInspectionID)
VALUES				(GRBID, LAST_INSERT_ID());

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `CompleteInspection` (IN `UserID` SMALLINT UNSIGNED, `InspectionID` BIGINT UNSIGNED)  BEGIN
DECLARE		MsgResult 		varchar(25);

IF		NOT ISNULL((SELECT InspectStart
					FROM  Inspection
					WHERE pkInspectionID = InspectionID)) 
		AND ISNULL((SELECT InspectEnd
					FROM  Inspection
					WHERE pkInspectionID = InspectionID))

THEN			UPDATE	Inspection
		SET		InspectEnd = now(), fkUserIDComplete = UserID

		WHERE	pkInspectionID = InspectionID;

		SET 	MsgResult = 'Inspection Completed';

ELSE			SET 	MsgResult = 'Error; not running';

END IF;

SELECT	MsgResult;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `CompleteOpInspection` (IN `UserID` SMALLINT UNSIGNED, `InspectionID` BIGINT UNSIGNED)  BEGIN
UPDATE		Inspection
SET			InspectEnd = now()

WHERE		pkInspectionID = InspectionID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `CompleteOpInspection2` (IN `UserID` SMALLINT UNSIGNED, `InspectionID` BIGINT UNSIGNED)  BEGIN
DECLARE	MsgResult varchar(25);
DECLARE	OpNo		tinyint unsigned;
DECLARE	WONo		mediumint unsigned;

IF		(SELECT Count(*)
		FROM 	Inspection
		WHERE 	pkInspectionID = InspectionID
		AND		InspectEnd IS NULL) = 0

THEN
			SET 	MsgResult = 'Error; not running';

ELSE 	IF	(SELECT Count(*)
			FROM	InspectPause
			WHERE	fkInspectionID = InspectionID
			AND EndTime IS NULL) = 0

		THEN

			UPDATE	Inspection
			SET		InspectEnd = now(), fkUserIDComplete = UserID

			WHERE	pkInspectionID = InspectionID;

			SET		MsgResult = 'Inspection Complete';

		ELSE
			SET MsgResult = 'Error; task paused';

		END IF;
END IF;

SET		WONo = (SELECT 	fkWorkOrderID
				FROM 		Operation O3
				INNER JOIN OperationInspect OI3
				ON			O3.pkOperationID = OI3.fkOperationID
				WHERE		fkInspectionID = InspectionID);

SET		OpNo = (SELECT 	OperationNumber
				FROM 		Operation O4
				INNER JOIN OperationInspect OI4
				ON			O4.pkOperationID = OI4.fkOperationID
				WHERE		fkInspectionID = InspectionID);

IF		(SELECT 	Count(*)
		FROM 		Operation
		WHERE		fkWorkOrderID = WONo) = OpNo

THEN			INSERT INTO 	Todo
						(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, Complete, Description)
		VALUES			(CurDate(), 6, UserID, 7, WONo, 0, 'WO complete; Send outvoice');

END IF; 

SELECT		MsgResult;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `CreateOpsFromTemplate` (IN `UserID` SMALLINT UNSIGNED, `NewWONo` MEDIUMINT UNSIGNED, `ETemplateNo` SMALLINT UNSIGNED)  BEGIN
DECLARE done 																		INT DEFAULT FALSE;
DECLARE CRiskStatement 																CHAR(100);
DECLARE OpStore 																	INT unsigned;
DECLARE OperationTemplateID, ItemStore 												mediumint unsigned;
DECLARE EngineeringTemplateID, RFQOid, StdSuppID									smallint unsigned;
DECLARE COperationNumber, COperationTypeID, MachiningID, CLinked, OrderItemNumber 	tinyint unsigned;
DECLARE Op, OpMat, RFQOrd, OrdIt, RFQno, POnot, POCom, OpOrdIt, RFQOSup, OpMac,
		OpMacProg, MachSet, OpTr, OpPro, RFQOSu										tinyint unsigned;
DECLARE CEstimatedSetTime, CEstimatedRunTime 										time;

DECLARE OpTmpltCursor CURSOR FOR 
		SELECT pkOperationTemplateID, fkEngineeringTemplateID, OperationNumber, fkOperationTypeID, fkMachiningID, 
				EstimatedSetTime, EstimatedRunTime, RiskStatement, Linked 

		FROM 	OperationTemplate
		
		WHERE	fkEngineeringTemplateID = ETemplateNo;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  OPEN OpTmpltCursor;

  read_loop: LOOP
		FETCH OpTmpltCursor INTO OperationTemplateID, EngineeringTemplateID, COperationNumber, COperationTypeID, MachiningID, 
								CEstimatedSetTime, CEstimatedRunTime, CRiskStatement, CLinked;

		IF done THEN
		  LEAVE read_loop;

		END IF;

		INSERT INTO	Operation
					(fkworkOrderID, OperationNumber, fkOperationTypeID, fkMachiningTypeID,
					EstimatedSetTime, EstimatedRunTime, RiskStatement, Linked)
		VALUES		(NewWONo, COperationNumber, COperationTypeID, MachiningID, 
					CEstimatedSetTime, CEstimatedRunTime, CRiskStatement, CLinked);
					
		SET 		OpStore = Last_Insert_ID();

		INSERT INTO spud			
					(Section, NewID)
		VALUES		('OpMain', Row_count());

		IF COperationTypeID = 1 THEN

				INSERT INTO	OperationMaterial
							(pkOperationID, fkMaterialSpecID, Description, Size, fkShapeID, PartLength, fkApprovalID, 
							SupplyCondition, FreeIssue, UnitID, StdfkSupplierID, IssueInstruction, SPUfkUOMID, 
							SPU, Yield, TestPieces, StdDeliveryDays, AddedByfkUserID)
				SELECT		OpStore, fkMaterialSpecID, Description, Size, fkShapeID, PartLength, fkApprovalID, 
							SupplyCondition, FreeIssue, UnitID, StdfkSupplierID, IssueInstruction, SPUfkUOMID,
							UnitID, Yield, TestPieces, StdDeliveryDays, AddedByfkUserID

				FROM		TemplateMaterial

				WHERE		pkOperationTemplateID = OperationTemplateID;

				INSERT INTO spud			
							(Section, NewID)
				VALUES		('OpMat1', Row_count());
				
								IF (SELECT 	FreeIssue 
					FROM 	TemplateMaterial
					WHERE	pkOperationTemplateID = OperationTemplateID) IS NULL OR 
					(SELECT 	FreeIssue 
					FROM 	TemplateMaterial
					WHERE	pkOperationTemplateID = OperationTemplateID) <> 1 THEN
				
												INSERT INTO	RFQOrder
									(fkApprovalLevelID, fkUserID, CreateDate, LeadDays, Cancelled, Complete, POType)
						SELECT		fkApprovalID, UserID, Now(), StdDeliveryDays, 0, 0, 1

						FROM		TemplateMaterial

						WHERE		pkOperationTemplateID = OperationTemplateID;

						
						SET			RFQOid = Last_Insert_ID();

						INSERT INTO spud			
									(Section, NewID)
						VALUES		('RFord1', Row_count());


												INSERT INTO	OrderItem
									(fkRFQOID, ItemNo, fkPartTemplateID, fkShapeID, SPU, fkUOMID, Quantity, Description,
									Size, UnitID, AddedByfkUserID)
						SELECT		RFQOid, 1,(SELECT fkPartTemplateID
																FROM EngineeringTemplate
																WHERE pkEngineeringTemplateID = ETemplateNo), 
									fkShapeID, 1, SPUfkUOMID, 
																	((SELECT QuantityReqd + QuantityOvers
										FROM WorkOrder
										WHERE pkWorkOrderID = NewWONo)
									*1.1 + (SELECT TestPieces
											FROM TemplateMaterial
											WHERE pkOperationTemplateID = OperationTemplateID)
									/ (SELECT Yield
										FROM TemplateMaterial
										WHERE pkOperationTemplateID = OperationTemplateID))

									, Description, Size, UnitID, UserID

						FROM		TemplateMaterial

						WHERE		pkOperationTemplateID = OperationTemplateID;

												SET ItemStore = Last_Insert_ID();

						INSERT INTO spud			
									(Section, NewID)
						VALUES		('OrdIt1', Row_count());
						
						
												INSERT INTO	RFQn
									(fkRFQOID, AddedByfkUserID)
						VALUES		(RFQOid, UserID);

						INSERT INTO spud			
									(Section, NewID)
						VALUES		('RFQNo1', Row_count());


												INSERT INTO	PONote
									(fkRFQOID, Note)
						SELECT		RFQOid, Comment

						FROM		TemplateApprovalComments

						WHERE		fkOperationTemplateID = OperationTemplateID;

						INSERT INTO spud			
									(Section, NewID)
						VALUES		('POnot1', Row_count());


												INSERT INTO	POComment
									(fkRFQOID, Comment, Deleted)
						SELECT		RFQOid, Comment, 0

						FROM		TemplatePOComments

						WHERE		fkOperationTemplateID = OperationTemplateID;

						INSERT INTO spud			
									(Section, NewID)
						VALUES		('POcom1', Row_count());


						
												INSERT INTO	OperationOrderItem
									(fkOperationID, fkItemID, AddedByfkUserID)
						VALUES		(OpStore, ItemStore, UserID);

						INSERT INTO spud			
									(Section, NewID)
						VALUES		('OpOrI1', Row_count());


												SET StdSuppID = (SELECT StdfkSupplierID
										FROM TemplateMaterial
										WHERE pkOperationTemplateID = OperationTemplateID);

						IF  StdSuppID IS NOT NULL THEN
								INSERT INTO	RFQOSupplier
											(fkRFQOID, fkSupplierID, Selected)
								SELECT		RFQOid, StdSuppID, 1

								FROM		TemplateMaterial

								WHERE		pkOperationTemplateID = OperationTemplateID;

						INSERT INTO spud			
									(Section, NewID)
						VALUES		('RFOSu1', Row_count());

						END IF;
				END IF;


		END IF;

		IF COperationTypeID = 2 THEN
				
				INSERT INTO	OperationMachine
							(fkOperationID, fkMachineID, Multi)
				SELECT		DISTINCT OpStore, fkMachineID, CLinked

				FROM		OpTmpltMachineProgram

				WHERE		fkOperationTemplateID = OperationTemplateID;

				INSERT INTO spud			
							(Section, NewID)
				VALUES		('OPmac2', Row_count());


								INSERT INTO	OpMachineProgram
							(fkOperationID, fkMachineID, fkProgramNumber, DateAdded)
				SELECT		OpStore, fkMachineID, ProgramNumber, DATE(Now())

				FROM		OpTmpltMachineProgram

				WHERE		fkOperationTemplateID = OperationTemplateID;

				INSERT INTO spud			
							(Section, NewID)
				VALUES		('OpMcP2', Row_count());


								INSERT INTO	MachineSet
							(fkOperationID, fkMachineID)
				SELECT		DISTINCT OpStore, fkMachineID

				FROM		OpTmpltMachineProgram

				WHERE		fkOperationTemplateID = OperationTemplateID;

				INSERT INTO spud			
							(Section, NewID)
				VALUES		('McSet2', Row_count());


		END IF;

		IF COperationTypeID = 3 THEN
				INSERT INTO	OperationTreatment
							(pkOperationID, fkTreatmentID, LeadTime, ApprovalComment, AddedByfkUserID)
				SELECT		OpStore, fkTreatmentID, LeadTime, ApprovalComment, UserID
				
				FROM		TemplateTreatment

				WHERE		pkOperationTemplateID = OperationTemplateID;

				INSERT INTO spud			
							(Section, NewID)
				VALUES		('OpTr3', Row_count());

												
								INSERT INTO	RFQOrder
							(fkUserID, CreateDate, LeadDays, Cancelled, Complete, POType)
				SELECT		UserID, Now(), LeadTime, 0, 0, 2

				FROM		TemplateTreatment

				WHERE		pkOperationTemplateID = OperationTemplateID;

				
				SET			RFQOid = Last_Insert_ID();
				INSERT INTO spud			
							(Section, NewID)
				VALUES		('RFOrd3', Row_count());


								INSERT INTO	OrderItem
							(fkRFQOID, ItemNo, fkPartTemplateID, Quantity, Description, AddedByfkUserID)
				SELECT		RFQOid, 1,(SELECT fkPartTemplateID
														FROM EngineeringTemplate
														WHERE pkEngineeringTemplateID = ETemplateNo), 
														(SELECT QuantityReqd + QuantityOvers
								FROM WorkOrder
								WHERE pkWorkOrderID = NewWONo)
							*1.1
							, Description , UserID

				FROM		TemplateTreatment TT1
				INNER JOIN	TreatmentList TL1
				ON			TT1.fkTreatmentID = TL1.pkTreatmentID

				WHERE		pkOperationTemplateID = OperationTemplateID;

								SET ItemStore = Last_Insert_ID();
				INSERT INTO spud			
							(Section, NewID)
				VALUES		('OrdIt3', Row_count());
				
				
								INSERT INTO	RFQn
							(fkRFQOID, AddedByfkUserID)
				VALUES		(RFQOid, UserID);

				INSERT INTO spud			
							(Section, NewID)
				VALUES		('RFQn3', Row_count());


								INSERT INTO	PONote
							(fkRFQOID, Note)
				SELECT		RFQOid, Comment

				FROM		TemplateApprovalComments

				WHERE		fkOperationTemplateID = OperationTemplateID;

				INSERT INTO spud			
							(Section, NewID)
				VALUES		('POnot3', Row_count());


								INSERT INTO	POComment
							(fkRFQOID, Comment, Deleted)
				SELECT		RFQOid, Comment, 0

				FROM		TemplatePOComments

				WHERE		fkOperationTemplateID = OperationTemplateID;

				INSERT INTO spud			
							(Section, NewID)
				VALUES		('POcom3', Row_count());


				
								INSERT INTO	OperationOrderItem
							(fkOperationID, fkItemID, AddedByfkUserID)
				VALUES		(OpStore, ItemStore, UserID);

				INSERT INTO spud			
							(Section, NewID)
				VALUES		('OpOrI3', Row_count());

								SET StdSuppID = (SELECT StdfkSupplierID
								FROM TemplateMaterial
								WHERE pkOperationTemplateID = OperationTemplateID);

				IF  StdSuppID IS NOT NULL THEN
						INSERT INTO	RFQOSupplier
									(fkRFQOID, fkSupplierID, Selected)
						SELECT		RFQOid, StdSuppID, 1

						FROM		TemplateTreatment

						WHERE		pkOperationTemplateID = OperationTemplateID;
				
				INSERT INTO spud			
							(Section, NewID)
				VALUES		('RFOSu3', Row_count());

				END IF;

		END IF;

		
				INSERT INTO	OperationProcess
					(fkOperationID, ProcessNote)
		SELECT		OpStore, ProcessNote

		FROM		TemplateProcess

		WHERE		fkOperationTemplateID = OperationTemplateID;

		
		INSERT INTO spud			
					(Section, NewID)
		VALUES		('OpPro', Row_count());

  END LOOP;

  CLOSE OpTmpltCursor;


END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `CreateOpsFromTemplate2` (IN `UserID` SMALLINT UNSIGNED, `NewWONo` MEDIUMINT UNSIGNED, `ETemplateNo` SMALLINT UNSIGNED)  BEGIN
DECLARE done 																		INT DEFAULT FALSE;
DECLARE CRiskStatement 																CHAR(100);
DECLARE OpStore 																	INT unsigned;
DECLARE OperationTemplateID, ItemStore 												mediumint unsigned;
DECLARE EngineeringTemplateID, RFQOid, StdSuppID									smallint unsigned;
DECLARE COperationNumber, COperationTypeID, MachiningID, CLinked, OrderItemNumber 	tinyint unsigned;
DECLARE Op, OpMat, RFQOrd, OrdIt, RFQno, POnot, POCom, OpOrdIt, RFQOSup, OpMac,
		OpMacProg, MachSet, OpTr, OpPro, RFQOSu										tinyint unsigned;
DECLARE CEstimatedSetTime, CEstimatedRunTime 										time;

DECLARE OpTmpltCursor CURSOR FOR 
		SELECT pkOperationTemplateID, fkEngineeringTemplateID, OperationNumber, fkOperationTypeID, fkMachiningID, 
				EstimatedSetTime, EstimatedRunTime, RiskStatement, Linked 

		FROM 	OperationTemplate
		
		WHERE	fkEngineeringTemplateID = ETemplateNo;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  OPEN OpTmpltCursor;

  read_loop: LOOP
		FETCH OpTmpltCursor INTO OperationTemplateID, EngineeringTemplateID, COperationNumber, COperationTypeID, MachiningID, 
								CEstimatedSetTime, CEstimatedRunTime, CRiskStatement, CLinked;

		IF done THEN
		  LEAVE read_loop;

		END IF;

		INSERT INTO	Operation
					(fkworkOrderID, OperationNumber, fkOperationTypeID, fkMachiningTypeID,
					EstimatedSetTime, EstimatedRunTime, RiskStatement, Linked)
		VALUES		(NewWONo, COperationNumber, COperationTypeID, MachiningID, 
					CEstimatedSetTime, CEstimatedRunTime, CRiskStatement, CLinked);
					
		SET 		OpStore = Last_Insert_ID();
		SET			Op = Row_count();

		IF COperationTypeID = 1 THEN

				INSERT INTO	OperationMaterial
							(pkOperationID, fkMaterialSpecID, Description, Size, fkShapeID, PartLength, fkApprovalID, 
							SupplyCondition, FreeIssue, UnitID, StdfkSupplierID, IssueInstruction, SPUfkUOMID, 
							SPU, Yield, TestPieces, StdDeliveryDays, AddedByfkUserID)
				SELECT		OpStore, fkMaterialSpecID, Description, Size, fkShapeID, PartLength, fkApprovalID, 
							SupplyCondition, FreeIssue, UnitID, StdfkSupplierID, IssueInstruction, SPUfkUOMID,
							UnitID, Yield, TestPieces, StdDeliveryDays, AddedByfkUserID

				FROM		TemplateMaterial

				WHERE		pkOperationTemplateID = OperationTemplateID;

				SET			OpMat = Row_count();
				
								IF (SELECT 	FreeIssue 
					FROM 	TemplateMaterial
					WHERE	pkOperationTemplateID = OperationTemplateID) IS NULL OR 
					(SELECT 	FreeIssue 
					FROM 	TemplateMaterial
					WHERE	pkOperationTemplateID = OperationTemplateID) <> 1 THEN
				
												INSERT INTO	RFQOrder
									(fkApprovalLevelID, fkUserID, CreateDate, LeadDays, Cancelled, Complete, POType)
						SELECT		fkApprovalID, UserID, Now(), StdDeliveryDays, 0, 0, 1

						FROM		TemplateMaterial

						WHERE		pkOperationTemplateID = OperationTemplateID;

						
						SET			RFQOid = Last_Insert_ID();
						SET			RFQOrd = Row_count();


												INSERT INTO	OrderItem
									(fkRFQOID, ItemNo, fkPartTemplateID, fkShapeID, SPU, fkUOMID, Quantity, Description,
									Size, UnitID, AddedByfkUserID)
						SELECT		RFQOid, 1,(SELECT fkPartTemplateID
																FROM EngineeringTemplate
																WHERE pkEngineeringTemplateID = ETemplateNo), 
									fkShapeID, 1, SPUfkUOMID, 
																	((SELECT QuantityReqd + QuantityOvers
										FROM WorkOrder
										WHERE pkWorkOrderID = NewWONo)
									*1.1 + (SELECT TestPieces
											FROM TemplateMaterial
											WHERE pkOperationTemplateID = OperationTemplateID)
									/ (SELECT Yield
										FROM TemplateMaterial
										WHERE pkOperationTemplateID = OperationTemplateID))

									, Description, Size, UnitID, UserID

						FROM		TemplateMaterial

						WHERE		pkOperationTemplateID = OperationTemplateID;

												SET ItemStore = Last_Insert_ID();
						SET	OrdIt = Row_count();
						
						
												INSERT INTO	RFQn
									(fkRFQOID, AddedByfkUserID)
						VALUES		(RFQOid, UserID);

						SET			RFQno = Row_count();


												INSERT INTO	PONote
									(fkRFQOID, Note)
						SELECT		RFQOid, Comment

						FROM		TemplateApprovalComments

						WHERE		fkOperationTemplateID = OperationTemplateID;

						SET			POnot = Row_count();


												INSERT INTO	POComment
									(fkRFQOID, Comment, Deleted)
						SELECT		RFQOid, Comment, 0

						FROM		TemplatePOComments

						WHERE		fkOperationTemplateID = OperationTemplateID;

						SET			POCom = Row_count();


						
												INSERT INTO	OperationOrderItem
									(fkOperationID, fkItemID, AddedByfkUserID)
						VALUES		(OpStore, ItemStore, UserID);

						SET			OpOrdIt = Row_count();


												SET StdSuppID = (SELECT StdfkSupplierID
										FROM TemplateMaterial
										WHERE pkOperationTemplateID = OperationTemplateID);

						IF  StdSuppID IS NOT NULL THEN
								INSERT INTO	RFQOSupplier
											(fkRFQOID, fkSupplierID, Selected)
								SELECT		RFQOid, StdSuppID, 1

								FROM		TemplateMaterial

								WHERE		pkOperationTemplateID = OperationTemplateID;

						SET			RFQOSup = Row_count();

						END IF;
				END IF;


		END IF;

		IF COperationTypeID = 2 THEN
				INSERT INTO	OperationMachine
							(fkOperationID, fkMachineID, Multi)
				SELECT		OpStore, fkMachineID, CLinked

				FROM		OpTmpltMachineProgram

				WHERE		fkOperationTemplateID = OperationTemplateID;

				SET			OpMac = Row_count();


								INSERT INTO	OpMachineProgram
							(fkOperationID, fkMachineID, fkProgramNumber, DateAdded)
				SELECT		OpStore, fkMachineID, ProgramNumber, DATE(Now())

				FROM		OpTmpltMachineProgram

				WHERE		fkOperationTemplateID = OperationTemplateID;

				SET			OpMacProg = Row_count();


												INSERT INTO	MachineSet
							(fkOperationID, fkMachineID)
				SELECT		(OpStore, fkMachineID)

				FROM		OpTmpltMachineProgram

				WHERE		fkOperationTemplateID = OperationTemplateID;

				SET			MachSet = Row_count();


		END IF;

		IF COperationTypeID = 3 THEN
				INSERT INTO	OperationTreatment
							(pkOperationID, fkTreatmentID, LeadTime, ApprovalComment, AddedByfkUserID)
				SELECT		OpStore, fkTreatmentID, LeadTime, ApprovalComment, UserID
				
				FROM		TemplateTreatment

				WHERE		pkOperationTemplateID = OperationTemplateID;

				SET			OpTr = Row_count();

												
								INSERT INTO	RFQOrder
							(fkUserID, CreateDate, LeadDays, Cancelled, Complete, POType)
				SELECT		UserID, Now(), LeadTime, 0, 0, 2

				FROM		TemplateTreatment

				WHERE		pkOperationTemplateID = OperationTemplateID;

				
				SET			RFQOid = Last_Insert_ID();
				SET			RFQOrd = Row_count();


								INSERT INTO	OrderItem
							(fkRFQOID, ItemNo, fkPartTemplateID, Quantity, Description, AddedByfkUserID)
				SELECT		RFQOid, 1,(SELECT fkPartTemplateID
														FROM EngineeringTemplate
														WHERE pkEngineeringTemplateID = ETemplateNo), 
														(SELECT QuantityReqd + QuantityOvers
								FROM WorkOrder
								WHERE pkWorkOrderID = NewWONo)
							*1.1
							, Description , UserID

				FROM		TemplateTreatment TT1
				INNER JOIN	TreatmentList TL1
				ON			TT1.fkTreatmentID = TL1.pkTreatmentID

				WHERE		pkOperationTemplateID = OperationTemplateID;

								SET ItemStore = Last_Insert_ID();
				SET OrdIt = Row_count();
				
				
								INSERT INTO	RFQn
							(fkRFQOID, AddedByfkUserID)
				VALUES		(RFQOid, UserID);

				SET			RFQno = Row_count();


								INSERT INTO	PONote
							(fkRFQOID, Note)
				SELECT		RFQOid, Comment

				FROM		TemplateApprovalComments

				WHERE		fkOperationTemplateID = OperationTemplateID;

				SET			POnot = Row_count();


								INSERT INTO	POComment
							(fkRFQOID, Comment, Deleted)
				SELECT		RFQOid, Comment, 0

				FROM		TemplatePOComments

				WHERE		fkOperationTemplateID = OperationTemplateID;

				SET			POCom = Row_count();


				
								INSERT INTO	OperationOrderItem
							(fkOperationID, fkItemID, AddedByfkUserID)
				VALUES		(OpStore, ItemStore, UserID);

				SET			OpOrdIt = Row_count();

								SET StdSuppID = (SELECT StdfkSupplierID
								FROM TemplateMaterial
								WHERE pkOperationTemplateID = OperationTemplateID);

				IF  StdSuppID IS NOT NULL THEN
						INSERT INTO	RFQOSupplier
									(fkRFQOID, fkSupplierID, Selected)
						SELECT		RFQOid, StdSuppID, 1

						FROM		TemplateTreatment

						WHERE		pkOperationTemplateID = OperationTemplateID;
				
				SET			RFQOSu = Row_count();

				END IF;

		END IF;

		
				INSERT INTO	OperationProcess
					(fkOperationID, ProcessNote)
		SELECT		OpStore, ProcessNote

		FROM		TemplateProcess

		WHERE		fkOperationTemplateID = OperationTemplateID;

		
		SET			OpPro = Row_count();

  END LOOP;

  CLOSE OpTmpltCursor;

SELECT CONCAT(Op, OpMat, RFQOrd, OrdIt, RFQno, POnot, POCom, OpOrdIt, RFQOSup, OpMac, OpMacProg, MachSet, OpTr, OpPro);

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `CreateWorkOrder` (IN `LineItem` MEDIUMINT UNSIGNED, `ApprovalID` TINYINT UNSIGNED, `QtyReqd` SMALLINT UNSIGNED, `QtyOvers` SMALLINT UNSIGNED, `PriceEach` DECIMAL(7,2), `DeliveryDate` DATE, `FAIR` TINYINT UNSIGNED, `Live` TINYINT UNSIGNED, OUT `NewWONo` MEDIUMINT UNSIGNED)  BEGIN
INSERT INTO			WorkOrder
					(fkLineItemID, fkApprovalID, QuantityReqd, QuantityOvers, PriceEach, OrigDeliveryDate, FAIR, Live)
VALUES				(LineItem, ApprovalID, QtyReqd, QtyOvers, PriceEach, DeliveryDate, FAIR, Live);

SELECT				Last_Insert_ID() INTO NewWONo;

CALL				ProcCreateOpsFromTemplate(NewWONo);

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `CustomerTest` ()  BEGIN
SELECT * FROM Customer;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `GetApprovalList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		pkApprovalID, Description
				
FROM			Approval
WHERE			Deleted <> 1;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `GetCustOrderNotes` (IN `OrderNo` INT)  BEGIN
SELECT			Note

FROM			CustomerOrderNote
WHERE			fkOrderID = Orderno;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `GetPOItemDetail` (IN `ItemNo` TINYINT UNSIGNED)  BEGIN
SET ItemNo = ItemNo -1;

SELECT		O.pkOperationID AS OperationID, O.OperationNumber AS OpNo, 
			(SELECT COUNT(*) FROM Operation WHERE fkWorkOrderID = O.fkWorkOrderID) AS TotalOps, 
			OT.OpType, OI.pkItemID AS ItemID, ItemNo, OM.Quantity AS Qty, OM.SPU, OM.SPUfkUOMID AS SPUID, OM.Size

FROM		OrderItem OI
LEFT JOIN	OperationOrderItem OOI
ON			OI.pkItemID = OOI.fkItemID
LEFT JOIN	Operation O
ON			OOI.fkOperationID = O.pkOperationID
LEFT JOIN	OperationType OT
ON			O.fkOperationTypeID = OT.pkOperationTypeID
LEFT JOIN	OperationMaterial OM
ON			O.pkOperationID = OM.pkOperationID

WHERE		OI.fkRFQOID = RFQOID
AND			pkItemID = (SELECT DISTINCT pkItemID 
						FROM OrderItem
						WHERE fkRFQOID = RFQOID
						GROUP BY pkItemID
						ORDER BY pkItemID
						LIMIT ItemNo,1)
;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `GetPreviousPurchases` (IN `PartNumber` VARCHAR(25))  BEGIN
SELECT		pkPOnID AS PONo, CreateDate AS OrderDate, S.SupplierName, OI.Quantity AS Qty, OP.OperationNumber AS OpNo, 
			IF(OM.UnitID = 1, "mm", 
				IF(OM.UnitID = 2, "inches", "")) AS Unit, PT.Description, OM.Size, SH.Description AS Shape, 
			OI.Price AS EachPrice, OI.Price * OI.Quantity AS OrderValue

FROM		PartTemplate	PT
INNER JOIN	Lineitem		LI
ON			PT.pkPartTemplateID = LI.fkPartTemplateID
INNER JOIN	WorkOrder 		WO
ON			LI.pkLineItemID = WO.fkLineItemID
INNER JOIN	Operation 		OP
ON			WO.pkWorkOrderID = OP.fkWorkOrderID
LEFT JOIN	OperationOrderItem OOI
ON			OP.pkOperationID = OOI.fkOperationID
INNER JOIN	OrderItem 		OI
ON			OOI.fkItemID = OI.pkItemID
INNER JOIN	Pon 			PON
ON			OI.fkRFQOID = PON.fkRFQOID
INNER JOIN	POInvoice 		POI
ON			Pon.pkPOnID = POI.fkPurchaseOrderID
INNER JOIN	Invoice 		I
ON			POI.fkInvoiceID = I.pkInvoiceID
INNER JOIN	RFQOSupplier	RS
ON			PON.fkRFQOID = RS.fkRFQOID
INNER JOIN	Supplier		S
ON			RS.fkSupplierID = S.pkSupplierID
INNER JOIN	OperationMaterial OM
ON			OP.pkOperationID = OM.pkOperationID
INNER JOIN	Shape			SH
ON			OM.fkShapeID = SH.pkShapeID


WHERE		PT.PartNumber = PartNumber;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `GetPreviousPurchases2` (IN `PartNumber` VARCHAR(25))  BEGIN
SELECT		pkPOnID AS PONo, CreateDate AS OrderDate, S.SupplierName, OI.Quantity AS Qty, OP.OperationNumber AS OpNo, 
			IF(OM.UnitID = 1, "mm", 
				IF(OM.UnitID = 2, "inches", "")) AS Unit, PT.Description, OM.Size, SH.Description AS Shape, 
			OI.Price AS EachPrice, OI.Price * OI.Quantity AS OrderValue

FROM		PartTemplate	PT
INNER JOIN	Lineitem		LI
ON			PT.pkPartTemplateID = LI.fkPartTemplateID
INNER JOIN	WorkOrder 		WO
ON			LI.pkLineItemID = WO.fkLineItemID
INNER JOIN	Operation 		OP
ON			WO.pkWorkOrderID = OP.fkWorkOrderID
LEFT JOIN	OperationOrderItem OOI
ON			OP.pkOperationID = OOI.fkOperationID
INNER JOIN	OrderItem 		OI
ON			OOI.fkItemID = OI.pkItemID
INNER JOIN	Pon 			PON
ON			OI.fkRFQOID = PON.fkRFQOID
INNER JOIN	POInvoice 		POI
ON			Pon.pkPOnID = POI.fkPurchaseOrderID
INNER JOIN	Invoice 		I
ON			POI.fkInvoiceID = I.pkInvoiceID
INNER JOIN	RFQOSupplier	RS
ON			PON.fkRFQOID = RS.fkRFQOID
INNER JOIN	Supplier		S
ON			RS.fkSupplierID = S.pkSupplierID
INNER JOIN	OperationMaterial OM
ON			OP.pkOperationID = OM.pkOperationID
INNER JOIN	Shape			SH
ON			OM.fkShapeID = SH.pkShapeID


WHERE		PT.PartNumber = PartNumber;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `GetPreviousPurchases3` (IN `PartNumber` VARCHAR(25))  BEGIN
SELECT		pkPOnID AS PONo, CreateDate AS OrderDate, S.SupplierName, OI.Quantity AS Qty, OP.OperationNumber AS OpNo, 
			IF(OM.UnitID = 1, "mm", 
				IF(OM.UnitID = 2, "inches", "")) AS Unit, PT.Description, OM.Size, SH.Description AS Shape, 
			OI.Price AS EachPrice, OI.Price * OI.Quantity AS OrderValue

FROM		PartTemplate	PT
LEFT JOIN	Lineitem		LI
ON			PT.pkPartTemplateID = LI.fkPartTemplateID
INNER JOIN	WorkOrder 		WO
ON			LI.pkLineItemID = WO.fkLineItemID
INNER JOIN	Operation 		OP
ON			WO.pkWorkOrderID = OP.fkWorkOrderID
LEFT JOIN	OperationOrderItem OOI
ON			OP.pkOperationID = OOI.fkOperationID
INNER JOIN	OrderItem 		OI
ON			OOI.fkItemID = OI.pkItemID
INNER JOIN	Pon 			PON
ON			OI.fkRFQOID = PON.fkRFQOID



INNER JOIN	RFQOSupplier	RS
ON			PON.fkRFQOID = RS.fkRFQOID
INNER JOIN	Supplier		S
ON			RS.fkSupplierID = S.pkSupplierID
INNER JOIN	OperationMaterial OM
ON			OP.pkOperationID = OM.pkOperationID
INNER JOIN	Shape			SH
ON			OM.fkShapeID = SH.pkShapeID


WHERE		PT.PartNumber = PartNumber;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `GetWODetail` (IN `workOrder` MEDIUMINT UNSIGNED)  BEGIN
SELECT	DISTINCT	pkPartTemplateID AS PartNumber, PT.IssueNumber AS Issue, D.Reference AS DrawingNumber, 
					D.IssueNumber AS DrgIssue, M.pkMachineID AS MachineID, CONCAT(M.Manufacturer, ': ', M.Model) AS Machine, 
					OM.fkMaterialID AS GRBID, 
					LI.fkOrderID AS OrderID, WO.pkWorkOrderID AS WorkOrderID, WO.QuantityReqd AS QtyOff, WO.FastTrack
					 

FROM			WorkOrder WO
INNER JOIN		LineItem LI
ON				WO.fkLineItemID = LI.pkLineItemID
INNER JOIN		Operation O
ON				WO.pkWorkOrderID = O.fkWorkOrderID
LEFT JOIN		OperationMaterial OM
ON				O.pkOperationID = OM.pkOperationID
LEFT JOIN		PartTemplate PT
ON				LI.fkPartTemplateID = PT.pkPartTemplateID
INNER JOIN		Drawing D
ON				PT.fkDrawingID = D.pkDrawingID
LEFT JOIN		OperationMachine OMA
ON				O.pkOperationID = OMA.fkOperationID
INNER JOIN		Machine M
ON				OMA.fkMachineID = M.pkMachineID
INNER JOIN		OperationMaterial OPM
ON				O.pkOperationID = OPM.pkOperationID

WHERE			WO.pkWorkOrderID = WorkOrder;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `GetWorkOrderPOs` (IN `WoNo` INT)  BEGIN
SELECT		PON.pkPOnID AS PONumber, RO.CreateDate, RO.Cancelled, I.Description, I.Quantity, I.Price,
			I.Comment,
			IF((SELECT		COUNT(*) 
			FROM 		Allocated
			WHERE		fkOperationID = OP.pkOperationID) < 1,
			(IF((SELECT		COUNT(*)
			FROM 		POGoods
			WHERE		POGoods.fkRFQOID = RO.pkRFQOID) < 1, 'Ordered', 'In stock')),
			'Issued') AS 'Status'
			
FROM		RFQOrder RO
LEFT JOIN	PON
ON			RO.pkRFQOID = PON.fkRFQOID
LEFT JOIN	Item I
ON			RO.pkRFQOID = I.fkRFQOID
LEFT JOIN	OperationPO 		OPO
ON			RO.pkRFQOID = 			OPO.fkRFQOID
LEFT JOIN	Operation 			OP
ON			OPO.fkOperationID = 	OP.pkOperationID
LEFT JOIN	WorkOrder 			WO
ON			OP.fkWorkOrderID = 		WO.pkWorkOrderID

WHERE		WO.pkWorkOrderID = WoNo
;	
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `PauseOpInspection` (IN `UserID` SMALLINT UNSIGNED, `InspectionID` INT UNSIGNED, `ReasonID` TINYINT UNSIGNED)  BEGIN
IF EXISTS	(SELECT StartTime 
			FROM InspectPause 
			WHERE fkInspectionID = InspectionID
			AND fkMachineID = MachineID) 
THEN
		UPDATE	InspectPause
	SET		EndTime = now(), fkUserIDEnd = UserID

	WHERE	fkInspectionID = InspectionID
	AND		fkMachineID = MachineID;
	
ELSE
		INSERT INTO	InspectPause
				(fkInspectionID, fkMachineID, StartTime, fkReasonID, fkUserIDStart)
VALUES			(InspectionID, MachineID, now(), ReasonID, UserID);
	
END IF; 

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `PauseOpInspection2` (IN `UserID` SMALLINT UNSIGNED, `InspectionID` INT UNSIGNED, `ReasonID` TINYINT UNSIGNED)  BEGIN
IF EXISTS	(SELECT StartTime 
			FROM InspectPause 
			WHERE fkInspectionID = InspectionID
			AND fkMachineID = MachineID) 
THEN
		UPDATE	InspectPause
	SET		EndTime = now(), fkUserIDEnd = UserID

	WHERE	fkInspectionID = InspectionID
	AND		fkMachineID = MachineID;
	
ELSE
		INSERT INTO	InspectPause
				(fkInspectionID, fkMachineID, StartTime, fkReasonID, fkUserIDStart)
VALUES			(InspectionID, MachineID, now(), ReasonID, UserID);
	
END IF; 

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAcknowledgementRpt` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN

SELECT		CONCAT('F.A.O. ',(SELECT ContactName 
			FROM DeliveryContact
			WHERE fkDeliveryPointID = CO.fkDeliveryPointID)) AS Name, 
			CONCAT('Fax No: ', Facsimile) AS Fax, CustomerName, 
			Address1, Address2, Address3, Address4, Postcode, CO.CustomerOrderRef AS OrderNo,
			Date(Now()) AS RcvdDate, ESLQuoteNumber, A.Description AS Approval, 
			IF(RiskAssessment = 1, 'Low', 
				IF(RiskAssessment = 2, 'Med', 
					IF(RiskAssessment = 3, 'High', 'unset'))) AS AnyRisks, 
			IF(ReviewStatus = 1, 'In process',
				IF(ReviewStatus = 2, 'Passed',
					IF(ReviewStatus = 3, 'Failed', 'unset'))) AS CRComplete,
			CustomerReference AS LineNo, QuantityReqd, PT.Description, D.Reference AS DrawingNo, 
			CONCAT('Issue ', D.IssueNumber) AS DrgIssue, PartNumber, 
			CONCAT('Issue ', PT.IssueNumber) AS PartIssue,
			PriceEach, OrigDeliveryDate, pkWorkOrderID AS OurRef, (PriceEach * QuantityReqd) AS Total, 
			AN.Note AS Comment

FROM			WorkOrder WO
INNER JOIN		LineItem LI
ON				WO.fkLineItemID = LI.pkLineItemID
LEFT JOIN		PartTemplate PT
ON				LI.fkParttemplateID = PT.pkPartTemplateID
LEFT JOIN		Drawing D
ON				PT.fkDrawingID = D.pkDrawingID
LEFT JOIN		Approval A
ON				WO.fkApprovalID = A.pkApprovalID
INNER JOIN		CustomerOrder CO
ON				LI.fkOrderID = CO.pkOrderID
INNER JOIN		Customer C
ON				CO.fkCustomerID = C.pkCustomerID

LEFT JOIN		AcknowledgementNote AN
ON				CO.pkOrderID = AN.fkOrderID			

WHERE			pkWorkOrderID = WorkOrderID;		
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddAcknowNote` (IN `UserID` SMALLINT UNSIGNED, `OrderID` MEDIUMINT UNSIGNED, `AcknowNote` VARCHAR(80))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		AcknowledgementNote
				(fkOrderID, Note, NoteDate)
VALUES			(OrderID, AcknowNote, DATE(Now()));

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddApproval` (IN `UserID` SMALLINT UNSIGNED, `Approval` VARCHAR(20))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO 	Approval 
				(Description, Deleted)
				
VALUES			(Approval, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddAssignedRole` (IN `UserID` SMALLINT UNSIGNED, `StaffUserID` SMALLINT UNSIGNED, `RoleID` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		UserDept
					(fkUserID, fkDeptID, AddedByfkUserID)
VALUES				(StaffUserID, RoleID, UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddBranch` (IN `UserID` SMALLINT UNSIGNED, `BranchName` VARCHAR(25), `Address1` VARCHAR(45), `Address2` VARCHAR(40), `Address3` VARCHAR(40), `Address4` VARCHAR(30), `Postcode` VARCHAR(8), `Phone` VARCHAR(20), `Fax` VARCHAR(20))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		BranchLocation
				(Name, Address1, Address2, Address3, Address4, Postcode, Phone, Fax, Deleted, AddedByfkUserID)
VALUES			(BranchName, Address1, Address2, Address3, Address4, Postcode, Phone, Fax, 0, UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddBusinessShutdown` (IN `UserID` SMALLINT UNSIGNED, `ShutDate` DATE, `ShutReason` VARCHAR(22))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		BusinessShutdown
				(ShutDate, ShutReason)

VALUES			(ShutDate, ShutReason);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddComment` (IN `UserID` SMALLINT UNSIGNED, `Comment` VARCHAR(100))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		Comments
				(Comments, fkUserID, Deleted)
VALUES			(Comment, UserID, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddCommentToPO` (IN `UserID` SMALLINT UNSIGNED, `RFQOID` SMALLINT UNSIGNED, `Comment` VARCHAR(100))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		POComment
				(fkRFQOID, Comment, Deleted)
VALUES			(RFQOID, Comment, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddCourier` (IN `CourierName` VARCHAR(25))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO 	Courier
				(Name, Deleted)
VALUES			(CourierName, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddCustDeliveryPoint` (IN `UserID` SMALLINT UNSIGNED, `CustomerID` SMALLINT UNSIGNED, `Address1` VARCHAR(45), `Address2` VARCHAR(40), `Address3` VARCHAR(40), `Address4` VARCHAR(30), `Postcode` CHAR(8))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		DeliveryPoint
				(fkCustomerID, Address1, Address2, Address3, Address4, Postcode)
VALUES			(CustomerID, Address1, Address2, Address3, Address4, Postcode);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddCustomerConformityNote` (IN `UserID` SMALLINT UNSIGNED, `CustomerID` SMALLINT UNSIGNED, `ConformityNote` VARCHAR(80))  BEGIN

INSERT INTO	StdConformityNote
				(fkCustomerID, Note, AddedByfkUserID)
VALUES			(CustomerID, ConformityNote, UserID);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddCustomerContact` (IN `UserID` SMALLINT UNSIGNED, `CustomerID` SMALLINT UNSIGNED, `ContactName` VARCHAR(45), `Position` VARCHAR(25), `Phone` VARCHAR(13), `Extension` SMALLINT UNSIGNED, `Email` VARCHAR(50), `AcknowContact` TINYINT UNSIGNED, `PrimaryContact` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		CustomerContact
				(fkCustomerID, ContactName, Position, Phone, Extension, Email, AcknowContact, PrimaryContact, fkUserIDUpdated) 
			
VALUES			(CustomerID, ContactName, Position, Phone, Extension, Email, AcknowContact, PrimaryContact, UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddCustomerNote` (IN `UserID` SMALLINT UNSIGNED, `CustomerID` SMALLINT UNSIGNED, `Note` VARCHAR(100))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		CustomerNote
				(fkCustomerID, Note, AddedByfkUserID, Deleted)
VALUES			(CustomerID, Note, UserID, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddCustOrderNote` (IN `UserID` SMALLINT UNSIGNED, `OrderID` MEDIUMINT UNSIGNED, `OrderNote` VARCHAR(80))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		CustomerOrderNote
				(fkOrderID, Note, NoteDate)
VALUES			(OrderID, OrderNote, DATE(Now()));

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddDeliveryContact` (IN `DeliveryPointID` SMALLINT UNSIGNED, `ContactName` VARCHAR(45), `Phone` VARCHAR(13))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		DeliveryContact
				(fkDeliveryPointID, ContactName, Phone) 
			
VALUES			(DeliveryPointID, ContactName, Phone);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddDeliveryPointContact` (IN `UserID` SMALLINT UNSIGNED, `DPID` SMALLINT UNSIGNED, `ContactName` VARCHAR(45), `Phone` VARCHAR(13))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		DeliveryContact
				(fkDeliveryPointID, ContactName, Phone) 
			
VALUES			(DPID, ContactName, Phone);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddDeviationReason` (IN `UserID` SMALLINT UNSIGNED, `Deviation` VARCHAR(20))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO 	DeviationType
				
				(DeviationType, Deleted)

VALUES			(Deviation, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddDocumentDropType` (IN `UserID` SMALLINT UNSIGNED, `DropType` VARCHAR(17))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO 		DocumentDropType 
					(DropType, Deleted)
				
VALUES				(DropType, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddEmploymentContract` (IN `StaffUserID` SMALLINT UNSIGNED, `FileName` VARCHAR(30))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
UPDATE			User
SET				ContractFileName = FileName
WHERE			pkUserID = StaffUserID;

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddFixture` (IN `LocationID` SMALLINT UNSIGNED, `GRBNumber` INT UNSIGNED, `FixtureNumber` CHAR, `Description` CHAR, `PartTemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		Kythera.Fixture 
				(fkLocationID, GRBNumber, FixtureNumber, Description, FixtureStatus, fkPartTemplateID) 
VALUES			(LocationID, GRBNumber, Fixturenumber, Description, 1, PartTemplateID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddGRB` (IN `UserID` SMALLINT UNSIGNED, IN `RcvdDate` DATE, IN `MaterialTypeID` TINYINT UNSIGNED, IN `OrderID` MEDIUMINT UNSIGNED, IN `LineItemID` MEDIUMINT UNSIGNED, IN `POnID` SMALLINT UNSIGNED, IN `ItemID` MEDIUMINT UNSIGNED, IN `ETemplateID` SMALLINT UNSIGNED, IN `WorkOrderNo` MEDIUMINT UNSIGNED, IN `ReleaseNoteNo` VARCHAR(15), IN `ID` SMALLINT UNSIGNED, IN `Type` CHAR(1), IN `Description` VARCHAR(80), IN `DeliveryNoteNo` VARCHAR(20), IN `PartNumber` VARCHAR(25), IN `QtyRcvd` SMALLINT UNSIGNED, IN `QtyManu` SMALLINT UNSIGNED, IN `Size` DECIMAL(5,2), IN `UOMID` TINYINT UNSIGNED, IN `ShapeID` TINYINT UNSIGNED, IN `StoreAreaID` TINYINT UNSIGNED, IN `XLocation` CHAR(1), IN `YLocation` TINYINT UNSIGNED, IN `DirectIssue` TINYINT UNSIGNED, IN `ApprovalTypeID` TINYINT UNSIGNED, IN `RejectNoteID` SMALLINT UNSIGNED, IN `FastTrack` TINYINT)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;

DECLARE GRBStore, OpStore				int unsigned;
DECLARE ETID, PTIDStore, RQ, OQ			smallint unsigned;
DECLARE PE								decimal(7,2) unsigned;
DECLARE DD								date;
DECLARE NewWOStore						mediumint;


IF	NOT ISNULL(ETemplateID) THEN
		SET		PTIDStore = (SELECT fkPartTemplateID 
							FROM EngineeringTemplate
							WHERE pkEngineeringTemplateID = ETemplateID);
END IF;


INSERT INTO		GoodsIn
				(fkPartID, fkSupplierID, fkCustomerID, ReceivedDate, IRNoteNumber, Description, DeliveryNoteNumber, AddedByfkUserID)
VALUES			(PTIDStore, IF(Type = 'S', ID, NULL), IF(Type = 'C', ID, NULL), RcvdDate, ReleaseNoteNo, Description,
				DeliveryNoteNo, UserID);

SET	GRBStore	= LAST_INSERT_ID();


CASE MaterialTypeID
WHEN 1 THEN
						INSERT INTO		Material
					(fkGRBID, fkMaterialTypeID, fkApprovalLevelID, fkShapeID, fkMaterialSpecID, 
																Description, fkUOMID, RcvdQuantity, Size, PONumber)
	VALUES			(GRBStore, MaterialTypeID, ApprovalTypeID, ShapeID, (SELECT DISTINCT fkMaterialSpecID
																			FROM Operation O1
																			INNER JOIN OperationMaterial OM1
																			ON O1.pkOperationID = OM1.pkOperationID
																			WHERE O1.fkWorkOrderID = WorkOrderNo),
																Description, UOMID, QtyRcvd, Size, POnID);

	INSERT INTO		MaterialLocation
					(fkMaterialID, fkStoreAreaID, fkXCoordID, fkYCoordID)
	VALUES			(GRBStore, StoreAreaID, (SELECT pkXCoordID
													FROM XCoordinate
													WHERE XCoord = YLocation
													AND fkStoreAreaID = StoreAreaID), (SELECT pkYCoordID
																						FROM YCoordinate
																						WHERE YCoord = XLocation
																						AND fkStoreAreaID = StoreAreaID));

				
	
		IF ItemID IS NOT NULL THEN
		
		INSERT INTO 		POGoods
						(fkGRBID, fkItemID)
		VALUES			(GRBStore, ItemID);


		SET 	OpStore	=	(SELECT fkOperationID 
								FROM OperationOrderItem
								WHERE fkItemID = ItemID);

	 			IF	(SELECT row_count() 
				FROM OperationOrderItem
				WHERE fkItemID = ItemID) THEN

						INSERT INTO 	Allocated 
										(fkOperationID, fkGRBID, Quantity, AddedByfkUserID, AllocateDate)
						VALUES			(OpStore, GRBStore, QtyRcvd, UserID, DATE(Now()));

 			END IF;

		IF (SELECT fkOperationTypeID FROM Operation WHERE pkOperationID = OpStore) = 3

		THEN				
			UPDATE Operation

				SET	EndQty = QtyRcvd

			WHERE	pkOperationID = OpStore;
		END IF;

	END IF;


WHEN 8 THEN
				INSERT INTO		Todo
						(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, PartNumber, Description, fkGRBID)
			VALUES		(Date(Now()), 4, UserID, 7, WorkOrderNo, PartNumber, "FI Material rcvd", GRBStore);
		
				IF WorkOrderNo 	IS NOT NULL THEN

		INSERT INTO 	Allocated
						(fkOperationID, fkGRBID, Quantity, AddedByfkUserID, AllocateDate)
			SELECT 		fkOperationID, GRBStore, QtyRcvd, UserID, DATE(now())

			FROM 		OperationOrderItem

			WHERE 		fkItemID = ItemID;

		END IF;

		IF FastTrack = 1 THEN
												INSERT INTO		Todo
								(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, PartNumber, Description, fkGRBID)
					VALUES		(Date(Now()), 4, UserID, 7, WorkOrderNo, PartNumber, "Fasttrack initiated", GRBStore);


				INSERT INTO		Todo
								(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, PartNumber, Description, fkGRBID)
					VALUES		(Date(Now()), 2, UserID, 2, WorkOrderNo, PartNumber, "Issue Material", GRBStore);


				INSERT INTO		CustomerOrder
								(fkCustomerID, CustomerOrderRef, CreationDate, CreationfkUserID, fkApprovalID, fkDeliveryPointID,
								CommerciallyViable, AmendmentReviewed)
					SELECT		fkCustomerID, CONCAT(LEFT(CustomerOrderRef,22), ' FT'), Now(), 22, fkApprovalID, fkDeliveryPointID,
								CommerciallyViable, AmendmentReviewed

					FROM		CustomerOrder

					WHERE		pkOrderID = OrderID;


				INSERT INTO		LineItem
								(fkOrderID, fkPartTemplateID, fkEngineeringTemplateID, CustomerReference,
								ESLQuoteNumber)
					VALUES		(Last_Insert_ID(), PTIDStore, ETemplateID, 'FT', 0);

								SELECT 			PE= PriceEach, DD= IFNULL(NewDeliveryDate,OrigDeliveryDate), RQ= QuantityReqd, OQ= QuantityOvers

					FROM		WorkOrder

					WHERE		pkWorkOrderID = WorkOrderNo;

								CALL 			ProcAddNewWO(255, Last_Insert_ID(), PE, DD, RQ, OQ, NULL, 0, @NewWONo);
				
					SET 		NewWOStore = (SELECT @NewWONo);

					SELECT 		NewWOStore;
								UPDATE			WorkOrder

					SET			Live = 1

					WHERE		pkWorkOrderID = NewWOStore;


				INSERT INTO		Todo
								(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, PartNumber, Description, fkGRBID)
					VALUES		(Date(Now()), 21, UserID, 1, WorkOrderNo, PartNumber, "New WO auto-scheduled", GRBStore);

				
		END IF;

WHEN 7 THEN

				UPDATE 	WorkOrder
		SET		Complete = 1

		WHERE	pkWorkOrderID = WorkOrderNo;

				INSERT INTO Material
					(fkGRBID, RcvdQuantity)
			VALUES	(GRBStore, QtyRcvd);

UPDATE GoodsIn
SET WorkORderID = WorkOrderNo
WHERE pkGRBID = GRBStore;

				INSERT INTO		Todo
						(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, PartNumber, Description, fkGRBID)
			VALUES		(Date(Now()), 22, UserID, 2, WorkOrderNo, PartNumber, "Dispatch parts", GRBStore);

END CASE;	



IF MaterialTypeID <> 7
THEN

				INSERT INTO		Todo
						(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, PartNumber, Description, fkGRBID)
			VALUES		(Date(Now()), 1, UserID, 5, WorkOrderNo, PartNumber, "Inspect Goods received", GRBStore);

END IF;

SELECT			GRBStore;


END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddGRBInvoice` (IN `UserID` SMALLINT UNSIGNED, `InvoiceID` INT UNSIGNED, `GRBID` INT UNSIGNED)  BEGIN

DECLARE 		Result		tinyint DEFAULT -1;

INSERT INTO		InvoiceGoods
  				(fkInvoiceID, fkGRBID)
VALUES			(InvoiceID, GRBID);

INSERT INTO		POInvoice
 				(fkInvoiceID, fkItemID, AddedByfkUserID)
VALUES			(InvoiceID, (SELECT MIN(fkItemID) FROM POGoods WHERE fkGRBID = GRBID), UserID); 


SELECT			Row_Count() INTO Result;

SELECT 			Result;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddIntMaterialNote` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED, `Note` VARCHAR(80))  BEGIN
INSERT INTO		OperationNote
				(fkOperationID, Note, fkNoteTypeID, CreateDate, AddedByfkUserID)
VALUES			((SELECT pkOperationID
					FROM Operation
					WHERE fkWorkOrderID = WorkOrderID
					AND OperationNumber = 1), Note, 1, Date(Now()), UserID); 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddItem` (IN `UserID` SMALLINT UNSIGNED, `RFQOID` SMALLINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE 		ItemStore tinyint unsigned DEFAULT 0;
SET ItemStore = (SELECT	Count(*) FROM OrderItem WHERE fkRFQOID = RFQOID) +1;

INSERT INTO		OrderItem
				(fkRFQOID, ItemNo, AddedByfkUserID)
VALUES			(RFQOID, ItemStore, UserID); 

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddMachineService` (IN `UserID` SMALLINT UNSIGNED, `MachineID` TINYINT UNSIGNED, `StartDate` DATE, `EndDate` DATE)  BEGIN

UPDATE			Machine
SET				ServiceStart = StartDate, ServiceEnd = EndDate, ServicefkUserID = UserID

WHERE			pkMachineID = MachineID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddMaterial` (IN `MaterialID` INT UNSIGNED, IN `OperationID` INT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
Insert INTO		Kythera.OperationMaterial 
				(fkOperationID, fkMaterialID) 
VALUES			(OperationID, MaterialID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddMaterialShape` (IN `UserID` SMALLINT UNSIGNED, `Shape` VARCHAR(15), `ReqdDesc` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		Shape
				(Description, Deleted, ReqdDesc)

VALUES			(Shape, 0, ReqdDesc);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddMaterialSpec` (IN `UserID` SMALLINT UNSIGNED, `MaterialSpec` CHAR(6))  BEGIN
INSERT INTO		MaterialSpec
				(MaterialSpec, AddedByfkUserID)
VALUES			(MaterialSpec, UserID);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddNCRDoc` (IN `UserID` SMALLINT UNSIGNED, `FileName` VARCHAR(20), `RejectNoteID` SMALLINT UNSIGNED)  BEGIN
UPDATE		RejectGRB
SET			NCRFileName = FileName, DroppedByfkUserID = UserID

WHERE		pkRejectID = RejectNoteID;	
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddNewCO` (IN `UserID` SMALLINT UNSIGNED, `CustomerID` SMALLINT UNSIGNED, `OrderRef` VARCHAR(25), `ReleaseTypeID` TINYINT UNSIGNED, `DeliveryPointID` SMALLINT UNSIGNED, `CommerciallyViable` TINYINT UNSIGNED, `AmendmentReviewed` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO	CustomerOrder
			(fKCustomerID, CustomerOrderRef, CreationDate, CreationfkUserID, fkApprovalID, fkDeliveryPointID,
			CommerciallyViable, AmendmentReviewed)

VALUES		(CustomerID, OrderRef, Date(now()), UserID, ReleaseTypeID, DeliveryPointID, CommerciallyViable, AmendmentReviewed);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddNewCustomer` (IN `UserID` SMALLINT UNSIGNED, `CustomerSageCode` CHAR(3), `CustomerName` VARCHAR(50), `Alias` VARCHAR(30), `Address1` VARCHAR(45), `Address2` VARCHAR(40), `Address3` VARCHAR(40), `Address4` VARCHAR(40), `Postcode` CHAR(8), `Phone` VARCHAR(20), `Facsimile` VARCHAR(20), `Website` VARCHAR(40), `Email` VARCHAR(50), `InvAddress1` VARCHAR(40), `InvAddress2` VARCHAR(40), `InvAddress3` VARCHAR(40), `InvAddress4` VARCHAR(40), `InvPostcode` CHAR(8), `FDApproval` TINYINT UNSIGNED, `MDApproval` TINYINT UNSIGNED, `GMApproval` TINYINT UNSIGNED, `CreditLimit` DECIMAL(6,0), `MarkUpQuote` TINYINT UNSIGNED, `PaymentTerms` TINYINT UNSIGNED, `PaymentRunFrequency` TINYINT UNSIGNED, `fkApprovalID` TINYINT UNSIGNED, `DeliveryCharge` DECIMAL(3,0), `IrishCurrency` TINYINT UNSIGNED, `USCurrency` TINYINT UNSIGNED, `EuroCurrency` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		Customer
				(CustomerSageCode, CustomerName, Alias, Address1, Address2, Address3,
				Address4, Postcode, Phone, Facsimile, Website, Email, InvAddress1,
				InvAddress2, InvAddress3, InvAddress4, InvPostcode, FDApproval,
				MDApproval, GMApproval, CreditLimit, MarkUpQuote, PaymentTerms,
				PaymentRunFrequency, fkApprovalID, DeliveryCharge, IrishCurrency,
				USCurrency, EuroCurrency)

VALUES			(CustomerSageCode, CustomerName, Alias, Address1, Address2, Address3,
				Address4, Postcode, Phone, Facsimile, Website, Email, InvAddress1,
				InvAddress2, InvAddress3, InvAddress4, InvPostcode, FDApproval,
				MDApproval, GMApproval, CreditLimit, MarkUpQuote, PaymentTerms,
				PaymentRunFrequency, fkApprovalID, DeliveryCharge, IrishCurrency,
				USCurrency, EuroCurrency);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddNewEngTemplate` (IN `UserID` SMALLINT UNSIGNED, `PTemplateID` SMALLINT UNSIGNED, `SupplyConditionID` TINYINT UNSIGNED)  BEGIN
INSERT INTO		EngineeringTemplate
				(fkSupplyConditionID, fkPartTemplateID, AddedByfkUserID)
VALUES			(SupplyConditionID, PTemplateID, UserID);

SELECT			LAST_INSERT_ID() AS ETemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddNewLineItem` (IN `UserID` SMALLINT UNSIGNED, `OrderID` MEDIUMINT UNSIGNED, `CustRef` VARCHAR(5), `QuoteNo` MEDIUMINT UNSIGNED, `PTemplateID` SMALLINT UNSIGNED, `ETemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		LineItem
				(fkOrderID, fkPartTemplateID, fkEngineeringTemplateID, CustomerReference,
				ESLQuoteNumber)
VALUES			(OrderID, PTemplateID, ETemplateID, CustRef, QuoteNo);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddNewMachine` (IN `Manufacturer` VARCHAR(15), `Model` VARCHAR(25), `Trn` TINYINT UNSIGNED, `Grd` TINYINT UNSIGNED, `Mil` TINYINT UNSIGNED, `GC` TINYINT UNSIGNED, `Oth` TINYINT UNSIGNED, `Offset` TINYINT UNSIGNED, `StationNo` TINYINT UNSIGNED, `MinStickOut` TINYINT UNSIGNED, `Designation` VARCHAR(15), `HolderNo` VARCHAR(15), `Diameter` SMALLINT UNSIGNED, `InsertDatumPoint` VARCHAR(15), `MinFluteLength` SMALLINT UNSIGNED, `Spindle` VARCHAR(10), `DifficultyLevel` TINYINT UNSIGNED, `CoolantType` VARCHAR(15), `TargetConcMin` TINYINT UNSIGNED, `TargetConcMax` TINYINT UNSIGNED, `SlideOilLevel` TINYINT UNSIGNED, `CoolantLevel` TINYINT UNSIGNED, `GreaseLevel` TINYINT UNSIGNED, `FanFilter` TINYINT UNSIGNED, `OilFilter` TINYINT, `CoolantFilter` TINYINT, `AirSystem` TINYINT, `IntegralLighting` TINYINT UNSIGNED, `InterlocksEngaged` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

INSERT INTO		Machine
				(Manufacturer, Model, ChuckDiameter, SkillOffset)
VALUES			(Manufacturer, Model, Diameter, DifficultyLevel);

INSERT INTO		MachineMaintParam
				(pkMachineID, StationNo, MinStickOut, Designation, HolderNo, 
				InsertDatumPoint, MinfluteLength,
				Spindle, CoolantType, TargetConcMin, TargetConcMax, 
				SlideOilLevel, CoolantLevel,
				GreaseLevel, Fanfilter, OilFilter, CoolantFilter, 
				AirSystem, IntegralLighting, 
				InterlocksEngaged, Offset, Turn, Grind, Mill, GearCut, Other)
VALUES			(LAST_INSERT_ID(), StationNo, MinStickOut, Designation, HolderNo, 
				InsertDatumPoint, MinfluteLength,
				Spindle, CoolantType, TargetConcMin, TargetConcMax, 
				SlideOilLevel, CoolantLevel,
				GreaseLevel, Fanfilter, OilFilter, CoolantFilter, 
				AirSystem, IntegralLighting, 
				InterlocksEngaged, Offset, Trn ,Grd, Mil, GC, Oth);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddNewMachiningType` (IN `Description` VARCHAR(25))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		MachiningType
				(Description, Deleted)
VALUES			(Description, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddNewPartTemplate` (IN `UserID` SMALLINT UNSIGNED, `PartNumber` VARCHAR(25), `PartIssue` CHAR(5), `Description` VARCHAR(40), `LeadDays` TINYINT UNSIGNED, `Price` DECIMAL(7,2), `DrawingNumber` VARCHAR(25), `Issue` CHAR(5), `ApprovalID` TINYINT UNSIGNED, `AssemblyQty` TINYINT UNSIGNED, `PartTemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

IF		ISNULL(DrawingNumber) THEN

		INSERT INTO		PartTemplate
						(fkParentPartTemplateID, QtyToParent, fkDrawingID, PartNumber, IssueNumber, Description, OnAlert, LockedDown,
							AddedByfkUserID, StdLeadDays, StdPrice, StdApprovalID)
		VALUES			(PartTemplateID, AssemblyQty, NULL, PartNumber, PartIssue, Description, 0, 0, UserID, LeadDays, Price, ApprovalID);
ELSE

		INSERT INTO		PartTemplate
						(fkParentPartTemplateID, QtyToParent, fkDrawingID, PartNumber, IssueNumber, Description, OnAlert, LockedDown,
							AddedByfkUserID, StdLeadDays, StdPrice, StdApprovalID)
		VALUES			(PartTemplateID, AssemblyQty, (SELECT pkDrawingID FROM Drawing WHERE Reference = DrawingNumber AND IssueNumber = Issue),
																			PartNumber, PartIssue, Description, 0, 0, UserID, LeadDays, Price, ApprovalID);

END IF;

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddNewPauseReason` (IN `Reason` VARCHAR(10))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		PauseReason
				(Description, Deleted)
VALUES			(Reason, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddNewRFQ` (IN `UserID` SMALLINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		RFQOrder
				(fkUserID, CreateDate, Complete)
VALUES			(UserID, DATE(now()), 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID() AS Last_Insert_ID;
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddNewStaff` (IN `UserID` SMALLINT UNSIGNED, `FirstName` VARCHAR(25), `LastName` VARCHAR(25), `Address1` VARCHAR(45), `Address2` VARCHAR(40), `Address3` VARCHAR(40), `Address4` VARCHAR(30), `Postcode` CHAR(8), `HomePhone` VARCHAR(20), `MobilePhone` VARCHAR(20), `Email` VARCHAR(50), `ChargeRate` DECIMAL(5,2) UNSIGNED, `SkillLevel` TINYINT UNSIGNED, `Employed` TINYINT UNSIGNED, `Extension` TINYINT UNSIGNED, `ReportsTo` SMALLINT UNSIGNED, `POLimit` SMALLINT UNSIGNED, `SpendLimit` SMALLINT UNSIGNED, `ForkOperator` TINYINT UNSIGNED, `FirstAid` TINYINT UNSIGNED, `LeaveDays` TINYINT UNSIGNED, `Salary` SMALLINT UNSIGNED, `Hours` TINYINT UNSIGNED, `Security` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;

INSERT INTO			User
					(UName, FirstName, LastName, Address1, Address2, Address3, Address4, Postcode, HomePhone,
					MobilePhone, Email, ChargeRate, SkillLevel, Employed, Extension, ReportsTo, POLimit, SpendLimit,
					ForkOperator, FirstAid, LeaveDays, Salary, Hours, Security)
VALUES				(CONCAT(LEFT(FirstName,1), LEFT(LastName,9)), FirstName, LastName, Address1, Address2, Address3, 
					Address4, Postcode, HomePhone, MobilePhone, Email, ChargeRate, SkillLevel, Employed, Extension, 
					ReportsTo, POLimit, SpendLimit, ForkOperator, FirstAid, LeaveDays, Salary, Hours, Security);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddNewSundry` (IN `Description` VARCHAR(20))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		Sundry
				(Sundry, Deleted)
VALUES			(Description, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddNewSupplier` (IN `UserID` SMALLINT UNSIGNED, `Supplier` VARCHAR(50), `Alias` VARCHAR(40), `Address1` VARCHAR(45), `Address2` VARCHAR(40), `Address3` VARCHAR(40), `Address4` VARCHAR(30), `Postcode` CHAR(8), `Phone` VARCHAR(20), `Facsimile` VARCHAR(20), `Website` VARCHAR(40), `Email` VARCHAR(50), `MinOrderCharge` SMALLINT UNSIGNED, `PaymentTerms` TINYINT UNSIGNED, `MaterialSupplier` TINYINT UNSIGNED, `TreatmentSupplier` TINYINT UNSIGNED, `CalibrationSupplier` TINYINT UNSIGNED, `ToolingSupplier` TINYINT UNSIGNED, `MiscSupplier` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO	Supplier
				(SupplierName, Alias, Address1, Address2, Address3, Address4, Postcode, Phone,
				Facsimile, Website, Email, MinOrderCharge, PaymentTerms, MaterialSupplier,
				TreatmentSupplier, CalibrationSupplier, ToolingSupplier, MiscSupplier, fkUserID, Deleted)
VALUES			(Supplier, Alias, Address1, Address2, Address3, Address4, Postcode, Phone,
				Facsimile, Website, Email, MinOrderCharge, PaymentTerms, MaterialSupplier,
				TreatmentSupplier, CalibrationSupplier, ToolingSupplier, MiscSupplier, UserID, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddNewSupplyCondition` (IN `Description` VARCHAR(20))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		SupplyCondition
				(Description, Deleted)
VALUES			(Description, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddNewTreatment` (IN `Description` VARCHAR(20))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		TreatmentList
				(Description, Deleted)
VALUES			(Description, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddNewWO` (IN `UserID` SMALLINT UNSIGNED, `LineItemID` MEDIUMINT UNSIGNED, `PriceEach` DECIMAL(7,2) UNSIGNED, `DeliveryDate` DATE, `Reqd` SMALLINT UNSIGNED, `Build` SMALLINT UNSIGNED, `ChangeReason` VARCHAR(30), `FAIR` TINYINT UNSIGNED, INOUT `NewWONo` MEDIUMINT UNSIGNED)  BEGIN
DECLARE		Resulta					tinyint 			DEFAULT -1;
DECLARE		ETemplateNo				smallint unsigned 	DEFAULT 0;
DECLARE		Overs 					smallint;
DECLARE		PriceStore				decimal(7,2);
DECLARE		SetSeconds, RunSeconds	bigint		unsigned		DEFAULT 0;
DECLARE		EstDelDate				date;
DECLARE 	done, Workingdays 		INT 				DEFAULT FALSE;

DECLARE 	Count 					INT;
DECLARE 	i 						INT;
DECLARE 	NewDate 				DATETIME;
    

DECLARE CEstimatedSetTime, CEstimatedRunTime 	time;

DECLARE OpTmpltCursor2 CURSOR FOR 
		SELECT 	EstimatedSetTime, EstimatedRunTime

		FROM 	OperationTemplate
		
		WHERE	fkEngineeringTemplateID = (SELECT fkEngineeringTemplateID FROM LineItem WHERE pkLineItemID = LineItemID);

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN OpTmpltCursor2;

	read_loop: LOOP
		FETCH OpTmpltCursor2 INTO CEstimatedSetTime, CEstimatedRunTime;

		IF done THEN
			LEAVE read_loop;

		END IF;

		SET		SetSeconds 	= SetSeconds + TIME_TO_SEC(CEstimatedSetTime);
		SET		RunSeconds  = RunSeconds + TIME_TO_SEC(CEstimatedRunTime) * Build;

	END LOOP;

CLOSE OpTmpltCursor2;

SET		ETemplateNo = (SELECT fkEngineeringTemplateID FROM LineItem WHERE pkLineItemID = LineItemID);
SET		WorkingDays = ((SetSeconds + RunSeconds) / 86400) + IFNULL((SELECT StdLeadDays 
															FROM	PartTemplate
															WHERE	pkPartTemplateID = (SELECT fkPartTemplateID
																						FROM LineItem
																						WHERE pkLineItemID = LineItemID)),0);

SET 	Count = 0;
SET 	i = 0;

WHILE (i < WorkingDays) DO
	BEGIN
			SELECT Count + 1 INTO Count;
            SELECT i + 1 INTO i;
            WHILE DAYOFWEEK(DATE_ADD(now(),INTERVAL Count DAY)) IN (1,7) DO
                BEGIN
                    SELECT Count + 1 INTO Count;
                END;
            END WHILE;
	END;
END WHILE;

SET 	EstDelDate = DATE_ADD(now(), INTERVAL Count DAY);


IF 		ISNULL(DeliveryDate) 

THEN 
		SELECT 	Resulta;
ELSE

		SET 	Overs = Build - Reqd;
		SET		ETemplateNo = (SELECT fkEngineeringTemplateID FROM LineItem WHERE pkLineItemID = LineItemID);
		SET		PriceStore  = (SELECT StdPrice FROM PartTemplate WHERE pkPartTemplateID = (SELECT fkPartTemplateID 
																							FROM LineItem 
																							WHERE pkLineItemID = LineItemID));

						
		INSERT INTO	WorkOrder
					(fkLineItemID, fkApprovalID, Complete, QuantityReqd, QuantityOvers, PriceEach, OrigDeliveryDate,
					ChangeReason, FAIR, ReviewStatus, RiskAssessment, Live, CreatedByfkUserID,
					Tooling, QAEquipment, CorrectApproval)
		SELECT		LineItemID, (SELECT fkApprovalID FROM LineItem LI 
								INNER JOIN CustomerOrder CO ON LI.fkOrderID = CO.pkOrderID 
								WHERE LI.pkLineItemID = LineItemID),
					0, Reqd, Overs, IFNULL(PriceStore, PriceEach), IFNULL(EstDelDate, DeliveryDate), ChangeReason, 
					FAIR, ReviewStatus, RiskAssessment, 0, UserID, Tooling, QAEquipment, CorrectApproval

		FROM		EngineeringTemplate

		WHERE		pkEngineeringTemplateID = ETemplateNo;

		SELECT		Last_Insert_ID() INTO NewWONo;
		SELECT		Row_Count() INTO Resulta;

		CALL		ProcCreateOpsFromTemplate(UserID, NewWONo, ETemplateNo);

END IF;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOp` (IN `EngTmpltID` INT, IN `OpTypeID` INT, IN `MachiningID` INT, IN `OPNo` INT, IN `SetTime` DEC, IN `RunTime` DEC)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		OperationTemplate
				(fkEngineeringTemplateID, fkOperationTypeID, MachiningID, OperationNumber, SetTime, RunTime)
VALUES			(EngTmpltID, OpTypeID, MachiningID, OPNo, SetTime, RunTime);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpConformityNotes` (IN `OperationID` INT, IN `Note` CHAR)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		OperationNote
				(fkOperationID, Note, fkNoteTypeID)
VALUES			(OperationID, Note, 2);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpEngineeringNotes` (IN `OperationID` INT, IN `Note` CHAR)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		OperationNote
				(fkOperationID, Note, fkNoteTypeID)
VALUES			(OperationID, Note, 3);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOperation` (IN `OperationID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOperatorNote` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `Note` VARCHAR(50))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		OperationNote
				(fkOperationID, Note, fkNoteTypeID, CreateDate, AddedByfkUserID)
VALUES			(OperationID, Note, 8, Date(Now()), UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID() AS LastInsertID;
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpInspectNotes` (IN `OperationID` INT UNSIGNED, `Note` CHAR)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		OperationNote
				(fkOperationID, Note, fkNoteTypeID)
VALUES			(OperationID, Note, 7);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpIntMaterialNotes` (IN `OperationID` INT, IN `Note` CHAR)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		OperationNote
				(fkOperationID, Note, fkNoteTypeID)
VALUES			(OperationID, Note, 3);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpIntTreatNotes` (IN `OperationID` INT, IN `Note` CHAR)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		OperationNote
				(fkOperationID, Note, fkNoteTypeID)
VALUES			(OperationID, Note, 6);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpMaterialNotes` (IN `OperationID` INT, IN `Note` CHAR)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		OperationNote
				(fkOperationID, Note, fkNoteTypeID)
VALUES			(OperationID, Note, 1);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpTimes` (IN `WorkOrderID` INT, IN `OperationID` INT, IN `SetTime` DEC, IN `RunTime` DEC)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE			Operation
SET				SetTime = SetTime, RunTime = RunTime
WHERE			fkWorkOrderID = WorkOrderID
	AND			pkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpTmpltConformityNotes` (IN `OperationTemplateID` INT, IN `Note` CHAR)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		TemplateNote
				(fkOperationTemplateID, Note, fkNoteTypeID)
VALUES			(OperationID, Note, 2);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpTmpltEngineeringNotes` (IN `OperationTemplateID` INT, IN `Note` CHAR)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		TemplateNote
				(fkOperationTemplateID, Note, fkNoteTypeID)
VALUES			(OperationID, Note, 4);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpTmpltInspectNotes` (IN `OperationTemplateID` INT, IN `Note` CHAR)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		TemplateNote
				(fkOperationTemplateID, Note, fkNoteTypeID)
VALUES			(OperationID, Note, 7);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpTmpltIntEngNotes` (IN `UserID` SMALLINT UNSIGNED, `OperationTemplateID` MEDIUMINT UNSIGNED, IN `Note` VARCHAR(80))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		TemplateNote
				(fkOperationTemplateID, Note, fkNoteTypeID, CreateDate, AddedByfkUserID)
VALUES			(OperationTemplateID, Note, 4, DATE(Now()), UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpTmpltIntMaterialNotes` (IN `UserID` SMALLINT UNSIGNED, `OperationTemplateID` MEDIUMINT UNSIGNED, `Note` VARCHAR(80))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		TemplateNote
				(fkOperationTemplateID, Note, fkNoteTypeID, CreateDate, AddedByfkUserID)
VALUES			(OperationTemplateID, Note, 1, Date(Now()), UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpTmpltIntReviewNote` (IN `UserID` SMALLINT UNSIGNED, `OperationTemplateID` MEDIUMINT UNSIGNED, `Note` VARCHAR(80))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		TemplateNote
				(fkOperationTemplateID, Note, fknoteTypeID, CreateDate, AddedByfkUserID)
VALUES			(OperationTemplateID, Note, 9, DATE(Now()), UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpTmpltIntTreatNotes` (IN `UserID` SMALLINT UNSIGNED, `OperationTemplateID` MEDIUMINT UNSIGNED, `Note` VARCHAR(80))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		TemplateNote

				(fkOperationTemplateID, Note, fkNoteTypeID, CreateDate, AddedByfkUserID)
VALUES			(OperationTemplateID, Note, 6, DATE(Now()), UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpTmpltMaterialNotes` (IN `UserID` SMALLINT UNSIGNED, `OperationTemplateID` MEDIUMINT UNSIGNED, `Note` VARCHAR(80))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		TemplateNote
				(fkOperationTemplateID, Note, fkNoteTypeID, CreateDate, AddedByfkUserID)
VALUES			(OperationTemplateID, Note, 1, Date(Now()), UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpTmpltSubconDetail` (IN `UserID` SMALLINT UNSIGNED, `OperationTemplateID` MEDIUMINT UNSIGNED, `TreatmentID` TINYINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `LeadTime` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

INSERT INTO			TemplateTreatment
					(pkOperationTemplateID, fkTreatmentID, LeadTime, StandardSupplierID, AddedByfkUserID)
VALUES				(OperationTemplateID, TreatmentID, LeadTime, SupplierID, UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpTmpltTreatmentNotes` (IN `OperationTemplateID` INT, IN `Note` CHAR)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		TemplateNote
				(fkOperationTemplateID, Note, fkNoteTypeID)
VALUES			(OperationID, Note, 5);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpTreatmentNotes` (IN `OperationID` INT, IN `Note` CHAR)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		OperationNote
				(fkOperationID, Note, fkNoteTypeID)
VALUES			(OperationID, Note, 5);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOpWOTmpltIntMaterialNotes` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `Note` VARCHAR(80))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		OperationNote
				(fkOperationID, Note, fkNoteTypeID, CreateDate, AddedByfkUserID)
VALUES			(OperationID, Note, 1, Date(Now()), UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
 	SELECT		Result;
 ELSE			
	SELECT		last_Insert_ID();
END IF;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOrderDoc` (IN `UserID` SMALLINT UNSIGNED, `OrderID` MEDIUMINT UNSIGNED, `FileName` VARCHAR(30))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		OrderDoc
				(fkOrderID, AddedByfkUserID, DateAdded, FileHandle)
VALUES			(OrderID, UserID, Now(), FileName);	

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddorderProgress` (IN `UserID` SMALLINT UNSIGNED, `RFQOID` SMALLINT UNSIGNED, `ContactType` TINYINT UNSIGNED, `Progress` VARCHAR(50), `Chase` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO	RFQProgress		
				(fkRFQOID, fkUserID, ContactTypeID, ProgressNote, CreateDate, Chase)
VALUES			(RFQOID, UserID, ContactType, Progress, Now(), Chase);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddOurApproval` (IN `UserID` SMALLINT UNSIGNED, `OurApproval` VARCHAR(20))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO 		OurApproval 
					(Approval, Deleted)
				
VALUES				(OurApproval, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddPartDoc` (IN `PTemplateID` SMALLINT UNSIGNED, `UserID` SMALLINT UNSIGNED, `DocTypeID` TINYINT UNSIGNED, `FileName` VARCHAR(30), `reference` VARCHAR(25), `Issue` VARCHAR(5))  BEGIN
DECLARE			Result				tinyint DEFAULT -1;
DECLARE			CurrentDrgID		smallint unsigned;

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

SET		CurrentDrgID =
		(SELECT		fkDrawingID 
		FROM 		PartTemplate
		WHERE		pkPartTemplateID = PTemplateID);


IF	DoctypeID = 4	THEN

			IF (SELECT	row_count() FROM 		WorkOrder 	WO
							INNER JOIN LineItem	LI
							ON 			WO.fkLineItemID = LI.pkLineItemID
							WHERE 		LI.fkPartTemplateID = PTemplateID
							AND 		Live = 1
							AND 		(Complete = 0 OR Complete IS NULL)) > 0 

			OR (SELECT 		IF(OnAlert = 1, OnAlert, LockedDown)
				FROM			PartTemplate
				WHERE			pkPartTemplateID = PTemplateID) = 1 THEN

						SELECT Result;

	ELSE 
			INSERT INTO	Drawing
							(Reference, DateAdded, AddedByfkUserID, FileHandle, IssueNumber)
			VALUES			(reference, DATE(Now()), UserID, FileName, Issue);	

			SELECT			Row_Count() INTO Result;


						UPDATE			PartTemplate
			SET				fkDrawingID = Last_Insert_ID()

			WHERE			pkPartTemplateID = PTemplateID;

						UPDATE			PartTemplate
			SET				OnAlert = 1

			WHERE			fkParentPartTemplateID = PTemplateID;

												

						UPDATE			PartTemplate
			SET				OnAlert = 1

			WHERE			fkDrawingID = CurrentDrgID;

			SELECT			Row_Count() INTO Result;
			SELECT			Result;

	END IF;

ELSE

	INSERT INTO	AssociatedPartDoc
					(fkPartTemplateID, fkUserID, DateAdded, Location, FileHandle, fkDocTypeID)
	VALUES			(PTemplateID, UserID, DATE(Now()), 'DocStore', FileName, DocTypeID);	

	SELECT			Row_Count() INTO Result;
	SELECT			Result;

END IF;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddPartReviewTask` (IN `PTemplateID` SMALLINT UNSIGNED, `UserID` SMALLINT UNSIGNED, `Description` VARCHAR(30), `ActionTypeID` TINYINT UNSIGNED, `FO` TINYINT UNSIGNED, `Pu` TINYINT UNSIGNED, `PE` TINYINT UNSIGNED, `En` TINYINT UNSIGNED, `Qu` TINYINT UNSIGNED, `SP` TINYINT UNSIGNED, `St` TINYINT UNSIGNED, `Fi` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		PartReviewTask
				(fkPartTemplateID, Description, fkActionTypeID, CreateDate, AddedByfkUserID, FO, Pu, PE, En, Qu, SP, St, Fi)
VALUES			(PTemplateID, Description, ActionTypeID, DATE(Now()), UserID, FO, Pu, PE, En, Qu, SP, St, Fi);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddPOReject` (IN `UserID` SMALLINT UNSIGNED, `GRBID` INT UNSIGNED, `RejectQty` SMALLINT UNSIGNED, `Deviation` VARCHAR(200), `RejectClauseID` TINYINT UNSIGNED, `Closed` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		RejectGRB
				(fkGRBID, RejectQty, RejectedByfkUserID, RejectedDate, Deviation, RejectClauseID, Closed)
VALUES			(GRBID, RejectQty, UserID, Date(Now()), Deviation, RejectClauseID, Closed);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID() AS Result;
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddProcessNote` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `ProcessNote` VARCHAR(60))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		OperationProcess
				(fkOperationID, ProcessNote)
VALUES			(OperationID, ProcessNote);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		Last_Insert_ID() AS LastInsertID;
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddProgram` (IN `OperationID` INT, IN `MachineID` INT, IN `ProgramNumber` INT)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO 	Kythera.OpMachineProgram  
				(fkProgramNumber, fkOperationID, fkMachineID) 
VALUES			(ProgramNumber, OperationID, MachineID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddRemark` (IN `UserID` SMALLINT UNSIGNED, `Remark` VARCHAR(100))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		Remark
				(Remark, fkUserID, Deleted)
VALUES			(Remark, UserID, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddRFQSupplier` (IN `UserID` SMALLINT UNSIGNED, `RFQOID` SMALLINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `TotalCost` DECIMAL(9,2), `DeliveryPrice` DECIMAL(3,0))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		RFQOSupplier
					(fkRFQOID, fkSupplierID, TotalCost, DeliveryCost)
VALUES				(RFQOID, SupplierID, TotalCost, DeliveryPrice);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
		
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddStoreArea` (IN `StoreArea` VARCHAR(15))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		StoreArea
				(StoreArea)
VALUES			(StoreArea);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddSupplierAdditional` (IN `UserID` SMALLINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `AdditionalTypeID` TINYINT UNSIGNED, `StartPoint` VARCHAR(9), `EndPoint` VARCHAR(9))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

INSERT INTO		SupplierAdditionalTime
					(fkSupplierID, Timetype, StartPoint, EndPoint)
VALUES				(SupplierID, AdditionalTypeID, StartPoint, EndPoint);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddSupplierApproval` (IN `UserID` SMALLINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `ApprovalID` TINYINT UNSIGNED, `Expiry` DATE)  BEGIN
INSERT INTO		SupplierApproval
				(fkSupplierID, fkApprovalID, ExpiryDate, AddedByfkUserID)
VALUES			(SupplierID, ApprovalID, Expiry, UserID);

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddSupplierContact` (IN `UserID` SMALLINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `ContactName` VARCHAR(45), `Phone` VARCHAR(13), `Email` VARCHAR(50), `PrimaryContact` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		SupplierContact
				(fkSupplierID, ContactName, Phone, Email, Prime, AddedbyfkUserID, Deleted) 
			
VALUES			(SupplierID, ContactName, Phone, Email, PrimaryContact, UserID, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddSupplierNADCAP` (IN `UserID` SMALLINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `ApprovalID` TINYINT UNSIGNED, `Expiry` DATE)  BEGIN

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		SupplierNADCAP
				(fkSupplierID, fkApprovalID, AddedByfkUserID, ExpiryDate)
VALUES			(SupplierID, ApprovalID, UserID, Expiry);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddSupplierNote` (IN `UserID` SMALLINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `Note` VARCHAR(100))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		SupplierNote
					(fkSupplierID, Note, AddedByfkUserID)
VALUES				(SupplierID, Note, UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddSupplierQuestions` (IN `UserID` SMALLINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `FileName` VARCHAR(30), `CompletedBy` VARCHAR(25))  BEGIN


UPDATE		Supplier
SET			QuestionsFilename = fileName, SQCompletedBy = CompletedBy

WHERE		pkSupplierID = SupplierID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddTemplateMaterial` (IN `UserID` SMALLINT UNSIGNED, `OperationTemplateID` MEDIUMINT UNSIGNED, `MaterialSpecID` SMALLINT UNSIGNED, `Description` VARCHAR(20), `Size` DECIMAL(7,3), `ShapeID` TINYINT UNSIGNED, `PartLength` DECIMAL(8,2), `Yield` TINYINT UNSIGNED, `ApprovalID` TINYINT UNSIGNED, `IssueInstruction` VARCHAR(50), `TestPieces` TINYINT UNSIGNED, `FreeIssue` TINYINT UNSIGNED, `UnitID` TINYINT UNSIGNED, `StdDelDays` TINYINT UNSIGNED, `SPUID` TINYINT UNSIGNED, `StdSupplierID` SMALLINT UNSIGNED)  BEGIN
DECLARE			Result			tinyint DEFAULT -1;
DECLARE			NewMaterialID	smallint;

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

IF ISNULL(MaterialSpecID) THEN

		INSERT INTO		MaterialSpec
						(MaterialSpec, AddedByfkUserID)
		VALUES			(LEFT(Description,6), UserID);

		SELECT			Last_Insert_ID() INTO NewMaterialID;

END IF;


INSERT INTO 	Kythera.TemplateMaterial (pkOperationTemplateID, fkMaterialSpecID, Description, Size, fkShapeID, PartLength, Yield, 
											fkApprovalID, TestPieces, FreeIssue, UnitID, StdDeliveryDays, SPUfkUOMID, StdfkSupplierID, 
											IssueInstruction, AddedByfkUserID) 
VALUES									 (OperationTemplateID, IF(ISNULL(MaterialSpecID), NewMaterialID, MaterialSpecID), 
											Description, Size, ShapeID, PartLength, Yield, ApprovalID, TestPieces, FreeIssue, 
											UnitID, StdDelDays, SPUID, StdSupplierID, IssueInstruction, UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddTemplateNote` (IN `UserID` SMALLINT UNSIGNED, `PTemplateID` SMALLINT UNSIGNED, `PartNote` VARCHAR(80))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		PartTemplateNote
					(fkPartTemplateID, Note, CreateDate, AddedByfkUserID)
VALUES				(PTemplateID, PartNote, DATE(Now()), UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddTemplateOp` (IN `UserID` SMALLINT UNSIGNED, `EngineeringTemplateID` SMALLINT UNSIGNED, `OpTypeID` TINYINT UNSIGNED, `MachiningID` TINYINT UNSIGNED, `TreatmentID` TINYINT UNSIGNED, `SetTime` TIME, `RunTime` TIME, `Linked` TINYINT UNSIGNED)  BEGIN
DECLARE			Result			tinyint 	DEFAULT -1;
DECLARE 			OpStore 		tinyint 	unsigned DEFAULT 0;
DECLARE			OpTmpltStore 	smallint 	unsigned;

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

SET OpStore = 	IFNULL((SELECT MAX(OperationNumber) + 1 FROM OperationTemplate WHERE fkEngineeringTemplateID = EngineeringTemplateID),1);

INSERT INTO		OperationTemplate
				(fkEngineeringTemplateID, fkOperationTypeID, fkMachiningID, OperationNumber, EstimatedSetTime, EstimatedRunTime,
				Linked)
VALUES			(EngineeringTemplateID, OpTypeID, MachiningID, OpStore, SetTime, RunTime, Linked);

SELECT			Row_Count() INTO Result;
SELECT			Last_Insert_ID() INTO OpTmpltStore;

	


IF TreatmentID IS NOT NULL THEN
	
	INSERT INTO		TemplateTreatment
					(pkOperationTemplateID, fkTreatmentID, AddedByfkUserID)
	VALUES			(OpTmpltStore, TreatmentID, UserID);

END IF;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddTemplateProcess` (IN `OperationTemplateID` MEDIUMINT UNSIGNED, `ProcessNote` VARCHAR(60))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		TemplateProcess
				(fkOperationTemplateID, ProcessNote)
VALUES			(OperationTemplateID, ProcessNote);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddTemplateRemark` (IN `UserID` SMALLINT UNSIGNED, `PTemplateID` SMALLINT UNSIGNED, `PartRemark` VARCHAR(80))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		TemplateRemark
					(fkPartTemplateID, Remark, CreateDate, AddedByUserID)
VALUES				(PTemplateID, PartRemark, DATE(Now()), UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddTmpltApprovalComment` (IN `UserID` SMALLINT UNSIGNED, `OperationtemplateID` MEDIUMINT UNSIGNED, `Comment` VARCHAR(80))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

INSERT INTO		TemplateApprovalComments
				(fkOperationTemplateID, Comment)
VALUES			(OperationTemplateID, comment); 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddTmpltConformityNote` (IN `UserID` SMALLINT UNSIGNED, `OperationtemplateID` MEDIUMINT UNSIGNED, `Note` VARCHAR(80))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

INSERT INTO		TemplateNote
				(fkOperationTemplateID, Note, fkNoteTypeID, CreateDate, AddedByfkUserID)
VALUES			(OperationTemplateID, Note, 2, DATE(Now()), UserID); 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddTmpltPOComment` (IN `UserID` SMALLINT UNSIGNED, `OperationtemplateID` MEDIUMINT UNSIGNED, `Comment` VARCHAR(45))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

INSERT INTO		TemplatePOComments
				(fkOperationTemplateID, Comment)
VALUES			(OperationTemplateID, Comment); 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddTmpltProgram` (IN `UserID` SMALLINT UNSIGNED, `OperationTemplateID` MEDIUMINT UNSIGNED, `MachineID` TINYINT UNSIGNED, `ProgramNumber` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO 	OpTmpltMachineProgram  
				(fkOperationTemplateID, fkMachineID, ProgramNumber) 
VALUES			(OperationTemplateID, MachineID, ProgramNumber);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddTool` (IN `OperationID` INT, IN `ToolID` INT, IN `MachineID` INT)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
Insert INTO		Kythera.OpToolMachine 
				(fkOperationID, fkToolID, fkMachineID) 
VALUES			(OperationID, ToolID, MachineID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddUOM` (IN `UserID` SMALLINT UNSIGNED, `UOM` VARCHAR(11))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		UnitOfMeasure
				(Description, Deleted)
VALUES			(UOM, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddWebsite` (IN `UserID` SMALLINT UNSIGNED, `Website` VARCHAR(45), `UserName` VARCHAR(25), `SitePassword` VARCHAR(15))  BEGIN
INSERT INTO		WebsiteAccess
					(SiteAddress, Username, SitePassword, Deleted, AddedByfkUserID)
VALUES				(Website, Username, SitePassword, 0, UserID);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddWODocument` (IN `UserID` TINYINT, IN `WorkOrderID` MEDIUMINT, IN `DocTypeID` TINYINT, IN `FileName` VARCHAR(30))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO		WODocument
				(fkWorkOrderID, fkDocumentDropTypeID, FileName, Print, AddedByfkUserID, DateAdded)
VALUES			(WorkOrderID, DocTypeID, FileName, 0, UserID, CurDate());

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddWOLinkToPOItem` (IN `UserID` TINYINT UNSIGNED, `OperationID` INT UNSIGNED, `ItemID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE			PTID	smallint unsigned;

INSERT INTO		OperationOrderItem
				(fkOperationID, fkItemID, AddedByfkUserID)
VALUES			(OperationID, ItemID, UserID);

SELECT			Row_Count() INTO Result;

SET		PTID =  (SELECT fkPartTemplateID FROM LineItem LI INNER JOIN WorkOrder WO
				ON LI.pkLineItemID = WO.fkLineItemID
				INNER JOIN	Operation O ON WO.pkWorkOrderID = O.fkWorkOrderID
				WHERE O.pkOperationID = OperationID);

UPDATE			OrderItem
SET				fkPartTemplateID = PTID,
				Description = (SELECT Description FROM PartTemplate
								WHERE pkPartTemplateID = PTID);

SELECT			Result;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddWOOpTmpltIntEngNotes` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, IN `Note` VARCHAR(80))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		OperationNote
				(fkOperationID, Note, fkNoteTypeID, CreateDate, AddedByfkUserID)
VALUES			(OperationID, Note, 4, DATE(Now()), UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddWOOpTmpltIntReviewNote` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `Note` VARCHAR(80))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		OperationNote
				(fkOperationID, Note, fkNoteTypeID, CreateDate, AddedByfkUserID)
VALUES			(OperationID, Note, 9, DATE(Now()), UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddWOOpTmpltIntTreatNotes` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `Note` VARCHAR(80))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		OperationNote

				(fkOperationID, Note, fkNoteTypeID, CreateDate, AddedByfkUserID)
VALUES			(OperationID, Note, 6, DATE(Now()), UserID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddWOOutvoice` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED, `Qty` SMALLINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;

INSERT INTO		Outvoice
					(CreateDate, CreatedByfkUserID, SentDate, Quantity, DeliveryChg, Complete)
	VALUES			(Now(), UserID, NULL, Qty, NULL, NULL);


INSERT INTO		WOOutvoice
					(fkInvoiceID, fkWorkOrderID)
	VALUES			(Last_Insert_ID(), WorkOrderID);

	SELECT			Row_Count() INTO Result;



IF 				Result < 1
THEN
		SELECT		Result;

ELSE			
		SELECT		last_Insert_ID();
	
		INSERT INTO	Todo
					(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, PartNumber, Description, fkGRBID)
		VALUES		(CurDate(), 22, UserID, 2, WorkOrderID, PartNumber, "Dispatch parts", (SELECT 	MAX(pkGRBID)
																							FROM 	GoodsIn
																							WHERE 	WorkOrderID = WorkOrderID));
	
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddWorkOrderNote` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED, `Note` VARCHAR(80))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;

INSERT INTO		WorkOrderNote
				(Note, fkWorkOrderID)

VALUES			(Note, WorkOrderID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddWOTemplateMaterial` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `MaterialSpecID` SMALLINT UNSIGNED, `Description` VARCHAR(20), `Size` DECIMAL(7,3), `ShapeID` TINYINT UNSIGNED, `PartLength` DECIMAL(8,2), `Yield` TINYINT UNSIGNED, `ApprovalID` TINYINT UNSIGNED, `IssueInstruction` VARCHAR(50), `TestPieces` TINYINT UNSIGNED, `FreeIssue` TINYINT UNSIGNED, `UnitID` TINYINT UNSIGNED, `StdDelDays` TINYINT UNSIGNED, `SPUID` TINYINT UNSIGNED, `StdSupplierID` SMALLINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO 	Kythera.OperationMaterial (pkOperationID, fkMaterialSpecID, Description, Size, fkShapeID, PartLength, Yield, 
											fkApprovalID, TestPieces, FreeIssue, UnitID, StdDeliveryDays, SPUfkUOMID, StdfkSupplierID, 
 											IssueInstruction, AddedByfkUserID) 
VALUES									  (OperationID, MaterialSpecID, Description, Size, ShapeID, PartLength, Yield, 
											ApprovalID, TestPieces, FreeIssue, UnitID, StdDelDays, SPUID, StdSupplierID, IssueInstruction, UserID);

SELECT			Row_Count() INTO Result;

IF 	Result < 1
THEN
	SELECT	Result;
ELSE			
 	SELECT	last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddWOTemplateOp` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED, `OpTypeID` TINYINT UNSIGNED, `MachiningID` TINYINT UNSIGNED, `TreatmentID` TINYINT UNSIGNED, `SetTime` TIME, `RunTime` TIME, `Linked` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE 		OpStore tinyint unsigned DEFAULT 0;

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

SET 			OpStore = 	(SELECT MAX(OperationNumber) + 1 FROM Operation WHERE fkWorkOrderID = WorkOrderID);
IF 				ISNULL(OpStore) 
THEN 			SET OpStore = 1;
END IF;

INSERT INTO		Operation
				(fkWorkOrderID, OperationNumber, fkOperationTypeID, fkMachiningTypeID, EstimatedSetTime, EstimatedRunTime, Linked)
VALUES			(WorkOrderID, OpStore, OpTypeID, MachiningID, SetTime, RunTime, Linked);

SELECT			Row_Count() INTO Result;

IF TreatmentID IS NOT NULL THEN
	
INSERT INTO		OperationTreatment
				(pkOperationID, fkTreatmentID, AddedByfkUserID)
VALUES			(Last_Insert_ID(), TreatmentID, UserID);

END IF;

IF Result < 1
	THEN SELECT		Result;
ELSE			
	SELECT	last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddWOTemplateProcess` (IN `OperationID` INT UNSIGNED, `ProcessNote` VARCHAR(60))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		OperationProcess
				(fkOperationID, ProcessNote)
VALUES			(OperationID, ProcessNote);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddWOTmpltApprovalComment` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `Comment` VARCHAR(80))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

INSERT INTO		OperationApprovalComments
				(fkOperationID, Comment)
VALUES			(OperationID, comment); 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddWOTmpltConformityNote` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `Note` VARCHAR(80))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

INSERT INTO		OperationNote
				(fkOperationID, Note, fkNoteTypeID, CreateDate, AddedByfkUserID)
VALUES			(OperationID, Note, 2, DATE(Now()), UserID); 

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddWOTmpltPOComment` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `Comment` VARCHAR(80))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

INSERT INTO		POComment
				(fkRFQOID, Comment)
VALUES			((SELECT fkRFQOID 
					FROM OrderItem OI
					INNER JOIN OperationOrderItem OOI
					ON OI.pkItemID = OOI.fkItemID
					WHERE OOI.fkOperationID = OperationID), Comment); 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddWOTmpltProgram` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `MachineID` TINYINT UNSIGNED, `ProgramNumber` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO 	OpMachineProgram  
				(fkOperationID, fkMachineID, fkProgramNumber) 
VALUES			(OperationID, MachineID, ProgramNumber);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddXLocation` (IN `StoreAreaID` SMALLINT UNSIGNED, `XCoord` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		xCoordinate
				(fkStoreAreaID, XCoord, Deleted)
VALUES			(StoreAreaID, XCoord, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAddYLocation` (IN `StoreAreaID` SMALLINT UNSIGNED, `YCoord` CHAR(1))  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		YCoordinate
				(fkStoreAreaID, YCoord, Deleted)
VALUES			(StoreAreaID, YCoord, 0);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAllocateResource` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `ResourceUserID` SMALLINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;



	IF 
		(SELECT fkOperationID FROM MachineSet WHERE fkOperationID = OperationID) IS NOT NULL
	THEN	
		IF 
			(SELECT EndTime FROM MachineSet WHERE fkOperationID = OperationID) IS NULL 
		THEN					UPDATE		MachineSet
				SET			fkUserIDSetter = ResourceUserID

				WHERE 		fkOperationID = OperationID;

				SELECT		Row_Count() INTO Result;

												
				
				
		ELSE
								UPDATE		OperationMachine
				SET			fkUserIDOperator = ResourceUserID 

				WHERE		fkOperationID = OperationID;

				SELECT		Row_Count() INTO Result;
		END IF;

	ELSE
												
		
														UPDATE		OperationMachine
		SET			fkUserIDOperator = ResourceUserID 

		WHERE		fkOperationID = OperationID;

		SELECT		Row_Count() INTO Result;

	END IF;





SELECT		Result;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcAmendSupplierPersonnel` (IN `UserID` SMALLINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `Type1` TINYINT UNSIGNED, `Type2` TINYINT UNSIGNED, `Type3` TINYINT UNSIGNED, `Type4` TINYINT UNSIGNED, `Type5` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		Supplier
SET			Type1 = Type1, Type2 = Type2, Type3 = Type3, Type4 = Type4, Type5 = Type5

WHERE		pkSupplierID = SupplierID;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcBeginInspection` (IN `UserID` SMALLINT UNSIGNED, `GRBID` INT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
INSERT INTO			Inspection
					(fkUserID, fkTypeID, InspectStart)
VALUES				(UserID, 1, now());

INSERT INTO			MaterialInspect
					(fkMaterialID, fkInspectionID)
VALUES				(GRBID, LAST_INSERT_ID());

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcBeginMaterialInspection` (IN `UserID` SMALLINT UNSIGNED, `GRBID` INT UNSIGNED)  BEGIN
DECLARE		Result			tinyint DEFAULT -1;
DECLARE		MsgResult 		varchar(25);

IF		EXISTS(SELECT fkMaterialID
				FROM MaterialInspect
				WHERE fkMaterialID = GRBID)

THEN			SET		MsgResult = 'Error; started';

ELSE			INSERT INTO		Inspection
						(fkUserID, fkTypeID, InspectStart)
		VALUES			(UserID, 1, now());

		INSERT INTO		MaterialInspect
						(fkMaterialID, fkInspectionID)
		VALUES			(GRBID, LAST_INSERT_ID());

		SELECT	Row_Count() INTO Result;
		SET		MsgResult = 'Inspection started';

END IF;

IF 		Result < 1
THEN
		SELECT		MsgResult, Result;
ELSE			
		SELECT		MsgResult, last_Insert_ID() AS Last_Insert_ID;
END IF;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcBeginMaterialIssue` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
UPDATE		OperationMachine
SET			fkUserIDRunStart = UserID, RunStart = now()

WHERE		fkOperationID = (SELECT pkOperationID 
								FROM Operation 
								WHERE OperationNumber = 1 
								AND fkWorkOrderID = WorkOrderID);

SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcBeginOperation` (IN `UserID` SMALLINT UNSIGNED, `OperationId` INT UNSIGNED)  BEGIN
DECLARE		Result								tinyint DEFAULT -1;
DECLARE		ResultMsg 							varchar(30) DEFAULT NULL;
DECLARE		StrtTime, RunStrt, SetEnd, RunEnd 	datetime;
DECLARE		LatestPauseID 						int unsigned DEFAULT NULL;
DECLARE		WONo								mediumint unsigned;


IF	(SELECT fkUserIDOperator 
	FROM	OperationMachine OM1
	WHERE	fkOperationID = OperationId) IS NULL
THEN
	IF EXISTS(SELECT 	*
					FROM	MachineSet
					WHERE	fkOperationID = OperationID
					AND StartTime IS NULL
					AND EndTime IS NULL)
	THEN
			IF		(SELECT 	fkUserIDSetter 
						FROM	MachineSet
						WHERE	fkOperationID = OperationId) IS NULL

			THEN
					SET ResultMsg = 'Resource not set!';
			END IF;

	ELSE
			SET ResultMsg = 'Resource not set!';
	END IF;

END IF;

IF 
		ISNULL(ResultMsg )

THEN
				SET StrtTime = (SELECT StartTime FROM MachineSet WHERE fkOperationID = OperationId);
		SET SetEnd = (SELECT EndTime FROM MachineSet WHERE fkOperationID = OperationId);
		SET RunStrt = (SELECT RunStart FROM OperationMachine WHERE fkOperationID = OperationId);
		SET RunEnd = (SELECT OM2.RunEnd FROM OperationMachine OM2 WHERE OM2.fkOperationID = OperationId);

		IF
									EXISTS(SELECT 	*
						FROM	MachineSet
						WHERE	fkOperationID = OperationID)
		THEN
						IF
								StrtTime IS NOT NULL
			THEN
								IF 
					SetEnd IS NOT NULL
				THEN	
															SET LatestPauseID = (	SELECT MAX(pkPauseID)
											FROM Pause
											WHERE fkOperationID = OperationId);

					IF
												RunStrt IS NOT NULL
						AND RunEnd IS NULL
						AND (SELECT EndTime
									FROM Pause
									WHERE pkPauseID = LatestPauseID) IS NOT NULL
								
					THEN
												SET ResultMsg = 'Error; job already running.';

					ELSE
												IF 	RunStrt IS NOT NULL
							AND RunEnd IS NULL

						THEN
														SET ResultMsg = 'Error; job paused; try Pause!';

						ELSE 
							IF RunEnd IS NULL
							THEN

								SET WONo = (SELECT fkWorkOrderID
											FROM	Operation
											WHERE	pkOperationID = OperationId);

																UPDATE		OperationMachine
								SET			fkUserIDRunStart = UserID, RunStart = Now() 

								WHERE 		fkOperationID = OperationId;

								SELECT		Row_Count() INTO Result;	

																UPDATE		Operation
								SET			StartQty = (SELECT QuantityReqd + QuantityOvers
														FROM	WorkOrder
														WHERE	pkworkOrderID = WONo);

								SET ResultMsg = 'Job is now running.';

							ELSE
														SET ResultMsg = 'Error; Job complete.';

							END IF;
						END IF;
					END IF;

				ELSE
										SET LatestPauseID = (	SELECT MAX(pkPauseID)
											FROM Pause
											WHERE fkOperationID = OperationId);

					IF LatestPauseID IS NOT NULL 
						AND ISNULL((SELECT EndTime
									FROM Pause
									WHERE pkPauseID = LatestPauseID))
					
					THEN
												SET ResultMsg = 'Error; Set paused. Try Pause!';


					ELSE
												SET ResultMsg = 'Error; Set already running.';


					END IF;

				END IF;

			ELSE
								UPDATE		MachineSet
				SET			fkUserIDSetStart = UserID, StartTime = Now()

				WHERE 		fkOperationID = OperationId
					AND		fkMachineID = (SELECT fkMachineID
											FROM OperationMachine
											WHERE fkOperationID = OperationID);

				SELECT			Row_Count() INTO Result;

				SET ResultMsg = 'Machine set is now running.';

			END IF;

		ELSE
			
									IF
				RunStrt IS NOT NULL
			THEN
								SET ResultMsg = 'Error; Job already running..';

			ELSE
								UPDATE		OperationMachine
				SET			fkUserIDRunStart = UserID, RunStart = Now()

				WHERE 		fkOperationID = OperationId;

				SELECT			Row_Count() INTO Result;

				SET ResultMsg = 'Job is now running.';

			END IF;

		END IF;

END IF;

SELECT Result, ResultMsg;


END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcBeginOpInspection` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED)  BEGIN
DECLARE	MSet		tinyint unsigned;
DECLARE	MsgResult 	varchar(25);
DECLARE	InspectID	bigint unsigned;

		

IF EXISTS(SELECT pkSetID 
				FROM MachineSet
				WHERE fkOperationID = OperationID)

THEN
				IF 		(SELECT 		Count(*)
				FROM		SetInspect SI1
				INNER JOIN	MachineSet MS1
				ON			SI1.fkSetID = MS1.pkSetID
				WHERE		MS1.fkOperationID = OperationID) > 0

		THEN
								IF 		(SELECT Count(*)
						FROM	OperationInspect
						WHERE	fkOperationID = OperationID)  > 0
				THEN

						SET 	MsgResult = 'Err; Set running/complete';

				ELSE
												INSERT INTO	Inspection
										(fkUserID, fkTypeID, InspectStart)
						VALUES			(UserID, 2, now());

						SET		InspectID = last_Insert_ID();

												INSERT INTO	OperationInspect
										(fkOperationID, fkInspectionID)
						VALUES			(OperationID, InspectID);

						SET MsgResult = 'Op Inspect started';
				END IF;

		ELSE
								INSERT INTO	Inspection
								(fkUserID, fkTypeID, InspectStart)
				VALUES			(UserID, 2, now());

				SET		InspectID = last_Insert_ID();

								INSERT INTO		SetInspect
								(fkSetID, fkInspectionID)
				VALUES			((SELECT pkSetID 
									FROM MachineSet
									WHERE fkOperationID = OperationID), InspectID);

				SET 	MsgResult = 'Set Inspect started';

		END IF;

ELSE
				IF 		(SELECT Count(*)
				FROM	OperationInspect
				WHERE	fkOperationID = OperationID)  > 0
	
		THEN
				SET 	MsgResult = 'Err; Op running/complete';

		ELSE
								INSERT INTO	Inspection
								(fkUserID, fkTypeID, InspectStart)
				VALUES			(UserID, 2, now());

				SET		InspectID = last_Insert_ID();

								INSERT INTO	OperationInspect
								(fkOperationID, fkInspectionID)
				VALUES			(OperationID, InspectID);

				SET MsgResult = 'Op Inspect started';
									
		END IF;
END IF;
	
SELECT		InspectID AS Last_Insert_ID, MsgResult;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCancelLineItem` (IN `UserID` SMALLINT UNSIGNED, `LineItemID` MEDIUMINT UNSIGNED, `IssueType` TINYINT UNSIGNED, `CancelReason` VARCHAR(80))  BEGIN
UPDATE		LineItem
SET			IssueType = IssueType, CancelReason = CancelReason

WHERE		pkLineItemID = LineItemID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCancelWO` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED, `ChangeReason` VARCHAR(30))  BEGIN

DECLARE		Status tinyint;

IF 
	(SELECT 	PreCancel FROM WorkOrder
	WHERE		pkWorkOrderID = WorkOrderID) = 1
THEN
	UPDATE			WorkOrder
	SET				Live = 2, ChangeReason = ChangeReason

	WHERE			pkWorkOrderID = WorkOrderID;

	INSERT INTO	Todo
				(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, Description)
	VALUES		(Date(Now()), 21, UserID, 5, WorkOrderID, "WO Cancelled");

	SELECT		1 AS Status;

ELSE
	SELECT 	2 AS Status;
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCertificateOfConformity` (IN `InvoiceNumber` INT UNSIGNED)  BEGIN


select CustomerName, InvAddress1, InvAddress2, InvAddress3, InvAddress4, InvPostcode,
		O.pkInvoiceID, 41868 AS 'CertNo', GI.DeliveryNoteNumber, P.Description, D.Reference AS DrawingNumber,
		D.IssueNumber, GI.IdentificationMark, GI.IRNoteNumber, OPN.Note AS CoCNote, pkPOnID AS fkPOID, WO.pkWorkOrderID,
		WO.Complete

from		WorkOrder WO
inner join	lineitem LI
on			WO.fkLineItemID = LI.pkLineItemID
inner join	customerorder CO
on			LI.fkOrderID = CO.pkOrderID
inner join	customer C
on			CO.fkCustomerID = C.pkCustomerID
inner join	operation OP
on			WO.pkWorkOrderID = OP.fkWorkOrderID
LEFT JOIN	PartTemplate P
ON			LI.fkPartTemplateID = P.pkPartTemplateID
LEFT JOIN	OperationNote OPN
ON			OP.pkOperationID = OPN.fkOperationID
left join	drawing D
on			P.fkDrawingID = D.pkDrawingID
inner join	wooutvoice WOO
on			WO.pkWorkOrderID = WOO.fkWorkOrderID
inner join	outvoice O
on			WOO.fkInvoiceID = O.pkInvoiceID
inner join	allocated A
on			OP.pkOperationID = A.fkOperationID
inner join	material M
on			A.fkGRBID = M.fkGRBID
INNER join	goodsin GI
on			M.fkGRBID = GI.pkGRBID
inner join	operationmaterial OM
on			OP.pkOperationID = OM.pkOperationID
inner join	pogoods POG
on			A.fkGRBID = POG.fkGRBID
left JOIN	OrderItem OI
ON			POG.fkItemID = OI.pkItemID
left JOIN	POn
ON			OI.fkRFQOID = POn.fkRFQOID
where		O.pkInvoiceID = InvoiceNumber
AND			OPN.fkNoteTypeID = 2;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCertificateOfConformity2` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
select 		CustomerName, InvAddress1, InvAddress2, InvAddress3, InvAddress4, InvPostcode,
			O.pkInvoiceID, GI.IRNoteNumber AS 'CertNo', GI.DeliveryNoteNumber, P.Description, D.Reference AS DrawingNumber,
			D.IssueNumber, GI.IdentificationMark, GI.IRNoteNumber, OPN.Note AS CoCNote, pkPOnID AS fkPOID, WO.pkWorkOrderID,
			WO.Complete

FROM		WorkOrder WO
INNER JOIN	lineitem LI
ON			WO.fkLineItemID = LI.pkLineItemID
INNER JOIN	customerorder CO
ON			LI.fkOrderID = CO.pkOrderID
INNER JOIN	customer C
ON			CO.fkCustomerID = C.pkCustomerID
INNER JOIN	operation OP
ON			WO.pkWorkOrderID = OP.fkWorkOrderID
LEFT JOIN	PartTemplate P
ON			LI.fkPartTemplateID = P.pkPartTemplateID
LEFT JOIN	OperationNote OPN
ON			OP.pkOperationID = OPN.fkOperationID
LEFT JOIN	drawing D
ON			P.fkDrawingID = D.pkDrawingID
LEFT JOIN	wooutvoice WOO
ON			WO.pkWorkOrderID = WOO.fkWorkOrderID
LEFT JOIN	outvoice O
ON			WOO.fkInvoiceID = O.pkInvoiceID
INNER JOIN	allocated A
ON			OP.pkOperationID = A.fkOperationID
INNER JOIN	material M
ON			A.fkGRBID = M.fkGRBID
INNER JOIN	goodsin GI
ON			M.fkGRBID = GI.pkGRBID
INNER JOIN	operationmaterial OM
ON			OP.pkOperationID = OM.pkOperationID
INNER JOIN	pogoods POG
ON			A.fkGRBID = POG.fkGRBID
INNER JOIN	OrderItem OI
ON			POG.fkItemID = OI.pkItemID
INNER JOIN	POn
ON			OI.fkRFQOID = POn.fkRFQOID

WHERE		WO.pkWorkOrderID = WorkOrderID
AND			OP.fkOperationTypeID = 1
AND			OPN.fkNoteTypeID = 2
;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCloseTask` (IN `UserID` SMALLINT UNSIGNED, `TaskID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		PartReviewTask
SET			Closed = 1

WHERE		pkTaskID = TaskID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCOCNotes` ()  BEGIN

SELECT 		fkOperationID, Note AS CoCNote

FROM		OperationNote

WHERE 		fkNoteTypeID = 2;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCommentSearch` (IN `SearchField` VARCHAR(100))  BEGIN
SELECT		Comment

FROM		POComment

WHERE		Comment LIKE CONCAT(SearchField, "%")
AND			Deleted <> 1;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCompleteInspection` (IN `UserID` SMALLINT UNSIGNED, `InspectionID` BIGINT UNSIGNED)  BEGIN
DECLARE	MsgResult 	varchar(25);
DECLARE	OpNo		tinyint unsigned;
DECLARE	WONo		mediumint unsigned;
DECLARE	GRBStore	int unsigned;
DECLARE 	Scrap		smallint unsigned;


IF 		ISNULL(InspectionID)
THEN	SET MsgResult = 'Error; Not Running';
ELSE

SET		WONo = (SELECT 	fkWorkOrderID
				FROM 		Operation O3
				INNER JOIN OperationInspect OI3
				ON			O3.pkOperationID = OI3.fkOperationID
				WHERE		fkInspectionID = InspectionID);

IF		(SELECT Count(*)
		FROM 	Inspection
		WHERE 	pkInspectionID = InspectionID
		AND		InspectEnd IS NULL) = 0

THEN
		SET 	MsgResult = 'Error; not run/complete';

ELSE 	
		IF	(SELECT Count(*)
			FROM	InspectPause
			WHERE	fkInspectionID = InspectionID
			AND EndTime IS NULL) = 0

		THEN

			UPDATE	Inspection
			SET		InspectEnd = now(), fkUserIDComplete = UserID

			WHERE	pkInspectionID = InspectionID;

			SET		MsgResult = 'Inspection Complete';

			SET		GRBStore = (SELECT 	MAX(pkGRBID)
								FROM 	GoodsIn
								WHERE 	WorkOrderID = WONo);

			SET		OpNo = (SELECT 	OperationNumber
							FROM 		Operation O5
							INNER JOIN 	OperationInspect OI5
							ON			O5.pkOperationID = OI5.fkOperationID
							WHERE		fkInspectionID = InspectionID);


						IF EXISTS (SELECT * 
							FROM MaterialInspect 
							WHERE fkInspectionID = InspectionID)
			THEN
					UPDATE	GoodsIn
					SET		Inspected = 1
					WHERE	pkGRBID = IFNULL(GRBStore, (SELECT 	fkMaterialID
														FROM		MaterialInspect
														WHERE		fkInspectionID = InspectionID));

			ELSE

					IF		EXISTS (SELECT * 
									FROM OperationInspect 
									WHERE fkInspectionID = InspectionID)
																												AND	(SELECT 	Count(*)
									FROM 		Operation
									WHERE		fkWorkOrderID = WONo) = OpNo

					THEN
														UPDATE 	WorkOrder
							SET		Complete = 1
							WHERE	pkWorkOrderID = (SELECT 	fkWorkOrderID
													FROM		Operation O2
													INNER JOIN	OperationInspect OI2
													ON			O2.pkOperationID = OI2.fkOperationID
													WHERE		fkInspectionID = InspectionID);

							UPDATE Inspection
							SET	   Outcome = 1
							WHERE  pkInspectionID = InspectionID;

														IF		(SELECT 	Count(*)
									FROM 		Operation
									WHERE		fkWorkOrderID = WONo) = OpNo

							THEN										INSERT INTO 	Todo
													(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, Complete, Description)
									VALUES			(CurDate(), 6, UserID, 7, WONo, 0, 'WO complete; Send outvoice');

							END IF; 

					ELSE
							IF
									(SELECT Count(*)
									FROM SetInspect
									WHERE fkInspectionID = InspectionID) = 1
							THEN
									UPDATE Inspection
									SET	   Outcome = 1
									WHERE  pkInspectionID = InspectionID;

																		INSERT INTO		Todo
									(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, Complete, Description)
									VALUES		(CurDate(), 1, UserID, 5, WONo, 0, "Inspect machine set");
	
							END IF;

							IF ISNULL(MsgResult)
							THEN
							SET MsgResult = 'Inspection updated';
							END IF;

					END IF;

			END IF;

		ELSE
			SET MsgResult = 'Error; task paused';

		END IF;
END IF;
END IF;

SELECT		MsgResult;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCompleteOperation` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED)  BEGIN
DECLARE		LatestPauseID		int unsigned DEFAULT NULL;
DECLARE		RunStrt, SetStrt	datetime;
DECLARE		ResultMsg			varchar(30);


SET RunStrt = 			(SELECT RunStart 
						FROM OperationMachine
						WHERE fkOperationID = OperationID);

SET LatestPauseID = 	(SELECT MAX(pkPauseID)
						FROM Pause
						WHERE fkOperationID = OperationID);

IF EXISTS(SELECT 	*
				FROM	MachineSet
				WHERE	fkOperationID = OperationID)

THEN
	IF 		(SELECT EndTime FROM MachineSet WHERE fkOperationID = OperationID) IS NULL

	THEN
		IF 			(LatestPauseID IS NOT NULL 
			AND (SELECT EndTime
					FROM Pause
					WHERE pkPauseID = LatestPauseID) IS NOT NULL)
			OR ISNULL(LatestPauseID)

		THEN
						UPDATE		MachineSet
			SET			fkUserIDSetEnd = UserID, EndTime = Now()

			WHERE 		fkOperationID = OperationID;

			SET ResultMsg = 'Set Completed.';

		ELSE
			SET ResultMsg = 'Error; Op paused.';

		END IF;

	ELSE
				IF
						RunStrt IS NOT NULL
			AND (LatestPauseID IS NOT NULL 
			AND (SELECT EndTime
					FROM Pause
					WHERE pkPauseID = LatestPauseID) IS NOT NULL
			OR ISNULL(LatestPauseID))
								
		THEN					IF ISNULL((SELECT RunEnd 
							FROM OperationMachine 
							WHERE fkOperationID = OperationID))

				THEN
						UPDATE		OperationMachine
						SET			fkUserIDRunEnd = UserID, RunEnd = Now()

						WHERE 		fkOperationID = OperationID;

						SET ResultMsg = 'Job Completed.';
				ELSE
						
						SET ResultMsg = 'Error; Job already Complete.';
				END IF;

		ELSE
					
				IF	 ISNULL(RunStrt)

				THEN

										SET ResultMsg = 'Error; job not running.';
						
				ELSE

										SET ResultMsg = 'Error; job paused.';

				END IF;

		END IF;
	END IF;
ELSE
	IF
				RunStrt IS NOT NULL
		AND (LatestPauseID IS NOT NULL 
		AND (SELECT EndTime
				FROM Pause
				WHERE pkPauseID = LatestPauseID) IS NOT NULL
				OR ISNULL(LatestPauseID))
								
	THEN			IF (SELECT RunEnd 
						FROM OperationMachine 
						WHERE fkOperationID = OperationID) IS NULL

			THEN
					UPDATE		OperationMachine
					SET			fkUserIDRunEnd = UserID, RunEnd = Now()

					WHERE 		fkOperationID = OperationID;

					SET ResultMsg = 'Job Completed.';
			ELSE
						
					SET ResultMsg = 'Error; Job already Complete.';
			END IF;
	ELSE
				SET ResultMsg = 'Error; Paused or not running.';
	END IF;

END IF;

SELECT ResultMsg;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCompletePO` (IN `UserID` SMALLINT UNSIGNED, `PONo` SMALLINT UNSIGNED)  BEGIN
UPDATE		RFQOrder
SET			Complete = 1

WHERE		pkRFQOID = (SELECT fkRFQOID FROM POn WHERE pkPOnID = PONo);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCompleteTodo` (IN `TodoID` MEDIUMINT UNSIGNED, IN `UserID` SMALLINT UNSIGNED)  BEGIN
UPDATE		Todo
SET			Complete = 1, CompletedByfkUserID = UserID
WHERE		pkTodoID = TodoID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcContactList` ()  BEGIN
SELECT		UName

FROM		User

WHERE		Employed <> 0 AND Employed IS NOT NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCopyPO` (IN `UserID` SMALLINT UNSIGNED, `PONo` SMALLINT UNSIGNED)  BEGIN

DECLARE			IdStore mediumint DEFAULT 0;

INSERT INTO		RFQOrder
				(fkApprovalLevelID, fkUserID, OtherCharges, ReturnDrawing, VeryUrgent, InternalOnly,
				MOQ, Reference, cocRequired, CreateDate, LeadDays, OrderQuantity, Cancelled, Complete, POType, 
				DeliveryCommentID)
SELECT			fkApprovalLevelID, UserID, OtherCharges, ReturnDrawing, VeryUrgent, InternalOnly,
				MOQ, Reference, cocRequired, DATE(Now()), LeadDays, OrderQuantity, Cancelled, Complete, POType, 
				DeliveryCommentID

FROM			RFQOrder RO
INNER JOIN		POn		 PO
ON				RO.pkRFQOID = PO.fkRFQOID
INNER JOIN		RFQOSupplier ROS
ON				RO.pkRFQOID = ROS.fkRFQOID				

WHERE			PO.pkPOnID = PONo
AND				ROS.Selected = 1;

SET				IdStore = Last_Insert_ID();


INSERT INTO		OrderItem
				(fkRFQOID, fkPartTemplateID, fkShapeID, SPU, fkUOMID, Quantity, Description, Comment, Price,
				Size, Per)
SELECT			IdStore, fkPartTemplateID, fkShapeID, SPU, fkUOMID, Quantity, Description, Comment, Price,
				Size, Per

FROM			OrderItem OI
INNER JOIN		POn	PN
ON				OI.fkRFQOID = PN.fkRFQOID

WHERE			PN.pkPOnID = PONo;


INSERT INTO		RFQOSupplier
				(fkRFQOID, fkSupplierID, fkContactID, Selected)
SELECT			IdStore, fkSupplierID, fkContactID, Selected

FROM			RFQOSupplier ROS2
INNER JOIN		POn		 PO2
ON				ROS2.fkRFQOID = PO2.fkRFQOID				

WHERE			PO2.pkPOnID = PONo
AND				ROS2.Selected = 1;


INSERT INTO		POn
				(fkRFQOID, CreateDate, AddedByfkUserID)
SELECT			IdStore, Date(Now()), UserID;


SELECT			IdStore AS RFQOID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCourierBooked` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `CarriageTypeID` TINYINT UNSIGNED, `CourierID` TINYINT UNSIGNED)  BEGIN

DECLARE		GRBStore	int unsigned;
DECLARE		WOStore		mediumint unsigned;
DECLARE		POnStore	smallint unsigned;

SET			WOStore		= (SELECT fkWorkOrderID 
							FROM Operation
							WHERE pkOperationID = OperationID);

SET 		GRBStore 	= IF(ISNULL((SELECT fkGRBID FROM Allocated WHERE fkOperationID = OperationID)),
									(SELECT pkGRBID FROM GoodsIn WHERE WorkOrderID = WOStore),
									(SELECT fkGRBID FROM Allocated WHERE fkOperationID = OperationID));

SET			POnStore	= (SELECT pkPOnID FROM POn P 
						INNER JOIN	OrderItem OI
						ON			P.fkRFQOID = OI.fkRFQOID
						INNER JOIN	OperationOrderItem OOI
						ON			OI.pkItemID = OOI.fkItemID
						WHERE		OOI.fkOperationID = OperationID);


IF 
	(SELECT COUNT(*) FROM Dispatch WHERE fkGRBID = GRBStore) > 0

THEN

	UPDATE		Dispatch D

	SET			fkPOnID = POnStore, CourierBookDate = now(), BookedByfkUserID = UserID, fkCourierID = CourierID, CarriageTypeID = CarriageTypeID

	WHERE		
												fkGRBID = GRBStore;

ELSE

	INSERT INTO Dispatch 
				(fkGRBID, fkPOnID, CourierBookDate, BookedByfkUserID, fkCourierID, CarriageTypeID)
	VALUES		(GRBStore, POnStore, now(), UserID, CourierID, CarriageTypeID);

END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCreateOpsFromTemplate` (IN `UserID` SMALLINT UNSIGNED, `NewWONo` MEDIUMINT UNSIGNED, `ETemplateNo` SMALLINT UNSIGNED)  BEGIN

DECLARE done 																		INT DEFAULT FALSE;
DECLARE CRiskStatement 																varchar(100);
DECLARE OpStore 																	INT unsigned;
DECLARE OperationTemplateID, ItemStore 												mediumint unsigned;
DECLARE EngineeringTemplateID, RFQOid, StdSuppID									smallint unsigned;
DECLARE COperationNumber, COperationTypeID, MachiningID, CLinked, OrderItemNumber, FI	tinyint unsigned;
DECLARE CEstimatedSetTime, CEstimatedRunTime 										time;

DECLARE OpTmpltCursor CURSOR FOR 
		SELECT 	pkOperationTemplateID, fkEngineeringTemplateID, OperationNumber, fkOperationTypeID, fkMachiningID, 
				EstimatedSetTime, EstimatedRunTime, RiskStatement, Linked 

		FROM 	OperationTemplate
		
		WHERE	fkEngineeringTemplateID = ETemplateNo;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  OPEN OpTmpltCursor;

  read_loop: LOOP
		FETCH OpTmpltCursor INTO OperationTemplateID, EngineeringTemplateID, COperationNumber, COperationTypeID, MachiningID, 
								CEstimatedSetTime, CEstimatedRunTime, CRiskStatement, CLinked;

IF done THEN
			LEAVE read_loop;

		END IF;

		INSERT INTO	Operation
					(fkworkOrderID, OperationNumber, fkOperationTypeID, fkMachiningTypeID,
					EstimatedSetTime, EstimatedRunTime, RiskStatement, Linked)
		VALUES		(NewWONo, COperationNumber, COperationTypeID, MachiningID, 
					CEstimatedSetTime, CEstimatedRunTime, CRiskStatement, CLinked);
					
		SET 		OpStore = Last_Insert_ID();
		
		IF COperationTypeID = 1 THEN

				UPDATE		Operation
				SET			StartQty = (SELECT CEIL((QuantityReqd + QuantityOvers) *1.1 +1)
										FROM WorkOrder
										WHERE pkWorkOrderID = NewWONo)
				WHERE		pkOperationID = OpStore;

								INSERT INTO	OperationMachine
							(fkOperationID, fkMachineID, Multi)
					VALUES	(OpStore, 20, 0);
				
				
				
				INSERT INTO	OperationMaterial
							(pkOperationID, fkMaterialSpecID, Description, Size, fkShapeID, PartLength, fkApprovalID, 
							SupplyCondition, FreeIssue, UnitID, StdfkSupplierID, IssueInstruction, SPUfkUOMID, 
							SPU, Yield, TestPieces, StdDeliveryDays, AddedByfkUserID)
				SELECT		OpStore, fkMaterialSpecID, Description, Size, fkShapeID, PartLength, fkApprovalID, 
							SupplyCondition, FreeIssue, UnitID, StdfkSupplierID, IssueInstruction, SPUfkUOMID,
							UnitID, Yield, TestPieces, StdDeliveryDays, AddedByfkUserID

				FROM		TemplateMaterial

				WHERE		pkOperationTemplateID = OperationTemplateID;

								

								SET		FI = (SELECT 	FreeIssue 
								FROM 	TemplateMaterial
								WHERE	pkOperationTemplateID = OperationTemplateID);

				IF 		ISNULL(FI) OR  FI <> 1 THEN
				
												INSERT INTO	RFQOrder
									(fkApprovalLevelID, fkUserID, CreateDate, LeadDays, Cancelled, Complete, POType)
						SELECT		fkApprovalID, UserID, Now(), StdDeliveryDays, 0, 0, 1

						FROM		TemplateMaterial

						WHERE		pkOperationTemplateID = OperationTemplateID;

						
						SET			RFQOid = Last_Insert_ID();
						

												INSERT INTO	OrderItem
									(fkRFQOID, ItemNo, fkPartTemplateID, fkShapeID, SPU, fkUOMID, Quantity, Description,
									Size, UnitID, AddedByfkUserID)
						SELECT		RFQOid, 1,(SELECT fkPartTemplateID
																FROM EngineeringTemplate
																WHERE pkEngineeringTemplateID = ETemplateNo), 
									fkShapeID, 1, SPUfkUOMID, 
																	((SELECT QuantityReqd + QuantityOvers
										FROM WorkOrder
										WHERE pkWorkOrderID = NewWONo)
									*1.1 + (SELECT TestPieces
											FROM TemplateMaterial
											WHERE pkOperationTemplateID = OperationTemplateID)
									/ (SELECT Yield
										FROM TemplateMaterial
										WHERE pkOperationTemplateID = OperationTemplateID))

									, Description, Size, UnitID, UserID

						FROM		TemplateMaterial

						WHERE		pkOperationTemplateID = OperationTemplateID;

												SET ItemStore = Last_Insert_ID();
												
						
												INSERT INTO	RFQn
									(fkRFQOID, AddedByfkUserID)
						VALUES		(RFQOid, UserID);

						

						
												INSERT INTO	OperationOrderItem
									(fkOperationID, fkItemID, AddedByfkUserID)
						VALUES		(OpStore, ItemStore, UserID);

						

												SET StdSuppID = (SELECT StdfkSupplierID
										FROM TemplateMaterial
										WHERE pkOperationTemplateID = OperationTemplateID);

						IF  StdSuppID IS NOT NULL THEN
								INSERT INTO	RFQOSupplier
											(fkRFQOID, fkSupplierID, Selected)
								SELECT		RFQOid, StdSuppID, 1

								FROM		TemplateMaterial

								WHERE		pkOperationTemplateID = OperationTemplateID;

								
						
						END IF;
				END IF;


		END IF;

		IF COperationTypeID = 2 THEN

				INSERT INTO	OperationMachine
							(fkOperationID, fkMachineID, Multi)
				SELECT		DISTINCT OpStore, fkMachineID, CLinked

				FROM		OpTmpltMachineProgram

				WHERE		fkOperationTemplateID = OperationTemplateID;

				

								INSERT INTO	OpMachineProgram
							(fkOperationID, fkMachineID, fkProgramNumber, DateAdded)
				SELECT		OpStore, fkMachineID, IFNULL(ProgramNumber, 999999), DATE(Now())

				FROM		OpTmpltMachineProgram

				WHERE		fkOperationTemplateID = OperationTemplateID;

				

												IF CEstimatedSetTime IS NOT NULL THEN
						INSERT INTO	MachineSet
									(fkOperationID, fkMachineID)
						SELECT		DISTINCT OpStore, fkMachineID

						FROM		OpTmpltMachineProgram

						WHERE		fkOperationTemplateID = OperationTemplateID;

										END IF;
				

		END IF;

				IF COperationTypeID = 3 THEN
				INSERT INTO	OperationTreatment
							(pkOperationID, fkTreatmentID, LeadTime, AddedByfkUserID)
				SELECT		OpStore, fkTreatmentID, LeadTime, UserID
				
				FROM		TemplateTreatment

				WHERE		pkOperationTemplateID = OperationTemplateID;

				

												
								INSERT INTO	RFQOrder
							(fkUserID, CreateDate, LeadDays, Cancelled, Complete, POType)
				SELECT		UserID, Now(), LeadTime, 0, 0, 2

				FROM		TemplateTreatment

				WHERE		pkOperationTemplateID = OperationTemplateID;

				
				SET			RFQOid = Last_Insert_ID();
				

								INSERT INTO	OrderItem
							(fkRFQOID, ItemNo, fkPartTemplateID, Quantity, Description, AddedByfkUserID)
				SELECT		RFQOid, 1,(SELECT fkPartTemplateID
														FROM EngineeringTemplate
														WHERE pkEngineeringTemplateID = ETemplateNo), 
														(SELECT QuantityReqd + QuantityOvers
								FROM WorkOrder
								WHERE pkWorkOrderID = NewWONo)
							*1.1
							, Description , UserID

				FROM		TemplateTreatment TT1
				INNER JOIN	TreatmentList TL1
				ON			TT1.fkTreatmentID = TL1.pkTreatmentID

				WHERE		pkOperationTemplateID = OperationTemplateID;

								SET ItemStore = Last_Insert_ID();
								
				
								INSERT INTO	RFQn
							(fkRFQOID, AddedByfkUserID)
				VALUES		(RFQOid, UserID);

				

				
								INSERT INTO	OperationOrderItem
							(fkOperationID, fkItemID, AddedByfkUserID)
				VALUES		(OpStore, ItemStore, UserID);

				
								SET StdSuppID = (SELECT StandardSupplierID
								FROM TemplateTreatment
																								WHERE pkOperationTemplateID = OperationTemplateID);

				IF  StdSuppID IS NOT NULL THEN
						INSERT INTO	RFQOSupplier
									(fkRFQOID, fkSupplierID, Selected)
												VALUES		(RFQOid, StdSuppID, 1);
						
						
										
						
				END IF;

		END IF;

				INSERT INTO	OperationProcess
					(fkOperationID, ProcessNote)
		SELECT		OpStore, ProcessNote

		FROM		TemplateProcess

		WHERE		fkOperationTemplateID = OperationTemplateID;
		
		
				INSERT INTO	PONote
					(fkRFQOID, Note)
		SELECT		RFQOid, Comment

		FROM		TemplateApprovalComments

		WHERE		fkOperationTemplateID = OperationTemplateID;

		

				INSERT INTO	POComment
					(fkRFQOID, Comment, Deleted)
		SELECT		RFQOid, Comment, 0

		FROM		TemplatePOComments

		WHERE		fkOperationTemplateID = OperationTemplateID;

		

				INSERT INTO	OperationApprovalComments
					(fkOperationID, Comment)
		SELECT		OpStore, Comment

		FROM		TemplateApprovalComments

		WHERE		fkOperationTemplateID = OperationTemplateID;

				INSERT INTO	OperationNote
					(fkOperationID, Note, fkNoteTypeID, CreateDate, AddedByfkUserID)
		SELECT		OpStore, Note, fkNoteTypeID, CurDate(), UserID

		FROM		TemplateNote

		WHERE		fkOperationTemplateID = OperationTemplateID;


  END LOOP;

	

  CLOSE OpTmpltCursor;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCreateOutvoiceCredit` (IN `UserID` SMALLINT UNSIGNED, `InvoiceNo` INT UNSIGNED, `CoCReqd` TINYINT UNSIGNED, `Quantity` SMALLINT UNSIGNED, `IdentMarks` VARCHAR(20))  BEGIN
INSERT INTO		OutvoiceCredit
				(fkInvoiceID, Quantity, CreatedByfkUserID, CoCReqd, IdentMarks)
VALUES			(InvoiceNo, Quantity, UserID, CocReqd, IdentMarks);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCreateWOOutvoice` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED, `InvoiceNo` INT UNSIGNED)  BEGIN
INSERT INTO		WOOutvoice
				(fkInvoiceID, fkWorkOrderID, fkUserID)
VALUES			(InvoiceNo, WorkOrderID, UserID);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCreateWorkOrder` (IN `LineItem` MEDIUMINT UNSIGNED, `ApprovalID` TINYINT UNSIGNED, `QtyReqd` SMALLINT UNSIGNED, `QtyOvers` SMALLINT UNSIGNED, `PriceEach` DECIMAL(7,2), `DeliveryDate` DATE, `FAIR` TINYINT UNSIGNED, `Live` TINYINT UNSIGNED, OUT `NewWONo` MEDIUMINT UNSIGNED)  BEGIN
INSERT INTO			WorkOrder
					(fkLineItemID, fkApprovalID, QuantityReqd, QuantityOvers, PriceEach, OrigDeliveryDate, FAIR, Live)
VALUES				(LineItem, ApprovalID, QtyReqd, QtyOvers, PriceEach, DeliveryDate, FAIR, Live);

SELECT				Last_Insert_ID() INTO NewWONo;

CALL				ProcCreateOpsFromTemplate(NewWONo);

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCustomerDetail` (IN `CustomerID` SMALLINT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkCustomerID, CustomerSageCode AS FinanceCode, CustomerName, Alias, Address1,
			Address2, Address3, Address4, Postcode, Phone, Facsimile, Website, Email,
			InvAddress1, InvAddress2, InvAddress3, InvAddress4, InvPostcode, 
			FDApproval, MDApproval, GMApproval, CreditLimit, MarkUpQuote
FROM		Customer
WHERE		Deleted <> 1 AND pkCustomerID = CustomerID;
			
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCustomerList` ()  BEGIN
SELECT		pkCustomerID, Alias

FROM		Customer
WHERE		Deleted <> 1;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCustomerOrderList` (IN `CustomerID` SMALLINT)  BEGIN
SELECT		pkOrderID AS OrderID, CustomerOrderRef
FROM		CustomerOrder

WHERE		fkCustomerID = CustomerID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCustomers` ()  BEGIN
SELECT		pkCustomerID, Alias

FROM		Customer
WHERE		Deleted <> 1;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcCustomerSearch` (IN `CustomerString` CHAR(50))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		CustomerName
FROM		Customer

WHERE		Deleted <> 1 AND CustomerName LIKE CustomerString;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeleteApproval` (IN `UserID` SMALLINT UNSIGNED, `ApprovalID` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
DELETE FROM		Approval 
				
WHERE			pkApprovalID = ApprovalID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeleteBranch` (IN `UserID` SMALLINT UNSIGNED, `BranchID` TINYINT UNSIGNED)  BEGIN
UPDATE		BranchLocation
SET			Deleted = 1, AddedByfkUserID = UserID

WHERE		pkBranchID = BranchID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeleteBusinessShutdown` (IN `UserID` SMALLINT UNSIGNED, `ShutdownID` TINYINT UNSIGNED)  BEGIN
DELETE FROM			BusinessShutdown

WHERE				pkShutdownID = ShutdownID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeleteCustomerAdditional` (IN `UserID` SMALLINT UNSIGNED, `AdditionalID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		CustomerAdditionalTime
SET			Deleted = 1, UpdatedByfkUserID = UserID

WHERE		pkAdditionalID = AdditionalID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeleteCustomerContact` (IN `UserID` SMALLINT UNSIGNED, `ContactID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		CustomerContact
SET			Deleted = 1, fkUserIDUpdated = UserID 
			
WHERE		pkContactID = ContactID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeleteCustomerNote` (IN `UserID` SMALLINT UNSIGNED, `NoteID` MEDIUMINT UNSIGNED)  BEGIN
UPDATE		CustomerNote
SET			Deleted = 1, AddedByfkUserID = UserID

WHERE		pkCustomerNoteID = NoteID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeleteDeviationReason` (IN `UserID` SMALLINT UNSIGNED, `DeviationID` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
DELETE FROM 	DeviationType

WHERE			(pkDeviationTypeID = DeviationID);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeleteDocumentDropType` (IN `UserID` SMALLINT UNSIGNED, `DropTypeID` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
DELETE FROM 		DocumentDropType 
				
WHERE				pkDocumentDropTypeID = DropTypeID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeleteMachiningType` (IN `UserID` SMALLINT UNSIGNED, `MachiningTypeID` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE			MachiningType

SET				Deleted = 1

WHERE			pkMachiningTypeID = MachiningTypeID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeleteMaterialShape` (IN `UserID` SMALLINT UNSIGNED, `ShapeID` TINYINT UNSIGNED)  BEGIN
UPDATE 	Shape

SET			Deleted = 1, fkUserID = UserID

WHERE		pkShapeID = ShapeID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeleteOurApproval` (IN `UserID` SMALLINT UNSIGNED, `OurApprovalID` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
DELETE FROM		OurApproval 
				
WHERE			pkApprovalID = OurApprovalID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeleteSundry` (IN `UserID` SMALLINT UNSIGNED, `SundryID` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE			Sundry
	
SET				Deleted= 1
WHERE			pkSundryID = sundryID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeleteTreatment` (IN `UserID` SMALLINT UNSIGNED, `TreatmentID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE			TreatmentList
SET				Deleted = 1, DeletedByfkUserID = UserID

WHERE			pkTreatmentID = TreatmentID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeleteUOM` (IN `UserID` SMALLINT UNSIGNED, `UOMID` TINYINT UNSIGNED)  BEGIN
DELETE FROM		UnitOfMeasure

WHERE			pkUOMID = UOMID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeleteWebsite` (IN `UserID` SMALLINT UNSIGNED, `WebsiteID` TINYINT UNSIGNED)  BEGIN
UPDATE			WebsiteAccess

SET				Deleted = 1, DeletedByfkUserID = UserID

WHERE			pkWebsiteID = WebsiteID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeliveryCommentsList` ()  BEGIN
SELECT		pkDeliveryCommentID AS DeliveryCommentID, DeliveryComment

FROM		DeliveryComment

WHERE		Deleted <> 1 AND Deleted IS NOT NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeliveryNoteRpt` (IN `WorkOrderID` MEDIUMINT UNSIGNED, IN `POnID` SMALLINT UNSIGNED, IN `GRBNo` INT UNSIGNED, IN `InvoiceNo` INT UNSIGNED)  NO SQL
BEGIN
DECLARE			TestRemark		varchar(80) DEFAULT 'Advice note remark';
-- 08/11/2016 Kythera - data for the delivery note report
-- 10/11/2016 pon

IF	-- not a treatment
	InvoiceNo IS NOT NULL
THEN
	SELECT		DISTINCT C.Alias AS Name, C.Address1, C.Address2, C.Address3, C.Address4, C.Postcode, 
				pkInvoiceID AS InvoiceNo, GI2.IRNoteNumber AS CertNo, CustomerOrderRef AS ContractNo, 
				'n/a' AS SCOrderNo, LI.CustomerReference AS LineNo, 'Yes' AS OrderComplete, 
				pkWorkOrderID, D.DispatchDate, CustomerSageCode AS AccountNo, OV.Quantity,
				PT.Description, TestRemark AS Remarks

	FROM		GoodsIn			GI
	INNER JOIN	WorkOrder		WO
	ON			GI.WorkOrderID = WO.pkWorkOrderID
	INNER JOIN	Operation		O
	ON			WO.pkWorkOrderID = O.fkWorkOrderID
	INNER JOIN	LineItem		LI
	ON			WO.fkLineItemID = LI.pkLineItemID
	INNER JOIN	CustomerOrder	CO
	ON			LI.fkOrderID = CO.pkOrderID
	INNER JOIN	Customer		C
	ON			CO.fkCustomerID = C.pkCustomerID
 				
	LEFT JOIN	PartTemplate 	PT	
	ON			LI.fkPartTemplateID = PT.pkPartTemplateID

	LEFT JOIN	Dispatch		D
	ON			GI.pkGRBID = D.fkGRBID

	LEFT JOIN 	Allocated 		A
	ON			O.pkOperationID = A.fkOperationID
	LEFT JOIN	GoodsIn		GI2
	ON			A.fkGRBID = GI2.pkGRBID

	inner JOIN	WOOutvoice		WOV
	ON			WO.pkWorkOrderID = WOV.fkWorkOrderID
    inner join Outvoice OV 
    ON			WOV.fkInvoiceID = OV.pkInvoiceID

	LEFT JOIN	Material		M
	ON			GI.pkGRBID = M.fkGRBID

	WHERE		WO.pkWorkOrderID = WorkOrderID
	AND			OV.pkInvoiceID = InvoiceNo;


ELSE -- its a treatment

	SELECT		DISTINCT S.Alias AS Name, S.Address1, S.Address2, S.Address3, S.Address4, S.Postcode, 
				'n/a' AS InvoiceNo, GI.IRNoteNumber AS CertNo, 'n/a' AS ContractNo, pkPOnID AS SCOrderNo, 1 AS LineNo, 
				'No' AS OrderComplete, O.fkWorkOrderID AS WONo, 
				D.DispatchDate, 'n/a' AS AccountNo, O.StartQty AS Qty, OI.Description, RO.Reference AS Remarks

	FROM		Pon P
	INNER JOIN	OrderItem OI
	ON			P.fkRFQOID = OI.fkRFQOID
	LEFT JOIN	RFQOrder RO
	ON			OI.fkRFQOID = RO.pkRFQOID
	INNER JOIN	RFQOSupplier RS
	ON			OI.fkRFQOID = RS.fkRFQOID
	INNER JOIN	Supplier S
	ON			RS.fkSupplierID = S.pkSupplierID
	-- INNER JOIN	POGoods	POG
	-- ON			OI.pkItemID = POG.fkItemID
	LEFT JOIN	Dispatch	D
	ON			P.pkPOnID = D.fkPOnID
	LEFT JOIN	GoodsIn GI
	ON			D.fkGRBID = GI.pkGRBID

	LEFT JOIN	OperationOrderItem	OOI
	ON			OI.pkItemID = OOI.fkItemID
	LEFT JOIN	Operation	O
	ON			OOI.fkOperationID = O.pkOperationID

	WHERE		RS.Selected = 1
	AND			P.pkPOnID = POnID;

END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDeliveryNoteRpt2` (IN `WorkOrderID` MEDIUMINT UNSIGNED, `POnID` SMALLINT UNSIGNED, `GRBNo` INT UNSIGNED)  BEGIN



	SELECT		'name' AS Name, 'Address1 sxxxxxxxxxxxxxe' AS Address1, 'Address2 sxxxxxxxxxxxxxe' AS Address2, 'Address3 sxxxxxxxxxxxxxe' AS Address3, 
				'Address4 sxxxxxxxxxxxxxe' AS Address4, 'Postcode' AS Postcode, 
				10001 AS InvoiceNo, 'ABC123ST16CN' AS CertNo, 'ABC123ST16' AS ContractNo, 
				12345 AS SCOrderNo, 9191 AS LineNo, 'Yes' AS OrderComplete, 
				10101 AS pkWorkOrderID, '2017-07-07' AS DispatchDate, 'A1' AS AccountNo, 567 AS RcvdQuantity,
				'A380 teatray' AS Description, 'Super long Test Remark' AS Remarks;


END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDispatched` (IN `UserID` SMALLINT UNSIGNED, IN `OperationID` INT UNSIGNED)  BEGIN

DECLARE		Result		tinyint DEFAULT -1;
DECLARE		GRBStore	int unsigned;
DECLARE		WOStore		mediumint unsigned;

SET			WOStore		= (SELECT WorkOrderID 
								FROM Operation
								WHERE pkOperationID = OperationID);

SET 		GRBStore 	= IF(ISNULL((SELECT fkGRBID FROM Allocated WHERE fkOperationID = OperationID)),
									(SELECT pkGRBID FROM GoodsIn WHERE WorkOrderID = (WOStore)),
									(SELECT fkGRBID FROM Allocated WHERE fkOperationID = OperationID));

UPDATE		Dispatch
SET			DispatchDate = now(), DispatchByfkUserID = UserID

WHERE		fkGRBID = GRBStore;

SELECT row_count() INTO Result;

-- IF		Result > 0 THEN
-- 		UPDATE		WorkOrder 
-- 		SET 		Complete = 1

-- 		WHERE		pkWorkOrderID = WOStore;
-- END IF;

IF		
			OperationID IN (SELECT pkOperationID FROM OperationTreatment)
THEN
			UPDATE		OperationTreatment
			SET			DispatchDate = now(), DispatchByfkUserID = UserID

			WHERE		pkOperationID = OperationID;

			SELECT row_count() INTO Result;

END IF; 

SELECT Result;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDropDeliveryDocument` (IN `UserID` SMALLINT UNSIGNED, `GRBNo` INT UNSIGNED, `DocTypeID` TINYINT UNSIGNED, `FileName` VARCHAR(30))  BEGIN
INSERT INTO		GoodsDocument
				(Handle, fkGRBID, DocumentType, DroppedByfkUserID)
VALUES			(FileName, GRBNo, DocTypeID, UserID);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDropInvoice` (IN `UserID` SMALLINT UNSIGNED, `FileName` VARCHAR(20), `InvoiceNo` VARCHAR(25), `ActualTotal` DECIMAL(9,2), `InvoiceOK` TINYINT UNSIGNED)  BEGIN
DECLARE			Result tinyint DEFAULT -1;

IF
	(SELECT 	COUNT(pkInvoiceID)
	FROM 		Invoice

	WHERE 		ClientInvoiceNumber = InvoiceNo) > 0

THEN
	SET 		Result = -2 ;
	
ELSE
	INSERT INTO		Invoice
					(ClientInvoiceNumber, RcvdDate, RcvdByfkUserID, FileName, InvoiceOK, ActualTotal)
	VALUES			(InvoiceNo, Now(), UserID, FileName, InvoiceOK, ActualTotal);

	 	 	 	
	SET Result =	Last_Insert_ID();

END IF;

	SELECT Result AS InvoiceID;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcDueInReport` ()  BEGIN

SELECT		DISTINCT SupplierName AS Supplier, pkPOnID AS PONumber, OI.Description

FROM		POn				PO
INNER JOIN	OrderItem 		OI
ON			PO.fkRFQOID = OI.fkRFQOID
INNER JOIN	RFQOSupplier 	RS
ON			OI.fkRFQOID = RS.fkRFQOID
INNER JOIN	Supplier		S
ON			RS.fkSupplierID = S.pkSupplierID

WHERE		OI.pkItemID NOT IN (SELECT fkItemID FROM POGoods)
AND			Selected = 1;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcEndMaterialIssue` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE	OpID				int unsigned;
DECLARE	Qty					smallint unsigned;

SET OpID = (SELECT pkOperationID 
								FROM Operation O
								INNER JOIN OperationMachine OM
								ON O.pkOperationID = OM.fkOperationID
																WHERE O.fkWorkOrderID = WorkOrderID
								AND O.fkOperationtypeID = 1
								AND (O.Bypass <> 1 OR ISNULL(O.Bypass) = TRUE)
								AND (O.Subcon <> 1 OR ISNULL(O.Subcon) = TRUE)
								AND ISNULL(OM.RunEnd));

UPDATE		OperationMachine
SET			fkUserIDRunEnd = UserID, RunEnd = now()

WHERE		fkOperationID = OpID;

SET			Qty = (SELECT StartQty
					FROM Operation
					WHERE pkOperationID = OpID);

UPDATE		Operation
SET			EndQty = Qty

WHERE		pkOperationID = OpID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcEngineeringDocDrop` (IN `OperationID` INT, IN `ImageFile` BLOB)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
Insert INTO Kythera.OpADD (fkOperationID, Document) VALUES(OperationID, ImageFile );
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcEngineeringToReview` (IN `UserID` SMALLINT UNSIGNED, `ETemplateID` SMALLINT UNSIGNED, `PartNumber` VARCHAR(25))  BEGIN

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		Todo 
				(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, PartNumber, fkEngineeringTemplateID,
				Complete, Description)
VALUES			(Date(Now()), 4, UserID, 4, PartNumber, ETemplateID, 0, 'Review engineering');
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcEngToReview` (IN `UserID` SMALLINT, IN `PartNumber` VARCHAR(25))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		Kythera.Todo 
				(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, PartNumber)
VALUES			(Date(Now()), 4, UserID, 4, PartNumber);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcFixtureList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		F.GRBNumber, L.Location, F.FixtureNumber, F.Description, F.FixtureStatus
FROM 		Fixture	F
LEFT JOIN	Location L
ON			F.fkLocationID = L.pkLocationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGenerateNewGRB` (IN `UserID` SMALLINT UNSIGNED)  BEGIN
SELECT		MAX(pkGRBID) + 1 AS GRBNo 

FROM		GoodsIn;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetAcknowNotes` (IN `OrderID` SMALLINT UNSIGNED)  BEGIN
SELECT			Note, NoteDate

FROM			AcknowledgementNote
WHERE			fkOrderID = OrderID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetActionTypeList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkActionTypeID AS ActionTypeID, Description AS ActionType

FROM		ActionType

WHERE		Deleted <> 1 OR Deleted IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetADDList` (IN `OperationTemplateID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkAPDID AS ADDID, FileHandle AS FileName, DropType AS DocType, AutoPrint

FROM		AssociatedPartDoc APD
INNER JOIN	DocumentDropType DDT
ON			APD.fkDocTypeID = DDT.pkDocumentDropTypeID

WHERE		fkPartTemplateID = (SELECT fkPartTemplateID 
									FROM OperationTemplate OT
									INNER JOIN EngineeringTemplate ET
									ON OT.fkEngineeringTemplateID = ET.pkEngineeringTemplateID
									WHERE pkOperationTemplateID = OperationTemplateID);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetAllApprovalList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		pkApprovalID, Description
				
FROM			Approval
WHERE			Deleted <> 1;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetAllInvoiceList` ()  BEGIN
SELECT		pkInvoiceID AS InvoiceID, ClientInvoiceNumber AS InvoiceRef

FROM		Invoice;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetApprovalList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		pkApprovalID, Description
				
FROM			Approval
WHERE			Deleted <> 1;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetApprovalNadcapList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
	(SELECT 		pkApprovalID, Description
	FROM		approval
	ORDER BY	Description)
UNION
	(SELECT		pkApprovalID, Description
	FROM		nadcapapproval
	ORDER BY	Description);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetBranches` ()  BEGIN
SELECT		pkBranchID AS BranchID, Name AS BranchName

FROM		BranchLocation

WHERE		Deleted <> 1 OR Deleted IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetBusinessShutdowns` ()  BEGIN
SELECT		pkShutdownID AS ShutdownID, Shutdate, ShutReason
FROM		BusinessShutdown;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetComments` ()  BEGIN
SELECT		pkCommentID AS CommentID, Comments AS Comment

FROM		Comments

WHERE		Deleted <> 1 OR Deleted IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetCourierList` ()  BEGIN
SELECT		pkCourierID AS CourierID, Name

FROM		Courier

WHERE		Deleted <> 1 OR Deleted IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetCustAdditional` (IN `CustomerID` SMALLINT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		IF(TimeType=1, 'Shutdown', IF(TimeType=2, 'Collection', IF(Timetype=3,'Other', ''))) AS Type, StartPoint, EndPoint
			
FROM		CustomerAdditionalTime

WHERE		fkCustomerID = CustomerID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetCustDeliveryPoint` (IN `CustomerID` SMALLINT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		Address1, Address2, Address3, Address4, Postcode,
			DC.ContactName, DC.Phone
			
FROM		DeliveryPoint DP
LEFT JOIN	DeliveryContact DC
ON			DP.pkDeliveryPointID = DC.fkDeliveryPointID

WHERE		DP.fkCustomerID = CustomerID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetCustDeliveryPoints` (IN `CustomerID` SMALLINT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		DP.pkdeliveryPointID AS DPID, Address1, Address2, Address3, Address4, Postcode,
			DC.pkDeliveryContactID AS DCID, DC.ContactName, DC.Phone
			
FROM		DeliveryPoint DP
LEFT JOIN	DeliveryContact DC
ON			DP.pkDeliveryPointID = DC.fkDeliveryPointID

WHERE		DP.fkCustomerID = CustomerID
AND			(DP.Deleted <> 1 OR Deleted IS NULL);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetCustomerAdditional` (IN `CustomerID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkAdditionalID AS AdditionalID, IF(TimeType=1, 'Shutdown', IF(TimeType=2, 'Collection', IF(Timetype=3,'Other', ''))) AS Type, 
			StartPoint, EndPoint
			
FROM		CustomerAdditionalTime

WHERE		fkCustomerID = CustomerID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetCustomerConformityNotes` (IN `CustomerID` SMALLINT UNSIGNED)  BEGIN
SELECT		Note AS ConformityNote
FROM		StdConformityNote

WHERE		fkCustomerID = CustomerID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetCustomerContact` (IN `CustomerID` SMALLINT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		ContactName, Phone, Email, AcknowContact,
			PrimaryContact 
			
FROM		CustomerContact CC

WHERE		fkCustomerID = CustomerID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetCustomerContacts` (IN `CustomerID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkContactID AS ContactID, ContactName, Position, Phone, Extension, Email, AcknowContact,
			PrimaryContact 
			
FROM		CustomerContact CC

WHERE		fkCustomerID = CustomerID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetCustomerDetail` (IN `CustomerID` SMALLINT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkCustomerID, CustomerSageCode AS FinanceCode, CustomerName, Alias, Address1,
			Address2, Address3, Address4, Postcode, Phone, Facsimile, Website, Email,
			InvAddress1, InvAddress2, InvAddress3, InvAddress4, InvPostcode, VATRate, 
			FDApproval, MDApproval, GMApproval, CreditLimit, MarkUpQuote, PaymentTerms,
			PaymentRunFrequency, fkApprovalID, DeliveryCharge
			, IrishCurrency, USCurrency, EuroCurrency, DeliveryGrace
			
FROM		Customer C
WHERE		C.Deleted <> 1 AND pkCustomerID = CustomerID;
			
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetCustomerDetail2` (IN `CustomerID` SMALLINT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkCustomerID, CustomerSageCode AS FinanceCode, CustomerName, Alias,
			Phone, Facsimile, Day1Open, Day1Close, Day2Open, Day2Close, Day3Open,
			Day3Close, Day4Open, Day4Close, Day5Open, Day5Close, Day6Open, Day6Close,
			Day7Open, Day7Close, F.Note AS FreeformNote
			
FROM		Customer C
LEFT JOIN	FreeformNote F
ON			C.pkcustomerID = F.fkCustomerID
WHERE		C.Deleted <> 1 AND pkCustomerID = CustomerID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetCustomerHistory` (IN `CustomerID` SMALLINT)  BEGIN
SELECT		CreationDate AS Raised, NewDeliveryDate AS Completed, pkOrderID AS CONo, 
			pkWorkOrderID AS WONo, PartNumber AS PartNo, QuantityReqd AS Qty, 99 AS Days, 
			CON.Note AS OrderNote, PreventActionNote AS RejectAction , DeviationType AS "Reject reason"

FROM		CustomerOrder		CO
LEFT JOIN	LineItem 			LI	ON	CO.pkOrderID = LI.fkOrderID
LEFT JOIN	WorkOrder 			WO	ON	LI.pkLineItemID = WO.fkLineItemID
LEFT JOIN	PartTemplate 		PT	ON	LI.fkPartTemplateID = PT.pkPartTemplateID
LEFT JOIN	CustomerOrderNote 	CON	ON	CO.pkOrderID = CON.fkOrderID
LEFT JOIN	ExternalReject		ER	ON	WO.pkWorkOrderID = ER.fkWorkOrderID
LEFT JOIN	DeviationType		DT	ON	ER.fkDeviationTypeID = DT.pkDeviationTypeID

WHERE		fkCustomerID = CustomerID;		
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetCustomerNotes` (IN `CustomerID` SMALLINT UNSIGNED)  BEGIN
SELECT		pkCustomerNoteID AS NoteID, Note

FROM		CustomerNote

WHERE		fkCustomerID = CustomerID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetCustomerOrder` (IN `OrderID` INT UNSIGNED)  BEGIN
SELECT			CustomerOrderRef, fkApprovalID, fkDeliveryPointID, 
				CommerciallyViable, AmendmentReviewed

FROM			CustomerOrder
WHERE			pkOrderID = OrderID;
				
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetCustomerOrderHistory` (IN `CustomerID` SMALLINT UNSIGNED)  BEGIN
SELECT		fkPartTemplateID AS PartID, CreationDate AS Raised, NewDeliveryDate AS Completed, pkOrderID AS CONo

FROM		CustomerOrder		CO
LEFT JOIN	LineItem 			LI	ON	CO.pkOrderID = LI.fkOrderID
INNER JOIN	WorkOrder 			WO	ON	LI.pkLineItemID = WO.fkLineItemID

WHERE		fkCustomerID = CustomerID;		
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetCustomerSupplierList` ()  BEGIN
SELECT		pkSupplierID AS ID, 'S' AS Type, Alias

FROM		Supplier

WHERE		Deleted <> 1 OR Deleted IS NULL

UNION

SELECT		pkCustomerID AS ID, 'C' AS Type, Alias

FROM		Customer

WHERE		Deleted <> 1 OR Deleted IS NULL

ORDER BY	Alias;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetCustOrderNotes` (IN `OrderID` SMALLINT UNSIGNED)  BEGIN
SELECT			Note, NoteDate

FROM			CustomerOrderNote
WHERE			fkOrderID = OrderID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetDeviationList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		pkDeviationTypeID AS DeviationID, DeviationType AS Deviation
				
FROM		DeviationType

WHERE		Deleted <> 1 AND Deleted IS NOT NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetDispatchDetail` (IN `WorkOrderID` MEDIUMINT UNSIGNED, IN `POnID` SMALLINT UNSIGNED, IN `GRBNo` INT UNSIGNED)  BEGIN

IF		WorkOrderID IS NOT NULL

THEN 
	SELECT		DISTINCT PT.Description, C.Alias AS Name, C.Address1, C.Address2, C.Address3, C.Address4, C.Postcode, 
				D.SpecialInstruction,  D.CarriageTypeID AS CarriageTypeID, D.fkCourierID AS CourierID

	FROM		GoodsIn			GI
	INNER JOIN	WorkOrder		WO
	ON			GI.WorkOrderID = WO.pkWorkOrderID
	INNER JOIN	Operation		O
	ON			WO.pkWorkOrderID = O.fkWorkOrderID
	INNER JOIN	LineItem		LI
	ON			WO.fkLineItemID = LI.pkLineItemID
	INNER JOIN	CustomerOrder	CO
	ON			LI.fkOrderID = CO.pkOrderID
	INNER JOIN	Customer		C
	ON			CO.fkCustomerID = C.pkCustomerID
 				
	LEFT JOIN	PartTemplate 	PT	
	ON			LI.fkPartTemplateID = PT.pkPartTemplateID

	LEFT JOIN	Dispatch		D
	ON			GI.pkGRBID = D.fkGRBID

-- 	INNER JOIN 	Allocated 		A
-- 	ON			O.pkOperationID = A.fkOperationID
-- 	LEFT JOIN	Dispatch		D2
-- 	ON			A.fkGRBID = D2.fkGRBID

	WHERE		WO.pkWorkOrderID = WorkOrderID
	AND			GI.pkGRBID = GRBNo;


ELSE 
	SELECT		DISTINCT OI.Description, S.Alias AS Name, S.Address1, S.Address2, S.Address3, S.Address4, S.Postcode, 
				D.SpecialInstruction, D.CarriageTypeID, D.fkCourierID AS CourierID

	FROM		Pon P
	INNER JOIN	OrderItem OI
	ON			P.fkRFQOID = OI.fkRFQOID
	INNER JOIN	RFQOSupplier RS
	ON			OI.fkRFQOID = RS.fkRFQOID
	INNER JOIN	Supplier S
	ON			RS.fkSupplierID = S.pkSupplierID
			LEFT JOIN	Dispatch	D
	ON			P.pkPOnID = D.fkPOnID

	WHERE		RS.Selected = 1
	AND			P.pkPOnID = POnID;
	
END IF;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetDispatchList` ()  BEGIN

SELECT		  (SELECT MAX(pkOperationID) 
                           FROM Operation 
                           WHERE fkWorkOrderID = WO.pkWorkOrderID) AS OperationID, WO.pkWorkOrderID AS WorkOrderID, C.Alias AS Customer, 
												OV.Quantity AS Qty, 
			GI.pkGRBID AS GRBNo, CONCAT(LEFT(SA.StoreArea, 1), '-', XCoord, YCoord) AS Location, pkInvoiceID


FROM		Outvoice OV
INNER JOIN	WOOutvoice WOO
ON			OV.pkInvoiceID = WOO.fkInvoiceID
left JOIN	WorkOrder WO
ON			WOO.fkWorkOrderID = WO.pkworkOrderID



INNER JOIN 	GoodsIn GI
ON			WO.pkWorkOrderID = GI.WorkOrderID
INNER JOIN	LineItem	LI
ON			WO.fkLineItemID = LI.pkLineItemID
INNER JOIN	CustomerOrder CO
ON			LI.fkOrderID = CO.pkOrderID
INNER JOIN	Customer C
ON			CO.fkCustomerID = C.pkCustomerID

left JOIN	MaterialLocation ML 
ON			GI.pkGRBID = ML.fkMaterialID
inner JOIN	StoreArea SA
ON			ML.fkStoreAreaID = SA.pkStoreAreaID
inner JOIN	XCoordinate		X
ON			ML.fkXCoordID = X.pkXCoordID
inner JOIN	YCoordinate		Y
ON			ML.fkYCoordID = Y.pkYCoordID

WHERE		WO.Live = 1 AND WO.Complete = 1

	AND		ISNULL(OV.SentDate);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetDocumentDropTypes` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		pkDocumentDropTypeID, DropType
				
FROM			DocumentDropType

WHERE			Deleted <> 1 AND Deleted IS NOT NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetFat` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		FatType AS FatTypeID, IF(FatType = 1, 'Between Operations',
				IF(FatType = 2, 'Delivery', '')) AS FATType, FatDays AS DelDays

FROM		FAT;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetFinanceAdmin` ()  BEGIN
DECLARE 	VATRate tinyint unsigned DEFAULT (SELECT Rate AS VATRate FROM Rate WHERE pkRateID = 1);

DECLARE 	FAIRCharge tinyint unsigned DEFAULT (SELECT Rate AS FAIRCharge FROM Rate WHERE pkRateID = 2);

DECLARE	MaterialMarkup tinyint unsigned DEFAULT (SELECT Rate AS MaterialMarkup FROM Rate WHERE pkRateID = 3);

DECLARE	USD	decimal(5,4) unsigned DEFAULT (SELECT Rate AS USD FROM ExchangeRate WHERE pkExchangerateID = 1);

DECLARE	Euros decimal(5,4) unsigned DEFAULT (SELECT Rate AS Euros FROM ExchangeRate WHERE pkExchangerateID = 2);

DECLARE	IrishPounds	decimal(5,4) unsigned DEFAULT (SELECT Rate AS IrishPounds FROM ExchangeRate WHERE pkExchangerateID = 3);
		
SELECT VATRate, FAIRCharge, MaterialMarkup, USD, Euros, IrishPounds;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetFuckingWODetail` (IN `workOrder` MEDIUMINT UNSIGNED)  BEGIN
SELECT	DISTINCT	pkPartTemplateID AS PartNumber, PT.IssueNumber AS Issue, D.Reference AS DrawingNumber, 
					D.IssueNumber AS DrgIssue, M.pkMachineID AS MachineID, CONCAT(M.Manufacturer, ': ', M.Model) AS Machine, 
					OM.fkMaterialID AS GRBID, 
					LI.fkOrderID AS OrderID, WO.pkWorkOrderID AS WorkOrderID, WO.QuantityReqd AS QtyOff, WO.FastTrack
					 

FROM			WorkOrder WO
INNER JOIN		LineItem LI
ON				WO.fkLineItemID = LI.pkLineItemID
INNER JOIN		Operation O
ON				WO.pkWorkOrderID = O.fkWorkOrderID
LEFT JOIN		OperationMaterial OM
ON				O.pkOperationID = OM.pkOperationID
LEFT JOIN		PartTemplate PT
ON				LI.fkPartTemplateID = PT.pkPartTemplateID
INNER JOIN		Drawing D
ON				PT.fkDrawingID = D.pkDrawingID
LEFT JOIN		OperationMachine OMA
ON				O.pkOperationID = OMA.fkOperationID
INNER JOIN		Machine M
ON				OMA.fkMachineID = M.pkMachineID
INNER JOIN		OperationMaterial OPM
ON				O.pkOperationID = OPM.pkOperationID

WHERE			WO.pkWorkOrderID = WorkOrder;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetGoodsInList` ()  BEGIN
SELECT		pkGRBID AS GRBID

FROM		GoodsIn

WHERE		Inspected <> 1 OR Inspected IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetIncompleteInvoicedWOs` ()  BEGIN

DECLARE			CursQuantityReqd 					smallint unsigned;
DECLARE			OpEndQty 							int unsigned DEFAULT 0;
DECLARE			CursWorkOrderID						mediumint unsigned;
DECLARE			QtyStore							int unsigned DEFAULT 0;
DECLARE			CursCustomerName					varchar(50);
DECLARE			CursCreationDate					datetime;
DECLARE 		done 								INT DEFAULT FALSE;

DECLARE WOCursor CURSOR FOR 
		SELECT		DISTINCT WO.pkWorkOrderID , CustomerName, QuantityReqd, 
					CO.CreationDate

		FROM		WorkOrder WO
		LEFT JOIN	WOOutvoice WOV
		ON			WO.pkWorkOrderID = WOV.fkWorkOrderID
		LEFT JOIN	Outvoice OV
		ON			WOV.fkInvoiceID = OV.pkInvoiceID
		INNER JOIN	LineItem LI
		ON			WO.fkLineItemID = LI.pkLineItemID
		INNER JOIN	CustomerOrder CO
		ON			LI.fkOrderID = CO.pkOrderID
		INNER JOIN	Customer C
		ON			CO.fkCustomerID = C.pkCustomerID

				WHERE		WO.Complete = 1;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

BEGIN
DROP TEMPORARY TABLE IF EXISTS WosToInvoice;
CREATE TEMPORARY TABLE WosToInvoice
		(WONo mediumint unsigned, CustomerName varchar(100), Qty bigint unsigned, Created datetime);
END;

  OPEN WOCursor;

read_loop: LOOP
		FETCH WOCursor INTO CursWorkOrderID, CursCustomerName, CursQuantityReqd, CursCreationDate;

		IF 			done THEN
					LEAVE read_loop;

		END IF;

		SET 		OpEndQty = (SELECT MIN(EndQty) 

		FROM 		Operation
		
		WHERE		fkWorkOrderID = CursWorkOrderID);


		SET			QtyStore = (SELECT	SUM(Quantity)

		FROM		Outvoice OV
		INNER JOIN	WOOutvoice WOV
		ON			OV.pkInvoiceID = WOV.fkInvoiceID
		
		WHERE		fkWorkOrderID = CursWorkOrderID);

		IF QtyStore IS NULL THEN 
					SET QtyStore = 0; 

		END IF;

		IF			OpEndQty - QtyStore > 0 THEN

				
					INSERT INTO WosToInvoice
					(SELECT	CursWorkOrderID, CursCustomerName, 
					OpEndQty - QtyStore, CursCreationDate);
		END IF;

END Loop;

Close WOCursor;

SELECT * FROM WosToInvoice;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetInspectionDetail` (IN `GRBID` INT UNSIGNED)  BEGIN
DECLARE		InspectID		bigint unsigned;

IF		EXISTS(SELECT fkMaterialID
				FROM MaterialInspect
				WHERE fkMaterialID = GRBID)

THEN	SET 	InspectID = (SELECT fkInspectionID 
								FROM MaterialInspect 
								WHERE fkMaterialID = GRBID);
END IF;

SELECT			fkApprovalLevelID AS ApprovalTypeID, IFNULL(MS.MaterialSpec, M.Description) AS Spec, IRNoteNumber AS ReleaseNoteNo,
								InspectID AS InspectionID

FROM			Material M
LEFT JOIN		MaterialSpec MS
ON				M.fkMaterialSpecID = MS.pkMaterialSpecID

WHERE			fkGRBID = GRBID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetInspectionSchedule` ()  BEGIN

DECLARE 		done 														INT DEFAULT FALSE;
DECLARE			COperationID												int unsigned;
DECLARE			CWorkOrderID, COrderID, CQty								mediumint unsigned;
DECLARE			COpNo, CLastOp, CSetRun, COperationNumber, COpTypeID		tinyint unsigned;
DECLARE			CPartTemplateID, CQuantityReqd, NWFinishDays, NWStartDays	smallint;
DECLARE			CDDStart, CStartDate, CFinishDate, CDDFinish, CWOODD			date;
DECLARE			DDFinishStore, DDStartStore									date;
DECLARE			CPartNumber, COperation										varchar(25);
DECLARE			CResource													varchar(10);
DECLARE			CInspectType												char(4);

DECLARE			OpInspectCursor CURSOR FOR
SELECT			OperationID, WorkOrderID, OrderID, PartTemplateID, PartNumber, 
				Qty, OpTypeID, Operation, DDStart, StartDate, FinishDate, DDFinish, Resource, InspectType, WOODD

FROM 			TmpInspSchedule;


DECLARE 		CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

CREATE TEMPORARY TABLE TmpInspSchedule (
				OperationID		integer unsigned PRIMARY KEY, 
				WorkOrderID		mediumint unsigned, 
				OrderID			mediumint unsigned, 
				PartTemplateID	smallint unsigned, 
				PartNumber		varchar(25), 
				Qty 			mediumint unsigned,
				OpTypeID		tinyint unsigned,
				Operation		varchar(25),  
				DDStart			date, 
				StartDate		date, 
				FinishDate		date, 
				DDFinish		date,
				Resource		varchar(10),
				InspectType		char(4),
				WOODD			date)

AS	
SELECT			O.pkOperationID AS OperationID, WO.pkWorkOrderID AS WorkOrderID, LI.fkOrderID AS OrderID, 
				PT.pkPartTemplateID AS PartTemplateID, PT.PartNumber, 
				IF(ISNULL((SELECT EndQty
							FROM Operation
														WHERE pkOperationID = O.pkOperationID
							AND	OperationNumber = O.OperationNumber -1)), ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1) + 1),
							(SELECT EndQty
							FROM Operation
							WHERE fkWorkOrderID = O.fkWorkOrderID
							AND	OperationNumber = O.OperationNumber -1)) AS Qty, 
				O.fkOperationTypeID AS OpTypeID, IF(EXISTS(SELECT fkOperationID 
															FROM MachineSet
															WHERE fkOperationID = O.pkOperationID)
															AND NOT EXISTS(SELECT 	fkSetID
																			FROM		SetInspect SI2
																			INNER JOIN	MachineSet MS2
																			ON			SI2.fkSetID = MS2.pkSetID
																			WHERE		MS2.fkOperationID = O.pkOperationID), 
				'Machineset', ML.description) AS Operation, 
				NULL AS DDStartDate, OMA.InspectStart AS StartDate, OMA.InspectEnd AS FinishDate, NULL AS DDFinishDate, 
				CONCAT(LEFT(IF(ISNULL(MS.EndTime), U2.FirstName, U.FirstName),1), ' ', IF(ISNULL(MS.EndTime), U2.LastName, U.LastName)) AS Resource, 
												IF(OperationNumber = (SELECT MAX(OperationNumber)
					FROM Operation
					WHERE fkworkOrderID = WO.pkWorkOrderID), 'FI',  
						IF(WO.FAIR = 1 AND OperationNumber = 2, 'FAIR', 
							IF(OperationNumber = 1, 'FO', 'STD'))) AS InspectType,
				IF(ISNULL(WO.NewDeliveryDate), WO.OrigDeliveryDate, WO.NewDeliveryDate) AS WOODD

FROM			Operation O
INNER JOIN		WorkOrder WO
ON				O.fkWorkOrderID = WO.pkWorkOrderID
INNER JOIN		LineItem LI
ON				WO.fkLineItemID = LI.pkLineItemID
LEFT JOIN		PartTemplate PT
ON				LI.fkPartTemplateID = PT.pkPartTemplateID
LEFT JOIN		OperationMachine OMA
ON				O.pkOperationID = OMA.fkOperationID
LEFT JOIN		User U
ON				OMA.fkUserIDOperator = U.pkUserID

LEFT JOIN		MachineSet MS
ON				O.pkOperationID = MS.fkOperationID
LEFT JOIN		User U2
ON				MS.fkUserIDSetter = U2.pkUserID

LEFT JOIN		MachiningType ML
ON				O.fkMachiningTypeID = ML.pkMachiningTypeID





WHERE			WO.Live = 1 
AND 			(WO.Complete 	<> 1 OR ISNULL(WO.Complete) 	= TRUE)
AND				(WO.PreCancel 	<> 1 OR ISNULL(WO.PreCancel) 	= TRUE)
AND 			(O.Bypass 		<> 1 OR ISNULL(O.Bypass) 		= TRUE)
AND				(O.Subcon 		<> 1 OR ISNULL(O.Subcon) 		= TRUE)
AND				fkOperationTypeID <> 1
AND				NOT EXISTS		(SELECT * 
				FROM OperationInspect OI5
				INNER JOIN Inspection I5
				ON OI5.fkInspectionID = I5.pkInspectionID
				WHERE fkOperationID = O.pkOperationID
				AND I5.InspectEnd IS NOT NULL);
				

OPEN 			OpInspectCursor;

CREATE TEMPORARY TABLE TmpInspSchedule2 (	OperationID		integer unsigned PRIMARY KEY, 
											WorkOrderID		mediumint unsigned, 
											OrderID			mediumint unsigned, 
											PartTemplateID	smallint unsigned, 
											PartNumber		varchar(25), 
											Qty 			mediumint unsigned,
											OpTypeID		tinyint unsigned,
											Operation		varchar(25),  
											DDStart			date, 
											StartDate		date, 
											FinishDate		date, 
											DDFinish		date,
											Resource		varchar(10),
											InspectType		char(4),
											WOODD			date)

			SELECT * FROM 	TmpInspSchedule
			ORDER BY		DDStart ASC;

read_loop: LOOP
		FETCH OpInspectCursor INTO COperationID, CWorkOrderID, COrderID, CPartTemplateID, CPartNumber, 
				CQty, COpTypeID, COperation, CDDStart, CStartDate, CFinishDate, CDDFinish, CResource, CInspectType, CWOODD;


														SET 	DDFinishStore = DATE_ADD(CWOODD, INTERVAL -(SELECT FatDays FROM Fat WHERE FatType = 2) DAY);
 
						SET 	NWFinishDays = (5 * (DATEDIFF(CWOODD, DDFinishStore) DIV 7) + 
								MID('0123444401233334012222340111123400001234000123440', 7 * WEEKDAY(DDFinishStore) + WEEKDAY(CWOODD) + 1, 1));

						IF 		CInspectType = 'FAIR' THEN

								SET 	DDStartStore = DATE_ADD(DDFinishStore, INTERVAL -(3.5) DAY_HOUR);

						ELSE 
								IF (CInspectType = 'FO' OR CInspectType = 'FI') THEN

										SET 	DDStartStore = DATE_ADD(DDFinishStore, INTERVAL -(2.5) DAY_HOUR);

								ELSE											SET 	DDStartStore = DATE_ADD(DDFinishStore, INTERVAL -(2.0) DAY_HOUR);
								END IF;
						END IF;

						SET		NWStartDays = (5 * (DATEDIFF(CWOODD, DDStartStore) DIV 7) + 
								MID('0123444401233334012222340111123400001234000123440', 7 * WEEKDAY(DDStartStore) + WEEKDAY(CWOODD) + 1, 1));


						UPDATE 	TmpInspSchedule2

						SET		DDFinish = DATE_ADD(DDFinishStore, INTERVAL - NWFinishDays DAY),

								DDStart = DATE_ADD(DDStartStore, INTERVAL - NWStartDays DAY)

						WHERE 	OperationID = COperationID;



 		IF done THEN
 			LEAVE read_loop;

 		END IF;

END LOOP;

Close OpInspectCursor;

SELECT 		OperationID, WorkOrderID, OrderID, PartTemplateID, PartNumber, 
			Qty, OptypeID, Operation, DDStart, StartDate, FinishDate, DDFinish, Resource, InspectType
 
FROM 		TmpInspSchedule2
ORDER BY	DDStart ASC;

DROP TABLE 	TmpInspSchedule2;
DROP TABLE 	TmpInspSchedule;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetInspectOpList` (IN `WorkOrderID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED)  BEGIN
SELECT		O.pkOperationID AS OperationID, O.OperationNumber AS OpNo, O.fkOperationTypeID AS OpType, 
						IF(O.fkOperationTypeID = 3, TL.Description, MT.Description) AS Operation, 
			O.EndQty AS Made, I.InspectQty AS Inspected, 
			I.ScrappedQty AS Scrapped, O.EndQty AS Forwarded, OperationID AS HighlightedOp,
			fkWorkOrderID AS WorkOrderNo

FROM		Operation O
LEFT JOIN	OperationTreatment OT
ON			O.pkOperationID = OT.pkOperationID
LEFT JOIN	TreatmentList TL
ON			OT.fkTreatmentID = TL.pkTreatmentID
LEFT JOIN	OperationInspect OI
ON			O.pkOperationID = OI.fkOperationID
LEFT JOIN	Inspection I
ON			OI.fkInspectionID = I.pkInspectionID
LEFT JOIN	MachiningType MT
ON			O.fkMachiningTypeID = MT.pkMachiningTypeID

WHERE		O.fkWorkOrderID = WorkOrderID
AND			O.fkOperationTypeID <> 1

ORDER BY	O.OperationNumber;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetIntMaterialNotes` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		Note
FROM		OperationNote OPN

WHERE		OPN.fkOperationID = (SELECT pkOperationID 
								FROM Operation 
								WHERE fkWorkOrderID = WorkOrderID 
								AND OperationNumber = 1)
AND			OPN.fkNoteTypeID = 1; 	
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetLIWOinfo` (IN `OrderID` MEDIUMINT UNSIGNED)  BEGIN
SELECT			pkLineItemID AS LineItemID, CustomerReference AS CustRef, ESLQuoteNumber AS QuoteNo, 
				fkPartTemplateID AS PTemplateID, fkEngineeringTemplateID AS ETemplateID,
				PriceEach, OrigDeliveryDate AS DeliveryDate, QuantityReqd AS Reqd, QuantityReqd + QuantityOvers AS Build,
				ChangeReason, FAIR, pkWorkOrderID AS WONo, Live

FROM			LineItem LI
LEFT JOIN		WorkOrder WO
ON				LI.pkLineItemID = WO.fkLineItemID

WHERE			LI.fkOrderID = OrderID
AND				CancelReason IS NULL
AND				(WO.Live <> 2 OR WO.Live IS NULL)
;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetMachineListDetails` ()  BEGIN
SELECT		pkMachineID AS MachineID, Manufacturer, Model, ServiceStart, ServiceEnd

FROM		Machine

WHERE		InUseYN <> 2 OR InUseYN IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetMachineMasterList` ()  BEGIN
SELECT		M.pkMachineID AS MachineID, Manufacturer, Model, Turn AS Trn, Grind AS Grd, Mill AS Mil, 
			GearCut AS GC, Other AS Oth, IF(InUseYN = 2 OR InUseYN IS NULL, 0, 1) AS Active

FROM		Machine M
LEFT JOIN	MachineMaintParam MMP
ON			M.pkMachineID = MMP.pkMachineID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetMachineOpPrograms` (IN `OperationTemplateID` MEDIUMINT UNSIGNED, `MachineID` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		ProgramNumber		

FROM		OpTmpltMachineProgram

WHERE		fkOperationTemplateID = OperationTemplateID
AND			fkMachineID = MachineID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetMachineParams` (IN `MachineID` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		Offset, StationNo, MinStickOut, Designation, 
			HolderNo, ChuckDiameter AS Diameter, InsertDatumPoint,
			MinfluteLength, Spindle,
			SkillOffset AS DifficultyLevel,
			CoolantType, TargetConcMin, TargetConcMax, SlideOilLevel, CoolantLevel, GreaseLevel, 
			Fanfilter, OilFilter, CoolantFilter, AirSystem, IntegralLighting, InterlocksEngaged,
			Turn AS Trn, Grind AS Grd, Mill AS Mil, GearCut AS GC, Other AS oth 

FROM		Machine M
LEFT JOIN	MachineMaintParam P
ON			M.pkMachineID = P.pkMachineID

WHERE		M.pkMachineID = MachineID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetMachineStats` (IN `MachineID` TINYINT UNSIGNED)  BEGIN
SELECT			M.FileName, SkillOffset AS DifficultyLevel, '19000101' AS ConcCheck, 'Dave Rogers' AS ConcUser, 
				'19000101' AS LubeCheck, 'Dave Rogers' AS LubeUser, '19000101' AS LevelCheck, 'Dave Rogers' AS LevelUser, 
				'19000101' AS PressureCheck, 'Dave Rogers' AS PressureUser, 99 AS ThisMonth, 88 AS LastMonth, 10 AS ThisQuarter,
				9 AS LastQuarter, 100 AS ThisYear, 99 AS LastYear

FROM			Machine M
LEFT JOIN		MachineMaintenance MM
ON				M.pkMachineID = MM.fkMachineID

WHERE			pkMachineID = MachineID
AND				(InUseYN <> 2 OR InUseYN IS NULL);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetMachiningTypeList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkMachiningTypeID AS MachiningTypeID, Description

FROM		MachiningType

WHERE		Deleted <> 1 OR Deleted IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetMaintenanceStats` (IN `MachineID` TINYINT UNSIGNED)  BEGIN
SELECT			
								CreateDate, Issue, Resolution

FROM			Machine M
LEFT JOIN		MachineMaintenance MM
ON				M.pkMachineID = MM.fkMachineID

WHERE			pkMachineID = MachineID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetMatchingCOList` (IN `PartTemplateID` SMALLINT UNSIGNED)  BEGIN
SELECT		pkOrderID AS OrderID, CustomerOrderRef,  
			1 AS FastTrack

FROM		PartTemplate 	PT
INNER JOIN	LineItem		LI
ON			PT.pkPartTemplateID = LI.fkPartTemplateID
INNER JOIN	CustomerOrder 	CO
ON			LI.fkOrderID = CO.pkOrderID

WHERE		PT.pkPartTemplateID = PartTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetMatchingLineItems` (IN `OrderID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		pkLineItemID AS LineItemID, CustomerReference AS LineItemRef, pkWorkOrderID AS WorkOrderID

FROM		LineItem LI
INNER JOIN	WorkOrder WO
ON			LI.pkLineItemID = WO.fkLineItemID

WHERE		fkOrderID = OrderID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetMatchingSpecPOs` (IN `Spec` CHAR(6))  BEGIN
SELECT		RO.CreateDate AS OrderDate, PON.pkPOnID AS PONo, SupplierName, RO.OrderQuantity AS Qty, 
			UOM.Description AS UOM, OI.Price

FROM		MaterialSpec 	MS
INNER JOIN	OperationMaterial 	OM
ON			MS.pkMaterialSpecID = OM.fkMaterialSpecID
INNER JOIN	OperationOrderItem OOI
ON			OM.pkOperationID = OOI.fkOperationID
INNER JOIN	OrderItem 		OI
ON			OOI.fkItemID = OI.pkItemID
INNER JOIN	RFQOrder	RO
ON			OI.fkRFQOID = RO.pkRFQOID
INNER JOIN	Pon 			PON
ON			OI.fkRFQOID = PON.fkRFQOID
INNER JOIN	RFQOSupplier	RS
ON			PON.fkRFQOID = RS.fkRFQOID
INNER JOIN	Supplier		S
ON			RS.fkSupplierID = S.pkSupplierID
INNER JOIN	UnitOfMeasure			UOM
ON			OM.SPUfkUOMID = UOM.pkUOMID

WHERE		MS.Materialspec LIKE  CONCAT(Spec, "%");
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetMaterialDetail` (IN `WorkOrderID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		OM.* , MS.MaterialSpec, OPN.Note, S.Description AS Shape, AL.Description AS Approval, OM.Units
FROM 		Kythera.OperationMaterial OM
LEFT JOIN	Kythera.OperationNote OPN
	ON		OM.pkOperationID = OPN.fkOperationID
LEFT JOIN	Operation O
	ON		OM.pkOperationID = O.pkOperationID
LEFT JOIN	MaterialSpec MS
	ON		OM.fkMaterialSpecID = MS.pkMaterialSpecID
LEFT JOIN	Shape S
	ON		OM.fkShapeID = S.pkShapeID
LEFT JOIN	ApprovalLevel AL
	ON		OM.fkApprovalID = AL.pkApprovalLevelID
WHERE 		O.fkWorkOrderID = WorkOrderID
AND			OPN.fkNoteTypeID = 1;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetMaterialIssueDetail` (IN `WorkOrderID` MEDIUMINT UNSIGNED, `GRBID` INT UNSIGNED)  BEGIN

DECLARE		NextOpID		int unsigned;


IF 			WorkOrderID IS NOT NULL
THEN

	SET NextOpID = (SELECT MIN(pkOperationID) 
					FROM Operation 
					WHERE fkWorkOrderID = WorkOrderID
					AND fkOperationTypeID = 2
					AND EndQty IS NULL);

	SELECT						M.fkGRBID AS GRBNo, CONCAT(StoreArea, ' ', XCoord, YCoord) AS Location, Length, 
				QuantityReqd + QuantityOvers AS Qty, PT.PartNumber, MaterialSpec, M.Size, IF(M.UnitID = 1, "mm",
				IF(M.UnitID = 2, "inches", "ns")) AS Units, SH.Description AS Shape, GI.IRNoteNumber AS ReleaseType, 
				GI.Description, OM.IssueInstruction AS Instruction, (SELECT CONCAT(M3.Manufacturer, ' ', M3.Model)
																		FROM OperationMachine OM3
																		INNER JOIN Machine M3
																		ON OM3.fkMachineID = M3.pkMachineID
																		WHERE OM3.fkOperationID = NextOpID) AS Machine

	FROM		Material M
	INNER JOIN	GoodsIn GI
	ON			M.fkGRBID = GI.pkGRBID
	INNER JOIN	Allocated A
	ON			M.fkGRBID = A.fkGRBID
	INNER JOIN	Operation O
	ON			A.fkOperationID = O.pkOperationID
	INNER JOIN	WorkOrder WO
	ON			O.fkWorkOrderID = WO.pkWorkOrderID
	LEFT JOIN	OperationMaterial OM
	ON			O.pkOperationID = OM.pkOperationID


	LEFT JOIN	MaterialLocation ML
	ON			M.pkMaterialID = ML.fkMaterialID
	LEFT JOIN	StoreArea SA
	ON			ML.fkStoreAreaID = SA.pkStoreAreaID
	LEFT JOIN	XCoordinate X
	ON			SA.pkStoreAreaID = X.fkStoreAreaID
	LEFT JOIN	YCoordinate Y
	ON			SA.pkStoreAreaID = Y.fkStoreAreaID

		LEFT JOIN	PartTemplate PT
	ON			GI.fkPartID = PT.pkPartTemplateID
	LEFT JOIN	MaterialSpec MS
	ON			OM.fkMaterialSpecID = MS.pkMaterialSpecID
	LEFT JOIN	Shape SH
	ON			M.fkShapeID = SH.pkShapeID

	WHERE		O.fkWorkOrderID = WorkOrderID
	AND			O.fkOperationTypeID = 1;
ELSE

			SELECT		M.fkGRBID AS GRBNo, CONCAT(StoreArea, ' ', XCoord, YCoord) AS Location, Length, 
				M.Quantity AS Qty, PT.PartNumber, MaterialSpec, M.Size, IF(M.UnitID = 1, "mm",
				IF(M.UnitID = 2, "inches", "")) AS Units, SH.Description AS Shape, GI.IRNoteNumber AS ReleaseType, 
				GI.Description, OM.IssueInstruction AS Instruction, CONCAT(M.Manufacturer, ' ', M.Model) AS Machine

								
	FROM		Material M
	left JOIN	GoodsIn GI
	ON			M.fkGRBID = GI.pkGRBID
	LEFT JOIN	Allocated A
	ON			M.fkGRBID = A.fkGRBID
	left JOIN	Operation O	ON			A.fkOperationID = O.pkOperationID

	LEFT JOIN	MaterialLocation ML
	ON			M.pkMaterialID = ML.fkMaterialID
	LEFT JOIN	StoreArea SA
	ON			ML.fkStoreAreaID = SA.pkStoreAreaID
	LEFT JOIN	XCoordinate X
	ON			SA.pkStoreAreaID = X.fkStoreAreaID
	LEFT JOIN	YCoordinate Y
	ON			SA.pkStoreAreaID = Y.fkStoreAreaID

		LEFT JOIN	PartTemplate PT
 ON			GI.fkPartID = PT.pkPartTemplateID
	LEFT JOIN	MaterialSpec MS
	ON			M.fkMaterialSpecID = MS.pkMaterialSpecID
	LEFT JOIN	Shape SH
	ON			M.fkShapeID = SH.pkShapeID

	WHERE		M.fkGRBID = GRBID;
END IF;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetMaterialIssues` (IN `GRBNo` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		A.AllocateDate AS DateIssued, PT.PartNumber, A.Quantity AS Qty, WO.pkWorkOrderID AS WONo

FROM 		Allocated A
INNER JOIN	Operation O
ON			A.fkOperationID = O.pkOperationID
INNER JOIN	WorkOrder WO
ON			O.fkWorkOrderID = WO.pkWorkOrderID
INNER JOIN	LineItem LI
ON			WO.fkLineItemID = LI.pkLineItemID
INNER JOIN	PartTemplate PT
ON			LI.fkPartTemplateID = PT.pkPartTemplateID
INNER JOIN	Material M
ON			A.fkGRBID = M.fkGRBID

WHERE 		M.fkGRBID = GRBNo;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetMaterialShapes` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		pkShapeID, Description, ReqdDesc
				
FROM			Shape
WHERE			Deleted <> 1;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetMaterialShapesList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		pkShapeID, Description, ReqdDesc
				
FROM			Shape
WHERE			Deleted <> 1;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetMaterialTypeList` ()  BEGIN
SELECT		pkMaterialTypeID As MaterialType, Type

FROm		Materialtype

WHERE		Deleted <> 1 AND Deleted IS NOT NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetNADCAPList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
	SELECT		pkApprovalID AS NADCAPApprovalID, Description AS NADCAPApproval

	FROM		nadcapapproval

	WHERE		Deleted <> 1 OR Deleted IS NULL

	ORDER BY	Description;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetNextCreditNote` ()  BEGIN
SELECT		IF(pkCreditID IS NULL, 1, MAX(pkCreditID) + 1) AS ReturnNoteNo
FROM		OutvoiceCredit;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetNextGRBRejectNo` ()  BEGIN

SELECT		MAX(pkRejectID) + 1 AS RejectNoteID
FROM		RejectGRB;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetNextOutvoiceNo` ()  BEGIN
SELECT		MAX(pkInvoiceID) + 1 AS InvoiceNo
FROM		Outvoice;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetNextRejectNo` ()  BEGIN
SELECT		MAX(pkRejectID) + 1 AS RejectNoteID
FROM		RejectInvoice;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpConformityNotes` (IN `OperationID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT	Note
FROM	Kythera.OperationNote OPN
WHERE	OPN.fkOperationID = OperationID
AND		OPN.fkNoteTypeID = 2; 


END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpEngineeringNotes` (IN `OperationID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT	Note
FROM	Kythera.OperationNote OPN
WHERE	OPN.fkOperationID = OperationID
AND		OPN.fkNoteTypeID = 4; 


END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpenHours` (IN `SupplierID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		Day1Open, Day1Close, Day2Open, Day2Close, Day3Open, Day3Close, Day4Open, Day4Close, 
				Day5Open, Day5Close, Day6Open, Day6Close, Day7Open, Day7Close

FROM			Supplier

WHERE			pkSupplierID = SupplierID;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpeningHours` (IN `CustomerID` INT UNSIGNED)  BEGIN
SELECT		Day1Open, Day1Close, Day2Open, Day2Close, Day3Open, Day3Close, Day4Open, Day4Close,
			Day5Open, Day5Close, Day6Open, Day6Close, Day7Open, Day7Close
FROM		Customer
WHERE		pkCustomerID = CustomerID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOperationNotes` (IN `OperationID` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

SELECT		Note
FROM		OperationNote OPN

WHERE		OPN.fkOperationID = OperationID
			AND			OPN.fkNoteTypeID = 8; 


END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOperationStatus` (IN `OperationID` INT UNSIGNED)  BEGIN

DECLARE OpStatus 	tinyint DEFAULT 0;
DECLARE SetRun 		tinyint DEFAULT 0;
DECLARE MaxPauseID	int 	unsigned DEFAULT 0;

IF		EXISTS(SELECT 	*
				FROM	MachineSet
				WHERE	fkOperationID = OperationID)

		THEN	SET 	SetRun = 0;

		IF		NOT 	ISNULL((SELECT EndTime
								FROM MachineSet
								WHERE fkOperationID = OperationID))

		THEN					IF		EXISTS(SELECT 	*
								FROM	OperationMachine
								WHERE	fkOperationID = OperationID)

				THEN	SET		SetRun = 1;

						IF		NOT 	ISNULL((SELECT RunEnd 
												FROM OperationMachine 
												WHERE fkOperationID = OperationID))
								OR

								ISNULL((SELECT RunStart 
												FROM OperationMachine 
												WHERE fkOperationID = OperationID))

						THEN									SET		SetRun = 1;
								SET 	OpStatus = 0;

						ELSE	
																SET		SetRun = 1;
								SET 	OpStatus = 1;
								
						END IF;
				END IF;

		ELSE	
				IF		NOT ISNULL((SELECT StartTime
									FROM	MachineSet
									WHERE	fkOperationID = OperationID))
		
				THEN
												SET		SetRun 	= 0;
						SET 	OpStatus = 1;

				ELSE
												SET		SetRun 	= 0;
						SET 	OpStatus = 0;

				END IF;

		END IF;
ELSE
				IF		EXISTS(SELECT 	*
						FROM	OperationMachine
						WHERE	fkOperationID = OperationID)

		THEN	SET		SetRun = 1;

				IF		NOT 	ISNULL((SELECT RunEnd 
										FROM OperationMachine 
										WHERE fkOperationID = OperationID))
						OR

						ISNULL((SELECT RunStart 
										FROM OperationMachine 
										WHERE fkOperationID = OperationID))

				THEN							SET		SetRun = 1;
						SET 	OpStatus = 0;

				ELSE	
												SET		SetRun = 1;
						SET 	OpStatus = 1;
								
				END IF;
		END IF;
END IF;

IF EXISTS(SELECT *
				FROM Pause
				WHERE fkOperationID = OperationID
				AND ISNULL(EndTime) = true)

THEN
				SET 	OpStatus = 2;

END IF;

SELECT 		OpStatus, SetRun;	

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpFixtures` (IN `OperationID` INT)  BEGIN
SELECT 		F.GRBNumber, L.Location, F.FixtureNumber, F.Description, F.FixtureStatus
FROM 		Fixture	F
LEFT JOIN	Location L
ON			F.fkLocationID = L.pkLocationID
INNER JOIN	OperationFixture OF
ON			F.pkFixtureID = OF.fkFixtureID
WHERE		OF.fkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpInspectionCE` (IN `OperationID` INT UNSIGNED)  BEGIN

SELECT			CheckEquipment AS CheckEquip

FROM			Inspection I
INNER JOIN		OperationInspect OI
ON				I.pkInspectionID = OI.fkInspectionID

WHERE			fkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpInspectionDetail` (IN `OperationID` INT UNSIGNED)  BEGIN

DECLARE			QtyStore	smallint unsigned;
DECLARE			OpNo		tinyint unsigned;

SET	OpNo = (SELECT OperationNumber FROM Operation WHERE pkOperationID = OperationID);

SET QtyStore =  (SELECT	EndQty
				FROM	Operation

				WHERE	pkOperationID = OperationID
				AND		OperationNumber = OpNo -1);

		

SELECT			IFNULL(I.pkInspectionID, I2.pkInspectionID) AS InspectionID, IFNULL(I.Detail, I2.Detail) AS InspectInfo, 
				IFNULL(I.InspectQty, I2.InspectQty) AS QtyInspected, 
				IFNULL(I.ScrappedQty, I2.ScrappedQty) AS QtyScrapped, IF(OpNo = 1, O.EndQty, QtyStore) AS SubConQty, 
				IFNULL(I.ScrapReasonID, I2.ScrapReasonID) AS ReasonID

FROM			Operation O 
LEFT JOIN		OperationInspect OI
ON				O.pkOperationID = OI.fkOperationID
LEFT JOIN		Inspection I
ON				OI.fkInspectionID = I.pkInspectionID

LEFT JOIN		MachineSet MS
ON				O.pkOperationID = MS.fkOperationID
LEFT JOIN		SetInspect SI
ON				MS.pkSetID = SI.fkSetID		
LEFT JOIN		Inspection I2
ON				SI.fkInspectionID = I2.pkInspectionID

WHERE			O.pkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpInspectionERPA` (IN `OperationID` INT UNSIGNED)  BEGIN

SELECT			ER.PreventActionNote AS ExtRejectAction,
				DT.DeviationType AS RejectReason

FROM			ExternalReject ER
INNER JOIN		WorkOrder WO
ON				ER.fkWorkOrderID = WO.pkWorkOrderID
INNER JOIN		LineItem LI
ON				WO.fkLineItemID = LI.pkLineItemID
INNER JOIN		DeviationType DT
ON				ER.fkDeviationTypeID = DT.pkDeviationTypeID

WHERE			LI.fkEngineeringTemplateID = (SELECT LI1.fkEngineeringTemplateID
												FROM LineItem LI1
												INNER JOIN WorkOrder WO1
												ON LI1.pkLineItemID = WO1.fkLineItemID
												INNER JOIN	Operation O1
												ON WO1.pkWorkOrderID = O1.fkWorkOrderID
												WHERE O1.pkOperationID = OperationID);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpInspectionON` (IN `OperationID` INT UNSIGNED)  BEGIN

SELECT			OPN.Note AS OperatorNote

FROM			Inspection I
INNER JOIN		OperationInspect OI
ON				I.pkInspectionID = OI.fkInspectionID
INNER JOIN		Operation O
ON				OI.fkOperationID = O.pkOperationID

LEFT JOIN		OperationNote OPN
ON				O.pkOperationID = OPN.fkOperationID

WHERE			OI.fkOperationID = OperationID
AND				OPN.fkNoteTypeID = 8;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpInspectionPISI` (IN `OperationID` INT UNSIGNED)  BEGIN

SELECT			I.Detail AS PreviousScrapInfo, 
				IF(ScrapReasonID = 1, 'Machine Breakdown',
					IF(ScrapReasonID = 2, 'Tool worn',
						IF(ScrapReasonID = 3, 'Operator Error',
							IF(ScrapReasonID = 4, 'Material Hardness', 'Not set')))) AS PreviousScrapReason

FROM			Inspection I
INNER JOIN		OperationInspect OI
ON				I.pkInspectionID = OI.fkInspectionID
INNER JOIN		Operation O
ON				OI.fkOperationID = O.pkOperationID
INNER JOIN		WorkOrder WO
ON				O.fkWorkOrderID = WO.pkWorkOrderID
INNER JOIN		LineItem LI
ON				WO.fkLineItemID = LI.pkLineItemID

WHERE			OI.fkOperationID <> OperationID

AND				fkOperationTypeID = (SELECT O1.fkOperationTypeID
									FROM Operation O1
									WHERE O1.pkOperationID = OperationID)
AND				fkMachiningTypeID = (SELECT O1.fkMachiningTypeID
									FROM Operation O1
									WHERE O1.pkOperationID = OperationID)
AND				LI.fkEngineeringTemplateID = (SELECT LI1.fkEngineeringTemplateID
												FROM LineItem LI1
												INNER JOIN WorkOrder WO1
												ON LI1.pkLineItemID = WO1.fkLineItemID
												INNER JOIN	Operation O2
												ON WO1.pkWorkOrderID = O2.fkWorkOrderID
												WHERE O2.pkOperationID = OperationID);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpInspectionProcesses` (IN `OperationID` INT UNSIGNED)  BEGIN
SELECT			DISTINCT OP.ProcessNote

FROM			OperationProcess OP

WHERE			fkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpInspectNotes` (IN `OperationID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT	Note
FROM	Kythera.OperationNote OPN
WHERE	OPN.fkOperationID = OperationID
AND		OPN.fkNoteTypeID = 7; 


END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpIntMaterialNotes` (IN `OperationID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT	Note
FROM	Kythera.OperationNote OPN
WHERE	OPN.fkOperationID = OperationID
AND		OPN.fkNoteTypeID = 3; 


END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpIntTreatNotes` (IN `OperationID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT	Note
FROM	Kythera.OperationNote OPN
WHERE	OPN.fkOperationID = OperationID
AND		OPN.fkNoteTypeID = 6; 


END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpMaterialNotes` (IN `OperationID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT	Note
FROM	Kythera.OperationNote OPN
WHERE	OPN.fkOperationID = OperationID
AND		OPN.fkNoteTypeID = 1; 


END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpOperatorNotes` (IN `OperationID` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

SELECT		Note
FROM		OperationNote OPN

WHERE		OPN.fkOperationID = OperationID
			AND			OPN.fkNoteTypeID = 8; 


END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpProcesses` (IN `OperationID` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		OP.ProcessNote		
FROM		OperationProcess OP
WHERE		OP.fkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpTmpltConformityNotes` (IN `OperationTemplateID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT	Note
FROM	Kythera.TemplateNote OPN
WHERE	OPN.fkOperationTemplateID = OperationTemplateID
AND		OPN.fkNoteTypeID = 2; 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpTmpltEngineeringNotes` (IN `OperationTemplateID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT	Note
FROM	Kythera.TemplateNote OPN
WHERE	OPN.fkOperationTemplateID = OperationTemplateID
AND		OPN.fkNoteTypeID = 4; 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpTmpltInspectNotes` (IN `OperationTemplateID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT	Note
FROM	Kythera.TemplateNote OPN
WHERE	OPN.fkOperationTemplateID = OperationTemplateID
AND		OPN.fkNoteTypeID = 7; 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpTmpltIntEngNotes` (IN `OperationTemplateID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		Note

FROM		TemplateNote TN

WHERE		TN.fkOperationTemplateID = OperationTemplateID
AND			TN.fkNoteTypeID = 4; 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpTmpltIntMaterialNotes` (IN `OperationTemplateID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkTemplateNoteID AS NoteID, Note
FROM		TemplateNote TN

WHERE		TN.fkOperationTemplateID = OperationTemplateID
AND			TN.fkNoteTypeID = 1; 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpTmpltIntReviewNotes` (IN `OperationTemplateID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		Note

FROM		TemplateNote

WHERE		fkOperationTemplateID = OperationTemplateID
AND			fkNoteTypeID = 9;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpTmpltIntTreatNotes` (IN `OperationTemplateID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		Note

FROM		TemplateNote OPN

WHERE		OPN.fkOperationTemplateID = OperationTemplateID
AND			OPN.fkNoteTypeID = 6; 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpTmpltMaterialNotes` (IN `OperationTemplateID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT	Note
FROM	Kythera.TemplateNote OPN
WHERE	OPN.fkOperationTemplateID = OperationTemplateID
AND		OPN.fkNoteTypeID = 1; 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpTmpltReviewDetail` (IN `OperationTemplateID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		ReviewStatus, RiskAssessment AS Risk, Tooling, QAEquipment AS QAEquip, CorrectApproval

FROM		EngineeringTemplate

WHERE		pkEngineeringTemplateID = (SELECT fkEngineeringTemplateID 
										FROM OperationTemplate 
										WHERE pkOperationTemplateID = OperationTemplateID);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpTmpltSubconDetail` (IN `OperationTemplateID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT			TT.fkTreatmentID AS TreatmentID, T.Description AS Treatment, pkSupplierID AS SupplierID, S.SupplierName, 
				TT.ApprovalComment, TT.LeadTime

FROM			TemplateTreatment TT
INNER JOIN		TreatmentList T
ON				TT.fkTreatmentID = T.pkTreatmentID
LEFT JOIN		Supplier S
ON				TT.StandardSupplierID = S.pkSupplierID

WHERE			TT.pkOperationTemplateID = OperationTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpTmpltTreatmentNotes` (IN `OperationTemplateID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT	Note
FROM	Kythera.TemplateNote OPN
WHERE	OPN.fkOperationTemplateID = OperationTemplateID
AND		OPN.fkNoteTypeID = 5; 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpTools` (IN `OperationID` INT, IN `MachineID` INT)  BEGIN
SELECT 		T.GRBNumber, L.Location, T.SerialNumber, T.Description, T.ToolStatus
FROM 		Tool T
LEFT JOIN	Location L
ON			T.fkLocationID = L.pkLocationID
INNER JOIN	OpToolMachine OTM
ON			T.pkToolID = OTM.fkToolID
WHERE		OTM.fkOperationID = OperationID
AND			OTM.fkMachineID = MachineID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpTreatmentNotes` (IN `OperationID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT	Note
FROM	Kythera.OperationNote OPN
WHERE	OPN.fkOperationID = OperationID
AND		OPN.fkNoteTypeID = 5; 


END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOpWOTmpltIntMaterialNotes` (IN `OperationID` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkOperationNoteID AS NoteID, Note
FROM		OperationNote

WHERE		fkOperationID = OperationID
AND			fkNoteTypeID = 1; 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOrderComments` (IN `RFQOID` SMALLINT UNSIGNED)  BEGIN
SELECT		Comment

FROM		POComment

WHERE		fkRFQOID = RFQOID
AND			Deleted <> 1;			
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOrderDocuments` (IN `RFQOID` SMALLINT UNSIGNED)  BEGIN
SELECT		pkWODocumentID AS DocumentID, DateAdded AS DropDate, fkDocumentDropTypeID AS TypeID, DDT.DropType AS DocType, 
			Print AS AutoPrint

FROM		OrderItem OI
LEFT JOIN	OperationOrderItem OOI
ON			OI.pkItemID = OOI.fkItemID
LEFT JOIN	Operation O
ON			OOI.fkOperationID = O.pkOperationID
LEFT JOIN	WODocument WOD
ON			O.fkWorkOrderID = WOD.fkWorkOrderID
INNER JOIN	DocumentDropType DDT
ON			WOD.fkDocumentDropTypeID =DDT.pkDocumentDropTypeID

WHERE		OI.fkRFQOID = RFQOID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOrderNotesHistory` (IN `CONo` MEDIUMINT UNSIGNED)  BEGIN

SELECT		Note AS OrderNote

FROM		CustomerOrderNote

WHERE		fkOrderID = CONo;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetorderProgress` (IN `RFQOID` SMALLINT UNSIGNED)  BEGIN
SELECT		U.UName AS StaffMember, ContactTypeID, ProgressNote AS Progress, CreateDate, Chase

FROM		RFQProgress RP
INNER JOIN	User U
ON			RP.fkUserID = U.pkUserID

WHERE		fkRFQOID = RFQOID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOrderRejectHistory` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN

SELECT		PreventActionNote AS RejectAction , DeviationType AS RejectReason

FROM		ExternalReject		ER	
LEFT JOIN	DeviationType		DT	
ON			ER.fkDeviationTypeID = DT.pkDeviationTypeID

WHERE		fkWorkOrderID = WorkOrderID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetOurApprovalList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		pkApprovalID, Approval
				
FROM			OurApproval
WHERE			Deleted <> 1;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPartAssemblies` (IN `PTemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		PartNumber

FROM		PartTemplate

WHERE		fkParentPartTemplateID = PTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPartDetail` (IN `ETemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		fkSupplyConditionID AS COSID, ET.CorrectApproval AS ReleaseTypeID, PT.Description AS PartDescription, 
			pkworkOrderID AS WorkOrderID, C.Alias AS CustomerName, PT.Onalert AS Alert, PT.LockedDown AS Locked

FROM			Engineeringtemplate ET
LEFT JOIN		PartTemplate PT
ON				ET.fkPartTemplateID = PT.pkPartTemplateID
LEFT JOIN		LineItem LI
ON				ET.pkEngineeringTemplateID = LI.fkEngineeringTemplateID
LEFT JOIN		WorkOrder WO
ON				LI.pkLineItemID = WO.fkLineItemID
LEFT JOIN		CustomerOrder CO
ON				LI.fkOrderID = CO.pkOrderID
LEFT JOIN		Customer C
ON				CO.fkCustomerID = C.pkCustomerID

WHERE			pkEngineeringtemplateID = ETemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPartDocs` (IN `PTemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT			Uname, DropType, AutoPrint, FileHandle AS FileName
FROM			AssociatedPartDoc APD
INNER JOIN		User U
ON				APD.fkUserID = U.pkUserID
INNER JOIN		DocumentDropType DDT
ON				APD.fkDocTypeID = DDT.pkDocumentDropTypeID
				
WHERE			fkPartTemplateID = PTemplateID

UNION

SELECT			Uname, 'Drg' AS DropType, 0 AS AutoPrint, FileHandle AS FileName
FROM			Drawing D
INNER JOIN		User U
ON				D.AddedByfkUserID = U.pkUserID
				
WHERE			pkDrawingID = (SELECT fkDrawingID
								FROM PartTemplate 
								WHERE pkPartTemplateID = PTemplateID);	
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPartNos` ()  BEGIN

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT			pkPartTemplateID AS PartID, PartNumber

FROM			PartTemplate PT;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPartReviewTasks` (IN `PTemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT			pkTaskID AS TaskID, Description, fkActionTypeID, CreateDate, FO, Pu, PE, En, Qu, SP, St, Fi, Closed

FROM			PartReviewTask

WHERE			fkPartTemplateID = PTemplateID
AND				(Closed <> 1 OR ISNULL(Closed));
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPartTemplateNotes` (IN `PTemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		Note AS PartNote, CreateDate AS NoteDate

FROM		PartTemplateNote

WHERE		fkPartTemplateID = PTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPartTemplateRemarks` (IN `PTemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		Remark AS PartRemark, CreateDate AS RemarkDate

FROM		TemplateRemark

WHERE		fkPartTemplateID = PTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPartWOList` (IN `PTemplateID` SMALLINT UNSIGNED)  BEGIN
-- DAP 02/03/2016 Kythera  - get all WOs previously carried out on selected part no.
-- need to sort the time taken for the job, from get time taken proc 
-- 17/11/2016 repointed info fields to TM

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT			PartNumber, IssueNumber AS Issue, PT.Description, Alias AS CustomerName, pkWorkOrderID AS WONo, CO.CreationDate AS OrderDate, 
				WO.Complete AS Completed, QuantityReqd AS Qty, PriceEach, (QuantityReqd * PriceEach) AS Value, 
IFNULL(TM.PartLength, 'Not set') AS Length, 
				IFNULL(TM.Size, 'Not set') AS Width, 
				IF(fkShapeID = 1 OR fkShapeID = 2 OR fkShapeID = 6 OR fkShapeID = 8, TM.Size, 'Not set') AS Height, 
				'Not set' AS Weight, A.Description AS Approval, IFNULL(Finish, 'not set') AS Finish, '9hrs' AS Time

FROM			LineItem LI
INNER JOIN		WorkOrder WO
ON				LI.pkLineItemID = WO.fkLineItemID
INNER JOIN		CustomerOrder CO
ON				LI.fkOrderID = CO.pkOrderID
INNER JOIN		Customer C
ON				CO.fkCustomerID = C.pkCustomerID
INNER JOIN		PartTemplate PT
ON				LI.fkPartTemplateID = PT.pkPartTemplateID

INNER JOIN		EngineeringTemplate ET
ON				PT.pkPartTemplateID = ET.fkPartTemplateID
INNER JOIN		OperationTemplate OT
ON				ET.pkEngineeringTemplateID = OT.fkEngineeringTemplateID
INNER JOIN		TemplateMaterial TM
ON				OT.pkOperationTemplateID = TM.pkOperationTemplateID

INNER JOIN		Approval A
ON				WO.fkApprovalID = A.pkApprovalID

WHERE			LI.fkPartTemplateID = PTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPauseReasons` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkReasonID AS PauseReasonID , Description AS Reason

FROM		PauseReason

WHERE		Deleted <> 1 OR Deleted IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPerformance` ()  BEGIN
DECLARE Cancelled	tinyint DEFAULT 0;
DECLARE Outstanding	tinyint DEFAULT 0;
DECLARE Orders		tinyint DEFAULT 0;
DECLARE AvLeadDays	tinyint DEFAULT 0;
DECLARE Delivered	tinyint DEFAULT 0;
DECLARE EarlyLate	tinyint DEFAULT 0;
DECLARE Rejects		tinyint DEFAULT 0;
DECLARE Early		tinyint DEFAULT 0;
DECLARE OnTime		tinyint DEFAULT 0;
DECLARE Late		tinyint DEFAULT 0;

SET Cancelled = 
(SELECT 	sum(
(SELECT  	count(*)
FROM		rfqorder PO
WHERE		PO.Cancelled = 1
AND 		pkRFQOID = PS.fkRFQOID))

FROM		Supplier S
LEFT JOIN	rfqosupplier PS
ON			S.pkSupplierID = PS.fkSupplierID

GROUP BY	alias);

SET OutStanding = 
(SELECT 	sum(
(SELECT  	count(*)
FROM		rfqorder RO
INNER JOIN	OrderItem OI
ON			RO.pkRFQOID = OI.fkRFQOID
WHERE		OI.pkItemID NOT IN (SELECT fkItemID FROM POGoods)
AND			(RO.Cancelled <> 1 OR ISNULL(RO.Cancelled))
AND 		pkRFQOID = PS.fkRFQOID))

FROM		Supplier S
LEFT JOIN	rfqosupplier PS
ON			S.pkSupplierID = PS.fkSupplierID

GROUP BY	alias);

SET Orders = 
(select 	count(*)  
FROM		rfqosupplier PS
INNER JOIN	rfqorder PO
ON			PS.fkRFQOID = PO.pkRFQOID
INNER JOIN	Supplier SS
ON			Ps.fkSupplierID = SS.pkSupplierID
		
GROUP BY	alias);



SET Delivered = 
(SELECT 	sum(
(SELECT  	count(*)
FROM		RFQOrder RO
INNER JOIN 	OrderItem OI 
ON 			RO.pkRFQOID = OI.fkRFQOID	
WHERE		pkItemID IN (SELECT fkItemID FROM POGoods POG )
AND			(RO.Cancelled <> 1 OR ISNULL(RO.Cancelled))))

FROM		Supplier S
LEFT JOIN	posupplier PS
ON			S.pkSupplierID = PS.fkSupplierID

GROUP BY	alias);



SET Rejects = 
(SELECT 	sum(
(SELECT  	count(*) 
FROM		rfqorder PO
INNER JOIN	OrderItem OI
ON			PO.pkRFQOID = OI.fkRFQOID
INNER JOIN	pogoods PG
ON			OI.pkItemID = PG.fkItemID
INNER JOIN	goodsin GI
ON			PG.fkGRBID = GI.pkGRBID	
WHERE		(PO.Cancelled <> 1 OR ISNULL(PO.Cancelled))
AND			(LeadDays > 0 OR ISNULL(LeadDays) = false)
AND 		pkRFQOID = PS.fkRFQOID
AND			PS.Selected = 1
AND			DATEDIFF(ReceivedDate, (DATE_ADD(DATE(PO.CreateDate), INTERVAL LeadDays DAY))) >4))

FROM		Supplier S
LEFT JOIN	rfqosupplier PS
ON			S.pkSupplierID = PS.fkSupplierID

GROUP BY	alias);

SET Early = 
(SELECT 	sum(
(SELECT  	count(*) 
FROM		rfqorder PO
INNER JOIN	OrderItem OI
ON			PO.pkRFQOID = OI.fkRFQOID
INNER JOIN	pogoods PG
ON			OI.pkItemID = PG.fkItemID
INNER JOIN	goodsin GI
ON			PG.fkGRBID = GI.pkGRBID	
WHERE		(PO.Cancelled <> 1 OR ISNULL(PO.Cancelled))
AND			(LeadDays > 0 OR ISNULL(LeadDays) = false)
AND 		pkRFQOID = PS.fkRFQOID
AND			PS.Selected = 1
AND			DATEDIFF((DATE_ADD(DATE(PO.CreateDate), INTERVAL LeadDays DAY)), ReceivedDate) >4))

FROM		Supplier S
LEFT JOIN	rfqosupplier PS
ON			S.pkSupplierID = PS.fkSupplierID

GROUP BY	alias);

SET OnTime = 
(SELECT 	sum(
(SELECT  	COUNT(*)
FROM		rfqorder PO
INNER JOIN	OrderItem OI
ON			PO.pkRFQOID = OI.fkRFQOID
INNER JOIN	pogoods PG
ON			OI.pkItemID = PG.fkItemID
INNER JOIN	goodsin GI
ON			PG.fkGRBID = GI.pkGRBID	
WHERE		(PO.Cancelled <> 1 OR ISNULL(PO.Cancelled))
AND			(LeadDays > 0 OR ISNULL(LeadDays) = false)
AND 		pkRFQOID = PS.fkRFQOID
AND			PS.Selected = 1		
AND 		DATE_ADD(DATE(PO.CreateDate), INTERVAL LeadDays DAY) <= ReceivedDate))

FROM		Supplier S
LEFT JOIN	rfqosupplier PS
ON			S.pkSupplierID = PS.fkSupplierID

GROUP BY	alias);

SET Late = 
(SELECT 	sum(
(SELECT  	count(*) 
FROM		rfqorder PO
INNER JOIN	OrderItem OI
ON			PO.pkRFQOID = OI.fkRFQOID
INNER JOIN	pogoods PG
ON			OI.pkItemID = PG.fkItemID
INNER JOIN	goodsin GI
ON			PG.fkGRBID = GI.pkGRBID	
WHERE		(PO.Cancelled <> 1 OR ISNULL(PO.Cancelled))
AND			(LeadDays > 0 OR ISNULL(LeadDays) = false)
AND 		pkRFQOID = PS.fkRFQOID
AND			PS.Selected = 1
AND			DATEDIFF(ReceivedDate, (DATE_ADD(DATE(PO.CreateDate), INTERVAL LeadDays DAY))) >4))

FROM		Supplier S
LEFT JOIN	rfqosupplier PS
ON			S.pkSupplierID = PS.fkSupplierID

GROUP BY	alias);

SELECT		Cancelled, Outstanding, Orders, AvLeadDays, Delivered, EarlyLate, Rejects, Early, OnTime, Late;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPOInvoices` (IN `RFQOID` SMALLINT UNSIGNED)  BEGIN
SELECT		pkInvoiceID AS InvoiceNo, I.TotalPrice AS ExpectedPrice, I.ActualTotal AS ActualPrice, RcvdByfkUserID AS User
		
FROM		POn P
INNER JOIN	RFQOrder RFQO
ON			P.fkRFQOId = RFQO.pkRFQOID
INNER JOIN	OrderItem OI
ON			RFQO.pkRFQOID = OI.fkRFQOID
INNER JOIN	POInvoice PI
ON			OI.pkItemID = PI.fkItemID
INNER JOIN	Invoice I
ON			PI.fkInvoiceID = I.pkInvoiceID

WHERE		P.fkRFQOID = RFQOID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPOItemDetail` (IN `RFQOID` SMALLINT UNSIGNED, `ItemNo` TINYINT UNSIGNED)  BEGIN
SELECT		O.pkOperationID AS OperationID, O.OperationNumber AS OpNo, 
			(SELECT COUNT(*) FROM OrderItem WHERE fkRFQOID = RFQOID) AS TotalItems, 
			OT.OpType, OI.pkItemID AS ItemID, ItemNo, OI.Quantity AS Qty, OI.SPU, OI.fkUOMID AS SPUID, OI.Size,
			OI.UnitID, OI.fkShapeID AS ShapeID, OI.Description AS PartDescription, OI.Comment, OI.Price, OI.Per

FROM		OrderItem OI
LEFT JOIN	OperationOrderItem OOI
ON			OI.pkItemID = OOI.fkItemID
LEFT JOIN	Operation O
ON			OOI.fkOperationID = O.pkOperationID
LEFT JOIN	WorkOrder WO
ON			O.fkWorkOrderID = WO.pkWorkOrderID
LEFT JOIN	LineItem LI
ON			WO.fkLineItemID = LI.pkLineItemID
LEFT JOIN	PartTemplate PT
ON			LI.fkPartTemplateID = PT.pkPartTemplateID
LEFT JOIN	OperationType OT
ON			O.fkOperationTypeID = OT.pkOperationTypeID
LEFT JOIN	OperationMaterial OM
ON			O.pkOperationID = OM.pkOperationID

WHERE		OI.fkRFQOID = RFQOID
AND			OI.ItemNo = ItemNo
																		;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPOItemsList` (IN `POnID` SMALLINT UNSIGNED)  BEGIN

SELECT		pkItemID AS ItemID, pkPartTemplateID AS PartTemplateID, OI.Description, PT.PartNumber

FROM		POn P
LEFT JOIN	OrderItem OI
ON			P.fkRFQOID = OI.fkRFQOID
LEFT JOIN	PartTemplate PT
ON			OI.fkPartTemplateID = PT.pkPartTemplateID

WHERE		P.pkPOnID = POnID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPOItemStoresDetail` (IN `ItemID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		SupplierName, O.fkWorkOrderID AS WONo, OI.Description, OI.Size, OI.fkUOMID AS UOMID, OI.fkShapeID AS ShapeID, 
			RO.fkApprovalLevelID AS ApprovalTypeID

FROM		OrderItem 			OI
INNER JOIN	RFQOrder			RO
ON			OI.fkRFQOID = RO.pkRFQOID
LEFT JOIN	OperationOrderItem 	OOI
ON			OI.pkItemID = OOI.fkItemID
LEFT JOIN	Operation 			O
ON			OOI.fkOperationID = O.pkOperationID
INNER JOIN	RFQOSupplier		RS
ON			OI.fkRFQOID = RS.fkRFQOID
INNER JOIN	Supplier 			S
ON			RS.fkSupplierID = S.pkSupplierID

WHERE		OI.pkItemID = ItemID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPOLinkedWODetail` (IN `ItemID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		WO.pkWorkOrderID AS WONo, PT.PartNumber, (WO.QuantityReqd + WO.QuantityOvers) AS Qty, 
			O.pkOperationID AS OperationID, OperationNumber AS OperationNo, 
			IF(O.fkOperationTypeID = 1, "Material Issue", 
				IF(O.fkOperationTypeID = 2, MT.Description,
					IF(O.fkOperationTypeID = 3, TL.Description, "unset"))) AS OpDesc

FROM		OperationOrderItem OOI
INNER JOIN	Operation O
ON			OOI.fkOperationID = O.pkOperationID
INNER JOIN	WorkOrder WO
ON			O.fkWorkOrderID = WO.pkWorkOrderID
INNER JOIN	LineItem LI
ON			WO.fkLineItemID = LI.pkLineItemID
LEFT JOIN	PartTemplate PT
ON			LI.fkPartTemplateID = PT.pkPartTemplateID

LEFT JOIN	MachiningType MT
ON			O.fkMachiningTypeID = MT.pkMachiningTypeID
LEFT JOIN	OperationTreatment OT
ON			O.pkOperationID = OT.pkOperationID
LEFT JOIN	TreatmentList TL
ON			OT.fkTreatmentID = TL.pkTreatmentID

WHERE		OOI.fkItemID = ItemID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPOList` ()  BEGIN
SELECT		pkPOnID AS POnID, CONCAT(LEFT(FirstName, 1), LEFT(LastName, 1)) AS OrderedBy

FROM		POn			P
INNER JOIN	RFQOrder 	RO
ON			P.fkRFQOID = RO.pkRFQOID
INNER JOIN	User		U
ON			P.AddedByfkUserID = U.pkUserID

WHERE		(Cancelled <> 1 OR Cancelled IS NULL)
AND			(Complete <> 1 OR Complete IS NULL);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPOOpenGRBList` (IN `RFQOID` SMALLINT UNSIGNED)  BEGIN
SELECT		fkGRBID AS GRBID

FROM		POGoods		PG
INNER JOIN	OrderItem	OI
ON			PG.fkItemID = OI.pkItemID

WHERE		fkRFQOID = RFQOID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPOSupplierDetail` (IN `RFQOID` SMALLINT UNSIGNED)  BEGIN

DECLARE		StartDate 							Date;
DECLARE		LeadDayss, NonWorkDays, ShutDays 	tinyint DEFAULT 0;
DECLARE		LoopCount 							smallint DEFAULT 1;

SET			StartDate = (SELECT CreateDate FROM RFQOrder WHERE pkRFQOID = RFQOID);	
SET			LeadDayss = (SELECT LeadDays FROM RFQOrder WHERE pkRFQOID = RFQOID);	

WorkDay: LOOP

	IF 	Weekday(DATE_ADD(StartDate, INTERVAL LoopCount DAY)) > 4 THEN

			SET NonWorkDays = NonWorkDays + 1;
			SET LeadDayss = LeadDayss + 1;

	END IF;

	IF (SELECT Count(*) 
			FROM BusinessShutdown
			WHERE Month(ShutDate) = Month(DATE_ADD(StartDate, INTERVAL LoopCount DAY))
			AND DAYOFMONTH(ShutDate) = DAYOFMONTH(DATE_ADD(StartDate, INTERVAL LoopCount DAY))) > 0 THEN
				
			SET ShutDays = ShutDays + 1;
			SET LeadDayss = LeadDayss + 1;

	END IF;

	IF 	LoopCount = LeadDayss THEN 

		Leave WorkDay;
	END IF;

IF 	LoopCount = 365 THEN 

		Leave WorkDay;
	END IF;


	SET LoopCount = LoopCount + 1;

END LOOP;

SELECT		fkSupplierID AS SupplierID, Reference AS SupplierRef, RS.fkContactID AS ClientContactID, 
			RS.Deliverycost AS DeliveryCharge, 
			RS.CertCharge, RS.TotalCost, MIN(PC.Comment) AS ApprovalComment, RO.LeadDays AS DeliveryDays, 
			IF(RO.DeliveryDate > 0, RO.DeliveryDate, DATE_ADD(CreateDate, INTERVAL LeadDayss DAY)) AS DeliveryDate, 
			RO.DeliveryCommentID

			
FROM		RFQOrder RO
INNER JOIN	RFQOSupplier RS
ON			RO.pkRFQOID = RS.fkRFQOID

LEFT JOIN	POComment PC
ON			RS.fkRFQOID = PC.fkRFQOID

WHERE		RO.pkRFQOID = RFQOID
AND			Selected = 1; 

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPreviousPurchases` (IN `PartNumber` VARCHAR(25))  BEGIN
SELECT		pkPOnID AS PONo, CreateDate AS OrderDate, S.SupplierName, OI.Quantity AS Qty, OP.OperationNumber AS OpNo, 
			IF(OM.UnitID = 1, "mm", 
				IF(OM.UnitID = 2, "inches", "")) AS Unit, PT.Description, OM.Size, SH.Description AS Shape, 
			OI.Price AS EachPrice, OI.Price * OI.Quantity AS OrderValue

FROM		PartTemplate	PT
LEFT JOIN	Lineitem		LI
ON			PT.pkPartTemplateID = LI.fkPartTemplateID
INNER JOIN	WorkOrder 		WO
ON			LI.pkLineItemID = WO.fkLineItemID
INNER JOIN	Operation 		OP
ON			WO.pkWorkOrderID = OP.fkWorkOrderID
LEFT JOIN	OperationOrderItem OOI
ON			OP.pkOperationID = OOI.fkOperationID
INNER JOIN	OrderItem 		OI
ON			OOI.fkItemID = OI.pkItemID
INNER JOIN	Pon 			PON
ON			OI.fkRFQOID = PON.fkRFQOID



INNER JOIN	RFQOSupplier	RS
ON			PON.fkRFQOID = RS.fkRFQOID
INNER JOIN	Supplier		S
ON			RS.fkSupplierID = S.pkSupplierID
INNER JOIN	OperationMaterial OM
ON			OP.pkOperationID = OM.pkOperationID
INNER JOIN	Shape			SH
ON			OM.fkShapeID = SH.pkShapeID


WHERE		PT.PartNumber = PartNumber;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPrevPurchases` (IN `PartNumber` VARCHAR(25))  BEGIN

SELECT		pkPOnID AS PONo, CreateDate AS OrderDate, S.SupplierName, OI.Quantity AS Qty, OP.OperationNumber AS OpNo, 
			IF(OM.UnitID = 1, "mm", 
				IF(OM.UnitID = 2, "inches", "")) AS Unit, PT.Description, OM.Size, SH.Description AS Shape, 
			OI.Price AS EachPrice, OI.Price * OI.Quantity AS OrderValue
			
FROM		PartTemplate	PT
LEFT JOIN	Lineitem		LI
ON			PT.pkPartTemplateID = LI.fkPartTemplateID
INNER JOIN	WorkOrder 		WO
ON			LI.pkLineItemID = WO.fkLineItemID
INNER JOIN	Operation 		OP
ON			WO.pkWorkOrderID = OP.fkWorkOrderID
LEFT JOIN	OperationOrderItem OOI
ON			OP.pkOperationID = OOI.fkOperationID
LEFT JOIN	OrderItem 		OI
ON			OOI.fkItemID = OI.pkItemID
INNER JOIN	Pon 			PON
ON			OI.fkRFQOID = PON.fkRFQOID
LEFT JOIN	POInvoice 		POI
ON			OI.pkItemID = POI.fkItemID
LEFT JOIN	Invoice 		I
ON			POI.fkInvoiceID = I.pkInvoiceID
INNER JOIN	RFQOSupplier	RS
ON			PON.fkRFQOID = RS.fkRFQOID
AND			Selected = 1
INNER JOIN	Supplier		S
ON			RS.fkSupplierID = S.pkSupplierID
LEFT JOIN	OperationMaterial OM
ON			OP.pkOperationID = OM.pkOperationID
LEFT JOIN	Shape			SH
ON			OM.fkShapeID = SH.pkShapeID


WHERE		PT.PartNumber = PartNumber;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetProcesses` (IN `OperationID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT	OP.ProcessNote		
FROM	Kythera.OperationProcess OP
WHERE	OP.fkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetProductionSchedule` ()  BEGIN

DECLARE 		done 														INT DEFAULT FALSE;
DECLARE			COperationID												int unsigned;
DECLARE			CWorkOrderID, COrderID, CQty								mediumint unsigned;
DECLARE			CMachineID, COpNo, CLastOp, CSetRun, COperationNumber		tinyint unsigned;
DECLARE			CPartTemplateID, CQuantityReqd							 	smallint;
DECLARE			NWFinishDays, NWStartDays									mediumint;
DECLARE			CAvail, CDDStart, CStartDate, CFinish, CDDFinish, CWOODD	date;
DECLARE			DDFinishStore, DDStartStore									datetime;
DECLARE			CMachine													varchar(40);
DECLARE			CPartNumber, COperation										varchar(25);
DECLARE			CResource													varchar(10);

DECLARE			OpRunCursor CURSOR FOR
SELECT			MachineID, Machine, OperationID, WorkOrderID, OrderID, PartTemplateID, PartNumber, 
				Qty, Operation, Avail, DDStart, StartDate, Finish, DDFinish, Resource, OpNo, LastOp, SetRun, WOODD

FROM 			TmpProdSchedule;


DECLARE 		CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

CREATE TEMPORARY TABLE TmpProdSchedule (
				MachineID		tinyint unsigned, 
				Machine			varchar(40), 
				OperationID		integer unsigned PRIMARY KEY, 
				WorkOrderID		mediumint unsigned, 
				OrderID			mediumint unsigned, 
				PartTemplateID	smallint unsigned, 
				PartNumber		varchar(25), 
				Qty 			mediumint unsigned, 
				Operation		varchar(25), 
				Avail			date, 
				DDStart			date, 
				StartDate		date, 
				Finish			date, 
				DDFinish		date,
				Resource		varchar(10),
				OpNo			tinyint unsigned, 
				LastOp			tinyint unsigned, 
				SetRun			tinyint unsigned,
				WOODD			date)

AS	
SELECT		M.pkMachineID AS MachineID, CONCAT(M.Manufacturer, ' ', M.Model) AS Machine, O.pkOperationID AS OperationID,
			WO.pkWorkOrderID AS WorkOrderID, LI.fkOrderID AS OrderID, PT.pkPartTemplateID AS PartTemplateID, PT.PartNumber, 
								(SELECT EndQty
				FROM Operation OO
				WHERE OO.fkWorkOrderID = O.fkWorkOrderID
				AND	OO.OperationNumber = O.OperationNumber -1) -
			IFNULL((SELECT ScrappedQty 
				FROM Inspection I1
				INNER JOIN OperationInspect OI1
				ON I1.PkInspectionID = OI1.fkInspectionID
				WHERE OI1.fkOperationID = (SELECT pkOperationID
											FROM Operation OOO
											WHERE OOO.fkWorkOrderID = O.fkWorkOrderID
											AND	OOO.OperationNumber = O.OperationNumber -1)) IS NOT NULL, 0) AS Qty,
			MT.Description AS Operation, 

					NULL AS Avail,

					NULL AS DDStart,

					DATE(OM.RunStart) AS StartDate,

					DATE(OM.InspectEnd) AS Finish,

					DATE_ADD(WO.OrigDeliveryDate, INTERVAL -(SELECT FatDays FROM Fat WHERE FatType = 2) DAY) AS DDFinish,
					IF(ISNULL(MS.EndTime), U2.UName, U.UName) AS Resource, O.OperationNumber AS OpNo,
				(SELECT MAX(OperationNumber) 
				FROM Operation OPO 
				WHERE fkWorkOrderID = (SELECT fkWorkOrderID
										FROM Operation
										WHERE pkOperationID = O.pkOperationID)) AS LastOp,
					IF(O.EstimatedSetTime > 0 AND MS.EndTime IS NOT NULL, 1,
					IF(ISNULL(O.EstimatedSetTime), 1, 0)) AS SetRun, 
			IF(ISNULL(WO.NewDeliveryDate), WO.OrigDeliveryDate, WO.NewDeliveryDate) AS WOODD

	FROM			Operation O
	LEFT JOIN		WorkOrder WO
	ON				O.fkWorkOrderID = WO.pkWorkOrderID
	LEFT JOIN		LineItem LI
	ON				WO.fkLineItemID = LI.pkLineItemID
	LEFT JOIN		PartTemplate PT
	ON				LI.fkPartTemplateID = PT.pkPartTemplateID
	LEFT JOIN		MachiningType MT
	ON				O.fkMachiningTypeID = MT.pkMachiningTypeID
	LEFT JOIN		OperationMachine OM
	ON				O.pkOperationID = OM.fkOperationID
	LEFT JOIN		User U
	ON				OM.fkUserIDOperator = U.pkUserID
	LEFT JOIN		MachineSet MS
	ON				O.pkOperationID = MS.fkOperationID
	LEFT JOIN		User U2
	ON				MS.fkUserIDSetter = U2.pkUserID
	INNER JOIN		Machine M
	ON				OM.fkMachineID = M.pkMachineID

	WHERE			WO.Live = 1 AND (WO.complete <> 1 OR ISNULL(WO.Complete) = TRUE)
	AND				(WO.PreCancel <> 1 OR ISNULL(WO.PreCancel) = TRUE)
	AND 			O.fkOperationTypeID = 2 AND (O.Bypass <> 1 OR ISNULL(O.Bypass) = TRUE)
	AND				(O.Subcon <> 1 OR ISNULL(O.Subcon) = TRUE)
	AND				OM.RunEnd IS NULL
	
		ORDER BY		MachineID, WOODD ASC;			


OPEN 		OpRunCursor;

CREATE TEMPORARY TABLE TmpProdSchedule2 (	MachineID		tinyint unsigned, 
											Machine			varchar(40), 
											OperationID		integer unsigned PRIMARY KEY, 
											WorkOrderID		mediumint unsigned, 
											OrderID			mediumint unsigned, 
											PartTemplateID	smallint unsigned, 
											PartNumber		varchar(25), 
											Qty 			mediumint unsigned, 
											Operation		varchar(25), 
											Avail			date, 
											DDStart			date, 
											StartDate		date, 
											Finish			date, 
											DDFinish		date,
											Resource		varchar(10),
											OpNo			tinyint unsigned, 
											LastOp			tinyint unsigned, 
											SetRun			tinyint unsigned,
											WOODD			date)
				SELECT * FROM TmpProdSchedule
				ORDER BY		MachineID, WOODD ASC;

read_loop: LOOP
		FETCH OpRunCursor INTO CMachineID, CMachine, COperationID, CWorkOrderID, COrderID, CPartTemplateID, CPartNumber, 
				CQty, COperation, CAvail, CDDStart, CStartDate, CFinish, CDDFinish, CResource, COpNo, CLastOp, CSetRun, CWOODD;


				IF COpNo = CLastOp THEN

						SET 		DDFinishStore = DATE_ADD(CWOODD, INTERVAL -(SELECT FatDays FROM Fat WHERE FatType = 2) DAY);

						SET			NWFinishDays = (5 * (DATEDIFF(CWOODD, DDFinishStore) DIV 7) + 
									MID('0123444401233334012222340111123400001234000123440', 7 * WEEKDAY(DDFinishStore) + WEEKDAY(CWOODD) + 1, 1));


						SET			DDStartStore = DATE_ADD(CDDFinish, INTERVAL -(SELECT 	(SUM(TIME_TO_SEC(EstimatedSetTime)) + SUM(TIME_TO_SEC(EstimatedRunTime) * CQty)) / 32400
																			FROM 	Operation
																			WHERE 	fkWorkOrderID = CWorkOrderID 
																			AND	 	OperationNumber = COpNo) DAY_HOUR);

						SET			NWStartDays = (5 * (DATEDIFF(CWOODD, DDStartStore) DIV 7) + 
									MID('0123444401233334012222340111123400001234000123440', 7 * WEEKDAY(DDStartStore) + WEEKDAY(CWOODD) + 1, 1));
 

						UPDATE 		TmpProdSchedule2

						SET 		DDFinish = DATE_ADD(DDFinishStore, INTERVAL - NWFinishDays DAY),

									DDStart = DATE_ADD(DDStartStore, INTERVAL - NWStartDays DAY),


									Avail = (SELECT IF(ISNULL(MAX(CDDFinish)), CurDate(), MAX(CDDFinish)) 
											FROM 	TmpProdSchedule
											WHERE 	MachineID = CMachineID)

						WHERE 		OperationID = COperationID;

												
				ELSE
						 
						SET 		DDFinishStore = DATE_ADD(DATE_ADD(DATE_ADD(CWOODD, INTERVAL - (SELECT (SUM(TIME_TO_SEC(EstimatedSetTime)) + SUM(TIME_TO_SEC(EstimatedRunTime) * CQty)) /  32400
																							FROM 	Operation
																							WHERE 	fkWorkOrderID = CWorkOrderID
																							AND		(Subcon <> 1 OR ISNULL(Subcon))
																							AND		(Bypass <> 1 OR ISNULL(Bypass))
																							AND	 	OperationNumber > COpNo) DAY_HOUR), 
														INTERVAL -(SELECT FatDays FROM Fat WHERE FatType = 1) DAY), 
												INTERVAL -((SELECT FatDays FROM Fat WHERE FatType = 2) - (SELECT FatDays FROM Fat WHERE FatType = 1)) DAY);
						
 
						SET 		NWFinishDays = (5 * (DATEDIFF(CWOODD, DDFinishStore) DIV 7) + 
									MID('0123444401233334012222340111123400001234000123440', 7 * WEEKDAY(DDFinishStore) + WEEKDAY(CWOODD) + 1, 1));


						SET 		DDStartStore = DATE_ADD(DDFinishStore, INTERVAL -(SELECT 	SUM(TIME_TO_SEC(EstimatedSetTime)) + SUM(TIME_TO_SEC(EstimatedRunTime) * CQty) / 32400
																					FROM 	Operation
																					WHERE 	fkWorkOrderID = CWorkOrderID 
																					AND		(Subcon <> 1 OR ISNULL(Subcon))
																					AND		(Bypass <> 1 OR ISNULL(Bypass))
																					AND	 	OperationNumber >= COpNo) DAY_HOUR);

						SET			NWStartDays = (5 * (DATEDIFF(CWOODD, DDStartStore) DIV 7) + 
									MID('0123444401233334012222340111123400001234000123440', 7 * WEEKDAY(DDStartStore) + WEEKDAY(CWOODD) + 1, 1));


						UPDATE 		TmpProdSchedule2

						SET			DDFinish = DATE_ADD(DDFinishStore, INTERVAL - NWFinishDays DAY),

									DDStart = DATE_ADD(DDStartStore, INTERVAL - NWStartDays DAY),
					

									Avail = (SELECT IF(MAX(CDDFinish) IS NULL, CurDate(), MAX(CDDFinish)) 
											FROM 	TmpProdSchedule
											WHERE 	MachineID = CMachineID)

						WHERE 		OperationID = COperationID;

												
END IF;

												UPDATE		Operation
						
						SET			ScheduledStartDate = DATE_ADD(DDStartStore, INTERVAL - NWStartDays DAY)

						WHERE		pkOperationID = COperationID;


 		IF done THEN
 			LEAVE read_loop;

 		END IF;

END LOOP;

Close OpRunCursor;

SELECT 		MachineID, Machine, OperationID, WorkOrderID, OrderID, PartTemplateID, PartNumber, 
			Qty, Operation, DDStart, StartDate, Finish, DDFinish, Resource, OpNo, LastOp, SetRun, Avail
 
FROM 		TmpProdSchedule2
ORDER BY	MachineID, OperationID ASC;

DROP TABLE 	TmpProdSchedule2;
DROP TABLE 	TmpProdSchedule;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetProductionScheduleMI` ()  BEGIN

DECLARE 		done 														INT DEFAULT FALSE;
DECLARE			COperationID												int unsigned;
DECLARE			CWorkOrderID, COrderID, CQty, NWFinishDays, NWStartDays		mediumint unsigned;
DECLARE			COpNo, CLastOp, COperationNumber							tinyint unsigned;
DECLARE			CPartTemplateID, CQuantityReqd							 	smallint unsigned;
DECLARE			CAvail, CDDStart, CStartDate, CFinish, CDDFinish, CWOODD	date;
DECLARE			DDFinishStore, DDStartStore									date;
DECLARE			CPartNumber, COperation										varchar(25);
DECLARE			CSupplier													varchar(40);

DECLARE			OpRunMICursor CURSOR FOR
SELECT			OperationID, WorkOrderID, OrderID, PartTemplateID, PartNumber, 
				Qty, Operation, Avail, DDStart, StartDate, Finish, DDFinish, Supplier, OpNo, LastOp, WOODD

FROM 			TmpProdScheduleMI;


DECLARE 		CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

CREATE TEMPORARY TABLE TmpProdScheduleMI (
				OperationID		integer unsigned PRIMARY KEY, 
				WorkOrderID		mediumint unsigned, 
				OrderID			mediumint unsigned, 
				PartTemplateID	smallint unsigned, 
				PartNumber		varchar(25), 
				Qty 			mediumint unsigned, 
				Operation		varchar(25), 
				Avail			date, 
				DDStart			date, 
				StartDate		date, 
				Finish			date, 
				DDFinish		date,
				Supplier		varchar(40),
				OpNo			tinyint unsigned, 
				LastOp			tinyint unsigned, 
				WOODD			date)

AS	

	SELECT		O.pkOperationID AS OperationID,
				WO.pkWorkOrderID AS WorkOrderID, LI.fkOrderID AS OrderID, PT.pkPartTemplateID AS PartTemplateID, PT.PartNumber, 
						CEIL(((WO.QuantityReqd + WO.quantityOvers) *1.1) + 1) AS Qty,
				'Material Issue' AS Operation, 

						DATE(RO.CreateDate) AS Avail,

						NULL AS DDStart,

						(SELECT DATE(RunStart) FROM OperationMachine WHERE fkOperationID = O.pkOperationID) AS StartDate,

										(SELECT DATE(RunEnd) FROM OperationMachine WHERE fkOperationID = O.pkOperationID) AS Finish,


						DATE_ADD(WO.OrigDeliveryDate, INTERVAL -(SELECT FatDays FROM Fat WHERE FatType = 3) DAY) AS DDFinish,
 
				S.Alias AS Supplier, O.OperationNumber AS OpNo,
				(SELECT MAX(OperationNumber) 
				FROM Operation OPO 
				WHERE fkWorkOrderID = (SELECT fkWorkOrderID
										FROM Operation
										WHERE pkOperationID = O.pkOperationID)) AS LastOp,
				IF(ISNULL(WO.NewDeliveryDate), WO.OrigDeliveryDate, WO.NewDeliveryDate) AS WOODD

	FROM			Operation O
	INNER JOIN		WorkOrder WO
	ON				O.fkWorkOrderID = WO.pkWorkOrderID
	INNER JOIN		LineItem LI
	ON				WO.fkLineItemID = LI.pkLineItemID
	LEFT JOIN		PartTemplate PT
	ON				LI.fkPartTemplateID = PT.pkPartTemplateID
	INNER JOIN		OperationType OT
	ON				O.fkOperationTypeID = OT.pkOperationTypeID

	LEFT JOIN		OperationOrderItem OOI
	on				O.pkOperationID = OOI.fkOperationID
	LEFT JOIN		OrderItem OI
	ON				OOI.fkItemID = OI.pkItemID
	LEFT JOIN		RFQOrder RO
	ON				OI.fkRFQOID = RO.pkRFQOID
	LEFT JOIN		RFQOSupplier ROS
	ON				RO.pkRFQOID = ROS.fkRFQOID
	AND				ROS.Selected = 1
	LEFT JOIN		Supplier S
	ON				ROS.fkSupplierID = S.pkSupplierID

	WHERE			WO.Live = 1 AND (WO.complete <> 1 OR ISNULL(WO.Complete) = TRUE)
	AND 			O.fkOperationTypeID = 1 AND (O.Bypass <> 1 OR ISNULL(O.Bypass) = TRUE)
	AND				(RO.POType = 1 OR ISNULL(RO.POType))

	ORDER BY		WorkOrderID, OperationNumber ASC;

OPEN 		OpRunMICursor;

CREATE TEMPORARY TABLE TmpProdScheduleMI2 ( 
											OperationID		integer unsigned PRIMARY KEY, 
											WorkOrderID		mediumint unsigned, 
											OrderID			mediumint unsigned, 
											PartTemplateID	smallint unsigned, 
											PartNumber		varchar(25), 
											Qty 			mediumint unsigned, 
											Operation		varchar(25), 
											Avail			date, 
											DDStart			date, 
											StartDate		date, 
											Finish			date, 
											DDFinish		date,
											Supplier		varchar(40),
											OpNo			tinyint unsigned, 
											LastOp			tinyint unsigned, 
											WOODD			date)
				SELECT * FROM TmpProdScheduleMI
				ORDER BY		WOODD ASC;

read_loop: LOOP
		FETCH OpRunMICursor INTO COperationID, CWorkOrderID, COrderID, CPartTemplateID, CPartNumber, 
				CQty, COperation, CAvail, CDDStart, CStartDate, CFinish, CDDFinish, CSupplier, COpNo, CLastOp, CWOODD;

						SET			DDFinishStore = DATE_ADD(DATE_ADD(DATE_ADD(DATE_ADD(CWOODD, INTERVAL -(SELECT (SUM(TIME_TO_SEC(EstimatedSetTime)) + SUM(TIME_TO_SEC(EstimatedRunTime) * CQty)) / 32400
																							FROM 	Operation
																							WHERE 	fkWorkOrderID = CWorkOrderID
																							AND		(Bypass <> 1 OR ISNULL(Bypass) = TRUE)
																							AND	 	OperationNumber > COpNo) DAY_HOUR), 
														INTERVAL -(SELECT FatDays FROM Fat WHERE FatType = 1) DAY), 
												INTERVAL -((SELECT FatDays FROM Fat WHERE FatType = 2) - (SELECT FatDays FROM Fat WHERE FatType = 1)) DAY), 
										INTERVAL (SELECT FatDays FROM Fat WHERE FatType = 3) DAY);

						SET 		NWFinishDays = (5 * (DATEDIFF(CWOODD, DDFinishStore) DIV 7) + 
									MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(DDFinishStore) + WEEKDAY(CWOODD) + 1, 1));


						SET			DDStartStore = DATE_ADD(DDFinishStore, INTERVAL -(SELECT 	SUM(EstimatedSetTime * CQty) + SUM(EstimatedRunTime * CQty) 
																			FROM 	Operation
																			WHERE 	fkWorkOrderID = CWorkOrderID 
																			AND		(Bypass <> 1 OR ISNULL(Bypass) = TRUE)
																			AND	 	OperationNumber = COpNo) DAY_HOUR);

						SET			NWStartDays = (5 * (DATEDIFF(CWOODD, DDStartStore) DIV 7) + 
									MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(DDStartStore) + WEEKDAY(CWOODD) + 1, 1));


												UPDATE 		TmpProdScheduleMI2

						SET 		DDFinish = DATE_ADD(DDFinishStore, INTERVAL - NWFinishDays DAY),
 
									DDStart = DATE_ADD(DDStartStore, INTERVAL - NWStartDays DAY)

																																				
						WHERE 		OperationID = COperationID;


												UPDATE		Operation
						
						SET			ScheduledStartDate = DATE_ADD(DDStartStore, INTERVAL - NWStartDays DAY)

						WHERE		pkOperationID = COperationID;

 		IF done THEN
 			LEAVE read_loop;

 		END IF;

END LOOP;

Close OpRunMICursor;

SELECT 		OperationID, WorkOrderID, OrderID, PartTemplateID, PartNumber, 
			Qty, Operation, DDStart, StartDate, Finish, DDFinish, Supplier, OpNo, LastOp, Avail
 
FROM 		TmpProdScheduleMI2
ORDER BY	DDStart ASC;

DROP TABLE 	TmpProdScheduleMI2;
DROP TABLE 	TmpProdScheduleMI;
	
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetProductionScheduleSC` ()  BEGIN

DECLARE 		done 														INT DEFAULT FALSE;
DECLARE			COperationID												int unsigned;
DECLARE			CWorkOrderID, COrderID, CQty, NWFinishDays, NWStartDays		mediumint unsigned;
DECLARE			COpNo, CLastOp, COperationNumber							tinyint unsigned;
DECLARE			CPartTemplateID, CQuantityReqd								smallint unsigned;
DECLARE			CAvail, CDDStart, CStartDate, CFinish, CDDFinish, CWOODD	date;
DECLARE			DDFinishStore, DDStartStore									date;
DECLARE			CPartNumber, COperation										varchar(25);
DECLARE			CSupplier													varchar(40);

DECLARE			OpRunSCCursor CURSOR FOR
SELECT			OperationID, WorkOrderID, OrderID, PartTemplateID, PartNumber, 
				Qty, Operation, Avail, DDStart, StartDate, Finish, DDFinish, Supplier, OpNo, LastOp, WOODD

FROM 			TmpProdScheduleSC;


DECLARE 		CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

CREATE TEMPORARY TABLE TmpProdScheduleSC (
				OperationID		integer unsigned PRIMARY KEY, 
				WorkOrderID		mediumint unsigned, 
				OrderID			mediumint unsigned, 
				PartTemplateID	smallint unsigned, 
				PartNumber		varchar(25), 
				Qty 			mediumint unsigned, 
				Operation		varchar(25), 
				Avail			date, 
				DDStart			date, 
				StartDate		date, 
				Finish			date, 
				DDFinish		date,
				Supplier		varchar(40),
				OpNo			tinyint unsigned, 
				LastOp			tinyint unsigned, 
				WOODD			date)

AS	
SELECT		O.pkOperationID AS OperationID,
				WO.pkWorkOrderID AS WorkOrderID, LI.fkOrderID AS OrderID, PT.pkPartTemplateID AS PartTemplateID, PT.PartNumber, 
						CEIL(((WO.QuantityReqd + WO.quantityOvers) *1.1) + 1) AS Qty,
				TR.Description AS Operation, 

						DATE(RO.CreateDate) AS Avail,

						NULL AS DDStart,

						D.DispatchDate AS StartDate,

						NULL AS Finish,

						DATE_ADD(WO.OrigDeliveryDate, INTERVAL -(SELECT FatDays FROM Fat WHERE FatType = 2) DAY) AS DDFinish,
 
				S.Alias AS Supplier, O.OperationNumber AS OpNo,
				(SELECT MAX(OperationNumber) 
				FROM Operation OPO 
				WHERE fkWorkOrderID = (SELECT fkWorkOrderID
										FROM Operation
										WHERE pkOperationID = O.pkOperationID)) AS LastOp,
				IF(ISNULL(WO.NewDeliveryDate), WO.OrigDeliveryDate, WO.NewDeliveryDate) AS WOODD

	FROM			Operation O
	LEFT JOIN		WorkOrder WO
	ON				O.fkWorkOrderID = WO.pkWorkOrderID
	LEFT JOIN		LineItem LI
	ON				WO.fkLineItemID = LI.pkLineItemID
	LEFT JOIN		PartTemplate PT
	ON				LI.fkPartTemplateID = PT.pkPartTemplateID
	LEFT JOIN		MachiningType MT
	ON				O.fkMachiningTypeID = MT.pkMachiningTypeID
	LEFT JOIN		OperationMachine OM
	ON				O.pkOperationID = OM.fkOperationID
	LEFT JOIN		User U
	ON				OM.fkUserIDRunStart = U.pkUserID
	INNER JOIN		OperationTreatment OTR
	ON				O.pkOperationID = OTR.pkOperationID
	LEFT JOIN		TreatmentList TR
	ON				OTR.fkTreatmentId = TR.pkTreatmentID

	LEFT JOIN		OperationOrderItem OOI
	on				O.pkOperationID = OOI.fkOperationID
	LEFT JOIN		OrderItem OI
	ON				OOI.fkItemID = OI.pkItemID
	LEFT JOIN		RFQOrder RO
	ON				OI.fkRFQOID = RO.pkRFQOID
	LEFT JOIN		RFQOSupplier ROS
	ON				RO.pkRFQOID = ROS.fkRFQOID
	LEFT JOIN		Supplier S
	ON				ROS.fkSupplierID = S.pkSupplierID
	LEFT JOIN		POn PON
	ON				RO.pkRFQOID = PON.fkRFQOID
	LEFT JOIN		Dispatch D 
	ON				PON.pkPOnID = D.fkPOnID

	WHERE			WO.Live = 1 AND (WO.complete <> 1 OR ISNULL(WO.Complete) = true)
	AND				(O.Bypass <> 1 OR ISNULL(O.Bypass) = TRUE)
	AND 			(O.fkOperationTypeID = 3 
	OR 				(O.fkOperationTypeID = 2 AND O.Subcon = 1))
	AND				(RO.POType = 2 OR ISNULL(RO.POType))
	AND          	(ROS.Selected = 1 OR ISNULL(ROS.Selected))

	ORDER BY		WorkOrderID, OperationNumber ASC;


OPEN 		OpRunSCCursor;

CREATE TEMPORARY TABLE TmpProdScheduleSC2 ( 
											OperationID		integer unsigned PRIMARY KEY, 
											WorkOrderID		mediumint unsigned, 
											OrderID			mediumint unsigned, 
											PartTemplateID	smallint unsigned, 
											PartNumber		varchar(25), 
											Qty 			mediumint unsigned, 
											Operation		varchar(25), 
											Avail			date, 
											DDStart			date, 
											StartDate		date, 
											Finish			date, 
											DDFinish		date,
											Supplier		varchar(40),
											OpNo			tinyint unsigned, 
											LastOp			tinyint unsigned, 
											WOODD			date)
				SELECT * FROM TmpProdScheduleSC
				ORDER BY		WOODD ASC;

read_loop: LOOP
		FETCH OpRunSCCursor INTO COperationID, CWorkOrderID, COrderID, CPartTemplateID, CPartNumber, 
				CQty, COperation, CAvail, CDDStart, CStartDate, CFinish, CDDFinish, CSupplier, COpNo, CLastOp, CWOODD;


				IF COpNo = CLastOp THEN 

						SET			DDFinishStore = DATE_ADD(CWOODD, INTERVAL -(SELECT FatDays 
																				FROM Fat 
																				WHERE FatType = 3) DAY);

						SET			NWFinishDays = (5 * (DATEDIFF(CWOODD, DDFinishStore) DIV 7) + 
									MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(DDFinishStore) + WEEKDAY(CWOODD) + 1, 1));


						SET			DDStartStore = DATE_ADD(DDFinishStore, INTERVAL -(SELECT 	(SUM(TIME_TO_SEC(EstimatedSetTime)) + SUM(TIME_TO_SEC(EstimatedRunTime) * CQty)) / 32400
																				FROM 	Operation
																				WHERE 	fkWorkOrderID = CWorkOrderID 
																				AND	 	OperationNumber = COpNo) DAY_HOUR);

						SET			NWStartDays = (5 * (DATEDIFF(CWOODD, DDStartStore) DIV 7) + 
									MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(DDStartStore) + WEEKDAY(CWOODD) + 1, 1));


						UPDATE 		TmpProdScheduleSC2
						SET 		DDFinish = DATE_ADD(DDFinishStore, INTERVAL - NWFinishDays DAY),

									DDStart = DATE_ADD(DDStartStore, INTERVAL - NWStartDays DAY)

																																													
						WHERE 		OperationID = COperationID;

				ELSE

						SET			DDFinishStore = DATE_ADD(DATE_ADD(DATE_ADD(DATE_ADD(CWOODD, INTERVAL -(SELECT (SUM(TIME_TO_SEC(EstimatedSetTime)) + SUM(TIME_TO_SEC(EstimatedRunTime) * CQty)) / 32400
																							FROM 	Operation
																							WHERE 	fkWorkOrderID = CWorkOrderID 
																							AND		(Bypass <> 1 OR ISNULL(Bypass) = TRUE)
																							AND	 	OperationNumber > COpNo) DAY_HOUR), 
														INTERVAL -(SELECT FatDays FROM Fat WHERE FatType = 1) DAY), 
												INTERVAL -((SELECT FatDays FROM Fat WHERE FatType = 2) - (SELECT FatDays FROM Fat WHERE FatType = 1)) DAY), 
										INTERVAL (SELECT FatDays FROM Fat WHERE FatType = 3) DAY);

						SET			NWFinishDays = (5 * (DATEDIFF(CWOODD, DDFinishStore) DIV 7) + 
									MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(DDFinishStore) + WEEKDAY(CWOODD) + 1, 1));


						SET			DDStartStore = DATE_ADD(DDFinishStore, INTERVAL -(SELECT 	(SUM(TIME_TO_SEC(EstimatedSetTime)) + SUM(TIME_TO_SEC(EstimatedRunTime) * CQty)) / 32400 
																			FROM 	Operation
																			WHERE 	fkWorkOrderID = CWorkOrderID
																			AND		(Bypass <> 1 OR ISNULL(Bypass) = TRUE)
																			AND	 	OperationNumber = COpNo) DAY_HOUR);

						SET			NWStartDays = (5 * (DATEDIFF(CWOODD, DDStartStore) DIV 7) + 
									MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(DDStartStore) + WEEKDAY(CWOODD) + 1, 1));


						UPDATE 		TmpProdScheduleSC2
						SET 		DDFinish = DATE_ADD(DDFinishStore, INTERVAL - NWFinishDays DAY),

									DDStart = DATE_ADD(DDStartStore, INTERVAL - NWStartDays DAY)

																																				
						WHERE 		OperationID = COperationID;
				END IF;


												UPDATE		Operation
						
						SET			ScheduledStartDate = DATE_ADD(DDStartStore, INTERVAL - NWStartDays DAY)

						WHERE		pkOperationID = COperationID;

 		IF done THEN
 			LEAVE read_loop;

 		END IF;

END LOOP;

Close OpRunSCCursor;

SELECT 		OperationID, WorkOrderID, OrderID, PartTemplateID, PartNumber, 
			Qty, Operation, DDStart, StartDate, Finish, DDFinish, Supplier, OpNo, LastOp, Avail
 
FROM 		TmpProdScheduleSC2
ORDER BY	DDStart ASC;

DROP TABLE 	TmpProdScheduleSC2;
DROP TABLE 	TmpProdScheduleSC;
	
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetPrograms` (IN `OperationID` INT, IN `MachineID` INT)  BEGIN
SELECT	 		OMP.fkProgramNumber
FROM			Kythera.OpMachineProgram OMP  

WHERE			OMP.fkOperationID = OperationID
AND				OMP.fkMachineID = MachineID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetRejectsList` ()  BEGIN
SELECT		pkRejectID AS RejectNoteID

FROM		RejectGRB

WHERE		Closed <> 1 OR closed IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetRemarks` ()  BEGIN
SELECT		pkRemarkID AS RemarkID, Remark

FROM		Remark

WHERE		Deleted <> 1 OR Deleted IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetRemarksComments` ()  BEGIN
SELECT		pkRemarkID AS RemarkID, Remark

FROM		Remark

WHERE		Deleted <> 1 OR Deleted IS NULL;


SELECT		pkCommentID AS CommentID, Comments AS Comment

FROM		Comments

WHERE		Deleted <> 1 OR Deleted IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetResourceList` ()  BEGIN
SELECT 	pkUserID AS UserID, UName

FROM		User U

WHERE		EXISTS (SELECT fkDeptID FROM UserDept WHERE fkUserID = U.pkUserID AND fkDeptID = 1)
AND			Employed = 1;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetRFQPODetail` (IN `RFQNo` SMALLINT UNSIGNED, `PONo` SMALLINT UNSIGNED)  BEGIN
SELECT		IF(RFQNo IS NOT NULL, RO.pkRFQOID, '') AS RFQOID, RO.CreateDate AS Raised, RO.Complete, 
			RN.pkRFQnID AS QuoteRaised, RO.POType, (IF(PONo IS NOT NULL, (SELECT Count(*) FROM OrderItem
																		INNER JOIN POn 
																		ON OrderItem.fkRFQOID = POn.fkRFQOID
																		WHERE pkPOnID = PONo),
			(SELECT Count(*) FROM OrderItem
			INNER JOIN RFQn 
			ON OrderItem.fkRFQOID = RFQn.fkRFQOID
			WHERE pkRFQnID = RFQNo))) AS TotalItems, PT.PartNumber

FROM		RFQOrder RO
LEFT JOIN	RFQn RN
ON			RO.pkRFQOID = RN.fkRFQOID
LEFT JOIN	POn PN
ON			RO.pkRFQOID = PN.fkRFQOID
INNER JOIN	OrderItem I
ON			RO.pkRFQOID = I.fkRFQOID
LEFT JOIN	OperationOrderItem OOI
ON			I.pkItemID = OOI.fkItemID
INNER JOIN	Operation O
ON			OOI.fkOperationID = O.pkOperationID
INNER JOIN	WorkOrder WO
ON			O.fkWorkOrderID = WO.pkWorkOrderID
INNER JOIN	PartTemplate PT
ON			I.fkPartTemplateID = PT.pkPartTemplateID

WHERE		IF(PONo IS NOT NULL,
				PN.pkPOnID  = PONo,
				RN.pkRFQnID = RFQNo);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSelectedSuppliers` (IN `RFQOID` SMALLINT UNSIGNED)  BEGIN
SELECT		fkSupplierID AS SupplierID, TotalCost, DeliveryCost AS DeliveryPrice

FROM		RFQOSupplier RS

WHERE		fkRFQOID = RFQOID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSelectedWODetail` (IN `WONo` MEDIUMINT UNSIGNED)  BEGIN
SELECT		CONCAT(PT.PartNumber, '//', PT.IssueNumber) AS PartNumber, WO.QuantityReqd + WO.QuantityOvers AS Qty, 
			O.pkOperationID AS OperationID, OperationNumber AS OperationNo, fkOperationTypeID AS OpTypeID

FROM		Operation O
INNER JOIN	WorkOrder WO
ON			O.fkWorkOrderID = WO.pkWorkOrderID
INNER JOIN	LineItem LI
ON			WO.fkLineItemID = LI.pkLineItemID
INNER JOIN	PartTemplate PT
ON			LI.fkPartTemplateID = PT.pkPartTemplateID

WHERE		WO.pkWorkOrderID = WONo

ORDER BY OperationNo;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetShapeList` ()  BEGIN
SELECT		pkShapeID AS ShapeID, Description AS Shape
FROM		Shape

WHERE		Deleted <> 1;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetStaffDetail` (IN `StaffUserID` SMALLINT UNSIGNED)  BEGIN
SELECT		pkUserID AS PayrollNumber, FirstName, LastName, Address1, Address2, Address3, Address4, Postcode, HomePhone,
			MobilePhone, Email, ChargeRate, SkillLevel, Employed, Extension, ReportsTo, POLimit, SpendLimit,
			ForkOperator, FirstAid, LeaveDays, Salary, Hours, Security

FROM		User

WHERE		pkUserID = StaffUserID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetStaffList` (IN `SearchString` VARCHAR(50))  BEGIN
SELECT			pkUserID AS StaffUserID, CONCAT(FirstName, ' ', LastName) AS Name

FROM			User

WHERE			CONCAT(FirstName, ' ', LastName) LIKE CONCAT(SearchString, '%');
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetStaffRoles` (IN `StaffUserID` SMALLINT UNSIGNED)  BEGIN
SELECT		D.pkDeptID AS RoleID, D.Description AS Role

FROM		User U
INNER JOIN	UserDept UD
ON			pkUserID = UD.fkUserID
INNER JOIN	Dept D
ON			UD.fkDeptID = D.pkDeptID

WHERE		U.pkUserID = StaffUserID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetStdSuppliers` ()  BEGIN
SELECT		pkSupplierID AS StdSupplierID, Alias, Email
FROM		Supplier

WHERE		Deleted <> 1 OR Deleted IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetStoreAreaList` ()  BEGIN
SELECT		pkStoreAreaID AS StoreAreaID, StoreArea

FROM		StoreArea;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetStoreLocations` (IN `StoreAreaID` TINYINT UNSIGNED)  BEGIN
SELECT 	XCoord AS XLocation, YCoord AS YLocation
FROM 		XCoordinate X
JOIN		YCoordinate Y
ON			X.fkStoreAreaID = Y.fkStoreAreaID

WHERE		X.fkStoreAreaID = StoreAreaID 
AND 		((X.Deleted <> 1 OR X.Deleted IS NULL) AND (Y.Deleted <> 1 OR Y.Deleted IS NULL));
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSubconDetail` (IN `WorkOrderID` INT, IN `OperationID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		T.Description AS Treatment, S.SupplierName, OT.LeadTime, OT.SelectedPrice, OT.ApprovalID

FROM		Operation O
INNER JOIN	OperationTreatment OT
	ON		O.pkOperationID = OT.pkOperationID
INNER JOIN	TreatmentList T
	ON		OT.fkTreatmentID = T.pkTreatmentID
INNER JOIN	SupplierTreatment ST
	ON		OT.pkOperationID = ST.fkOperationID
INNER JOIN	Supplier S
	ON		ST.fkSupplierID = S.pkSupplierID

WHERE		O.fkWorkOrderID = WorkOrderID
AND			O.pkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSubconNotes` (IN `WorkOrderID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		Note

FROM		Operation O
INNER JOIN	OperationNote OPN
ON			O.pkOperationID = OPN.fkOperationID

WHERE		O.fkWorkOrderID = WorkOrderID
AND			OPN.fkNoteTypeID = 3;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSubconReview` (IN `WorkOrderID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		T.Description AS Treatment, S.SupplierName, OT.LeadTime, OT.SelectedPrice, OT.ApprovalID

FROM		Operation O
INNER JOIN	OperationTreatment OT
	ON		O.pkOperationID = OT.pkOperationID
INNER JOIN	TreatmentList T
	ON		OT.fkTreatmentID = T.pkTreatmentID
INNER JOIN	SupplierTreatment ST
	ON		OT.pkOperationID = ST.fkOperationID
INNER JOIN	Supplier S
	ON		ST.fkSupplierID = S.pkSupplierID

WHERE		O.fkWorkOrderID = WorkOrderID;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSundriesList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkSundryID AS SundryID , Sundry AS Description

FROM		Sundry

WHERE		Deleted <> 1 OR Deleted IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSupplierAdditional` (IN `SupplierID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkSupplierAdditionalID AS AdditionalID, 
			IF(TimeType=1, 'Shutdown', IF(TimeType=2, 'Collection', IF(Timetype=3,'Other', ''))) AS Additional, 
			StartPoint, EndPoint
			
FROM		SupplierAdditionalTime

WHERE		fkSupplierID = SupplierID;	
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSupplierApprovals` (IN `SupplierID` SMALLINT UNSIGNED)  BEGIN
SELECT		A.pkApprovalID AS ApprovalID, A.Description AS ISOApproval, SA.ExpiryDate AS Expiry

FROM		Supplier S
INNER JOIN	SupplierApproval SA
ON			S.pkSupplierID = SA.fkSupplierID
INNER JOIN	Approval A
ON			SA.fkApprovalID = A.pkApprovalID

WHERE		S.pkSupplierID = SupplierID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSupplierContacts` (IN `SupplierID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkContactID AS ContactID, ContactName, Phone, Email,
			Prime 
			
FROM		SupplierContact SC

WHERE		fkSupplierID = SupplierID
AND			(Deleted <> 1 OR Deleted IS NULL);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSupplierDetail` (IN `SupplierID` SMALLINT UNSIGNED)  BEGIN
SELECT		SupplierName AS Supplier, Alias, Address1, Address2, Address3, Address4, Postcode, Phone,
			Facsimile, Website, Email, MinOrderCharge, PaymentTerms, MaterialSupplier,
			TreatmentSupplier, CalibrationSupplier, ToolingSupplier, MiscSupplier, 
			IF(Deleted = 1, 0, 1) AS Current

FROM		Supplier S

WHERE		S.pkSupplierID = SupplierID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSupplierList` (IN `SearchString` VARCHAR(50))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		S.pkSupplierID AS SupplierID, S.SupplierName AS Supplier 

FROM 		Supplier S

WHERE 		(Deleted <> 1 OR Deleted IS NULL)
AND			Alias LIKE CONCAT(SearchString, '%');
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSupplierNADCAPs` (IN `SupplierID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		pkApprovalID AS NADCAPID, Description AS NADCAPApproval, ExpiryDate AS Expiry

FROM			NadcapApproval A
INNER JOIN		SupplierNadcap SN
ON				A.pkApprovalID = SN.fkApprovalID

WHERE			SN.fkSupplierID = SupplierID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSupplierNotes` (IN `SupplierID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		Note
FROM		SupplierNote

WHERE		fkSupplierID = SupplierID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSupplierOrderHistory` (IN `SupplierID` SMALLINT UNSIGNED)  BEGIN
SELECT			CompleteDate AS Completed, pkPOnID AS PONo, CONCAT(U.FirstName, ' ', U.LastName) AS AddedBy

FROM			RFQOrder RO
INNER JOIN		POn P
ON				RO.pkRFQOID = P.fkRFQOID
INNER JOIN		RFQOSupplier ROS
ON				RO.pkRFQOID = ROS.fkRFQOID
LEFT JOIN		User U
ON				RO.fkUserID = U.pkUserID

WHERE			ROS.fkSupplierID = SupplierID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSupplierOrderHistoryWO` (IN `PONo` SMALLINT UNSIGNED)  BEGIN
SELECT DISTINCT pkworkOrderID AS WONo, PT.PartNumber AS PartNo, WO.QuantityReqd AS Qty
			
FROM			WorkOrder WO
INNER JOIN		LineItem LI
ON				WO.fkLineItemID = LI.pkLineItemID
LEFT JOIN		PartTemplate PT
ON				LI.fkPartTemplateID = PT.pkPartTemplateID
INNER JOIN		Operation O
ON				WO.pkWorkOrderID = O.fkWorkOrderID
INNER JOIN		OperationOrderItem OOI
ON				O.pkOperationID = OOI.fkOperationID
INNER JOIN		OrderItem OI
ON				OOI.fkItemID = OI.pkItemID
INNER JOIN		RFQOrder RO
ON				OI.fkRFQOID = RO.pkRFQOID
INNER JOIN		POn P
ON				RO.pkRFQOID = P.fkRFQOID

WHERE			P.pkPOnID = PONo;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSupplierPerformance` ()  BEGIN
     
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
 
SELECT 		Alias, IF(ISNULL(MaterialSupplier), 0, MaterialSupplier) AS Material, 
			IF(ISNULL(TreatmentSupplier), 0 , TreatmentSupplier) AS Treatment, 
			IF(ISNULL(CalibrationSupplier),
				IF(ISNULL(ToolingSupplier), 
					IF(ISNULL(MiscSupplier), 0, MiscSupplier), ToolingSupplier), CalibrationSupplier) AS Miscellaneous,
			(SELECT count(*)  
FROM		RFQOSupplier RS
INNER JOIN	rfqorder RO
ON			RS.fkRFQOID = RO.pkRFQOID

WHERE		fkSupplierID = S.pkSupplierID)  AS 'Total Orders',

			sum(
(SELECT  	count(*)
FROM		rfqorder RO
INNER JOIN	RFQOSupplier RS
ON			RO.pkRFQOID = RS.fkRFQOID

WHERE		RO.pkRFQOID NOT IN (SELECT fkItemID FROM POGoods)
AND			(RO.Cancelled <> 1 OR ISNULL(RO.Cancelled))
AND 		pkRFQOID = RS.fkRFQOID)) AS 'Outstanding',

			sum(
(SELECT  	count(*)
FROM		rfqorder RO
INNER JOIN	RFQOSupplier RS
ON			RO.pkRFQOID = RS.fkRFQOID

WHERE		RO.pkRFQOID IN (SELECT fkItemID FROM POGoods)
AND			(RO.Cancelled <> 1 OR ISNULL(RO.Cancelled))
AND 		pkRFQOID = RS.fkRFQOID)) AS 'Delivered',


			sum(
(SELECT  	count(*)
FROM		rfqorder RO
WHERE		RO.Cancelled = 1
AND 		pkRFQOID = RS.fkRFQOID)) AS Cancelled,

			CAST(sum(
(SELECT  	count(LeadDays)
FROM		RFQorder RO
INNER JOIN	OrderItem OI
ON			RO.pkRFQOID = OI.fkRFQOID
WHERE		(RO.Cancelled <> 1 OR ISNULL(RO.Cancelled))
AND			OI.pkItemID IN (SELECT fkItemID FROM POGoods)
AND			ISNULL(LeadDays) = FALSE
AND 		pkRFQOID = RS.fkRFQOID) /
(SELECT  	count(*)
FROM		RFQorder RO
INNER JOIN	OrderItem OI
ON			RO.pkRFQOID = OI.fkRFQOID
WHERE		(RO.Cancelled <> 1 OR ISNULL(RO.Cancelled))
AND			OI.pkItemID IN (SELECT fkItemID FROM POGoods)
AND			ISNULL(LeadDays) = FALSE
AND 		pkRFQOID = RS.fkRFQOID)) AS DECIMAL(5,0))  AS AvLeadDays,


sum(
(SELECT  	count(DATEDIFF(ReceivedDate, DATE(PO.CreateDate)) )
FROM		rfqorder PO
INNER JOIN	OrderItem OI
ON			PO.pkRFQOID = OI.fkRFQOID
INNER JOIN	pogoods PG
ON			OI.pkItemID = PG.fkItemID
INNER JOIN	goodsin GI
ON			PG.fkGRBID = GI.pkGRBID	

WHERE		(PO.Cancelled <> 1 OR ISNULL(PO.Cancelled))
AND			(LeadDays > 0 OR ISNULL(LeadDays) = false)
AND 		pkRFQOID = RS.fkRFQOID
AND			RS.Selected = 1)) AS ActualDays, 


			sum(
(SELECT  	count(*) 
FROM		rfqorder PO
INNER JOIN	OrderItem OI
ON			PO.pkRFQOID = OI.fkRFQOID
INNER JOIN	pogoods PG
ON			OI.pkItemID = PG.fkItemID
INNER JOIN	goodsin GI
ON			PG.fkGRBID = GI.pkGRBID	

WHERE		(PO.Cancelled <> 1 OR ISNULL(PO.Cancelled))
AND			(LeadDays > 0 OR ISNULL(LeadDays) = false)
AND 		pkRFQOID = RS.fkRFQOID
AND			RS.Selected = 1
AND			DATEDIFF(ReceivedDate, (DATE_ADD(DATE(PO.CreateDate), INTERVAL LeadDays DAY))) <>0)) AS EarlyLate,


			sum(
(SELECT  	count(*) 
FROM		rfqorder PO
INNER JOIN	OrderItem OI
ON			PO.pkRFQOID = OI.fkRFQOID
INNER JOIN	pogoods PG
ON			OI.pkItemID = PG.fkItemID
INNER JOIN	goodsin GI
ON			PG.fkGRBID = GI.pkGRBID	

WHERE		(PO.Cancelled <> 1 OR ISNULL(PO.Cancelled))
AND			(LeadDays > 0 OR ISNULL(LeadDays) = false)
AND 		pkRFQOID = RS.fkRFQOID
AND			RS.Selected = 1
AND			DATEDIFF((DATE_ADD(DATE(PO.CreateDate), INTERVAL LeadDays DAY)), ReceivedDate) >4)) AS Early,


			sum(
(SELECT  	COUNT(*)
FROM		rfqorder PO
INNER JOIN	OrderItem OI
ON			PO.pkRFQOID = OI.fkRFQOID
INNER JOIN	pogoods PG
ON			OI.pkItemID = PG.fkItemID
INNER JOIN	goodsin GI
ON			PG.fkGRBID = GI.pkGRBID	

WHERE		(PO.Cancelled <> 1 OR ISNULL(PO.Cancelled))
AND			(LeadDays > 0 OR ISNULL(LeadDays) = false)
AND 		pkRFQOID = RS.fkRFQOID
AND			RS.Selected = 1		
AND 		DATE_ADD(DATE(PO.CreateDate), INTERVAL LeadDays DAY) <= ReceivedDate)) AS OnTime,


			sum(
(SELECT  	count(*) 
FROM		rfqorder PO
INNER JOIN	OrderItem OI
ON			PO.pkRFQOID = OI.fkRFQOID
INNER JOIN	pogoods PG
ON			OI.pkItemID = PG.fkItemID
INNER JOIN	goodsin GI
ON			PG.fkGRBID = GI.pkGRBID	

WHERE		(PO.Cancelled <> 1 OR ISNULL(PO.Cancelled))
AND			(LeadDays > 0 OR ISNULL(LeadDays) = false)
AND 		pkRFQOID = RS.fkRFQOID
AND			RS.Selected = 1
AND			DATEDIFF(ReceivedDate, (DATE_ADD(DATE(PO.CreateDate), INTERVAL LeadDays DAY))) >4)) AS Late,


			sum(
(SELECT  	count(*) 
FROM		rfqorder PO
INNER JOIN	OrderItem OI
ON			PO.pkRFQOID = OI.fkRFQOID
INNER JOIN	pogoods PG
ON			OI.pkItemID = PG.fkItemID

WHERE		(PO.Cancelled <> 1 OR ISNULL(PO.Cancelled))
AND			(LeadDays > 0 OR ISNULL(LeadDays) = false)
AND 		pkRFQOID = RS.fkRFQOID
AND			RS.Selected = 1
AND			fkGRBID IN (SELECT fkGRBID FROM RejectGRB))) AS Rejects,


0 AS concessions,


IF(ISNULL(sum(
	(SELECT  	count(*)
	FROM		rfqorder RO
	INNER JOIN	RFQOSupplier RS
 	ON			RO.pkRFQOID = RS.fkRFQOID))), 0, 
	CONCAT(CAST(100 - sum(
					(SELECT  	count(*) 
					FROM		rfqorder PO
					INNER JOIN	OrderItem OI
					ON			PO.pkRFQOID = OI.fkRFQOID
					INNER JOIN	pogoods PG
					ON			OI.pkItemID = PG.fkItemID

					WHERE		(PO.Cancelled <> 1 OR ISNULL(PO.Cancelled))
					AND			(LeadDays > 0 OR ISNULL(LeadDays) = false)
					AND 		pkRFQOID = RS.fkRFQOID
					AND			RS.Selected = 1
					AND			fkGRBID IN (SELECT fkGRBID FROM RejectGRB))) /
															
					(SELECT  	count(*)
					FROM		rfqorder RO
					INNER JOIN	RFQOSupplier RS
					ON			RO.pkRFQOID = RS.fkRFQOID)AS DECIMAL(5,2)), '%')) AS QRating,


CONCAT(CAST((sum(
(SELECT  	COUNT(*)
FROM		rfqorder PO
INNER JOIN	OrderItem OI
ON			PO.pkRFQOID = OI.fkRFQOID
INNER JOIN	pogoods PG
ON			OI.pkItemID = PG.fkItemID
INNER JOIN	goodsin GI
ON			PG.fkGRBID = GI.pkGRBID	

WHERE		(PO.Cancelled <> 1 OR ISNULL(PO.Cancelled))
AND			(LeadDays > 0 OR ISNULL(LeadDays) = false)
AND 		pkRFQOID = RS.fkRFQOID
AND			RS.Selected = 1		
AND 		DATE_ADD(DATE(PO.CreateDate), INTERVAL LeadDays DAY) <= ReceivedDate)) +
IF(ISNULL(sum(
	(SELECT  	count(*)
	FROM		rfqorder RO
	INNER JOIN	RFQOSupplier RS
 	ON			RO.pkRFQOID = RS.fkRFQOID))), 0, 
	CAST(100 - sum(
					(SELECT  	count(*) 
					FROM		rfqorder PO
					INNER JOIN	OrderItem OI
					ON			PO.pkRFQOID = OI.fkRFQOID
					INNER JOIN	pogoods PG
					ON			OI.pkItemID = PG.fkItemID

					WHERE		(PO.Cancelled <> 1 OR ISNULL(PO.Cancelled))
					AND			(LeadDays > 0 OR ISNULL(LeadDays) = false)
					AND 		pkRFQOID = RS.fkRFQOID
					AND			RS.Selected = 1
					AND			fkGRBID IN (SELECT fkGRBID FROM RejectGRB))) /
															
					(SELECT  	count(*)
					FROM		rfqorder RO
					INNER JOIN	RFQOSupplier RS
					ON			RO.pkRFQOID = RS.fkRFQOID)AS DECIMAL(5,2))))/2 AS DECIMAL(5,2)), '%') AS VRating

FROM		RFQOSupplier RS
INNER JOIN	Supplier S
ON			RS.fkSupplierID = S.pkSupplierID

GROUP BY	Alias;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSupplierPerformanceTotals` ()  BEGIN

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT			'1st class' AS PerformanceGroup, 51.9 AS Material, 40.0 AS Treatments, 24.1 AS Miscellaneous
UNION
SELECT			'Standard' AS PerformanceGroup, 44.4 AS Material, 46.7 AS Treatments, 44.8 AS Miscellaneous
UNION
SELECT			'Sub standard' AS PerformanceGroup, 3.7 AS Material, 13.3 AS Treatments, 31.0 AS Miscellaneous
UNION
SELECT			'Unacceptable' AS PerformanceGroup, 0 AS Material, 0 AS Treatments, 0 AS Miscellaneous
UNION
SELECT			'Totals' AS PerformanceGroup, 90.2 AS Material, 83.6 AS Treatments, 86.8 AS Miscellaneous;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSupplierPersonnel` (IN `SupplierID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT			Type1, Type2, Type3, Type4, Type5

FROM			Supplier

WHERE			pkSupplierID = SupplierID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetSupplyConditionList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkSupplyConditionID AS SupplyConditionID , Description

FROM		SupplyCondition

WHERE		Deleted <> 1 OR Deleted IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetTemplateDetail` (IN `EngineeringTemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		fkPartTemplateID AS PartTemplateID, PartNumber, IssueNumber, EngineeringIssue AS EngIssue, PlanningComplete

FROM		EngineeringTemplate ET
INNER JOIN	PartTemplate PT
ON			ET.fkPartTemplateID = PT.pkPartTemplateID

WHERE		pkEngineeringTemplateID = EngineeringTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetTemplateEngineering` (IN `OperationTemplateID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		DISTINCT OTMP.fkMachineID AS MachineID, RiskStatement, EngComplete

FROM		OperationTemplate OT
INNER JOIN	OpTmpltMachineProgram OTMP
ON			OT.pkOperationTemplateID = OTMP.fkOperationTemplateID

WHERE		pkOperationTemplateID = OperationTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetTemplateMaterial` (IN `OperationTemplateID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT			FreeIssue, IF(TM.Description IS NOT NULL, '', MS.MaterialSpec) AS Spec, 
							IF(TM.Description IS NOT NULL, TM.Description, MS.Description) AS Description, 
				Size, UnitID, fkShapeID AS ShapeID, PartLength, fkApprovalID AS ApprovalID, SPUfkUOMID AS SPUID, 
				Yield, TestPieces, TM.StdDeliveryDays, IssueInstruction, StdfkSupplierID AS StdSupplierID, S.Email AS StdSupplierEmail

FROM			TemplateMaterial TM
INNER JOIN		MaterialSpec MS
ON				TM.fkMaterialSpecID = MS.pkMaterialSpecID
LEFT JOIN		Supplier S
ON				TM.StdfkSupplierID = S.pkSupplierID
	
WHERE			pkOperationTemplateID = OperationTemplateID;			
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetTemplateProcesses` (IN `OperationTemplateID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		ProcessNote

FROM		TemplateProcess

WHERE		fkOperationTemplateID = OperationTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetTmpltApprovalComments` (IN `OperationTemplateID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		pkTemplateApprovalCommentID AS CommentID, Comment

FROM		TemplateApprovalComments

WHERE		fkOperationTemplateID = OperationTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetTmpltConformityNotes` (IN `OperationTemplateID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

SELECT		pkTemplateNoteID AS NoteID, Note

FROM		TemplateNote TN

WHERE		TN.fkOperationTemplateID = OperationTemplateID
AND			TN.fkNoteTypeID = 2; 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetTmpltPOComments` (IN `OperationTemplateID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		pkTemplatePOCommentID AS CommentID, Comment

FROM		TemplatePOComments

WHERE		fkOperationTemplateID = OperationTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetTmpltProcesses` (IN `OperationTemplateID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		ProcessNote		

FROM		TemplateProcess TP

WHERE		TP.fkOperationTemplateID = OperationTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetTodoList` (IN `DeptID` TINYINT UNSIGNED)  BEGIN
SELECT		pkTodoID AS TodoID, if(DATEDIFF(now(), TD.RaisedDate) = 0, "Today", if(DATEDIFF(now(), TD.RaisedDate) = 1, "Yesterday",
				CONCAT(CAST(DATEDIFF(now(), TD.RaisedDate) AS CHAR), ' days old' ))) AS Age, 
			A.Description AS ActionType, C.CustomerName,  
			WO.QuantityReqd	+ WO.QuantityOvers AS Qty, TD.fkWorkOrderID AS WONo, TD.fkPartTemplateID AS PartID, 
			TD.fkEngineeringTemplateID AS ETemplateID, PT.PartNumber AS PartNo, fkGRBID AS GRBID
			

FROM		Todo TD
LEFT JOIN	WorkOrder WO
ON			TD.fkWorkOrderID = WO.pkWorkOrderID
LEFT JOIN	LineItem LI
ON			WO.fkLineItemID = LI.pkLineItemID
LEFT JOIN	CustomerOrder CO
ON			LI.fkOrderID = CO.pkOrderID
LEFT JOIN	Customer C
ON			fkCustomerID = pkCustomerID
LEFT JOIN	PartTemplate PT
ON			LI.fkPartTemplateID = PT.pkPartTemplateID

INNER JOIN	ActionType	A
ON			TD.fkActionTypeID = A.pkActionTypeID

WHERE		TD.DeptID = DeptID
AND 		(TD.Complete <> 1 OR ISNULL(TD.complete));
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetTreatmentDispatchList` ()  BEGIN

		SELECT		OT4.pkOperationID AS OperationID, pkPOnID AS POnID, S.Alias AS Supplier, 
					IF( ISNULL(GI.pkGRBID), MAX(A.fkGRBID), MAX(GI.pkGRBID)) AS GRBNo,
					

					(SELECT	MIN(Op.EndQty)
					FROM 		Operation Op
					WHERE		Op.OperationNumber = O.OperationNumber-1 
					AND 		Op.fkWorkOrderID = O.fkWorkOrderID)  -
					(SELECT 	ScrappedQty
					FROM 		Inspection I1
					INNER JOIN OperationInspect OI1
					ON 			I1.pkInspectionID = OI1.fkInspectionID
					INNER JOIN Operation OP1
					ON 			OI1.fkOperationID = OP1.pkOperationID
					WHERE		OP1.OperationNumber = O.OperationNumber-1 
					AND 		OP1.fkWorkOrderID = O.fkWorkOrderID) AS Qty

		FROM		OperationTreatment OT4
		INNER JOIN	Operation O
		ON			OT4.pkOperationID = O.pkOperationID
		INNER JOIN	WorkOrder WO
		ON			O.fkWorkOrderID = WO.pkWorkOrderID
		INNER JOIN	OperationOrderItem OOI
		ON			OT4.pkOperationID = OOI.fkOperationID
		INNER JOIN	OrderItem OI
		ON			OOI.fkItemID = OI.pkItemID
		INNER JOIN	POn P
		ON			OI.fkRFQOID = P.fkRFQOID
		LEFT JOIN	RFQOSupplier RS
		ON			OI.fkRFQOID = RS.fkRFQOID
		LEFT JOIN	Supplier S
		ON			RS.fkSupplierID = S.pkSupplierID
												LEFT JOIN	Allocated A
		ON			O.pkOperationID = A.fkOperationID
		LEFT JOIN	GoodsIn GI
		ON			WO.pkWorkOrderID = GI.WorkOrderID

		WHERE		WO.Live = 1 AND (WO.complete <> 1 OR ISNULL(WO.Complete) = TRUE)
			AND		(O.Bypass <> 1 OR ISNULL(O.Bypass) = TRUE)
			AND		(WO.PreCancel <> 1 OR ISNULL(WO.PreCancel) = TRUE)
			AND 	(O.fkOperationTypeID = 3)
			AND		ISNULL((SELECT DispatchDate
							FROM OperationTreatment
							WHERE pkOperationID = O.pkOperationID))
UNION

				SELECT		O.pkOperationID AS OperationID, pkPOnID AS POnID, S.Alias AS Supplier, 
					IF( ISNULL(GI.pkGRBID), MAX(A.fkGRBID), MAX(GI.pkGRBID)) AS GRBNo,

					(SELECT 	MIN(Op.EndQty)
					FROM 		Operation Op
					WHERE		OperationNumber = O.OperationNumber-1 
					AND 		Op.fkWorkOrderID = O.fkWorkOrderID)  -
					(SELECT 	ScrappedQty
					FROM 		Inspection I2
					INNER JOIN OperationInspect OI2
					ON 			I2.pkInspectionID = OI2.fkInspectionID
					INNER JOIN Operation OP2
					ON 			OI2.fkOperationID = OP2.pkOperationID
					WHERE		OP2.OperationNumber = O.OperationNumber-1 
					AND 		OP2.fkWorkOrderID = O.fkWorkOrderID) AS Qty

		FROM		Operation O
		INNER JOIN	WorkOrder WO
		ON			O.fkWorkOrderID = WO.pkWorkOrderID
		LEFT JOIN	OperationOrderItem OOI
		ON			O.pkOperationID = OOI.fkOperationID
		LEFT JOIN	OrderItem OI
		ON			OOI.fkItemID = OI.pkItemID
		LEFT JOIN	POn P
		ON			OI.fkRFQOID = P.fkRFQOID
		LEFT JOIN	RFQOSupplier RS
		ON			OI.fkRFQOID = RS.fkRFQOID
		AND			Selected = 1
		LEFT JOIN	Supplier S
		ON			RS.fkSupplierID = S.pkSupplierID
				LEFT JOIN	Allocated A
		ON			O.pkOperationID = A.fkOperationID	
		LEFT JOIN	GoodsIn GI
		ON			WO.pkWorkOrderID = GI.WorkOrderID
		
		WHERE		WO.Live = 1 AND (WO.complete <> 1 OR ISNULL(WO.Complete) = true)
			AND		(WO.PreCancel <> 1 OR ISNULL(WO.PreCancel) = TRUE)
			AND		(O.Bypass <> 1 OR ISNULL(O.Bypass) = TRUE)
			AND 	(O.fkOperationTypeID = 2 AND O.Subcon = 1)

						AND		NOT ISNULL((SELECT 		InspectEnd 
								FROM 		Inspection I2
								INNER JOIN	OperationInspect OI2
								ON			I2.pkInspectionID = OI2.fkInspectionID
								INNER JOIN	Operation OO2
								ON			OI2.fkOperationID = OO2.pkOperationID
								WHERE		fkWorkOrderID = WO.pkWorkOrderID
								AND			OO2.OperationNumber = O.OperationNumber -1))
			AND		ISNULL((SELECT DispatchDate
								FROM OperationTreatment
								WHERE pkOperationID = O.pkOperationID));


END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetTreatmentsList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pktreatmentID AS TreatmentID , Description

FROM		TreatmentList

WHERE		Deleted <> 1 OR Deleted IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetUnpaidPurchases` ()  BEGIN
SELECT		fkGRBID, O.OperationNumber AS ItemNo, OI.Quantity AS Qty, GI.ReceivedDate AS DateIn, OI.Description, 
			(OI.Price * OI.Quantity) AS TotalPrice

FROM		Pon 				PON	
INNER JOIN	OrderItem 			OI
ON			PON.fkRFQOID 			= OI.fkRFQOID
INNER JOIN	POGoods 			PG
ON			OI.pkItemID 			= PG.fkItemID
LEFT JOIN	OperationOrderItem OOI
ON			OI.pkItemID 			= OOI.fkItemID
LEFT JOIN	Operation 			O
ON			OOI.fkOperationID 		= O.pkOperationID
INNER JOIN	GoodsIn 			GI
ON			PG.fkGRBID 				= GI.pkGRBID

WHERE		OI.pkItemID NOT IN (SELECT fkItemID FROM POInvoice);

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetUOMList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		pkUOMID, Description
				
FROM			UnitOfMeasure
WHERE			Deleted <> 1;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetUserName` (IN `UserID` SMALLINT UNSIGNED)  BEGIN
SELECT 		FirstName

FROM		User

WHERE		pkUserID = UserID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWebsites` ()  BEGIN
SELECT			pkWebsiteID AS WebsiteID, SiteAddress AS Website, Username AS UserName, SitePassword

FROM			WebsiteAccess

WHERE			Deleted <> 1 OR Deleted IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOADDList` (IN `OperationID` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkAPDID AS ADDID, FileHandle AS FileName, DropType AS DocType, AutoPrint

FROM		AssociatedPartDoc APD
INNER JOIN	DocumentDropType DDT
ON			APD.fkDocTypeID = DDT.pkDocumentDropTypeID

WHERE		fkPartTemplateID = (SELECT fkPartTemplateID 
									FROM LineItem LI
									INNER JOIN WorkOrder WO
									ON LI.pkLineItemID = WO.fkLineItemID
									INNER JOIN	Operation O
									ON WO.pkWorkOrderID = O.fkWorkOrderID
									WHERE O.pkOperationID = OperationID);

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOConformityNotes` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		Note AS ConformityNote

FROM		OperationNote OPN
INNER JOIN	Operation O
ON			OPN.fkOperationID = O.pkOperationID

WHERE		fkNoteTypeID = 2
AND			O.fkWorkOrderID = WorkOrderID;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWODeliveries` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		DispatchDate AS DeliveryDate

FROM		Dispatch D
INNER JOIN	GoodsIn GI
ON			D.fkGRBID = GI.pkGRBID

WHERE		GI.WorkOrderID = WorkOrderID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWODetail` (IN `WorkOrder` MEDIUMINT UNSIGNED)  BEGIN
DECLARE		NextOpID		int unsigned;

	SET NextOpID = (SELECT MIN(pkOperationID) 
					FROM Operation 
					WHERE fkWorkOrderID = WorkOrder
					AND fkOperationTypeID = 2
					AND EndQty IS NULL);

SELECT	DISTINCT						PartNumber, PT.IssueNumber AS Issue, D.Reference AS DrawingNumber, 
					D.IssueNumber AS DrgIssue, M.pkMachineID AS MachineID, (SELECT CONCAT(M3.Manufacturer, ' ', M3.Model)
																		FROM OperationMachine OM3
																		INNER JOIN Machine M3
																		ON OM3.fkMachineID = M3.pkMachineID
																		WHERE OM3.fkOperationID = NextOpID) AS Machine, 
					A.fkGRBID AS GRBID, 
					LI.fkOrderID AS OrderID, WO.pkWorkOrderID AS WorkOrderID, WO.QuantityReqd AS QtyOff, WO.FastTrack
					 

FROM			WorkOrder WO
INNER JOIN		LineItem LI
ON				WO.fkLineItemID = LI.pkLineItemID
INNER JOIN		Operation O
ON				WO.pkWorkOrderID = O.fkWorkOrderID
LEFT JOIN		Allocated A
ON				O.pkOperationID = A.fkOperationID
LEFT JOIN		PartTemplate PT
ON				LI.fkPartTemplateID = PT.pkPartTemplateID
INNER JOIN		Drawing D
ON				PT.fkDrawingID = D.pkDrawingID
LEFT JOIN		OperationMachine OMA
ON				O.pkOperationID = OMA.fkOperationID
INNER JOIN		Machine M
ON				OMA.fkMachineID = M.pkMachineID
LEFT JOIN		OperationMaterial OPM
ON				O.pkOperationID = OPM.pkOperationID

WHERE			WO.pkWorkOrderID = WorkOrder;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWODocuments` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		DDT.Droptype AS description, WOD.Print, FileName

FROM		WODocument WOD
INNER JOIN	DocumentDropType DDT
ON			WOD.fkDocumentDropTypeID = DDT.pkDocumentDropTypeID

WHERE		WOD.fkWorkOrderID = WorkOrderID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOFONotes` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
SELECT			CON.Note

FROM			CustomerOrderNote CON
INNER JOIN		CustomerOrder CO
ON				CON.fkOrderID = CO.pkOrderID
INNER JOIN		LineItem LI
ON				CO.pkOrderID = LI.fkOrderID
INNER JOIN		WorkOrder WO
ON				LI.pkLineItemID = WO.fkLineItemID

WHERE			WO.pkWorkOrderID = WorkOrderID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOMachineOpPrograms` (IN `OperationID` INT UNSIGNED, `MachineID` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		fkProgramNumber	AS ProgramNumber	

FROM		OpMachineProgram

WHERE		fkOperationID = OperationID
AND			fkMachineID = MachineID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOMaterialDetail` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		MS.MaterialSpec, OM.Size, OM.UnitID, OM.fkShapeID, OM.PartLength, fkApprovalID AS ApprovalID, SPU AS SPUNo, 
			SPUfkUOMID AS SPUID, (WO.QuantityReqd/OM.Yield) AS ReserveQty, IFNULL(MS.MaterialSpec, OM.Description) AS Description, 
			OM.IssueInstruction, CONCAT(M.Manufacturer, M.Model) AS Machine

FROM 		Kythera.OperationMaterial OM
INNER JOIN	Operation O
	ON		OM.pkOperationID = O.pkOperationID
INNER JOIN	MaterialSpec MS
	ON		OM.fkMaterialSpecID = MS.pkMaterialSpecID
INNER JOIN	Shape S
	ON		OM.fkShapeID = S.pkShapeID
INNER JOIN	ApprovalLevel AL
	ON		OM.fkApprovalID = AL.pkApprovalLevelID
INNER JOIN	OperationMachine OMA
	ON		O.pkOperationID = OMA.fkOperationID
left JOIN	Machine M
	ON		OMA.fkMachineID = M.pkMachineID

WHERE 		O.fkWorkOrderID = WorkOrderID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOMaterialNotes` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		Note AS MaterialNote

FROM		OperationNote OPN
INNER JOIN	Operation O
ON			OPN.fkOperationID = O.pkOperationID

WHERE		fkNoteTypeID = 1
AND			O.fkWorkOrderID = WorkOrderID;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOOpsDetail` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		O.pkOperationID AS OperationID, OT.pkOperationTypeID AS OperationTypeID, 
			OperationNumber, IF(M.Description IS NULL, OT.OpType, M.Description) AS Operation, 
			IF(ISNULL(MS.Endtime), O.EstimatedSetTime, 
			(IF(DATEDIFF(MS.EndTime, MS.StartTime) =0, TIMEDIFF(MS.EndTime, MS.StartTime), 
					CONCAT(DATEDIFF(MS.EndTime, MS.StartTime), 'Days')))) AS ActualSetTime,
 
			IF(ISNULL(OM.RunEnd), O.EstimatedRunTime, 
			(IF(DATEDIFF(OM.RunEnd, OM.RunStart) =0, TIMEDIFF(OM.RunEnd, OM.RunStart),
					CONCAT(DATEDIFF(OM.RunEnd, OM.RunStart), 'Days')))) AS ActualRunTime,  
			CONCAT(LEFT(IFNULL(U.FirstName, U2.FirstName), 1), LEFT(IFNULL(U.LastName, U2.LastName), 1)) AS Operator, 
			IF(ISNULL(O.Bypass),0,1) AS Bypass, IF(ISNULL(O.Subcon), 0, 1) AS Subcon, IF(ISNULL(RunEnd), 0, 1) AS Complete
								

FROM		Operation O
INNER JOIN	OperationType OT
	ON		O.fkOperationTypeID = OT.pkOperationTypeID
LEFT JOIN	OperationMachine OM
	ON		O.pkOperationID = OM.fkOperationID
LEFT JOIN	MachiningType M
	ON		O.fkMachiningTypeID = M.pkMachiningTypeID
LEFT JOIN	User U
	ON		OM.fkUserIDOperator = U.pkUserID

LEFT JOIN	MachineSet MS
	ON		O.pkOperationID = MS.fkOperationID
LEFT JOIN	User U2
	ON		MS.fkUserIDSetter = U2.pkUserID

WHERE		O.fkWorkOrderID = WorkOrderID
ORDER BY	O.OperationNumber;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOOpTmpltIntEngNotes` (IN `OperationID` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		Note

FROM		OperationNote

WHERE		fkOperationID = OperationID
AND			fkNoteTypeID = 4; 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOOpTmpltIntReviewNotes` (IN `OperationID` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		Note

FROM		OperationNote

WHERE		fkOperationID = OperationID
AND			fkNoteTypeID = 9;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOOpTmpltIntTreatNotes` (IN `OperationID` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		Note

FROM		OperationNote OPN

WHERE		OPN.fkOperationID = OperationID
AND			OPN.fkNoteTypeID = 6;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOOpTmpltReviewDetail` (IN `OperationID` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		ReviewStatus, RiskAssessment AS Risk, Tooling, QAEquipment AS QAEquip, CorrectApproval

FROM		WorkOrder

WHERE		pkWorkOrderID = (SELECT fkWorkOrderID 
								FROM Operation
								WHERE pkOperationID = OperationID);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOOpTmpltSubconDetail` (IN `OperationID` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT			OT.fkTreatmentID AS TreatmentID, T.Description AS Treatment, pkSupplierID AS SupplierID, S.SupplierName, 
				OT.ApprovalComment, OT.LeadTime

FROM			OperationTreatment OT
INNER JOIN		TreatmentList T
ON				OT.fkTreatmentID = T.pkTreatmentID
LEFT JOIN		SupplierTreatment ST
ON				OT.pkOperationID = ST.fkOperationID
LEFT JOIN		Supplier S
ON				ST.fkSupplierID = S.pkSupplierID

WHERE			OT.pkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOOutvoiceList` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		fkInvoiceID AS InvoiceNo

FROM		WorkOrder WO
INNER JOIN	WOOutvoice WOO	
ON			WO.pkWorkOrderID = WOO.fkWorkOrderID
INNER JOIN	Outvoice OV
ON			WOO.fkInvoiceID = OV.pkInvoiceID

WHERE		WO.pkWorkOrderID = WorkOrderID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOOutvoices` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		DISTINCT fkInvoiceID AS InvoiceNo, OV.CreateDate, 
									IFNULL(OV.Quantity,0) AS Qty, IFNULL(WO.PriceEach,0) AS EachPrice, 
			IFNULL(DeliveryChg,0) AS DeliveryChg, 
			IF(ISNULL(OV.AmendedValue), (OV.Quantity * WO.PriceEach) + IFNULL(DeliveryChg,0), OV.AmendedValue + IFNULL(DeliveryChg,0)) AS TotalPrice, 
			IRNoteNumber AS CertificateNo, WO.Complete, (SELECT Rate FROM VAT) AS VATRate

FROM		WorkOrder WO
INNER JOIN	WOOutvoice WOO	
ON			WO.pkWorkOrderID = WOO.fkWorkOrderID
INNER JOIN	Outvoice OV
ON			WOO.fkInvoiceID = OV.pkInvoiceID
LEFT JOIN	Operation OP
ON			WO.pkWorkOrderID = OP.fkWorkOrderID
INNER JOIN	Allocated A
ON			OP.pkOperationID = A.fkOperationID
INNER JOIN	GoodsIn GI
ON			A.fkGRBID = GI.pkGRBID

WHERE		WO.pkWorkOrderID = WorkOrderID
AND			(OV.Complete <> 1 OR OV.Complete IS NULL)

GROUP BY	InvoiceNo;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWORaisedOutvoices` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN

DECLARE			OpEndQty, CursQuantity 				smallint unsigned;
DECLARE			QtyStore							mediumint unsigned DEFAULT 0;
DECLARE			CursInvoiceID 						int unsigned;
DECLARE			CursComplete						tinyint unsigned;
DECLARE 			done 								INT DEFAULT FALSE;

DECLARE WOCursor CURSOR FOR 
		SELECT		pkInvoiceID, Quantity, Complete
		FROM		Outvoice OV
		INNER JOIN	WOOutvoice WOV
		ON			OV.pkInvoiceID = WOV.fkInvoiceID
		
		WHERE		fkWorkOrderID = WorkOrderID;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;


SET OpEndQty = (SELECT MIN(EndQty) 

		FROM 	Operation
		
		WHERE	fkWorkOrderID = WorkOrderID);


  OPEN WOCursor;

read_loop: LOOP
		FETCH WOCursor INTO CursInvoiceID, CursQuantity, CursComplete;

		IF done THEN
			LEAVE read_loop;

		END IF;

SET		QtyStore = QtyStore + CursQuantity;

END Loop;

Close WOCursor;


SELECT		OpEndQty - QtyStore AS QtyLeft, fkInvoiceID AS InvoiceNo, OV.Quantity AS Qty, OV.CreateDate AS Created

FROM		WorkOrder WO
LEFT JOIN	WOOutvoice WOO	
ON			WO.pkWorkOrderID = WOO.fkWorkOrderID
LEFT JOIN	Outvoice OV
ON			WOO.fkInvoiceID = OV.pkInvoiceID

WHERE		WO.pkWorkOrderID = WorkOrderID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWorkOrder` (IN `WorkOrder` INT)  BEGIN
SELECT			OrigDeliveryDate, pkInvoiceID, WO.QuantityReqd AS Qty, WO.QuantityOvers AS Overs,
				WO.QuantityReqd + WO.QuantityOvers AS TotalQty, PriceEach, DeliveryCharge,
				OT.SelectedPrice AS TreatmentPrice, I.Price, WO.Complete

FROM			WorkOrder WO
INNER JOIN		WOOutvoice WOO
ON				WOO.fkWorkOrderID = WO.pkWorkOrderID
INNER JOIN		Outvoice OU
ON				WOO.fkInvoiceID = OU.pkInvoiceID
INNER JOIN		LineItem LI
ON				WO.fkLineItemID = LI.pkLineItemID
INNER JOIN		CustomerOrder CO
ON				LI.fkOrderID = CO.pkOrderID
INNER JOIN		Customer C
ON				CO.fkCustomerID = C.pkCustomerID
INNER JOIN		Operation O
ON				WO.pkWorkOrderID = O.fkWorkOrderID
LEFT JOIN		OperationTreatment OT
ON				O.pkOperationID = OT.pkOperationID
LEFT JOIN		OperationMaterial OM
ON				O.pkOperationID = OM.pkOperationID
INNER JOIN		OperationPO OP
ON				O.pkOperationID = OP.fkOperationID
INNER JOIN		RFQOrder RO
ON				OP.fkRFQOID = RO.pkRFQOID
INNER JOIN		Instruction I
ON				RO.pkRFQOID = I.fkRFQPOID

WHERE			WO.pkWorkOrderID = WorkOrder;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWorkOrderCoCInfo` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		DISTINCT pkGRBID AS GRB, GI.IRNoteNumber, A.Display, A.Print, PaperworkComplete
FROM		WorkOrder 	WO
INNER JOIN	Operation 	O
ON			WO.pkWorkOrderID = O.fkWorkOrderID
INNER JOIN	Allocated A
ON			O.pkOperationID = A.fkOperationID
INNER JOIN	GoodsIn GI
ON			A.fkGRBID = GI.pkGRBID

WHERE		WO.pkWorkOrderID = WorkOrderID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWorkOrderDetail` (IN `WorkOrder` MEDIUMINT UNSIGNED)  BEGIN
SELECT			pkPartTemplateID, PartNumber, PT.Description, pkOrderID AS OrderID, CustomerOrderRef, 
				WO.QuantityReqd AS Quantity, OrigDeliveryDate AS DueDate, NewDeliveryDate AS EstDate, 
				CONCAT(Manufacturer, " ", Model) AS WhereMachine, IssueNumber AS Issue, MIN(OperationNumber) AS dummy

FROM			WorkOrder WO
INNER JOIN		LineItem LI
ON				WO.fkLineItemID = LI.pkLineItemID
INNER JOIN		CustomerOrder CO
ON				LI.fkOrderID = CO.pkOrderID
INNER JOIN		Operation O
ON				WO.pkWorkOrderID = O.fkWorkOrderID
LEFT JOIN		OperationMachine OMA
ON				O.pkOperationID = OMA.fkOperationID
INNER JOIN		Machine MA
ON				OMA.fkMachineID = MA.pkMachineID
LEFT JOIN		PartTemplate PT
ON				LI.fkPartTemplateID = PT.pkPartTemplateID

WHERE			WO.pkWorkOrderID = WorkOrder 
;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWorkOrderGRBs` (IN `WONo` INT UNSIGNED)  BEGIN

SELECT 		PG.fkGRBID AS GRB, PON.pkPOnID AS PO, RO.pkRFQOID AS RFQ, I.pkInvoiceID AS Invoice,
			count(O.fkWorkOrderID) AS OpNumber, OpType, S.SupplierName AS Supplier
FROM 		WorkOrder WO
INNER JOIN	Operation O
ON			WO.pkWorkOrderID = O.fkworkOrderID
INNER JOIN	OperationType OT
ON			O.fkOperationTypeID = OT.pkOperationTypeID
LEFT JOIN		OperationOrderItem OOI
ON				O.pkOperationID = OOI.fkOperationID
INNER JOIN		OrderItem OI
ON				OOI.fkItemID = OI.pkItemID
INNER JOIN	RFQOrder RO
ON			OI.fkRFQOID = RO.pkRFQOID
LEFT JOIN	Pon PON
ON			RO.pkRFQOID = PON.fkRFQOID
LEFT JOIN	POGoods PG
ON			OI.pkItemID = PG.fkItemID
LEFT JOIN	POInvoice POI
ON			OI.pkItemID = POI.fkItemID
LEFT JOIN	Invoice I
ON			POI.fkInvoiceID = I.pkInvoiceID
LEFT JOIN	RFQOSupplier RS
ON			RO.pkRFQOID = RS.fkRFQOID
LEFT JOIN	Supplier S
ON			RS.fkSupplierID = S.pkSupplierID

WHERE		WO.pkWorkOrderID = WONo;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWorkOrderHistory` (IN `CONo` MEDIUMINT UNSIGNED)  BEGIN
SELECT		DISTINCT pkWorkOrderID AS WONo, PartNumber AS PartNo, QuantityReqd AS Qty, 99 AS Days

FROM		LineItem 			LI	
INNER JOIN	WorkOrder 			WO	ON	LI.pkLineItemID = WO.fkLineItemID
LEFT JOIN	PartTemplate 		PT	ON	LI.fkPartTemplateID = PT.pkPartTemplateID
LEFT JOIN	CustomerOrderNote 	CON	ON	LI.fkOrderID = CON.fkOrderID


WHERE		LI.fkOrderID = CONo;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWorkOrderLabour` (IN `WoNo` INT UNSIGNED)  BEGIN

DECLARE		RunDays			tinyint unsigned;

SELECT			OMA.ExpectedPrice AS MaterialCost, OTR.SelectedPrice AS TreatmentCost, OpType AS Operation,

								(5 * (DATEDIFF(MS.EndTime, MS.StartTime) DIV 7) + 
				MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(MS.StartTime) + WEEKDAY(MS.EndTime) + 1, 1)-1) *9
				+
								CAST((DATE_ADD(TIMEDIFF(MS.EndTime, DATE_ADD(DATE(MS.EndTime), INTERVAL '08:00:00' HOUR)), INTERVAL
				TIMEDIFF(DATE_ADD(DATE(MS.StartTime), INTERVAL '17:00:00' HOUR), MS.StartTime) HOUR_SECOND) /10000) AS DECIMAL(4,2)) AS SetTime,


								(5 * (DATEDIFF(OM.RunEnd, OM.RunStart) DIV 7) + 
				MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(OM.RunStart) + WEEKDAY(OM.RunEnd) + 1, 1)) *9
				+
								CAST((DATE_ADD(TIMEDIFF(OM.RunEnd, DATE_ADD(DATE(OM.RunEnd), INTERVAL '08:00:00' HOUR)), INTERVAL
				TIMEDIFF(DATE_ADD(DATE(OM.RunStart), INTERVAL '17:00:00' HOUR), OM.RunStart) HOUR_SECOND) /10000) AS DECIMAL(4,2)) AS RunTime,


								(5 * (DATEDIFF(OM.InspectEnd, OM.InspectStart) DIV 7) + 
				MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(OM.InspectStart) + WEEKDAY(OM.InspectEnd) + 1, 1)-1) *9
				+
								CAST((DATE_ADD(TIMEDIFF(OM.InspectEnd, DATE_ADD(DATE(OM.InspectEnd), INTERVAL '08:00:00' HOUR)), INTERVAL
				TIMEDIFF(DATE_ADD(DATE(OM.InspectStart), INTERVAL '17:00:00' HOUR), OM.InspectStart) HOUR_SECOND) /10000) AS DECIMAL(4,2)) 
				+
								(5 * (DATEDIFF(MS.InspectEnd, MS.InspectStart) DIV 7) + 
				MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(MS.InspectStart) + WEEKDAY(MS.InspectEnd) + 1, 1)-1) *9
				+
								CAST((DATE_ADD(TIMEDIFF(MS.InspectEnd, DATE_ADD(DATE(MS.InspectEnd), INTERVAL '08:00:00' HOUR)), INTERVAL
				TIMEDIFF(DATE_ADD(DATE(MS.InspectStart), INTERVAL '17:00:00' HOUR), MS.InspectStart) HOUR_SECOND) /10000) AS DECIMAL(4,2)) AS TimeTaken,


				(IF(OMA.ExpectedPrice IS NULL, 0, OMA.ExpectedPrice) + IF(OTR.SelectedPrice IS NULL, 0, OTR.SelectedPrice)) AS Cost,

				OP.EndQty AS Qty, WO.QuantityReqd
				
FROM			WorkOrder 			WO
INNER JOIN		Operation 			OP
ON				WO.pkWorkOrderID = 		OP.fkWorkOrderID
INNER JOIN		OperationType		OT
ON				OP.fkOperationTypeID = 	OT.pkOperationTypeID
INNER JOIN		OperationMachine 	OM
ON				OP.pkOperationID = 		OM.fkOperationID
INNER JOIN		MachineSet 			MS
ON				OM.fkOperationID = 		MS.fkOperationID
AND				OM.fkMachineID	 = 		MS.fkMachineID
LEFT JOIN		Pause 				P
ON				OM.fkOperationID = 		P.fkOperationID
AND				OM.fkMachineID	 = 		P.fkMachineID

LEFT JOIN		OperationMaterial 	OMA
ON				OP.pkOperationID = 		OMA.pkOperationID
LEFT JOIN		OperationTreatment 	OTR
ON				OP.pkOperationID = 		OTR.pkOperationID

WHERE			OP.fkWorkOrderID = WoNo

GROUP BY		OM.fkMachineID, OM.fkOperationID

ORDER BY		OP.OperationNumber;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWorkOrderNotes` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		Note
FROM		WorkOrderNote

WHERE		fkWorkOrderID = WorkOrderID;	
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWorkOrderPOs` (IN `WoNo` INT UNSIGNED)  BEGIN
SELECT		PON.pkPOnID AS PONumber, RO.CreateDate, RO.Cancelled, OI.Description, OI.Quantity, OI.Price,
			OI.Comment,
			IF((SELECT		COUNT(*) 
			FROM 		Allocated
			WHERE		fkOperationID = OP.pkOperationID) < 1,
			(IF((SELECT		COUNT(*)
			FROM 		POGoods
			WHERE		POGoods.fkItemID = OI.pkItemID) < 1, 'Ordered', 'In stock')),
			'Issued') AS 'Status'
			
FROM		RFQOrder RO
LEFT JOIN	PON
ON			RO.pkRFQOID = PON.fkRFQOID
LEFT JOIN	OrderItem OI
ON			RO.pkRFQOID = OI.fkRFQOID
LEFT JOIN	OperationOrderItem 		OOI
ON			OI.pkItemID = 			OOI.fkItemID
LEFT JOIN	Operation 			OP
ON			OOI.fkOperationID = 	OP.pkOperationID
LEFT JOIN	WorkOrder 			WO
ON			OP.fkWorkOrderID = 		WO.pkWorkOrderID

WHERE		WO.pkWorkOrderID = WoNo
;	
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOTemplateDetail` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		LI.fkPartTemplateID AS PartTemplateID, PartNumber, IssueNumber, EngineeringIssue AS EngIssue, 
			PlanningComplete



FROM		LineItem LI
INNER JOIN	PartTemplate PT
ON			LI.fkPartTemplateID = PT.pkPartTemplateID
LEFT JOIN	EngineeringTemplate ET
ON			PT.pkPartTemplateID = ET.fkPartTemplateID
INNER JOIN	WorkOrder WO
ON			WO.fkLineItemID = LI.pkLineItemID 
INNER JOIN	Operation O
ON			WO.pkWorkOrderID = O.fkWorkOrderID 

WHERE		pkWorkOrderID = WorkOrderID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOTemplateEngineering` (IN `OperationID` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		DISTINCT OMP.fkMachineID AS MachineID, RiskStatement, EngComplete

FROM		Operation O
INNER JOIN	OpMachineProgram OMP
ON			O.pkOperationID = OMP.fkOperationID

WHERE		pkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOTemplateMaterial` (IN `OperationID` INT UNSIGNED)  BEGIN

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT			FreeIssue, IF(OM.Description IS NOT NULL, '', MS.MaterialSpec) AS Spec, 
							IF(OM.Description IS NOT NULL, OM.Description, MS.Description) AS Description, 
				Size, UnitID, fkShapeID AS ShapeID, PartLength, fkApprovalID AS ApprovalID, SPUfkUOMID AS SPUID, 
				Yield, TestPieces, OM.StdDeliveryDays, IssueInstruction, StdfkSupplierID AS StdSupplierID, S.Email AS StdSupplierEmail

FROM			OperationMaterial OM
INNER JOIN		MaterialSpec MS
ON				OM.fkMaterialSpecID = MS.pkMaterialSpecID
LEFT JOIN		Supplier S
ON				OM.StdfkSupplierID = S.pkSupplierID
	
WHERE			pkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOTemplateProcesses` (IN `OperationID` INT UNSIGNED)  BEGIN
SELECT		ProcessNote

FROM		OperationProcess

WHERE		fkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOTmpltApprovalComments` (IN `OperationID` INT UNSIGNED)  BEGIN
SELECT		pkApprovalCommentID AS CommentID, Comment

FROM		OperationApprovalComments

WHERE		fkOperationID = OperationID
;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOTmpltConformityNotes` (IN `OperationID` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

SELECT		pkOperationNoteID AS NoteID, Note

FROM		OperationNote

WHERE		fkOperationID = OperationID
AND			fkNoteTypeID = 2
; 
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetWOTmpltPOComments` (IN `OperationID` INT UNSIGNED)  BEGIN

SELECT		pkPOCommentID AS CommentID, PC.Comment

FROM		POComment PC
INNER JOIN	OrderItem OI
ON			PC.fkRFQOID = OI.fkRFQOID
INNER JOIN	OperationOrderItem OOI
ON			OI.pkItemID = OOI.fkItemID

WHERE		OOI.fkOperationID = OperationID
;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetXStoreLocations` (IN `StoreAreaID` TINYINT UNSIGNED)  BEGIN
SELECT 	XCoord AS XLocation
FROM 		XCoordinate X

WHERE		X.fkStoreAreaID = StoreAreaID 
AND 		(X.Deleted <> 1 OR X.Deleted IS NULL);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcGetYStoreLocations` (IN `StoreAreaID` TINYINT UNSIGNED)  BEGIN
SELECT 	YCoord AS YLocation
FROM		YCoordinate Y
WHERE		(Y.Deleted <> 1 OR Y.Deleted IS NULL) 
AND			Y.fkStoreAreaID = StoreAreaID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcInvoiceRpt` (IN `InvoiceID` INT UNSIGNED)  BEGIN

SELECT			CustomerName, Address1, Address2, Address3, Address4, Postcode,
				pkInvoiceID AS InvoiceNo, 46714 AS CertificateNo, 0 AS ContractNo, 
				0 AS SubconOrderNo, CustomerReference AS OrderLineNo,
				IF(WO.Complete = 1, 'Yes', 'No') AS OrderComplete, pkWorkOrderID AS WONo, CustomerSageCode AS AccountNo, 
				OV.Quantity, PT.Description, D.Reference AS DrawingNo,
				CONCAT('Issue ', D.IssueNumber) AS DrgIssue, PartNumber, 
				CONCAT('Issue ', PT.IssueNumber) AS PartIssue, PriceEach, 
												IFNULL(OV.AmendedValue, (IFNULL(OV.Quantity, QuantityReqd))) * PriceEach AS Total, 
				(SELECT Rate FROM Rate WHERE pkRateID = 1) AS VATRate, 
				ROUND(IFNULL(OV.AmendedValue, (IFNULL(OV.Quantity, QuantityReqd)) * PriceEach) 
									* ((SELECT Rate FROM Rate WHERE pkRateID = 1)/100),2) AS VATTotal,
				IFNULL(DeliveryChg, 0) AS DeliveryChg, CurDate() AS InvoiceDate, 
				IFNULL((DeliveryChg/100) * (SELECT Rate FROM Rate WHERE pkRateID = 1),0) AS DeliveryVAT,
				(ROUND(IFNULL(OV.AmendedValue, (IFNULL(OV.Quantity, QuantityReqd)) * PriceEach)
									* ((SELECT Rate FROM Rate WHERE pkRateID = 1)/100),2)
									+ IFNULL(((DeliveryChg/100) * (SELECT Rate FROM Rate WHERE pkRateID = 1)),0)) AS VATGrandTotal,
				(IFNULL(OV.AmendedValue, (IFNULL(OV.Quantity, QuantityReqd))) * PriceEach) + IFNULL(DeliveryChg, 0) AS GrandTotal,
				ROUND(IFNULL(OV.AmendedValue, (IFNULL(OV.Quantity, QuantityReqd)) * PriceEach) 
									* ((SELECT Rate FROM Rate WHERE pkRateID = 1)/100),2)
									+ IFNULL(((DeliveryChg/100) * (SELECT Rate FROM Rate WHERE pkRateID = 1)), 0) +
									IFNULL(OV.AmendedValue, (IFNULL(OV.Quantity, QuantityReqd)) * PriceEach) + IFNULL(DeliveryChg, 0) AS SuperTotal

FROM			Outvoice OV
INNER JOIN		WOOutvoice WOV
ON				OV.pkInvoiceID = WOV.fkInvoiceID
INNER JOIN		WorkOrder WO
ON				WOV.fkWorkOrderID = WO.pkWorkOrderID 
LEFT JOIN		LineItem LI
ON				WO.fkLineItemID = LI.pkLineItemID
LEFT JOIN		PartTemplate PT
ON				LI.fkParttemplateID = PT.pkPartTemplateID
LEFT JOIN		Drawing D
ON				PT.fkDrawingID = D.pkDrawingID

INNER JOIN		CustomerOrder CO
ON				LI.fkOrderID = CO.pkOrderID
INNER JOIN		Customer C
ON				CO.fkCustomerID = C.pkCustomerID			

WHERE			pkInvoiceID = InvoiceID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcInvoiceSent` (IN `UserID` SMALLINT UNSIGNED, IN `InvoiceID` INT UNSIGNED)  NO SQL
UPDATE		outvoice
SET 		SentDate = now(), SentByfkUserID = UserID

WHERE		pkInvoiceID = InvoiceID$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcIssueMaterialForWO` (IN `UserID` SMALLINT UNSIGNED, `PartNumber` VARCHAR(25), `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
INSERT INTO		Todo
				(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, PartNumber, Complete, 
				CompletedByfkUserID, fkGRBID)
VALUES			(date(now()), 2, UserID, 2, WorkOrderID, PartNumber, 0, NULL, 

					(SELECT MIN(fkGRBID) 
					FROM Allocated A1
					INNER JOIN Operation O1
					ON A1.fkOperationID = O1.pkOperationID 

					INNER JOIN OperationMachine OM1
					ON O1.pkOperationID = OM1.fkOperationID
					WHERE O1.fkWorkOrderID = WorkOrderID

					AND ISNULL(OM1.RunEnd)));

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcJILRpt` (IN `GRBID` INT UNSIGNED)  BEGIN
SELECT		Alias AS CustomerName, pkWorkOrderID AS WONo, WO.QuantityReqd + WO.QuantityOvers AS Qty, 
			MS.MaterialSpec, pkGRBID AS GRB, S.Description AS Shape, 
            CONCAT(M.Size, IF(M.UnitID = 1, ' mm', ' inches')) AS Size, D.Reference AS DrawingNo, D.IssueNumber

FROM		GoodsIn			GI
INNER JOIN 	Allocated 		A
ON			GI.pkGRBID = A.fkGRBID 
INNER JOIN	Operation		O
ON			A.fkOperationID = O.pkOperationID
INNER JOIN	WorkOrder		WO
ON			O.fkWorkOrderID = WO.pkWorkOrderID
INNER JOIN	LineItem		LI
ON			WO.fkLineItemID = LI.pkLineItemID
INNER JOIN	CustomerOrder	CO
ON			LI.fkOrderID = CO.pkOrderID
INNER JOIN	Customer		C
ON			CO.fkCustomerID = C.pkCustomerID

INNER JOIN	Material M
ON			GI.pkGRBID = M.fkGRBID
INNER JOIN	MaterialSpec MS
ON			M.fkMaterialSpecID = MS.pkMaterialSpecID
left join Shape S 
ON			M.fkShapeID = S.pkShapeID
 				
LEFT JOIN	PartTemplate 	PT	
ON			LI.fkPartTemplateID = PT.pkPartTemplateID
LEFT JOIN	Drawing D
ON			PT.fkDrawingID = D.pkDrawingID

WHERE		pkGRBID = GRBID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcLaunchWO` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN

UPDATE			WorkOrder
SET				Live = 1

WHERE			pkWorkOrderID = WorkOrderID;

INSERT INTO	Todo
				(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID,
				PartNumber, Complete, Description)

SELECT			DATE(Now()), 15, UserID, 4, WorkOrderID, PT.PartNumber, 0, CONCAT(LEFT(OI.Description,26), 
										IF(fkOperationTypeID = 1, ' MAT',
												IF(fkOperationTypeID = 3, ' SC', ' OTH')))

FROM			OrderItem OI
INNER JOIN		OperationOrderItem OOI
ON				OI.pkItemID = OOI.fkItemID
INNER JOIN		Operation O
ON				OOi.fkOperationID = O.pkOperationID
LEFT JOIN		PartTemplate PT
ON				OI.fkPartTemplateID = PT.pkPartTemplateID

WHERE			O.fkWorkOrderId = WorkOrderID;				

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcListOps` (IN `WorkOrderID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		O.pkOperationID, M.Description AS Machining, O.OperationNumber AS OpNo, 
				OT.pkOperationTypeID AS OpTypeID, OT.OpType, O.EstimatedSetTime, O.EstimatedRunTime
FROM		Operation O
INNER JOIN	OperationType OT
	ON		O.fkOperationTypeID = OT.pkOperationTypeID
LEFT JOIN	OperationMachine OM
	ON		O.pkOperationID = OM.fkOperationID
LEFT JOIN	MachiningType M
	ON		O.fkMachiningTypeID = M.pkMachiningTypeID

WHERE		O.fkWorkOrderID = WorkOrderID
ORDER BY	OpNo;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcListTemplateOps` (IN `EngineeringTemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

SELECT 		OTM.pkOperationTemplateID AS OpTemplateID, OTM.OperationNumber AS OpNo, OT.pkOperationTypeID AS OpTypeID, 
				IF(OTM.fkOperationTypeID = 2, MT.Description, 
					IF(OTM.fkOperationTypeID = 3, TL.Description, OT.OpType)) AS Operation, Linked, 
				OTM.EstimatedSetTime, OTM.EstimatedRunTime

FROM			OperationTemplate OTM
left JOIN		OperationType OT
ON				OTM.fkOperationTypeID = OT.pkOperationTypeID
LEFT JOIN		MachiningType MT
ON				OTM.fkMachiningID = MT.pkMachiningTypeID
LEFT JOIN		TemplateTreatment TT
ON				OTM.pkOperationTemplateID = TT.pkOperationTemplateID
left JOIN		TreatmentList TL
ON				TT.fkTreatmentID = TL.pkTreatmentID

WHERE			OTM.fkEngineeringTemplateID = EngineeringTemplateID
ORDER BY		OperationNumber;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcListWOTemplateOps` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

SELECT 			O.pkOperationID AS OpTemplateID, O.OperationNumber AS OpNo, O.fkOperationTypeID AS OpTypeID, 
					IF(O.fkOperationTypeID = 2, MT.Description, 
						IF(O.fkOperationTypeID = 3, TL.Description, "Material Issue")) AS Operation, Linked, 
					EstimatedSetTime, EstimatedRunTime

FROM			Operation O	
LEFT JOIN		OperationTreatment OT
ON				O.pkOperationID = OT.pkOperationID
LEFT JOIN		TreatmentList TL
ON				OT.fkTreatmentID = TL.pkTreatmentID
LEFT JOIN		MachiningType MT
ON				O.fkMachiningTypeID = MT.pkMachiningTypeID

WHERE			O.fkWorkOrderID = WorkOrderID
ORDER BY		OperationNumber;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcMachineList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		pkMachineID AS MachineID, CONCAT(M.Manufacturer, ' ', M.Model) AS Machine
FROM 		Machine M

WHERE		InUseYN <> 2 OR InUseYN IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcMachiningList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkMachiningTypeID AS MachiningListID, Description
 
FROM 		MachiningType 

WHERE 		Deleted <> 1 OR Deleted IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcMaterialInStock` ()  BEGIN


DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 					M.fkGRBID AS GRBNo, MS.MaterialSpec AS Spec, M.Size, 
			IF(M.UnitID = 1,'mm', 
				IF (M.UnitID = 2, 'inches', 'ns')) AS Units, SH.Description AS Shape, AL.Description AS ReleaseLevel, 
						UOM.Description AS PurchaseLength, GI.ReceivedDate AS DateIn, M.RcvdQuantity AS OrderQty, 
			M.RcvdQuantity - (SELECT SUM(Quantity) 
								FROM Allocated 
								WHERE fkGRBID = M.fkGRBID) AS InStock, 
			CONCAT(StoreArea, ' ', XCoord, YCoord) AS Location

FROM 		Material M
INNER JOIN	GoodsIn GI
ON			M.fkGRBID = GI.pkGRBID 
LEFT JOIN	MaterialLocation ML 
ON			M.fkGRBID = ML.fkMaterialID
LEFT JOIN	StoreArea SA
ON			ML.fkStoreAreaID = SA.pkStoreAreaID
LEFT JOIN	XCoordinate X
ON			ML.fkXCoordID = X.pkXCoordID

LEFT JOIN	YCoordinate Y
ON			ML.fkYCoordID = Y.pkYCoordID

LEFT JOIN	MaterialSpec MS
ON			M.fkMaterialSpecID = MS.pkMaterialSpecID
LEFT JOIN	Shape SH
ON			M.fkShapeID = SH.pkShapeID
LEFT JOIN	ApprovalLevel AL
ON			M.fkApprovalLevelID = AL.pkApprovalLevelID

LEFT JOIN	POGoods POG
ON			GI.pkGRBID = POG.fkGRBID
LEFT JOIN	OrderItem OI
ON			POG.fkITemID = OI.pkItemID
LEFT JOIN	UnitOfMeasure UOM
ON			OI.SPU = UOM.pkUOMID

WHERE 		M.RcvdQuantity > 0
ORDER BY	M.RcvdDate DESC;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcMaterialLists` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 	pkApprovalLevelID AS ApprovalID, Description 
FROM 	ApprovalLevel
WHERE	Deleted <>1 OR Deleted IS NULL;

SELECT 	pkUOMID AS UOMID, Description 
FROM 	UnitOfMeasure
WHERE	Deleted <>1 OR Deleted IS NULL;

SELECT 	pkShapeID AS ShapeID, Description 
FROM 	Shape
WHERE	Deleted <>1 OR Deleted IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcMaterialsInStock` (IN `SpecString` CHAR(8), IN `ApprovalString` CHAR(20))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		MS.MaterialSpec, S.SupplierName, M.Description, M.Size, SH.Description AS Shape, AL.Description AS Approval, M.Quantity, 
			M.Length, (M.PriceEach * M.Quantity) AS Price, (M.PriceEach / M.Length) AS PerMetre, M.PriceEach

FROM 		Material M
INNER JOIN	GoodsIn GI
ON			M.fkGRBID = GI.pkGRBID
LEFT JOIN	POGoods PG 
ON 			M.fkGRBID = PG.fkGRBID
LEFT JOIN	OrderItem OI 
ON 			PG.fkItemID = OI.pkItemID
LEFT JOIN	RFQOSupplier RS 
ON 			OI.fkRFQOID = RS.fkRFQOID
LEFT JOIN	Supplier S 
ON 			RS.fkSupplierID = S.pkSupplierID
LEFT JOIN 	MaterialSpec MS 
ON 			M.fkMaterialSpecID = MS.pkMaterialSpecID
LEFT JOIN	Shape SH 
ON 			M.fkShapeID = SH.pkShapeID
LEFT JOIN	ApprovalLevel AL 
ON 			M.fkApprovalLevelID = AL.pkApprovalLevelID

WHERE 		MS.MaterialSpec LIKE CONCAT(SpecString, "%") 
AND 		AL.Description  LIKE CONCAT(ApprovalString, "%") 
AND 		M.Quantity > 0

ORDER BY	GI.ReceivedDate DESC;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcMaterialSpecInStock` (IN `MaterialSpecID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT * FROM Material WHERE fkMaterialSpecID = MaterialSpecID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcMaterialSpecList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT MaterialSpec FROM MaterialSpec;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcMaterialSpecQPO` (IN `SpecString` CHAR(8), IN `ApprovalString` CHAR(20))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		MS.MaterialSpec, S.SupplierName, MS.Description, OM.Size, SH.Description AS Shape, AL.Description AS Approval,  
			MSU.Quantity, '-' AS Length, MSU.MOQ, OM.ExpectedPrice, MSU.MetrePrice, MSU.EachPrice 

FROM 		OperationMaterial OM
LEFT JOIN	MaterialSpec MS	
				ON OM.fkMaterialSpecID = MS.pkMaterialSpecID
INNER JOIN	MaterialSupplier MSU 
				ON	OM.pkOperationID = MSU.fkOperationID
LEFT JOIN	Supplier S 
				ON MSU.fkSupplierID = S.pkSupplierID 
LEFT JOIN	Shape SH
				ON OM.fkShapeID = SH.pkShapeID
LEFT JOIN	ApprovalLevel AL
				ON OM.fkApprovalID = AL.pkApprovalLevelID			

WHERE 		MS.MaterialSpec LIKE SpecString AND AL.Description = ApprovalString;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcNewSupplier` (IN `SupplierName` VARCHAR(50), IN `Alias` VARCHAR(40), IN `Address1` VARCHAR(45), IN `Address2` VARCHAR(40), IN `Address3` VARCHAR(40), IN `Address4` VARCHAR(30), IN `Postcode` VARCHAR(8), IN `Phone` VARCHAR(20), IN `Facsimile` VARCHAR(20), IN `Website` VARCHAR(50), IN `Email` VARCHAR(50), IN `MinOrderCharge` DECIMAL(10,0), IN `PaymentTerms` TINYINT, IN `MaterialSupplier` TINYINT, IN `TreatmentSupplier` TINYINT, IN `CalibrationSupplier` TINYINT, IN `ToolingSupplier` TINYINT, IN `MiscSupplier` TINYINT)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER 
			FOR SQLWARNING 
				BEGIN 
					SELECT ErrDetail = 'warning'; 
				END;
DECLARE EXIT HANDLER 
			FOR SQLEXCEPTION 
				BEGIN 
					SELECT ErrDetail = 'exception'; 
				END;
INSERT INTO	Supplier
			(SupplierName, Alias, Address1, Address2, Address3, Address4, Postcode, Phone, Facsimile, Website, Email,
			MinOrderCharge, PaymentTerms, MaterialSupplier, TreatmentSupplier, CalibrationSupplier, ToolingSupplier, 
			MiscSupplier, Deleted)
VALUES		(SupplierName, Alias, Address1, Address2, Address3, Address4, Postcode, Phone, Facsimile,
			Website, Email, MinOrderCharge, PaymentTerms, MaterialSupplier, TreatmentSupplier,
			CalibrationSupplier, ToolingSupplier, MiscSupplier, 0);


SELECT			Row_Count() INTO Result;

IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		last_Insert_ID();
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPackingSlip` (IN `GRBNo` INT UNSIGNED)  BEGIN

IF		(SELECT Complete 
	FROM WorkOrder WO2
	INNER JOIN GoodsIn GI2
	ON WO2.pkWorkOrderID = GI2.WorkOrderID
	WHERE GI2.pkGRBID = GRBNo) = 1

THEN 
		SELECT	C.Alias AS DeliverTo, GI.WorkOrderID, PT.PartNumber, M.RcvdQuantity, PT.Description, D.DispatchDate, 
				D.SpecialInstruction, IF(D.CarriageTypeID = 1, 'Next Day',
											IF(D.CarriageTypeID = 2, '9am',
												IF(D.CarriageTypeID = 3, '12 noon',
													IF(D.CarriageTypeID = 4, '5.30pm', 'Error')))) AS CarriageType, CR.Name AS Courier

	FROM		Dispatch		D
	LEFT JOIN	GoodsIn			GI
	ON			D.fkGRBID = GI.pkGRBID
	LEFT JOIN 	Allocated 		A
	ON			D.fkGRBID = A.fkGRBID 
	LEFT JOIN	Material M
	ON			GI.pkGRBID = M.fkGRBID
	LEFT JOIN	WorkOrder		WO
	ON			GI.WorkOrderID = WO.pkWorkOrderID
	LEFT JOIN	LineItem		LI
	ON			WO.fkLineItemID = LI.pkLineItemID
	LEFT JOIN	CustomerOrder	CO
	ON			LI.fkOrderID = CO.pkOrderID
	LEFT JOIN	Customer		C
	ON			CO.fkCustomerID = C.pkCustomerID
	LEFT JOIN	PartTemplate 	PT	
	ON			LI.fkPartTemplateID = PT.pkPartTemplateID

	LEFT JOIN	Courier CR
	ON			D.fkCourierID = CR.pkCourierID

	WHERE		D.fkGRBID = GRBNo;

ELSE 
	SELECT		S.Alias AS DeliverTo, O.fkWorkOrderID, PT.PartNumber, Quantity, PT.Description, D.DispatchDate, 
				D.SpecialInstruction, IF(D.CarriageTypeID = 1, 'Next Day',
											IF(D.CarriageTypeID = 2, '9am',
												IF(D.CarriageTypeID = 3, '12 noon',
													IF(D.CarriageTypeID = 4, '5.30pm', 'Not set')))) AS CarriageType, CR.Name AS Courier, 'treat'

	FROM		Pon P
	left JOIN	OrderItem OI
	ON			P.fkRFQOID = OI.fkRFQOID
	left JOIN	OperationOrderItem OOI
	ON			OI.pkItemID = OOI.fkItemID
	left JOIN	Operation O
	ON			OOI.fkOperationID = O.pkOperationID

	LEFT JOIN	PartTemplate PT
	ON			OI.fkPartTemplateID = PT.pkPartTemplateID
	left JOIN	RFQOSupplier RS
	ON			OI.fkRFQOID = RS.fkRFQOID
	left JOIN	Supplier S
	ON			RS.fkSupplierID = S.pkSupplierID
	left JOIN	POGoods	POG
	ON			OI.pkItemID = POG.fkItemID
	LEFT JOIN	Dispatch	D
	ON			POG.fkGRBID = D.fkGRBID
	LEFT JOIN	Courier CR
	ON			D.fkCourierID = CR.pkCourierID

	WHERE		RS.Selected = 1
	AND			POG.fkGRBID = GRBNo;

END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPartLinkToMaster` (IN `UserID` SMALLINT UNSIGNED, `Qty` TINYINT UNSIGNED, `PTemplateID` SMALLINT UNSIGNED, `MasterPartTemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		PartTemplate
SET			fkParentPartTemplateID = MasterPartTemplateID, QtyToParent = Qty

WHERE		pkPartTemplateID = PTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPartNumberList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkPartTemplateID AS PartTemplateID, PartNumber

FROM		PartTemplate;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPartNumberSearch` (IN `PartNumberSearchField` VARCHAR(25))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		PartNumber
FROM		PartTemplate

WHERE		PartNumber LIKE CONCAT(PartNumberSearchField, "%");
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPartSearchList` (IN `SearchField` VARCHAR(25))  BEGIN
SELECT		PT.PartNumber, PT.Issuenumber AS Issue, OI.Quantity AS Qty, OI.Description, pkPOnID AS PONo, RO.CreateDate AS Raised, 
			RO.pkRFQOID AS RFQNo, RO.Complete

FROM		PartTemplate PT
INNER JOIN	LineItem LI
ON			PT.pkPartTemplateID = LI.fkPartTemplateID
INNER JOIN	WorkOrder WO
ON			LI.pkLineItemID = WO.fkLineItemID
INNER JOIN	Operation O
ON			WO.pkWorkOrderID = O.fkWorkOrderID
INNER JOIN	OperationOrderItem OOI
ON			O.pkOperationID = OOI.fkOperationID
INNER JOIN	OrderItem OI
ON			OOI.fkItemID = OI.pkItemID
INNER JOIN	RFQOrder RO
ON			OI.fkRFQOID = RO.pkRFQOID
INNER JOIN	POn PON
ON			RO.pkRFQOID = PON.fkRFQOID

WHERE		PartNumber LIKE CONCAT(SearchField, "%");
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPartTemplateAlert` (IN `UserID` SMALLINT UNSIGNED, `PTemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		PartTemplate
SET			OnAlert = 1

WHERE		pkPartTemplateID = PTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPartTemplateAlertOff` (IN `UserID` SMALLINT UNSIGNED, `PTemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		PartTemplate
SET			OnAlert = 0

WHERE		pkPartTemplateID = PTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPartTemplateList` ()  BEGIN
SELECT		PT.pkPartTemplateID AS PTemplateID, E.pkEngineeringTemplateID AS ETemplateID, 
			PartNumber, '//', max(IssueNumber) AS Issue, '//', 
			IFNULL((SELECT 	max(Description) 
					FROM 			EngineeringTemplate ET 
					INNER JOIN 		SupplyCondition SC
					ON 				ET.fkSupplyConditionID = SC.pkSupplyConditionID 
					WHERE 			fkPartTemplateID = PT.pkPartTemplateID
					AND SC.Deleted <> 1),"no Eng Tmplt Defined") AS COS, 
			IF((SELECT Count(*) 
				FROM 		LineItem  LL
				INNER JOIN	WorkOrder WOR 
				ON			LL.pkLineItemID = WOR.fkLineItemID
				WHERE 		fkPartTemplateID = PTemplateID 
				AND 		(CancelReason IS NULL OR CancelReason < 1)
				AND WOR.Complete = 1) >0, 1, 0) AS Status, 1 AS PlanningComplete

FROM 		PartTemplate PT
LEFT JOIN	EngineeringTemplate E
ON			PT.pkPartTemplateID = E.fkPartTemplateID

GROUP BY 	PartNumber

UNION

SELECT		PT.pkPartTemplateID AS PTemplateID, T.pkEngineeringTemplateID AS ETemplateID, 
			PartNumber, '//', max(IssueNumber) AS Issue, '//', SC.Description AS COS,
			IF((SELECT Count(*) 
				FROM 		LineItem  L
				INNER JOIN	WorkOrder WRK
				ON			L.pkLineItemID = WRK.fkLineItemID
				WHERE 		fkPartTemplateID = PTemplateID 
				AND 		(CancelReason IS NULL OR CancelReason < 1)
				AND WRK.Complete = 1) >0, 1, 0) AS Status, 1 AS PlanningComplete

FROM 		PartTemplate PT
LEFT JOIN	EngineeringTemplate T
ON			PT.pkPartTemplateID = T.fkPartTemplateID
INNER JOIN 	SupplyCondition SC
ON 			T.fkSupplyConditionID = SC.pkSupplyConditionID
 
ORDER BY 	PartNumber;		
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPartTemplateLock` (IN `UserID` SMALLINT UNSIGNED, `PTemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		PartTemplate
SET			LockedDown = 1

WHERE		pkPartTemplateID = PTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPartTemplateLockOff` (IN `UserID` SMALLINT UNSIGNED, `PTemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		PartTemplate
SET			LockedDown = 0

WHERE		pkPartTemplateID = PTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPartTemplateSearch` (IN `PartNumberSearchString` VARCHAR(25), `Filter` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;


	SELECT		DISTINCT PT.pkPartTemplateID AS PTemplateID, T.pkEngineeringTemplateID AS ETemplateID, 
				PartNumber, '//', IssueNumber AS Issue, '//', IF(CC.Description IS NOT NULL, CC.Description,"no Eng Tmplt Defined") AS COS

	FROM 		PartTemplate PT
	INNER JOIN	EngineeringTemplate T
	ON			PT.pkPartTemplateID = T.fkPartTemplateID
	INNER JOIN	SupplyCondition CC
	ON			T.fkSupplyConditionID = CC.pkSupplyConditionID

	WHERE		PartNumber LIKE CONCAT(PartNumberSearchString,'%')
	
	
UNION
	SELECT		DISTINCT PT.pkPartTemplateID AS PTemplateID, T.pkEngineeringTemplateID AS ETemplateID, 
				PartNumber, '//', IssueNumber AS Issue, '//', IF(CC.Description IS NOT NULL, CC.Description,"no Eng Tmplt Defined") AS COS

	FROM 		PartTemplate PT
	LEFT JOIN	EngineeringTemplate T
	ON			PT.pkPartTemplateID = T.fkPartTemplateID
	LEFT JOIN	SupplyCondition CC
	ON			T.fkSupplyConditionID = CC.pkSupplyConditionID

	WHERE		PartNumber LIKE CONCAT(PartNumberSearchString,'%')
	
	ORDER BY 	PartNumber, Issue;




	





END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPauseInspection` (IN `UserID` SMALLINT UNSIGNED, `InspectionID` BIGINT UNSIGNED, `ReasonID` TINYINT UNSIGNED)  BEGIN
DECLARE		MsgResult 		varchar(25);

IF		ISNULL(InspectionID)
THEN	SET MsgResult = 'Error; not running';
ELSE

IF		EXISTS		(SELECT *
					FROM  Inspection
					WHERE pkInspectionID = InspectionID)
		AND			(SELECT InspectStart
					FROM  Inspection
					WHERE pkInspectionID = InspectionID) IS NOT NULL
		AND 		ISNULL((SELECT InspectEnd
							FROM  Inspection
							WHERE pkInspectionID = InspectionID))

THEN					IF EXISTS	(SELECT StartTime 
					FROM InspectPause 
					WHERE fkInspectionID = InspectionID 
					AND EndTime IS NULL)
							
		THEN

						UPDATE	InspectPause
			SET		EndTime = now(), fkUserIDEnd = UserID

			WHERE	fkInspectionID = InspectionID;

			SET		MsgResult = 'Unpaused';
			
		ELSE
						INSERT INTO	InspectPause
						(fkInspectionID, StartTime, fkReasonID, fkUserIDStart)
			VALUES		(InspectionID, now(), ReasonID, UserID);
			
			SET		MsgResult = 'Paused';

		END IF;

ELSE
		IF		ISNULL((SELECT InspectStart
						FROM  Inspection
						WHERE pkInspectionID = InspectionID)) 
		THEN
								SET		MsgResult = 'Error; not running';
		ELSE
								SET		MsgResult = 'Error; complete';

		END IF;
END IF;
END IF;

SELECT	MsgResult;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPauseMaterialIssue` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN

IF EXISTS (SELECT pkPauseID FROM Pause WHERE fkOperationID = (SELECT pkOperationID 
														FROM Operation 
														WHERE OperationNumber = 1 
														AND fkWorkOrderID = WorkOrderID)
								AND	fkMachineID = (SELECT DISTINCT fkMachineID
													FROM OperationMachine OM
													INNER JOIN Operation O
													ON OM.fkOperationID = (SELECT pkOperationID 
																			FROM Operation 
																			WHERE OperationNumber = 1 
																			AND fkWorkOrderID = WorkOrderID)

													WHERE fkWorkOrderID = WorkOrderID)) THEN
		UPDATE Pause 
	SET EndTime = now(), fkUserIDEnd = UserID

	WHERE 	fkOperationID = (SELECT pkOperationID 
								FROM Operation 
								WHERE OperationNumber = 1 
								AND fkWorkOrderID = WorkOrderID)
	AND		fkMachineID = (SELECT DISTINCT fkMachineID
							FROM OperationMachine OM
							INNER JOIN Operation O
							ON OM.fkOperationID = (SELECT pkOperationID 
													FROM Operation 
													WHERE OperationNumber = 1 
													AND fkWorkOrderID = 10101));
ELSE
		INSERT INTO		Pause
					(fkOperationID, fkMachineID, StartTime, fkReasonID, fkUserIDStart)
	VALUES			((SELECT pkOperationID 
					FROM Operation 
					WHERE OperationNumber = 1 
					AND fkWorkOrderID = WorkOrderID), (SELECT DISTINCT fkMachineID
														FROM OperationMachine OM
														INNER JOIN Operation O
														ON OM.fkOperationID = (SELECT pkOperationID 
																				FROM Operation 
																				WHERE OperationNumber = 1 
																				AND fkWorkOrderID = 10101)), now(), 1, UserID);	

END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPauseOperation` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `MachineID` TINYINT UNSIGNED, `ReasonID` TINYINT UNSIGNED)  BEGIN
DECLARE		MaxPauseID	int default 0;
DECLARE		ResultMsg   varchar(30);

IF NOT ISNULL((SELECT RunStart FROM OperationMachine WHERE fkOperationID = OperationID))
		OR NOT ISNULL((SELECT StartTime FROM MachineSet WHERE fkOperationID = OperationID))
THEN 
	
			IF NOT	ISNULL((SELECT StartTime 
			FROM Pause P 
			WHERE P.fkOperationID = OperationID
			AND P.fkMachineID = MachineID
			AND ISNULL(P.EndTime)))
THEN
	SET		MaxPauseID = (SELECT pkPauseID
						FROM Pause 
						WHERE fkOperationID = OperationID
						AND fkMachineID = MachineID
						AND ISNULL(EndTime));

		UPDATE	Pause
	SET		EndTime = now(), fkUserIDEnd = UserID

	WHERE	fkOperationID = OperationID
	AND		fkMachineID = MachineID
	AND		pkPauseID = MaxPauseID;

	SET ResultMsg = 'Op running again';

ELSE
				INSERT INTO	Pause
					(fkOperationID, fkMachineID, StartTime, fkReasonID, fkUserIDStart)
		VALUES		(OperationID, MachineID, now(), ReasonID, UserID);

	SET ResultMsg = 'Op now paused';

END IF;

ELSE
SET ResultMsg = 'Error; nothing running';
END IF;
	
	SELECT ResultMsg;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPauseOpInspection` (IN `UserID` SMALLINT UNSIGNED, `InspectionID` INT UNSIGNED, `ReasonID` TINYINT UNSIGNED)  BEGIN
IF EXISTS	(SELECT StartTime 
			FROM InspectPause 
			WHERE fkInspectionID = InspectionID
			AND fkMachineID = MachineID) 
THEN
		UPDATE	InspectPause
	SET		EndTime = now(), fkUserIDEnd = UserID

	WHERE	fkInspectionID = InspectionID
	AND		fkMachineID = MachineID;
	
ELSE
		INSERT INTO	InspectPause
				(fkInspectionID, fkMachineID, StartTime, fkReasonID, fkUserIDStart)
VALUES			(InspectionID, MachineID, now(), ReasonID, UserID);
	
END IF; 

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPhotocopyTemplate` (IN `UserID` SMALLINT UNSIGNED, `PTemplateID` SMALLINT UNSIGNED, `OrigETemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE done 																		INT DEFAULT FALSE;
DECLARE CRiskStatement 																CHAR(100);
DECLARE OpStore 																	INT unsigned;
DECLARE OperationTemplateID, ItemStore, NewOpTmpltID								mediumint unsigned;
DECLARE EngineeringTemplateID, RFQOid, StdSuppID, NewETemplateID					smallint unsigned;
DECLARE COperationNumber, COperationTypeID, CMachiningID, CLinked, OrderItemNumber,
		CEngComplete																tinyint unsigned;
DECLARE CEstimatedSetTime, CEstimatedRunTime 										time;

DECLARE OpTmpltCursor CURSOR FOR 
		SELECT 	pkOperationTemplateID, fkEngineeringTemplateID, fkOperationTypeID, fkMachiningID, OperationNumber,
				EstimatedSetTime, EstimatedRunTime, EngComplete, RiskStatement, Linked 

		FROM 	OperationTemplate
		
		WHERE	fkEngineeringTemplateID = OrigETemplateID;

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;


INSERT INTO		EngineeringTemplate
				(fkSupplyconditionID, fkPartTemplateID, ReviewStatus, Tooling, QAEquipment, CorrectApproval, RiskAssessment, 
				AddedByfkUserID, EngineeringIssue, PlanningComplete)
	SELECT 		fkSupplyconditionID, PTemplateID, ReviewStatus, Tooling, QAEquipment, CorrectApproval, RiskAssessment, UserID,
				0, 0

	FROM		EngineeringTemplate ET

	WHERE 		pkEngineeringTemplateID = OrigETemplateID;
					
SET				NewETemplateID = Last_Insert_ID();


  OPEN OpTmpltCursor;

  read_loop: LOOP
		FETCH OpTmpltCursor INTO OperationTemplateID, EngineeringTemplateID, COperationNumber, COperationTypeID, CMachiningID, 
								CEstimatedSetTime, CEstimatedRunTime, CEngComplete, CRiskStatement, CLinked;

		IF done THEN
			LEAVE read_loop;

		END IF;

		INSERT INTO 	OperationTemplate
						(fkEngineeringTemplateID, fkOperationTypeID, fkMachiningID, OperationNumber,
						EstimatedSetTime, EstimatedRunTime, EngComplete, RiskStatement, Linked)

			VALUES		(NewETemplateID, COperationNumber, COperationTypeID, CMachiningID, 
						CEstimatedSetTime, CEstimatedRunTime, CEngComplete, CRiskStatement, CLinked);

		SET				NewOpTmpltID = Last_Insert_ID();


		INSERT INTO		TemplateMaterial
						(pkOperationTemplateID, fkMaterialSpecID, fkRFQOID, Description, Size, fkShapeID, PartLength, Yield, 
						fkApprovalID, SupplyCondition, TestPieces, FreeIssue, UnitID, StdDeliveryDays, SPUfkUOMID, 
						StdfkSupplierID, IssueInstruction, AddedByfkUserID)

			SELECT		NewOpTmpltID, fkMaterialSpecID, fkRFQOID, Description, Size, fkShapeID, PartLength, Yield, 
						fkApprovalID, SupplyCondition, TestPieces, FreeIssue, UnitID, StdDeliveryDays, SPUfkUOMID, 
						StdfkSupplierID, IssueInstruction, AddedByfkUserID		

			FROM		TemplateMaterial

			WHERE		pkOperationTemplateID = OperationTemplateID;


		INSERT INTO		OpTmpltMachineProgram
						(fkOperationTemplateID, fkMachineID, ProgramNumber)

			SELECT		NewOpTmpltID, fkMachineID, ProgramNumber

			FROM		OpTmpltMachineProgram

			WHERE		fkOperationTemplateID = OperationTemplateID;


		INSERT INTO		TemplateTreatment
						(pkOperationTemplateID, fkTreatmentID, LeadTime, StandardSupplierID, ApprovalComment, AddedByfkUserID)

			SELECT		NewOpTmpltID, fkTreatmentID, LeadTime, StandardSupplierID, ApprovalComment, AddedByfkUserID

			FROM		TemplateTreatment

			WHERE		pkOperationTemplateID = OperationTemplateID;


		INSERT INTO		TemplateProcess
						(fkOperationTemplateID, ProcessNote)

			SELECT		NewOpTmpltID, ProcessNote

			FROM		TemplateProcess

			WHERE		fkOperationTemplateID = OperationTemplateID;



		INSERT INTO		TemplateMachining
						(fkOperationTemplateID, fkMachiningListID)

			SELECT		NewOpTmpltID, fkMachiningListID

			FROM		TemplateMachining

			WHERE		fkOperationTemplateID = OperationTemplateID;


		INSERT INTO		TemplateApprovalComments
						(fkOperationTemplateID, Comment)

			SELECT		NewOpTmpltID, Comment

			FROM		TemplateApprovalComments

			WHERE		fkOperationTemplateID = OperationTemplateID;


		INSERT INTO		TemplateNote
						(fkOperationTemplateID, Note, fkNoteTypeID, CreateDate, AddedByfkUserID, fkPartTemplateID)

			SELECT		NewOpTmpltID, Note, fkNoteTypeID, CreateDate, AddedByfkUserID, fkPartTemplateID

			FROM		TemplateNote

			WHERE		fkOperationTemplateID = OperationTemplateID;


		INSERT INTO		TemplatePOComments
						(fkOperationTemplateID, Comment)

			SELECT		NewOpTmpltID, Comment

			FROM		TemplatePOComments

			WHERE		fkOperationTemplateID = OperationTemplateID;

  END LOOP;

CLOSE OpTmpltCursor;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPOSearchList` (IN `POSearchField` VARCHAR(5))  BEGIN
SELECT		pkPOnID AS PONo, fkRFQOID AS RFQOID

FROM		POn

WHERE		CAST(pkPOnID AS char) LIKE CONCAT(POSearchField, "%");
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPurchaseOrder` (IN `PONo` SMALLINT UNSIGNED)  BEGIN
SELECT 
DISTINCT	SupplierName, Address1, Address2, Address3, Address4, Postcode, SC.ContactName AS FAO,
			PON.pkPOnID AS pkPurchaseOrderID, CO.CustomerOrderRef AS CertNo, GI.DeliveryNoteNumber, PT.Description,
			CONCAT(OI.Quantity, ' off') AS Quantity, CONCAT("£", IF(Per = 1, '', OI.Price), ' (each)') AS Price, 
			CONCAT("£", IF(Per = 2, OI.Quantity * OI.Price, OI.Price), ' (Lot)') AS ItemTotal,
			D.Reference, D.IssueNumber, PT.PartNumber, PT.IssueNumber AS PartIssue, GI.IdentificationMark, 
			GI.IRNoteNumber, CoCNote, POG.fkGRBID, WO.pkWorkOrderID, WO.Complete, I.Description AS InstructionDesc,
			DATE_ADD(PON.CreateDate, INTERVAL IFNULL(PT.StdLeadDays, 10) DAY) DelDate

FROM		rfqorder 			RO
INNER JOIN	POn PON
ON			RO.pkRFQOID = PON.fkRFQOID
LEFT JOIN	RFQOSupplier 			RS
ON			RO.pkRFQOID = 			RS.fkRFQOID
AND			RS.Selected = 1
LEFT JOIN	Supplier 			S
ON			RS.fkSupplierID = 		S.pkSupplierID
LEFT JOIN	SupplierContact 		SC
ON			RS.fkContactID = 		SC.pkContactID

LEFT JOIN	OrderItem			OI
ON			RO.pkRFQOID =			OI.fkRFQOID
LEFT JOIN	OperationOrderItem 		OOO
ON			OI.pkItemID = 			OOO.fkItemID
LEFT JOIN	Operation 			OP
ON			OOO.fkOperationID = 	OP.pkOperationID
LEFT JOIN	WorkOrder 			WO
ON			OP.fkWorkOrderID = 		WO.pkWorkOrderID
LEFT JOIN	LineItem 			LI
ON			WO.fkLineItemID = 		LI.pkLineItemID
LEFT JOIN	parttemplate 		PT
ON			LI.fkPartTemplateID = 	PT.pkPartTemplateID
LEFT JOIN	drawing 			D
ON			PT.fkDrawingID = 		D.pkDrawingID
LEFT JOIN	allocated 			A
ON			OP.pkOperationID = 		A.fkOperationID
LEFT JOIN	material 			M
ON			A.fkGRBID = 		M.fkGRBID
left JOIN	goodsin 			GI
ON			M.fkGRBID = 		GI.pkGRBID
LEFT JOIN	operationmaterial 	OM
ON			OP.pkOperationID = 		OM.pkOperationID
LEFT JOIN	pogoods 			POG
ON			A.fkGRBID = 		POG.fkGRBID	
LEFT JOIN	Instruction 		I
ON			RO.pkRFQOID = 			I.fkRFQOID	
LEFT JOIN	CustomerOrder 		CO
ON			LI.fkOrderID = 			CO.pkOrderID
			
WHERE		PON.pkPOnID = PONo;	
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPurchaseOrderPreview` (IN `RFQOID` SMALLINT UNSIGNED)  BEGIN
SELECT 
DISTINCT	SupplierName, Address1, Address2, Address3, Address4, Postcode, SC.ContactName AS FAO,
			99999 AS pkPurchaseOrderID, CO.CustomerOrderRef AS CertNo, 'DelNoteNo' AS DeliveryNoteNumber, PT.Description,
			CONCAT(OI.Quantity, ' off') AS Quantity, CONCAT("?", OI.Price, ' (each)') AS Price, 
			CONCAT("?", IF(Per = 2, OI.Quantity * OI.Price, OI.Price), ' (Lot)') AS ItemTotal,
			D.Reference, D.IssueNumber, PT.PartNumber, PT.IssueNumber AS PartIssue, 'IdentMark' AS IdentificationMark, 
			'IRnoteNo' AS IRNoteNumber, CoCNote, 'GRBNo' AS fkGRBID, WO.pkWorkOrderID, WO.Complete, I.Description AS InstructionDesc,
			DATE_ADD(CurDate(), INTERVAL IFNULL(PT.StdLeadDays, 10) DAY) DelDate, pkRFQOID

FROM		rfqorder 			RO
LEFT JOIN	RFQOSupplier 			RS 
ON			RO.pkRFQOID = 			RS.fkRFQOID
AND			RS.Selected = 1
LEFT JOIN	Supplier 			S
ON			RS.fkSupplierID = 		S.pkSupplierID
LEFT JOIN	SupplierContact 		SC
ON			RS.fkContactID = 		SC.pkContactID

LEFT JOIN	OrderItem			OI
ON			RO.pkRFQOID =			OI.fkRFQOID
LEFT JOIN	OperationOrderItem 		OOO
ON			OI.pkItemID = 			OOO.fkItemID
LEFT JOIN	Operation 			OP
ON			OOO.fkOperationID = 	OP.pkOperationID
LEFT JOIN	WorkOrder 			WO
ON			OP.fkWorkOrderID = 		WO.pkWorkOrderID
LEFT JOIN	LineItem 			LI
ON			WO.fkLineItemID = 		LI.pkLineItemID
LEFT JOIN	parttemplate 		PT
ON			LI.fkPartTemplateID = 	PT.pkPartTemplateID
LEFT JOIN	drawing 			D
ON			PT.fkDrawingID = 		D.pkDrawingID
LEFT JOIN	allocated 			A
ON			OP.pkOperationID = 		A.fkOperationID
LEFT JOIN	material 			M
ON			A.fkGRBID = 		M.fkGRBID
LEFT JOIN	operationmaterial 	OM
ON			OP.pkOperationID = 		OM.pkOperationID
LEFT JOIN	Instruction 		I
ON			RO.pkRFQOID = 			I.fkRFQOID	
LEFT JOIN	CustomerOrder 		CO
ON			LI.fkOrderID = 			CO.pkOrderID
			
WHERE		RO.pkRFQOID = RFQOID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPurchToQuote` (IN `OperationTemplateID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE Kythera.TemplateMaterial SET PurchasingToQuote = 1 WHERE pkOperationTemplateID = OperationTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPurchToReview` (IN `UserID` SMALLINT UNSIGNED, `PartTemplateID` SMALLINT UNSIGNED, `ETemplateID` SMALLINT UNSIGNED, `PartNumber` VARCHAR(25))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		Kythera.Todo 
				(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkPartTemplateID, PartNumber, 
				fkEngineeringTemplateID, Description)
VALUES			(Date(Now()), 4, UserID, 4, PartTemplateID, PartNumber, ETemplateID, 'Material Review');
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcPurchToReviewSC` (IN `UserID` SMALLINT UNSIGNED, `PartTemplateID` SMALLINT UNSIGNED, `ETemplateID` SMALLINT UNSIGNED, `PartNumber` VARCHAR(25))  BEGIN 

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		Kythera.Todo 
				(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkPartTemplateID, PartNumber, Complete, 
				fkEngineeringTemplateID, Description)
VALUES			(Date(Now()), 4, UserID, 2, PartTemplateID, PartNumber, 0, ETemplateID, 'Sub-con Review');

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcQualityToReview` (IN `UserID` SMALLINT UNSIGNED, `ETemplateID` SMALLINT UNSIGNED, `PartNumber` VARCHAR(25))  BEGIN

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		Todo
				(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, PartNumber, fkEngineeringTemplateID, 
				Complete, Description)
VALUES			(DATE(Now()), 10, UserID, 5, Partnumber, ETemplateID, 0, 'Review Part tmplt');
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRaiseAdjustment` (IN `UserID` SMALLINT UNSIGNED, `InvoiceID` INT UNSIGNED, `ReasonID` TINYINT UNSIGNED, `DebitAmount` DECIMAL(7,2))  BEGIN
INSERT INTO		RejectInvoice
				(fkInvoiceID, Value, CreatedByfkUserID, fkReasonID, Reject)
VALUES			(InvoiceID, DebitAmount, UserID, ReasonID, 0);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRaisePO` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `POQty` SMALLINT UNSIGNED)  BEGIN
DECLARE			ApprovalStore, LeadStore	tinyint unsigned;
DECLARE			RFQStore, POStore			smallint unsigned;
DECLARE			ItemStore, WOStore			mediumint unsigned;

SET				WOStore = 		(SELECT fkWorkOrderID FROM Operation WHERE pkOperationID = OperationID);
SET				ApprovalStore = (SELECT fkApprovalID FROM OperationTreatment WHERE pkOperationID = OperationID);
SET				LeadStore = 	(SELECT LeadTime FROM OperationTreatment WHERE pkOperationID = OperationID);

INSERT INTO		RFQOrder
				(fkApprovalLevelID, fkUserID, Reference, CreateDate, LeadDays, OrderQuantity, Cancelled, Complete, POType)
VALUES			(ApprovalStore, UserID, CONCAT("AutoPO WO", WOStore), now(), LeadStore, POQty, 0, 0, 2);

SET				RFQStore = LAST_INSERT_ID();


INSERT INTO		POn
				(fkRFQOID, CreateDate, AddedByfkUserID)
VALUES			(RFQStore, CurDate(), UserID);

SET				POStore = LAST_INSERT_ID();


INSERT INTO		OrderItem
				(fkRFQOID, ItemNo, Quantity, AddedByfkUserID)
VALUES			(RFQStore, 1, POQty, UserID);

SET				ItemStore = LAST_INSERT_ID();


INSERT INTO		OperationOrderItem
				(fkOperationID, fkItemID, AddedByfkUserID)
VALUES			(OperationID, ItemStore, UserID);

SELECT			Row_Count();


INSERT INTO		Todo
				(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, Complete, Description)
	VALUES		(CurDate(), 14, UserID, 4, 0, CONCAT("Complete Subcon PO No.", POStore));

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRaiseReject` (IN `UserID` SMALLINT UNSIGNED, `InvoiceID` INT UNSIGNED, `ReasonID` TINYINT UNSIGNED)  BEGIN
INSERT INTO		RejectInvoice
				(fkInvoiceID, Value, CreatedByfkUserID, fkReasonID, Reject)
VALUES			(InvoiceID, 0, UserID, ReasonID, 1);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRaiseReprintPO` (IN `UserID` SMALLINT UNSIGNED, `RFQOID` SMALLINT UNSIGNED)  BEGIN

IF 		(SELECT count(*)
				FROM POn 
				WHERE fkRFQOID = RFQOID) = 0
THEN

		INSERT INTO		POn
						(fkRFQOID, CreateDate, AddedByfkUserID)
		VALUES			(RFQOID, CurDate(), UserID);

		SELECT			LAST_INSERT_ID() AS PONo;

ELSE

		SELECT			pkPOnID AS PONo
		FROM			POn
		WHERE 			fkRFQOID = RFQOID;

END IF;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRaiseRFQ` (IN `UserID` SMALLINT UNSIGNED, `RFQOID` SMALLINT UNSIGNED)  BEGIN
INSERT INTO		RFQn
					(fkRFQOID, AddedByfkUserID)
VALUES				(RFQOID, UserID);


SELECT				LAST_INSERT_ID() AS pkRFQnID, ContactName, Email

FROM				RFQOSupplier RS
INNER JOIN			SupplierContact SC
ON					RS.fkSupplierID = SC.fkSupplierID

WHERE				fkRFQOID = RFQOID
AND					SC.Prime = 1;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRemoveAssignedRole` (IN `UserID` SMALLINT UNSIGNED, `StaffUserID` SMALLINT UNSIGNED, `RoleID` TINYINT UNSIGNED)  BEGIN
DELETE	FROM		UserDept
					
WHERE				fkUserID = StaffUserID
AND					fkDeptID = RoleID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRemoveComment` (IN `CommentID` SMALLINT UNSIGNED)  BEGIN
UPDATE		Comments
SET			Deleted = 1

WHERE		pkCommentID = CommentID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRemoveCourier` (IN `CourierID` TINYINT UNSIGNED)  BEGIN
UPDATE 	Courier
SET			Deleted = 1

WHERE		pkCourierID = CourierID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRemoveMachine` (IN `MachineID` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		Machine
SET			InUseYN = 2

WHERE		pkMachineID = MachineID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRemoveRemark` (IN `RemarkID` SMALLINT UNSIGNED)  BEGIN
UPDATE		Remark
SET			Deleted = 1

WHERE		pkRemarkID = RemarkID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRemoveStoreArea` (IN `StoreAreaID` TINYINT UNSIGNED)  BEGIN
DELETE FROM	StoreArea
WHERE		pkStoreAreaID = StoreAreaID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRemoveSupplierAdditional` (IN `UserID` SMALLINT UNSIGNED, `AdditionalID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
DELETE FROM		SupplierAdditionalTime

WHERE				pkSupplierAdditionalID = AdditionalID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRemoveSupplierContact` (IN `UserID` SMALLINT UNSIGNED, `ContactID` SMALLINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		SupplierContact
SET			Deleted = 1
			
WHERE		pkContactID = ContactID;

SELECT			Row_Count() INTO Result;

IF Result <> 1
THEN
	SELECT		Result;
ELSE			
	SELECT		1;
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRemoveXLocation` (IN `StoreAreaID` TINYINT UNSIGNED, `XCoordinate` TINYINT UNSIGNED)  BEGIN
UPDATE			XCoordinate
SET				Deleted = 1

WHERE			fkStoreAreaID = StoreAreaID
AND				XCoord = XCoordinate;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRemoveYLocation` (IN `StoreAreaID` TINYINT UNSIGNED, `YCoordinate` CHAR(1))  BEGIN
UPDATE			YCoordinate
SET				Deleted = 1
WHERE			fkStoreAreaID = StoreAreaID
AND				YCoord = YCoordinate;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRepeatOp` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED)  BEGIN

DECLARE		Result		tinyint default 0;
DECLARE		Result2		tinyint default 0;
DECLARE		RepeatStore	tinyint default 0;

SET			RepeatStore = (SELECT Repeated FROM OperationMachine WHERE fkOperationID = OperationID);

IF 			RepeatStore IS NULL 
THEN 		SET RepeatStore = 0; 
END IF;

UPDATE		OperationMachine
SET			fkUserIDRunStart = NULL, RunStart = NULL, fkUserIDRunEnd = NULL, RunEnd = NULL,
			fkUserIDInspectStart = NULL, InspectStart = NULL, fkUserIDInspectEnd = NULL, 
			InspectEnd = NULL,
			Repeated = RepeatStore + 1, fkUserIDRepeat = UserID

WHERE 		fkOperationID = OperationID;

	SELECT			Row_Count() INTO Result;	


UPDATE		MachineSet
SET			fkUserIDSetStart = NULL, StartTime = NULL, fkUserIDSetEnd = NULL, EndTime = NULL,
			fkUserIDInspectStart = NULL, InspectStart = NULL, InspectEnd = NULL, 
			fkUserIDInspectEnd = NULL, fkUserIDSetter = NULL

WHERE 		fkOperationID = OperationID;

	SELECT			Row_Count() INTO Result2;


SELECT		CONCAT(Result, Result2) AS 11IsGood;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRepeatWorkOrder` (IN `UserID` TINYINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED, `Qty` SMALLINT UNSIGNED)  BEGIN

DECLARE			WOIdStore, ItemIDStore, COID 							mediumint unsigned DEFAULT 0;
DECLARE			OpIdStore 												int unsigned DEFAULT 0;
DECLARE			RFQOIdStore, NewRFQOID									smallint unsigned DEFAULT 0;

DECLARE 		done 															INT DEFAULT FALSE;
DECLARE		CursOperationID													int unsigned;
DECLARE		CursOperationNumber, CursOperationTypeID, CursMachiningTypeID, 
				CursBypass, CursSubcon, CursEngComplete, CursLinked				tinyint unsigned;
DECLARE		CursEstimatedSetTime, CursEstimatedRunTime						time;
DECLARE		CursStartQty, CursEndQty										smallint unsigned;
DECLARE		CursScheduledStartDate											date;
DECLARE		CursRiskStatement												varchar(100);


DECLARE OpCursor CURSOR FOR 
		SELECT pkOperationID, OperationNumber, fkOperationTypeID, fkMachiningTypeID, ScheduledStartDate,
				StartQty, EndQty, EstimatedSetTime, EstimatedRunTime, Bypass, Subcon, RiskStatement, 
				EngComplete, Linked 

		FROM 	Operation
		
		WHERE	fkWorkOrderID = WorkOrderID;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;


INSERT INTO		WorkOrder
				(fkLineItemID, fkApprovalID, Complete, QuantityReqd, QuantityOvers, PriceEach, FAIR, Live,
					CreatedByfkUserID, FastTrack,Tooling, QAEquipment, CorrectApproval)
SELECT			fkLineItemID, fkApprovalID, 0, Qty, 0, PriceEach, FAIR, 1,
					UserID, FastTrack, Tooling, QAEquipment, CorrectApproval
FROM			WorkOrder
WHERE			pkWorkOrderID = WorkOrderID;

SET				WOIdStore = LAST_INSERT_ID();



  OPEN OpCursor;

  read_loop: LOOP
		FETCH OpCursor INTO CursOperationID, CursOperationNumber, CursOperationTypeID, CursMachiningTypeID, 
							CursScheduledstartDate, CursStartQty, CursEndQty, CursEstimatedSetTime, 
							CursEstimatedRunTime, CursBypass, CursSubcon, CursRiskStatement, CursEngComplete, 
							CursLinked;

		IF done THEN
			LEAVE read_loop;

		END IF;

		INSERT INTO	Operation
						(fkWorkOrderID, OperationNumber, fkOperationTypeID, fkMachiningTypeID, StartQty,
						EndQty, EstimatedSetTime, EstimatedRunTime, Bypass, Subcon, RiskStatement, 
						EngComplete, Linked)
			VALUES		(WOIdStore, CursOperationNumber, CursOperationTypeID, CursMachiningTypeID, CursStartQty,
						CursEndQty, CursEstimatedSetTime, CursEstimatedRunTime, CursBypass, CursSubcon, 
						CursRiskStatement, CursEngComplete, CursLinked);
		
			SET			OpIDStore = LAST_INSERT_ID();


		INSERT INTO	OperationMaterial
						(pkOperationID, fkMaterialSpecID, Description, Size, fkShapeID, PartLength, fkApprovalID, 
						SupplyCondition, FreeIssue, UnitID, StdfkSupplierID, IssueInstruction, SPUfkUOMID, 
						SPU, Yield, TestPieces, StdDeliveryDays, AddedByfkUserID)
			SELECT		OpIDStore, fkMaterialSpecID, Description, Size, fkShapeID, PartLength, fkApprovalID, 
						SupplyCondition, FreeIssue, UnitID, StdfkSupplierID, IssueInstruction, SPUfkUOMID,
						UnitID, Yield, TestPieces, StdDeliveryDays, UserID

			FROM		OperationMaterial

			WHERE		pkOperationID = CursOperationID;


		INSERT INTO	OperationTreatment
						(pkOperationID, fkTreatmentID, LeadTime, ApprovalComment, AddedByfkUserID)
			SELECT		OpIDStore, fkTreatmentID, LeadTime, ApprovalComment, UserID
						
			FROM		OperationTreatment

			WHERE		pkOperationID = CursOperationID;


		INSERT INTO	OperationMachine
						(fkOperationID, fkMachineID, Multi)
			SELECT		DISTINCT OpIDStore, fkMachineID, Multi

			FROM		OperationMachine

			WHERE		fkOperationID = CursOperationID;


		INSERT INTO	OpMachineProgram
						(fkOperationID, fkMachineID, fkProgramNumber, DateAdded)
			SELECT		OpIDStore, fkMachineID, fkProgramNumber, DATE(Now())

			FROM		OpMachineProgram

			WHERE		fkOperationID = CursOperationID;


		INSERT INTO	MachineSet
						(fkOperationID, fkMachineID)
		SELECT			DISTINCT OpIDStore, fkMachineID

			FROM		MachineSet

			WHERE		fkOperationID = CursOperationID;


		INSERT INTO	OperationProcess
							(fkOperationID, ProcessNote)
				SELECT		OpIDStore, ProcessNote

				FROM	OperationProcess

			WHERE		fkOperationID = CursOperationID;


		INSERT INTO	OperationOrderItem
						(fkOperationID, fkItemID, AddedByfkUserID)
			SELECT		OpIDStore, ItemIDStore = fkItemID, UserID

			FROM		OperationOrderItem

			WHERE		fkOperationID = CursOperationID;

				
		INSERT INTO	OrderItem
						(fkRFQOID, ItemNo, fkPartTemplateID, fkShapeID, SPU, fkUOMID, Quantity, Description,
						Comment, Price, Size, UnitID, Per, AddedByfkUserID)
			SELECT		RFQOIDStore = fkRFQOID, ItemNo, fkPartTemplateID, fkShapeID, SPU, fkUOMID, Quantity, Description, 
						Comment, Price, Size, UnitID, Per, UserID

			FROM		OrderItem Oi
			INNER JOIN	OperationOrderItem OOI
			ON			OI.pkItemID = OOI.fkItemID

			WHERE		OOI.fkOperationID = CursOperationID;


		INSERT INTO	RFQOrder
						(fkApprovalLevelID, fkUserID, OtherCharges, ReturnDrawing, VeryUrgent, InternalOnly,
						MOQ, Reference, cocRequired, CreateDate, LeadDays, OrderQuantity, Cancelled, Complete, POType)
			SELECT		fkApprovalLevelID, UserID, OtherCharges, ReturnDrawing, VeryUrgent, InternalOnly,
						MOQ, Reference, cocRequired, Now(), LeadDays, OrderQuantity, 0, 0, POType

			FROM		RFQOrder

			WHERE		pkRFQOID = RFQOIDStore;

			SET			NewRFQOID = Last_Insert_ID();


		INSERT INTO	RFQn
						(fkRFQOID, AddedByfkUserID)
			VALUES		(NewRFQOID, UserID);


		INSERT INTO	RFQOSupplier
						(fkRFQOID, fkSupplierID, fkcontactID, Selected, TotalCost, DeliveryCost, CertCharge)
			SELECT		NewRFQOID, fkSupplierID, fkcontactID, Selected, TotalCost, DeliveryCost, CertCharge

			FROM		RFQOSupplier

			WHERE		fkRFQOID = RFQOIDStore;


		INSERT INTO	PONote
						(fkRFQOID, Note)
			SELECT		NewRFQOID, Note

			FROM		PONote

			WHERE		fkRFQOID = RFQOIDStore;


		INSERT INTO	POComment
						(fkRFQOID, Comment, Deleted)
			SELECT		NewRFQOID, Comment, 0

			FROM		POComment

			WHERE		fkRFQOID = RFQOIDStore
			AND			Deleted = 0;
			

END Loop;


Close OpCursor;


INSERT INTO	CustomerOrder
				(fKCustomerID, CustomerOrderRef, CreationDate, CreationfkUserID, fkApprovalID, fkDeliveryPointID,
				CommerciallyViable, AmendmentReviewed)
	SELECT		fkCustomerID, CustomerOrderRef, Date(now()), UserID, fkApprovalID, fkDeliveryPointID, CommerciallyViable, 
				AmendmentReviewed

	FROM		CustomerOrder

	WHERE		pkOrderID = (SELECT fkOrderID 
							FROM LineItem LI
							INNER JOIN WorkOrder WO
							ON LI.pkLineItemID = WO.fkLineItemID
							WHERE WO.pkWorkOrderID = WorkOrderID);

	SET			COID = Last_Insert_ID();


INSERT INTO	LineItem
				(fkOrderID, fkPartTemplateID, fkEngineeringTemplateID, CustomerReference, ESLQuoteNumber)
	SELECT		COID, fkPartTemplateID, fkEngineeringTemplateID, CustomerReference, ESLQuoteNumber	

	FROM		LineItem

	WHERE		pkLineItemID = (SELECT fkLineItemID
								FROM	WorkOrder
								WHERE 	pkWorkOrderID = WorkOrderID);


END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcReserveMaterial` (IN `UserID` SMALLINT UNSIGNED, `GRBNo` INT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED, `Qty` SMALLINT UNSIGNED)  BEGIN
INSERT INTO		Allocated
				(fkOperationID, fkGRBID, Display, Print, Quantity, AddedByfkUserID, AllocateDate)
VALUES			((SELECT		O.pkOperationID
					FROM		Operation O
					INNER JOIN	OperationMaterial OM
					ON			O.pkOperationID = OM.pkOperationID

					WHERE		O.fkWorkOrderID = WorkOrderID),
																GRBNo, NULL, NULL, Qty, UserID, DATE(Now()));

UPDATE			Material
SET				Quantity = Quantity - Qty

WHERE			fkGRBID = GRBNo;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRFQSearchList` (IN `RFQSearchField` VARCHAR(5))  BEGIN
SELECT		pkRFQOID AS RFQNo

FROM		RFQOrder

WHERE		CAST(pkRFQOID AS char) LIKE CONCAT(RFQSearchField, "%");
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRmvCustDeliveryPoint` (IN `UserID` TINYINT UNSIGNED, `DeliveryPointID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		DeliveryPoint
SET			Deleted = 1, DeletedByfkUserID = UserID

WHERE		pkDeliveryPointID = DeliveryPointID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRmvDeliveryPointContact` (IN `UserID` TINYINT UNSIGNED, `DCID` SMALLINT UNSIGNED)  BEGIN
DELETE FROM		DeliveryContact 
			
WHERE			pkDeliveryContactID = DCID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRmvGRBInvoice` (IN `UserID` SMALLINT UNSIGNED, `InvoiceID` INT UNSIGNED, `GRBID` INT UNSIGNED)  BEGIN
DECLARE 		Result		tinyint DEFAULT -1;

DELETE FROM		InvoiceGoods

WHERE			fkInvoiceID = InvoiceID
AND				fkGRBID = GRBID;

DELETE FROM		POInvoice

WHERE			fkInvoiceID = InvoiceID
AND				fkItemID = (SELECT MIN(fkItemID) FROM POGoods WHERE fkGRBID = GRBID);

SELECT			Row_Count() INTO Result;

SELECT 			Result;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRmvOperation` (IN `OperationID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
DELETE FROM Kythera.Operation 
WHERE pkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRmvSupplierApproval` (IN `UserID` SMALLINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `ApprovalID` TINYINT UNSIGNED)  BEGIN
DELETE FROM		SupplierApproval

WHERE			fkSupplierID = SupplierID
AND				fkApprovalID = ApprovalID;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRmvSupplierNADCAP` (IN `UserID` SMALLINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `ApprovalID` TINYINT UNSIGNED)  BEGIN

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
DELETE FROM		SupplierNADCAP

WHERE			fkSupplierID = SupplierID
AND				fkApprovalID = ApprovalID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRmvTemplateOp` (IN `UserID` SMALLINT UNSIGNED, `OperationTemplateID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

DELETE FROM 			OperationTemplate 

WHERE 					pkOperationTemplateID = OperationTemplateID;

DELETE FROM 			TemplateTreatment

WHERE 					pkOperationTemplateID = OperationTemplateID;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRmvWOOperation` (IN `OperationID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
DELETE FROM Kythera.Operation 
WHERE pkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRmvWOTemplateOp` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

DELETE FROM 			Operation 

WHERE 					pkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRouteCard` (IN `WorkOrderID` INT UNSIGNED)  BEGIN

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		CONCAT('Part No. ',P.PartNumber) AS PartNumber, P.IssueNumber AS PartIssue, CONCAT('"', P.Description, '"') AS Description, 
			D.Reference, D.IssueNumber AS DRGIssue, O.fkWorkOrderID AS pkWorkOrderID,
			O.pkOperationID, ifnull(M.Description, OT.OpType) AS MachiningOp, O.fkOperationTypeID AS OpTypeID,
			U.pkUserID AS InspectorID, U.Firstname AS Inspector1st, U.Lastname AS Inspector2nd,
			I.Outcome, IF(O.fkOperationTypeID <> 3, MC.Manufacturer, TL.Description) AS Manufacturer, 
			IF(O.fkOperationTypeID <> 3, MC.Model, '') AS Model, A.fkGRBID AS BatchNo, 
			QuantityReqd + QuantityOvers AS Qty, GI.IRNoteNumber


FROM		Operation O 
LEFT JOIN	WorkOrder WO
	ON		O.fkWorkOrderID = WO.pkWorkOrderID
LEFT JOIN	LineItem LI
	ON		WO.fkLineItemID = LI.pkLineItemID
LEFT JOIN	PartTemplate P
	ON		LI.fkPartTemplateID = P.pkPartTemplateID
LEFT JOIN	Drawing D
	ON		P.fkDrawingID = D.pkDrawingID

LEFT JOIN	OperationMachine OMC
	ON		O.pkOperationID = OMC.fkOperationID
LEFT JOIN	Machine MC
	ON		OMC.fkMachineID = MC.pkMachineID

LEFT JOIN	OperationInspect OI
	ON		O.pkOperationId = OI.fkOperationID
LEFT JOIN	Inspection I
	ON		OI.fkInspectionID = I.pkInspectionID
LEFT JOIN	User U
	ON		I.fkUserID = U.pkUserID

LEFT JOIN	Allocated A
	ON		O.pkOperationID = A.fkOperationID

LEFT JOIN	OperationMachining OM
	ON		O.pkOperationID = OM.fkOperationID
LEFT JOIN	MachiningType M
	ON		OM.fkMachiningListID = M.pkMachiningTypeID

LEFT JOIN	OperationType OT
	ON		O.fkOperationTypeID = OT.pkOperationTypeID

LEFT JOIN	GoodsIn GI
	ON		A.fkGRBID = GI.pkGRBID

LEFT JOIN	OperationTreatment OTR
	ON		O.pkOperationID = OTR.pkOperationID
LEFT JOIN	TreatmentList TL
	ON		OTR.fkTreatmentID = TL.pkTreatmentID

WHERE		O.fkWorkOrderID = WorkOrderID
ORDER BY	O.pkOperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRouteCard2` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		CONCAT('Part No. ',P.PartNumber) AS PartNumber, P.IssueNumber AS PartIssue, CONCAT('"', P.Description, '"') AS Description, 
			D.Reference, D.IssueNumber AS DRGIssue, O.fkWorkOrderID AS pkWorkOrderID,
			O.pkOperationID, ifnull(M.Description, OT.OpType) AS MachiningOp, O.fkOperationTypeID AS OpTypeID,
			U.pkUserID AS InspectorID, U.Firstname AS Inspector1st, U.Lastname AS Inspector2nd,
			I.Outcome, MC.Manufacturer, MC.Model, A.fkGRBID AS BatchNo, QuantityReqd + QuantityOvers AS Qty, GI.IRNoteNumber,
			ONO.Note,
			MS.MaterialSpec, S.Description AS Shape, UOM.Description AS UOM, 
			AP.Description AS Approval, CONCAT(Size, IF(UnitID = 1, 'mm', IF(UnitID = 2, 'Inches','Err'))) AS size, OM.Quantity, 
			OM.PartLength AS IssueLgth

FROM		Operation O 
LEFT JOIN	WorkOrder WO
	ON		O.fkWorkOrderID = WO.pkWorkOrderID
LEFT JOIN	LineItem LI
	ON		WO.fkLineItemID = LI.pkLineItemID
LEFT JOIN	PartTemplate P 
	ON		LI.fkPartTemplateID = P.pkPartTemplateID
LEFT JOIN	Drawing D
	ON		P.fkDrawingID = D.pkDrawingID

LEFT JOIN	OperationMachine OMC
	ON		O.pkOperationID = OMC.fkOperationID
LEFT JOIN	Machine MC
	ON		OMC.fkMachineID = MC.pkMachineID

LEFT JOIN	OperationInspect OI
	ON		O.pkOperationId = OI.fkOperationID
LEFT JOIN	Inspection I
	ON		OI.fkInspectionID = I.pkInspectionID
LEFT JOIN	User U
	ON		I.fkUserID = U.pkUserID

LEFT JOIN	Allocated A
	ON		O.pkOperationID = A.fkOperationID

LEFT JOIN	OperationMachining OMA
	ON		O.pkOperationID = OMA.fkOperationID
LEFT JOIN	MachiningType M
	ON		OMA.fkMachiningListID = M.pkMachiningTypeID

LEFT JOIN	OperationType OT
	ON		O.fkOperationTypeID = OT.pkOperationTypeID

LEFT JOIN	OperationNote ONO
	ON		O.pkOperationID = ONO.fkOperationID

LEFT JOIN	OperationMaterial OM
	ON		O.pkOperationID = OM.pkOperationID
LEFT JOIN	MaterialSpec	MS
ON			OM.fkMaterialSpecID = MS.pkMaterialSpecID
LEFT JOIN	Shape S
ON			OM.fkShapeID = S.pkShapeID
LEFT JOIN	UnitOfMeasure UOM
ON			OM.SPUfkUOMID = UOM.pkUOMID
LEFT JOIN	Approval AP
ON			OM.fkApprovalID = AP.pkApprovalID
LEFT JOIN	Allocated AL
ON			OM.pkOperationID = AL.fkOperationID
LEFT JOIN	GoodsIn GI
ON			AL.fkGRBID = GI.pkGRBID

WHERE		O.fkWorkOrderID = WorkOrderID
ORDER BY	O.pkOperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRouteCardFixture` (IN `WorkOrderID` INT)  BEGIN
SELECT f.grbnumber, f.fixturenumber, f.description FROM part p inner join fixture f on p.pkpartid =  f.fkpartid
inner join lineitem li on p.pkpartid = li.fkpartid 
inner join workorder wo on li.pklineitemid = wo.fklineitemid 
inner join operation o on wo.pkworkorderid = o.fkworkorderid
where wo.pkworkorderid = WorkOrderID and o.fkoperationtypeid = 2
order by o.pkoperationid;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRouteCardMaterial` (IN `OperationID` INT UNSIGNED)  BEGIN
SELECT 		pkOperationID, MS.MaterialSpec, S.Description AS Shape, UOM.Description AS UOM, 
			A.Description AS Approval, Size, OM.Quantity, PartLength AS IssueLgth

FROM		OperationMaterial OM
INNER JOIN	MaterialSpec	MS
ON			OM.fkMaterialSpecID = MS.pkMaterialSpecID
INNER JOIN	Shape S
ON			OM.fkShapeID = S.pkShapeID
INNER JOIN	UnitOfMeasure UOM
ON			OM.SPUfkUOMID = UOM.pkUOMID
INNER JOIN	Approval A
ON			OM.fkApprovalID = A.pkApprovalID
LEFT JOIN	Allocated AL
ON			OM.pkOperationID = AL.fkOperationID
LEFT JOIN	GoodsIn GI
ON			AL.fkGRBID = GI.pkGRBID

WHERE		pkOperationID = OperationID
;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRouteCardNotes` (IN `OperationID` INT UNSIGNED)  BEGIN
SELECT 		fkOperationID, Note	

FROM		OperationNote

WHERE		fkOperationID = OperationID
AND			(fkNoteTypeID = 3 
OR			fkNoteTypeID = 6
OR			fkNoteTypeID = 8);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRouteCardOpProcess` ()  BEGIN
SELECT 		fkOperationID, ProcessNote	

FROM		OperationProcess

;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRouteCardOpProcessbullshit` ()  BEGIN
SELECT 		fkOperationID, ProcessNote	

FROM		OperationProcess;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRouteCardProcess` (IN `OperationID` INT UNSIGNED)  BEGIN
SELECT 		fkOperationID, ProcessNote	

FROM		OperationProcess

WHERE		fkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRouteCardProcessOld` (IN `WorkOrderID` INT UNSIGNED)  BEGIN
SELECT 		Note	

FROM		Operation O
LEFT JOIN	OperationNote ONT 
ON			O.pkOperationID = ONT.fkOperationID

WHERE		fkWorkOrderID = WorkOrderID
AND			fkNoteTypeID = IF(O.fkOperationTypeID = 1, 3,
								IF(O.fkOperationTypeID = 2, 8,
									IF(O.fkOperationTypeID = 3, 5, 10)));
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRouteCardPrograms` (IN `OperationID` INT UNSIGNED)  BEGIN
SELECT 		fkProgramNumber

FROM		OpMachineProgram

WHERE		fkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRouteCardTemplate` (IN `ETemplateID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		CONCAT('Part No. ',P.PartNumber) AS PartNumber, P.IssueNumber AS PartIssue, CONCAT('"', P.Description, '"') AS Description, 
			D.Reference, D.IssueNumber AS DRGIssue, O.fkWorkOrderID AS pkWorkOrderID,
			O.pkOperationID, ifnull(M.Description, OT.OpType) AS MachiningOp, O.fkOperationTypeID AS OpTypeID,
			U.pkUserID AS InspectorID, U.Firstname AS Inspector1st, U.Lastname AS Inspector2nd,
			I.Outcome, MC.Manufacturer, MC.Model, A.fkGRBID AS BatchNo, QuantityReqd + QuantityOvers AS Qty, GI.IRNoteNumber,
			ONO.Note,
			MS.MaterialSpec, S.Description AS Shape, UOM.Description AS UOM, 
			AP.Description AS Approval, CONCAT(Size, IF(UnitID = 1, 'mm', IF(UnitID = 2, 'Inches','Err'))) AS size, OM.Quantity, 
			OM.PartLength AS IssueLgth

FROM		Operation O 
LEFT JOIN	WorkOrder WO
	ON		O.fkWorkOrderID = WO.pkWorkOrderID
LEFT JOIN	LineItem LI
	ON		WO.fkLineItemID = LI.pkLineItemID
LEFT JOIN	PartTemplate P 
	ON		LI.fkPartTemplateID = P.pkPartTemplateID
LEFT JOIN	Drawing D
	ON		P.fkDrawingID = D.pkDrawingID

LEFT JOIN	OperationMachine OMC
	ON		O.pkOperationID = OMC.fkOperationID
LEFT JOIN	Machine MC
	ON		OMC.fkMachineID = MC.pkMachineID

LEFT JOIN	OperationInspect OI
	ON		O.pkOperationId = OI.fkOperationID
LEFT JOIN	Inspection I
	ON		OI.fkInspectionID = I.pkInspectionID
LEFT JOIN	User U
	ON		I.fkUserID = U.pkUserID

LEFT JOIN	Allocated A
	ON		O.pkOperationID = A.fkOperationID

LEFT JOIN	OperationMachining OMA
	ON		O.pkOperationID = OMA.fkOperationID
LEFT JOIN	MachiningType M
	ON		OMA.fkMachiningListID = M.pkMachiningTypeID

LEFT JOIN	OperationType OT
	ON		O.fkOperationTypeID = OT.pkOperationTypeID

LEFT JOIN	OperationNote ONO
	ON		O.pkOperationID = ONO.fkOperationID

LEFT JOIN	OperationMaterial OM
	ON		O.pkOperationID = OM.pkOperationID
LEFT JOIN	MaterialSpec	MS
ON			OM.fkMaterialSpecID = MS.pkMaterialSpecID
LEFT JOIN	Shape S
ON			OM.fkShapeID = S.pkShapeID
LEFT JOIN	UnitOfMeasure UOM
ON			OM.SPUfkUOMID = UOM.pkUOMID
LEFT JOIN	Approval AP
ON			OM.fkApprovalID = AP.pkApprovalID
LEFT JOIN	Allocated AL
ON			OM.pkOperationID = AL.fkOperationID
LEFT JOIN	GoodsIn GI
ON			AL.fkGRBID = GI.pkGRBID

WHERE		O.fkWorkOrderID = ETemplateID
ORDER BY	O.pkOperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRouteCardTemplate2` (IN `ETemplateID` SMALLINT UNSIGNED)  BEGIN

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		CONCAT('Part No. ',P.PartNumber) AS PartNumber, P.IssueNumber AS PartIssue, CONCAT('"', P.Description, '"') AS Description, 
			D.Reference, D.IssueNumber AS DRGIssue, 
						O.pkOperationTemplateID AS OperationID, ifnull(M.Description, OT.OpType) AS MachiningOp, O.fkOperationTypeID AS OpTypeID,
									IF(O.fkOperationTypeID <> 3, MC.Manufacturer, TL.Description) AS Manufacturer, 
			IF(O.fkOperationTypeID <> 3, MC.Model, '') AS Model, 
						ONO.Note, MS.MaterialSpec, S.Description AS Shape, UOM.Description AS UOM, 
			AP.Description AS Approval, CONCAT(Size, IF(UnitID = 1, 'mm', IF(UnitID = 2, 'Inches','Err'))) AS size, 
						OM.PartLength AS IssueLgth, OperationNumber

FROM		EngineeringTemplate ET 

INNER JOIN	OperationTemplate O
	ON		ET.pkEngineeringTemplateID = O.fkEngineeringTemplateID

INNER JOIN	PartTemplate P
	ON		ET.fkPartTemplateID = P.pkPartTemplateID
INNER JOIN	Drawing D
	ON		P.fkDrawingID = D.pkDrawingID

LEFT JOIN	OpTmpltMachineProgram OTMP
	ON		O.pkOperationTemplateID = OTMP.fkOperationTemplateID
LEFT JOIN	MachiningType M
	ON		O.fkMachiningID = M.pkMachiningTypeID
LEFT JOIN	Machine MC
	ON		OTMP.fkMachineID = MC.pkMachineID

INNER JOIN	OperationType OT
	ON		O.fkOperationTypeID = OT.pkOperationTypeID

LEFT JOIN	TemplateNote ONO
	ON		O.pkOperationTemplateID = ONO.fkOperationTemplateID

LEFT JOIN	TemplateMaterial OM
	ON		O.pkOperationTemplateID = OM.pkOperationTemplateID

LEFT JOIN	MaterialSpec	MS
ON			OM.fkMaterialSpecID = MS.pkMaterialSpecID
LEFT JOIN	Shape S
ON			OM.fkShapeID = S.pkShapeID
LEFT JOIN	UnitOfMeasure UOM
ON			OM.SPUfkUOMID = UOM.pkUOMID
LEFT JOIN	Approval AP
ON			OM.fkApprovalID = AP.pkApprovalID

LEFT JOIN	TemplateTreatment OTR
	ON		O.pkOperationTemplateID = OTR.pkOperationTemplateID
LEFT JOIN	TreatmentList TL
	ON		OTR.fkTreatmentID = TL.pkTreatmentID

WHERE		O.fkEngineeringTemplateID = ETemplateID
ORDER BY	O.OperationNumber;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRouteCardTmpltProcess` ()  BEGIN
SELECT 		fkOperationTemplateID, ProcessNote	

FROM		TemplateProcess;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRouteCardTool` (IN `WorkOrderID` INT)  BEGIN
SELECT distinct t.grbnumber, t.serialnumber, t.description, TT.TType, TST.SubType , T.ToolStatus 
FROM part p 
inner join tool t on p.pkpartid =  t.fkpartid
inner join lineitem li on p.pkpartid = li.fkpartid 
inner join workorder wo on li.pklineitemid = wo.fklineitemid 
inner join operation o on wo.pkworkorderid = o.fkworkorderid

INNER JOIN	ToolType TT
	ON		T.fkTypeID = TT.pkTypeID
INNER JOIN	ToolSubType TST
	ON		T.fkSubTypeID = TST.pkSubTypeID
where wo.pkworkorderid = WorkOrderID and o.fkoperationtypeid = 2
order by o.pkoperationid;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRptInspectRejectAdvice` (IN `RejectID` SMALLINT UNSIGNED)  BEGIN

SELECT		RG.pkRejectID AS RejectNoteID, O.fkWorkOrderID AS WorkOrderNo, NULL AS AdviceNoteNo,
			GI.IRNoteNumber AS CertificateNo, Date(GI.ReceivedDate) AS Date,
			IF(DeviationClauseID = 1, 'Damaged', 
				IF(DeviationClauseID = 2, 'Material issue',
					IF(DeviationClauseID = 3, 'Quantity issue',
						IF(DeviationClauseID = 4, 'Dimensional issue',
							IF(DeviationClauseID = 5, 'Treatment issue',
								IF(DeviationClauseID = 6, 'Paperwork Issue', 
						IF(RejectClauseID = 1, 'Rectification', 
				IF(RejectClauseID = 2, 'Replacement',
					IF(RejectClauseID = 3, 'Credit only',
						IF(RejectClauseID = 4, 'Information only','Other')))))))))) AS RejectClause,
						RG.Deviation, 'Not Set' AS RootCause, 'Not Set' AS CorrectiveAction, 'Not Set' AS ActionPrevent,
			UName AS ProposedBy, RG.RejectedDate AS EffectiveDate

FROM		RejectGRB				RG
INNER JOIN	POGoods					PG
ON			RG.fkGRBID = PG.fkGRBID
LEFT JOIN	GoodsIn GI
ON			RG.fkGRBId = GI.pkGRBID

INNER JOIN	OperationOrderItem		OOI
ON			PG.fkItemID = OOI.fkItemID
INNER JOIN	Operation				O
ON			OOI.fkOperationID = O.pkOperationID
LEFT JOIN	User					U
ON			RG.RejectedByfkUserID = U.pkUserID

WHERE		pkRejectID = RejectID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcRptLate` ()  BEGIN
SELECT			WO.pkWorkOrderID, WO.OrigDeliveryDate, WO.NewDeliveryDate
FROM			WorkOrder	WO
WHERE			WO.Live = 1
AND				WO.NewDeliveryDate > WO.OrigDeliveryDate;

				
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSCApprovalComments` (IN `CommentSearch` VARCHAR(45))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		Comments

FROM		Comments

WHERE		Deleted <> 1 OR Deleted IS NULL
AND			Comments LIKE CONCAT(CommentSearch, '%');
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcScheduleClash` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

SELECT		fkWorkOrderID, ScheduledStartDate, EstimatedSetTime, EstimatedRunTime, Manufacturer, Model,
			DATE_ADD(ScheduledStartDate, 
			INTERVAL (EstimatedSetTime + EstimatedRunTime*(QuantityReqd + QuantityOvers)) MINUTE) AS EndTime, 
			QuantityReqd, QuantityOvers

FROM		WorkOrder WO
INNER JOIN	Operation O 
ON 			WO.pkWorkOrderID = O.fkWorkOrderID
INNER JOIN	OperationMachine OM 
ON 			O.pkOperationID = OM.fkOperationID
INNER JOIN	Machine M 
ON 			OM.fkMachineID = M.pkMachineID

WHERE		WO.Live = 1 AND (WO.complete <> 1 OR ISNULL(WO.Complete) = true)
	AND		(O.Bypass <> 1 OR ISNULL(O.Bypass) = TRUE)
	AND		(WO.PreCancel <> 1 OR ISNULL(WO.PreCancel) = TRUE)

GROUP BY	fkMachineID, EndTime;		
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSearchApprovalComments` (IN `CommentSearch` VARCHAR(80))  BEGIN
SELECT		Comments

FROM		Comments

WHERE		Comments LIKE CONCAT(CommentSearch, "%")
;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSearchMaterial` (IN `MaterialDescription` CHAR(20))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT * FROM Material WHERE Description LIKE CONCAT('%',MaterialSpec,'%');
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSearchMaterialInStock` (IN `GRBNo` INT UNSIGNED, `Spec` VARCHAR(6), `Size` DECIMAL(5,2) UNSIGNED)  BEGIN

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

SELECT 		GI.pkGRBID AS GRBNo, MS.MaterialSpec AS Spec, M.Size, IF(M.UnitID = 1,"mm","inches") AS Units, S.Description AS Shape, 
			APP.Description AS ReleaseLevel, M.Length AS PurchaseLength, GI.ReceivedDate AS DateIn, M.Quantity AS OrderQty, 
			CONCAT(LEFT(SA.StoreArea, 1), '-', XCoord, YCoord) AS Location,
			M.RcvdQuantity - (SELECT SUM(Quantity) 
								FROM Allocated 
								WHERE fkGRBID = M.fkGRBID) AS InStock

FROM 		GoodsIn GI
INNER JOIN	Material M
ON			GI.pkGRBID = M.fkGRBID
LEFT JOIN	MaterialSpec MS
ON			M.fkMaterialSpecID = MS.pkMaterialSpecID
LEFT JOIN	Shape S
ON			M.fkShapeID = S.pkShapeID
LEFT JOIN	Approval APP
ON			M.fkApprovalLevelID = APP.pkApprovalID
INNER JOIN	Allocated A
ON			M.fkGRBID = A.fkGRBID
LEFT JOIN	MaterialLocation ML
ON			M.fkGRBID = ML.fkMaterialID

LEFT JOIN	StoreArea SA
ON			ML.fkStoreAreaID = SA.pkStoreAreaID
LEFT JOIN	XCoordinate		X
ON			ML.fkXCoordID = X.pkXCoordID
LEFT JOIN	YCoordinate		Y
ON			ML.fkYCoordID = Y.pkYCoordID

WHERE 		IF(GRBNo >0,
				(IF(Spec IS NOT NULL,
					IF(Size >0,
						GI.pkGRBID LIKE CONCAT(GRBNo, '%')
						AND MS.MaterialSpec LIKE CONCAT(Spec, '%')
						AND M.Size >= Size,
						GI.pkGRBID LIKE CONCAT(GRBNo, '%')
						AND MS.MaterialSpec LIKE CONCAT(Spec, '%')
					),
					IF(Size >0,
						GI.pkGRBID LIKE CONCAT(GRBNo, '%')
						AND M.Size >= Size,
						GI.pkGRBID LIKE CONCAT(GRBNo, '%')
				))),
				(IF(Spec IS NOT NULL,
					IF(Size >0,
						MS.MaterialSpec LIKE CONCAT(Spec, '%')
						AND M.Size >= Size,
						MS.MaterialSpec LIKE CONCAT(Spec, '%')
					),
					M.Size >= Size)
				));
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSearchMaterialSpec` (IN `MaterialSpec2` VARCHAR(6))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkMaterialSpecID AS SpecID, MaterialSpec

FROM 		MaterialSpec 

WHERE 		MaterialSpec LIKE CONCAT(MaterialSpec2,'%');
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSearchPOComments` (IN `CommentSearch` VARCHAR(80))  BEGIN
SELECT		Comments

FROM		Comments

WHERE		Comments LIKE CONCAT(CommentSearch, "%")
;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSelectTemplateMachine` (IN `UserID` SMALLINT UNSIGNED, `OperationTemplateID` MEDIUMINT UNSIGNED, `MachineID` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

IF		OperationTemplateID NOT IN (SELECT fkOperationTemplateID FROM OpTmpltMachineProgram)
THEN
		INSERT INTO OpTmpltMachineProgram
					(fkOperationTemplateID, fkMachineID)
		VALUES		(OperationTemplateID, MachineID);

ELSE
		UPDATE		OpTmpltMachineProgram
		SET			fkMachineID = MachineID

		WHERE		fkOperationTemplateID = OperationTemplateID;

END IF;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSelectWOTemplateMachine` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `MachineID` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

IF		OperationID NOT IN (SELECT fkOperationID FROM OperationMachine)
THEN
		INSERT INTO OperationMachine
					(fkOperationID, fkMachineID)
		VALUES		(OperationID, MachineID);

ELSE
		UPDATE		OperationMachine
		SET			fkMachineID = MachineID

		WHERE		fkOperationID = OperationID;

END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSetWOCancel` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN

UPDATE		WorkOrder
SET			PreCancel = 1

WHERE		pkWorkOrderID = WorkOrderID;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSubconList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

SELECT 	pkTreatmentID AS TreatmentID, Description

FROM 		TreatmentList

WHERE		Deleted <> 1 OR Deleted IS NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSubConQPO` (IN `TreatmentID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT DISTINCT		S.SupplierName, TL.Description, NA.Description AS ReleaseDescription, ST.Quantity, ST.Price

FROM		SupplierTreatment ST
LEFT JOIN	OperationTreatment OT
ON			ST.fkOperationID = OT.pkOperationID
LEFT JOIN	Supplier S
ON			ST.fkSupplierID = S.pkSupplierID
LEFT JOIN	TreatmentList TL
ON			OT.fkTreatmentID = TL.pkTreatmentID
LEFT JOIN	SupplierNADCAP SN
ON			S.pkSupplierID = SN.fkSupplierID
LEFT JOIN	NADCAPApproval NA
ON			SN.fkApprovalID = NA.pkApprovalID

WHERE		OT.fkTreatmentID = TreatmentID
AND			NA.Description IS NOT NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSubconStandard` (IN `CustomerID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		C.fkApprovalID

FROM 		Customer C
WHERE 		C.pkCustomerID = CustomerID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSubconStandardList` ()  BEGIN
SELECT 		pkApprovalID, Description 
FROM 		NadcapApproval
WHERE		Deleted = 0;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSupplierDetail` (IN `SupplierID` SMALLINT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkSupplierID, SupplierName, Alias, Address1,
			Address2, Address3, Address4, Postcode, Phone, Facsimile, Website, Email,
			MinOrderCharge, PaymentTerms, MaterialSupplier, TreatmentSupplier, 
			CalibrationSupplier, ToolingSupplier, MiscSupplier
FROM		Supplier
WHERE		Deleted <> 1 AND pkSupplierID = SupplierId;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSupplierList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		S.pkSupplierID AS SupplierID, S.SupplierName 

FROM 		Supplier S

WHERE 		Deleted <> 1 OR Deleted IS NULL
ORDER BY	SupplierName;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSupplierOrderHistory` (IN `SupplierID` SMALLINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		DISTINCT P.pkPOnID AS 'PONo.', I.RcvdDate, fkUserID 

FROM		RFQOSupplier RS
INNER JOIN	RFQOrder RO
ON			RS.fkRFQOID = RO.pkRFQOID
INNER JOIN	POn P
ON			RO.pkRFQOID = P.fkRFQOID
INNER JOIN	OrderItem OI
ON			RO.pkRFQOID = OI.fkRFQOID
INNER JOIN	POInvoice PI
ON			OI.pkItemID = PI.fkItemID
INNER JOIN	Invoice I
ON			PI.fkInvoiceID = I.pkInvoiceID

WHERE		I.RcvdDate IS NOT NULL
AND			RS.fkSupplierID = SupplierID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSupplierOrderHistoryLink` (IN `PONumber` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		DISTINCT WO.pkWorkOrderID, PT.PartNumber, WO.QuantityReqd + WO.QuantityOvers AS 'Qty' 

FROM		OperationPO OP
INNER JOIN	Operation O
ON			OP.fkOperationID = O.pkOperationID
INNER JOIN	WorkOrder WO
ON			O.fkWorkOrderID = WO.pkWorkOrderID
INNER JOIN	LineItem LI
ON			WO.fkLineItemID = LI.pkLineItemID
INNER JOIN	PartTemplate PT
ON			LI.fkPartTemplateID = PT.pkPartTemplateID ;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSupplierSearch` (IN `SupplierString` CHAR(50))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		SupplierName
FROM		Supplier

WHERE		Deleted <> 1 AND SupplierName LIKE SupplierString;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSwapOps` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `OperationID2` INT UNSIGNED)  BEGIN
DECLARE		OpStore tinyint DEFAULT 0;
DECLARE		OpStore2 tinyint DEFAULT 0;

SET			OpStore = (SELECT OperationNumber FROM Operation WHERE pkOperationID = OperationID);
SET			OpStore2 = (SELECT OperationNumber FROM Operation WHERE pkOperationID = OperationID2);

UPDATE		Operation
SET			OperationNumber = OpStore2
WHERE		pkOperationID = OperationID;

UPDATE		Operation
SET			OperationNumber = OpStore
WHERE		pkOperationID = OperationID2;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcSystemRoles` ()  BEGIN
SELECT		D.pkDeptID AS RoleID, D.Description AS Role

FROM		User U
INNER JOIN	UserDept UD
ON			pkUserID = UD.fkUserID
INNER JOIN	Dept D
ON			UD.fkDeptID = D.pkDeptID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcTmpltApprovalComments` (IN `OperationTemplateID` MEDIUMINT)  BEGIN
SELECT		pkTemplateApprovalCommentID AS CommentID, Comment
FROM		TemplateApprovalComments
WHERE		fkOperationTemplateID = OperationTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcTmpltPOComments` (IN `OperationTemplateID` MEDIUMINT)  BEGIN
SELECT		pkTemplatePOCommentID AS CommentID, Comment
FROM		TemplatePOComments
WHERE		fkOperationTemplateID = OperationTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcToolList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 	T.GRBNumber, T.SerialNumber, T.Description

FROM 		Tool	T;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateADD` (IN `ADDID` MEDIUMINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
IF			(SELECT AutoPrint FROM AssociatedPartDoc WHERE pkAPDID = ADDID) = 1 THEN
	
	UPDATE		AssociatedPartDoc
	SET			AutoPrint = 0 WHERE pkAPDID = ADDID;
ELSE 

	UPDATE		AssociatedPartDoc
	SET			AutoPrint = 1 WHERE pkAPDID = ADDID;

END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateCO` (IN `UserID` SMALLINT UNSIGNED, `CustomerOrderNo` MEDIUMINT UNSIGNED, `OrderRef` VARCHAR(25), `ReleaseTypeID` TINYINT UNSIGNED, `DeliveryPointID` SMALLINT UNSIGNED, `CommerciallyViable` TINYINT UNSIGNED, `AmendmentReviewed` TINYINT UNSIGNED)  BEGIN
UPDATE		CustomerOrder
SET			CustomerOrderRef = OrderRef, fkApprovalID = ReleaseTypeID, fkDeliveryPointID = DeliveryPointID, 
			CommerciallyViable = CommerciallyViable, AmendmentReviewed = AmendmentReviewed
WHERE		pkOrderID = CustomerOrderNo;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateCustDeliveryPoint` (IN `UserID` TINYINT, `DPID` SMALLINT, `Address1` VARCHAR(45), `Address2` VARCHAR(40), `Address3` VARCHAR(40), `Address4` VARCHAR(30), `Postcode` CHAR(8))  BEGIN

UPDATE		DeliveryPoint
SET			Address1 = Address1, Address2 = Address2, Address3 = Address3, Address4 = Address4,
			Postcode = Postcode
WHERE		pkDeliveryPointID = DPID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateCustomerContact` (IN `UserID` TINYINT UNSIGNED, `ContactID` SMALLINT UNSIGNED, `ContactName` VARCHAR(45), `Position` VARCHAR(25), `Phone` VARCHAR(13), `Extension` SMALLINT UNSIGNED, `Email` VARCHAR(50), `AcknowContact` TINYINT UNSIGNED, `PrimaryContact` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		CustomerContact
SET			ContactName = ContactName, Position = Position, Phone = Phone, Extension = Extension, 
			Email = Email, AcknowContact = AcknowContact, PrimaryContact = PrimaryContact, fkUserIDUpdated = UserID 
			
WHERE		pkContactID = ContactID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateCustomerDetail` (IN `UserID` SMALLINT UNSIGNED, `CustomerID` SMALLINT UNSIGNED, `CustomerSageCode` CHAR(3), `CustomerName` VARCHAR(50), `Alias` VARCHAR(30), `Address1` VARCHAR(45), `Address2` VARCHAR(40), `Address3` VARCHAR(40), `Address4` VARCHAR(30), `Postcode` CHAR(8), `Phone` VARCHAR(20), `Facsimile` VARCHAR(20), `Website` VARCHAR(40), `Email` VARCHAR(50), `InvAddress1` VARCHAR(45), `InvAddress2` VARCHAR(40), `InvAddress3` VARCHAR(40), `InvAddress4` VARCHAR(30), `InvPostcode` CHAR(8), `FDApproval` TINYINT UNSIGNED, `MDApproval` TINYINT UNSIGNED, `GMApproval` TINYINT UNSIGNED, `CreditLimit` DECIMAL(6,0), `MarkUpQuote` TINYINT UNSIGNED, `PaymentTerms` TINYINT UNSIGNED, `PaymentRunFrequency` TINYINT UNSIGNED, `fkApprovalID` TINYINT UNSIGNED, `DeliveryCharge` DECIMAL(3,0) UNSIGNED, `IrishCurrency` TINYINT UNSIGNED, `USCurrency` TINYINT UNSIGNED, `EuroCurrency` TINYINT UNSIGNED, `Day1Open` TIME, `Day1Close` TIME, `Day2Open` TIME, `Day2close` TIME, `Day3Open` TIME, `Day3Close` TIME, `Day4Open` TIME, `Day4Close` TIME, `Day5Open` TIME, `Day5Close` TIME, `Day6Open` TIME, `Day6Close` TIME, `Day7Open` TIME, `Day7Close` TIME, `Deleted` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		Customer 
SET			CustomerSageCode = CustomerSageCode, CustomerName = CustomerName, Alias = Alias, Address1 = Address1,
			Address2 = Address2, Address3 = Address3, Address4 = Address4, Postcode = Postcode, Phone = Phone, 
			Facsimile = Facsimile, Website = Website, Email = Email, InvAddress1 = InvAddress1, InvAddress2 = InvAddress2, 
			InvAddress3 = InvAddress3, InvAddress4 = InvAddress4, InvPostcode = InvPostcode, FDApproval = FDApproval, 
			MDApproval = MDApproval, GMApproval = GMApproval, CreditLimit = CreditLimit, MarkUpQuote = MarkUpQuote, 
			PaymentTerms = PaymentTerms, PaymentRunFrequency = PaymentRunFrequency, InvAddress1 = InvAddress1, 
			InvAddress2 = InvAddress2, InvAddress3 = InvAddress3, InvAddress4 = InvAddress4, InvPostcode = InvPostcode, 
			fkApprovalID = FDApproval, DeliveryCharge = DeliveryCharge, IrishCurrency = IrishCurrency, USCurrency = USCurrency,
			EuroCurrency = EuroCurrency, Day1Open = Day1Open, Day1Close = Day1Close, Day2Open = day2Open, Day2Close = Day2Close,
			Day3Open = Day3Open, Day3Close = Day3Close, Day4Open = Day4Open, Day4Close = Day4Close, Day5Open = Day5Open,
			Day5Close = Day5Close, Day6Open = Day6Open, Day6Close = Day6Close, Day7Open = Day7Open, Day7Close = Day7Close,
			Deleted = Deleted
			
WHERE		pkCustomerID = CustomerID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateFat` (IN `UserID` SMALLINT UNSIGNED, `FATTypeID` TINYINT UNSIGNED, `FATDays` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE			FAT
SET				FatDays = FATDays
WHERE			FatType = FATTypeID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateFinanceAdmin` (IN `VATRate` TINYINT UNSIGNED, `FAIRCharge` TINYINT UNSIGNED, `MaterialMarkup` TINYINT UNSIGNED, `USD` DECIMAL(5,4) UNSIGNED, `Euros` DECIMAL(5,4) UNSIGNED, `IrishPounds` DECIMAL(5,4) UNSIGNED)  BEGIN
UPDATE Rate SET Rate = VATRate WHERE pkRateID = 1;

UPDATE Rate SET Rate = FAIRCharge WHERE pkRateID = 2;

UPDATE Rate SET Rate = MaterialMarkup WHERE pkRateID = 3;

UPDATE ExchangeRate SET Rate = USD WHERE pkExchangerateID = 1;

UPDATE ExchangeRate SET Rate = Euros WHERE pkExchangerateID = 2;

UPDATE ExchangeRate SET Rate = IrishPounds WHERE pkExchangerateID = 3;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateInspectionDetail` (IN `UserID` SMALLINT UNSIGNED, `GRBID` INT UNSIGNED, `AllOK` TINYINT UNSIGNED, `QtyRejected` SMALLINT UNSIGNED, `RejectReasonID` TINYINT UNSIGNED)  BEGIN
DECLARE ReceivedQuantity	smallint DEFAULT 0;

SET ReceivedQuantity = 		(SELECT RcvdQuantity 
							FROM Material
							WHERE fkGRBID = GRBID);

UPDATE			Inspection
SET				fkUserID = UserID, Outcome = IF(AllOK=1, 1, 0), ScrappedQty = IFNULL(QtyRejected,0)

WHERE			pkInspectionID = (SELECT fkInspectionID 
									FROM MaterialInspect 
									WHERE fkMaterialID = GRBID);

IF AllOK <> 1 THEN
		INSERT INTO		RejectGRB
						(fkGRBID, RejectQty, RejectedByfkUserID, DeviationClauseID)
		VALUES			(GRBID, 
																QtyRejected, UserID, RejectReasonID);

		SELECT 		Last_Insert_ID() AS RejectID;

END IF;

IF AllOK = 1 THEN
		INSERT INTO		Todo
						(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, PartNumber, Description, fkGRBID)
			VALUES		(Date(Now()), 2, UserID, 2, (SELECT WorkOrderID
													FROM GoodsIn
													WHERE pkGRBID = GRBID), NULL, "Issue Material", GRBID);

END IF;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateIRList` (IN `UserID` SMALLINT UNSIGNED, `GRBNo` INT UNSIGNED, `Display` TINYINT UNSIGNED, `Print` TINYINT UNSIGNED)  BEGIN
UPDATE		Allocated
SET			Display = Display, Print = Print

WHERE		fkGRBID = GRBNo;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateLineItem` (IN `UserID` TINYINT, `LineItemID` MEDIUMINT, `CustRef` VARCHAR(5), `QuoteNo` MEDIUMINT, `PTemplateID` SMALLINT, `ETemplateID` SMALLINT)  BEGIN
UPDATE			LineItem
SET				fkPartTemplateID = PTemplateID, fkEngineeringTemplateID = ETemplateID, CustomerReference = CustRef,
				ESLQuoteNumber = QuoteNo
WHERE			pkLineItemID = LineItemID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateMachineParams` (IN `MachineID` TINYINT UNSIGNED, `Manufacturer` VARCHAR(15), `Model` VARCHAR(25), `Trn` TINYINT UNSIGNED, `Grd` TINYINT UNSIGNED, `Mil` TINYINT UNSIGNED, `GC` TINYINT UNSIGNED, `Oth` TINYINT UNSIGNED, `Offset` TINYINT UNSIGNED, `StationNo` TINYINT UNSIGNED, `MinStickOut` TINYINT UNSIGNED, `Designation` VARCHAR(15), `HolderNo` VARCHAR(15), `Diameter` SMALLINT UNSIGNED, `InsertDatumPoint` VARCHAR(15), `MinFluteLength` SMALLINT UNSIGNED, `Spindle` VARCHAR(10), `DifficultyLevel` TINYINT UNSIGNED, `CoolantType` VARCHAR(15), `TargetConcMin` TINYINT UNSIGNED, `TargetConcMax` TINYINT UNSIGNED, `SlideOilLevel` TINYINT UNSIGNED, `CoolantLevel` TINYINT UNSIGNED, `GreaseLevel` TINYINT UNSIGNED, `FanFilter` TINYINT UNSIGNED, `OilFilter` TINYINT UNSIGNED, `CoolantFilter` TINYINT UNSIGNED, `AirSystem` TINYINT UNSIGNED, `IntegralLighting` TINYINT UNSIGNED, `InterlocksEngaged` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		Machine
SET			Manufacturer = Manufacturer, ChuckDiameter = Diameter, SkillOffset = DifficultyLevel 

WHERE		pkMachineID = MachineID;

UPDATE		MachineMaintParam
SET			StationNo = StationNo, MinStickOut = MinStickOut, Designation = Designation, HolderNo = HolderNo, 
			InsertDatumPoint = InsertDatumPoint, MinfluteLength = MinfluteLength,
			Spindle = Spindle, CoolantType = CoolantType, TargetConcMin = TargetConcMin, TargetConcMax = TargetConcMax, 
			SlideOilLevel = SlideOilLevel, CoolantLevel = CoolantLevel,
			GreaseLevel = GreaseLevel, Fanfilter = Fanfilter, OilFilter = OilFilter, CoolantFilter = CoolantFilter, 
			AirSystem = AirSystem, IntegralLighting = IntegralLighting, 
			InterlocksEngaged = InterlocksEngaged, Offset = Offset,
			Turn = Trn, Grind = Grd, Mill = Mil, GearCut = GC, Other = Oth

WHERE		pkMachineID = MachineID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateOpDetail` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `Bypass` TINYINT UNSIGNED, `Subcon` TINYINT UNSIGNED)  BEGIN

UPDATE		Operation
SET			Bypass = Bypass, Subcon = Subcon

WHERE		pkOperationID = OperationID;


INSERT INTO OperationTreatment
			(pkOperationID, AddedByfkUserID)
VALUES		(OperationID, UserID);

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateOpenHours` (IN `UserID` SMALLINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `Day1Open` TIME, `Day1Close` TIME, `Day2Open` TIME, `Day2Close` TIME, `Day3Open` TIME, `Day3Close` TIME, `Day4Open` TIME, `Day4Close` TIME, `Day5Open` TIME, `Day5Close` TIME, `Day6Open` TIME, `Day6Close` TIME, `Day7Open` TIME, `Day7Close` TIME)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		Supplier
SET			Day1Open = Day1Open, Day1Close = Day1Close, Day2Open = Day2Open, Day2Close = Day2Close, Day3Open = Day3Open, 
			Day3Close = Day3Close, Day4Open = Day4Open, Day4Close = Day4Close, Day5Open = Day5Open, Day5Close = Day5Close, 
			Day6Open = Day6Open, Day6Close = Day6Close, Day7Open = Day7Open, Day7Close = Day7Close

WHERE		pkSupplierID = SupplierID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateOpeningHours` (IN `UserID` SMALLINT UNSIGNED, `CustomerID` SMALLINT UNSIGNED, `Day1Open` TIME, `Day1Close` TIME, `Day2Open` TIME, `Day2Close` TIME, `Day3Open` TIME, `Day3Close` TIME, `Day4Open` TIME, `Day4Close` TIME, `Day5Open` TIME, `Day5Close` TIME, `Day6Open` TIME, `Day6Close` TIME, `Day7Open` TIME, `Day7Close` TIME)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		Customer
SET			Day1Open = Day1Open, Day1Close = Day1Close, Day2Open = Day2Open, Day2Close = Day2Close, Day3Open = Day3Open, 
			Day3Close = Day3Close, Day4Open = Day4Open, Day4Close = Day4Close, Day5Open = Day5Open, Day5Close = Day5Close, 
			Day6Open = Day6Open, Day6Close = Day6Close, Day7Open = Day7Open, Day7Close = Day7Close

WHERE		pkCustomerID = CustomerID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateOpInspectionDetail` (IN `UserID` SMALLINT UNSIGNED, `InspectionID` INT UNSIGNED, `InspectionDetail` VARCHAR(50), `QtyInspected` SMALLINT UNSIGNED, `QtyScrapped` SMALLINT UNSIGNED, `ReasonID` TINYINT UNSIGNED)  BEGIN
DECLARE		StartQty			smallint unsigned;
DECLARE		OpID				int unsigned;
UPDATE		Inspection
SET			fkUserID = UserID, Detail = InspectionDetail, InspectQty = QtyInspected, ScrappedQty = QtyScrapped,
			ScrapReasonID = ReasonID

WHERE		pkInspectionID = InspectionID;

SET			OpID = (SELECT fkOperationID
					FROM OperationInspect
					WHERE fkInspectionID = InspectionID);

SET			StartQty = (SELECT StartQty
						FROM Operation
						WHERE pkOperationID = OpID);

UPDATE		Operation
SET			EndQty = StartQty - QtyScrapped

WHERE		pkOperationID = OpID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateOpTmpltReviewDetail` (IN `UserID` SMALLINT UNSIGNED, `OperationTemplateID` MEDIUMINT, `Reviewstatus` TINYINT UNSIGNED, `Risk` TINYINT UNSIGNED, `Tooling` TINYINT UNSIGNED, `QAEquip` TINYINT UNSIGNED, `CorrectApproval` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		EngineeringTemplate

SET			ReviewStatus = ReviewStatus, RiskAssessment = Risk, Tooling = Tooling, QAEquipment = QAEquip, CorrectApproval = CorrectApproval

WHERE		pkEngineeringTemplateID = (SELECT fkEngineeringTemplateID 
										FROM OperationTemplate 
										WHERE pkOperationTemplateID = OperationTemplateID);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateOpTmpltSubconDetail` (IN `UserID` SMALLINT UNSIGNED, `OperationTemplateID` MEDIUMINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `LeadTime` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

UPDATE			TemplateTreatment
SET				LeadTime = LeadTime, 
				StandardSupplierID = SupplierID, AddedByfkUserID = UserID

WHERE			pkOperationTemplateID = OperationTemplateID;

SELECT			Row_Count() INTO Result;

SELECT			Result;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateOrderDocument` (IN `UserID` SMALLINT UNSIGNED, `DocumentID` INT UNSIGNED, `AutoPrint` TINYINT UNSIGNED)  BEGIN
UPDATE		WODocument
SET			Print = AutoPrint

WHERE		pkWODocumentID = DocumentID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateOrderItemDetail` (IN `UserID` SMALLINT UNSIGNED, `ItemID` INT UNSIGNED, `Qty` SMALLINT UNSIGNED, `SPU` TINYINT UNSIGNED, `SPUID` TINYINT UNSIGNED, `Size` DECIMAL(7,3) UNSIGNED, `UnitID` TINYINT UNSIGNED, `ShapeID` TINYINT UNSIGNED, `PartDescription` VARCHAR(45), `Comment` VARCHAR(45), `Price` DECIMAL(7,2) UNSIGNED, `Per` TINYINT UNSIGNED)  BEGIN
UPDATE		OrderItem
SET			fkPartTemplateID = (SELECT fkPartTemplateID FROM LineItem LI
								INNER JOIN WorkOrder WO
								ON LI.pkLineItemID = WO.fkLineItemID
								INNER JOIN Operation O
								ON WO.pkWorkOrderID = O.fkWorkOrderID
								INNER JOIN OperationOrderItem OOI
								ON O.pkOperationID = OOI.fkOperationID
								WHERE OOI.fkItemID = ItemID), fkShapeID = ShapeID, SPU = SPU, fkUOMID = SPUID, Quantity = Qty, 
			Description = PartDescription, Comment = Comment, Price = Price, Size = Size, UnitID = UnitID, Per = Per

WHERE		pkItemID = ItemID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdatePOSupplierDetail` (IN `UserID` SMALLINT UNSIGNED, `RFQOID` SMALLINT UNSIGNED, `SupplierRef` VARCHAR(45), `DeliveryCharge` DECIMAL(5,2) UNSIGNED, `CertCharge` DECIMAL(5,2) UNSIGNED, `TotalCost` DECIMAL(9,2) UNSIGNED, `ApprovalComment` VARCHAR(45), `DeliveryDays` SMALLINT UNSIGNED, `DeliveryDate` DATE, `DeliveryCommentID` SMALLINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `ContactID` SMALLINT UNSIGNED)  BEGIN
DECLARE		CommentIDStore	mediumint unsigned;
DECLARE		Result			tinyint DEFAULT -1;
DECLARE		Result2			tinyint DEFAULT -1;
DECLARE		Result3			tinyint DEFAULT -1;

SET 		CommentIDStore = (SELECT MIN(pkPOCommentID) FROM POComment WHERE fkRFQOID = RFQOID);

UPDATE		RFQOrder
SET			LeadDays = DeliveryDays, Reference = SupplierRef, DeliveryDate = DeliveryDate, 
			DeliveryCommentID = DeliveryCommentID

WHERE		pkRFQOID = RFQOID;

SELECT		Row_Count() INTO Result;


IF NOT EXISTS(SELECT * 
						FROM RFQOSupplier 
						WHERE fkRFQOID = RFQOID)

THEN		INSERT INTO RFQOSupplier
						(fkRFQOID, fkSupplierID, fkcontactID, Selected, TotalCost, DeliveryCost, CertCharge)
			VALUES		(RFQOID, SupplierID, ContactID, 1, TotalCost, DeliveryCharge, CertCharge);

			SELECT		Row_Count() INTO Result2;
ELSE

			UPDATE		RFQOSupplier
			SET			fkSupplierID = SupplierID, fkcontactID = ContactID, CertCharge = CertCharge, TotalCost = TotalCost, 
						DeliveryCost = DeliveryCharge

			WHERE		fkRFQOID = RFQOID AND Selected = 1;

			SELECT		Row_Count() INTO Result2;

END IF;


IF			NOT ISNULL(CommentIDStore)
THEN
			UPDATE		POComment
			SET			Comment = ApprovalComment

			WHERE		pkPOCommentID = (CommentIDStore);

			SELECT		Row_Count() INTO Result3;

ELSE
			INSERT INTO POComment
						(fkRFQOID, Comment, Deleted)
			VALUES		(RFQOID, ApprovalComment, 0);

			SELECT		Row_Count() INTO Result3;
END IF;


SELECT CONCAT(Result, ' ', Result2, ' ', Result3) AS Result;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateStaffDetail` (IN `UserID` SMALLINT UNSIGNED, `StaffUserID` SMALLINT UNSIGNED, `FirstName` VARCHAR(25), `LastName` VARCHAR(25), `Address1` VARCHAR(45), `Address2` VARCHAR(40), `Address3` VARCHAR(40), `Address4` VARCHAR(30), `Postcode` CHAR(8), `HomePhone` VARCHAR(20), `MobilePhone` VARCHAR(20), `Email` VARCHAR(50), `ChargeRate` DECIMAL(5,2), `SkillLevel` TINYINT UNSIGNED, `Employed` TINYINT UNSIGNED, `Extension` TINYINT UNSIGNED, `ReportsTo` SMALLINT UNSIGNED, `POLimit` SMALLINT UNSIGNED, `SpendLimit` SMALLINT UNSIGNED, `ForkOperator` TINYINT UNSIGNED, `FirstAid` TINYINT UNSIGNED, `LeaveDays` TINYINT UNSIGNED, `Salary` SMALLINT UNSIGNED, `Hours` TINYINT UNSIGNED, `Security` TINYINT UNSIGNED)  BEGIN
UPDATE				User
SET					UName = CONCAT(LEFT(FirstName,1), LEFT(LastName,9)), FirstName = FirstName, LastName = LastName, Address1 = Address1, 
					Address2 = Address2, Address3 = Address3, Address4 = Address4, Postcode = Postcode, HomePhone = HomePhone,
					MobilePhone = MobilePhone, Email = Email, ChargeRate = ChargeRate, SkillLevel = SkillLevel, Employed = Employed, 
					Extension = Extension, ReportsTo = ReportsTo, POLimit = POLimit, SpendLimit = SpendLimit,
					ForkOperator = ForkOperator, FirstAid = FirstAid, LeaveDays = LeaveDays, Salary = Salary, Hours = Hours, 
					Security = Security

WHERE				pkUserID = StaffUserID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateStockLocation` (IN `GRBNo` INT UNSIGNED, `StoreAreaID` TINYINT UNSIGNED, `Xlocation` TINYINT UNSIGNED, `Ylocation` CHAR(1))  BEGIN
DECLARE		XStore		smallint unsigned;
DECLARE		YStore		smallint unsigned;

SET XStore	=	(SELECT		pkXCoordID		
				FROM		XCoordinate

				WHERE		fkStoreAreaID = StoreAreaID AND XCoord = Xlocation);

SET YStore	=	(SELECT		pkYCoordID		
				FROM		YCoordinate

				WHERE		fkStoreAreaID = StoreAreaID AND YCoord = Ylocation);
IF
	XStore AND YStore IS NOT NULL
THEN
	UPDATE		MaterialLocation
	SET			fkStoreAreaID = StoreAreaID, fkXCoordID = XStore, fkYCoordID = YStore 

	WHERE		fkMaterialID = GRBNo;
ELSE
	SELECT 'non-existent Location';
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateSupplierContact` (IN `ContactID` SMALLINT, `ContactName` VARCHAR(45), `Phone` VARCHAR(13), `Email` VARCHAR(50), `Prime` TINYINT, `Deleted` TINYINT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		SupplierContact
SET			ContactName = ContactName, Phone = Phone, Email = Email, Prime = Prime, Deleted = Deleted 
			
WHERE		pkContactID = ContactID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateSupplierDetail` (IN `UserID` SMALLINT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `Supplier` VARCHAR(50), `Alias` VARCHAR(40), `Address1` VARCHAR(45), `Address2` VARCHAR(40), `Address3` VARCHAR(40), `Address4` VARCHAR(30), `Postcode` CHAR(8), `Phone` VARCHAR(20), `Facsimile` VARCHAR(20), `Website` VARCHAR(40), `Email` VARCHAR(50), `MinOrderCharge` SMALLINT UNSIGNED, `PaymentTerms` TINYINT UNSIGNED, `MaterialSupplier` TINYINT UNSIGNED, `TreatmentSupplier` TINYINT UNSIGNED, `CalibrationSupplier` TINYINT UNSIGNED, `ToolingSupplier` TINYINT UNSIGNED, `MiscSupplier` TINYINT UNSIGNED, `Current` TINYINT UNSIGNED)  BEGIN
UPDATE		Supplier
SET			SupplierName = Supplier, Alias = Alias, Address1 = Address1, Address2 = Address2, Address3 = Address3, 
			Address4 = Address4, Postcode = Postcode, Phone = Phone, Facsimile = Facsimile, Website = Website, Email = Email, 
			MinOrderCharge = MinOrderCharge, PaymentTerms = PaymentTerms, MaterialSupplier = MaterialSupplier,
			TreatmentSupplier = TreatmentSupplier, CalibrationSupplier = CalibrationSupplier, ToolingSupplier = ToolingSupplier, 
			MiscSupplier = MiscSupplier, fkUserID = UserID, Deleted = IF(Current = 1, 0, 1)

WHERE		pkSupplierID = SupplierID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdatetemplateDetail` (IN `UserID` SMALLINT, `EngineeringtemplateID` SMALLINT, `PlanningComplete` TINYINT)  BEGIN

UPDATE		EngineeringTemplate
SET			PlanningComplete = PlanningComplete

WHERE		pkEngineeringTemplateID = EngineeringTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateWO` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED, `PriceEach` DECIMAL(7,2) UNSIGNED, `Deliverydate` DATE, `Reqd` SMALLINT UNSIGNED, `Build` SMALLINT UNSIGNED, `ChangeReason` VARCHAR(30), `FAIR` TINYINT UNSIGNED)  BEGIN
DECLARE OversStore	smallint default 0;
SET OversStore  = Build - Reqd;
UPDATE			WorkOrder
SET				PriceEach = PriceEach, OrigDeliveryDate = Deliverydate, QuantityReqd = Reqd, QuantityOvers = OversStore, 
				ChangeReason = ChangeReason, FAIR = FAIR

WHERE			pkWorkOrderID = WorkOrderID;				
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateWOOpTmpltReviewDetail` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `Reviewstatus` TINYINT UNSIGNED, `Risk` TINYINT UNSIGNED, `Tooling` TINYINT UNSIGNED, `QAEquip` TINYINT UNSIGNED, `CorrectApproval` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE		WorkOrder

SET			ReviewStatus = ReviewStatus, RiskAssessment = Risk, Tooling = Tooling, QAEquipment = QAEquip, CorrectApproval = CorrectApproval

WHERE		pkWorkOrderID = (SELECT fkWorkOrderID 
										FROM Operation
										WHERE pkOperationID = OperationID);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateWOOpTmpltSubconDetail` (IN `UserID` SMALLINT UNSIGNED, `OperationID` INT UNSIGNED, `SupplierID` SMALLINT UNSIGNED, `LeadTime` TINYINT UNSIGNED)  BEGIN
DECLARE			Result	tinyint DEFAULT -1;
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

UPDATE			OperationTreatment
SET				LeadTime = LeadTime,
				AddedByfkUserID = UserID	

WHERE			pkOperationID = OperationID;

SELECT			Row_Count() INTO Result;


UPDATE			RFQOSupplier
SET				fkSupplierID = SupplierID

WHERE			fkRFQOID = (SELECT fkRFQOID 
							FROM OrderItem OI
							INNER JOIN OperationOrderItem OOI
							ON OI.pkItemID = OOI.fkItemID
							WHERE OOI.fkOperationID = OperationID)
AND				Selected = 1;


IF Result < 1
THEN
	SELECT		Result;
ELSE			
	SELECT		1;
END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateWOOutvoice` (IN `UserID` SMALLINT UNSIGNED, `Invoice` INT UNSIGNED, `DelCharge` DECIMAL(5,2) UNSIGNED, `ValueExVAT` DECIMAL(7,2) UNSIGNED, `WOComplete` TINYINT UNSIGNED)  BEGIN
UPDATE		Outvoice OV

INNER JOIN	WOOutvoice WOO	
ON			OV.pkInvoiceID = WOO.fkInvoiceID
INNER JOIN	WorkOrder WO
ON			WOO.fkWorkOrderID = WO.pkWorkOrderID

SET			DeliveryChg = DelCharge, OV.Complete = WOComplete, AmendedValue = ValueExVAT, AmendedByfkUserID = UserID

WHERE		pkInvoiceID = Invoice;


END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpdateWorkOrderCoCInfo` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED, `PaperworkComplete` TINYINT UNSIGNED)  BEGIN
UPDATE		WorkOrder WO
SET			PaperworkComplete = PaperworkComplete

WHERE		WO.pkWorkOrderID = WorkOrderID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUpIssuePartTemplate` (IN `UserID` SMALLINT UNSIGNED, `PTemplateID` SMALLINT UNSIGNED, `NewIssue` VARCHAR(5), `FileHandle` VARCHAR(45))  BEGIN

DECLARE	Result												tinyint DEFAULT -1;
DECLARE RefStore, MsgResult 								varchar(25);
DECLARE	CurrentDrgID, ParentTemplateID, NewTemplateID		smallint unsigned;
											
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;

IF (SELECT	DISTINCT row_count() FROM 		WorkOrder 	WO
						INNER JOIN LineItem	LI
						ON 			WO.fkLineItemID = LI.pkLineItemID
						WHERE 		LI.fkPartTemplateID = PTemplateID
						AND 		Live = 1
						AND 		(Complete = 0 OR Complete IS NULL)) > 0 
	OR (SELECT 		IF(OnAlert = 1, OnAlert, LockedDown)
		FROM			PartTemplate
		WHERE			pkPartTemplateID = PTemplateID) = 1 THEN

				SET		MsgResult = 'Error: Live WOs';
		SELECT	Result, MsgResult;

ELSE

				SET			CurrentDrgID = (SELECT		fkDrawingID 
									FROM 		PartTemplate
									WHERE		pkPartTemplateID = PTemplateID);

		INSERT INTO	PartTemplate
					(fkParentPartTemplateID, QtyToParent, fkDrawingID, PartNumber, IssueNumber,
					Description, OnAlert, LockedDown, AddedByfkUserID, StdLeadDays, StdPrice,
					StdApprovalID, PartLength, Width, Height, Weight, Finish)
		SELECT		fkParentPartTemplateID, QtyToParent, fkDrawingID, PartNumber, IssueNumber,
					Description, OnAlert, LockedDown, AddedByfkUserID, StdLeadDays, StdPrice,
					StdApprovalID, PartLength, Width, Height, Weight, Finish
		FROM		PartTemplate
		WHERE		pkPartTemplateID = PTemplateID;

		SET			NewTemplateID = Last_Insert_ID();

		UPDATE		PartTemplate 
		SET			AddedByfkUserID = UserID, IssueNumber = NewIssue

		WHERE		pkPartTemplateID = NewTemplateID;

							UPDATE		PartTemplate 
		SET			UpIssueDate = CurDate(), UpIssueUserID = UserID

		WHERE		pkPartTemplateID = PTemplateID;


				UPDATE		PartTemplate
		SET			OnAlert = 1

		WHERE		fkParentPartTemplateID = PTemplateID;

				UPDATE		PartTemplate
		SET			OnAlert = 1

		WHERE		fkDrawingID = CurrentDrgID;


		IF	FileHandle IS NOT NULL THEN

				SET RefStore = (SELECT Reference 
								FROM Drawing 
								WHERE pkDrawingID = (SELECT fkDrawingID 
													FROM PartTemplate 
													WHERE pkPartTemplateID = PTemplateID));

				INSERT INTO	Drawing
							(Reference, DateAdded, AddedByfkUserID, FileHandle, IssueNumber)
				VALUES		(RefStore, DATE(Now()), UserID, FileHandle, NewIssue);


								UPDATE		PartTemplate
				SET			fkDrawingID = Last_Insert_ID()

				WHERE		pkPartTemplateID = NewTemplateID;
		END IF;


		SET		Result = 1;
		SET		MsgResult = 'Part up-issued';
		SELECT 	Result, MsgResult;
END IF;

END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUseMaterialPrice` (IN `Price` DECIMAL, IN `OperationID` INT, IN `MaterialID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE Kythera.OperationMaterial SET ExpectedPrice = Price WHERE pkOperationID = OperationID AND pkMaterialSpecID = MaterialSpecID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUsePrice` (IN `OperationID` INT, IN `MaterialID` INT, IN `Price` DECIMAL)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO OperationMaterial (fkOperationID, fkMaterialID, ExpectedPrice) VALUES(OperationID, MaterialID, Price);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUserDept` (IN `UserID` SMALLINT UNSIGNED)  BEGIN

SELECT 		fkDeptID AS DeptID, D.Description
 
FROM 		User U
INNER JOIN	UserDept UD 
ON 			U.pkUserID = UD.fkUserID
INNER JOIN	Dept D
ON			UD.fkDeptID = D.pkDeptID

WHERE		fkUserID = UserID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUserLogin` (IN `UserName` VARCHAR(10), IN `UserPwd` VARCHAR(14))  BEGIN
SELECT 		pkUserID AS UserID, fkDeptID AS DeptID
FROM 		User U
INNER JOIN	UserDept UD ON U.pkUserID = UD.fkUserID

WHERE		UName = UserName AND pwd = md5(UserPwd)
AND			Employed <> 2
;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcUseTreamentPrice` (IN `Price` DECIMAL, IN `OperationID` INT, IN `TreatmentID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
UPDATE Kythera.OperationTreatment SET ExpectedPrice = Price WHERE pkOperationID = OperationID AND pkTreatmentID = TreatmentID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcViewADD` (IN `WorkOrderID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		FileHandle AS FileName

FROM			AssociatedPartDoc APD
INNER JOIN		PartTemplate PT
ON				APD.fkPartTemplateID = PT.pkPartTemplateID
INNER JOIN		LineItem LI
ON				PT.pkPartTemplateID = LI.fkPartTemplateID
INNER JOIN		WorkOrder WO
ON				LI.pkLineItemID = WO.fkLineItemID

WHERE			WO.pkWorkOrderID = WorkOrderID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcViewAllTmpltADD` (IN `PartTemplateID` SMALLINT UNSIGNED)  BEGIN
SELECT		FileHandle AS FileName

FROM			AssociatedPartDoc

WHERE			fkPartTemplateID = PartTemplateID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcViewDrawing` (IN `OperationID` INT UNSIGNED)  BEGIN 

SELECT		FileHandle AS FileName, pkDrawingID

FROM		Drawing D
INNER JOIN	PartTemplate PT
ON			D.pkDrawingID = PT.fkDrawingID
INNER JOIN	LineItem LI
ON			PT.pkPartTemplateID = LI.fkPartTemplateID
INNER JOIN	WorkOrder WO
ON			LI.pkLineItemID = WO.fkLineItemID
INNER JOIN	Operation O
ON			WO.pkWorkOrderID = O.fkWorkOrderID

WHERE		O.pkOperationID = OperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcViewSupplierQuestions` (IN `SupplierID` SMALLINT UNSIGNED)  BEGIN
SELECT		QuestionsFilename AS FileName

FROM		Supplier

WHERE		pkSupplierID = SupplierID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcViewTmpltADD` (IN `ADDID` MEDIUMINT UNSIGNED)  BEGIN
SELECT		FileHandle AS FileName

FROM			AssociatedPartDoc

WHERE			pkAPDID = ADDID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcWOEngineeringToReview` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED, `PartNumber` VARCHAR(25))  BEGIN

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		Todo 
				(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, PartNumber, 
				fkEngineeringTemplateID, Complete)
VALUES			(Date(Now()), 4, UserID, 4, WorkOrderID, PartNumber, (SELECT DISTINCT(fkEngineeringTemplateID)
																			FROM LineItem LL
																			INNER JOIN workOrder WW
																			ON LL.pkLineItemID = WW.fkLineItemID
																			WHERE pkworkOrderID = WorkOrderID), 0);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcWOList` ()  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkWorkOrderID AS WONo
FROM		WorkOrder WO

WHERE					Complete = 0;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcWOPurchToReview` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED, `PartNumber` VARCHAR(25))  BEGIN

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
 INSERT INTO	Kythera.Todo 
 				(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, PartNumber, 
				fkEngineeringTemplateID, Description)
 VALUES		(Date(Now()), 4, UserID, 2, WorkOrderID, PartNumber, (SELECT DISTINCT(fkEngineeringTemplateID)
																			FROM LineItem LL
																			INNER JOIN workOrder WW
																			ON LL.pkLineItemID = WW.fkLineItemID
																			WHERE pkworkOrderID = WorkOrderID)
				,'Material review');
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcWOPurchToReviewSC` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED, `PartNumber` VARCHAR(25))  BEGIN 
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO		Kythera.Todo 
				(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, PartNumber, fkEngineeringTemplateID, 
				Complete, Description)
VALUES			(Date(Now()), 4, UserID, 2, WorkOrderID, PartNumber, (SELECT DISTINCT(fkEngineeringTemplateID)
																		FROM LineItem LL
																		INNER JOIN workOrder WW
																		ON LL.pkLineItemID = WW.fkLineItemID
																		WHERE pkworkOrderID = WorkOrderID),
				0,'Sub-con review');
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcWOQualityToReview` (IN `UserID` SMALLINT UNSIGNED, `WorkOrderID` MEDIUMINT UNSIGNED, `PartNumber` VARCHAR(25))  BEGIN

DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
INSERT INTO	Todo
				(RaisedDate, fkActionTypeID, RaisedByfkUserID, DeptID, fkWorkOrderID, PartNumber, 
				fkEngineeringTemplateID, Complete)
VALUES			(DATE(Now()), 10, UserID, 5, WorkOrderID, Partnumber, (SELECT DISTINCT(fkEngineeringTemplateID)
																		FROM LineItem LL
																		INNER JOIN workOrder WW
																		ON LL.pkLineItemID = WW.fkLineItemID
																		WHERE pkworkOrderID = WorkOrderID), 0);
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcWorkingDays` (IN `StartDate` DATE, IN `EndDate` DATE)  BEGIN
SELECT 
	CASE 
	WHEN 
		(dayofweek(StartDate) = 1 and dayofweek(EndDate) = 3) or 
		(dayofweek(StartDate) = 1 and dayofweek(EndDate) = 4) or 
		(dayofweek(StartDate) = 1 and dayofweek(EndDate) = 5) or 
		(dayofweek(StartDate) = 1 and dayofweek(EndDate) = 6) or 
		(dayofweek(StartDate) = 1 and dayofweek(EndDate) = 7) or 
		(dayofweek(StartDate) = 2 and dayofweek(EndDate) = 1) or 
		(dayofweek(StartDate) = 2 and dayofweek(EndDate) = 2) or 
		(dayofweek(StartDate) = 2 and dayofweek(EndDate) = 3) or 
		(dayofweek(StartDate) = 2 and dayofweek(EndDate) = 7) or 
		(dayofweek(StartDate) = 3 and dayofweek(EndDate) = 2) or 
		(dayofweek(StartDate) = 3 and dayofweek(EndDate) = 3) or 
		(dayofweek(StartDate) = 3 and dayofweek(EndDate) = 4) or 
		(dayofweek(StartDate) = 3 and dayofweek(EndDate) = 7) or 
		(dayofweek(StartDate) = 4 and dayofweek(EndDate) = 3) or 
		(dayofweek(StartDate) = 4 and dayofweek(EndDate) = 4) or 
		(dayofweek(StartDate) = 4 and dayofweek(EndDate) = 5) or 
		(dayofweek(StartDate) = 4 and dayofweek(EndDate) = 7) or 
		(dayofweek(StartDate) = 5 and dayofweek(EndDate) = 4) or 
		(dayofweek(StartDate) = 5 and dayofweek(EndDate) = 5) or 
		(dayofweek(StartDate) = 5 and dayofweek(EndDate) = 6) or 
		(dayofweek(StartDate) = 5 and dayofweek(EndDate) = 7) or 
		(dayofweek(StartDate) = 6 and dayofweek(EndDate) = 5) or 
		(dayofweek(StartDate) = 6 and dayofweek(EndDate) = 6) or 
		(dayofweek(StartDate) = 7 and dayofweek(EndDate) = 6) 
	THEN ((ROUND(((datediff(EndDate, StartDate)/7)*5),0))+1)
	WHEN 
		(dayofweek(StartDate) = 2 and dayofweek(EndDate) = 4) or 
		(dayofweek(StartDate) = 2 and dayofweek(EndDate) = 5) or 
		(dayofweek(StartDate) = 2 and dayofweek(EndDate) = 6) or 
		(dayofweek(StartDate) = 3 and dayofweek(EndDate) = 5) or 
		(dayofweek(StartDate) = 3 and dayofweek(EndDate) = 6) or 
		(dayofweek(StartDate) = 4 and dayofweek(EndDate) = 6) 
	THEN ((ROUND(((datediff(EndDate, StartDate)/7)*5),0))+2) 
	WHEN (dayofweek(StartDate) = 7 and dayofweek(EndDate) = 1) 
	THEN ((ROUND(((datediff(EndDate, StartDate)/7)*5),0))-1) 
	ELSE (ROUND(((datediff(EndDate, StartDate)/7)*5),0))
- (select Count(Date) from Holiday where Date between StartDate and EndDate)
END
AS Days;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcWorkOrderSearch` (IN `WOSearchField` CHAR(5), `Live` TINYINT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkWorkOrderID
FROM		WorkOrder WO

WHERE		WO.Live = IFNULL(Live, 0)
AND 		CAST(pkWorkOrderID AS char) LIKE CONCAT(WOSearchField, "%");
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProcWorkOrdersForPart` (IN `PartNumber` VARCHAR(25))  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT		pkWorkOrderID
FROM		WorkOrder WO
INNER JOIN	LineItem LI
ON			WO.fkLineItemID = LI.pkLineItemID
LEFT JOIN	PartTemplate PT
ON			LI.fkPartTemplateID = PT.pkPartTemplateID

WHERE		PT.PartNumber = PartNumber;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProductionSchedule` ()  BEGIN
SET @DateStore = '1900-01-01';

IF 		OOO.OperationNumber = (SELECT MAX(OperationNumber) FROM Operation OOO WHERE fkOperationID = O.pkOperationID) 
THEN
	SELECT		CONCAT(M.Manufacturer, ' ', M.Model) AS Machine, WO.pkWorkOrderID AS 'WO No.', LI.fkOrderID AS 'CO No.', PT.PartNumber, 
						ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1) + 1) AS Qty,
				MT.Description AS Operation, 
														(O.EstimatedSetTime + O.EstimatedRunTime) 
					* ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1 + 1)) AS OpTimeSecs,
					
						@DateStore AS DDStart,

						@DateStore AS StartDate,

						@DateStore AS Finish,

						DATE_ADD(WO.OrigDeliveryDate, INTERVAL -(SELECT FatDays FROM Fat WHERE FatType = 2) DAY),
 
				U.UName AS Resource, O.OperationNumber AS OpNo,
				(SELECT MAX(OperationNumber) FROM Operation WHERE fkOperationID = O.pkOperationID) AS LastOp

	FROM			Operation O
	LEFT JOIN		WorkOrder WO
	ON				O.fkWorkOrderID = WO.pkWorkOrderID
	LEFT JOIN		LineItem LI
	ON				WO.fkLineItemID = LI.pkLineItemID
	LEFT JOIN		PartTemplate PT
	ON				LI.fkPartTemplateID = PT.pkPartTemplateID
	LEFT JOIN		OperationType OT
	ON				O.fkOperationTypeID = OT.pkOperationTypeID
	LEFT JOIN		MachiningType MT
	ON				O.fkMachiningTypeID = MT.pkMachiningTypeID
	LEFT JOIN		OperationMachine OM
	ON				O.pkOperationID = OM.fkOperationID
	LEFT JOIN		User U
	ON				OM.fkUserID = U.pkUserID
	LEFT JOIN		Machine M
	ON				OM.fkMachineID = M.pkMachineID

	WHERE			WO.Live = 1 AND (WO.complete <> 1 OR ISNULL(WO.Complete) = true)
	AND 			OT.pkOperationTypeID <> 1

		ORDER BY		O.OperationNumber DESC;

ELSE 

					SELECT		CONCAT(M.Manufacturer, ' ', M.Model) AS Machine, WO.pkWorkOrderID AS 'WO No.', LI.fkOrderID AS 'CO No.', PT.PartNumber, 
						ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1) + 1) AS Qty,
				MT.Description AS Operation, 
														(O.EstimatedSetTime + O.EstimatedRunTime) 
					* ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1 + 1)) AS OpTimeSecs,
					
						@DateStore AS DDStart,

						@DateStore AS StartDate,

						@DateStore AS Finish,

						DATE_ADD(DATE_ADD(@DateStore, INTERVAL -(SELECT FatDays FROM Fat WHERE FatType = 1) DAY), INTERVAL 
					-((O.EstimatedSetTime + O.EstimatedRunTime) * ROUND((WO.QuantityReqd + WO.QuantityOvers)*1.1 + 1)/3600) DAY)

	FROM			Operation O
	LEFT JOIN		WorkOrder WO
	ON				O.fkWorkOrderID = WO.pkWorkOrderID
	LEFT JOIN		LineItem LI
	ON				WO.fkLineItemID = LI.pkLineItemID
	LEFT JOIN		PartTemplate PT
	ON				LI.fkPartTemplateID = PT.pkPartTemplateID
	LEFT JOIN		OperationType OT
	ON				O.fkOperationTypeID = OT.pkOperationTypeID
	LEFT JOIN		MachiningType MT
	ON				O.fkMachiningTypeID = MT.pkMachiningTypeID
	LEFT JOIN		OperationMachine OM
	ON				O.pkOperationID = OM.fkOperationID
	LEFT JOIN		User U
	ON				OM.fkUserID = U.pkUserID
	LEFT JOIN		Machine M
	ON				OM.fkMachineID = M.pkMachineID

	WHERE			WO.Live = 1 AND (WO.complete <> 1 OR ISNULL(WO.Complete) = true)
	AND 			OT.pkOperationTypeID <> 1

		ORDER BY		O.OperationNumber DESC;

END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProductionSchedule2` ()  BEGIN
SET @DateStore = '1900-01-01';

IF 		OOO.OperationNumber = (SELECT MAX(OperationNumber) FROM Operation OOO WHERE fkOperationID = O.pkOperationID) 
THEN
	SELECT		CONCAT(M.Manufacturer, ' ', M.Model) AS Machine, WO.pkWorkOrderID AS 'WO No.', LI.fkOrderID AS 'CO No.', PT.PartNumber, 
						ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1) + 1) AS Qty,
				MT.Description AS Operation, 
														(O.EstimatedSetTime + O.EstimatedRunTime) 
					* ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1 + 1)) AS OpTimeSecs,
					
						@DateStore AS DDStart,

						@DateStore AS StartDate,

						@DateStore AS Finish,

						DATE_ADD(WO.OrigDeliveryDate, INTERVAL -(SELECT FatDays FROM Fat WHERE FatType = 2) DAY),
 
				U.UName AS Resource, O.OperationNumber AS OpNo,
				(SELECT MAX(OperationNumber) FROM Operation WHERE fkOperationID = O.pkOperationID) AS LastOp

	FROM			Operation O
	LEFT JOIN		WorkOrder WO
	ON				O.fkWorkOrderID = WO.pkWorkOrderID
	LEFT JOIN		LineItem LI
	ON				WO.fkLineItemID = LI.pkLineItemID
	LEFT JOIN		PartTemplate PT
	ON				LI.fkPartTemplateID = PT.pkPartTemplateID
	LEFT JOIN		OperationType OT
	ON				O.fkOperationTypeID = OT.pkOperationTypeID
	LEFT JOIN		MachiningType MT
	ON				O.fkMachiningTypeID = MT.pkMachiningTypeID
	LEFT JOIN		OperationMachine OM
	ON				O.pkOperationID = OM.fkOperationID
	LEFT JOIN		User U
	ON				OM.fkUserID = U.pkUserID
	LEFT JOIN		Machine M
	ON				OM.fkMachineID = M.pkMachineID

	WHERE			WO.Live = 1 AND (WO.complete <> 1 OR ISNULL(WO.Complete) = true)
	AND 			OT.pkOperationTypeID <> 1

		ORDER BY		O.OperationNumber DESC;

ELSE 

					SELECT		CONCAT(M.Manufacturer, ' ', M.Model) AS Machine, WO.pkWorkOrderID AS 'WO No.', LI.fkOrderID AS 'CO No.', PT.PartNumber, 
						ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1) + 1) AS Qty,
				MT.Description AS Operation, 
														(O.EstimatedSetTime + O.EstimatedRunTime) 
					* ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1 + 1)) AS OpTimeSecs,
					
						@DateStore AS DDStart,

						@DateStore AS StartDate,

						@DateStore AS Finish,

						DATE_ADD(DATE_ADD(@DateStore, INTERVAL -(SELECT FatDays FROM Fat WHERE FatType = 1) DAY), INTERVAL 
					-((O.EstimatedSetTime + O.EstimatedRunTime) * ROUND((WO.QuantityReqd + WO.QuantityOvers)*1.1 + 1)/3600) DAY)

	FROM			Operation O
	LEFT JOIN		WorkOrder WO
	ON				O.fkWorkOrderID = WO.pkWorkOrderID
	LEFT JOIN		LineItem LI
	ON				WO.fkLineItemID = LI.pkLineItemID
	LEFT JOIN		PartTemplate PT
	ON				LI.fkPartTemplateID = PT.pkPartTemplateID
	LEFT JOIN		OperationType OT
	ON				O.fkOperationTypeID = OT.pkOperationTypeID
	LEFT JOIN		MachiningType MT
	ON				O.fkMachiningTypeID = MT.pkMachiningTypeID
	LEFT JOIN		OperationMachine OM
	ON				O.pkOperationID = OM.fkOperationID
	LEFT JOIN		User U
	ON				OM.fkUserID = U.pkUserID
	LEFT JOIN		Machine M
	ON				OM.fkMachineID = M.pkMachineID

	WHERE			WO.Live = 1 AND (WO.complete <> 1 OR ISNULL(WO.Complete) = true)
	AND 			OT.pkOperationTypeID <> 1

		ORDER BY		O.OperationNumber DESC;

END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProductionScheduleOld` ()  BEGIN
SET @DateStore = '1900-01-01';

IF 		OOO.OperationNumber = (SELECT MAX(OperationNumber) FROM Operation OOO WHERE fkOperationID = O.pkOperationID) 
THEN
	SELECT		CONCAT(M.Manufacturer, ' ', M.Model) AS Machine, WO.pkWorkOrderID AS 'WO No.', LI.fkOrderID AS 'CO No.', PT.PartNumber, 
						ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1) + 1) AS Qty,
				MT.Description AS Operation, 
														(O.EstimatedSetTime + O.EstimatedRunTime) 
					* ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1 + 1)) AS OpTimeSecs,
					
						@DateStore AS DDStart,

						@DateStore AS StartDate,

						@DateStore AS Finish,

						DATE_ADD(WO.OrigDeliveryDate, INTERVAL -(SELECT FatDays FROM Fat WHERE FatType = 2) DAY),
 
				U.UName AS Resource, O.OperationNumber AS OpNo,
				(SELECT MAX(OperationNumber) FROM Operation WHERE fkOperationID = O.pkOperationID) AS LastOp

	FROM			Operation O
	LEFT JOIN		WorkOrder WO
	ON				O.fkWorkOrderID = WO.pkWorkOrderID
	LEFT JOIN		LineItem LI
	ON				WO.fkLineItemID = LI.pkLineItemID
	LEFT JOIN		PartTemplate PT
	ON				LI.fkPartTemplateID = PT.pkPartTemplateID
	LEFT JOIN		OperationType OT
	ON				O.fkOperationTypeID = OT.pkOperationTypeID
	LEFT JOIN		MachiningType MT
	ON				O.fkMachiningTypeID = MT.pkMachiningTypeID
	LEFT JOIN		OperationMachine OM
	ON				O.pkOperationID = OM.fkOperationID
	LEFT JOIN		User U
	ON				OM.fkUserID = U.pkUserID
	LEFT JOIN		Machine M
	ON				OM.fkMachineID = M.pkMachineID

	WHERE			WO.Live = 1 AND (WO.complete <> 1 OR ISNULL(WO.Complete) = true)
	AND 			OT.pkOperationTypeID <> 1

		ORDER BY		O.OperationNumber DESC;

ELSE 

					SELECT		CONCAT(M.Manufacturer, ' ', M.Model) AS Machine, WO.pkWorkOrderID AS 'WO No.', LI.fkOrderID AS 'CO No.', PT.PartNumber, 
						ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1) + 1) AS Qty,
				MT.Description AS Operation, 
														(O.EstimatedSetTime + O.EstimatedRunTime) 
					* ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1 + 1)) AS OpTimeSecs,
					
						@DateStore AS DDStart,

						@DateStore AS StartDate,

						@DateStore AS Finish,

						DATE_ADD(DATE_ADD(@DateStore, INTERVAL -(SELECT FatDays FROM Fat WHERE FatType = 1) DAY), INTERVAL 
					-((O.EstimatedSetTime + O.EstimatedRunTime) * ROUND((WO.QuantityReqd + WO.QuantityOvers)*1.1 + 1)/3600) DAY)

	FROM			Operation O
	LEFT JOIN		WorkOrder WO
	ON				O.fkWorkOrderID = WO.pkWorkOrderID
	LEFT JOIN		LineItem LI
	ON				WO.fkLineItemID = LI.pkLineItemID
	LEFT JOIN		PartTemplate PT
	ON				LI.fkPartTemplateID = PT.pkPartTemplateID
	LEFT JOIN		OperationType OT
	ON				O.fkOperationTypeID = OT.pkOperationTypeID
	LEFT JOIN		MachiningType MT
	ON				O.fkMachiningTypeID = MT.pkMachiningTypeID
	LEFT JOIN		OperationMachine OM
	ON				O.pkOperationID = OM.fkOperationID
	LEFT JOIN		User U
	ON				OM.fkUserID = U.pkUserID
	LEFT JOIN		Machine M
	ON				OM.fkMachineID = M.pkMachineID

	WHERE			WO.Live = 1 AND (WO.complete <> 1 OR ISNULL(WO.Complete) = true)
	AND 			OT.pkOperationTypeID <> 1

		ORDER BY		O.OperationNumber DESC;

END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ProductionScheduleold2` ()  BEGIN
SET @DateStore = '1900-01-01';

IF 		OOO.OperationNumber = (SELECT MAX(OperationNumber) FROM Operation OOO WHERE fkOperationID = O.pkOperationID) 
THEN
	SELECT		CONCAT(M.Manufacturer, ' ', M.Model) AS Machine, WO.pkWorkOrderID AS 'WO No.', LI.fkOrderID AS 'CO No.', PT.PartNumber, 
						ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1) + 1) AS Qty,
				MT.Description AS Operation, 
														(O.EstimatedSetTime + O.EstimatedRunTime) 
					* ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1 + 1)) AS OpTimeSecs,
					
						@DateStore AS DDStart,

						@DateStore AS StartDate,

						@DateStore AS Finish,

						DATE_ADD(WO.OrigDeliveryDate, INTERVAL -(SELECT FatDays FROM Fat WHERE FatType = 2) DAY),
 
				U.UName AS Resource, O.OperationNumber AS OpNo,
				(SELECT MAX(OperationNumber) FROM Operation WHERE fkOperationID = O.pkOperationID) AS LastOp

	FROM			Operation O
	LEFT JOIN		WorkOrder WO
	ON				O.fkWorkOrderID = WO.pkWorkOrderID
	LEFT JOIN		LineItem LI
	ON				WO.fkLineItemID = LI.pkLineItemID
	LEFT JOIN		PartTemplate PT
	ON				LI.fkPartTemplateID = PT.pkPartTemplateID
	LEFT JOIN		OperationType OT
	ON				O.fkOperationTypeID = OT.pkOperationTypeID
	LEFT JOIN		MachiningType MT
	ON				O.fkMachiningTypeID = MT.pkMachiningTypeID
	LEFT JOIN		OperationMachine OM
	ON				O.pkOperationID = OM.fkOperationID
	LEFT JOIN		User U
	ON				OM.fkUserID = U.pkUserID
	LEFT JOIN		Machine M
	ON				OM.fkMachineID = M.pkMachineID

	WHERE			WO.Live = 1 AND (WO.complete <> 1 OR ISNULL(WO.Complete) = true)
	AND 			OT.pkOperationTypeID <> 1

		ORDER BY		O.OperationNumber DESC;

ELSE 

					SELECT		CONCAT(M.Manufacturer, ' ', M.Model) AS Machine, WO.pkWorkOrderID AS 'WO No.', LI.fkOrderID AS 'CO No.', PT.PartNumber, 
						ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1) + 1) AS Qty,
				MT.Description AS Operation, 
														(O.EstimatedSetTime + O.EstimatedRunTime) 
					* ROUND(((WO.QuantityReqd + WO.quantityOvers) *1.1 + 1)) AS OpTimeSecs,
					
						@DateStore AS DDStart,

						@DateStore AS StartDate,

						@DateStore AS Finish,

						DATE_ADD(DATE_ADD(@DateStore, INTERVAL -(SELECT FatDays FROM Fat WHERE FatType = 1) DAY), INTERVAL 
					-((O.EstimatedSetTime + O.EstimatedRunTime) * ROUND((WO.QuantityReqd + WO.QuantityOvers)*1.1 + 1)/3600) DAY)

	FROM			Operation O
	LEFT JOIN		WorkOrder WO
	ON				O.fkWorkOrderID = WO.pkWorkOrderID
	LEFT JOIN		LineItem LI
	ON				WO.fkLineItemID = LI.pkLineItemID
	LEFT JOIN		PartTemplate PT
	ON				LI.fkPartTemplateID = PT.pkPartTemplateID
	LEFT JOIN		OperationType OT
	ON				O.fkOperationTypeID = OT.pkOperationTypeID
	LEFT JOIN		MachiningType MT
	ON				O.fkMachiningTypeID = MT.pkMachiningTypeID
	LEFT JOIN		OperationMachine OM
	ON				O.pkOperationID = OM.fkOperationID
	LEFT JOIN		User U
	ON				OM.fkUserID = U.pkUserID
	LEFT JOIN		Machine M
	ON				OM.fkMachineID = M.pkMachineID

	WHERE			WO.Live = 1 AND (WO.complete <> 1 OR ISNULL(WO.Complete) = true)
	AND 			OT.pkOperationTypeID <> 1

		ORDER BY		O.OperationNumber DESC;

END IF;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `RouteCard` (IN `WorkOrderID` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		CONCAT('Part No. ',P.PartNumber) AS PartNumber, P.IssueNumber AS PartIssue, CONCAT('"', P.Description, '"') AS Description, 
			D.Reference, D.IssueNumber AS DRGIssue, O.fkWorkOrderID AS pkWorkOrderID,
			O.pkOperationID, ifnull(M.Description, OT.OpType) AS MachiningOp, O.fkOperationTypeID AS OpTypeID,
			U.pkUserID AS InspectorID, U.Firstname AS Inspector1st, U.Lastname AS Inspector2nd,
			I.Outcome, MC.Manufacturer, MC.Model, A.fkGRBID AS BatchNo, QuantityReqd + QuantityOvers AS Qty, GI.IRNoteNumber


FROM		Operation O 
LEFT JOIN	WorkOrder WO
	ON		O.fkWorkOrderID = WO.pkWorkOrderID
LEFT JOIN	LineItem LI
	ON		WO.fkLineItemID = LI.pkLineItemID
LEFT JOIN	PartTemplate P
	ON		LI.fkPartTemplateID = P.pkPartTemplateID
LEFT JOIN	Drawing D
	ON		P.fkDrawingID = D.pkDrawingID

LEFT JOIN	OperationMachine OMC
	ON		O.pkOperationID = OMC.fkOperationID
LEFT JOIN	Machine MC
	ON		OMC.fkMachineID = MC.pkMachineID

LEFT JOIN	OperationInspect OI
	ON		O.pkOperationId = OI.fkOperationID
LEFT JOIN	Inspection I
	ON		OI.fkInspectionID = I.pkInspectionID
LEFT JOIN	User U
	ON		I.fkUserID = U.pkUserID

LEFT JOIN	Allocated A
	ON		O.pkOperationID = A.fkOperationID

LEFT JOIN	OperationMachining OM
	ON		O.pkOperationID = OM.fkOperationID
LEFT JOIN	MachiningType M
	ON		OM.fkMachiningListID = M.pkMachiningTypeID

LEFT JOIN	OperationType OT
	ON		O.fkOperationTypeID = OT.pkOperationTypeID

LEFT JOIN	GoodsIn GI
	ON		A.fkGRBID = GI.pkGRBID

WHERE		O.fkWorkOrderID = WorkOrderID
ORDER BY	O.pkOperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `RouteCardFixture` (IN `WorkOrderID` INT UNSIGNED)  BEGIN
SELECT f.grbnumber, f.fixturenumber, f.description FROM part p inner join fixture f on p.pkpartid =  f.fkpartid
inner join lineitem li on p.pkpartid = li.fkpartid 
inner join workorder wo on li.pklineitemid = wo.fklineitemid 
inner join operation o on wo.pkworkorderid = o.fkworkorderid
where wo.pkworkorderid = WorkOrderID and o.fkoperationtypeid = 2
order by o.pkoperationid;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `RouteCardOld` (IN `WorkOrderID` INT UNSIGNED)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT 		CONCAT('Part No. ',P.PartNumber) AS PartNumber, P.IssueNumber AS PartIssue, CONCAT('"', P.Description, '"') AS Description, 
			D.Reference, D.IssueNumber AS DRGIssue, O.fkWorkOrderID AS pkWorkOrderID,
			O.pkOperationID, ifnull(M.Description, OT.OpType) AS MachiningOp, O.fkOperationTypeID AS OpTypeID,
			U.pkUserID AS InspectorID, U.Firstname AS Inspector1st, U.Lastname AS Inspector2nd,
			I.Outcome, MC.Manufacturer, MC.Model, A.fkGRBID AS BatchNo, QuantityReqd + QuantityOvers AS Qty, GI.IRNoteNumber


FROM		Operation O 
LEFT JOIN	WorkOrder WO
	ON		O.fkWorkOrderID = WO.pkWorkOrderID
LEFT JOIN	LineItem LI
	ON		WO.fkLineItemID = LI.pkLineItemID
LEFT JOIN	PartTemplate P
	ON		LI.fkPartTemplateID = P.pkPartTemplateID
LEFT JOIN	Drawing D
	ON		P.fkDrawingID = D.pkDrawingID

LEFT JOIN	OperationMachine OMC
	ON		O.pkOperationID = OMC.fkOperationID
LEFT JOIN	Machine MC
	ON		OMC.fkMachineID = MC.pkMachineID

LEFT JOIN	OperationInspect OI
	ON		O.pkOperationId = OI.fkOperationID
LEFT JOIN	Inspection I
	ON		OI.fkInspectionID = I.pkInspectionID
LEFT JOIN	User U
	ON		I.fkUserID = U.pkUserID

LEFT JOIN	Allocated A
	ON		O.pkOperationID = A.fkOperationID

LEFT JOIN	OperationMachining OM
	ON		O.pkOperationID = OM.fkOperationID
LEFT JOIN	MachiningType M
	ON		OM.fkMachiningListID = M.pkMachiningTypeID

LEFT JOIN	OperationType OT
	ON		O.fkOperationTypeID = OT.pkOperationTypeID

LEFT JOIN	GoodsIn GI
	ON		A.fkGRBID = GI.pkGRBID

WHERE		O.fkWorkOrderID = WorkOrderID
ORDER BY	O.pkOperationID;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `RouteCardOpProcess` (IN `WorkOrderID` INT UNSIGNED)  BEGIN
SELECT 		fkOperationID, ProcessNote	

FROM		OperationProcess OP

INNER JOIN	Operation O
ON			OP.fkOperationID = O.pkOperationID
INNER JOIN	WorkOrder WO
ON			O.fkWorkOrderID = WO.pkWorkOrderID

WHERE		pkWorkOrderID = WorkOrderID
;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `SubConQPO` (IN `TreatmentID` INT)  BEGIN
DECLARE EXIT HANDLER FOR SQLWARNING BEGIN END;
SELECT DISTINCT		S.SupplierName, TL.Description, NA.Description AS ReleaseDescription, ST.Quantity, ST.Price

FROM		SupplierTreatment ST
LEFT JOIN	OperationTreatment OT
ON			ST.fkOperationID = OT.pkOperationID
LEFT JOIN	Supplier S
ON			ST.fkSupplierID = S.pkSupplierID
LEFT JOIN	TreatmentList TL
ON			OT.fkTreatmentID = TL.pkTreatmentID
LEFT JOIN	SupplierNADCAP SN
ON			S.pkSupplierID = SN.fkSupplierID
LEFT JOIN	NADCAPApproval NA
ON			SN.fkApprovalID = NA.pkApprovalID

WHERE		OT.fkTreatmentID = TreatmentID
AND			NA.Description IS NOT NULL;
END$$

CREATE DEFINER=`david`@`localhost` PROCEDURE `ViewAllCustomerDetail` ()  BEGIN
SELECT * FROM ExportCustomers;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `acknowledgementnote`
--

CREATE TABLE `acknowledgementnote` (
  `pkAcknowNoteID` mediumint(8) UNSIGNED NOT NULL,
  `fkOrderID` mediumint(8) UNSIGNED NOT NULL,
  `Note` varchar(80) DEFAULT NULL,
  `NoteDate` date DEFAULT NULL COMMENT 'added 25/3/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acknowledgementnote`
--

INSERT INTO `acknowledgementnote` (`pkAcknowNoteID`, `fkOrderID`, `Note`, `NoteDate`) VALUES
(1, 2, 'ABC123ST17  acknowledgement form note 1', '2016-11-17'),
(2, 2, 'ABC123ST17 Acknowledgement form note 2', '2016-11-17');

-- --------------------------------------------------------

--
-- Table structure for table `actiontype`
--

CREATE TABLE `actiontype` (
  `pkActionTypeID` tinyint(4) NOT NULL,
  `Description` varchar(15) DEFAULT NULL,
  `Deleted` tinyint(4) DEFAULT NULL COMMENT '0 = no, 1 = yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `actiontype`
--

INSERT INTO `actiontype` (`pkActionTypeID`, `Description`, `Deleted`) VALUES
(1, 'GR in', 0),
(2, 'Issue', 0),
(3, 'SC', 0),
(4, 'Review', 0),
(5, 'Invoice', 0),
(6, 'Outvoice', 0),
(7, 'Late Outvoice', 0),
(8, 'Payroll', 0),
(9, 'Split batch', 0),
(10, 'CR', 0),
(11, 'Plan', 0),
(12, 'Plan chg', 0),
(13, 'Estimate', 0),
(14, 'Purchase', 0),
(15, 'Quote', 0),
(16, 'Safety', 0),
(17, 'Chase PO', 0),
(18, 'Credit', 0),
(19, 'NRE', 0),
(20, 'Service', 0),
(21, 'Schedule', 0),
(22, 'Dispatch', 0);

-- --------------------------------------------------------

--
-- Table structure for table `additionaltime`
--

CREATE TABLE `additionaltime` (
  `pkAdditionalID` int(11) UNSIGNED NOT NULL,
  `fkCustomerID` int(11) UNSIGNED DEFAULT NULL,
  `TimeType` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1= Shutdown, 2 = collection , 3 = other',
  `StartPoint` varchar(9) DEFAULT NULL COMMENT 'can be day name or date (without year)',
  `EndPoint` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='additional collection / shut down times';

-- --------------------------------------------------------

--
-- Table structure for table `allocated`
--

CREATE TABLE `allocated` (
  `fkOperationID` int(11) UNSIGNED NOT NULL,
  `fkGRBID` int(11) UNSIGNED NOT NULL COMMENT 'added 21/1/16',
  `Display` tinyint(4) DEFAULT NULL COMMENT 'added 21/1/16',
  `Print` tinyint(4) DEFAULT NULL COMMENT 'added 21/1/16',
  `Quantity` smallint(6) DEFAULT NULL COMMENT 'added 27/1/16',
  `AddedByfkUserID` tinyint(4) DEFAULT NULL COMMENT 'added 27/1/16',
  `AllocateDate` date DEFAULT NULL COMMENT 'added 9/2/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='rmvd materialid 9/3/16		';

--
-- Dumping data for table `allocated`
--

INSERT INTO `allocated` (`fkOperationID`, `fkGRBID`, `Display`, `Print`, `Quantity`, `AddedByfkUserID`, `AllocateDate`) VALUES
(1, 49, 1, 0, 20, 4, '2016-11-16'),
(3, 50, 1, 0, 58, 4, '2016-11-16');

-- --------------------------------------------------------

--
-- Table structure for table `approval`
--

CREATE TABLE `approval` (
  `pkApprovalID` tinyint(4) UNSIGNED NOT NULL COMMENT 'chg to auto 5/2/16',
  `Description` varchar(15) DEFAULT NULL,
  `Deleted` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1 = yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `approval`
--

INSERT INTO `approval` (`pkApprovalID`, `Description`, `Deleted`) VALUES
(1, 'Commercial', 0),
(2, 'AS9100C', 0),
(3, 'ISO9000', 0),
(4, 'BAE', 0),
(5, 'Airbus', 0),
(6, 'Messier', 0),
(7, 'Spirit Aero', 0),
(8, 'Ultra', 0),
(9, 'Bombardier', 0);

-- --------------------------------------------------------

--
-- Table structure for table `approvalarea`
--

CREATE TABLE `approvalarea` (
  `pkApprovalAreaID` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `approvallevel`
--

CREATE TABLE `approvallevel` (
  `pkApprovalLevelID` tinyint(3) UNSIGNED NOT NULL,
  `Description` varchar(15) DEFAULT NULL,
  `Deleted` tinyint(3) UNSIGNED DEFAULT '0' COMMENT '1 = yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `approvallevel`
--

INSERT INTO `approvallevel` (`pkApprovalLevelID`, `Description`, `Deleted`) VALUES
(1, 'Commercial', 0),
(2, 'ISO9001', 0),
(3, 'AS9100C', 0),
(4, 'BAE', 0),
(5, 'AirbusUK', 0),
(6, 'Messier', 0),
(7, 'Spirit Aero', 0),
(8, 'Ultra', 0),
(9, 'Bombardier', 0);

-- --------------------------------------------------------

--
-- Table structure for table `approvalreq`
--

CREATE TABLE `approvalreq` (
  `pkApprovalReqID` int(11) UNSIGNED NOT NULL,
  `fkApprovalID` int(11) UNSIGNED DEFAULT NULL,
  `fkApprovalAreaID` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `associatedpartdoc`
--

CREATE TABLE `associatedpartdoc` (
  `pkAPDID` mediumint(8) UNSIGNED NOT NULL,
  `fkPartTemplateID` smallint(5) UNSIGNED DEFAULT NULL,
  `fkUserID` smallint(5) UNSIGNED DEFAULT NULL,
  `DateAdded` datetime DEFAULT NULL,
  `Location` varchar(45) DEFAULT NULL COMMENT 'not sure if needed; std folder> htdocs/DocStore',
  `FileHandle` varchar(30) DEFAULT NULL,
  `fkDocTypeID` tinyint(4) UNSIGNED DEFAULT NULL,
  `AutoPrint` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 1/3/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='other drawing documents linked to the part template';

--
-- Dumping data for table `associatedpartdoc`
--

INSERT INTO `associatedpartdoc` (`pkAPDID`, `fkPartTemplateID`, `fkUserID`, `DateAdded`, `Location`, `FileHandle`, `fkDocTypeID`, `AutoPrint`) VALUES
(1, 14, 4, '2016-11-17 00:00:00', 'DocStore', '1479416396_FAIR.JPG', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `branchlocation`
--

CREATE TABLE `branchlocation` (
  `pkBranchID` tinyint(3) UNSIGNED NOT NULL,
  `Name` varchar(25) DEFAULT NULL,
  `Address1` varchar(45) DEFAULT NULL,
  `Address2` varchar(40) DEFAULT NULL,
  `Address3` varchar(40) DEFAULT NULL,
  `Address4` varchar(30) DEFAULT NULL COMMENT 'added 23/2/16',
  `PostCode` varchar(8) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `Fax` varchar(20) DEFAULT NULL,
  `Deleted` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 16/2/16',
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 23/2/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branchlocation`
--

INSERT INTO `branchlocation` (`pkBranchID`, `Name`, `Address1`, `Address2`, `Address3`, `Address4`, `PostCode`, `Phone`, `Fax`, `Deleted`, `AddedByfkUserID`) VALUES
(1, 'Basildon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 2),
(2, 'Harlow', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `businessshutdown`
--

CREATE TABLE `businessshutdown` (
  `pkShutdownID` tinyint(4) NOT NULL,
  `ShutDate` date DEFAULT NULL,
  `ShutReason` varchar(22) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='lists additional dates where the business is closed.';

--
-- Dumping data for table `businessshutdown`
--

INSERT INTO `businessshutdown` (`pkShutdownID`, `ShutDate`, `ShutReason`) VALUES
(1, '2015-12-25', 'Christmas!'),
(2, '2015-12-26', 'Box day'),
(3, '2016-01-01', 'New Year'),
(4, '2015-03-25', 'Good friday'),
(5, '2015-03-28', 'Easter Monday'),
(6, '2015-05-02', 'May day'),
(7, '2015-05-30', 'Spring Bank hol'),
(8, '2015-08-08', 'Summer Bank hol'),
(9, '2015-08-29', 'Late Summer BH'),
(10, '2016-01-01', 'News Years Day');

-- --------------------------------------------------------

--
-- Table structure for table `calibrate`
--

CREATE TABLE `calibrate` (
  `fkSupplierID` int(11) UNSIGNED NOT NULL,
  `fkMeasuringDeviceID` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `calibrationparameter`
--

CREATE TABLE `calibrationparameter` (
  `pkParameterID` tinyint(4) NOT NULL,
  `fkDeviceID` tinyint(4) DEFAULT NULL,
  `CheckInterval` smallint(6) DEFAULT NULL,
  `NominalSize` decimal(4,3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `calibrationreading`
--

CREATE TABLE `calibrationreading` (
  `pkReadingID` smallint(6) NOT NULL,
  `fkCalibrationParameterID` tinyint(4) DEFAULT NULL,
  `Reading` decimal(4,3) DEFAULT NULL,
  `CalibrationReadingcol` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `certreqs`
--

CREATE TABLE `certreqs` (
  `pkCertReqID` int(11) UNSIGNED NOT NULL,
  `fkCustomerID` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Certificate requirements';

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `pkCommentID` smallint(6) NOT NULL,
  `Comments` varchar(100) DEFAULT NULL COMMENT 'chgd size 17/2/16',
  `fkUserID` smallint(6) UNSIGNED DEFAULT NULL,
  `Deleted` tinyint(4) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`pkCommentID`, `Comments`, `fkUserID`, `Deleted`) VALUES
(1, 'Machined and inspected to Drawing, from ESL procured material', 2, NULL),
(2, 'Broached, deburred and inspected to Drawing, from your Free issue Material.', 1, NULL),
(3, 'Special test comment, to be used for Engineering template Approval comments', 4, NULL),
(4, 'special test comment 2 - approval comments', 4, NULL),
(5, 'special test comment 3 - approval comments', 4, NULL),
(6, 'PO test comment, to be used for Engineering template PO comments', 4, NULL),
(7, 'PO test comment 2 - PO comments section', 4, NULL),
(8, 'PO test comment 3 - PO comments section', 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contingency`
--

CREATE TABLE `contingency` (
  `pkContingencyID` int(11) UNSIGNED NOT NULL,
  `Description` varchar(20) DEFAULT NULL,
  `Days` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `courier`
--

CREATE TABLE `courier` (
  `pkCourierID` tinyint(4) NOT NULL,
  `Name` varchar(25) DEFAULT NULL,
  `Deleted` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1 = yes. updated unsigned 16/2/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `courier`
--

INSERT INTO `courier` (`pkCourierID`, `Name`, `Deleted`) VALUES
(1, 'XDP', 0),
(2, 'Bailin', 0),
(3, 'test courier', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `pkCustomerID` smallint(6) UNSIGNED NOT NULL COMMENT '15/2/16 chgd to ai',
  `CustomerSageCode` char(3) DEFAULT NULL COMMENT 'code to match customer in Sage accounting AAA-ZZZ, 000-999',
  `CustomerName` varchar(50) DEFAULT NULL COMMENT 'Name of business\n1-40chars',
  `Alias` varchar(30) DEFAULT NULL COMMENT 'Alternative name for this customer to set it apart from similar businesses 1-30chars',
  `Address1` varchar(45) DEFAULT NULL COMMENT '1-40chars',
  `Address2` varchar(40) DEFAULT NULL,
  `Address3` varchar(40) DEFAULT NULL,
  `Address4` varchar(30) DEFAULT NULL,
  `Postcode` char(8) DEFAULT NULL COMMENT '6-8chars',
  `Phone` varchar(20) DEFAULT NULL COMMENT 'main phone number of business 10-13numbers',
  `Facsimile` varchar(20) DEFAULT NULL COMMENT '10-13numbers\n',
  `Website` varchar(40) DEFAULT NULL COMMENT 'full address of business website 1-40chars\n',
  `Email` varchar(50) DEFAULT NULL COMMENT '1-50chars',
  `InvAddress1` varchar(45) DEFAULT NULL COMMENT 'Main invoice address line1 1-40chars',
  `InvAddress2` varchar(40) DEFAULT NULL,
  `InvAddress3` varchar(40) DEFAULT NULL,
  `InvAddress4` varchar(30) DEFAULT NULL,
  `InvPostcode` char(8) DEFAULT NULL COMMENT '6-8chars',
  `VATRate` decimal(3,1) DEFAULT NULL COMMENT 'added 5/2/16',
  `FDApproval` tinyint(4) UNSIGNED DEFAULT '0' COMMENT 'Financial Director approval for this customer? 0 or 1',
  `MDApproval` tinyint(4) UNSIGNED DEFAULT '0' COMMENT 'Managing Director approval for this customer? 0 or 1',
  `GMApproval` tinyint(4) UNSIGNED DEFAULT '0' COMMENT 'General Manager approval for this customer? 0 or 1',
  `CreditLimit` decimal(6,0) UNSIGNED DEFAULT NULL COMMENT 'maximum value this customer can be in debt to us for 1 - 999,999.99',
  `MarkUpQuote` tinyint(4) UNSIGNED DEFAULT NULL COMMENT 'the percentage we should mark up the material costs by for this quote',
  `PaymentTerms` tinyint(4) UNSIGNED DEFAULT NULL COMMENT 'the time period the customer should pay us in - 30, 60 or 90 days 1 -3',
  `PaymentRunFrequency` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1 = weekly, 2 = bi-monthly, 3 = monthly 1-3 ',
  `fkApprovalID` tinyint(4) UNSIGNED DEFAULT NULL,
  `DeliveryCharge` decimal(3,0) DEFAULT '0',
  `IrishCurrency` tinyint(4) UNSIGNED DEFAULT '0' COMMENT '1 = yes',
  `USCurrency` tinyint(4) UNSIGNED DEFAULT '0',
  `EuroCurrency` tinyint(4) UNSIGNED DEFAULT '0',
  `Day1Open` time DEFAULT NULL COMMENT 'time of 00:00 indicates closed.',
  `Day1Close` time DEFAULT NULL,
  `Day2Open` time DEFAULT NULL,
  `Day2Close` time DEFAULT NULL,
  `Day3Open` time DEFAULT NULL,
  `Day3Close` time DEFAULT NULL,
  `Day4Open` time DEFAULT NULL,
  `Day4Close` time DEFAULT NULL,
  `Day5Open` time DEFAULT NULL,
  `Day5Close` time DEFAULT NULL,
  `Day6Open` time DEFAULT NULL,
  `Day6Close` time DEFAULT NULL,
  `Day7Open` time DEFAULT NULL,
  `Day7Close` time DEFAULT NULL,
  `Deleted` tinyint(4) UNSIGNED ZEROFILL DEFAULT '0000',
  `FAO` varchar(15) DEFAULT NULL COMMENT 'for attention of - column; only for royal mail spreadsheet extract',
  `DeliveryGrace` tinyint(4) DEFAULT NULL COMMENT 'added 5/2/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`pkCustomerID`, `CustomerSageCode`, `CustomerName`, `Alias`, `Address1`, `Address2`, `Address3`, `Address4`, `Postcode`, `Phone`, `Facsimile`, `Website`, `Email`, `InvAddress1`, `InvAddress2`, `InvAddress3`, `InvAddress4`, `InvPostcode`, `VATRate`, `FDApproval`, `MDApproval`, `GMApproval`, `CreditLimit`, `MarkUpQuote`, `PaymentTerms`, `PaymentRunFrequency`, `fkApprovalID`, `DeliveryCharge`, `IrishCurrency`, `USCurrency`, `EuroCurrency`, `Day1Open`, `Day1Close`, `Day2Open`, `Day2Close`, `Day3Open`, `Day3Close`, `Day4Open`, `Day4Close`, `Day5Open`, `Day5Close`, `Day6Open`, `Day6Close`, `Day7Open`, `Day7Close`, `Deleted`, `FAO`, `DeliveryGrace`) VALUES
(1, 'tst', 'Test customer 1 DP sxxxxxxxxxxxxxxxxxxe', 'Davey boy sxxxxxxxxxxxxxxxxxxe', 'Test Address 1 sxxxxxxxxxxxxxxxxxxxxxxxe', 'Test Address 2 sxxxxxxxxxxxxxxxxxxxxxxxe', 'Test Address 3 sxxxxxxxxxxxxxxxxxxxxxxxe', 'Test Address 4 sxxxxxxxxxxxxxe', 'SS12 9HK', '01268 123456', '01268 234567', 'www.simbiis.com sxxxxxxxxxxxxxxxxxxxxxxe', 'davidphillips@simbiis.com sxxxxxxxxxxxxxxxxxxxxxxe', 'Test inv Address 1 sxxxxxxxxxxxxxxxxxxxe', 'Test inv Address 2 sxxxxxxxxxxxxxxxxxxxe', 'Test inv Address 3 sxxxxxxxxxxxxxxxxxxxe', 'Test inv Address 4 sxxxxxxxxxe', 'SS12 9BB', NULL, 1, 1, 1, '999999', 127, 3, 3, 1, '0', 0, 0, 0, '00:00:09', '00:00:17', '00:00:09', '00:00:17', '00:00:09', '00:00:17', '00:00:09', '00:00:09', '00:00:17', '00:00:16', '00:00:09', '00:00:17', '00:00:10', '00:00:12', 0000, NULL, NULL),
(1001, 'A01', 'Aviation Tool Corporation Plc', 'Aviation ', 'Airport Works', 'Green Lane', 'Hounslow', 'Middlesex', 'TW4 6DG', '020 8570 9664', '0208 5709660', '', 'atctools@aol.com', 'Airport Works', 'Green Lane', 'Hounslow', 'Middlesex', 'TW4 6DG', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1002, 'A02', 'Aeromet International PLC', 'Aeromet ', '21 Laker Road', 'Rochester', 'Kent', 'England', 'ME1 3QX', '01795 415000', '01795 415035', '', '', '21 Laker Road', 'Rochester', 'Kent', '', 'ME1 3QX', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1003, 'A03', 'Arrowsmith Engineering & Consultants Limited', 'Arrowsmith ', 'Ackender Road', 'Alton', 'Hants', 'England', 'GU34 1JP', '01420 86052', '01420 84899', '', 'info@arrowsmitheng.freeserve.co.uk', 'Ackender Road', 'Alton', 'Hants', '', 'GU34 1JP', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1005, 'A05', 'Aerospace Machining Technology Ltd', 'Aerospace ', '20 West Shore Road', 'Granton', 'Edinburgh', 'Scotland', 'EH5 1QD', '0131 552 4271', '0131 552 2552', '', 'busenq@amtltd.co.uk', '20 West Shore Road', 'Granton', 'Edinburgh', 'Scotland', 'EH5 1QD', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1007, 'A07', 'Avon Valley Precision Engineering', 'Avon ', 'Bath Road', 'Bitton', 'Bristol', 'England', 'BS15 6HZ', '0117 932 8118', '0117 932 8119', '', '', 'Bath Road', 'Bitton', 'Bristol', '', 'BS15 6HZ', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1008, 'A08', 'Allied Aeroprecision', 'Allied ', 'Fort Street Industrial Estate', 'Pickup Street', 'Blackburn', 'Lancashire', 'BB1 5DW', '01254 581628', '01254 676111', '', '', 'Fort Street Industrial Estate', 'Pickup Street', 'Blackburn', 'Lancashire', 'BB1 5DW', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1009, 'A09', 'AMETEK Airtechnology Group Limited', 'AMETEK ', 'Airscrew Limited', 'Unit 2 111 Windmill Road', 'Sunbury-on-Thames', 'Middlesex', 'TW16 7EF', '01932 754444', '01932 754872', '', '', 'Airscrew Limited', 'Unit 2 111 Windmill Road', 'Sunbury-on-Thames', 'Middlesex', 'TW16 7EF', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1011, 'A11', 'AT Engine Controls Ltd', 'AT ', 'Units 10/11 Shield Drive', 'Wardley Industrial Estate', 'Worsley', 'Manchester', 'M28 2QB', '0161 727 0600', '0161 727 7188', '', '', 'Units 10/11 Shield Drive', 'Wardley Industrial Estate', 'Worsley', 'Manchester', 'M28 2QB', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1012, 'A12', 'Aerospace Surface Treatments Ltd', 'Aerospace ', 'Tattersall Way', 'Chelmsford Industrial Park', 'Chelmsford', 'Essex', 'CM1 3UB', '01245 350444', '01245 256925', '', '', 'Tattersall Way', 'Chelmsford Industrial Park', 'Chelmsford', 'Essex', 'CM1 3UB', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1013, 'A13', 'Ashleys of Yeovil Ltd', 'Ashleys ', 'West Hendford', 'Yeovil', 'Somerset', 'England', 'BA20 2AJ', '01935 423251', '01935 431177', '', '', 'West Hendford', 'Yeovil', 'Somerset', ' ', 'BA20 2AJ', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1014, 'A14', 'Aviation & Defence Spares Ltd', 'Aviation ', 'Unit 3 Branksome Business Park', 'Bourne Valley Road', 'Poole', 'Dorset', 'BH12 1DW', '01202 768667', '01202 768664', 'www.aviationdefence.co.uk', 'sales@aviationdefence.co.uk', 'Unit 3 Branksome Business Park', 'Bourne Valley Road', 'Poole', 'Dorset', 'BH12 1DW', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1015, 'A15', 'A & G Precision And Sons Ltd', 'A ', 'Unit 5 Preesall Mill Industrial Estate', 'Park Lane', 'Preesall', 'Lancashire', 'FY6 0YU', '01253 812451', '01253 812828', '', '', 'Unit 5 Preesall Mill Industrial Estate', 'Park Lane', 'Preesall', 'Lancashire', 'FY6 0YU', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1016, 'A16', 'AEM Limited', 'AEM ', 'Haine Industrial Park', '6-8 Wilton Road', 'Ramsgate', 'Kent', 'CT12 5HE', '01279 680030', '01279 680040', '', '', 'Haine Industrial Park', '6-8 Wilton Road', 'Ramsgate', 'Kent', 'CT12 5HE', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1018, 'A18', 'Atlantic Precision Engineering Ltd', 'Atlantic ', 'Aragon Road', 'Blackbushe Business Park', 'Yateley', 'Hants.', 'GU46 6ET', '(01252) 870225', '(01252) 871082', '', '', 'Aragon Road', 'Blackbushe Business Park', 'Yateley', 'Hants', 'GU46 6ET', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1020, 'A20', 'Airbus Operations Limited', 'Airbus ', 'Transit Facility Building 05H', 'Filton', 'Bristol', 'England', 'BS99 7AR', '0117 969 3831', '0117 936 2828', '', '', 'Transit Facility Building 05H', 'Filton', 'Bristol', ' ', 'BS99 7AR', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1101, 'B01', 'Bingham Precision Engineers Limited', 'Bingham ', '4 Saffron Court', 'Southfields Business Park', 'Basildon', 'Essex', 'SS15 6SS', '01268 411693', '01268 543934', '', '', '4 Saffron Court', 'Southfields Business Park', 'Basildon', 'Essex', 'SS15 6SS', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1102, 'B02', 'Bellhouse Hartwell and Co. Ltd', 'Bellhouse ', 'Greenvale Works', 'Leigh Road', 'Westhoughton', 'Lancashire', 'BL5 2JW', '(01942) 814000', '(01942) 814952', '', '', 'Greenvale Works', 'Leigh Road', 'Westhoughton', 'Lancashire', 'BL5 2JW', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1103, 'B03', 'BAE SYSTEMS (Operations) Ltd', 'BAE ', 'Goods Inwards', 'BROUGH', 'East Riding of Yorkshire', '', 'HU15 1EQ', '01482 667121', '01482 663776', '', '', 'Goods Inwards', 'BROUGH', 'East Riding of Yorkshire', '', 'HU15 1EQ', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1104, 'B04', 'BAE SYSTEMS (Operations) Limited (No Approval)', 'BAE ', 'European Logistics Centre ', '16 Vickers Drive', 'Brooklands Business Park', 'Weybridge Surrey', 'KT13 0UJ', '01292 67 5000', '01292 67 5700', '', '', 'European Logistics Centre ', '16 Vickers Drive', 'Brooklands Business Park', 'Weybridge Surrey', 'KT13 0UJ', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1107, 'B07', 'BAE Systems (Operations) Ltd', 'BAE ', 'c/o Product Support Limited (PSL)', 'Unit 12 Walker Industrial Park', 'Walker Road Guide', 'Blackburn Lancashire', 'BB1 2LJ', '0161 681 2020', '0161 683 5987', '', '', 'c/o Product Support Limited (PSL)', 'Unit 12 Walker Industrial Park', 'Walker Road Guide', 'Blackburn Lancashire', 'BB1 2LJ', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1108, 'B08', 'Bowmill Engineering Ltd', 'Bowmill ', '18-25 Morris Road', 'Nuffield', 'Poole ', 'Dorset', 'BH17 0GG', '01202 266266', '01202 665301', '', 'sales@bowmill.co.uk', '18-25 Morris Road', 'Nuffield', 'Poole ', 'Dorset', 'BH17 0GG', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1110, 'B10', 'BAE SYSTEMS (Operations) Ltd', 'BAE ', 'c/o Wincanton Group Limited', 'Unit 12 Walker Industrial Park', 'Walker Road Guide', 'Blackburn Lancashire', 'BB1 2LJ', '0161 955 8668', '0161 683 5987', '', '', 'c/o Wincanton Group Limited', 'Unit 12 Walker Industrial Park', 'Walker Road Guide', 'Blackburn Lancashire', 'BB1 2LJ', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1111, 'B11', 'Bridon International GmbH', 'Bridon ', 'Vicinay Cadenas SA', 'Particularde Sagarduy 5', '48015 Bilboa', 'Spain', '48015', '+49 (0) 209 8001 0', '+49 (0) 209 8001 275', '', '', 'Vicinay Cadenas SA', 'Particularde Sagarduy 5', '48015 Bilboa', 'Spain', '48015', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1114, 'B14', 'BCW Engineering Limited', 'BCW ', 'BCW B7 Unit 1 Widow Hill Court', 'Widow Hill Road', 'Heasandford Industrial Estate', 'Burnley', 'BB10 2TT', '01282 872474', '01282 872499', '', '', 'BCW B7 Unit 1 Widow Hill Court', 'Widow Hill Road', 'Heasandford Industrial Estate', 'Burnley', 'BB10 2TT', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1115, 'B15', 'Beagle Aircraft Ltd', 'Beagle ', 'Stony Lane', 'Christchurch', 'Dorset', 'England', 'BH23 1EX', '(01202) 482296', '(01202) 499449', 'www.smiths-aerospace.com', '', 'Stony Lane', 'Christchurch', 'Dorset', ' ', 'BH23 1EX', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1116, 'B16', 'BAE SYSTEMS (Operations) Limited', 'BAE ', 'Advanced Technology Centre', 'West Hanningfield Road', 'Great Baddow', 'Chelmsford Essex', 'CM2 8HN', '01245 473331', '01245 242340', '', '', 'Advanced Technology Centre', 'West Hanningfield Road', 'Great Baddow', 'Chelmsford Essex', 'CM2 8HN', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1117, 'B17', 'BAE SYSTEMS (Ops) Ltd', 'BAE ', 'EFA 3A (was EFA 4 Shed)', 'Samlesbury Aerodrome', 'Balderstone Blackburn', 'Lancashire', 'BB2 7LF', '01254 768632', '01254 766304', '', '', 'EFA 3A (was EFA 4 Shed)', 'Samlesbury Aerodrome', 'Balderstone Blackburn', 'Lancashire', 'BB2 7LF', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1120, 'B20', 'BAE SYSTEMS (Operations) Ltd', 'BAE ', 'c/o Wincanton Group Limited', 'Unit 12 Walker Industrial Park', 'Guide Blackburn', 'Lancashire', 'BB1 2LJ', '0161 955 8822', '0161 683 5987', '', '', 'c/o Wincanton Group Limited', 'Unit 12 Walker Industrial Park', 'Guide Blackburn', 'Lancashire', 'BB1 2LJ', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1123, 'B23', 'BAE SYSTEMS (Operations) Ltd', 'BAE ', 'Operations', 'Airport Works', 'Rochester', 'Kent', 'ME1 2XX', '01634 844400', '01634 816619', '', '', 'Operations', 'Airport Works', 'Rochester', 'Kent', 'ME1 2XX', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1201, 'C01', 'CASCO Limited', 'CASCO ', 'Star Estate', 'Partridge Green', 'Horsham', 'West Sussex', 'RH13 8RA', '01403 711444', '01403 711582', 'www.casco.aero', 'info@casco.aero', 'Star Estate', 'Partridge Green', 'Horsham', 'West Sussex', 'RH13 8RA', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1202, 'C02', 'Cookski', 'Cookski', '1 Linnets', 'Kingswood', 'Basildon', 'Essex', 'SS16 5ES', '01268 280290', '01268 280290', '', '', '1 Linnets', 'Kingswood', 'Basildon', 'Essex', 'SS16 5ES', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1203, 'C03', 'Chinn Limited', 'Chinn ', 'Coventry Road', 'Exhall', 'Coventry', 'England', 'CV7 9FT', '02476 369400', '02476 368000', '', '', 'Coventry Road', 'Exhall', 'Coventry', ' ', 'CV7 9FT', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1204, 'C04', 'CKC Engineering Services', 'CKC ', '14 Josselin Road', 'Burnt Mills Industrial Estate', 'Basildon', 'Essex', 'SS13 1QF', '01268 273188', '01268 725044', '', '', '14 Josselin Road', 'Burnt Mills Industrial Estate', 'Basildon', 'Essex', 'SS13 1QF', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1206, 'C06', 'Control Techniques Dynamics Ltd', 'Control ', 'South Way', 'Walworth Industrial Estate', 'Andover', 'Hampshire', 'SP10 5AB', '01264 387600', '01264 387693', '', '', 'South Way', 'Walworth Industrial Estate', 'Andover', 'Hampshire', 'SP10 5AB', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1210, 'C10', 'Centronic Limited', 'Centronic ', '267 King Henry\'s Drive', 'New Addington', 'Croydon', 'England', 'CR9 0BG', '01689 808004', '01689 808021', '', '', '267 King Henry\'s Drive', 'New Addington', 'Croydon', ' ', 'CR9 0BG', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1211, 'C11', 'Ceewrite Engineering Ltd (In Administration)', 'Ceewrite ', 'Tradecroft Unit 3', 'Wide Street', 'Portland', 'Dorset', 'DT5 2LN', '01305 822887', '01305 820273', 'www.ceewrite.co.uk', '', 'Tradecroft Unit 3', 'Wide Street', 'Portland', 'Dorset', 'DT5 2LN', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1213, 'C13', 'CMF Ltd', 'CMF ', '26-36 Horton Road', 'West Drayton', 'Middlesex', 'England', 'UB7 8JE', '0208 844 0940', '0208 751 5793', '', '', '26-36 Horton Road', 'West Drayton', 'Middlesex', 'England', 'UB7 8JE', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1215, 'C15', 'CompAir UK Limited', 'CompAir ', 'Reavell House', '53-56 White House Road', 'Ipswich', 'Suffolk', 'IP1 5PB', '01473 242000', '01473 747462', '', '', 'Reavell House', '53-56 White House Road', 'Ipswich', 'Suffolk', 'IP1 5PB', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1302, 'D02', 'Doncasters Bramah', 'Doncasters ', 'Holbrook Works', 'Station Road', 'Halfway', 'Sheffield', 'S20 3GB', '0114 251 2000', '0114 251 4720', '', '', 'Holbrook Works', 'Station Road', 'Halfway', 'Sheffield', 'S20 3GB', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1307, 'D07', 'Dunkermotoren Linear Systems Ltd', 'Dunkermotoren ', 'Wollaston Way', 'Burnt Mill Industrial Estate', 'Basildon', 'Essex', 'SS13 1DJ', '01268 287070', '01268 928372', '', '', 'Wollaston Way', 'Burnt Mill Industrial Estate', 'Basildon', 'Essex', 'SS13 1DJ', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1312, 'D12', 'Doncasters Bramah', 'Doncasters ', 'Holbrook Works', 'Station Road', 'Halfway', 'Sheffield', 'S20 3GB', '0114 251 2000', '0114 251 4720', '', '', 'Holbrook Works', 'Station Road', 'Halfway', 'Sheffield', 'S20 3GB', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1401, 'E01', 'ELE Advanced Technologies Ltd', 'ELE ', 'Work in Progress - Colne', 'Skipton Road', 'Colne', 'Lancashire', 'BB8 0PF', '01282 856800', '01282 867394', '', '', 'Work in Progress - Colne', 'Skipton Road', 'Colne', 'Lancashire', 'BB8 0PF', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1402, 'E02', 'Eaton Aerospace Limited', 'Eaton ', 'Control Cell - Building 2', 'Abbey Park', 'Titchfield Fareham', 'Hampshire', 'PO14 4QA', '01329 853000', '01329 853796', '', '', 'Control Cell - Building 2', 'Abbey Park', 'Titchfield Fareham', 'Hampshire', 'PO14 4QA', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1403, 'E03', 'EMD Drive Systems Limited', 'EMD ', 'C/O Parvalux Electric Motors Ltd', '490-492 Wallisdown Road', 'Bournemouth', 'England', 'BH11 8PU', '01202 512575', '01202 530885', '', '', 'C/O Parvalux Electric Motors Ltd', '490-492 Wallisdown Road', 'Bournemouth', 'England', 'BH11 8PU', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1404, 'E04', 'Eamont Products Ltd', 'Eamont ', '44 Bowlers Croft', 'Basildon', 'Essex', 'England', 'SS14 3ED', '01268 288028', '01268 272345', '', '', '44 Bowlers Croft', 'Basildon', 'Essex', 'England', 'SS14 3ED', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1501, 'F01', 'FR-HiTemp Limited', 'FR-HiTemp ', 'Control Cell - Building 2', 'Abbey Park', 'Titchfield Fareham', 'Hampshire', 'PO14 4QA', '01329 853000', '01329 853797', '', '', 'Control Cell - Building 2', 'Abbey Park', 'Titchfield Fareham', 'Hampshire', 'PO14 4QA', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1505, 'F05', 'FGP Precision Engineering Limited', 'FGP ', 'Albany Court Albany Road', 'Granby Industrial Estate', 'Weymouth', 'Dorset', 'DT4 9TH', '(01305) 773638', '(01305) 760093', '', 'fgpoffice@fgpltd.com', 'Albany Court Albany Road', 'Granby Industrial Estate', 'Weymouth', 'Dorset', 'DT4 9TH', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1601, 'G01', 'Gardner Aerospace - Basildon Limited', 'Gardner ', 'Rear of Factory', 'Wollaston Crescant', 'Burnt Mills Industrial Estate', 'Basildon Essex', 'SS13 1QQ', '01268 729311', '01268 728951', '', 'Info@gardner-aerospace-basildon.com', 'Rear of Factory', 'Wollaston Crescant', 'Burnt Mills Industrial Estate', 'Basildon Essex', 'SS13 1QQ', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1603, 'G03', 'Gardner Aerospace - Burnley Limited', 'Gardner ', 'Hargher Clough Works', 'Hargher Street', 'Burnley', 'England', 'BB11 4EG', '01282 416466', '01282 450363', '', 'info@gardner-aerospace.com', 'Hargher Clough Works', 'Hargher Street', 'Burnley', ' ', 'BB11 4EG', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1604, 'G04', 'Gardner Aerospace - Nuneaton Ltd', 'Gardner ', 'Whitacre Road Industrial Estate', 'Nuneaton', 'Warwickshire', 'England', 'CV11 6BP', '02476 350888', '02476 381615', '', 'info@gardner-aerospace.com', 'Whitacre Road Industrial Estate', 'Nuneaton', 'Warwickshire', ' ', 'CV11 6BP', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1605, 'G05', 'GAP Engineering Limited', 'GAP ', 'Pitwood Park Industrial Estate', 'Waterfield', 'Tadworth', 'Surrey', 'KT20 5JL', '01737 359416', '01737 370331', '', '', 'Pitwood Park Industrial Estate', 'Waterfield', 'Tadworth', 'Surrey', 'KT20 5JL', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1606, 'G06', 'Gibbs Gears Precision Engineers Ltd', 'Gibbs ', 'Units 3 & 4 Triangle Business Park', 'Quilters Way', 'Stoke Mandeville', 'Bucks', 'HP22 5BL', '01296 739020', '01296 739039', '', 'sales@gibbsgears.com', 'Units 3 & 4 Triangle Business Park', 'Quilters Way', 'Stoke Mandeville', 'Bucks', 'HP22 5BL', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1607, 'G07', 'G.Webb Automation Limited', 'G.Webb ', 'Link Industrial Estate', 'Howsell Road', 'Malvern Link', 'Worcs', 'WR14 1TF', '01684 892929', '01684 576641', '', '', 'Link Industrial Estate', 'Howsell Road', 'Malvern Link', 'Worcs', 'WR14 1TF', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1609, 'G09', 'Gardner Denver Ltd', 'Gardner ', 'Production', 'Reavell House', '53-56 White Horse Road', 'Ipswich Suffolk', 'IP1 5PB', '01473 242149', '01473 747462', '', '', 'Production', 'Reavell House', '53-56 White Horse Road', 'Ipswich Suffolk', 'IP1 5PB', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1701, 'H01', 'Honeywell Aerospace UK', 'Honeywell ', 'Wincanton', 'DSE Building 197', 'Bunford Lane', 'Yeovil Somerset', 'BA20 2YD', '01935 475181', '01935 446582', '', '', 'Wincanton', 'DSE Building 197', 'Bunford Lane', 'Yeovil Somerset', 'BA20 2YD', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1702, 'H02', 'Horstman Defence Systems Limited', 'Horstman ', 'Locksbrook Road', 'Bath & NES', 'Avon', 'England', 'BA1 3EX', '01225 423111', '01225 428676', '', '', 'Locksbrook Road', 'Bath & NES', 'Avon', ' ', 'BA1 3EX', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1703, 'H03', 'Hampson (BHW Components Ltd)', 'Hampson ', 'Worthington Way', 'Hawkley Brook', 'Wigan', 'England', 'WN3 6XE', '01942 821205', '01942 829552/821377', '', '', 'Worthington Way', 'Hawkley Brook', 'Wigan', ' ', 'WN3 6XE', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1704, 'H04', 'Honeywell Aerospace UK', 'Honeywell ', 'Repair & Overhaul', 'Aviation Park West', 'Bournemouth Airport', 'Christchurch Dorset', 'BH23 6EW', '01202 581818', '01202 581919', '', '', 'Repair & Overhaul', 'Aviation Park West', 'Bournemouth Airport', 'Christchurch Dorset', 'BH23 6EW', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1711, 'H11', 'Hyde Precision Components Ltd', 'Hyde ', 'Oldham Street', 'Denton', 'Manchester', 'England', 'M34 3SA', '0161 337 9242', '0161 335 0787', '', '', 'Oldham Street', 'Denton', 'Manchester', ' ', 'M34 3SA', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1713, 'H13', 'Hyde Aero Products Limited', 'Hyde ', 'Tudor Works', 'Ashton Street', 'Dukinfield', 'Cheshire', 'SK16 4RR', '0161 343 5844', '0161 343 5833', '', '', 'Tudor Works', 'Ashton Street', 'Dukinfield', 'Cheshire', 'SK16 4RR', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1803, 'I03', 'IEC Engineering Ltd', 'IEC ', 'Brookside Avenue', 'Rustington', 'West Sussex', 'England', 'BN16 3LF', '(01903) 773337', '(01903) 786619', '//www.ieceng.co.uk', 'info@ieceng.co.uk', 'Brookside Avenue', 'Rustington', 'West Sussex', ' ', 'BN16 3LF', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1804, 'I04', 'Instro Precision Limited', 'Instro ', 'Hornet Close', 'Pysons Road Industrial Estate', 'Broadstairs', 'Kent', 'CT10 2YD', '01843 604455', '01843 861032', '', 'purchasing@intro.com', 'Hornet Close', 'Pysons Road Industrial Estate', 'Broadstairs', 'Kent', 'CT10 2YD', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1805, 'I05', 'Inflite Engineering Services Ltd', 'Inflite Manchester', 'Broadlink Industrial Estate', 'Moston Road', 'Middleton Oldham', 'Manchester', 'M24 1UB', '0161 643 6668', '0161 654 4050', '', '', 'Broadlink Industrial Estate', 'Moston Road', 'Middleton Oldham', 'Manchester', 'M24 1UB', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1806, 'I06', 'Inflite Engineering Services Ltd', 'Inflite Herts', 'Woodside Industrial Estate (W1)', 'Dunmow Road', 'Bishops Stortford', 'Hertfordshire', 'CM23 5RG', '01279 837837/681681', '01279 311293', '', '', 'Woodside Industrial Estate (W1)', 'Dunmow Road', 'Bishops Stortford', 'Hertfordshire', 'CM23 5RG', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(1903, 'J03', 'Joy Mining Machinery Ltd', 'Joy ', 'Worcester Plant', 'Bromyard Road', 'Worcester', 'England', 'WR2 5EG', '0870 252 1000', '0870 252 1888', '', '', 'Worcester Plant', 'Bromyard Road', 'Worcester', ' ', 'WR2 5EG', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2001, 'K01', 'Kilgour Industries Limited', 'Kilgour ', 'Red Marsh Drive', 'Red Marsh Industrial Estate', 'Thornton-Cleveleys', 'Lancashire', 'FY5 4HP', '01253 821953', '01253 823126', '', '', 'Red Marsh Drive', 'Red Marsh Industrial Estate', 'Thornton-Cleveleys', 'Lancashire', 'FY5 4HP', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2002, 'K02', 'Kongsberg Automotive', 'Kongsberg ', 'Christopher Martin Road', 'Basildon', 'Essex', 'England', 'SS14 3ES', '01268 595098', '', '', '', 'Christopher Martin Road', 'Basildon', 'Essex', ' ', 'SS14 3ES', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2104, 'L04', 'Lentern (Aircraft) Limited', 'Lentern ', 'Unit A', '20 Stephenson Road', 'Leigh-on-Sea', 'Essex', 'SS9 5LS', '01702 202553', '01702 206248/200910', '', '', 'Unit A', '20 Stephenson Road', 'Leigh-on-Sea', 'Essex', 'SS9 5LS', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2203, 'M03', 'Ametek Airtechnology Group Limited', 'Ametek ', 'Muirhead Aerospace', 'Oakfield Road', 'Penge', 'London', 'SE20 8EW', '020 8659 9090', '020 8659 1177', 'www.muirheadaerospace.com', 'sales@muirheadaerospace.com', 'Muirhead Aerospace', 'Oakfield Road', 'Penge', 'London', 'SE20 8EW', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2205, 'M05', 'Magellan Aerospace Structures', 'Magellan ', 'Rackery Lane', 'Llay Industrial Estate', 'Llay', 'Wrexham', 'LL12 0PB', '01978 852101', '01978 856279', '', '', 'Rackery Lane', 'Llay Industrial Estate', 'Llay', 'Wrexham', 'LL12 0PB', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2206, 'M06', 'Ametek Airtechnology Group Ltd', 'Ametek ', 'Muirhead Aerospace', 'Ametek Corporation', 'East Portway', 'Andover Hampshire', 'SP10 3LU', '01264 349600', '01264 336444', '', '', 'Muirhead Aerospace', 'Ametek Corporation', 'East Portway', 'Andover Hampshire', 'SP10 3LU', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2209, 'M09', 'Midland Aerospace Limited', 'Midland ', 'Unit 5 Orchard Way', 'Calladine Park', 'Sutton-in-Ashfield', 'Nottingham', 'NG17 1JU', '01623 446556', '01623 446546', '', '', 'Unit 5 Orchard Way', 'Calladine Park', 'Sutton-in-Ashfield', 'Nottingham', 'NG17 1JU', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2213, 'M13', 'Ametek Airtechnology Group Limited', 'Ametek ', 'Muirhead Aerospace Limited', 'Oakfield Road', 'Penge', 'London', 'SE20 8EW', '0208 659 9090', '0208 659 1177', '', '', 'Muirhead Aerospace Limited', 'Oakfield Road', 'Penge', 'London', 'SE20 8EW', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2301, 'N01', 'Nuneaton Precisions Ltd', 'Nuneaton ', 'Veasey Close', 'Attleborough Fields Industrial Estate', 'Nuneaton', 'Warwickshire', 'CV11 6RT', '024 7634 3116', '024 7664 2355', 'www.nuneaton-precisions.com', 'sales@nuneaton-precisions.com', 'Veasey Close', 'Attleborough Fields Industrial Estate', 'Nuneaton', 'Warwickshire', 'CV11 6RT', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2302, 'N02', 'Nu-Pro Surface Treatments Ltd', 'Nu-Pro ', 'Eagle Works', 'London Road', 'Thrupp Stroud', 'Gloucester', 'GL5 2BA', '01453 883344', '01453 731597', '', '', 'Eagle Works', 'London Road', 'Thrupp Stroud', 'Gloucester', 'GL5 2BA', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2303, 'N03', 'Nginuity Limited', 'Nginuity ', 'Machining Division', 'Brookside Avenue', 'Rustington', 'West Sussex', 'BN16 3LF', '01903 773337', '01903 786618', '', '', 'Machining Division', 'Brookside Avenue', 'Rustington', 'West Sussex', 'BN16 3LF', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2304, 'N04', 'NMB-Minebea UK Limited', 'NMB-Minebea ', 'Doddington Road', 'Lincoln', ' ', 'England', 'LN6 3RA', '01522 500933', '01522 503841', '', '', 'Doddington Road', 'Lincoln', ' ', ' ', 'LN6 3RA', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2305, 'N05', 'Norcot Engineering Ltd', 'Norcot ', 'Richmond House', 'Hill Street', 'Ashton-Under-Lyne', 'Manchester', 'OL7 0PZ', '0161 339 9361', '0161 343 3069', 'www.norcot.com', 'info@norcot.com', 'Richmond House', 'Hill Street', 'Ashton-Under-Lyne', 'Manchester', 'OL7 0PZ', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2314, 'N14', 'NMB-Minebea UK Limited', 'NMB-Minebea ', 'Doddington Road', 'Lincoln', ' ', 'England', 'LN6 3RA', '01522 500933', '01522 503841', '', '', 'Doddington Road', 'Lincoln', ' ', ' ', 'LN6 3RA', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2403, 'P03', 'P.G. Technology', 'P.G. ', '5 Falcon Business Centre', '2-4 Willow Lane', 'Mitcham', 'Surrey', 'CR4 4NA', '0208 648 9461', '0208 685 9638', 'www.pgtechnology.co.uk', 'purchasing@pgtechnology.co.uk', '5 Falcon Business Centre', '2-4 Willow Lane', 'Mitcham', 'Surrey', 'CR4 4NA', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2404, 'P04', 'Pattonair (International) Limited', 'Pattonair ', 'Kingswey Business Park', 'Forsyth Road', 'Sheerwater', 'Woking Surrey   ', 'GU21 5SA', '01483 774600', '01483 774619', '', '', 'Kingswey Business Park', 'Forsyth Road', 'Sheerwater', 'Woking Surrey   ', 'GU21 5SA', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2405, 'P05', 'Phoenix CNC Engineering Ltd', 'Phoenix ', 'Acton Road', 'Long Eaton', 'Nottingham', 'England', 'NG10 1FU', '0115 946 9896', '0115 972 3900', '', '', 'Acton Road', 'Long Eaton', 'Nottingham', '', 'NG10 1FU', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2406, 'P06', 'Polskie Zaklady Lotnicze Sp. z o.o.', 'Polskie ', 'ul. Wojska Polskiego 3', '39-300 Mielec', 'POLAND', 'England', '', '00 48 17 788 7471', '00 48 17 788 6955', '', 'pzlpurchase@ptc.pl', 'ul. Wojska Polskiego 3', '39-300 Mielec', 'POLAND', '', '39-300', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2407, 'P07', 'Parvalux Electric Motors Limited', 'Parvalux ', '490-492 Wallisdown Road', 'Bournemouth', 'Dorset', 'England', 'BH11 8PU', '01202 512575', '01202 530885', '', '', '490-492 Wallisdown Road', 'Bournemouth', 'Dorset', '', 'BH11 8PU', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2408, 'P08', 'Parker Hannifin Ltd', 'Parker ', 'Hydraulic Controls Division', 'Tachbrook Park Drive', 'Tachbrook Park', 'Warwick Warwickshire', 'CV34 6TU', '01926 833700', '01926 889172', '', '', 'Hydraulic Controls Division', 'Tachbrook Park Drive', 'Tachbrook Park', 'Warwick Warwickshire', 'CV34 6TU', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2409, 'P09', 'Protocon Engineering Ltd', 'Protocon ', 'Stock Road', 'Southend-on-Sea', 'Essex', 'England', 'SS2 5QF', '01702 612312', '01702 461717', '', '', 'Stock Road', 'Southend-on-Sea', 'Essex', '', 'SS2 5QF', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2422, 'P22', 'Pennine Tools Aerospace Ltd', 'Pennine ', 'Unit 1', 'Ravenscroft Business Park', 'Jackdaw Road', 'Barnoldswick Lancashire', 'BB18 6DX', '01282 815555', '01282 813831', '', '', 'Unit 1', 'Ravenscroft Business Park', 'Jackdaw Road', 'Barnoldswick Lancashire', 'BB18 6DX', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2501, 'Q01', 'Quorum Technologies Ltd', 'Quorum ', '1 & 2 Eden Business Centre', 'South Stour Avenue', 'Ashford', 'Kent', 'TN23 7RS', '01233 652661', '01233 640744', '', '', '1 & 2 Eden Business Centre', 'South Stour Avenue', 'Ashford', 'Kent', 'TN23 7RS', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2601, 'R01', 'Robert Pringle Engineers Limited', 'Robert ', 'Network House', 'Perry Road', 'Harlow', 'Essex', 'CM18 7ND', '01279 418300', '01279 418100', '', '', 'Network House', 'Perry Road', 'Harlow', 'Essex', 'CM18 7ND', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2603, 'R03', 'Roseville Group Limited', 'Roseville ', 'Guildprime Business Centre', 'Southend Road', 'Billericay', 'Essex', 'CM11 2PZ', '01277 630101', '01277 637590', '', '', 'Guildprime Business Centre', 'Southend Road', 'Billericay', 'Essex', 'CM11 2PZ', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2604, 'R04', 'Redcliffe Precision Ltd', 'Redcliffe ', '46 Ashton Vale Road', 'Bristol', ' ', 'England', 'BS3 2HQ', '0117 963 5616', '0117 963 5674', '', '', '46 Ashton Vale Road', 'Bristol', ' ', ' ', 'BS3 2HQ', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2606, 'R06', 'Regal Precision Engineers(Colne)Ltd', 'Regal ', 'Walton St Works', 'Walton Street', 'Colne', 'Lancashire', 'BB8 0EL', '01282 864347', '01282 860209', 'www.rpe-colne.co.uk', 'info@rpe-colne.co.uk', 'Walton St Works', 'Walton Street', 'Colne', 'Lancashire', 'BB8 0EL', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2607, 'R07', 'Rose Bearings Ltd', 'Rose ', 'Doddington Road', 'Lincoln', ' ', 'England', 'LN6 3RA', '01522 500933', '01522 503841', '', '', 'Doddington Road', 'Lincoln', ' ', ' ', 'LN6 3RA', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2608, 'R08', 'Rewinds & J. Windsor & Sons (Engineers) Ltd', 'Rewinds ', 'Units 7 & 8 Poulton Industrial Estate', 'Westfield Road', 'Wallasey', 'Wirral', 'CH44 7HZ', '0151 652 1315', '0151 653 6826', '', '', 'Units 7 & 8 Poulton Industrial Estate', 'Westfield Road', 'Wallasey', 'Wirral', 'CH44 7HZ', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2701, 'S01', 'Stanworth Engineers Limited', 'Stanworth ', 'Brown Street', 'Burnley', 'Lancashire', 'England', 'BB11 1PN', '01282 421427', '01282 458318', '', '', 'Brown Street', 'Burnley', 'Lancashire', '', 'BB11 1PN', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2703, 'S03', 'Stimpson Engineering Limited', 'Stimpson ', 'Unit 1 & 2', 'Old Saw Mills Industrial Estate', 'Broughton Gifford', 'Melksham Wiltshire', 'SN12 8PH', '01225 782904', '01225 782999', '', 'enquires@stimpstonengineering.co.uk', 'Unit 1 & 2', 'Old Saw Mills Industrial Estate', 'Broughton Gifford', 'Melksham Wiltshire', 'SN12 8PH', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2705, 'S05', 'SPS Aerostructures Ltd', 'SPS ', 'Willow Drive', 'Sherwood Business Park', 'Annesley', 'Nottinghamshire', 'NG15 0DP', '0115 988 0000', '0115 988 0001', '', '', 'Willow Drive', 'Sherwood Business Park', 'Annesley', 'Nottinghamshire', 'NG15 0DP', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2706, 'S06', 'Selex Sensors & Airborne Systems Ltd', 'Selex ', 'Phase 1', '2 Crewe Road North', 'Edinburgh', 'Scotland', 'EH5 2XS', '0131 343 5414', '0131 343 5415', '', '', 'Phase 1', '2 Crewe Road North', 'Edinburgh', 'Scotland', 'EH5 2XS', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2707, 'S07', 'S.G. Brown', 'S.G. ', '1 Garnett Close', 'Greycaine Industrial Estate', 'Watford', 'England', 'WD24 7JZ', '(01923) 470807', '(01923) 244849', '', '', '1 Garnett Close', 'Greycaine Industrial Estate', 'Watford', ' ', 'WD24 7JZ', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2708, 'S08', 'John Sills Limited', 'John ', '65 East Barnet Road', 'New Barnet', 'Hertfordshire', 'England', 'EN4 8RN', '0208 449 7363', '0208 441 2907', '', '', '65 East Barnet Road', 'New Barnet', 'Hertfordshire', ' ', 'EN4 8RN', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2711, 'S11', 'Selex Galileo Ltd', 'Selex ', 'Christopher Martin Road', 'Basildon', 'Essex', 'England', 'SS14 3EL', '01268 885292', '01268 887398', '', '', 'Christopher Martin Road', 'Basildon', 'Essex', ' ', 'SS14 3EL', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2801, 'T01', 'Teledyne TSS Limited', 'Teledyne ', '1 Garnett Close', 'Greycaine Industrial Estate', 'Watford', 'Herts', 'WD24 7GL', '01923 470800', '01923 244849', '', '', '1 Garnett Close', 'Greycaine Industrial Estate', 'Watford', 'Herts', 'WD24 7GL', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2803, 'T03', 'Textile Automation Sales Ltd.', 'Textile ', 'Unit 9 North\'s Estate', 'Old Oxford Road', 'Piddington', 'High Wycombe Bucks', 'HP14 3BE', '01494 882772', '0800 0664522', '', '', 'Unit 9 North\'s Estate', 'Old Oxford Road', 'Piddington', 'High Wycombe Bucks', 'HP14 3BE', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2804, 'T04', 'Thales Optics Limited', 'Thales ', 'Glascoed Road', 'St Asaph', 'Denbighshire', 'England', 'LL17 0LL', '01745 588000', '01745 584963', '', '', 'Glascoed Road', 'St Asaph', 'Denbighshire', ' ', 'LL17 0LL', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2805, 'T05', 'Trig Engineering Ltd', 'Trig ', 'Huntworth Business Park', 'Bridgwater', 'Somerset', 'England', 'TA6 6TS', '01278 440000', '01278 444455', '', '', 'Huntworth Business Park', 'Bridgwater', 'Somerset', ' ', 'TA6 6TS', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2808, 'T08', 'Trefn Engineering Ltd', 'Trefn ', 'Rackery Lane', 'Llay Industrial Estate', 'Llay', 'Wrexham', 'LL12 0PB', '01978 852101', '01978 854439', '', '', 'Rackery Lane', 'Llay Industrial Estate', 'Llay', 'Wrexham', 'LL12 0PB', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2809, 'T09', 'Triumph Controls - UK Ltd', 'Triumph ', 'Christopher Martin Road', 'Basildon', 'Essex', 'England', 'SS14 3ES', '(01268) 270195', '(01268) 284608', '', '', 'Christopher Martin Road', 'Basildon', 'Essex', ' ', 'SS14 3ES', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2810, 'T10', 'T & R Precision Engineering Ltd', 'T & R Precision', 'Lowther Lane', 'Foulridge', 'Colne', 'Lancashire', 'BB8 7JY', '(01282) 862116', '(01282) 867036', '', '', 'Lowther Lane', 'Foulridge', 'Colne', 'Lancashire', 'BB8 7JY', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2811, 'T11', 'Tooling and Equipment Engineers Ltd', 'Tooling ', '114A Earlsdon Avenue South', 'Earlsdon', 'Coventry', 'England', 'CV5 6DN', '024 7669 1522', '024 7669 1544', '', 'info@tandee.co.uk', '114A Earlsdon Avenue South', 'Earlsdon', 'Coventry', ' ', 'CV5 6DN', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2901, 'U01', 'Unigears (UK) Limited', 'Unigears ', 'Unit 8 Henwood Business Centre', 'Henwood Estate', 'Ashford', 'Kent', 'TN24 8DH', '01233 642798', '01233 650725', 'www.Unigears.co.uk', '', 'Unit 8 Henwood Business Centre', 'Henwood Estate', 'Ashford', 'Kent', 'TN24 8DH', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2903, 'U03', 'UFC Aerospace Europe Ltd', 'UFC ', '2 Cramptons Road', 'Sevenoaks', 'Kent', 'England', 'TN14 5EF', '01732 450011', '01732 455884', '', '', '2 Cramptons Road', 'Sevenoaks', 'Kent', ' ', 'TN14 5EF', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(2904, 'U04', 'Ultra Electronics', 'Ultra ', 'Precision Air & Land Systems', 'Kingsditch Lane', 'Cheltenham', 'Gloucestershire', 'GL51 9PG', '01242 221166', '01242 221167', 'www.ultra-electrics.com', '', 'Precision Air & Land Systems', 'Kingsditch Lane', 'Cheltenham', 'Gloucestershire', 'GL51 9PG', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(3002, 'V02', 'Victoria Production Engineering Ltd', 'Victoria ', 'Oldham Street', 'Denton', 'Manchester', 'England', 'M34 3SA', '0161 320 1800', '0161 320 1810', '', '', 'Oldham Street', 'Denton', 'Manchester', ' ', 'M34 3SA', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(3005, 'V05', 'VT Controls', 'VT ', 'Units 10/11 Shield Drive', 'Wardley Industrial Estate', 'Worsley', 'Manchester', 'M28 2QB', '0161 727 9927', '0161 727 7188', '', '', 'Units 10/11 Shield Drive', 'Wardley Industrial Estate', 'Worsley', 'Manchester', 'M28 2QB', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(3007, 'V07', 'VT TSS Ltd', 'VT TSS', 'S.G. Brown Division', '1 Garnett Close', 'Greycaine Industrial Estate', 'Watford', 'WD24 7JZ', '(01923) 470807', '(01923) 244849', '', '', 'S.G. Brown Division', '1 Garnett Close', 'Greycaine Industrial Estate', 'Watford', 'WD24 7JZ', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(3101, 'W01', 'Washington Precision Engineering (NSE) Limited', 'Washington ', 'Units 5-6 Tilley Road', 'Crowther Industrial Estate', 'District 3 Washington', 'Tyne & Wear', 'NE38 0AE', '0191 416 1564', '0191 415 3712', '', '', 'Units 5-6 Tilley Road', 'Crowther Industrial Estate', 'District 3 Washington', 'Tyne & Wear', 'NE38 0AE', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(3402, 'O02', 'OTM Servo Mechanism Ltd', 'OTM ', 'The Avenue', 'Egham', 'Surrey', 'England', 'TW20 9AL', '01784 433155', '01784 471307', '', '', 'The Avenue', 'Egham', 'Surrey', ' ', 'TW20 9AL', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL),
(3407, 'O07', 'Oliver Valves Ltd', 'Oliver ', 'Haig Road', 'Parkgate Industrial Estate', 'Knutsford', 'Cheshire', 'WA16 8DX', '01565 632636', '01565 652014', '', '', 'Haig Road', 'Parkgate Industrial Estate', 'Knutsford', 'Cheshire', 'WA16 8DX', NULL, 1, 1, 1, '1000', 30, 2, 3, 1, '0', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customeradditionaltime`
--

CREATE TABLE `customeradditionaltime` (
  `pkAdditionalID` mediumint(8) UNSIGNED NOT NULL COMMENT 'medint 24/3/16',
  `fkCustomerID` smallint(5) UNSIGNED DEFAULT NULL,
  `TimeType` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1= Shutdown, 2 = collection , 3 = other',
  `StartPoint` varchar(10) DEFAULT NULL COMMENT 'can be day name or date (without year)',
  `EndPoint` varchar(10) DEFAULT NULL,
  `Deleted` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 24/3/16',
  `UpdatedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 24/3/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='additional collection / shut down times';

--
-- Dumping data for table `customeradditionaltime`
--

INSERT INTO `customeradditionaltime` (`pkAdditionalID`, `fkCustomerID`, `TimeType`, `StartPoint`, `EndPoint`, `Deleted`, `UpdatedByfkUserID`) VALUES
(1, 1, 1, '1st Aug', '14th Aug', 0, 2),
(2, 1, 2, 'Tuesday', 'Tuesday', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customerapproval`
--

CREATE TABLE `customerapproval` (
  `fkCustomerID` smallint(6) UNSIGNED DEFAULT NULL,
  `fkApprovalID` tinyint(4) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customercontact`
--

CREATE TABLE `customercontact` (
  `pkContactID` smallint(6) UNSIGNED NOT NULL,
  `fkCustomerID` smallint(6) UNSIGNED NOT NULL,
  `ContactName` varchar(45) DEFAULT NULL,
  `Position` varchar(25) DEFAULT NULL,
  `Phone` varchar(13) DEFAULT NULL,
  `Extension` smallint(6) UNSIGNED DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `AcknowContact` tinyint(4) UNSIGNED DEFAULT NULL COMMENT 'specifies the contact for Acknowledgements to be sent to',
  `PrimaryContact` tinyint(4) UNSIGNED DEFAULT NULL,
  `Deleted` tinyint(4) DEFAULT NULL,
  `fkUserIDUpdated` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 29/2/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customercontact`
--

INSERT INTO `customercontact` (`pkContactID`, `fkCustomerID`, `ContactName`, `Position`, `Phone`, `Extension`, `Email`, `AcknowContact`, `PrimaryContact`, `Deleted`, `fkUserIDUpdated`) VALUES
(1, 1, 'Cuthbert', 'tealady', '123345567', 123, 'b@b.com', 1, 0, 0, 2),
(2, 1, 'D Phillips', 'Captain Access', '07973 166881', 1234, 'd.phillips@eslengineers.co.uk', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customernote`
--

CREATE TABLE `customernote` (
  `pkCustomerNoteID` mediumint(8) UNSIGNED NOT NULL COMMENT 'medint 24/3/16',
  `fkCustomerID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'smallint 24/3/16',
  `Note` varchar(100) DEFAULT NULL,
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 15/2/16. unsigned all fields.',
  `Deleted` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 24/3/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customernote`
--

INSERT INTO `customernote` (`pkCustomerNoteID`, `fkCustomerID`, `Note`, `AddedByfkUserID`, `Deleted`) VALUES
(1, 1, 'a lovely note that I dreamt up myself sxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxe', 2, 0),
(2, 1, 'just a note test of noteworthiness', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customerorder`
--

CREATE TABLE `customerorder` (
  `pkOrderID` mediumint(9) UNSIGNED ZEROFILL NOT NULL,
  `fkCustomerID` smallint(5) UNSIGNED NOT NULL,
  `CustomerOrderRef` varchar(25) DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreationfkUserID` int(11) UNSIGNED DEFAULT NULL,
  `fkApprovalID` tinyint(4) UNSIGNED DEFAULT NULL,
  `fkDeliveryPointID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Rmvd invoice address 20/1/16',
  `CommerciallyViable` tinyint(3) UNSIGNED DEFAULT NULL,
  `AmendmentReviewed` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customerorder`
--

INSERT INTO `customerorder` (`pkOrderID`, `fkCustomerID`, `CustomerOrderRef`, `CreationDate`, `CreationfkUserID`, `fkApprovalID`, `fkDeliveryPointID`, `CommerciallyViable`, `AmendmentReviewed`) VALUES
(000000001, 1, 'ABC123ST16bCO', '2016-11-16 00:00:00', 4, 2, 3, 0, 0),
(000000002, 1, 'ABC123ST17CO', '2016-11-17 00:00:00', 4, 3, 3, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `customerordernote`
--

CREATE TABLE `customerordernote` (
  `pkOrderNoteID` mediumint(8) UNSIGNED NOT NULL,
  `fkOrderID` mediumint(8) UNSIGNED DEFAULT NULL,
  `Note` varchar(80) DEFAULT NULL,
  `NoteDate` date DEFAULT NULL COMMENT 'added 25/3/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customerordernote`
--

INSERT INTO `customerordernote` (`pkOrderNoteID`, `fkOrderID`, `Note`, `NoteDate`) VALUES
(1, 2, 'ABC123ST17 CO note 1', '2016-11-17'),
(2, 2, 'ABC123ST17 CO note 2', '2016-11-17');

-- --------------------------------------------------------

--
-- Table structure for table `customerpartmark`
--

CREATE TABLE `customerpartmark` (
  `fkCustomerID` smallint(5) UNSIGNED DEFAULT NULL,
  `fkPartMarkReqID` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customerpersonnel`
--

CREATE TABLE `customerpersonnel` (
  `pkCustomerPersonnelID` int(11) NOT NULL,
  `fkCustomerID` smallint(6) DEFAULT NULL,
  `PersonnelType` tinyint(4) DEFAULT NULL COMMENT '1= Manu. (Direct), 2 = Manu. (indirect), 3 = Quality, 4 = Engineering, 5 = Other',
  `Number` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `deliverycomment`
--

CREATE TABLE `deliverycomment` (
  `pkDeliveryCommentID` smallint(5) UNSIGNED NOT NULL,
  `DeliveryComment` varchar(45) DEFAULT NULL,
  `Deleted` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1 = yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='added 3/2/16';

--
-- Dumping data for table `deliverycomment`
--

INSERT INTO `deliverycomment` (`pkDeliveryCommentID`, `DeliveryComment`, `Deleted`) VALUES
(1, 'Ring the bell twice and run!', 0),
(2, 'A more appropriate delivery comment', 0);

-- --------------------------------------------------------

--
-- Table structure for table `deliverycontact`
--

CREATE TABLE `deliverycontact` (
  `pkDeliveryContactID` smallint(6) UNSIGNED NOT NULL,
  `fkDeliveryPointID` smallint(6) UNSIGNED DEFAULT NULL,
  `ContactName` varchar(45) DEFAULT NULL,
  `Phone` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `deliverypoint`
--

CREATE TABLE `deliverypoint` (
  `pkDeliveryPointID` smallint(6) UNSIGNED NOT NULL,
  `fkCustomerID` smallint(6) UNSIGNED DEFAULT NULL,
  `Address1` varchar(45) DEFAULT NULL,
  `Address2` varchar(40) DEFAULT NULL,
  `Address3` varchar(40) DEFAULT NULL,
  `Address4` varchar(30) DEFAULT NULL,
  `Postcode` char(8) DEFAULT NULL,
  `Deleted` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 16/2/16',
  `DeletedByfkUserID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 16/2/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `deliverypoint`
--

INSERT INTO `deliverypoint` (`pkDeliveryPointID`, `fkCustomerID`, `Address1`, `Address2`, `Address3`, `Address4`, `Postcode`, `Deleted`, `DeletedByfkUserID`) VALUES
(3, 1, 'Test Delivery point address cust1 sxxxxxxxxe', 'Test Delivery point address 2 sxxxxxxxxe', 'Test Delivery point address 3 sxxxxxxxxe', 'Test Delivery point address  4', 'N0 1SE', 0, NULL),
(4, 1, 'Test Delivery point address cust1dp2 sxxxxxe', 'Test Delivery point address 2 sxxxxxxxxe', 'Test Delivery point address 3 sxxxxxxxxe', 'Test Delivery point address  4', 'pst cde', 0, NULL),
(5, 1001, 'Test Delivery point address cust1001 sxxxxxe', 'Test Delivery point address 2 sxxxxxxxxe', 'Test Delivery point address 3 sxxxxxxxxe', 'Test Delivery point address  4', 'dp1cde', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dept`
--

CREATE TABLE `dept` (
  `pkDeptID` tinyint(3) UNSIGNED NOT NULL COMMENT 'chgd 12/02/16',
  `Description` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='chgd 12/02/16';

--
-- Dumping data for table `dept`
--

INSERT INTO `dept` (`pkDeptID`, `Description`) VALUES
(1, 'Production'),
(2, 'Stores'),
(3, 'Engineering'),
(4, 'Purchasing'),
(5, 'Quality'),
(6, 'Planning/Est'),
(7, 'Front Office'),
(8, 'Finance/HR');

-- --------------------------------------------------------

--
-- Table structure for table `deviation`
--

CREATE TABLE `deviation` (
  `pkDeviationID` tinyint(4) NOT NULL,
  `Deviation` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `deviation`
--

INSERT INTO `deviation` (`pkDeviationID`, `Deviation`) VALUES
(1, 'Replacement'),
(2, 'rubbish');

-- --------------------------------------------------------

--
-- Table structure for table `deviationtype`
--

CREATE TABLE `deviationtype` (
  `pkDeviationTypeID` tinyint(4) NOT NULL,
  `DeviationType` varchar(20) DEFAULT NULL,
  `Deleted` tinyint(4) DEFAULT NULL COMMENT '1=yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `deviationtype`
--

INSERT INTO `deviationtype` (`pkDeviationTypeID`, `DeviationType`, `Deleted`) VALUES
(1, 'Replacement', 0),
(2, 'plain broken', 0),
(3, 'Operator error', 0);

-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE `device` (
  `pkDeviceID` tinyint(4) NOT NULL,
  `fkDeviceTypeID` tinyint(4) DEFAULT NULL,
  `fkLocationID` tinyint(4) DEFAULT NULL,
  `fkUnitTypeID` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='		';

-- --------------------------------------------------------

--
-- Table structure for table `devicelocation`
--

CREATE TABLE `devicelocation` (
  `pkLocationID` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `devicetype`
--

CREATE TABLE `devicetype` (
  `pkDeviceTypeID` int(11) UNSIGNED NOT NULL,
  `Description` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `devicetype`
--

INSERT INTO `devicetype` (`pkDeviceTypeID`, `Description`) VALUES
(1, 'Bore Micrometers'),
(2, 'Comparators'),
(3, 'CMM'),
(4, 'Dial test indicators'),
(5, 'Goulders'),
(6, 'Height Gauges'),
(7, 'Master slips'),
(8, 'Micrometers'),
(9, 'Microscopes'),
(10, 'Protractors'),
(11, 'Slip gauges'),
(12, 'Surface tables'),
(13, 'Surface testers'),
(14, 'Talymins'),
(15, 'Thread parallels'),
(16, 'Verniers');

-- --------------------------------------------------------

--
-- Table structure for table `deviceuse`
--

CREATE TABLE `deviceuse` (
  `pkUseID` mediumint(9) NOT NULL,
  `fkDeviceID` tinyint(4) DEFAULT NULL,
  `fkUserID` tinyint(4) DEFAULT NULL,
  `IssueDate` datetime DEFAULT NULL,
  `ReturnDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dispatch`
--

CREATE TABLE `dispatch` (
  `pkDispatchID` int(11) UNSIGNED NOT NULL COMMENT '22/3/16 ai',
  `fkGRBID` int(11) UNSIGNED DEFAULT NULL,
  `fkPOnID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 12/8/16',
  `fkInvoiceID` int(10) UNSIGNED NOT NULL,
  `DispatchDate` datetime DEFAULT NULL COMMENT 'added 18/2/16',
  `DispatchByfkUserID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 18/2/16',
  `SpecialInstruction` varchar(80) DEFAULT NULL COMMENT 'added 22/3/16',
  `fkCourierID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 11/5/16',
  `CarriageTypeID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 11/5/16 1=next day, 2=9am, 3=12noon, 4=5.30pm',
  `CourierBookDate` datetime DEFAULT NULL COMMENT 'added 11/5/16',
  `BookedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 11/5/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dispatch`
--

INSERT INTO `dispatch` (`pkDispatchID`, `fkGRBID`, `fkPOnID`, `fkInvoiceID`, `DispatchDate`, `DispatchByfkUserID`, `SpecialInstruction`, `fkCourierID`, `CarriageTypeID`, `CourierBookDate`, `BookedByfkUserID`) VALUES
(1, 49, 2, 0, NULL, NULL, NULL, 2, 1, '2016-11-16 17:50:08', 4),
(2, 50, 2, 0, NULL, NULL, NULL, 2, 2, '2016-11-16 20:07:24', 4);

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `pkDocumentID` int(11) UNSIGNED NOT NULL,
  `fkDocumentTypeID` int(11) UNSIGNED DEFAULT NULL,
  `fkWorkOrderID` int(11) UNSIGNED DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `CreatorfkUserID` int(11) UNSIGNED DEFAULT NULL,
  `SendDate` datetime DEFAULT NULL,
  `SenderfkUserID` int(11) UNSIGNED DEFAULT NULL,
  `SendMethod` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1 = printed, 2 = faxed, 3 = emailed'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `documentdroptype`
--

CREATE TABLE `documentdroptype` (
  `pkDocumentDropTypeID` tinyint(4) NOT NULL,
  `DropType` varchar(17) DEFAULT NULL,
  `Deleted` tinyint(4) DEFAULT NULL COMMENT '1=yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='27/1/16 now used for WO docs drop';

--
-- Dumping data for table `documentdroptype`
--

INSERT INTO `documentdroptype` (`pkDocumentDropTypeID`, `DropType`, `Deleted`) VALUES
(1, 'CMM', 0),
(2, 'FAIR', 0),
(3, 'Misc.', 0),
(4, 'Drg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `documenttype`
--

CREATE TABLE `documenttype` (
  `pkDocumentTypeID` tinyint(3) UNSIGNED NOT NULL COMMENT '1=AdviceDeliverNote, 2=RFQ, 3=ESLQuote, 4=Certs, 5=POs, 6=CRs, 7=InspectionFAIRS, 8=InternalTXs, 9=Deviation/Rejections, 10=Invoice',
  `Description` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='document drop types : really intended for the document Q';

--
-- Dumping data for table `documenttype`
--

INSERT INTO `documenttype` (`pkDocumentTypeID`, `Description`) VALUES
(1, 'Advice/Delivery Note'),
(2, 'Request for Quote'),
(3, 'ESL Quote'),
(4, 'Certificates'),
(5, 'Purchase Order'),
(6, 'Contract Review'),
(7, 'Inspection/FAIR'),
(8, 'Internal Transfer'),
(9, 'Deviation/Rejection'),
(10, 'Invoice');

-- --------------------------------------------------------

--
-- Table structure for table `drawing`
--

CREATE TABLE `drawing` (
  `pkDrawingID` smallint(6) UNSIGNED NOT NULL,
  `Reference` varchar(25) DEFAULT NULL,
  `DateAdded` datetime DEFAULT NULL,
  `AddedByfkUserID` smallint(6) UNSIGNED DEFAULT NULL,
  `FileHandle` varchar(45) DEFAULT NULL,
  `IssueNumber` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='				';

--
-- Dumping data for table `drawing`
--

INSERT INTO `drawing` (`pkDrawingID`, `Reference`, `DateAdded`, `AddedByfkUserID`, `FileHandle`, `IssueNumber`) VALUES
(1, 'ABC123ST16bDRG', '2016-11-16 00:00:00', 4, '1479303065_ABC123ST16.tif', '1a');

-- --------------------------------------------------------

--
-- Table structure for table `engineeringtemplate`
--

CREATE TABLE `engineeringtemplate` (
  `pkEngineeringTemplateID` smallint(5) UNSIGNED NOT NULL,
  `fkSupplyConditionID` tinyint(3) UNSIGNED DEFAULT NULL,
  `fkPartTemplateID` smallint(5) UNSIGNED NOT NULL,
  `ReviewStatus` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '0=in process, 1=passed, 2=failed',
  `Tooling` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1=yes, 2=no',
  `QAEquipment` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1=yes, 2=no',
  `CorrectApproval` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1=yes, 2=no',
  `RiskAssessment` tinyint(3) UNSIGNED DEFAULT '0' COMMENT '1=low, 2=med, 3=high',
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'add 1/3/16',
  `EngineeringIssue` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 3/3/16',
  `PlanningComplete` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '3/3/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `engineeringtemplate`
--

INSERT INTO `engineeringtemplate` (`pkEngineeringTemplateID`, `fkSupplyConditionID`, `fkPartTemplateID`, `ReviewStatus`, `Tooling`, `QAEquipment`, `CorrectApproval`, `RiskAssessment`, `AddedByfkUserID`, `EngineeringIssue`, `PlanningComplete`) VALUES
(1, 1, 14, 2, 1, 1, 1, 2, 4, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exchangerate`
--

CREATE TABLE `exchangerate` (
  `pkExchangeRateID` int(11) UNSIGNED NOT NULL,
  `Currency` varchar(12) DEFAULT NULL,
  `Rate` decimal(5,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='current exchange rates';

--
-- Dumping data for table `exchangerate`
--

INSERT INTO `exchangerate` (`pkExchangeRateID`, `Currency`, `Rate`) VALUES
(1, 'US Dollars', '1.5651'),
(2, 'Euros', '1.1696'),
(3, 'Irish Pounds', '0.9229');

-- --------------------------------------------------------

--
-- Stand-in structure for view `exportcustomers`
-- (See below for the actual view)
--
CREATE TABLE `exportcustomers` (
`FAO` varchar(15)
,`ClientName` varchar(50)
,`Address1` varchar(45)
,`Address2` varchar(40)
,`Address3` varchar(40)
,`Address4` varchar(30)
,`Postcode` char(8)
);

-- --------------------------------------------------------

--
-- Table structure for table `externalreject`
--

CREATE TABLE `externalreject` (
  `pkExternalRejectID` smallint(6) NOT NULL,
  `fkWorkOrderID` mediumint(9) DEFAULT NULL,
  `PreventActionNote` varchar(200) DEFAULT NULL,
  `fkDeviationTypeID` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='for parts coming back from customer - rejected for some reason';

-- --------------------------------------------------------

--
-- Table structure for table `fairreqs`
--

CREATE TABLE `fairreqs` (
  `pkFAIRReqs` int(11) UNSIGNED NOT NULL,
  `fkCustomerID` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fat`
--

CREATE TABLE `fat` (
  `pkFatID` tinyint(4) UNSIGNED NOT NULL,
  `FatType` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1 = between ops, 2 = delivery, 3 = subcon',
  `FatDays` tinyint(4) UNSIGNED DEFAULT NULL,
  `fkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 2/3/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fat`
--

INSERT INTO `fat` (`pkFatID`, `FatType`, `FatDays`, `fkUserID`) VALUES
(1, 1, 3, 4),
(2, 2, 4, 4),
(3, 3, 10, 4);

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) UNSIGNED NOT NULL,
  `mime` varchar(255) NOT NULL,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fixture`
--

CREATE TABLE `fixture` (
  `pkFixtureID` int(11) UNSIGNED NOT NULL,
  `fkLocationID` int(11) UNSIGNED DEFAULT NULL,
  `GRBNumber` int(11) UNSIGNED DEFAULT NULL,
  `FixtureNumber` varchar(20) DEFAULT NULL,
  `Description` varchar(25) DEFAULT NULL,
  `FixtureStatus` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1 = in stock, 2 = in use, 3 = unavailable',
  `fkPartTemplateID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'chgd 283/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='records all fixtures on the premises';

--
-- Dumping data for table `fixture`
--

INSERT INTO `fixture` (`pkFixtureID`, `fkLocationID`, `GRBNumber`, `FixtureNumber`, `Description`, `FixtureStatus`, `fkPartTemplateID`) VALUES
(1, NULL, 910072, '1234', 'a nice fixture', 1, 2),
(2, NULL, 910073, '4321', 'Flugelbinder base', 2, 4),
(3, NULL, 100003, '2284', 'base plate', 1, 1),
(4, NULL, 101010, '6789', 'Towerfield jig', 3, 3),
(5, NULL, 333900, '1928', 'jig', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `floorlocation`
--

CREATE TABLE `floorlocation` (
  `pkFloorLocationID` tinyint(4) UNSIGNED NOT NULL,
  `Location` char(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='to gauge location of machinery';

--
-- Dumping data for table `floorlocation`
--

INSERT INTO `floorlocation` (`pkFloorLocationID`, `Location`) VALUES
(1, 'A1'),
(2, 'A2'),
(3, 'A3'),
(4, 'A4'),
(5, 'B1');

-- --------------------------------------------------------

--
-- Table structure for table `freeformnote`
--

CREATE TABLE `freeformnote` (
  `pkFreeformNoteID` int(11) UNSIGNED NOT NULL,
  `fkCustomerID` int(11) UNSIGNED DEFAULT NULL,
  `Note` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='notes per customer on contacts screen';

--
-- Dumping data for table `freeformnote`
--

INSERT INTO `freeformnote` (`pkFreeformNoteID`, `fkCustomerID`, `Note`) VALUES
(1, 1, 'Has two Irish setters'),
(2, 1, 'Loves football. Supports Arsenal!');

-- --------------------------------------------------------

--
-- Table structure for table `goodsdocument`
--

CREATE TABLE `goodsdocument` (
  `pkDelDocID` int(11) UNSIGNED NOT NULL,
  `Location` varchar(45) DEFAULT NULL,
  `Handle` varchar(30) DEFAULT NULL COMMENT 'chd 20 to 30 10/2/16',
  `fkGRBID` int(11) UNSIGNED DEFAULT NULL,
  `DocumentType` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1=DN, 2=CoC/IR, 3=Inv',
  `DroppedByfkUserID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 10/2/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='chgd name 10/2/16';

--
-- Dumping data for table `goodsdocument`
--

INSERT INTO `goodsdocument` (`pkDelDocID`, `Location`, `Handle`, `fkGRBID`, `DocumentType`, `DroppedByfkUserID`) VALUES
(1, NULL, 'potato', 2, 1, 2),
(2, NULL, 'potato', 2, 1, 2),
(3, NULL, '1479304980_DN.png', 49, 1, 4),
(4, NULL, '1479305084_DN.png', 49, 1, 4),
(5, NULL, '1479309951_CoC examples.jpg', 50, 2, 4),
(6, NULL, '1479462913_ProcGetPOSuppli.png', 53, 2, 4),
(7, NULL, '1479463179_ProcGetPOSuppli.png', 53, 2, 4),
(8, NULL, '1479464106_ProcGetPOSuppli.png', 53, 2, 4),
(9, NULL, '1479464118_ProcGetPOSuppli.png', 53, 2, 4),
(10, NULL, '1479464232_ProcGetPOSuppli.png', 53, 2, 4),
(11, NULL, '1479464232_ProcGetPOSuppli.png', 53, 2, 4),
(12, NULL, '1479464796_ProcGetPOSuppli.png', 53, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `goodsin`
--

CREATE TABLE `goodsin` (
  `pkGRBID` int(11) UNSIGNED NOT NULL,
  `fkPartID` int(11) UNSIGNED DEFAULT NULL COMMENT '29/6/16 only for storeman to add - not linked to PT.',
  `fkSupplierID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'chgd to smallint 10/2/16',
  `fkCustomerID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 10/2/16',
  `ReceivedDate` datetime DEFAULT NULL,
  `IRNoteNumber` varchar(15) DEFAULT NULL,
  `Description` varchar(40) DEFAULT NULL,
  `DeliveryNoteNumber` varchar(20) DEFAULT NULL,
  `IdentificationMark` varchar(20) DEFAULT NULL,
  `AddedByfkUserID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 10/2/16',
  `WorkOrderID` mediumint(8) UNSIGNED DEFAULT NULL COMMENT 'added 13/216 just in case number is wrong/not found. 10/9/16 & for completed parts GR''d back in!',
  `Inspected` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 12/4/16 1=y. dont think needed 16/5'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `goodsin`
--

INSERT INTO `goodsin` (`pkGRBID`, `fkPartID`, `fkSupplierID`, `fkCustomerID`, `ReceivedDate`, `IRNoteNumber`, `Description`, `DeliveryNoteNumber`, `IdentificationMark`, `AddedByfkUserID`, `WorkOrderID`, `Inspected`) VALUES
(48, NULL, NULL, NULL, NULL, NULL, 'dummy grb', NULL, NULL, NULL, NULL, NULL),
(49, NULL, NULL, NULL, '2016-11-16 00:00:00', 'ABC123ST16bRN', 'S154D', 'DNABC123ST16b', NULL, 4, NULL, 1),
(50, NULL, NULL, NULL, '2016-11-16 00:00:00', 'ABC123ST16bSCRN', 'Heat Treatment', 'DNABC123ST16bSC', NULL, 4, NULL, 1),
(51, NULL, NULL, NULL, '2016-11-16 00:00:00', NULL, '', '', NULL, 4, NULL, NULL),
(52, NULL, NULL, NULL, '2016-11-17 00:00:00', NULL, '', '', NULL, 4, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `holiday`
--

CREATE TABLE `holiday` (
  `pkHolidayID` tinyint(4) NOT NULL,
  `Day` varchar(22) DEFAULT NULL,
  `Date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='uk national holidays';

--
-- Dumping data for table `holiday`
--

INSERT INTO `holiday` (`pkHolidayID`, `Day`, `Date`) VALUES
(1, 'New Years Day', '2016-01-01'),
(2, 'Good Friday', '2016-03-25'),
(3, 'Easter Monday', '2016-03-28'),
(4, 'Early May bank holiday', '2016-05-02'),
(5, 'Spring bank holiday', '2016-05-30'),
(6, 'Summer bank holiday', '2016-08-29'),
(7, 'Christmas eve', '2016-12-24'),
(8, 'Christmas day', '2016-12-25'),
(9, 'Boxing day', '2016-12-26');

-- --------------------------------------------------------

--
-- Table structure for table `inspection`
--

CREATE TABLE `inspection` (
  `pkInspectionID` bigint(20) UNSIGNED NOT NULL COMMENT 'chgd to auto 22/2/16. chgd to bigint',
  `fkUserID` smallint(5) UNSIGNED DEFAULT NULL,
  `Outcome` tinyint(3) UNSIGNED DEFAULT '0' COMMENT '0 = not inspected',
  `fkTypeID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '4/10/16 think this is 1=mat, 2=mach',
  `InspectStart` datetime DEFAULT NULL COMMENT 'chgd to datetime 22/1/16',
  `InspectEnd` datetime DEFAULT NULL COMMENT 'chgd to datetime 22/1/16',
  `fkUserIDComplete` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 23/2/16',
  `InspectQty` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 22/2/16',
  `ScrappedQty` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 22/2/16',
  `Detail` varchar(50) DEFAULT NULL COMMENT 'added 22/2/16',
  `ScrapReasonID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 10/3/16 no link - hard coded reasons\n1=mchn brkdn, 2=toolworn, 3=operror, 4=mathard',
  `CheckEquipment` varchar(50) DEFAULT NULL COMMENT 'added 8/9/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='holds details of all inspections';

--
-- Dumping data for table `inspection`
--

INSERT INTO `inspection` (`pkInspectionID`, `fkUserID`, `Outcome`, `fkTypeID`, `InspectStart`, `InspectEnd`, `fkUserIDComplete`, `InspectQty`, `ScrappedQty`, `Detail`, `ScrapReasonID`, `CheckEquipment`) VALUES
(1, 4, 1, 1, '2016-11-16 17:05:47', '2016-11-16 17:05:52', 4, NULL, 0, NULL, NULL, NULL),
(2, 4, 1, 2, '2016-11-16 17:10:45', '2016-11-16 17:10:51', 4, 0, 0, 'ABC123ST16b inspection info', NULL, NULL),
(3, 4, 0, 2, '2016-11-16 17:11:51', '2016-11-16 17:12:01', 4, 5, 0, 'ABC123ST16b inspection info', NULL, NULL),
(4, 4, 1, 1, '2016-11-16 18:27:05', '2016-11-16 18:27:10', 4, NULL, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inspectiondevice`
--

CREATE TABLE `inspectiondevice` (
  `pkDeviceID` smallint(6) UNSIGNED NOT NULL,
  `fkMasterTypeID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1= Measuring Device, 2 = Inspection device',
  `SerialNumber` varchar(20) DEFAULT NULL,
  `Range` varchar(10) DEFAULT NULL,
  `Description` varchar(40) DEFAULT NULL,
  `CalibrationDate` date DEFAULT NULL,
  `CalibrationPeriod` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'Months',
  `Manufacturer` varchar(10) DEFAULT NULL COMMENT 'test to handle imperial as well as metric - no calcs.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Brians devices\nthink mastertype is unittype.';

-- --------------------------------------------------------

--
-- Table structure for table `inspectiontype`
--

CREATE TABLE `inspectiontype` (
  `pkTypeID` int(11) UNSIGNED NOT NULL,
  `Type` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspectiontype`
--

INSERT INTO `inspectiontype` (`pkTypeID`, `Type`) VALUES
(1, 'standard'),
(2, '1st off'),
(3, 'FAIR'),
(4, 'final');

-- --------------------------------------------------------

--
-- Table structure for table `inspectpause`
--

CREATE TABLE `inspectpause` (
  `pkPauseID` int(11) UNSIGNED NOT NULL,
  `fkInspectionID` bigint(20) UNSIGNED NOT NULL COMMENT 'big 6/10/16',
  `StartTime` datetime DEFAULT NULL,
  `fkReasonID` tinyint(3) UNSIGNED DEFAULT NULL,
  `fkUserIDStart` smallint(5) UNSIGNED DEFAULT NULL,
  `EndTime` datetime DEFAULT NULL,
  `fkUserIDEnd` smallint(6) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='catalogs all pauses on a MATERIAL inspection.';

-- --------------------------------------------------------

--
-- Table structure for table `instruction`
--

CREATE TABLE `instruction` (
  `InstructionID` mediumint(8) UNSIGNED NOT NULL,
  `fkRFQOID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'chgd 27/4/16. not sure if tbl needed',
  `Quantity` smallint(5) UNSIGNED DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `Price` decimal(9,2) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `pkInvoiceID` int(11) UNSIGNED NOT NULL,
  `ClientInvoiceNumber` varchar(45) DEFAULT NULL,
  `RcvdDate` datetime DEFAULT NULL,
  `RcvdByfkUserID` smallint(6) UNSIGNED DEFAULT NULL,
  `FileName` varchar(30) DEFAULT NULL COMMENT 'added 9/2/16',
  `InvoiceOK` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 9/2/16',
  `TotalPrice` decimal(8,2) UNSIGNED DEFAULT NULL COMMENT '7/7/16 moved from poinvoice',
  `ActualTotal` decimal(9,2) UNSIGNED DEFAULT NULL COMMENT 'added 9/2/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoiceaddress`
--

CREATE TABLE `invoiceaddress` (
  `pkInvoiceAddressID` mediumint(8) UNSIGNED NOT NULL,
  `fkCustomerID` smallint(5) UNSIGNED NOT NULL,
  `InvoiceAddress1` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoiceaddress`
--

INSERT INTO `invoiceaddress` (`pkInvoiceAddressID`, `fkCustomerID`, `InvoiceAddress1`) VALUES
(1, 1, '123 Test place'),
(2, 1, 'No2 Address Street');

-- --------------------------------------------------------

--
-- Table structure for table `invoicegoods`
--

CREATE TABLE `invoicegoods` (
  `fkInvoiceID` int(10) UNSIGNED NOT NULL,
  `fkGRBID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='created 9/2/16 - to link multi GRBS to one received invoice, regardless of PO / GRB grouping';

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `pkItemID` mediumint(8) UNSIGNED NOT NULL,
  `fkRFQOID` smallint(5) UNSIGNED DEFAULT NULL,
  `fkPartID` smallint(5) UNSIGNED DEFAULT NULL,
  `fkShapeID` tinyint(3) UNSIGNED DEFAULT NULL,
  `fkUOMID` tinyint(3) UNSIGNED DEFAULT NULL,
  `Quantity` smallint(5) UNSIGNED DEFAULT NULL,
  `Description` varchar(45) DEFAULT NULL,
  `Comment` varchar(45) DEFAULT NULL,
  `Price` decimal(7,2) DEFAULT NULL,
  `Size` decimal(7,3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`pkItemID`, `fkRFQOID`, `fkPartID`, `fkShapeID`, `fkUOMID`, `Quantity`, `Description`, `Comment`, `Price`, `Size`) VALUES
(1, 1, 1, NULL, NULL, 5, 'Jack Torque Rod : S99 FHT', NULL, NULL, NULL),
(2, 2, 1, NULL, NULL, 5, 'FlugelBinder assembly parts', NULL, NULL, NULL),
(3, 2, 1, NULL, NULL, 5, 'complex washer', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lineitem`
--

CREATE TABLE `lineitem` (
  `pkLineItemID` mediumint(8) UNSIGNED NOT NULL COMMENT 'made auto 20/1/16',
  `fkOrderID` mediumint(8) UNSIGNED DEFAULT NULL,
  `fkPartTemplateID` smallint(5) UNSIGNED DEFAULT NULL,
  `fkEngineeringTemplateID` smallint(5) UNSIGNED DEFAULT NULL,
  `CustomerReference` varchar(5) DEFAULT NULL,
  `ESLQuoteNumber` mediumint(6) UNSIGNED ZEROFILL DEFAULT NULL COMMENT '26/9/16 zeros',
  `CancelReason` varchar(80) DEFAULT NULL COMMENT 'added 20/1/16',
  `IssueType` tinyint(4) DEFAULT NULL COMMENT '1=comms, 2=stock problem'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lineitem`
--

INSERT INTO `lineitem` (`pkLineItemID`, `fkOrderID`, `fkPartTemplateID`, `fkEngineeringTemplateID`, `CustomerReference`, `ESLQuoteNumber`, `CancelReason`, `IssueType`) VALUES
(1, 1, 14, 1, '9191', 009191, NULL, NULL),
(2, 2, 14, 1, '9191', 009191, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `machine`
--

CREATE TABLE `machine` (
  `pkMachineID` tinyint(4) UNSIGNED NOT NULL,
  `fkFloorLocationID` tinyint(4) UNSIGNED DEFAULT NULL,
  `Manufacturer` varchar(15) DEFAULT NULL,
  `Model` varchar(25) DEFAULT NULL,
  `SerialNumber` varchar(25) DEFAULT NULL,
  `LabourRate` decimal(2,0) UNSIGNED DEFAULT NULL,
  `Type` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1=lathe, 2=grind, 3=mill, 4=turn, 5 = multi',
  `Heads` tinyint(3) UNSIGNED DEFAULT NULL,
  `Axes` tinyint(3) UNSIGNED DEFAULT NULL,
  `ServiceContractYN` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1 = N, 2 = Y',
  `ChuckDiameter` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'mm',
  `SpindleBoreDiameter` decimal(5,2) UNSIGNED DEFAULT NULL,
  `JawsPartNumber` varchar(20) DEFAULT NULL,
  `SubChuckDiameter` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'mm',
  `SubJawsPartNumber` varchar(20) DEFAULT NULL COMMENT 'mm',
  `SubSpindleBoreDiameter` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'mm',
  `BAxisTravel` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'mm',
  `CAxisTravel` smallint(5) UNSIGNED DEFAULT NULL,
  `YAxisTravel` smallint(5) UNSIGNED DEFAULT NULL,
  `TailstockMP` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1 = Manual / 2 = Programmable',
  `Toolstations` tinyint(3) UNSIGNED DEFAULT NULL,
  `LengthOfTravel` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'mm',
  `ThroughToolCoolantYN` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1 = N, 2 = Y',
  `DrivenToolsMaxDiameter` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'mm',
  `BarFeed` varchar(20) DEFAULT NULL,
  `TimeOffset` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'an offset greater than one indicates more time required to make parts on this machine',
  `SkillOffset` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'Offset greater than 1 indicates more skill required to make parts on this machine.',
  `InUseYN` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1 = Y, 2 = N',
  `ServiceStart` date DEFAULT NULL COMMENT 'added 16/2/16',
  `ServiceEnd` date DEFAULT NULL COMMENT 'added 16/2/16',
  `ServicefkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 18/2/16',
  `FileName` varchar(25) DEFAULT NULL COMMENT 'added 18/2/16 for image of machine.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='					';

--
-- Dumping data for table `machine`
--

INSERT INTO `machine` (`pkMachineID`, `fkFloorLocationID`, `Manufacturer`, `Model`, `SerialNumber`, `LabourRate`, `Type`, `Heads`, `Axes`, `ServiceContractYN`, `ChuckDiameter`, `SpindleBoreDiameter`, `JawsPartNumber`, `SubChuckDiameter`, `SubJawsPartNumber`, `SubSpindleBoreDiameter`, `BAxisTravel`, `CAxisTravel`, `YAxisTravel`, `TailstockMP`, `Toolstations`, `LengthOfTravel`, `ThroughToolCoolantYN`, `DrivenToolsMaxDiameter`, `BarFeed`, `TimeOffset`, `SkillOffset`, `InUseYN`, `ServiceStart`, `ServiceEnd`, `ServicefkUserID`, `FileName`) VALUES
(1, 1, 'AB & M', '1-10-36 Vertical Broach', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(2, 1, 'Champion', '550 (manual)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(3, 1, 'Cincinnati', 'Arrow VMC-1000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(4, 1, 'Daewoo', 'Puma 2500SY (No.1)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(5, 1, 'Daewoo', 'Puma 2500SY (No.2)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 1, NULL, NULL, NULL, NULL),
(6, 1, 'Doosan', 'HP5100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 1, NULL, NULL, NULL, NULL),
(7, 1, 'Dugard', 'Eagle 200', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(8, 1, 'Dugard', 'Eagle 300', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(9, 1, 'Dugard', 'Eagle 1000 (Blue)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(10, 1, 'Dugard', 'Eagle 1000 (Brown)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(11, 1, 'Dugard', 'Top Mill (manual)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, NULL, NULL, NULL, NULL),
(12, 1, 'Dugard', 'MH500', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(13, 1, 'Harrison', '12 (manual)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(14, 1, 'Kia', 'SKT 250MS No.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(15, 1, 'Mazak', 'VTC800/30SR', '199162', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 1, NULL, NULL, NULL, NULL),
(16, 1, 'Mazak', 'VTC800 b', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(17, 1, 'Mikron', '102', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(18, 1, 'Mikron', '106', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(19, 1, 'Mikron', '132', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(20, 1, 'Prosaw', 'BS-330HAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(21, 1, 'Shigiya', 'GN-30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(22, 1, 'Sykes', 'V10A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(23, 1, 'Sykes', 'V10B (Grey)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, NULL, NULL, NULL, NULL),
(24, 1, 'Sykes', 'V10B NC (Green)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(25, 1, 'Sykes', 'V6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(26, 1, 'Tornos', 'ENC 164', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(27, 1, 'Tornos', 'Deco 2000 (26)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 1, NULL, NULL, NULL, NULL),
(28, 1, 'Yang', 'ML25A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, NULL, NULL, NULL),
(29, 1, 'Testa', 'Rossa', '007', NULL, 1, 5, 4, 1, 321, '23.00', 'AJAW01', 23, 'MiniMe1', 34, 0, 1, 1, 24, 0, 400, 1, 54, 'y', 2, 3, 2, NULL, NULL, NULL, NULL),
(30, NULL, 'Macc', 'Sheen', 'ESL001', '40', 2, 12, 5, 0, 106, '103.00', 'Brody', 25, 'Michael', 12, 1, 1, 1, 100, 1, 200, 0, 40, 'n', 3, 3, 2, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `machinemaintenance`
--

CREATE TABLE `machinemaintenance` (
  `pkMaintenanceID` smallint(5) UNSIGNED NOT NULL,
  `fkMachineID` tinyint(3) UNSIGNED DEFAULT NULL,
  `Issue` varchar(30) DEFAULT NULL,
  `CreateDate` date DEFAULT NULL,
  `Resolution` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='added 2/3/16';

--
-- Dumping data for table `machinemaintenance`
--

INSERT INTO `machinemaintenance` (`pkMaintenanceID`, `fkMachineID`, `Issue`, `CreateDate`, `Resolution`) VALUES
(1, 2, 'Broken Guards', '2016-03-02', 'ReplacedGuards'),
(2, 2, 'more broken guards', '2016-03-02', 'not replacing them again'),
(3, 29, 'cant find machine', '2016-04-27', 'find it'),
(4, 30, 'item has floated away', '2016-04-27', 'tie it down');

-- --------------------------------------------------------

--
-- Table structure for table `machinemaintparam`
--

CREATE TABLE `machinemaintparam` (
  `pkMachineID` tinyint(3) UNSIGNED NOT NULL COMMENT 'chgd to pk 17/3/16',
  `CoolantType` varchar(15) DEFAULT NULL,
  `TargetConcMin` tinyint(4) DEFAULT NULL,
  `TargetConcMax` tinyint(4) DEFAULT NULL,
  `SlideOilLevel` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'chgd from fkParameterid 17/3/16',
  `CoolantLevel` tinyint(3) UNSIGNED DEFAULT NULL,
  `GreaseLevel` tinyint(3) UNSIGNED DEFAULT NULL,
  `FanFilter` tinyint(3) UNSIGNED DEFAULT NULL,
  `OilFilter` tinyint(3) UNSIGNED DEFAULT NULL,
  `CoolantFilter` tinyint(3) UNSIGNED DEFAULT NULL,
  `AirSystem` tinyint(3) UNSIGNED DEFAULT NULL,
  `IntegralLighting` tinyint(3) UNSIGNED DEFAULT NULL,
  `InterlocksEngaged` tinyint(3) UNSIGNED DEFAULT NULL,
  `Offset` smallint(6) DEFAULT NULL,
  `StationNo` tinyint(4) DEFAULT NULL,
  `MinStickOut` tinyint(4) DEFAULT NULL,
  `Designation` varchar(15) DEFAULT NULL,
  `HolderNo` varchar(15) DEFAULT NULL,
  `InsertDatumPoint` varchar(15) DEFAULT NULL,
  `MinFluteLength` smallint(6) DEFAULT NULL,
  `Spindle` varchar(10) DEFAULT NULL,
  `Turn` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'all added 2/4/16',
  `Grind` tinyint(3) UNSIGNED DEFAULT NULL,
  `Mill` tinyint(3) UNSIGNED DEFAULT NULL,
  `GearCut` tinyint(3) UNSIGNED DEFAULT NULL,
  `Other` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='updated to general machine extension tbl 17/3/16';

--
-- Dumping data for table `machinemaintparam`
--

INSERT INTO `machinemaintparam` (`pkMachineID`, `CoolantType`, `TargetConcMin`, `TargetConcMax`, `SlideOilLevel`, `CoolantLevel`, `GreaseLevel`, `FanFilter`, `OilFilter`, `CoolantFilter`, `AirSystem`, `IntegralLighting`, `InterlocksEngaged`, `Offset`, `StationNo`, `MinStickOut`, `Designation`, `HolderNo`, `InsertDatumPoint`, `MinFluteLength`, `Spindle`, `Turn`, `Grind`, `Mill`, `GearCut`, `Other`) VALUES
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0),
(14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0),
(20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 1),
(22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0),
(29, '1', 97, 99, 3, 1, 0, 2, 2, 3, 1, 1, 1, 103, 1, 0, 'right', '2468', '100', 100, 'Main', 0, 1, 0, 1, 0),
(30, '1', 98, 99, 2, 2, 2, 1, 1, 1, 1, 0, 0, 101, 2, 0, 'Specifically', 'M414ST', 'unknown', 102, 'Main', 1, 0, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `machineset`
--

CREATE TABLE `machineset` (
  `pkSetID` mediumint(9) NOT NULL,
  `fkOperationID` int(10) UNSIGNED DEFAULT NULL,
  `fkMachineID` tinyint(3) UNSIGNED DEFAULT NULL,
  `fkUserIDSetStart` smallint(6) DEFAULT NULL,
  `StartTime` datetime DEFAULT NULL,
  `fkUserIDSetEnd` smallint(6) DEFAULT NULL,
  `EndTime` datetime DEFAULT NULL,
  `fkUserIDInspectStart` smallint(6) DEFAULT NULL,
  `InspectStart` datetime DEFAULT NULL COMMENT '10/11/6 think these are defunct; setinspect',
  `InspectEnd` datetime DEFAULT NULL,
  `fkUserIDInspectEnd` smallint(6) DEFAULT NULL,
  `fkUserIDSetter` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 9/5/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='machine set - could be multiple for one opmachine';

--
-- Dumping data for table `machineset`
--

INSERT INTO `machineset` (`pkSetID`, `fkOperationID`, `fkMachineID`, `fkUserIDSetStart`, `StartTime`, `fkUserIDSetEnd`, `EndTime`, `fkUserIDInspectStart`, `InspectStart`, `InspectEnd`, `fkUserIDInspectEnd`, `fkUserIDSetter`) VALUES
(1, 2, 6, 4, '2016-11-16 17:07:55', 4, '2016-11-16 17:07:57', NULL, NULL, NULL, NULL, 4),
(2, 5, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 8, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `machinetraining`
--

CREATE TABLE `machinetraining` (
  `fkUserID` smallint(5) UNSIGNED NOT NULL,
  `fkMachineID` tinyint(3) UNSIGNED NOT NULL,
  `Status` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1=Operator, 2=partly trained, 3=fully trained'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `machininglist`
--

CREATE TABLE `machininglist` (
  `pkMachiningListID` tinyint(3) UNSIGNED NOT NULL,
  `Description` varchar(25) DEFAULT NULL,
  `Deleted` tinyint(4) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='defunct. 2/3/16';

-- --------------------------------------------------------

--
-- Table structure for table `machiningtype`
--

CREATE TABLE `machiningtype` (
  `pkMachiningTypeID` tinyint(3) UNSIGNED NOT NULL COMMENT 'chgd ai 2/3/16',
  `Description` varchar(25) DEFAULT NULL,
  `Deleted` tinyint(4) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='chgd from list to type and chgd key to machiningtypeid 13/10/15';

--
-- Dumping data for table `machiningtype`
--

INSERT INTO `machiningtype` (`pkMachiningTypeID`, `Description`, `Deleted`) VALUES
(1, 'CNC Turning', 0),
(2, 'Manual Turning', 0),
(3, 'Horizontal Milling', 0),
(4, 'Manual Milling', 0),
(5, 'CNC Milling', 0),
(6, 'Cylindrical Grinding', 0),
(7, 'CNC Grinding', 0),
(8, 'Honing', 0),
(9, 'Thread Grinding', 0),
(10, 'Surface Grinding', 0),
(11, 'Gearcut', 0),
(12, 'Bevel Cutting', 0),
(13, 'Vertical Shaping', 0),
(14, 'Fitting', 0),
(15, 'Fly Press', 0),
(16, 'Vertical Broach', 0),
(17, 'Horizontal Broach', 0),
(18, 'NR Engineering', 0),
(19, 'Assembly', 0);

-- --------------------------------------------------------

--
-- Table structure for table `maintenanceparameter`
--

CREATE TABLE `maintenanceparameter` (
  `pkParameterID` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE `manufacturer` (
  `pkManufacturerID` smallint(5) UNSIGNED NOT NULL,
  `Description` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `material`
--

CREATE TABLE `material` (
  `pkMaterialID` mediumint(8) UNSIGNED NOT NULL,
  `fkGRBID` int(11) DEFAULT NULL COMMENT 'added 26/1/16. why not here before??',
  `fkMaterialTypeID` tinyint(3) UNSIGNED DEFAULT NULL,
  `fkApprovalLevelID` tinyint(3) UNSIGNED DEFAULT NULL,
  `fkShapeID` tinyint(3) UNSIGNED DEFAULT NULL,
  `fkUOMID` tinyint(3) UNSIGNED DEFAULT NULL,
  `fkMaterialSpecID` smallint(5) UNSIGNED DEFAULT NULL,
  `Description` varchar(20) DEFAULT NULL,
  `RcvdQuantity` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 9/2/16. chgd to rcvd from order 10/2/16',
  `Quantity` smallint(5) UNSIGNED DEFAULT NULL,
  `RcvdDate` date DEFAULT NULL,
  `Size` decimal(5,2) DEFAULT NULL,
  `UnitID` tinyint(4) DEFAULT NULL COMMENT 'for size unit type; inches=2, mm=1. added 27/1/16 ',
  `PONumber` mediumint(8) UNSIGNED DEFAULT NULL,
  `IRNoteNumber` varchar(25) DEFAULT NULL COMMENT 'chgd to char 25/1/16',
  `PriceEach` decimal(6,2) DEFAULT NULL,
  `Length` tinyint(4) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `material`
--

INSERT INTO `material` (`pkMaterialID`, `fkGRBID`, `fkMaterialTypeID`, `fkApprovalLevelID`, `fkShapeID`, `fkUOMID`, `fkMaterialSpecID`, `Description`, `RcvdQuantity`, `Quantity`, `RcvdDate`, `Size`, `UnitID`, `PONumber`, `IRNoteNumber`, `PriceEach`, `Length`) VALUES
(1, 49, 1, 5, 1, 4, 13, 'S154D', 20, NULL, NULL, '4.20', NULL, 1, NULL, NULL, NULL),
(2, 50, 1, 5, 9, 6, 13, 'Heat Treatment', 58, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL),
(3, 51, NULL, NULL, NULL, NULL, NULL, NULL, 54, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 52, 1, 7, 1, 4, NULL, '', 10, NULL, NULL, '3.00', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `materialinspect`
--

CREATE TABLE `materialinspect` (
  `fkMaterialID` int(10) UNSIGNED NOT NULL COMMENT '18/5/16 shld be grbid.',
  `fkInspectionID` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='created 22/2/16. shld be GoodsInspect';

--
-- Dumping data for table `materialinspect`
--

INSERT INTO `materialinspect` (`fkMaterialID`, `fkInspectionID`) VALUES
(49, 1),
(50, 4);

-- --------------------------------------------------------

--
-- Table structure for table `materiallocation`
--

CREATE TABLE `materiallocation` (
  `fkMaterialID` mediumint(8) UNSIGNED NOT NULL COMMENT 'actually GRBID 18/5/16',
  `fkStoreAreaID` tinyint(3) UNSIGNED NOT NULL COMMENT 'added 9/3/16',
  `fkXCoordID` tinyint(3) UNSIGNED NOT NULL COMMENT 'added 9/3/16',
  `fkYCoordID` tinyint(3) UNSIGNED NOT NULL COMMENT 'added 9/3/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='9/5/16 mvd locationid, all fields keyed';

--
-- Dumping data for table `materiallocation`
--

INSERT INTO `materiallocation` (`fkMaterialID`, `fkStoreAreaID`, `fkXCoordID`, `fkYCoordID`) VALUES
(49, 1, 2, 5),
(50, 2, 6, 6),
(52, 2, 6, 6);

-- --------------------------------------------------------

--
-- Table structure for table `materialnote`
--

CREATE TABLE `materialnote` (
  `pkMaterialNoteID` mediumint(9) NOT NULL,
  `fkCustomerID` smallint(6) DEFAULT NULL,
  `Note` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `materialspec`
--

CREATE TABLE `materialspec` (
  `pkMaterialSpecID` smallint(5) UNSIGNED NOT NULL,
  `MaterialSpec` varchar(6) DEFAULT NULL,
  `Description` varchar(20) DEFAULT NULL,
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 6/4/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `materialspec`
--

INSERT INTO `materialspec` (`pkMaterialSpecID`, `MaterialSpec`, `Description`, `AddedByfkUserID`) VALUES
(1, 'S07', 'S07-1008', NULL),
(2, 'S106D', '', NULL),
(3, 'S124', NULL, NULL),
(4, 'S124D', NULL, NULL),
(5, 'S130', NULL, NULL),
(6, 'S130D', NULL, NULL),
(7, 'S140', NULL, NULL),
(8, 'S143', NULL, NULL),
(9, 'S143D', NULL, NULL),
(10, 'S144', NULL, NULL),
(11, 'S154', NULL, NULL),
(12, 'S154B', NULL, NULL),
(13, 'S154D', NULL, NULL),
(14, 'S162', NULL, NULL),
(15, 'S28', NULL, NULL),
(16, 'S510', NULL, NULL),
(17, 'S514', NULL, NULL),
(18, 'S527', NULL, NULL),
(19, 'S80', NULL, NULL),
(20, 'S80D', NULL, NULL),
(21, 'S82D', NULL, NULL),
(22, 'S97', NULL, NULL),
(23, 'S98', NULL, NULL),
(24, 'S99', NULL, NULL),
(25, 'S99E', NULL, NULL),
(26, 'SAE660', NULL, NULL),
(27, 'spud', NULL, 4),
(28, 'S1099', NULL, 4);

-- --------------------------------------------------------

--
-- Table structure for table `materialsupplier`
--

CREATE TABLE `materialsupplier` (
  `fkOperationID` int(11) NOT NULL,
  `fkSupplierID` int(11) NOT NULL,
  `MetrePrice` decimal(8,2) DEFAULT NULL,
  `EachPrice` decimal(8,2) DEFAULT NULL,
  `MOQ` int(11) DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This is also where the RFQ / PO details reside!';

--
-- Dumping data for table `materialsupplier`
--

INSERT INTO `materialsupplier` (`fkOperationID`, `fkSupplierID`, `MetrePrice`, `EachPrice`, `MOQ`, `Quantity`) VALUES
(1, 1, '4.67', '0.00', 1, 25),
(2, 2, '4.88', '0.00', 1, 101);

-- --------------------------------------------------------

--
-- Table structure for table `materialtype`
--

CREATE TABLE `materialtype` (
  `pkMaterialTypeID` tinyint(3) UNSIGNED NOT NULL,
  `Type` varchar(15) DEFAULT NULL,
  `Deleted` tinyint(3) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `materialtype`
--

INSERT INTO `materialtype` (`pkMaterialTypeID`, `Type`, `Deleted`) VALUES
(1, 'Material', 0),
(2, 'Consumables', 0),
(3, 'Tooling', 0),
(4, 'QA / gauges', 0),
(5, 'Fixtures', 0),
(6, 'Miscellaneous', 0),
(7, 'Completed Parts', 0),
(8, 'FI Material', 0);

-- --------------------------------------------------------

--
-- Table structure for table `measuringdevice`
--

CREATE TABLE `measuringdevice` (
  `pkDeviceID` smallint(6) UNSIGNED NOT NULL,
  `GaugeID` varchar(10) DEFAULT NULL,
  `fkTypeID` tinyint(3) UNSIGNED DEFAULT NULL,
  `fkUnitTypeID` tinyint(3) UNSIGNED DEFAULT NULL,
  `Size` decimal(5,3) DEFAULT NULL,
  `Length` decimal(4,2) DEFAULT NULL,
  `Fit` char(3) DEFAULT NULL,
  `Description` varchar(35) DEFAULT NULL,
  `Extension` varchar(20) DEFAULT NULL,
  `Calibrate` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'Y/N',
  `NominalMinus` decimal(5,4) DEFAULT NULL,
  `NominalPlus` decimal(5,4) DEFAULT NULL,
  `P` tinyint(4) DEFAULT NULL,
  `N` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Dave Humphries devices		';

-- --------------------------------------------------------

--
-- Table structure for table `measuringdevicetype`
--

CREATE TABLE `measuringdevicetype` (
  `pkMeasuringDeviceTypeID` tinyint(3) UNSIGNED NOT NULL,
  `Description` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='device types for inspection measuring devices';

--
-- Dumping data for table `measuringdevicetype`
--

INSERT INTO `measuringdevicetype` (`pkMeasuringDeviceTypeID`, `Description`) VALUES
(1, 'Screw plug'),
(2, 'Calliper'),
(3, 'Screw ring'),
(4, 'Wear check');

-- --------------------------------------------------------

--
-- Table structure for table `nadcapapproval`
--

CREATE TABLE `nadcapapproval` (
  `pkApprovalID` tinyint(3) UNSIGNED NOT NULL,
  `Description` varchar(40) DEFAULT NULL,
  `Deleted` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nadcapapproval`
--

INSERT INTO `nadcapapproval` (`pkApprovalID`, `Description`, `Deleted`) VALUES
(1, 'Chemical Milling', 0),
(2, 'Chemical Processing', 0),
(3, 'Coating Testing', 0),
(4, 'Coatings', 0),
(5, 'Composites', 0),
(6, 'Corrosion Testing', 0),
(7, 'Electronics', 0),
(8, 'Fluid Distribution', 0),
(9, 'Heat Treat', 0),
(10, 'Materials Testing', 0),
(11, 'Non-conventional Machining', 0),
(12, 'Non-destructive Testing', 0),
(13, 'Paber Wear Testing', 0),
(14, 'Quality Systems', 0),
(15, 'Shot Peening', 0),
(16, 'Surface Enhancement', 0),
(17, 'Thermal Spray Coatings', 0),
(18, 'Welding', 0),
(19, 'Wet Tape Testing', 0),
(20, 'AC7120 - Circuit Card Assembly', 0),
(21, 'AC7121 - Cable & Harness Assembly', 0),
(22, 'Compliant', 0);

-- --------------------------------------------------------

--
-- Table structure for table `operatingprocedure`
--

CREATE TABLE `operatingprocedure` (
  `pkOperatingProcedureID` tinyint(3) UNSIGNED NOT NULL,
  `Description` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `operation`
--

CREATE TABLE `operation` (
  `pkOperationID` int(11) NOT NULL,
  `fkWorkOrderID` mediumint(9) NOT NULL,
  `OperationNumber` tinyint(4) NOT NULL,
  `fkOperationTypeID` tinyint(4) UNSIGNED NOT NULL,
  `fkMachiningTypeID` tinyint(4) DEFAULT NULL COMMENT 'chgd to typeid 13/10/15',
  `ScheduledStartDate` date DEFAULT NULL,
  `StartQty` smallint(5) UNSIGNED DEFAULT NULL,
  `EndQty` smallint(5) UNSIGNED DEFAULT NULL,
  `EstimatedSetTime` time DEFAULT NULL,
  `EstimatedRunTime` time DEFAULT NULL,
  `Bypass` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 18/2/16. 1=yes',
  `Subcon` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 18/2/16. 1=yes',
  `RiskStatement` varchar(100) DEFAULT NULL COMMENT 'added 25/4/16',
  `EngComplete` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 25/4/16',
  `Linked` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 26/4/16 to match optmplt'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `operation`
--

INSERT INTO `operation` (`pkOperationID`, `fkWorkOrderID`, `OperationNumber`, `fkOperationTypeID`, `fkMachiningTypeID`, `ScheduledStartDate`, `StartQty`, `EndQty`, `EstimatedSetTime`, `EstimatedRunTime`, `Bypass`, `Subcon`, `RiskStatement`, `EngComplete`, `Linked`) VALUES
(1, 1, 1, 1, NULL, '1896-07-04', 54, 61, '01:00:00', '00:00:30', NULL, NULL, NULL, NULL, 0),
(2, 1, 2, 2, 1, '1966-01-27', 54, NULL, '03:00:00', '00:00:25', NULL, NULL, NULL, NULL, 0),
(3, 1, 3, 3, NULL, '2016-10-29', 54, 58, '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, 0),
(4, 2, 1, 1, NULL, NULL, 84, NULL, '01:00:00', '00:00:30', NULL, NULL, NULL, NULL, 0),
(5, 2, 2, 2, 1, NULL, NULL, NULL, '03:00:00', '00:00:25', NULL, NULL, NULL, NULL, 0),
(6, 2, 3, 3, NULL, NULL, NULL, NULL, '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, 0),
(7, 3, 1, 1, NULL, NULL, 38, NULL, '01:00:00', '00:00:30', NULL, NULL, NULL, NULL, 0),
(8, 3, 2, 2, 1, NULL, NULL, NULL, '03:00:00', '00:00:25', NULL, NULL, NULL, NULL, 0),
(9, 3, 3, 3, NULL, NULL, NULL, NULL, '00:00:00', '00:00:00', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `operationapprovalcomments`
--

CREATE TABLE `operationapprovalcomments` (
  `pkApprovalCommentID` int(10) UNSIGNED NOT NULL,
  `fkOperationID` int(10) UNSIGNED DEFAULT NULL COMMENT 'chgd from med to int 2/11/16',
  `Comment` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `operationapprovalcomments`
--

INSERT INTO `operationapprovalcomments` (`pkApprovalCommentID`, `fkOperationID`, `Comment`) VALUES
(1, 1, 'ABC123ST16b std approval comment'),
(2, 3, 'ABC123ST16b std approval comment'),
(3, 4, 'ABC123ST16b std approval comment'),
(4, 6, 'ABC123ST16b std approval comment'),
(5, 7, 'ABC123ST16b std approval comment'),
(6, 9, 'ABC123ST16b std approval comment');

-- --------------------------------------------------------

--
-- Table structure for table `operationdocument`
--

CREATE TABLE `operationdocument` (
  `pkDocumentID` int(11) UNSIGNED NOT NULL,
  `fkOperationID` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this is for the document queue - currently not in use';

-- --------------------------------------------------------

--
-- Table structure for table `operationinspect`
--

CREATE TABLE `operationinspect` (
  `fkInspectionID` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'big 6/10/16',
  `fkOperationID` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='10/3/16 rmvd qtys - shld be in inspection tbl';

--
-- Dumping data for table `operationinspect`
--

INSERT INTO `operationinspect` (`fkInspectionID`, `fkOperationID`) VALUES
(3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `operationmachine`
--

CREATE TABLE `operationmachine` (
  `fkOperationID` int(11) UNSIGNED NOT NULL,
  `fkMachineID` tinyint(4) UNSIGNED NOT NULL,
  `Multi` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'yes/no 1= yes',
  `fkUserIDRunStart` smallint(6) UNSIGNED DEFAULT NULL,
  `RunStart` datetime DEFAULT NULL,
  `fkUserIDRunEnd` smallint(6) UNSIGNED DEFAULT NULL,
  `RunEnd` datetime DEFAULT NULL,
  `fkUserIDInspectStart` smallint(6) UNSIGNED DEFAULT NULL,
  `InspectStart` datetime DEFAULT NULL,
  `fkUserIDInspectEnd` smallint(6) UNSIGNED DEFAULT NULL,
  `InspectEnd` datetime DEFAULT NULL,
  `fkUserIDOperator` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 17/2/16. Other run users could be prod. mgr',
  `Repeated` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '30/6/16 has op been repeated: 1 or more.',
  `fkUserIDRepeat` smallint(5) UNSIGNED DEFAULT NULL COMMENT '30/6/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='stores machine details for given Operation. User is an fk but not the composite key.';

--
-- Dumping data for table `operationmachine`
--

INSERT INTO `operationmachine` (`fkOperationID`, `fkMachineID`, `Multi`, `fkUserIDRunStart`, `RunStart`, `fkUserIDRunEnd`, `RunEnd`, `fkUserIDInspectStart`, `InspectStart`, `fkUserIDInspectEnd`, `InspectEnd`, `fkUserIDOperator`, `Repeated`, `fkUserIDRepeat`) VALUES
(1, 20, 0, 4, '2016-11-16 17:07:23', 4, '2016-11-16 17:07:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 6, 0, 4, '2016-11-16 17:08:12', 4, '2016-11-16 17:08:15', NULL, NULL, NULL, NULL, 33, NULL, NULL),
(4, 20, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 20, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `operationmachining`
--

CREATE TABLE `operationmachining` (
  `fkOperationID` int(11) UNSIGNED NOT NULL,
  `fkMachiningListID` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='link table to confirm what machining task is allocated to this operation.';

--
-- Dumping data for table `operationmachining`
--

INSERT INTO `operationmachining` (`fkOperationID`, `fkMachiningListID`) VALUES
(2, 3),
(4, 5),
(299, 2),
(315, 6);

-- --------------------------------------------------------

--
-- Table structure for table `operationmaterial`
--

CREATE TABLE `operationmaterial` (
  `pkOperationID` int(11) UNSIGNED NOT NULL,
  `fkMaterialSpecID` smallint(5) UNSIGNED NOT NULL,
  `fkMaterialID` mediumint(9) DEFAULT NULL,
  `ExpectedPrice` decimal(8,2) DEFAULT NULL,
  `PONumber` smallint(5) UNSIGNED DEFAULT NULL,
  `IRNoteNumber` int(11) UNSIGNED DEFAULT NULL,
  `DeliveryNoteNumber` int(11) UNSIGNED DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL,
  `Description` varchar(45) DEFAULT NULL,
  `Size` decimal(7,3) UNSIGNED DEFAULT NULL,
  `Quantity` smallint(5) UNSIGNED DEFAULT NULL,
  `PurchasingToQuote` tinyint(4) DEFAULT '0' COMMENT 'y/n field?',
  `fkShapeID` tinyint(3) UNSIGNED DEFAULT NULL,
  `PartLength` decimal(8,2) DEFAULT NULL,
  `fkApprovalID` tinyint(3) UNSIGNED DEFAULT NULL,
  `SupplyCondition` varchar(200) DEFAULT NULL,
  `FreeIssue` tinyint(3) UNSIGNED DEFAULT '0',
  `UnitID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'what for? Qty is here.\nchgd 26/1/16 - forsize unitid: either inches=2, or mm=1',
  `StdPurchaseValue` decimal(6,4) UNSIGNED DEFAULT NULL,
  `StdPurchaseUnit` varchar(6) DEFAULT NULL,
  `StdfkSupplierID` smallint(5) UNSIGNED DEFAULT NULL,
  `CoCNote` varchar(100) DEFAULT NULL,
  `IssueInstruction` varchar(50) DEFAULT NULL,
  `SPUfkUOMID` tinyint(4) DEFAULT NULL COMMENT 'added 26/1/16',
  `SPU` tinyint(4) DEFAULT NULL,
  `Yield` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 25/4/16',
  `TestPieces` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 25/4/16',
  `StdDeliveryDays` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 25/4/16',
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 25/4/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Material Requirement .\nExpected price is per unit length. Only a non zero quantity indicates allocated material.';

--
-- Dumping data for table `operationmaterial`
--

INSERT INTO `operationmaterial` (`pkOperationID`, `fkMaterialSpecID`, `fkMaterialID`, `ExpectedPrice`, `PONumber`, `IRNoteNumber`, `DeliveryNoteNumber`, `ReceivedDate`, `Description`, `Size`, `Quantity`, `PurchasingToQuote`, `fkShapeID`, `PartLength`, `fkApprovalID`, `SupplyCondition`, `FreeIssue`, `UnitID`, `StdPurchaseValue`, `StdPurchaseUnit`, `StdfkSupplierID`, `CoCNote`, `IssueInstruction`, `SPUfkUOMID`, `SPU`, `Yield`, `TestPieces`, `StdDeliveryDays`, `AddedByfkUserID`) VALUES
(1, 13, NULL, NULL, NULL, NULL, NULL, NULL, 'S154D', '4.200', NULL, 0, 1, '30.00', 5, NULL, 0, 2, NULL, NULL, NULL, NULL, 'ABC123ST16b issue instruction', 4, 2, 3, 0, 0, 4),
(4, 13, NULL, NULL, NULL, NULL, NULL, NULL, 'S154D', '4.200', NULL, 0, 1, '30.00', 5, NULL, 0, 2, NULL, NULL, NULL, NULL, 'ABC123ST16b issue instruction', 4, 2, 3, 0, 0, 4),
(7, 13, NULL, NULL, NULL, NULL, NULL, NULL, 'S154D', '4.200', NULL, 0, 1, '30.00', 5, NULL, 0, 2, NULL, NULL, NULL, NULL, 'ABC123ST16b issue instruction', 4, 2, 3, 0, 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `operationnote`
--

CREATE TABLE `operationnote` (
  `pkOperationNoteID` int(11) UNSIGNED NOT NULL,
  `fkOperationID` int(11) UNSIGNED DEFAULT NULL,
  `Note` varchar(80) DEFAULT NULL,
  `fkNoteTypeID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1=material, 2=conformity, 3=internalmaterial, 4=engineering, 5=treatment, 6=internaltreatment, 7=inspectioncomments, 8=Operator: added 18/2/16 9=internalReview 4/3/16',
  `CreateDate` date DEFAULT NULL COMMENT 'added 25/4/16',
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 25/4/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Operation Notes on the template tabs';

--
-- Dumping data for table `operationnote`
--

INSERT INTO `operationnote` (`pkOperationNoteID`, `fkOperationID`, `Note`, `fkNoteTypeID`, `CreateDate`, `AddedByfkUserID`) VALUES
(1, 1, 'ABC123ST16b conf note', 2, '2016-11-16', 4),
(2, 1, 'ABC123ST16b int mat note', 1, '2016-11-16', 4),
(3, 1, 'ABC123ST16b int QA note', 9, '2016-11-16', 4),
(4, 2, 'ABC123ST16b int eng note', 4, '2016-11-16', 4),
(5, 3, 'ABC123ST16b int SC note', 6, '2016-11-16', 4),
(6, 4, 'ABC123ST16b conf note', 2, '2016-11-17', 4),
(7, 4, 'ABC123ST16b int mat note', 1, '2016-11-17', 4),
(8, 4, 'ABC123ST16b int QA note', 9, '2016-11-17', 4),
(9, 5, 'ABC123ST16b int eng note', 4, '2016-11-17', 4),
(10, 6, 'ABC123ST16b int SC note', 6, '2016-11-17', 4),
(11, 7, 'ABC123ST16b conf note', 2, '2016-11-21', 4),
(12, 7, 'ABC123ST16b int mat note', 1, '2016-11-21', 4),
(13, 7, 'ABC123ST16b int QA note', 9, '2016-11-21', 4),
(14, 8, 'ABC123ST16b int eng note', 4, '2016-11-21', 4),
(15, 9, 'ABC123ST16b int SC note', 6, '2016-11-21', 4);

-- --------------------------------------------------------

--
-- Table structure for table `operationorderitem`
--

CREATE TABLE `operationorderitem` (
  `fkOperationID` int(11) UNSIGNED NOT NULL,
  `fkItemID` mediumint(8) UNSIGNED NOT NULL COMMENT 'chgd 25/1/16',
  `AddedByfkUserID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 28/1/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='to allow multiple material / subcon purchases on one PO.\n12/1/16 updated to link to order item, not order';

--
-- Dumping data for table `operationorderitem`
--

INSERT INTO `operationorderitem` (`fkOperationID`, `fkItemID`, `AddedByfkUserID`) VALUES
(1, 1, 4),
(3, 2, 4),
(4, 3, 4),
(6, 4, 4),
(7, 5, 4),
(9, 6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `operationpo`
--

CREATE TABLE `operationpo` (
  `fkOperationID` int(11) UNSIGNED NOT NULL,
  `fkItemID` mediumint(8) UNSIGNED NOT NULL COMMENT 'chgd 22/1/16 from rfqoid to item id: multi items to one order'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='to allow multiple material / subcon purchases on one PO\n22/1/16 - now links to ITEM, not RFQO! items are sub PO';

--
-- Dumping data for table `operationpo`
--

INSERT INTO `operationpo` (`fkOperationID`, `fkItemID`) VALUES
(1, 1),
(1, 3),
(1, 39679);

-- --------------------------------------------------------

--
-- Table structure for table `operationprocess`
--

CREATE TABLE `operationprocess` (
  `pkProcessID` int(11) UNSIGNED NOT NULL,
  `fkOperationID` int(11) UNSIGNED DEFAULT NULL,
  `ProcessNote` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Processes on machining operation only';

--
-- Dumping data for table `operationprocess`
--

INSERT INTO `operationprocess` (`pkProcessID`, `fkOperationID`, `ProcessNote`) VALUES
(1, 2, 'ABC123ST16b process 1 of 1'),
(2, 5, 'ABC123ST16b process 1 of 1'),
(3, 8, 'ABC123ST16b process 1 of 1');

-- --------------------------------------------------------

--
-- Table structure for table `operationtemplate`
--

CREATE TABLE `operationtemplate` (
  `pkOperationTemplateID` mediumint(9) UNSIGNED NOT NULL,
  `fkEngineeringTemplateID` smallint(6) UNSIGNED NOT NULL,
  `fkOperationTypeID` tinyint(4) UNSIGNED DEFAULT NULL,
  `fkMachiningID` tinyint(4) UNSIGNED DEFAULT NULL,
  `OperationNumber` tinyint(4) UNSIGNED DEFAULT NULL,
  `EstimatedSetTime` time DEFAULT NULL,
  `EstimatedRunTime` time DEFAULT NULL,
  `EngComplete` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1= yes',
  `RiskStatement` varchar(100) DEFAULT NULL,
  `Linked` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 2/3/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `operationtemplate`
--

INSERT INTO `operationtemplate` (`pkOperationTemplateID`, `fkEngineeringTemplateID`, `fkOperationTypeID`, `fkMachiningID`, `OperationNumber`, `EstimatedSetTime`, `EstimatedRunTime`, `EngComplete`, `RiskStatement`, `Linked`) VALUES
(1, 1, 1, NULL, 1, '01:00:00', '00:00:30', NULL, NULL, 0),
(2, 1, 2, 1, 2, '03:00:00', '00:00:25', NULL, NULL, 0),
(3, 1, 3, NULL, 3, '00:00:00', '00:00:00', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `operationtreatment`
--

CREATE TABLE `operationtreatment` (
  `pkOperationID` int(11) UNSIGNED NOT NULL,
  `fkTreatmentID` smallint(5) UNSIGNED DEFAULT NULL COMMENT '2/11/16 int 2 small',
  `LeadTime` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'in days',
  `SelectedPrice` decimal(10,2) DEFAULT NULL COMMENT 'do not use. 16/2/16 - on rfqoSupplier',
  `fkApprovalID` tinyint(3) UNSIGNED DEFAULT NULL,
  `CourierBookDate` datetime DEFAULT NULL COMMENT 'added 16/2/16',
  `BookedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 16/2/16  moved to despatch 11/5/16',
  `fkCourierID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 16/2/16  moved to despatch 11/5/16',
  `CarriageTypeID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 16/2/16  moved to despatch 11/5/16',
  `DispatchDate` datetime DEFAULT NULL COMMENT 'added 16/2/16 moved to despatch 11/5/16',
  `DispatchedByfkUserID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 16/2/16  moved to despatch 11/5/16',
  `ApprovalComment` varchar(100) DEFAULT NULL COMMENT 'added 25/4/16 for BTs change to approval. BT has chgd back to multi approval!!',
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 25/4/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `operationtreatment`
--

INSERT INTO `operationtreatment` (`pkOperationID`, `fkTreatmentID`, `LeadTime`, `SelectedPrice`, `fkApprovalID`, `CourierBookDate`, `BookedByfkUserID`, `fkCourierID`, `CarriageTypeID`, `DispatchDate`, `DispatchedByfkUserID`, `ApprovalComment`, `AddedByfkUserID`) VALUES
(3, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(6, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(9, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4);

-- --------------------------------------------------------

--
-- Table structure for table `operationtype`
--

CREATE TABLE `operationtype` (
  `pkOperationTypeID` tinyint(3) UNSIGNED NOT NULL,
  `OpType` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Type 1: Material issue, Type 2: Machining, Type 3: Subcon';

--
-- Dumping data for table `operationtype`
--

INSERT INTO `operationtype` (`pkOperationTypeID`, `OpType`) VALUES
(1, 'Mtrl Issue'),
(2, 'Machining'),
(3, 'Subcon');

-- --------------------------------------------------------

--
-- Table structure for table `opmachineinspect`
--

CREATE TABLE `opmachineinspect` (
  `fkOperationID` int(11) UNSIGNED NOT NULL COMMENT '23/10/16 FUCKING DEFUNCT. GUI NO SUPPORT MULTI INSPECT',
  `fkMachineID` tinyint(3) UNSIGNED NOT NULL,
  `fkInspectionID` int(11) UNSIGNED NOT NULL,
  `Qty` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'na 22/2/16',
  `ScrappedQty` smallint(5) DEFAULT NULL COMMENT 'na 22/2/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='chgd from operationinspect. ';

--
-- Dumping data for table `opmachineinspect`
--

INSERT INTO `opmachineinspect` (`fkOperationID`, `fkMachineID`, `fkInspectionID`, `Qty`, `ScrappedQty`) VALUES
(1, 20, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `opmachineprogram`
--

CREATE TABLE `opmachineprogram` (
  `fkOperationID` int(11) UNSIGNED NOT NULL,
  `fkMachineID` tinyint(3) UNSIGNED NOT NULL,
  `fkProgramNumber` mediumint(8) UNSIGNED NOT NULL,
  `DateAdded` date DEFAULT NULL COMMENT 'date added? chgd name 27/7/17'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='stores Program numbers for relevant machine / part to be made';

--
-- Dumping data for table `opmachineprogram`
--

INSERT INTO `opmachineprogram` (`fkOperationID`, `fkMachineID`, `fkProgramNumber`, `DateAdded`) VALUES
(2, 6, 838485, '2016-11-16'),
(2, 6, 999999, '2016-11-16'),
(5, 6, 838485, '2016-11-17'),
(5, 6, 999999, '2016-11-17'),
(8, 6, 838485, '2016-11-21'),
(8, 6, 999999, '2016-11-21');

-- --------------------------------------------------------

--
-- Table structure for table `opsection`
--

CREATE TABLE `opsection` (
  `pkOpSectionID` tinyint(3) UNSIGNED NOT NULL,
  `fkOperatingProcedureID` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `opsubsection`
--

CREATE TABLE `opsubsection` (
  `pkOpSubSectionID` tinyint(3) UNSIGNED NOT NULL,
  `fkOpSectionID` tinyint(3) UNSIGNED NOT NULL,
  `OpSubSectioncol` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `optmpltmachineprogram`
--

CREATE TABLE `optmpltmachineprogram` (
  `pkMachineProgramID` int(10) UNSIGNED NOT NULL,
  `fkOperationTemplateID` mediumint(8) UNSIGNED NOT NULL,
  `fkMachineID` tinyint(4) UNSIGNED NOT NULL,
  `ProgramNumber` mediumint(9) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='stores Program numbers for relevant machine / part to be made, for template.\n3/3/16 one record per program, even on same machine! +chgd';

--
-- Dumping data for table `optmpltmachineprogram`
--

INSERT INTO `optmpltmachineprogram` (`pkMachineProgramID`, `fkOperationTemplateID`, `fkMachineID`, `ProgramNumber`) VALUES
(1, 2, 6, NULL),
(2, 2, 6, 838485);

-- --------------------------------------------------------

--
-- Table structure for table `orderdoc`
--

CREATE TABLE `orderdoc` (
  `pkOrderDocID` mediumint(8) UNSIGNED NOT NULL,
  `fkOrderID` mediumint(8) UNSIGNED DEFAULT NULL,
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL,
  `DateAdded` datetime DEFAULT NULL,
  `FileHandle` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='doc drop for CO added 15/4/16';

--
-- Dumping data for table `orderdoc`
--

INSERT INTO `orderdoc` (`pkOrderDocID`, `fkOrderID`, `AddedByfkUserID`, `DateAdded`, `FileHandle`) VALUES
(1, 1, 4, '2016-04-15 11:40:03', 'potato');

-- --------------------------------------------------------

--
-- Table structure for table `orderitem`
--

CREATE TABLE `orderitem` (
  `pkItemID` mediumint(8) UNSIGNED NOT NULL COMMENT 'chgd to auto 29/1/16',
  `fkRFQOID` smallint(5) UNSIGNED DEFAULT NULL,
  `ItemNo` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 11/4/16',
  `fkPartTemplateID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'chgd 27/1/16',
  `fkShapeID` tinyint(3) UNSIGNED DEFAULT NULL,
  `SPU` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 1/2/16',
  `fkUOMID` tinyint(3) UNSIGNED DEFAULT NULL,
  `Quantity` smallint(5) UNSIGNED DEFAULT NULL,
  `Description` varchar(45) DEFAULT NULL,
  `Comment` varchar(45) DEFAULT NULL,
  `Price` decimal(7,2) DEFAULT NULL,
  `Size` decimal(7,3) DEFAULT NULL,
  `UnitID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 9/6/16 1=mm, 2=inches',
  `Per` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 1/2/16 ''lot'' / ''each''',
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 11/4/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='chgd to order item 22/1/16 - less ambiguous';

--
-- Dumping data for table `orderitem`
--

INSERT INTO `orderitem` (`pkItemID`, `fkRFQOID`, `ItemNo`, `fkPartTemplateID`, `fkShapeID`, `SPU`, `fkUOMID`, `Quantity`, `Description`, `Comment`, `Price`, `Size`, `UnitID`, `Per`, `AddedByfkUserID`) VALUES
(1, 1, 1, 14, 1, 1, 4, 20, 'S154D', 'ABC123ST16b material PO item comment', '140.03', '4.200', 2, 1, 4),
(2, 2, 1, 14, NULL, 0, NULL, 59, 'Heat Treatment', 'ABC123ST16b SC PO Item 1 comment', '80.00', NULL, NULL, 1, 4),
(3, 3, 1, 14, 1, 1, 4, 83, 'S154D', NULL, NULL, '4.200', 2, NULL, 4),
(4, 4, 1, 14, NULL, NULL, NULL, 83, 'Heat Treatment', NULL, NULL, NULL, NULL, NULL, 4),
(5, 5, 1, 14, 1, 1, 4, 36, 'S154D', NULL, NULL, '4.200', 2, NULL, 4),
(6, 6, 1, 14, NULL, NULL, NULL, 36, 'Heat Treatment', NULL, NULL, NULL, NULL, NULL, 4);

-- --------------------------------------------------------

--
-- Table structure for table `ourapproval`
--

CREATE TABLE `ourapproval` (
  `pkApprovalID` tinyint(4) NOT NULL,
  `Approval` varchar(15) DEFAULT NULL,
  `Deleted` tinyint(4) DEFAULT NULL COMMENT '1= deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ourapproval`
--

INSERT INTO `ourapproval` (`pkApprovalID`, `Approval`, `Deleted`) VALUES
(1, 'Commercial', 0),
(2, 'AS9100C', 0),
(3, 'ISO9000', 0);

-- --------------------------------------------------------

--
-- Table structure for table `outvoice`
--

CREATE TABLE `outvoice` (
  `pkInvoiceID` int(11) UNSIGNED NOT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `CreatedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL,
  `SentDate` datetime DEFAULT NULL,
  `SentByfkUserID` smallint(5) UNSIGNED NOT NULL,
  `Quantity` smallint(6) UNSIGNED DEFAULT NULL,
  `DeliveryChg` decimal(7,2) UNSIGNED DEFAULT NULL COMMENT 'added 25/01/16',
  `Complete` tinyint(4) DEFAULT NULL COMMENT 'added 25/1/16. dont think set anywhere!',
  `AmendedValue` decimal(7,2) UNSIGNED DEFAULT NULL COMMENT 'added 2/4/16 ex vat',
  `AmendedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 2/4/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `outvoice`
--

INSERT INTO `outvoice` (`pkInvoiceID`, `CreateDate`, `CreatedByfkUserID`, `SentDate`, `SentByfkUserID`, `Quantity`, `DeliveryChg`, `Complete`, `AmendedValue`, `AmendedByfkUserID`) VALUES
(1, '2016-11-16 19:33:36', 4, NULL, 0, 54, '0.00', 0, '333.45', 4),
(2, '2016-11-16 19:33:37', 4, NULL, 0, 3, '0.00', 0, '33.45', 4);

-- --------------------------------------------------------

--
-- Table structure for table `outvoicecredit`
--

CREATE TABLE `outvoicecredit` (
  `pkCreditID` mediumint(9) NOT NULL COMMENT 'update 22/1/16',
  `fkInvoiceID` int(11) DEFAULT NULL,
  `Quantity` smallint(6) DEFAULT NULL,
  `CreatedByfkUserID` tinyint(4) DEFAULT NULL COMMENT 'added 22/1/16',
  `CoCReqd` tinyint(4) DEFAULT NULL COMMENT 'added 22/1/16',
  `IdentMarks` varchar(20) DEFAULT NULL COMMENT 'added 22/1/16\n'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='return / credit note against outgoing invoice';

-- --------------------------------------------------------

--
-- Table structure for table `overheadgroup`
--

CREATE TABLE `overheadgroup` (
  `pkGroupID` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `overheads`
--

CREATE TABLE `overheads` (
  `pkItemID` smallint(5) UNSIGNED NOT NULL,
  `fkGroupID` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `partmarkreq`
--

CREATE TABLE `partmarkreq` (
  `fkCustomerID` smallint(5) UNSIGNED NOT NULL,
  `fkPartMarkReqID` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `partreviewtask`
--

CREATE TABLE `partreviewtask` (
  `pkTaskID` mediumint(8) UNSIGNED NOT NULL,
  `fkPartTemplateID` smallint(5) UNSIGNED DEFAULT NULL,
  `Description` varchar(40) DEFAULT NULL,
  `fkActionTypeID` tinyint(3) UNSIGNED DEFAULT NULL,
  `CreateDate` date DEFAULT NULL,
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL,
  `FO` tinyint(3) UNSIGNED DEFAULT NULL,
  `Pu` tinyint(3) UNSIGNED DEFAULT NULL,
  `PE` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'Planning/Estimating',
  `En` tinyint(3) UNSIGNED DEFAULT NULL,
  `Qu` tinyint(3) UNSIGNED DEFAULT NULL,
  `SP` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'Scheduling/Production',
  `St` tinyint(3) UNSIGNED DEFAULT NULL,
  `Fi` tinyint(3) UNSIGNED DEFAULT NULL,
  `Closed` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='added 2/3/16';

-- --------------------------------------------------------

--
-- Table structure for table `parttemplate`
--

CREATE TABLE `parttemplate` (
  `pkPartTemplateID` smallint(5) UNSIGNED NOT NULL COMMENT 'ai 5/4/16',
  `fkParentPartTemplateID` smallint(5) UNSIGNED DEFAULT NULL,
  `QtyToParent` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 2/3/16',
  `fkDrawingID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'NULLd 14/9/16 - or no way to add new part!',
  `PartNumber` varchar(25) DEFAULT NULL,
  `IssueNumber` varchar(5) DEFAULT NULL,
  `Description` varchar(40) DEFAULT NULL,
  `OnAlert` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1 = yes',
  `LockedDown` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1 = yes',
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 2/3/16',
  `StdLeadDays` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 2/3/16 not sure needed; tmpltmat. 31/10/16',
  `StdPrice` decimal(7,2) DEFAULT NULL COMMENT 'added 2/3/16  not sure needed; tmpltmat. 31/10/16',
  `StdApprovalID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 2/3/16  not sure needed; tmpltmat. 31/10/16',
  `PartLength` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 2/3/16',
  `Width` smallint(5) UNSIGNED DEFAULT NULL,
  `Height` smallint(5) UNSIGNED DEFAULT NULL,
  `Weight` smallint(5) UNSIGNED DEFAULT NULL,
  `Finish` varchar(15) DEFAULT NULL,
  `UpIssueDate` date DEFAULT NULL COMMENT 'added 4/4/16',
  `UpIssueUserID` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `parttemplate`
--

INSERT INTO `parttemplate` (`pkPartTemplateID`, `fkParentPartTemplateID`, `QtyToParent`, `fkDrawingID`, `PartNumber`, `IssueNumber`, `Description`, `OnAlert`, `LockedDown`, `AddedByfkUserID`, `StdLeadDays`, `StdPrice`, `StdApprovalID`, `PartLength`, `Width`, `Height`, `Weight`, `Finish`, `UpIssueDate`, `UpIssueUserID`) VALUES
(13, NULL, NULL, NULL, 'dummy template', NULL, 'dummy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, NULL, 0, 1, 'ABC123ST16b/PT', '1a', 'A380 winglet', 0, 0, 4, 0, '5.85', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `parttemplatenote`
--

CREATE TABLE `parttemplatenote` (
  `pkPartTemplateNoteID` int(10) UNSIGNED NOT NULL,
  `fkPartTemplateID` smallint(5) UNSIGNED DEFAULT NULL,
  `Note` varchar(80) DEFAULT NULL,
  `CreateDate` date DEFAULT NULL,
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='new! added 3/3/16. 2/11/16 now for PC; int notes only';

-- --------------------------------------------------------

--
-- Table structure for table `pause`
--

CREATE TABLE `pause` (
  `pkPauseID` int(11) UNSIGNED NOT NULL,
  `fkOperationID` int(11) UNSIGNED NOT NULL,
  `fkMachineID` tinyint(3) UNSIGNED NOT NULL,
  `StartTime` datetime DEFAULT NULL,
  `fkReasonID` tinyint(3) UNSIGNED DEFAULT NULL,
  `fkUserIDStart` smallint(5) UNSIGNED DEFAULT NULL,
  `EndTime` datetime DEFAULT NULL,
  `fkUserIDEnd` smallint(6) UNSIGNED DEFAULT NULL COMMENT 'chgs to unsigned 15/2/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='catalogs all pauses on a set/run operation.';

-- --------------------------------------------------------

--
-- Table structure for table `pausereason`
--

CREATE TABLE `pausereason` (
  `pkReasonID` int(11) UNSIGNED NOT NULL,
  `Description` varchar(10) DEFAULT NULL COMMENT 'chgd 2/3/16',
  `Deleted` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='reason list for Operators to pause a job they are working on';

--
-- Dumping data for table `pausereason`
--

INSERT INTO `pausereason` (`pkReasonID`, `Description`, `Deleted`) VALUES
(1, 'Staff', 0),
(2, 'Breakdown', 0),
(3, 'Tooling', 0),
(4, 'Lube conc.', 0),
(5, 'Oil level', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pocomment`
--

CREATE TABLE `pocomment` (
  `pkPOCommentID` mediumint(9) UNSIGNED NOT NULL COMMENT 'chgd from smallint 1/2/16',
  `fkRFQOID` smallint(6) UNSIGNED DEFAULT NULL COMMENT 'add 1/2/16',
  `Comment` varchar(100) DEFAULT NULL,
  `Deleted` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='really rfqocomment tbl';

--
-- Dumping data for table `pocomment`
--

INSERT INTO `pocomment` (`pkPOCommentID`, `fkRFQOID`, `Comment`, `Deleted`) VALUES
(1, 1, 'ABC123ST16b supplier approval', 0),
(2, 2, 'ABC123ST16b supplier approval', 0),
(3, 3, 'ABC123ST16b std PO comment', 0),
(4, 5, 'ABC123ST16b std PO comment', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pogoods`
--

CREATE TABLE `pogoods` (
  `fkGRBID` int(11) UNSIGNED DEFAULT NULL,
  `fkItemID` mediumint(8) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pogoods`
--

INSERT INTO `pogoods` (`fkGRBID`, `fkItemID`) VALUES
(49, 1),
(50, 2);

-- --------------------------------------------------------

--
-- Table structure for table `poinvoice`
--

CREATE TABLE `poinvoice` (
  `fkInvoiceID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `fkItemID` mediumint(8) UNSIGNED NOT NULL COMMENT '7/7/16 to replace rfqoid',
  `TotalPrice` decimal(8,2) UNSIGNED DEFAULT NULL COMMENT '7/7/16 to b rmvd ',
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT '7/7/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='7/7/16 Damn! chg to ItemInvoice';

-- --------------------------------------------------------

--
-- Table structure for table `pon`
--

CREATE TABLE `pon` (
  `pkPOnID` smallint(5) UNSIGNED NOT NULL,
  `fkRFQOID` smallint(5) UNSIGNED NOT NULL,
  `CreateDate` date DEFAULT NULL COMMENT 'added 5/2/16. Filled when PO raised.',
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 5/2/16 chgd 6/10/16!'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='unique po number for actual PO.';

--
-- Dumping data for table `pon`
--

INSERT INTO `pon` (`pkPOnID`, `fkRFQOID`, `CreateDate`, `AddedByfkUserID`) VALUES
(1, 1, '2016-11-16', 4),
(2, 2, '2016-11-16', 4);

-- --------------------------------------------------------

--
-- Table structure for table `ponote`
--

CREATE TABLE `ponote` (
  `pkNoteID` mediumint(8) UNSIGNED NOT NULL COMMENT 'chgd auto 27/7/16',
  `fkRFQOID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'chgd 27/4/16',
  `Note` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='really rfqonote. not sure if needed 27/4/16';

--
-- Dumping data for table `ponote`
--

INSERT INTO `ponote` (`pkNoteID`, `fkRFQOID`, `Note`) VALUES
(1, 1, 'ABC123ST16b std approval comment'),
(2, 2, 'ABC123ST16b std approval comment'),
(3, 3, 'ABC123ST16b std approval comment'),
(4, 4, 'ABC123ST16b std approval comment'),
(5, 5, 'ABC123ST16b std approval comment'),
(6, 6, 'ABC123ST16b std approval comment');

-- --------------------------------------------------------

--
-- Table structure for table `posupplier`
--

CREATE TABLE `posupplier` (
  `fkPurchaseOrderID` mediumint(8) UNSIGNED NOT NULL,
  `fkSupplierID` smallint(6) UNSIGNED NOT NULL,
  `Selected` tinyint(4) UNSIGNED DEFAULT NULL COMMENT 'NULL= not set, 1 = selected'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This is where the RFQ to multiple Suppliers resides. selected record is accepted PO\ndefunct! 10/5/16';

--
-- Dumping data for table `posupplier`
--

INSERT INTO `posupplier` (`fkPurchaseOrderID`, `fkSupplierID`, `Selected`) VALUES
(1, 1001, 1),
(2, 1001, 1),
(39679, 1001, 1);

-- --------------------------------------------------------

--
-- Table structure for table `procedure`
--

CREATE TABLE `procedure` (
  `pkProcedureID` tinyint(3) UNSIGNED NOT NULL,
  `Area` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1=Induction, 2 = General',
  `Description` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Processes and procedure list for training purposes';

-- --------------------------------------------------------

--
-- Table structure for table `proceduretraining`
--

CREATE TABLE `proceduretraining` (
  `fkUserID` smallint(5) UNSIGNED NOT NULL,
  `fkProcedureID` tinyint(3) UNSIGNED NOT NULL,
  `Status` tinyint(4) UNSIGNED DEFAULT '0' COMMENT '1=Operator, 2=partly trained, 3=fully trained'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quotequantity`
--

CREATE TABLE `quotequantity` (
  `fkRFQPOID` smallint(5) UNSIGNED DEFAULT NULL,
  `QuantityOne` smallint(5) UNSIGNED DEFAULT NULL,
  `QuantityTwo` smallint(5) UNSIGNED DEFAULT NULL,
  `QuantityThree` smallint(5) UNSIGNED DEFAULT NULL,
  `QuantityFour` smallint(5) UNSIGNED DEFAULT NULL,
  `QuantityFive` smallint(5) UNSIGNED DEFAULT NULL,
  `QuanittySix` smallint(5) UNSIGNED DEFAULT NULL,
  `QuantitySeven` smallint(5) UNSIGNED DEFAULT NULL,
  `Selected` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1-7'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='stores up to seven quantity fields for any quote. Accepted qu is PO qu';

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `pkRateID` tinyint(3) UNSIGNED NOT NULL,
  `RateDescription` varchar(15) DEFAULT NULL,
  `Rate` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='general rates - finance admin	';

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`pkRateID`, `RateDescription`, `Rate`) VALUES
(1, 'VAT rate', 20),
(2, 'FAIR charge', 101),
(3, 'Material markup', 22);

-- --------------------------------------------------------

--
-- Table structure for table `regrind`
--

CREATE TABLE `regrind` (
  `pkRegrindID` mediumint(8) UNSIGNED NOT NULL,
  `fkToolID` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rejectgrb`
--

CREATE TABLE `rejectgrb` (
  `pkRejectID` smallint(5) UNSIGNED NOT NULL,
  `fkGRBID` int(10) UNSIGNED DEFAULT NULL,
  `RejectQty` smallint(5) UNSIGNED DEFAULT NULL,
  `RejectedByfkUserID` tinyint(3) UNSIGNED DEFAULT NULL,
  `RejectedDate` date DEFAULT NULL COMMENT 'added 17/5/16',
  `Deviation` varchar(200) DEFAULT NULL,
  `DeviationClauseID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1=damaged, 2=material issue, 3=quantity issue, 4=dimensional issue, 5=treatment issue, 6=paperwork issue\nswapped these two names round too.',
  `Closed` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1=closed',
  `NCRFileName` varchar(30) DEFAULT NULL,
  `DroppedByfkUserID` tinyint(4) DEFAULT NULL,
  `RejectClauseID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 18/10/16 1=Rectification, 2=Replacement, 3=Creditonly, 4=Informationonly\nswapped these two names round too.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='added 9/2/16. Reject GRB';

-- --------------------------------------------------------

--
-- Table structure for table `rejectinvoice`
--

CREATE TABLE `rejectinvoice` (
  `pkRejectID` smallint(5) UNSIGNED NOT NULL,
  `fkInvoiceID` int(10) UNSIGNED NOT NULL,
  `Value` decimal(7,2) DEFAULT NULL,
  `CreatedByfkUserID` tinyint(3) UNSIGNED DEFAULT NULL,
  `fkReasonID` tinyint(4) DEFAULT NULL COMMENT '1=incorrect invoice price, 2=credit not rcvd, 3=Goods tainted',
  `Reject` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'denotes full reject or adjustment. 1=reject'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='created 9/2/16 for invoice adjustment/rejects';

-- --------------------------------------------------------

--
-- Table structure for table `remark`
--

CREATE TABLE `remark` (
  `pkRemarkID` smallint(5) UNSIGNED NOT NULL,
  `Remark` varchar(100) DEFAULT NULL COMMENT 'chgd size 17/2/16',
  `fkUserID` smallint(6) UNSIGNED NOT NULL,
  `Deleted` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1=deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `requirement`
--

CREATE TABLE `requirement` (
  `pkRequirementID` tinyint(3) UNSIGNED NOT NULL,
  `fkOpSectionID` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rfqn`
--

CREATE TABLE `rfqn` (
  `pkRFQnID` smallint(5) UNSIGNED NOT NULL,
  `fkRFQOID` smallint(6) UNSIGNED NOT NULL,
  `AddedByfkUserID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 4/2/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='RFQ number - several RFQs can go out to different suppliers, for the same material for one WO.\nMust have SAME number.';

--
-- Dumping data for table `rfqn`
--

INSERT INTO `rfqn` (`pkRFQnID`, `fkRFQOID`, `AddedByfkUserID`) VALUES
(1, 1, 4),
(2, 2, 4),
(3, 1, 4),
(4, 2, 4),
(5, 3, 4),
(6, 4, 4),
(7, 5, 4),
(8, 6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `rfqorder`
--

CREATE TABLE `rfqorder` (
  `pkRFQOID` smallint(6) UNSIGNED NOT NULL,
  `fkApprovalLevelID` tinyint(4) UNSIGNED DEFAULT NULL,
  `fkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'created by?',
  `OtherCharges` decimal(5,2) UNSIGNED DEFAULT NULL,
  `ReturnDrawing` tinyint(4) UNSIGNED DEFAULT NULL COMMENT 'Y/N',
  `VeryUrgent` tinyint(4) UNSIGNED DEFAULT NULL COMMENT 'Y/N',
  `InternalOnly` tinyint(4) UNSIGNED DEFAULT NULL COMMENT 'Y/N',
  `MOQ` smallint(6) UNSIGNED DEFAULT NULL,
  `Reference` varchar(15) DEFAULT NULL,
  `cocRequired` tinyint(4) UNSIGNED DEFAULT NULL COMMENT 'Y/N',
  `CreateDate` datetime DEFAULT NULL,
  `LeadDays` tinyint(3) UNSIGNED DEFAULT NULL,
  `OrderQuantity` smallint(6) DEFAULT NULL,
  `Cancelled` tinyint(4) UNSIGNED DEFAULT NULL COMMENT 'Y/N',
  `Complete` tinyint(4) DEFAULT NULL COMMENT 'added 28/1/16',
  `CompleteDate` date DEFAULT NULL COMMENT 'added 24/2/16. to replace boolean',
  `POType` tinyint(4) DEFAULT NULL COMMENT 'added 28/1/16 1=material, 2=sub-con, 3=Tooling, 4=General',
  `DeliveryCommentID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 3/2/16',
  `DeliveryDate` date DEFAULT NULL COMMENT 'added 6/5/16 for po>supp tab'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='RFQ / PO details. PO has PONumber';

--
-- Dumping data for table `rfqorder`
--

INSERT INTO `rfqorder` (`pkRFQOID`, `fkApprovalLevelID`, `fkUserID`, `OtherCharges`, `ReturnDrawing`, `VeryUrgent`, `InternalOnly`, `MOQ`, `Reference`, `cocRequired`, `CreateDate`, `LeadDays`, `OrderQuantity`, `Cancelled`, `Complete`, `CompleteDate`, `POType`, `DeliveryCommentID`, `DeliveryDate`) VALUES
(1, 5, 4, NULL, NULL, NULL, NULL, NULL, 'ABC123ST16bSREF', NULL, '2016-11-16 16:37:45', 0, NULL, 0, 0, NULL, 1, 0, '2017-03-09'),
(2, NULL, 4, NULL, NULL, NULL, NULL, NULL, 'ABC123ST16SCref', NULL, '2016-11-16 16:37:45', 0, NULL, 0, 0, NULL, 2, 0, '2017-03-09'),
(3, 5, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-11-17 17:48:51', 0, NULL, 0, 0, NULL, 1, NULL, NULL),
(4, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-11-17 17:48:51', 0, NULL, 0, 0, NULL, 2, NULL, NULL),
(5, 5, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-11-21 13:06:40', 0, NULL, 0, 0, NULL, 1, NULL, NULL),
(6, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-11-21 13:06:40', 0, NULL, 0, 0, NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rfqosupplier`
--

CREATE TABLE `rfqosupplier` (
  `fkRFQOID` smallint(5) UNSIGNED NOT NULL,
  `fkSupplierID` smallint(6) UNSIGNED NOT NULL COMMENT 'comp key 11/4/16',
  `fkContactID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 7/6/16. not used - addrfqsupplier does not rcv contact param',
  `Selected` tinyint(4) UNSIGNED DEFAULT NULL COMMENT 'NULL= not set, 1 = selected',
  `TotalCost` decimal(9,2) UNSIGNED DEFAULT NULL COMMENT 'added 28/1/16',
  `DeliveryCost` decimal(5,2) UNSIGNED DEFAULT NULL COMMENT 'added 28/1/16',
  `CertCharge` decimal(5,2) UNSIGNED DEFAULT NULL COMMENT 'added 3/2/16 chgd to .2 5/5/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This is where the RFQ to multiple Suppliers resides. selected record is accepted PO\nname chg updated 28/1/16';

--
-- Dumping data for table `rfqosupplier`
--

INSERT INTO `rfqosupplier` (`fkRFQOID`, `fkSupplierID`, `fkContactID`, `Selected`, `TotalCost`, `DeliveryCost`, `CertCharge`) VALUES
(1, 3503, 5, 1, '0.00', '0.00', '0.00'),
(2, 3503, 5, 1, '0.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `rfqprogress`
--

CREATE TABLE `rfqprogress` (
  `pkProgressID` mediumint(8) UNSIGNED NOT NULL,
  `fkRFQOID` smallint(5) UNSIGNED DEFAULT NULL,
  `fkUserID` tinyint(3) UNSIGNED DEFAULT NULL,
  `ContactTypeID` tinyint(3) UNSIGNED DEFAULT NULL,
  `ProgressNote` varchar(50) DEFAULT NULL,
  `CreateDate` date DEFAULT NULL,
  `Chase` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1=yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='logs rfq supplier progress notes\ncreated 8/2/16';

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `pkRoleID` tinyint(3) UNSIGNED NOT NULL,
  `Description` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`pkRoleID`, `Description`) VALUES
(1, 'Operator'),
(2, 'Setter'),
(3, 'Inspector'),
(4, 'Storeman'),
(5, 'Office/Admin'),
(6, 'Engineer'),
(7, 'Planner/Est'),
(8, 'Finance/HR'),
(9, 'Sched/Prod'),
(10, 'Purchaser'),
(11, 'Director');

-- --------------------------------------------------------

--
-- Table structure for table `safetytask`
--

CREATE TABLE `safetytask` (
  `pkTaskID` tinyint(3) UNSIGNED NOT NULL,
  `fkTaskTypeID` tinyint(4) UNSIGNED DEFAULT NULL,
  `Anniversary` date DEFAULT NULL,
  `RefreshPeriod` tinyint(4) UNSIGNED DEFAULT NULL COMMENT 'months',
  `fkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Created By'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Fire drills, alarms etc.';

-- --------------------------------------------------------

--
-- Table structure for table `safetytaskcheck`
--

CREATE TABLE `safetytaskcheck` (
  `pkTaskCheckID` smallint(5) UNSIGNED NOT NULL,
  `fkTaskID` tinyint(3) UNSIGNED DEFAULT NULL,
  `CarriedOutBy` varchar(15) DEFAULT NULL,
  `DateCompleted` date DEFAULT NULL,
  `PassFail` tinyint(4) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `screen`
--

CREATE TABLE `screen` (
  `pkScreenID` tinyint(3) UNSIGNED NOT NULL,
  `Name` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='not in use! from 12/2/16';

-- --------------------------------------------------------

--
-- Table structure for table `screenrole`
--

CREATE TABLE `screenrole` (
  `fkScreenID` int(11) NOT NULL,
  `fkRoleID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='not in use! 12/2/16';

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `pkServiceID` mediumint(9) NOT NULL COMMENT 'could well be the POid from PurchaseOrder',
  `fkOperationID` int(11) DEFAULT NULL,
  `fkMachineID` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='additional information from a Purchase Order resulting in some sort of service (no GRB).\nUpdate: op and machine id optional.';

-- --------------------------------------------------------

--
-- Table structure for table `setinspect`
--

CREATE TABLE `setinspect` (
  `fkSetID` mediumint(8) UNSIGNED NOT NULL,
  `fkInspectionID` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='5/11/16';

--
-- Dumping data for table `setinspect`
--

INSERT INTO `setinspect` (`fkSetID`, `fkInspectionID`) VALUES
(24, 39),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `shape`
--

CREATE TABLE `shape` (
  `pkShapeID` tinyint(4) NOT NULL,
  `Description` varchar(15) DEFAULT NULL,
  `ReqdDesc` tinyint(4) DEFAULT NULL COMMENT '1 = yes',
  `Deleted` tinyint(4) DEFAULT '0',
  `fkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 24/3/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shape`
--

INSERT INTO `shape` (`pkShapeID`, `Description`, `ReqdDesc`, `Deleted`, `fkUserID`) VALUES
(1, 'Diameter', NULL, 0, NULL),
(2, 'Square', NULL, 0, NULL),
(3, 'Rectangle', NULL, 0, NULL),
(4, 'Flat', NULL, 0, NULL),
(5, 'Triangular', NULL, 0, NULL),
(6, 'Hexagonal', NULL, 0, NULL),
(7, 'Forging', NULL, 0, NULL),
(8, 'Tubular', NULL, 0, NULL),
(9, 'Part', NULL, 0, NULL),
(10, 'Casting', NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `specification`
--

CREATE TABLE `specification` (
  `pkSpecificationID` tinyint(3) UNSIGNED NOT NULL,
  `fkCustomerID` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spud`
--

CREATE TABLE `spud` (
  `pkID` tinyint(3) UNSIGNED NOT NULL,
  `PONumber` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spud`
--

INSERT INTO `spud` (`pkID`, `PONumber`) VALUES
(1, 2),
(2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `stdconformitynote`
--

CREATE TABLE `stdconformitynote` (
  `pkConformityNoteID` mediumint(9) UNSIGNED NOT NULL,
  `fkCustomerID` smallint(6) UNSIGNED DEFAULT NULL,
  `Note` varchar(80) DEFAULT NULL,
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 24/3/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer specific conformity notes';

-- --------------------------------------------------------

--
-- Table structure for table `storearea`
--

CREATE TABLE `storearea` (
  `pkStoreAreaID` tinyint(4) UNSIGNED NOT NULL,
  `StoreArea` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='general areas within Stores - Fixture, future delivery etc.';

--
-- Dumping data for table `storearea`
--

INSERT INTO `storearea` (`pkStoreAreaID`, `StoreArea`) VALUES
(1, 'Fixtures'),
(2, 'Main'),
(3, 'Future Delivery'),
(4, 'Stores Rack'),
(5, 'Issue'),
(6, 'Sheet Rack'),
(7, 'Goods in'),
(8, 'Free Issue'),
(9, 'Despatch');

-- --------------------------------------------------------

--
-- Table structure for table `storelocation`
--

CREATE TABLE `storelocation` (
  `pkLocationID` smallint(6) UNSIGNED NOT NULL,
  `fkStoreAreaID` tinyint(4) UNSIGNED DEFAULT NULL,
  `LocationCode` char(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this should be defunct 31/3/16';

--
-- Dumping data for table `storelocation`
--

INSERT INTO `storelocation` (`pkLocationID`, `fkStoreAreaID`, `LocationCode`) VALUES
(1, 1, '1A'),
(2, 1, '1B'),
(3, 1, '1C'),
(4, 1, '1D'),
(5, 1, '2A'),
(6, 1, '2B'),
(7, 1, '2C'),
(8, 1, '2D'),
(9, 1, '3A'),
(10, 1, '3B'),
(11, 1, '3C'),
(12, 1, '3D'),
(13, 2, '40'),
(14, 2, '41'),
(15, 2, '42'),
(16, 2, '43'),
(17, 2, '44'),
(18, 2, '45'),
(19, 2, '46'),
(20, 2, '47'),
(21, 2, '48'),
(22, 2, '49'),
(23, 2, '50'),
(24, 2, '51'),
(25, 2, '52'),
(26, 2, '53'),
(27, 2, '54'),
(28, 2, '55'),
(29, 2, '56'),
(30, 2, '57'),
(31, 2, '58'),
(32, 2, '59'),
(33, 2, '60'),
(34, 2, '61'),
(35, 2, '62'),
(36, 2, '63'),
(37, 2, '64'),
(38, 2, '65'),
(39, 2, '66'),
(40, 2, '67'),
(41, 2, '68'),
(42, 2, '69'),
(43, 2, '70'),
(44, 2, '71'),
(45, 2, '72'),
(46, 2, '73'),
(47, 2, '74'),
(48, 2, '75'),
(49, 2, '76'),
(50, 2, '77'),
(51, 2, '78'),
(52, 2, '79'),
(53, 2, '80');

-- --------------------------------------------------------

--
-- Table structure for table `sundry`
--

CREATE TABLE `sundry` (
  `pkSundryID` tinyint(4) NOT NULL,
  `Sundry` varchar(20) DEFAULT NULL,
  `Deleted` tinyint(4) DEFAULT NULL COMMENT '1= yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='general parts allocated to job, such as emery cloth & wire wool etc.';

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `pkSupplierID` smallint(6) UNSIGNED NOT NULL,
  `SupplierName` varchar(50) DEFAULT NULL,
  `Alias` varchar(40) DEFAULT NULL,
  `Address1` varchar(45) DEFAULT NULL,
  `Address2` varchar(40) DEFAULT NULL,
  `Address3` varchar(40) DEFAULT NULL,
  `Address4` varchar(30) DEFAULT NULL,
  `Postcode` varchar(8) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `Facsimile` varchar(20) DEFAULT NULL,
  `Website` varchar(40) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `MinOrderCharge` smallint(6) DEFAULT NULL,
  `PaymentTerms` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1 = 30 days, 2 = 60 days, 3 = 90 days',
  `MaterialSupplier` tinyint(4) UNSIGNED DEFAULT NULL,
  `TreatmentSupplier` tinyint(4) UNSIGNED DEFAULT NULL,
  `CalibrationSupplier` tinyint(4) UNSIGNED DEFAULT NULL,
  `ToolingSupplier` tinyint(4) UNSIGNED DEFAULT NULL,
  `MiscSupplier` tinyint(4) UNSIGNED DEFAULT NULL,
  `Deleted` tinyint(3) UNSIGNED DEFAULT NULL,
  `fkUserID` smallint(6) UNSIGNED DEFAULT NULL,
  `StdDeliveryDays` tinyint(4) UNSIGNED DEFAULT NULL,
  `Day1Open` time DEFAULT NULL COMMENT 'time of 00:00 indicates closed.',
  `Day1Close` time DEFAULT NULL,
  `Day2Open` time DEFAULT NULL,
  `Day2Close` time DEFAULT NULL,
  `Day3Open` time DEFAULT NULL,
  `Day3Close` time DEFAULT NULL,
  `Day4Open` time DEFAULT NULL,
  `Day4Close` time DEFAULT NULL,
  `Day5Open` time DEFAULT NULL,
  `Day5Close` time DEFAULT NULL,
  `Day6Open` time DEFAULT NULL,
  `Day6Close` time DEFAULT NULL,
  `Day7Open` time DEFAULT NULL,
  `Day7Close` time DEFAULT NULL,
  `QuestionsFilename` varchar(30) DEFAULT NULL COMMENT 'for supplier questionnaire. added 24/2/16',
  `SQCompletedBy` varchar(25) DEFAULT NULL COMMENT 'added 28/2/16',
  `Type1` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 29/2/16 moved from supplierpersonnel tbl',
  `Type2` tinyint(3) UNSIGNED DEFAULT NULL,
  `Type3` tinyint(3) UNSIGNED DEFAULT NULL,
  `Type4` tinyint(3) UNSIGNED DEFAULT NULL,
  `Type5` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Master Supplier list';

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`pkSupplierID`, `SupplierName`, `Alias`, `Address1`, `Address2`, `Address3`, `Address4`, `Postcode`, `Phone`, `Facsimile`, `Website`, `Email`, `MinOrderCharge`, `PaymentTerms`, `MaterialSupplier`, `TreatmentSupplier`, `CalibrationSupplier`, `ToolingSupplier`, `MiscSupplier`, `Deleted`, `fkUserID`, `StdDeliveryDays`, `Day1Open`, `Day1Close`, `Day2Open`, `Day2Close`, `Day3Open`, `Day3Close`, `Day4Open`, `Day4Close`, `Day5Open`, `Day5Close`, `Day6Open`, `Day6Close`, `Day7Open`, `Day7Close`, `QuestionsFilename`, `SQCompletedBy`, `Type1`, `Type2`, `Type3`, `Type4`, `Type5`) VALUES
(1001, 'AircraftMaterialsUK.com Ltd', 'AircraftMaterialsUK.com Ltd', '"Unit 7, Wycombe Industrial Mall"', '"West End Street,"', '"High Wycombe,"', 'Bucks.', 'HP11 2QY', '01245 496727', '01842 763497', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1002, 'Aviation Metals Ltd (Do not use after 14/07/08)', 'Aviation Metals Ltd', '"Michigan Drive,"', '"Tongwell,"', '"Milton Keynes,"', '', 'MK15 8JE', '01908 210012', '01908 210066', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1003, 'Alcester Broach & Tool Co.', 'Alcester Broach & Tool Co.', 'Pipers Road,', 'Redditch,', 'Worcs', '', 'B98 0HU', '01527 523107', '01527 526137', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1005, 'Allgear Tools', 'Allgear Tools', 'The Wharf', 'Carlton-on-Trent', 'Newark', 'Nottinghamshire', 'NG23 6NR', '01636 821100', '01636 821100', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1006, 'Aldruscilla', 'Aldruscilla', '8 Deer Park Road', 'Merton', 'London', '', 'SW13 3UU', '0208 543 8710', '0208 543 0605', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1007, 'A.C. Services (South West) Ltd', 'A.C. Services (South West) Ltd', 'Armoury Road', 'Lufton Trading Estate', 'Yeovil', 'Somerset', 'BA22 8RL', '01935 411710', '01935 411712', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1008, 'Ashton & Moore Limited', 'Ashton & Moore Limited', '12 Smith Street', 'Hockley', 'Birmingham', '', 'B19 3EX', '08456 188196', '08456 188197', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1009, 'Acorn Surface Technology', 'Acorn Surface Technology', 'Clover Street', 'Kirkby in Ashfield', 'Nottingham', '', 'NG17 7LJ', '01623 753107', '01623 754538', '', 'information@acornst.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1010, 'Apollo Metals Ltd (Do not use after 14/07/08)', 'Apollo Metals Ltd', '"Unit 21  Tanner Drive,"', 'Blakelands', 'Milton Keynes', '', 'MK14 5BU', '01908 612199', '02084430388', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1011, 'All Purpose Packaging', 'All Purpose Packaging', 'Aspect One', 'Gunnel\'s Wood Road', 'Stevenage', 'Hertfordshire', 'SG1 2DG', '(01438) 777001', '(01438) 777002', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1012, 'Able Group UK', 'Able Group UK', 'Able House', '39 Progress Road', 'Leigh-on-Sea', 'Essex', 'SS9 5PR', '0870 4130352', '0870 4130523', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1013, 'Abrasive Technology Limited', 'Abrasive Technology Limited', 'Unit 11 Boundary Business Court', '92-94 Church Road', 'Mitcham', 'Surrey', 'CR4 3TD', '0207 4710200', '0207 4710202', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1014, 'Attewell Ltd.', 'Attewell Ltd.', '"4 Southbridge Way,"', '"Southall,"', 'Middlesex', '', 'UB2 4BY', '0208 571 0055', '0208 571 7139', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1015, 'Andrews Heating and Drying', 'Andrews Heating and Drying', 'Archers Field', 'Burnt Mills Industrial Estate', 'Basildon', 'Essex', 'SS13 1DH', '0800 211 611', '01268 286664', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1016, 'Aquarius Plastics Ltd', 'Aquarius Plastics Ltd', 'Eydon House', '13 Midleton Industrial Estate', 'Guildford', 'Surrey', 'GU2 8XW', '(01483) 576044', '(01483) 302986', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1017, 'All Metal Services Ltd.', 'All Metal Services Ltd.', '"Unit 3, Trillenium"', 'Gorsey Lane', 'Coleshill', 'Birmingham', 'B46 1JU', '01675 430307', '01675 430346', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1019, 'Allday Time Systems', 'Allday Time Systems', 'Lynchford House', 'Lynchford Lane', 'Farnborough', 'Hampshire', 'GU14 6JD', '(01252) 544457', '(01252) 544463', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1020, 'Apperley Honing', 'Apperley Honing', 'Alpha Works', 'Alstone Lane', 'Cheltenham', '', 'GL51 8ES', '01242 525868', '01242 224738', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1021, 'Air Parts Ltd', 'Air Parts Ltd', '"Unit B2, Kingswey Business Park"', 'Forsyth Road', 'Woking', 'Surrey', 'GU21 5SA', '(01483) 227570', '(01483) 769189', 'www.air-parts.co.uk', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1022, 'Abbey Metal Finishing Co Ltd', 'Abbey Metal Finishing Co Ltd', 'Weddington Road', 'Nuneaton', 'Warwickshire', '', 'CV10 0AJ', '02476 350444', '02476 641299', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1023, 'Amari Plastics Plc', 'Amari Plastics Plc', 'Paycocke Road', 'Basildon', 'Essex', '', 'SS14 3NW', '01268 884444', '01268 884466', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1024, 'Aberlink Ltd', 'Aberlink Ltd', 'Eastcombe', 'Gloucestershire', '', '', 'GL6 7DY', '01453 884461', '01453 882348', 'www.aberlink.co.uk', 'sales@aberlink.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1027, 'A.P. Services', 'A.P. Services', '14 Norfolk Road', 'Feltham', 'Middlesex', '', 'TW13 5BX', '07860 427628', '0208 384 1450', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1030, 'Applied Quality Assurance Ltd', 'Applied Quality Assurance Ltd', 'Garden House', '25 Bedford Road', 'Shefford', 'Bedfordshire', 'SG17 5DJ', '01462 850011', '01462 850012', 'www.aqacalibration.co.uk', 'sales@aqacalibration.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1031, 'Advanced Protective Packaging Ltd', 'Advanced Protective Packaging Ltd', '25 Towerfield Road', 'Shoeburyness', 'Essex', '', 'SS3 9QT', '01702 293312', '01702 298556', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1032, 'ASG (Essex) Ltd', 'ASG (Essex) Ltd', '1 Bentalls', 'Basildon', 'Essex', '', 'SS14 3BS', '01268 706800', '01268 706801', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1033, 'AMB Stainless & Non Ferrous Ltd', 'AMB Stainless & Non Ferrous Ltd', '39 Washford Road', 'Sheffield', '', '', 'S9 3XW', '0114 243 0984', '0114 242 6657', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1034, 'A & H (Aircraft) Ltd', 'A & H (Aircraft) Ltd', 'Lovet Road', '', 'Harlow', 'Essex', 'CM19 5TB', '(01279) 412167', '(01279) 450749', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1035, 'A.L. Machine Tools Ltd', 'A.L. Machine Tools Ltd', '3 Hidcote Avenue', 'Sutton Coldfield', 'West Midlands', '', 'B76 1SA', '0121 313 1037', '0121 313 1037', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1036, 'Advanced Seals & Gaskets Ltd', 'Advanced Seals & Gaskets Ltd', 'Polymer Works', 'Hope Street', 'Dudley', '', 'DY2 8RS', '01384 252555', '01384 252373', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1037, 'Aalco', 'Aalco', 'Units 6 & 7', 'The Interchange', 'Wested Lane', '"Swanley ,Kent"', 'BR8 8TE', '01322610900', '01322610910', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1038, 'Associated Floor Coverings', 'Associated Floor Coverings', '1492/1494 London Road', 'Leigh-on-Sea', 'Essex', '', 'SS9 2UR', '01072710600', '01702471120', 'www.assocfloor.co.uk', 'info@assocfloor.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1039, 'Ankon Limited', 'Ankon Limited', 'Unit 11', 'Brunel Road', 'Manor Trading Estate', '"Benfleet,Essex"', 'SS7 4PS', '01268 566900', '01268 756607', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1040, 'Arc Training UK Ltd', 'Arc Training UK Ltd', 'Unit 3', 'Garage Lane Industrial Estate', 'Setchey', '"Kings Lynn, Norfolk"', 'PE33 0BE', '0800 8493081', '01553 811700', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1041, 'Acenta Steel Distribution', 'Acenta Steel Distribution', 'Central Warehouse', 'Paynes Lane', 'Rugby', '', 'CV21 2UW', '01788 542191', '01788 535352', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1043, 'Acedes Gear Tools Limited', 'Acedes Gear Tools Limited', '"Fleming Road,"', '"Newbury,"', 'Berkshire.', '', 'RG14 2DE', '(01635) 524252', '(01635) 521085', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1044, 'ATI Stellram', 'ATI Stellram', 'Hercules Way', 'Bowerhill', '"Melksham,Wiltshire"', 'UK', 'SN12 6TS', '01225 897100', '01225 897111', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1045, 'Abby Couriers Ltd', 'Abby Couriers Ltd', 'Stella House', 'Luckyn Lane', 'Basildon', 'Essex', 'SS14 3AX', '01268 330330', '01268 330331', 'www.abbycouriers.co.uk', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1046, 'Aid-Pack Systems Ltd', 'Aid-Pack Systems Ltd', '"Unit A, Paddock Wood Distribution Centre"', 'Paddock Wood', 'Tonbridge', 'Kent', 'TN12 6UU', '0800 542 4428', '0800 542 4429', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1047, 'Aaron Manufacturing Ltd', 'Aaron Manufacturing Ltd', 'Unit K', '25-27 Willis Way', 'Poole', 'Dorset', 'BH15 3TD', '01202 670071', '01202 682952', 'www.aaronmanufacturing.co.uk', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1048, 'AEM Limited', 'AEM Limited', '6-8 Wilton Road', 'Ramsgate', 'Kent', '', 'CT12 4BT', '01279 682519', '01279 682510', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1049, 'A & G Precision and Sons Ltd', 'A & G Precision and Sons Ltd', 'Preesall Mill Industrial Estate', 'Park Lane', 'Preesall', '"Poulton-le-Fylde,Lancashire"', 'FY6 0LU', '01253 812451', '01253 812828', 'www.agprecision.co.uk', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1050, 'Aerospace Surface Treatments Ltd', 'Aerospace Surface Treatments Ltd', 'Units 1 & 2 Farrow Road', 'Chelmsford Industrial Park', 'Chelmsford', 'Essex', 'CM1 3TH', '01245 350444', '01245 492901', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1051, 'Avon Valley Precision Engineering Ltd', 'Avon Valley Precision Engineering Ltd', 'Bath Road', 'Bitton', 'Bristol', '', 'BS30 6HZ', '0117 9328118', '0117 9328119', 'www.avonvalley-eng.co.uk', 'sales@avonvalley-eng.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1052, 'Aeromet International PLC', 'Aeromet International PLC', 'Eurolink Way', '', 'Sittingborne', 'Kent', 'ME10 3RN', '01795 415000', '01795 415015', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1053, 'P.F. Ahern (London) Ltd', 'P.F. Ahern (London) Ltd', 'Oliver Close', 'West Thurrock', 'Essex', '', 'RM20 3EE', '01708 865599', '01708 686638', 'enquiries@ahern.co.uk', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1055, 'ADT Fire and Security Plc', 'ADT Fire and Security Plc', '"Unit 5, Redwing Court Business Centre"', 'Ashton Road', 'Romford', 'Essex', 'RM3 8QQ', '01708 544000', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1057, 'Action EPC Ltd', 'Action EPC Ltd', '"Unit 14, Adams Business Centre"', 'Cranes Farm Road', 'Basildon', 'Essex', 'SS14 3JF', '01268 288387', '01268 288956', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1059, 'Abros Blinds (1992)', 'Abros Blinds (1992)', 'Unit 5 Gibcracks', 'Basildon', 'Essex', '', 'SS14 1PE', '01268 556405', '', '', 'www.abrosblinds.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1062, 'Adore Recruitment', 'Adore Recruitment', '28-32 High Road', 'Laindon', 'Essex', '', 'SS15 4HU', '01268 454151', '', 'www.adore-recruitment.co.uk', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1063, 'Aeropia Limited', 'Aeropia Limited', 'Aeropia House', 'Newton Road', 'Crawley', '', 'RH10 9TY', '01293 459500', '01293 459600', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1064, 'Avatan Handling Equipment Ltd', 'Avatan Handling Equipment Ltd', '27 Blythwood Road', 'Pinner', 'Middlesex', '', 'HA5 3QD', '0208 4294444', '0208 8669512', 'www.avatan.co.uk', 'salesoffice@avatan.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1069, 'Amari Aerospace Ltd', 'Amari Aerospace Ltd', 'Unit 1', 'Mauretania Road', 'Nursling', '"Southampton,Hants"', 'SO16 OYS', '02380742750', '02380741947', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1071, 'Ashford Couriers', 'Ashford Couriers', '22 Russell Road', 'Folkestone', 'Kent', '', 'CT19 5RJ', '01303 252151', '01303 251139', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1072, 'Arrowsmith Engineering (Coventry) Ltd', 'Arrowsmith Engineering (Coventry) Ltd', '50 Bayton Road', 'Exhall', 'Coventry', '', 'CV7 9EJ', '02476 361773', '02476 368126', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1102, 'BOC', 'BOC', 'Customer Service Centre', 'Priestley Road', 'Worsley', 'Manchester', 'M28 2UT', '0800 111 333', '0800 111 555', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1104, 'Brooklands Automation Ltd', 'Brooklands Automation Ltd', 'Hunter Terrace', 'Fletchworth Gate Industrial Estate', 'Coventry', '', 'CV5 6SP', '02476 671030', '02476 671033', '"Factory sold, call Dick Bulter direct"', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1106, 'BIS Door Systems Limited', 'BIS Door Systems Limited', 'P.O. Box 1', 'Wickford', 'Essex', '', '', '01268 767566', '01268 560284', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1107, 'BSA Oilite Bearings', 'BSA Oilite Bearings', 'Elton Park Works', 'Hadleigh Road', 'Ispwich', '', 'IP2 0HU', '01473 233300', '01473 230424', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1109, 'Benfleet Scrap Co. Ltd', 'Benfleet Scrap Co. Ltd', '"Unit 16, Brunel Road"', 'Manor Trading Estate', 'Thundersley', 'Essex', 'SS7 4PS', '01268 792494', '01268 566121', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1110, 'Bekham Resourcing', 'Bekham Resourcing', '"Suite 1, 12 Hornsby Square"', 'Southfields', 'Laindon', 'Essex', 'SS15 6SD', '01268 540001', '01268 540002', 'www.bekham.co.uk', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1112, 'BSI Management Systems', 'BSI Management Systems', 'PO Box 8000', 'Milton Keynes', '', '', 'MK14 6WW', '01908 228470', '01908 228042', '', 'client.support@bsi-global.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1113, 'Batalas Limited', 'Batalas Limited', 'Briar Mill House', '21 Inett Way', 'Droitwich Spa', 'Worcestershire', 'WR9 0DN', '0870 750 4400', '0870 706 0321', '', 'enquiries@batalas.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1114, '"Brock, L & T.I. Co Ltd. (Tangi Flow)"', '"Brock, L & T.I. Co Ltd. (Tangi Flow)"', '"Unit 1, 19 Falkland Close"', 'Charter Avenue Industrial Estate', 'Canley', 'Coventry', 'CV4 8AG', '02476 421200', '02476 421459', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1115, 'Bodycote Heat Treatments Ltd', 'Bodycote Heat Treatments Ltd', 'Chard Business Park', 'Chard', 'Somerset', '', 'TA20 1EA', '01460 67957', '01460 67962', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1117, 'B2 Ltd T/A Firestorm Marketing Solutions', 'Firestorm Marketing Solutions', 'Suite 1 First Floor', 'Matmar House', '35 Hull Road', 'York', 'YO10 3JW', '01904 438156', '01904 438172', 'www.FireStormltd.co.uk', 'sales@FireStormltd.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1119, 'Boddingtons Plastics Ltd', 'Boddingtons Plastics Ltd', 'Wheelbarrow Park Estate', 'Pattenden Lane', '"Marden, Tonbridge"', 'Kent', 'TN12 9QJ', '01622 833700', '01622 833701', '', 'sales@boddingtons.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1120, 'Balin Distribution Limited (Palletline)', 'Balin Distribution Limited (Palletline)', 'Harvey Road', 'Burnt Mills Industrial Estate', 'Basildon', 'Essex', 'SS13 1DF', '01268 723600', '01268 590968', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1122, 'BCMZ Precision Engineering Ltd', 'BCMZ Precision Engineering Ltd', '"Unit 2, Crestland Works"', 'Bull Lane Industrial Estate', '"Acton, Sudbury"', 'Suffolk', 'CO10 0BD', '01787 880699', '01787377966', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1123, 'Becker UK Ltd', 'Becker UK Ltd', '"Unit 1, Neptune Business Estate"', 'Medway City Estate', 'Rochester', 'Kent', 'ME2 4LT', '01634 730333', '01634 730444', '', 'sales@becker.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1124, 'Brammer', 'Brammer', '"Unit 5, Basildon Trade Centre, Luckyn Lane"', 'Off Miles Gray Road', 'Basildon', 'Essex', 'SS14 3GD', '01268 533515', '01268 532398', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1125, 'Bodycote Heat Treatments Ltd (Closed Feb 2009)', 'Bodycote Heat Treatments Ltd', 'Garth Road', 'Lower Morden', 'Surrey', '', 'SM4 4LT', '0208 337 7744', '0208 330 2699', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1126, 'Bodycote Heat Treatments Ltd', 'Bodycote Heat Treatments Ltd', 'Denny Industrial Estate', 'Pembroke Avenue', 'Waterbeach', 'Cambridge', 'CB5 9QR', '01223 860741', '01223 863498', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1127, 'Bodycote Metal Technology Ltd', 'Bodycote Metal Technology Ltd', 'Hulley Road', '', 'Macclesfield', 'Cheshire', 'SK10 2SG', '01625 505300', '01625 505311', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1128, 'Bodycote Heat Treatments Ltd', 'Bodycote Heat Treatments Ltd', 'F.A.O. WILLIAM LAMBERT', '', '', '', '', '01922 453388', '01992 459649', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1129, 'Bohler Special Steels', 'Bohler Special Steels', 'European Business Park', 'Taylors Lane', 'Oldbury', 'West Midlands', 'B69 2BN', '0121 552 2575', '0121 552 0023', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1130, 'British Safety Council       ', 'British Safety Council       ', '70 Chancellors Road', 'Hammersmith', 'London', '', 'W6 9RS', '0208 600 5577', '0208 563 7657', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1131, 'B.E. Wedge Ltd', 'B.E. Wedge Ltd', 'Strafford Street', 'Willenhall', 'West Midlands', '', 'WV13 1RZ', '01902 630311', '01902 366353', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1132, 'Beard and Fitch Ltd', 'Beard and Fitch Ltd', 'Crammond Park', 'Lovet Road', 'Harlow', 'Essex', 'CM19 5TF', '01279 425358', '01279 425187', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1133, 'Buildcraft', 'Buildcraft', '46 Cherrywood Rise', 'Orchard Heights', 'Ashford', 'Kent', 'TN25 4QA', '01233 643136', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1201, 'John Clayden & Partners (Lubysil) Ltd', 'John Clayden & Partners (Lubysil) Ltd', '9 Frensham Road', 'Sweet Briar Industrial Estate', 'Norwich', 'Norfolk', 'NR3 2BT', '01603 789924', '01603 417335', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1203, 'Caterbridge (Metalweb plc)', 'Caterbridge (Metalweb plc)', '"Unit 9, Trident Trading Estate"', 'Pindar Road', 'Hoddesdon', 'Hertfordshire', 'EN11 0WZ', '01992 456900', '01992 450557', '', 'CJCORN3@aol.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1206, 'Chaucer Solutions Ltd', 'Chaucer Solutions Ltd', 'Leycourt Farm', 'Eltisley Road', 'Great Gransden', '"Nr. Sandy, Beds"', 'SG19 3AS', '01767 677445', '01767 677655', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1207, 'City Link', 'City Link', 'Unit 3 - No 1 Sheepcotes', 'Springfield Business Park', 'Chelmsford', 'Essex', 'CM2 5AE', '01245 399 844', '01245 450 881', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1208, 'Colledge & Morley (Gears) Ltd', 'Colledge & Morley (Gears) Ltd', 'Curriers Close', 'Charter Avenue', 'Coventry', '', 'CV4 8AW', '02476 462328', '02476 694008', '', 'colledgeandmorley@compuserve.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1209, 'C.S. Inspection & Calibration Services Ltd', 'C.S. Inspection & Calibration', '"1 & 2 Fletchers Square,"', '"Temple Farm Ind. Estate, Sutton Road"', 'Southend on Sea', 'Essex', 'SS2 5RN', '01702 617107/8', '01702 469151', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1211, 'Creative Instrumentation Ltd', 'Creative Instrumentation Ltd', 'Cavea House', 'Decoy Road', 'Worthing', 'West Sussex', 'BN14 8ND', '01903 204542', '01903 215678', 'www.creativevacuum.co.uk', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1213, 'Clevedon Fastners', 'Clevedon Fastners', '11 Reddicap Trading Estate', 'Sutton Coldfield', 'West Midlands', '', 'B75 7DG', '0121 378 0619', '0121 378 3186', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1215, 'Curtis Machine Tools Ltd', 'Curtis Machine Tools Ltd', 'Martells Industrial Estate', 'Slough Lane', '"Ardleigh, Colchester"', 'Essex', 'C07 7RU', '01206 230032', '01206 231426', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1218, 'Cannon Hygiene Limited', 'Cannon Hygiene Limited', '', '', '', '', '', '', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1219, 'CSJ Enterprises', 'CSJ Enterprises', 'Evesham Road', 'Bishops Cleeve', 'Cheltenham', 'Gloucestershire', 'GL52 8SA', '01242 679386', '01242 678753', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1220, 'Capital Equipment & Machinery Ltd', 'Capital Equipment & Machinery Ltd', 'Mill Mead', 'Staines', 'Middlesex', '', 'TW18 4UQ', '01784 456151', '01784 466481', '01444245414', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1221, 'Cromwell Tools Ltd', 'Cromwell Tools Ltd', 'Unit 22', 'Heronsgate Industrial Estate', 'Paycocke Road', '"Basildon, Essex"', 'SS14 3EU', '01268 532555', '01268 289714', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1223, 'CKC Engineering Sevices', 'CKC Engineering Sevices', '31 Bowlers Croft', 'Honywood Road', 'Basildon', 'Essex', 'SS14 3DZ', '01268 273188', '01268 273199', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1224, 'Certex Lifting Products', 'Certex Lifting Products', 'Units 1 & 3 Viking Way', '"Off Church Manorway,"', 'Erith', 'Kent', 'DA16 1DB', '01322 442323', '01322 441044', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1225, 'J.J. Churchill Limited', 'J.J. Churchill Limited', 'Station Road', 'Market Bosworth', 'Nuneaton', '', 'CV13 0PF', '01455 299600', '01455 292330', 'www.jjchurchill.com', 'mail@jjchurchill.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1227, 'Connolly Refrigeration Limited', 'Connolly Refrigeration Limited', 'Howlett Way', 'Fison Way Industrial Estate', 'Thetford', 'Norfolk', 'IP24 1HZ', '01842 766655', '01842 763497', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1229, 'CML Alloys', 'CML Alloys', '"Building 8, First Avenue"', 'The Pensnett Estate', 'Kingswinford', 'West  Midlands', 'DY6 7TG', '01384 282400', '01384 270800', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1230, 'Calmet Laboratory Services', 'Calmet Laboratory Services', 'Hampton House', '1 Vicarage Road', 'Kingston-upon-Thames', 'Surrey', 'KT1 4EB', '0208 977 8455', '0208 614 8048', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1232, 'CBA Metals', 'CBA Metals', 'Daux Road', '', 'Billingshurst', 'West Sussex', 'RH14 9SN', '(01403) 783944', '(01403) 785244', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1233, 'Carpenter Technology (UK) Ltd', 'Carpenter Technology (UK) Ltd', 'Napier Way', 'Crawley', 'West Sussex', '', 'RH10 9RA', '01293 586112', '01293 537313', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1235, 'Cutter Grinding Services', 'Cutter Grinding Services', 'Mill Yard', '22b Guildford Street', 'Luton', 'Bedfordshire', 'LU1 2NR', '(01582) 486018', '(01582) 404164', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1236, 'Caparo Testing Technologies', 'Caparo Testing Technologies', 'Inspection House', '61 Albert Road North', 'Reigate', 'Surrey', 'RH2 9RS', '01737 222211', '01737 234228', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1237, 'C & F Millier Limited', 'C & F Millier Limited', '272 Southmead Road', 'Westbury on Tyrm', 'Bristol', '', 'BS10 5EW', '0117 950 5252', '0117 950 8969', 'www.cfmillier.co.uk', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1238, 'Columbia Metals Ltd.       ', 'Columbia Metals Ltd.       ', '"Wingfield Mews,"', '"Wingfield Street,"', 'Peckham', 'London', 'SE15 4LH', '0207 732 1022/8', '0207 732 1029', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1240, 'Capital Aluminium Extrusions Ltd', 'Capalex', 'Leconfield Industrial Estate', 'Cleator Moor', 'Cumbria', '', 'CA25 5QB', '01946 811771', '01946 813681', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1241, 'CNC Fluids Ltd', 'CNC Fluids Ltd', 'Broadmead Lane', 'Keynsham', 'Bristol', '', 'BS31 1ST', '0117 9867744', '0117 9867764', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1245, 'C.P. Cantwell & Co Ltd', 'C.P. Cantwell & Co Ltd', '60 London Road', 'Wickford', 'Essex', '', 'SS12 0AN', '01268 733433', '0208 633 1026', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1246, 'Cross Country Engineering (1987) Ltd', 'Cross Country Engineering (1987) Ltd', '"Unit 4, Darby Gate"', 'West Portaway', 'Andover', 'Hants', 'SP10 3LF', '01264 351409', '01264 333921', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1247, 'Coruba', 'Coruba', '"Unit 30, Purdeys Way"', 'Purdeys Industrial Estate', 'Rochford', 'Essex', 'SS4 1ND', '0845 130 1375', '0845 130 8375', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1248, 'Calor Gas Limited', 'Calor Gas Limited', 'Coryton Calor Direct', 'Manor Way', 'Coryton', 'Stanford - le - Hope', 'SS17 9LW', '01375665693', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1249, 'Cube 6 IT Limited', 'Cube 6 IT Limited', 'Lawrence House', 'The Street', 'Hatfield Peverel', 'Essex', 'CM3 2DN', '0872 331 0057', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1250, 'Actionbrook Services (Communicate)', 'Actionbrook Services (Communicate)', 'The Old Barn', 'East Hanningfield Road', 'Chelmsford', '', 'CM3 8EW', '01245 400044', '01245 400469', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1251, 'Christy Cooling Services Ltd', 'Christy Cooling Services Ltd', 'Temple Wood Estate', 'Stock Road', 'Chelmsford', 'Essex', 'CM2 8LP', '01277 841500', '01277 841731', '', 'christy.cooling@live.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1301, 'Delapena Honing Equipment    ', 'Delapena Honing Equipment    ', '"PO Box 6,"', '"Tewksbury Road,"', 'Cheltenham', 'Glos', 'GL51 9HS', '01242 516341', '01242 221246', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1303, 'C. Dugard  Limited', 'C. Dugard  Limited', '"75 Old Shoreham Road,"', '"Hove,"', 'East Sussex.', '', 'BN3 7BX', '01273 732286', '01273 206843', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1304, 'Dropsa UK Ltd', 'Dropsa UK Ltd', 'Eggham Business Village', 'Thorpe Industrial Estate', 'Eggham', 'Surrey', 'TW20 8RB', '01784 431177', '01784 438598', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1305, 'Digital Office Supplies Ltd', 'Digital Office Supplies Ltd', 'Bentalls House', '"Bentalls, Pipps Hill"', 'Basildon', 'Essex', 'SS14 3BS', '01268 532100', '01268 532102', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1306, 'Datel Solutions Limited', 'Datel Solutions Limited', '11 Stephens Crescent', 'Horndon on the Hill', 'Essex', '', 'SS17 8LZ', '01375 673119', '', '', 'datel_uk_99@yahoo.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1307, 'Deburring Centre Ltd', 'Deburring Centre Ltd', 'Unit 7 Warren Way', 'Holton Heath Trading Park', 'Poole', 'Dorset', 'BH16 6NJ', '01202 627130', '01202 627131', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1309, 'DPGE Rebuilds Ltd', 'DPGE Rebuilds Ltd', '"Unit 11, Martor Industrial Units"', 'Tormarton Road', 'Marshfield', 'Wiltshire', 'SN14 8LJ', '01225 892226', '01225 892129', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1310, 'Dathan Tool & Gauge Co. Ltd', 'Dathan Tool & Gauge Co. Ltd', 'Mean Lane', 'Meltham', 'Holmfirth', 'West Yorkshire', 'HD9 5RU', '01484 851207', '01484 852271', '', 'sales@dathan.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1311, 'DLoG (UK) Limited', 'DLoG (UK) Limited', 'Gannaway Lane', 'Newtown', 'Tewkesbury', 'Gloucestershire', 'GL20 8SJ', '0121 544 6256', '0845 658 5525', 'www.dlog.co.uk', 'info@dlog.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1312, 'Doncasters Bramah', 'Doncasters Bramah', 'Holbrook Works', 'Station Road', 'Halfway', 'Sheffield', 'S20 3GB', '0114 251 2000', '0114 251 4720', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1313, 'Delta Rubber Ltd', 'Delta Rubber Ltd', 'Unit 13G', 'Queensway', 'Stem Lane Industrial Estate', '"New Milton, Hants"', 'BH25 5NN', '01425 621900', '01425 621202', 'www.deltarubber.co.uk', 'deltasales@deltarubber.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1314, 'Dens Metals Ltd', 'Dens Metals Ltd', 'Fowler Road', 'West Pitkerro Industrial Estate', 'Dundee', 'Scotland', 'DD5 3RU', '01382 735801', '01382 735808', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1315, 'Dentons Fencing', 'Dentons Fencing', '16B Crowstone Road', 'Westcliffe', 'Essex', '', 'SS0 8BA', '01268553836', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1316, 'Dedicated Couriers Limited', 'Dedicated Couriers Limited', '"Unit 5, Fieldhouse Industrial Estate"', 'Fieldhouse Way', 'Sheffield', '', 'S4 7SF', '0114 256 2282', '0114 256 2283', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1318, 'Dyno-Rod', 'Dyno-Rod', 'Housesteads', 'Stockport Road', 'Hattersley', '', 'SK14 3QU', '01245 690155', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1401, 'Essex Packaging Supplies', 'Essex Packaging Supplies', '9 Bryant Ave', 'Romford', 'Essex', '', 'RM3 0AP', '01708-384949', '01708-381262', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1402, 'Elcometer Instuments Ltd', 'Elcometer Instuments Ltd', 'Edge Lane', 'Manchester', '', '', 'M43 6BU', '0161 371 6020', '0161 371 6010', 'www.elcometer.com', 'dpsales@elcometer.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1403, 'Edward Pryor & Sons Ltd', 'Edward Pryor & Sons Ltd', 'Egerton Street', 'Sheffield', '', '', 'S1 4JX', '0114 276 6044', '0114 276 6890', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1404, 'European Drives and Motor Repairs Ltd', 'European Drives and Motor Repairs', '9 Mansion Close', 'Moulton Park', '"Northampton,"', '', 'NN3 6RU', '01604 499777', '01604 492777', 'www.edmr.co.uk', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1407, 'Elmatic (Cardiff) Ltd', 'Elmatic (Cardiff) Ltd', 'Wentloog Road', '', 'Rumney', 'Cardiff', 'CF3 8XH', '02920 778727', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1409, 'Eaton Aerospace Limited', 'Eaton Aerospace Limited', 'Abbey Park', 'Southampton Road', '"Titchfield, Fareham"', 'Hampshire', 'PO14 4QA', '01329 853786', '01329 853894', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1410, 'Ensinger Ltd', 'Ensinger Ltd', 'Raynham Road', 'Bishop\'s Stortford', 'Hertfordshire', '', 'CM23 5PE', '01279 655145', '01279 715025', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1411, 'Expert (DO NOT USE)', 'Expert (DO NOT USE)', '"Garth Road,"', '"Lower Morden,"', '', 'Surrey', 'SM4 4LT', '0208 337 7744', '0208 330 2699', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1412, 'Eltro (GB) Limited', 'Eltro (GB) Limited', 'Unit B4 Armstong Mall', 'Southwood Business Park', 'Farnbrough', 'Hampshire', 'GU14 0NR', '01252 523000', '01252 522338', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1413, 'Enpar Special Alloys Limited', 'Enpar Special Alloys Limited', 'Station Road', 'Ecclesfield', 'Sheffield', '', 'S35 9YR', '0114 219 3002', '0114 219 1145', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `supplier` (`pkSupplierID`, `SupplierName`, `Alias`, `Address1`, `Address2`, `Address3`, `Address4`, `Postcode`, `Phone`, `Facsimile`, `Website`, `Email`, `MinOrderCharge`, `PaymentTerms`, `MaterialSupplier`, `TreatmentSupplier`, `CalibrationSupplier`, `ToolingSupplier`, `MiscSupplier`, `Deleted`, `fkUserID`, `StdDeliveryDays`, `Day1Open`, `Day1Close`, `Day2Open`, `Day2Close`, `Day3Open`, `Day3Close`, `Day4Open`, `Day4Close`, `Day5Open`, `Day5Close`, `Day6Open`, `Day6Close`, `Day7Open`, `Day7Close`, `QuestionsFilename`, `SQCompletedBy`, `Type1`, `Type2`, `Type3`, `Type4`, `Type5`) VALUES
(1417, 'East Lancashire Platers Ltd', 'East Lancashire Platers Ltd', 'Oxford Mill', 'Oxford Road', 'Burnley', 'Lancashire', 'BB11 3BA', '(01282) 425621', '(01282) 433618', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1420, 'E.I.S. Industrial Suppliers Ltd', 'E.I.S. Industrial Suppliers Ltd', 'Castle Lodge', '17 Castle Way', 'Leybourne', 'Kent', 'ME19 5HF', '0800 783 6006', '0800 783 6006', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1422, 'Equip (Midlands) Ltd', 'Equip (Midlands) Ltd', 'Byron Street', 'Buxton', 'Derbyshire', '', 'SK17 6NT', '01298 22233', '01298 72097', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1426, 'Environmental Scientifics Group', 'Environmental Scientifics Group', 'Acrewood Way', 'St Albans', 'Hertfordshire', '', 'AL4 0JY', '01727 816703', '01727 816700', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1427, 'European Ballscrew Services Ltd', 'European Ballscrew Services Ltd', 'Rose Cottage', 'North Buckland', 'Braunton', 'North Devon', 'EX33 1HY', '01271 890769', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1429, 'ESG', 'ESG', 'Acrewood Way', 'St Albans', 'Hertfordshire', '', 'AL4 0JY', '01727 816702', '01727 816700', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1430, 'Essex Freight Services Ltd', 'Essex Freight Services Ltd', '1 Middleton Row', 'South Woodham Ferrers', 'Chelmsford', 'Essex', 'CM3 5WE', '01245 426831', '01245 328713', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1432, 'Engineerstore (A.J. Howard)', 'Engineerstore (A.J. Howard)', 'East Street', 'Prittlewell', 'Southend-on-Sea', 'Essex', 'SS2 5EQ', '(01702) 611711', '(01702) 600050', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1433, 'EDP UK Limited', 'EDP UK Limited', '"Unit 4 ,The Forum"', 'Coopers Way', 'Temple Farm Industrial Estate', '"Southend on Sea , Essex"', 'SS2 5TE', '01702 618877', '01702 618811', 'www.edpgroup.co.uk', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1434, 'Electron Beam Processes Ltd', 'Electron Beam Processes Ltd', '"Unit 4, Octimum"', '"Kingswey Business Park, Forsyth Road"', 'Woking', 'Surrey', 'GU21 5SA', '01483 215400', '01483 215444', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1501, 'Fine Limit Welding', 'Fine Limit Welding', '10 Britannia Court', 'Basildon', 'Essex', '', 'SS13 1EU', '01268 725988', '01268 725988', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1502, 'Five Star Communications', 'Five Star Communications', 'St Olaves', 'Grove Road', 'Little Clacton', 'Essex', 'CO16 9NG', '01277 355555', '', '', 'sales@fivesatrcomms.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1503, 'Fenn Tool Ltd', 'Fenn Tool Ltd', '44 Springwood Drive', 'Springwood Industrial Estate', 'Braintree', 'Essex', 'CM7 2YN', '01376 347566', '01376 550827', 'www.fenntool.com', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1504, 'Frenco International (David Brown)', 'Frenco International (David Brown)', 'Unit 11', 'Fortnum Close', 'Kitts Green', 'Birmingham', 'B33 0LG', '0121 789 7895', '0121 789 7050', 'http-www.textron.com', 'info@frencotextron.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1506, 'Filtermist International Limited', 'Filtermist International Limited', 'Faraday Drive', 'Bridgnorth', 'Shropshire', '', 'WV15 5BA', '01746 765361', '01746 767807', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1509, 'Four Com', 'Four Com', '', '', '', '', '', '', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1510, 'Farleigh CNC Services', 'Farleigh CNC Services', 'PO Box 512', 'Maidstone', 'Kent', '', 'ME150BA', '01622 728596', '0870 0510818', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1511, 'Forest Heath Fasteners', 'Forest Heath Fasteners', 'Unit 12', 'Hampstead Avenue', 'Mildenhall', 'Suffolk', '', '01638 716170', '01638 510728', 'www.forestheath.co.uk', 'sales@forestheath.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1512, 'First Machine Tool Accessories', 'First Machine Tool Accessories', 'Unit 1', 'The Headlands', 'Downton', 'Salisbury', 'SP5 3JJ', '01725 512517', '01725 512529', '', 'enquiries@1mta.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1513, 'Floyd Automatic Tooling Ltd', 'Floyd Automatic Tooling Ltd', '17 Bondor Business Centre', 'London Road', 'Bladock', 'Hertfordshire', 'SG7 6HP', '(01462) 491919', '(01462) 490835', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1514, 'Fanuc FA UK Ltd', 'Fanuc FA UK Ltd', 'Fanuc House', 'No.1 Station Approach', 'Ruislip', 'Middlesex', 'HA4 8LF', '01895 634182', '01895 676140', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1520, 'Forst UK Ltd.', 'Forst UK Ltd.', '14 Dartford Road', '', 'Leicester', '', 'LE2 7PR', '0116 245 2000', '0116 245 2037', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1601, 'Garibaldi Grinders', 'Garibaldi Grinders', '"Unit 14, Brookside Business Park"', 'Rustington', 'West Sussex', '', 'BN16 3LP', '01903 779082', '01903 779082', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1603, 'Glacier Garlock Bearings Ltd', 'Glacier Garlock Bearings Ltd', 'Riccarton', '', 'Kilmarnock', 'Ayrshire', 'KA1 3NA', '(01563) 539999', '(01563) 571426', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1604, 'Goldcrest Oil Limited', 'Goldcrest Oil Limited', '35-37 Armstrong Road', 'Manor Trading Estate', 'Thundersley', 'Essex', 'SS7 4PW', '01268 751222', '01268 565263', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1605, 'Goss Components Limited', 'Goss Components Limited', '43 Fulbourne Road', 'London', '', '', 'E17 4AF', '0208 527 5599', '0208 527 1142', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1606, 'General Engineering (Treatments) Ltd', 'General Engineering (Treatments) Ltd', '"Unit 8, Stock Road"', 'Stock Road Industrial Estate', 'Southend-on-Sea', 'Essex', 'SS2 5QF', '(01702) 467335', '(01702) 616778', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1607, 'Gould Alloys Ltd.', 'Gould Alloys Ltd.', '"Unit 2, Carrwood Ind. Park,"', '"Off Carrwood Road,"', 'Chesterfield.', '', 'S41 9QB', '(01246) 260000', '(01246) 260999', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1608, 'General Engineering (Treatments) Ltd', 'General Engineering (Treatments) Ltd', '6 Vanguard Way', 'Shoeburyness', 'Southend-on-Sea', 'Essex', 'SS3 9QY', '01702 292421', '01702 292483', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1609, 'Sean Gooding Forklifts Ltd', 'Sean Gooding Forklifts Ltd', '58 Louis Drive', 'Rayleigh', 'Essex', '', 'SS6 9DX', '01268 785745', '01268 785745', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1611, 'GB Plating Ltd', 'GB Plating Ltd', '23 - 25 Noble Square', 'Burnt Mills Industrial Estate', 'Basildon', 'Essex', 'SS13 1LP', '01268 727504', '01268 727524', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1612, 'Gardner CNC Ltd', 'Gardner CNC Ltd', '11 Kingshurst', 'Radford Semele', 'Leamington Spa', 'Warwickshire', 'CV31 1TG', '01926 452228', '01926 423124', '', 'info@gardner-cnc.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1614, 'Gewefa Uk Ltd', 'Gewefa Uk Ltd', 'Edinburgh Way', 'Leafield Industrial Estate', 'Corsham', 'Wiltshire', 'SN13 9XZ', '01225811666', '01225811388', '', 'steve@gewefa.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1618, 'Gardner Aerospace', 'Gardner Aerospace', '4 Rowhedge Close', 'Wollaston Industrial Centre', 'Basildon', 'Essex', 'SS13 1QQ', '01268 729311', '01268 728951', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1702, 'Harps', 'Harps', 'Forum House', '2 Coopers Way', 'Temple Farm Industrial Estate', '"Southend-on-Sea, Essex"', 'SS2 5TE', '01702 462676', '01702 466276', '', 'john@harpsltd.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1703, 'Hone-All Precision Ltd', 'Hone-All Precision Ltd', 'Cherrycourt Way', 'Leighton Buzzard', 'Bedfordshire', '', 'LU7 4UH', '0845 5555 111', '0845 5555 222', 'www.hone-all.co.uk', 'info@hone-all.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1706, 'Haynes International Ltd', 'Haynes International Ltd', 'Parkhouse Street', 'Openshaw', 'Manchester', '', 'M11 2ER', '0161 230 2015', '0161 223 2412', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1707, 'Heidenhain (GB) Ltd', 'Heidenhain (GB) Ltd', 'Service Dept', '200 London Road', 'Burgess Hill', 'West Sussex', 'RH15 9RD', '01444 247711', '01444 480527', 'heidenhain.co.uk', 'servic@heidenhain.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1708, 'Hycrome (Europe) Ltd', 'Hycrome (Europe) Ltd', 'Heasandford Ind. Estate', 'Widow Hill Road', 'Burnley', 'Lancashire', 'BB10 2TT', '01282 418300', '01282 418310', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1709, 'Helical Components (Coventry) Limited', 'Helical Components (Coventry) Limited', 'Unit 2 Telford Road', 'Bayton Road Industrial Estate', 'Exhall', 'Coventry', 'CV7 9ES', '02476361058', '02476367270', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1710, 'Honeywell Aerospace (Treatment Shop)', 'Honeywell Aerospace (Treatment Shop)', 'Bunford Lane', 'Yeovil', 'Somerset', '', 'BA20 2YD', '01935 475181', '01935 427600', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1711, 'D.W. Hickey', 'D.W. Hickey', '4 Chequers', 'Bishop\'s Park', 'Bishop\'s Stortford', 'Hertfordshire', 'CM23 4BX', '01279 656509', '01279 656509', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1712, 'Hydrafeed Ltd', 'Hydrafeed Ltd', 'Talgrath House', 'Bond Avenue', 'Mount Farm Industrial Estate', 'Milton Keynes', 'MK1 1JD', '01908 376331', '01908 647843', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1714, 'HS Advisory Limited', 'HS Advisory Limited', 'Suite 17', 'Thamesgate House', 'Victoria Avenue', 'Southend-on-Sea', 'SS2 6BU', '08445717495', '', 'www.hsalimited.co.uk', 'sclark@hsalimited.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1715, 'HSS Hire', 'HSS Hire', 'Mace Lane Industrial Estate', 'Ashford', 'Kent', '', 'TN24 8PE', '01233 610 348', '01233 622834', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1717, 'Harmon Precision Grinding', 'Harmon Precision Grinding', '55 Haviland Road', 'Ferndown Industrial Estate', 'Wimborne', 'Dorset', 'BH21 7PY', '01202 654198', '01202 65199', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1719, 'HV Wooding Ltd', 'HV Wooding Ltd', 'Range Road Industrial Estate', 'Hythe', 'Kent', '', 'CT21 6HG', '01303 264471', '01303 262408', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1721, 'Heron Jader Ltd', 'Heron Jader Ltd', 'Station Road', '', 'Billericay', 'Essex', 'CM12 9DP', '01277 624444', '01277 631349', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1734, 'H & M Compressors  and Pumps Ltd', 'H & M Compressors  and Pumps Ltd', '"Unit B, Enterprise Centre"', 'Paycocke Road', 'Basildon', 'Essex', 'SS14 3DY', '01268 531288', '01268 532013', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1736, 'Hub Le Bas', 'Hub Le Bas', '"Nobel Road, Eley Trading Estate"', 'Angle Road', 'Edmonton', 'London', 'N18 3DW', '0208 803 1424', '0208 803 3187', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1737, 'Hillfoot Steel Group Ltd', 'Hillfoot Steel Group Ltd', 'Stockholding Division', 'Herries Road', 'Sheffield', '', 'S6 1LS', '0114 233 1133', '0114 285 2802', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1738, 'D. Hall Electrical Services Ltd', 'D. Hall Electrical Services Ltd', 'Unit 5', 'Lee Bridge Industrial Estate', 'Dean Clough', 'Halifax', 'HX3 5HE', '01422 300999', '01422 300777', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1801, 'Industrial Metal Services', 'Industrial Metal Services', '"PO Box 112, Metalstock House"', '"Vanguard Way, Shoeburyness"', 'Southend-on-Sea', 'Essex', 'SS3 9RE', '01702 296955', '01702 296444', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1802, 'Iscar Tools Ltd', 'Iscar Tools Ltd', 'Woodgate Business Park', 'Bartley Green', 'Birmingham', '', 'B32 3DE', '0121 422 8585', '0121 423 2789', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1806, 'Industrial Supply Alliance', 'Industrial Supply Alliance', 'East Street', 'Prittlewell', 'Southend-on-Sea', 'Essex', 'SS2 5EQ', '01702 611711', '01702 600050', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1807, 'Instrument Machining Service Ltd', 'Instrument Machining Service Ltd', '37 Howlett Way', 'Thetford', 'Norfolk', '', 'IP24 1HZ', '01842 752103', '01842 762795', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1808, 'IMTS Grinding Technology Ltd', 'IMTS Grinding Technology Ltd', '"Unit 11, Castleview Business Centre"', 'Gas House Road', 'Rochester', 'Kent', 'ME1 5QH', '01634 844477', '01634 844599', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1809, 'Instron Limited', 'Instron Limited', 'Coronation Road', 'High Wycombe', 'Buckinghamshire', '', 'HP12 3SY', '01494 456815', '01494 456667', 'www.instron.com', 'extra-uk@instron.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1811, 'Intercontinental Chemical Products Ltd', 'Intercontinental Chemical Products', '56-62 Lincoln Road', 'Tuxford', 'Newark', 'Notts', 'NG22 0HP', '01777 870756', '01777 871766', '', 'sales@intchems.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1812, 'Iso-Tech Calibration Services Ltd', 'Iso-Tech Calibration Services Ltd', '"Unit 3, Riverside Park"', 'East Service Road', 'Raynesway', 'Derby', 'DE21 7RW', '01332 664245', '01332 668399', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1814, 'Inspiratech 2000 Ltd', 'Inspiratech 2000 Ltd', '38 Lawford Lane', 'Bilton', 'Rugby', 'Warks', 'CV22 7JP', '01788 522792', '01788 522905', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1815, 'Integral Machine Tool Services LLP', 'Integral Machine Tool Services LLP', '32 Longfurlong', 'Rugby', 'Warwickshire', '', 'CV22 5QT', '07796 607 981', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1816, 'Indusmond Ltd', 'Indusmond Ltd', '32 Chelwood House', 'Gloucester Square', 'London', '', 'W2 2SZ', '0207 7067640', '0207 7069672', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1817, 'Intergear Gearcutting Engineers', 'Intergear Gearcutting Engineers', '"Unit 2/4, Britannia Court,"', '"Burnt Mills Industrial Estate,"', 'Basildon', 'Essex', 'SS13 1EU', '01268 726985', '01268 726985', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1820, 'Industria Bearings and Transmissions Ltd', 'Industria Bearings and Transmissions Ltd', 'Atlantic House', '76 Cowley Mill Road', 'Uxbridge', 'Middlesex', 'UB8 2QE', '01895 237971', '01895 272191', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1903, 'Tony Jenns', 'Tony Jenns', '1 Violet Drive', 'Woolwell', 'Plymouth', 'Devon', 'PL6 8AS', '01752 512307', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1907, 'J.J. Castings Investments Ltd', 'J.J. Castings Investments Ltd', 'Caerphilly Business Park', 'Van Road', 'Caerphilly', '', 'CF83 3EL', '029 2088 7837', '029 2086 1900', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1908, 'J.R. Consultants', 'J.R. Consultants', 'Union House', '679 High Road', 'Benfleet', 'Essex', 'SS7 5SF', '01268 758000', '01268 757500', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1909, 'J.P. Aero-Com Engineering Co Ltd', 'J.P. Aero-Com Engineering Co Ltd', 'Cherry Tree Rise', 'Buckhurst Hill', 'Essex', '', 'IG9 6EY', '020 8504 8833', '020 8505 0697', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2001, 'Kampf Otto Ltd.', 'Kampf Otto Ltd.', '"46-54 Leigh Road,"', '"Wimborne,"', 'Dorset.', '', 'BH21 1AQ', '01202 883189', '01202 880352', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2002, 'Kent Metro Ltd', 'Kent Metro Ltd', 'Metro House', 'Units 10 - 127 Kings North Ind. Estate', '"Hoo, Rochester"', 'Kent', 'ME3 9ND', '01634 255905', '01634 255945', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2003, 'Kepston Limited', 'Kepston Limited', '13-15 Western Way', '', 'Wednesbury', 'West Midlands', 'WS10 7BW', '0121 502 2972', '0121 556 5300', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2006, 'Key Personnel', 'Key Personnel', 'Crispin House', 'Maldon Road', 'Witham', 'Essex', 'CM8 2UR', '01376 514100', '01376 514600', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2008, 'K and B Machine Tool Services Ltd', 'K and B Machine Tool Services Ltd', '1 Farrier Road', 'Lincoln', '', '', 'LN6 3RU', '01522 687878', '01522 687879', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2009, 'Knight Precision Engineering', 'Knight Precision Engineering', '19 Tallon Road', 'Hutton Industrial Estate', 'Brentwood', 'Essex', 'CM13 1TG', '01277 233171', '01277 230963', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2010, 'Kerry London Ltd', 'Kerry London Ltd', '', '', '', '', '', '01923 211290', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2011, 'King & Fowler Ltd', 'King & Fowler', 'Brunswick Business Park', 'Harrison Way', 'Liverpool', '', 'L3 4BG', '0870 142 3109', '0151 709 2622', 'www.aerogistics.com', 'info@aerogistics.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2012, 'Keighley Laboratories Ltd', 'Keighley Laboratories Ltd', 'Croft House', 'South Street', 'Keighley', 'West Yorkshire', 'BD21 1EG', '01535 664211', '01535 680604', 'www.keighleylabs.co.uk', 'info@keighleylabs.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2013, 'K.G. & G (PAT) Limited', 'K.G. & G (PAT) Limited', '"Unit 6, H & H Industries"', 'Archersfield Close', 'Burnt Mills Industrial Estate', '"Basildon, Essex"', 'SS13 1DH', '01268 247039', '01268 247040', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2014, 'Kent Machine Services', 'Kent Machine Services', 'Keneva', 'London Road', '"Addington,West Malling"', '"Maidstone,Kent"', 'ME9 5AN', '01732 844508', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2103, 'Leader Chuck Systems Ltd.', 'Leader Chuck Systems Ltd.', 'Century Park', 'Birmingham', '', '', 'B9 4NZ', '0121-703-6783', '0121-703-6793', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2104, 'Leeds Bronze Engineering', 'Leeds Bronze Engineering', '14 Westland Square', 'Dewsbury Road', 'Leeds', '', 'LS11 5UB', '0113 271 8711', '0113 277 2145', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2105, 'Lindenhouse Software Limited', 'Lindenhouse Software Limited', 'Unit A', 'South Cambridge Business Park', 'Sawston', 'Cambridgeshire', 'CB22 3JH', '01223 492266', '01223 492267', '', 'Gary.Stewart@lindenhouse.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2110, 'Lorrford (Eurospan) Limited', 'Lorrford (Eurospan) Limited', 'Harvey Road', 'Burnt Mills Industrial Estate', 'Basildon', 'Essex', 'SS13 1YY', '01268 723456', '01268 590691', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2111, 'London Aluminium Systems Ltd', 'London Aluminium Systems Ltd', '4 Parklands Parade', 'Bath Road', 'Middlesex', '', 'TW5 9AX', '0800 881 5811', '0208 570 6207', '', 'jat@londonaluminiumsystems.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2116, 'L. W. Distribution Services', 'L. W. Distribution Services', 'Ellis House', 'Felstead Road', 'Benfleet', 'Essex', 'SS7 1BT', '01268 759870', '01268 759630', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2119, 'Lentern (Aircraft) Ltd', 'Lentern (Aircraft) Ltd', 'Main Road', '', 'Hockley', 'Essex', 'SS5 4JJ', '(01702) 202553', '(01702) 206248', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2123, 'Lapped Components', 'Lapped Components', '"Unit 4, Park Fark"', 'Park Road', 'Great Chesterford', 'Essex', 'CB10 1RN', '01799-530655', '01799-531752', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2201, 'MLR Engineering Ltd', 'MLR Engineering Ltd', 'Whites Farm', 'Barleylands Road', 'Basildon', 'Essex', 'SS15 4BG', '01268 288477', '01268 288467', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2203, 'Magnetic Systems Ltd', 'Magnetic Systems Ltd', 'John Street', 'Sheffield', 'South Yorkshire', '', 'S2 4QR', '01142 509581', '01142 509610', 'www.magsystems.co.uk', 'sales@magsystems.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2205, 'Mollarts Power Transmission', 'Mollarts Power Transmission', 'Chadburn House', 'Wakefield Road', 'Netherton', '"Bootle, Merseyside"', 'L30 6TZ', '0151 525 4155', '0151 525 4150', 'www.chadburns.com', 'sales@chadburns.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2206, 'Magellan Aerospace Bournemouth', 'Magellan Aerospace Bournemouth', 'Metal Treatments Division', 'Wallisdown Road', 'Bournemouth', 'Dorset', 'BH11 8QN', '01202 512405', '01202 536250', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2207, 'Metallon', 'Metallon', 'Unit 13 Industrial Estate', 'Old Church Road', 'East Hanningfield', '"Chelmsford, Essex"', 'CM3 8AB', '01245 400955', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2208, 'Mark 1 Hire Limited', 'Mark 1 Hire Limited', '34 Eastwood Road', 'Rayleigh', 'Essex', '', 'SS6 7JQ', '01268 745533', '01268 771811', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2209, 'Myonic Limited', 'Myonic Limited', '10 Warren Yard', 'Wolverton Mill', 'Milton Keynes', '', 'MK12 5NW', '01908 227123', '01908 310427', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2210, 'M2 Digital Limited', 'M2 Digital Limited', 'PO Box 2000', '380 Chester Road', 'Old Trafford', 'Manchester', 'M16 9XX', '0161 877 0222', '0161 877 0220', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2211, 'Metrology & Quality Services Ltd', 'Metrology & Quality Services Ltd', '"Unit 2, Executive Park,"', '"Hatfield Road,"', 'St Albans', 'Hertfordshire', 'AL1 4TA', '01727 847292', '01727 847661', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2212, 'M.A. FORD Eurpoe Ltd', 'M.A. FORD Eurpoe Ltd', '30 John Street', 'Derby', '', '', 'DE1 2LU', '01332 267960', '01332 267969', 'www.maford.com', 'sales@mafordeurope.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2213, 'Mitutoyo (UK) Limited', 'Mitutoyo (UK) Limited', '"Unit 6, Banner Park"', 'Wickmans Drive', 'Coventry', 'West Midlands', 'CV4 9XA', '08707 207037', '08707 207747', 'www.mitutoyo.co.uk', 'subconcoventry@mitutoyo.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2214, 'Maybrey Precision Castings Ltd', 'Maybrey Precision Castings Ltd', 'Unit 16 - 18 Kennet Road', 'Crayford', 'Kent', '', 'DA1 4QN', '01322 315370', '01322 550724', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2217, 'Matrix Machine Tools (Coventry) Ltd', 'Matrix Machine Tools (Coventry) Ltd', 'Unit A2', 'Earlplace Business Park', 'Fletchamstead Highway', 'Coventry', 'CV4 9XL', '02476 718886', '02476 678899', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2218, 'Mills CNC Ltd', 'Mills CNC Ltd', '"Units 2 & 3, Tachbrook Link"', 'Tachbrook Park Drive', 'Leamington Spa', 'Warwickshire', 'CV34 6SN', '01926 736736', '01926 736737', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2219, 'Multispark Erosion Ltd (Luton)', 'Multispark Erosion Ltd (Luton)', '"Camford Way,"', '"Sundon Park,"', 'Luton.', '', 'LU3 3AN', '(01582) 502015', '(01582) 507836', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2220, 'Multispark Erosion Ltd (Poole)', 'Multispark Erosion Ltd (Poole)', '1 Dawkins Business Centre', 'Hamworthy', 'Poole', 'Dorset', 'BH15 4JY', '01202 668989', '01202 668986', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2221, 'ML Fabcuts Ltd', 'ML Fabcuts Ltd', 'Unit 30b', 'Nuralite Industrial Estate', 'Canal Road', '"Higham, Kent"', 'ME3 7JA', '01474 825025', '01474 825125', 'www.mlfabcuts.co.uk', 'sales.mlfabcuts.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2222, 'MAG Maintenance UK Limited', 'MAG Maintenance UK Limited', '"Unit 6, Maybrook Road"', 'Maybrook Business Park', 'Minworth', 'Birmingham', 'B76 1AL', '0121 313 5300', '0121 313 5399', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2223, 'Metalweb Ltd', 'Metalweb Ltd', 'Unit 9', 'Trident Industrial Estate', 'Pindar Road', '"Hoddesdon, Herts"', 'EN11 0WZ', '01992 450300', '01992 450557', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2224, 'MG & A Engineering Limited', 'MG & A Engineering Limited', '"Unit 6, Swinborne Court"', 'Burnt Mills Industrial Estate', 'Basildon', 'Essex', 'SS13 1QA', '01268 590222', '01268 590666', '', 'mgaengineering@yahoo.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2225, 'Macro Developments Limited', 'Macro Developments Limited', '2 Limberline Estate', 'Hilsea', 'Portmouth', '', 'PO3 5HJ', '023 926 96615', '023 926 73651', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2226, 'Machine Tool Supplies Ltd', 'Machine Tool Supplies Ltd', 'Pedder House', '302-304 Chorley Old Road', 'Bolton', 'Lancashire', 'BL1 4JU', '01204 840111', '01204 844407', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2227, 'Metalfast', 'Metalfast', 'Unit 11', 'Cirrus Court', 'Huntingdon', '', 'PF29 7DR', '0844 2570337', '0844 2570336', '', 'lauren@metalfast.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2228, 'Masteel Uk Limited', 'Masteel Uk Limited', 'The Cedars', 'Coton Road', 'Nether Whitacre', '"Coleshill , Birmingham"', 'B46 2HH', '01675437733', '01675437734', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2230, 'Micro-Precision Limited', 'Micro-Precision Limited', 'Duxons Turn', 'Hemel Hempstead', 'Herts', '', 'HP2 4SB', '01442 241027', '01442 268074', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2231, 'Moss Express', 'Moss Express', 'Unit D', 'Eastway Industrial Estate', 'Witham', 'Essex', 'CM8 3YQ', '01376 505390', '01376 511432', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2232, 'MRS Scientific Ltd', 'MRS Scientific Ltd', 'Unit 1', 'Brocks Business Park', '"Hodgson Way,Wickford"', 'Essex', 'SS11 8YF', '01268 730777', '01268 560241', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2235, 'Macfarlane Group UK Ltd', 'Macfarlane Group UK Ltd', '"Unit 5,  Delta Park"', 'Millmarsh Lane', 'Enfield', 'Middlesex', 'EN3 7QJ', '0870 8500116', '0870 8500117', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2237, 'M.J Allen', 'M.J Allen', 'Hilton Road', 'Cobbs Wood Industrial Estate', 'Ashford', 'Kent', 'TN23 1EW', '01233 631554', '', '', 'sales@mjallen.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2239, 'Micron Sales & Services Limited', 'Micron Sales & Services Limited', '114 Elsa Road', '', 'Welling', 'Kent', 'DA16 1JT', '0208 298 0766', '0208 298 0767', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2241, 'Mercury Tool & Gauge Ltd', 'Mercury Tool & Gauge Ltd', '221 Torrington Avenue', 'Coventry', '', '', 'CV4 9HN', '02476 694030', '02476 694070', 'www.mercury-gauge.com', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2242, 'Mansfield Anodisers Ltd', 'Mansfield Anodisers Ltd', '46 Hermitage Way', 'Mansfield', '', '', 'NG18 5ES', '01623 627700', '01623 628800', 'www.mansfield-anodisers.co.uk', 'rob.carlisle@mansfield-anodisers.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2243, 'Multispline Ltd', 'Multispline Ltd', 'Unit 66', 'Station Road', 'Coleshill', 'Birmingham', 'B46 1JT', '(01675) 462253', '(01675) 463485', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2244, 'Midlands Tool Supplies', 'Midlands Tool Supplies', 'Unit 19A', 'Marlow Road Industrial Estate', 'Leicester', '', 'LE3 2BQ', '0116 2896092', '', '', 'mail@midlandstoolsupplies.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2246, 'Mettis Aerospace Limited', 'Mettis Aerospace Limited', 'Windsor Road', 'Redditch', 'Worcestershire', '', 'B97 6EF', '01527 406400', '01527 406401', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2248, 'M & M Asset Maintenance Ltd', 'M & M Asset Maintenance Ltd', 'Court Langley House', 'Bates Road', 'Maldon', 'Essex', 'CM9 5FA', '08443 358417', '08443 358425', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2251, 'Mulhouse Limited', 'Mulhouse Limited', 'Unit 36', 'Nobel Square', 'Burnt Mills Industrial Estate', '"Basildon, Essex"', 'SS13 1LT', '01268 726222', '01268 590424', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2252, 'Marposs Ltd', 'Marposs Ltd', 'Leofric Business Park', 'Progress Way', 'Coventry', '', 'CV3 2TJ', '0247 6636688', '0247 6636622', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2302, 'Nine Telecom Ltd', 'Nine Telecom Ltd', 'Marlbridge House', 'Enterprise Way', 'Edenbridge', 'Kent', 'TN8 6HF', '0800 9700 299', '0800 970 3999', 'www.ninetelecom.co.uk', 'info@ninetelecom.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2303, 'N.C.M.T.', 'N.C.M.T.', 'Ferry Works', 'Summer Road', 'Thames Ditton', 'Surrey', 'KT7 0QQ', '0208 398 4277', '0208 398 3631', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2304, 'N. J. Metrology', 'N. J. Metrology', 'P.O. Box 413', 'Bedford', 'Bedfordshire', '', 'MK41 8WE', '07831 207506', '01234 266474', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2305, 'Niagara (Now Acenta Steel)', 'Niagara (Now Acenta Steel)', 'Paynes Lane', 'Rugby', '', '', 'CV21 2UW', '01788 542191', '01788 535352', 'www.niag.com', 'sales@rug.niag.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2306, 'Namesco Limited', 'Namesco Limited', 'Acton House', 'Perdiswell Park', 'Worcester', '', 'WR3 7GD', '0870 120 8888', '0870 120 8008', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2308, 'Newall Measurement Systems', 'Newall Measurement Systems', 'Technology Gateway', 'Cornwall Road', 'South Wigston', 'Leicester', 'LE18 4XH', '0116 264 2730', '0116 264 2731', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2312, 'Norris Eng (Hobs & Cutters) Ltd.', 'Norris Eng (Hobs & Cutters) Ltd.', 'Challenge Road', 'Ashford', 'Middlesex', '', 'TW15 1BF', '01784 257999', '01784 257901', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2313, 'NordairNiche', 'NordairNiche', '"Unit 4, Chilford Court"', 'Rayne Road', 'Braintree', 'Essex', 'CM7 2QS', '(01376) 332200', '(01376) 332201', '', 'sales@nordairniche.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2319, 'Nu-Pro Ltd', 'Nu-Pro Ltd', 'Eagle Works', 'London Road', '"Thrupp, Stroud"', 'Gloucester', 'GL5 2BA', '01453 883344', '01453 731597', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2322, 'Nema Limited', 'Nema Limited', 'Lomax Street', 'Rochdale', 'Lancashire', '', 'OL12 0HD', '(01706) 640442', '(01706) 640444', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2324, 'Nylonic Engineering Co Ltd', 'Nylonic Engineering Co Ltd', 'Woodcock Hill Trading Estate', 'Harefield Road', 'Rickmansworth', 'Hertfordshire', 'WD3 1PN', '01923 778111', '01923 779250', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2327, 'NGK Berylco U.K. Ltd.', 'NGK Berylco U.K. Ltd.', 'Houston Park', 'Montford Street', 'Salford', '', 'M50 2RP', '0161 745 7162', '0161 745 7520', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2401, 'P D J Vibro Limited', 'P D J Vibro Limited', '46 Barton Road', 'Bletchley', 'Buckinghamshire', '', 'MK2 3BB', '01908 648757', '01908 648 766', '', 'info@vibratryfinishing.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `supplier` (`pkSupplierID`, `SupplierName`, `Alias`, `Address1`, `Address2`, `Address3`, `Address4`, `Postcode`, `Phone`, `Facsimile`, `Website`, `Email`, `MinOrderCharge`, `PaymentTerms`, `MaterialSupplier`, `TreatmentSupplier`, `CalibrationSupplier`, `ToolingSupplier`, `MiscSupplier`, `Deleted`, `fkUserID`, `StdDeliveryDays`, `Day1Open`, `Day1Close`, `Day2Open`, `Day2Close`, `Day3Open`, `Day3Close`, `Day4Open`, `Day4Close`, `Day5Open`, `Day5Close`, `Day6Open`, `Day6Close`, `Day7Open`, `Day7Close`, `QuestionsFilename`, `SQCompletedBy`, `Type1`, `Type2`, `Type3`, `Type4`, `Type5`) VALUES
(2402, 'Rodwell-Powell Ltd           ', 'Rodwell-Powell Ltd           ', '"Chester Hall Lane,"', '', 'Basildon', 'Essex', 'SS14 3DQ', '01268 286641', '01268 286644', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2403, 'Professional Welding Services Limited', 'Professional Welding Services Limited', '80-82 Cobham Road', 'Ferndown Industrial Estate', 'Wimbourne', 'Dorset', 'BH21 7RW', '01202 895080', '01202 861463', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2408, 'Pratt Burnerd International (Crawfords)', 'Pratt Burnerd International (Crawfords)', 'Park Works', 'Lister Lane', 'Halifax', 'West Yorkshire', 'HX1 5JH', '01422 366371', '01422 359379', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2409, 'Potterton Myson Service (Boiler)', 'Potterton Myson Service (Boiler)', '"Service Operations, Brook House,"', 'Coventry Road', 'Warwick', 'Warwickshire', 'CV34 4LL', '(0870) 6096 096', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2411, 'A.T. Poeton (Cardiff) Limited', 'A.T. Poeton (Cardiff) Limited', 'Penarth Road', 'Cardiff', 'South Wales', '', 'CF1 8UL', '02920 388182', '02920 388185', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2413, 'Premier Deep Hole Drilling Ltd', 'Premier Deep Hole Drilling Ltd', 'Wellington Road', 'London Colney', 'St. Albans', 'Herts', 'AL2 1EY', '01727 825031', '01727 826819', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2414, 'Precision Laser Processing Ltd', 'Precision Laser Processing Ltd', 'Butlers Leap Industrial Estate', 'Rugby', 'Warwickshire', '', 'CV21 3RQ', '01788 546004', '01788 546005', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2416, 'Prosaw Limited', 'Prosaw Limited', 'Telford Way', 'Kettering', 'Northants', '', 'NN16 8UN', '01536 410999', '01536 410080', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2417, 'Parker Hannifin Ltd', 'Parker Hannifin Ltd', 'Hydraulics Group, Hydraulics Controls Div.', 'Tachbrook Park Drive', 'Tachbrook Park', 'Warwick', 'CV34 6TU', '01926 833700', '01926 889172', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2418, 'Promotional Ideas', 'Promotional Ideas', '"West View Lodge, Main Road"', 'Rettendon Common', 'Chelmsford', 'Essex', 'CM3 8DJ', '01245 400303', '01245 400176', 'www.promationalideas.co.uk', 'pideas@aol.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2419, 'Planit Software Limited (Edgecam UK)', 'Planit Software Limited (Edgecam UK)', '45 Boulton Road', 'Reading', 'Berkshire', '', 'RG2 0NH', '0118 975 6084', '0118 975 6844', 'www.edgecam.com', 'cashwin@edgecam.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2420, 'Prospects College (Thames Gateway) Ltd', 'Prospects College', '"Prospects House,"', 'Crompton Close', 'Basildon', 'Essex', 'SS14 3AY', '01268 662444', '01268 522262', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2421, 'Perfect Bore Limited', 'Perfect Bore Limited', '"Unit 3, Hunstrete House, Sterling Park"', 'East Portway Industrial Estate', 'Andover', 'Hampshire', 'SP10 3TZ', '(01264) 360800', '(01264) 360809', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2422, 'Puckle Supplies', 'Puckle Supplies', 'Cooting Road', 'Aylesham Industrial Estate', 'Aylesham', '"Canterbury, Kent"', 'CT3 3EP', '01304 842843', '01304 842560', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2423, 'Protex Fasteners Ltd', 'Protex Fasteners Ltd', 'Arrow Road', 'Redditch', 'Worcestershire', '', 'B98 8PA', '01527 63231', '01527 66770', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2424, 'Protool Ltd', 'Protool Ltd', 'South Ash Manor', 'South Ash Road', 'Nr Sevenoaks', 'Kent', 'TN15 7EN', '01474 876964', '01474 876963', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2425, 'Pembroke Engineering', 'Pembroke Engineering', '34 Noble Square', 'Burnt Mills Industrial Estate', 'Basildon', 'Essex', 'SS13 1LT', '01268 726861', '01268 726879', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2427, 'Pirtex Basildon', 'Pirtex Basildon', '"Unit R, The Enterprise Centre,"', 'Paycocke Road', 'Basildon', 'Essex', 'SS14 3DY', '01268 522633', '01268 522644', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2428, 'Parkside Steel', 'Parkside Steel', 'Unit 6', 'Waterways Business Centre', 'Navigation Drive', '"Enfield, Middlesex"', 'EN3 6JJ', '01992 703500', '01992 703510', 'www.parksidesteel.uk.com', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2429, 'Planit Software Limited (Javelin)', 'Planit Software Limited (Javelin)', '8 Warren Park Way', 'Enderby', 'Leicester', '', 'LE19 4SA', '0116 272 5700', '0116 272 5725', 'www.javelin.planit.com', 'keithbreadmore@planit.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2430, 'Par Group Ltd', 'Par Group Ltd', 'Warre Street', 'Ashton Under Lyne', 'Lancashire', '', 'OL6 8NW', '01924 444529', '0161 3432088', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2431, 'Project Metrology Ltd', 'Project Metrology Ltd', 'Unit 1, Sir Frank Whittle Business Centre', '"Great Central Way,  Butlers Leap"', 'RUGBY', '', 'CV21 3XH', '01788 576444', '01788 576333', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2432, 'Poeten (Gloucester) Ltd', 'Poeten (Gloucester) Ltd', 'Eastern Avenue', 'Gloucester', 'Gloucestershire', '', 'GL4 3DN', '(01452) 300500', '(01452) 500400', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2433, 'PBR Abrasives Ltd', 'PBR Abrasives Ltd', '8/10 Wolverhampton Street', 'Willenhall', 'West Midlands', '', 'WV13 2NF', '01902 368624', '01902 634635', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2436, 'Pattonair International Limited', 'Pattonair International Limited', 'Kingswey Business Park', 'Forsyth Road', '"Sheerwater, Woking"', 'Surrey', 'GU21 5SA', '01483 774600', '01483 774619', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2437, 'Plastic Coatings Ltd', 'Plastic Coatings Ltd', 'Southern Division', 'Woodbridge Meadows', 'Guildford', 'Surrey', 'GU1 1BG', '(01483) 531155', '(01483) 533534', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2438, 'Praxair Surface Technologies Ltd', 'Praxair Surface Technologies Ltd', 'Drakes Way', '', 'Swindon', 'Wiltshire', 'SN3 3HX', '(01793) 512555', '(01793) 410289', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2440, 'Praxair Surface Technologies Ltd', 'Praxair Surface Technologies Ltd', 'Oldmixon Crescent', 'Oldmixon', 'Weston-super-Mare', 'Somerset', 'BS24 9AX', '(01934) 411300', '(01934) 411301', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2441, 'Pechiney Aviatube Ltd', 'Pechiney Aviatube Ltd', 'Lillyhall', '', 'Workington', 'Cumbria', 'CA14 4JY', '01900 322616', '01900 322598', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2501, 'Quality Control Technology Ltd', 'Quality Control Technology Ltd', '"Unit 8, Gainsborough Close"', 'Long Eaton', 'Nottingham', '', 'NG10 1PX', '0115 946 9111', '0115 946 9222', 'www.q-c-t.demon.co.uk', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2601, 'Rolled Alloys (DO NOT USE)', 'Rolled Alloys (DO NOT USE)', '"Unit 5, Priory Industrial Park"', 'Airspeed Road', 'Christchurch', 'Dorset', 'BH23 4HD', '01425 280000', '01425 280028', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2602, 'Regal Precision Engineers Ltd', 'Regal Precision Engineers Ltd', 'Walton St Works', 'Walton Street', 'Colne', 'Lancashire', 'BB8 0EL', '01282 864347', '01282 860209', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2603, 'Royalwolf Trading (UK) Ltd', 'Royalwolf Trading (UK) Ltd', 'Electron House', 'Tilbury Freeport', 'Essex', '', 'RM18 7HL', '01375 845001', '01375 845002', 'www.royalwolf.co.uk', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2604, 'Redco International Tools Ltd', 'Redco International Tools Ltd', '29 Brindley Road', 'Bayton Road Industrial Estate', '"Exhall, Nr Coventry"', 'Warwickshire', 'CV7 9EP', '01527 510474', '01527 510432', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2605, 'Ramp Industries Limited', 'Ramp Industries Limited', '1 & 3 Garrett Road', 'Lynx Trading Estate', 'Yeovil', 'Somerset', 'BA20 2TJ', '01935 427290', '01935 420753', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2606, 'Rapid Heat and Building Equipment Ltd', 'Rapid Heat and Building Equipment Ltd', '423 Beacontree Avenue', 'Dagenham', 'Essex', '', 'RM8 3UH', '', '0208 598 4027', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2609, 'Rayvac Refrigeration Services', 'Rayvac Refrigeration Services', 'East Thurrock Road', 'Grays', 'Essex', '', 'RM17 6SP', '01375 377055', '01375 390113', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2610, 'Robert Pringle Engineers Limited', 'Robert Pringle Engineers Limited', 'Network House', 'Perry Road', 'Harlow', 'Essex', 'CM18 7ND', '01279 418300', '01279 418100', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2613, 'Reliable Fire Protection Services', 'Reliable Fire Protection Services', '56 Glendale Gardens', '', 'Leigh on Sea', 'Essex', 'SS9 2AS', '01702 715226', '01702 475122', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2614, 'Renishaw Plc', 'Renishaw Plc', 'New Mills', 'Wotton-under-Edge', 'Gloucestershire', '', 'GL12 8JR', '01453 524524', '01453 524301', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2615, 'Riverside Precision Machining', 'Riverside Precision Machining', 'Riverside House', '16 Gramere Avenue', 'Hullbridge', 'Essex', 'SS5 6LF', '07516 480096', '01702 232384', '', 'riverside@talk21.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2616, 'Rajapack', 'Rajapack', '"Unit 1, Marston Gate"', 'Ridgmont', 'Bedford', 'Bedfordshire', 'MK43 0YL', '0800 542 4428', '0800 542 4429', 'rajapack.co.uk', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2618, 'Rawley J.H. (Plant Hire) Ltd', 'Rawley J.H. (Plant Hire) Ltd', 'Durham Road', 'Laindon', 'Basildon', 'Essex', 'SS15 6PJ', '01268 542121', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2619, 'Righton Limited', 'Righton Limited', '"Unit B, Sands Industrial Estate"', 'Hillbottom Road', 'High Wycombe', 'Bucks', 'HP12 4HS', '01494 443710', '01494 535862', 'www.righton.co.uk', 'wycoombesales@righton.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2620, 'Rolled Alloys', 'Rolled Alloys', 'Walker Industrial Park', 'Guide', 'Blackburn', 'Lancashire', 'BB1 2QE', '01254 582999', '01254 582666', 'www.rolledalloys.com', 'blackburn@rolledalloys.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2621, 'Rose Plastic UK Limited', 'Rose Plastic UK Limited', '"Unit 4, Bessemer Way"', 'Bessemer Business Park', 'Rotherham', '', 'S60 1EN', '01709 721794', '01709 724980', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2701, 'Surface Technology - Ultraseal Ltd', 'Surface Technology - Ultraseal Ltd', 'Arthur House', 'Petersfield Road', 'Slough', 'Berkshire', 'SL2 5DU', '01753 526877', '01753 532229', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2702, 'SGS Carbide Tool (UK) Ltd', 'SGS Carbide Tool (UK) Ltd', 'Unit 1 The Metro Centre', 'Toutley Road', 'Wokingham', 'Berks', 'RG41 1QW', '0118 979 5200', '0118 979 5295', 'www.sgstool.com', 'sales@sgstool.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2703, 'Smiths Metal Centres', 'Smiths Metal Centres', '7 Argyll Road', 'Dukes Park Industrial Estate', '"East Springfield, Chelmsford"', 'Essex', 'CM2 6PY', '01245 466664', '01245 465916', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2704, 'Slingsby H C  plc', 'Slingsby H C  plc', 'Preston Street', '', 'Bradford', 'West Yorkshire', 'BD7 1JF', '(01274) 721591', '(01274) 723044', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2705, 'Smiths Aerospace Ltd', 'Smiths Aerospace Ltd', 'Customer Services Division', 'Bishops Cleeve', 'Chelenham', 'Gloucestershire', 'GL52 8SF', '01242-669423', '01242669178', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2706, 'Robert Stuart PLC', 'Robert Stuart PLC', '10/11 Edinburgh Way', '', 'Harlow', 'Essex', 'CM20 2DH', '01279 442931', '01279 626063', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2707, 'Stainless Equipment Company Ltd', 'Stainless Equipment Company Ltd', 'Alma Road', 'Enfield', 'Middlesex', '', 'EN3 7BB', '0208 805 0884', '0208 804 8167', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2708, 'Shur-Lok International S.A.', 'Shur-Lok International S.A.', 'Parc Industriel', 'rue des Biolleux', 'B-4800 PETIT-RECHAIN', 'Belgium', 'B-4800', '0032-87320776', '0032-87320712', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2709, 'Satair Hardware Group', 'Satair Hardware Group', 'Shoreham Airport', 'Shoreham by Sea', 'West Sussex', '', 'BN42 5FN', '01273 464046', '01273 464577', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2710, 'Super Alloys International Limited', 'Super Alloys International Limited', 'Number 1 Java Park', 'Bradbourne Drive', 'Tilbrook', 'Milton Keynes', 'MK7 8AT', '01908 275700', '01908 275701', '', 'sales@superalloys.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2711, 'Silverfield Limited', 'Silverfield Limited', 'Aldon Road', 'x', 'Poulton-le-Fylde', 'Lancashire', 'FY6 8JL', '01253 891733', '01253 894404', 'x', 'x', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2712, 'Sumitomo Electric Hardmetal Ltd', 'Sumitomo Electric Hardmetal Ltd', '"54 Summerleys Road,"', '"Princes Risborough,"', 'Aylesbury', 'Buckinghamshire', 'HP27 9PW', '(01844) 342081', '(01844) 342415', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2713, 'Sheen Spark Ltd', 'Sheen Spark Ltd', 'Unit 1', 'Ewhurst Avenue', 'Selly Oak', 'Birmingham', 'B29 6EY', '0121 472 6241', '0121 472 5396', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2714, 'Stanworth Engineers Limited', 'Stanworth Engineers Limited', 'Brown Street', 'Burnley', 'Lancashire', 'x', 'BB11 1PN', '01282 421427', '01282 458318', 'x', 'x', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2715, 'Servis Heat Treatment Co Ltd', 'Servis Heat Treatment Co Ltd', '258B Ipswich Road', 'Trading Estate', 'Slough', 'x', 'SL1 4EP', '01753 521823', '01753 531094', 'x', 'x', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2716, 'Skymetals (Do not use after 30/04/05)', 'Skymetals', 'Unit 3 Trillennium', 'Highway Point', 'Coleshill', 'Birmingham', 'B46 1JU', '01675 430140', '01675 430346', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2717, 'SKF (U.K.) Ltd - Aerospace Division', 'SKF (U.K.) Ltd - Aerospace Division', '108 Strode Road', 'Clevedon', 'North Somerset', '', 'BS21 6QQ', '01275 876021', '01275 878480', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2718, 'S.G. Equipment Ltd', 'S.G. Equipment Ltd', 'Delta Point', 'Groby Road', 'Audenshaw', 'Manchester', 'M34 5UP', '0161 371 6130', '0161 371 7587', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2719, 'Saywell International', 'Saywell International', 'Aviation House', '"Woods Way, Goring By Sea"', 'Worthing', 'West Sussex', 'BN12 4QY', '(01903) 705700', '(01903) 705701', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2720, 'Stone Foundries Ltd', 'Stone Foundries Ltd', 'Woolwich Rd', 'London', 'x', 'x', 'SE7 8SL', '(0208) 853 4648', '(0208) 305 1934', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2721, 'Steelmans Broaches UK Ltd', 'Steelmans Broaches UK Ltd', '10 The Old Nurseries', 'Ravenstone', 'Coalville', 'Leicestershire', 'LE67 2LA', '01530 830593', '01530 838958', 'www.steelmans.co.uk', 'sales@steelmans.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2722, 'Smiths Metal Centres Ltd (Wimborne)', 'Smiths Metal Centres Ltd (Wimborne)', 'Ferndown Metal Centre', '"Units 8 & 9, Cedar Trade Park"', 'Ferndown Industrial Estate', '"Cobham Road, Wimborne"', 'BH21 7PE', '01202 893755', '01202 893712', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2723, 'Shack Tools Limited', 'Shack Tools Limited', 'Regent House', 'Church Street', 'Beaumaris', 'Anglesey', 'LL58 8TR', '0845 009 4082', '01691 670584', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2724, 'The Sunlight Service Group Ltd', 'The Sunlight Service Group Ltd', '"Unit 1, Beavers Centre"', 'Selina\'s Lane', 'Dagenham', 'Essex', 'RM8 1QH', '0208 984 7532', '0208 984 7977', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2726, 'Spline Gauges', 'Spline Gauges', 'Piccadilly', 'Tamworth', 'Staffordshire', 'x', 'B78 2ER', '01827 872771', '01827 874128', 'splinegauges.co.uk', 'Keith.Emery@splinegauges.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2727, 'Saltcoate Windows Ltd', 'Saltcoate Windows Ltd', 'Unit 28 Cutlers Road', 'Saltcoate Industrial Estate', 'South Woodham Ferrers', '"Chelmsford, Essex"', 'CM3 5XJ', '01245 324000', '01245 324222', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2729, 'Silgo Lubricants Ltd', 'Silgo Lubricants Ltd', 'Purfleet Industrial Park', '"Units 20, 22, 24 Juliet Way"', 'Thurrock Commerical Centre', '"South Ockendon, Essex"', 'RM15 4YG', '01708 865665', '01708 868996', 'http://www.silgo.co.uk', 'russell@silgo.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2730, 'Shamban Aerospace Sealing Systems', 'Shamban Aerospace Sealing Systems', 'Ryder Close', 'Cadley Hill Industrial Estate', 'Swadlincote', 'Derbyshire', 'DE11 9EU', '01283 200 000', '01283 550 903', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2731, 'Staples Direct', 'Staples Direct', 'P.O. Box 8422', 'Birmingham', '', '', 'B19 3RT', '0800 151414', '0800 141516', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2732, 'Signs and Labels Limited', 'Signs and Labels Limited', 'Douglas Bruce House', '"Corrie Way, Bredbury Industrial Park"', 'Stockport', 'Cheshire', 'SK6 2RR', '(0161) 494 6125', '(0161) 430 8514', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2733, 'SPS Technologies', 'SPS Technologies', '191 Barkby Road', 'Leicester', '', '', 'LE4 9HX', '0116 276 8261', '0116 274 0243', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2734, 'SAS Couriers (Essex) Ltd', 'SAS Couriers (Essex) Ltd', '"""The Office"""', '5 Gilbert Road', 'Chafford Hundred', 'Essex', 'RM16 6NN', '0845 555 0 444', '0845 555 0 445', 'www.sascouriers.com', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2736, 'South West Metal Finishing', 'South West Metal Finishing', 'Alphinbrook Road', 'Marsh Barton', 'Exeter', 'Devon', 'EX2 8TJ', '01392 258234', '01392 421538', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2737, 'Sprint Parcels', 'Sprint Parcels', 'Overdale', 'Lee Chapel Lane', 'Langdon Hills', 'Essex', 'SS16 5NX', '07788 484444', '01268 410735', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2739, 'Saras Process Ltd', 'Saras Process Ltd', 'Duchess Street Industrial Estate', 'Duchess Street', 'Shaw', 'Lancashire', 'OL2 7UT', '01706 845960', '01706 882403', '', 'sales@sarasprocess.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2740, 'Sifco Applied Surface Concepts Ltd', 'Sifco Applied Surface Concepts Ltd', 'Unit 12 - 14 Aston Fields Trading Estate', 'Aston Road', 'Bromsgrove', 'Worcestershire', 'B60 3EX', '01527 557740', '01527 832856', 'www.sifcoasc.com', 'plating@sifco.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2742, 'Swift Office Cleaning Services Ltd', 'Swift Office Cleaning Services Ltd', 'Swift House', 'Riverway', 'Harlow', 'Essex', 'CM20 2DW', '01279 413641', '01279 410247', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2744, 'Special Tooling Ltd', 'Special Tooling Ltd', '25A Orgreave Crescent', 'Dore House Industrial Estate', 'Handsworth', 'Sheffield', 'S13 9NQ', '01142 691169', '01142 699069', 'www.specialtoolingltd.com', 'info@specialtoolingltd.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2745, 'Safety Kleen UK Ltd', 'Safety Kleen UK Ltd', 'Unit B6 Redlands', 'Coulsdon', 'Surrey', '', 'CR5 2HT', '0208 763 4360', '0208 668 9267', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2746, 'Sil-Mid Ltd', 'Sil-Mid Ltd', '"2 Roman Park, Roman Way"', 'Coleshill', 'West Midlands', '', 'B46 1HG', '01675 432850', '01675 432870', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2747, 'Shelley Engineering (Redhill) Ltd', 'Shelley Engineering (Redhill) Ltd', '"Units 31 33, Grace Business Centre"', '23 Willow Lane', 'Mitcham', 'Surrey', 'CR4 4TU', '0208 685 0302', '0208 687 0572', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2748, 'Smart Calibrations', 'Smart Calibrations', '110 Manor Estate', '', 'Wolston', 'Warwickshire', 'CV8 3GX', '07736726616', '024 765 40565', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2749, 'Strand Furniture', 'Strand Furniture', 'C/O Alpha Garden Centre', '238 London Road', 'Wickford', '', 'SS12 0JX', '01268 572164', '01268 572164', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2751, 'Styrotech Ltd', 'Styrotech Ltd', '"Unit 33, Phoenix Trading Estate"', 'Charles Street', 'West Bromwich', '', 'B70 0AY', '0121 520 6343', '0121 520 4851', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2752, 'Surelight Electrical Contractors Ltd', 'Surelight Electrical Contractors Ltd', '2 Thistle Close', 'Noak Bridge', 'Basildon', 'Essex', 'SS15 5GX', '01268 273134', '', '', 'surelightelectrical@hotmail.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2754, 'Sonatest Limited', 'Sonatest Limited', 'Dickens Road', 'Wolverton', 'Milton Keynes', '', 'MK12 5QQ', '01908 316345', '01908 321323', 'www.sonatest.com', 'sales@sonatest.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2756, 'Sandvik Coromant UK Ltd', 'Sandvik Coromant UK Ltd', 'Manor Way', 'Halesowen', 'West Midlands', '', 'B62 8QZ', '0121 5045000', '0121 5045090', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2757, 'Southfields Engineering Co', 'Southfields Engineering Co', 'Unit 6 Guideprime Business Centre', 'Southend Road', 'Billericay', 'Essex', 'CM11 2PZ', '01277 655001', '01277 655002', 'www.southfields-eng.co.uk', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2801, 'Turner Electronics Limited', 'Turner Electronics Limited', 'Turner House', 'Roke Close', 'Kenley', 'Surrey', 'CR2 5NL', '0208 668 0821', '0208 668 2782', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2802, 'Thornton Precision Components Ltd', 'Thornton Precision Components Ltd', 'Beulah Road', 'Sheffield', '', '', 'S6 2AN', '0114 285 5881', '0114 233 6978', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2803, 'T F C  Plc', 'T F C  Plc', 'Hale House', 'Ghyll Industrial Estate', 'Heathfield', 'East Sussex', 'TN21 8AW', '01435 866011', '01435 866620', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2804, 'Tyco Fire & Integrated Solutions (UK) Ltd', 'Tyco Fire & Integrated Solutions', 'Tyco House', '57-63 Farnham Road', 'Slough', '', 'SL1 3TN', '01753 574111', '01753 824226', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2805, 'Tooling Enterprises', 'Tooling Enterprises', '104 Swan Lane', 'Wickford', 'Essex', '', 'SS11 7DF', '01268 763751', '01268 560382', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2806, 'TTI Group Ltd', 'TTI Group Ltd', 'Blackhorse Road', 'Letchworth Garden City', 'Hertfordshire', '', 'SG6 1HD', '01462 472100', '01462 480643', 'www.ttigroup.co.uk', 'letchworth@ttigroup.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2807, 'Timet UK Limited', 'Timet UK Limited', 'Service Centre Sales and Marketing', 'P O Box 704', 'Witton', 'Birmingham', 'B6 7UR', '0121 332 5253', '0121 356 5413', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2808, 'Taylor Forgings', 'Taylor Forgings', 'Canal Street Works', 'Effingham Road', 'Sheffield', '', 'S4 7ZB', '0114 273 1893', '0114 272 8440', 'www.taylorforgings.com', 'info@taylorforgings.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2809, 'Turner Tools Limited', 'Turner Tools Limited', '"Unit 15, Armstrong Close"', 'Chruchfield Industrial Estate', 'St Leonards-on-Sea', 'East Sussex', 'TN38 9ST', '01424 853055', '01424 851085', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2810, 'T & K Precision Ltd', 'T & K Precision Ltd', '10 Gatelodge Close', 'Round Spinney', 'Northampton', '', 'NN3 8RJ', '01604 493101', '01604 493208', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2811, 'Trelleborg Sealing Solutions UK Ltd', 'Trelleborg Sealing Solutions UK Ltd', 'Cadley Hill', 'Swadlincote', 'Derbyshire', '', 'DE11 9EU', '01283 222145', '01283  222140', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2812, 'Tesa Technology UK', 'Tesa Technology UK', 'Metrology House', 'Halesfield 13', 'Telford', 'Shropshire', 'TF7 4PL', '01952 681349', '01952 681391', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2813, 'Tecvac Limited', 'Tecvac Limited', 'Buckingway Business Park', 'Swavesey', 'Cambridge', '', 'CB4 5UG', '01954 233700', '01954 233733', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2817, 'Thame Engineering Co Ltd', 'Thame Engineering Co Ltd', '"Field End, Thame Road"', 'Long Crendon', 'Aylesbury', 'Buckinghamshire', 'HP18 9EJ', '01844 208050', '01844 201699', 'www.thame-eng.com', 'sales@thame-eng', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2818, 'TCS Group', 'TCS Group', 'Unit 5', 'Cranes Close', 'Basildon', 'Essex', 'SS14 3JB', '(01268) 293040', '(01268) 533338', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2819, 'Towerfield Plating Limited', 'Towerfield Plating Limited', 'Towerfield Industrial Estate', 'Shoeburyness', 'Essex', '', 'SS3 9QP', '01702 294161', '01702 295743', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2820, 'ThyssenKrupp Aerospace UK Limited', 'ThyssenKrupp Aerospace UK Limited', 'Aviation House', 'Garamonde Drive', 'Wymbush', 'Milton Keynes', 'MK8 8DF', '01908 556527', '01908 556501', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2821, 'Tornos Technologies UK Ltd', 'Tornos Technologies UK Ltd', '"Tornos House, Garden Road"', 'Whitwick Business Park', 'Coalville', 'Leicestershire', 'LE67 4JQ', '(01530) 513100', '(01530) 814212', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2822, 'ThyssenKrupp Aerospace UK Ltd', 'ThyssenKrupp Aerospace UK Ltd', 'Redfern Road', 'Tyseley', 'Birmingham', 'West Midlands', 'B11 2BH', '0121 3355100', '0121 3355055', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2824, 'Trac Measurement Systems Limited', 'Trac Measurement Systems Limited', 'Millenium Way', 'Pride Park', 'Derby', '', 'DE24 8HZ', '01332 851720', '01332 851739', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2825, '354 (threefivefour)', '354 (threefivefour)', 'Units 18-20 Herongate Trading Estate', 'Paycocke Road', 'Basildon', 'Essex', 'SS14 3EU', '01268 293794', '01268 330734', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2826, 'Tata Steel Uk Limited', 'Tata Steel Uk Limited', 'Tata Steel Metal Centre', 'Farningham Road Station', 'South Darenth', '"Dartford, Kent"', 'DA4 9LD', '01322 227272', '01322 864893', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2827, 'ThyssenKrupp Aerospace', 'ThyssenKrupp Aerospace', 'Bankfield Road', 'Tyldesley', 'Manchester', '', 'M29 8QH', '0161 9112828', '0161 9112899', '', 'paul.best@thyssenkrupp.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2828, 'Tiernay Transtar Metals', 'Tiernay Transtar Metals', '1 Meredews', 'Works Road', 'Letchworth', 'Hertfordshire', 'SG6 1WH', '(01462) 687650', '(01462) 684642', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2829, 'Tangi-Flow Products Ltd', 'Tangi-Flow Products Ltd', 'Automatic House', 'Discovery Way', 'Leofric Business Park', '"Binley, Coventry"', 'CV3 2TD', '024 76421200', '024 76421459', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2830, 'Threeway Precision', 'Threeway Precision', '11 Towerfield Road', 'Shoeburyness', 'Essex', '', 'SS3 9QE', '01702 297816', '01702 292389', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2831, 'Total Suisse Tooling Limited', 'Total Suisse Tooling Limited', '25 Green Road', 'Didcot', 'Oxfordshire', '', 'OX11 8SY', '01235 816623', '01235 812692', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2832, 'Trainease', 'Trainease', '7 Davenants', 'Basildon', 'Essex', '', 'SS13 1QX', '0870 902 0030', '0870 902 0040', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2833, 'Techniswage Limited', 'Techniswage Limited', '3B Summit Crescent Industrial Estate', 'Smethwick', 'Warley', 'West Midlands', 'B66 1BT', '0121 5532364', '0121 5532363', 'www.techniswage.com', 'sales@techniswage.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2901, 'Unit Site Services', 'Unit Site Services', 'Vine House', '27 Laburnum Way', 'Hatfield Perveral', '"Chelmsford, Essex"', 'CM3 2LP', '01245 382590', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2902, 'Universal Instrument Services Ltd', 'Universal Instrument Services Ltd', '"Unit 69, The Whittle Estate"', 'Cambridge Road', 'Whetstone', 'Leicester', 'LE8 6PA', '0116 275 0123', '0116 275 0262', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2904, 'Universal Steels & Aluminium (SW) Ltd', 'Universal Steels & Aluminium', '"Unit A, Cadbury Business Park"', 'Sparkford', 'Yeovil', '', 'BA22 7LH', '01963 441075', '01963 440116', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2905, 'U.C.B. Ferrocast Ltd', 'U.C.B. Ferrocast Ltd', 'Broombank Road', 'Chesterfield Trading Estate', 'Chesterfield', 'Derbyshire', 'S41 9QJ', '01246 269293', '01246 269373', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2906, 'Universal Marking Systems Ltd', 'Universal Marking Systems Ltd', 'Mount Road', 'Feltham', 'Middlesex', '', 'TW13 6AR', '0208 898 4884', '0208 898 9891', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3001, 'VES Precision Ltd', 'VES Precision Ltd', '10 Cropmead Industrial Estate', 'Crewkerne', 'Somerset', '', 'TA18 7HQ', '01460 270630', '01460 270631', '', 'ndring@vesprecision.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3002, 'Viking Direct', 'Viking Direct', 'Internet Account', '', '', '', '', '0844 412 1111', '', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3003, 'VSMPO Tirus Ltd', 'VSMPO Tirus Ltd', '"Unit 12, The io Centre"', 'Nash Road', 'Redditch', '', 'B98 7AS', '01527 514111', '01527 514112', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3004, 'Ventura Grinding Co Ltd', 'Ventura Grinding Co Ltd', 'Unit 18', 'Ventura Place', 'Poole', 'Dorset', 'BH16 5SW', '01202 631023', '01202 631060', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3101, 'WDS Components Ltd', 'WDS Components Ltd', 'Richardshaw Road', 'Grangefield Industrial Estate', 'Pudsey', '"Leeds, West Yorkshire"', 'LS28 6LE', '0845 606 6677', '0845 601 1173', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3102, 'Wilson Machinery International Ltd', 'Wilson Machinery International Ltd', 'Roseberry House', 'Old Church Road', 'East Hannningfield', 'Chelmsford', 'CM3 8BG', '01245 403040', '01245 403028', '', 'sales@wilsonmachinery.demon.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3103, 'Wiseman Threading Tools Ltd', 'Wiseman Threading Tools Ltd', '"Unit 11, Padgets Lane"', 'South Moons Moat', 'Redditch', 'Worcestershire', 'B98 0RA', '01527 520580', '01527 520280', 'www.threadtools.com', 'sales@threadtools.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3104, 'West Yorkshire Steel Company Limited', 'West Yorkshire Steel Company Limited', 'Sandbeck Works', 'Sandbeck Industrial Estate', 'Wetherby', 'Leeds', 'LS22 7DN', '0845 658 1304', '0845 658 1305', 'www.westyorkssteel.com', 'sales@westyorkssteel.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `supplier` (`pkSupplierID`, `SupplierName`, `Alias`, `Address1`, `Address2`, `Address3`, `Address4`, `Postcode`, `Phone`, `Facsimile`, `Website`, `Email`, `MinOrderCharge`, `PaymentTerms`, `MaterialSupplier`, `TreatmentSupplier`, `CalibrationSupplier`, `ToolingSupplier`, `MiscSupplier`, `Deleted`, `fkUserID`, `StdDeliveryDays`, `Day1Open`, `Day1Close`, `Day2Open`, `Day2Close`, `Day3Open`, `Day3Close`, `Day4Open`, `Day4Close`, `Day5Open`, `Day5Close`, `Day6Open`, `Day6Close`, `Day7Open`, `Day7Close`, `QuestionsFilename`, `SQCompletedBy`, `Type1`, `Type2`, `Type3`, `Type4`, `Type5`) VALUES
(3105, 'Wheelabrator Group Ltd', 'Wheelabrator Group Ltd', 'Impact Finishers', '107-109 Whitby Road', 'Slough', 'Berkshire', 'SL1 3DR', '(01753) 215676', '(01753) 215670', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3106, 'Wallwork Heat Treatment (Bury) Limited', 'Wallwork Heat Treatment (Bury) Limited', 'Lord Street', 'Bury', 'Lancashire', '', 'BL9 0RE', '0161 797 9111', '0161 763 1861', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3107, 'W.H. Tildesley Ltd', 'W.H. Tildesley Ltd', 'Bow Street', 'Willenhall', 'West Midlands', '', 'WV13 2AN', '01902 366440', '01902 366216', 'www.whtildesley.com', 'johncoles@whtildesley.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3108, 'Wickford Mould and Tool Ltd', 'Wickford Mould and Tool Ltd', '"Unit 2, Bruce Grove"', 'Wickford', 'Essex', '', 'SS11 8DB', '01268 732148', '01268 764394', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3109, 'Wilsons Ltd', 'Wilsons Ltd', 'Windover Road', 'Huntingdon', 'Cambridgeshire', '', 'PE29 7ED', '(01480) 456421', '(01480) 456425', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3110, 'Waterjet Profilers Ltd', 'Waterjet Profilers Ltd', '"Unit 3, Nobel Square"', 'Burnt Mills Industrial Estate', 'Basildon', 'Essex', 'SS13 1LS', '01268 724227', '01268 725249', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3111, 'Waterjet Profiles (2004) Ltd', 'Waterjet Profiles (2004) Ltd', 'Unit 4A Parkway', 'Junction 38 Business Park', '"Darton, Barnsley"', 'South Yorkshire', 'S75 5QQ', '01226 394040', '01226 387809', 'www.waterjet-profiles.co.uk', 'sales@waterjet-profiles.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3112, '"Wholesale Fittings Ltd.,     "', '"Wholesale Fittings Ltd.,     "', '"Unit 20 Repton Court, Repton Close"', '"Burnt Mills Industrial Estate,"', 'Basildon', 'Essex', 'SS13 1LN', '01268 532558', '01268 532559', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3113, 'WDS Limited', 'WDS Limited', 'Marlco Broach Division', '"Hagden Lane,"', 'Watford', 'Hertfordshire', 'WD18 7DJ', '(01923) 226606', '(01923) 242799', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3114, 'Ward CNC Machinery Ltd', 'Ward CNC Machinery Ltd', 'Albion Works', 'Savile Street', 'Sheffield', '', 'S4 7UD', '0114 276 5411', '0114 270 0786', '', 'sales@wardcnc.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3115, 'William Beckett Plastics Limited', 'William Beckett Plastics Limited', '"Unit A5,"', 'Tinsley Industrial Park', '"Shepcote Way,Sheffield"', 'South Yorkshire', 'S9 1TH', '0114 2434399', '0114 2560196', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3116, 'Winstonmead Plc', 'Winstonmead Plc', 'Alexander House', 'Honywood Road', 'Basildon', 'Essex', 'SS14 3DS', '01268 532533', '01268 532319', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3121, 'Walton Plating', 'Walton Plating', 'Sir Richards Bridge', '118 Ashley Road', 'Walton-on-Thames', 'Surrey', 'KT12 1HN', '01932 221206', '01932 246699', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3124, 'SWS (Stephen Whale Services Ltd)', 'SWS (Stephen Whale Services Ltd)', '"Units 2 & 3,  Coopers Park"', 'Springwood Industrial Estate', 'Braintree', 'Essex', 'CM7 2TN', '(01376) 550330', '(01376) 551721', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3126, 'Wormald Fire Systems (Now Tyco)', 'Wormald Fire Systems (Now Tyco)', '206 Bedford Avenue', 'Slough Trading Estate', 'Slough', 'Berkshire', 'SL1 4RY', '01753 574111', '01753 824226', 'www.wormald.co.uk', 'wfs.slough.uk@tycoint.com', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3127, 'Wesco Aircraft', 'Wesco Aircraft', 'Park Mill Way', 'Clayton West', 'Huddersfield', '', 'HD8 9XJ', '01484 867769', '01484 867711', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3128, 'WNT (UK) Ltd', 'WNT (UK) Ltd', 'Sheffield Airport', 'Business Park', 'Europa Link', 'Sheffield', 'S9 1XU', '0800 073 2073', '0800 073 2074', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3201, 'XDP Express', 'XDP Express', 'Meadows House', 'Ridley Road', 'Basildon', 'Essex', 'SS13 1EG', '01268 729846', '01268 220627', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3301, 'Yes2solutions', 'Yes2solutions', 'Bentalls', 'Pipps Hill Industrial Estate', 'Basildon', 'Essex', 'SS14 3BS', '0845 108 1888', '0845 108 1889', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3302, 'Yeovil Precision Castings Limited', 'Yeovil Precision Castings Limited', 'Buckland Road', 'Penn Mill Trading Estate', 'Yeovil', 'Somerset', 'BA21 5EF', '01935 426271', '01935 427669', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3303, 'Yamazaki Mazak U.K. Limited', 'Yamazaki Mazak U.K. Limited', 'Badgeworth Drive', 'Worcester', '', '', 'WR4 9NF', '01905 755755', '01905 755001', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3304, 'York Metrology Limited', 'York Metrology Limited', '6 Highmeres Road', 'Leicester', 'Leicestershire', '', 'LE4 9LZ', '0116 246 0250', '0116 246 0004', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3401, 'Otto Kampf', 'Otto Kampf', '46-54 Leigh Road', 'Wimbourne', 'Dorset', '', 'BH21 1AQ', '01202 883136', '01202 880352', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3402, 'Oilite Bearings Ltd', 'Oilite Bearings Ltd', '"Elton Park Works,"', '"Hadleigh Road,"', 'Ipswich', '', 'IP2 OAS', '01473 233300', '01473 230424', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3403, 'Orion Alloys Limited', 'Orion Alloys Limited', 'Unit A1', 'Riverway Industrial Estate', 'Riverway', '"Harlow, Essex"', 'CM20 2DP', '01279 434422', '01279 420044', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3406, 'Orchard Materials Ltd', 'Orchard Materials Ltd', '"Unit 7, Brunel Way"', 'Thornbury Industrial Estate', 'Thornbury', 'Bristol', 'BS35 3UR', '(01454) 415222', '(01454) 415333', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3408, 'Ovako Steels Ltd', 'Ovako Steels Ltd', 'Unit 2', 'Britannia Park', 'Trident Drive', 'Wednesbury', 'WS10 7XA', '0121 502 1010', '0121 505 0019', '', '', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3410, 'O-Vee Sping Gauges Ltd', 'O-Vee Sping Gauges Ltd', '105 Sussex Road', 'Watford', 'Herts', '', 'WD24 5HR', '01923 462997', '01923 443115', '', 'admin@ovee.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3501, 'Zincast Foundry Ltd', 'Zincast Foundry Ltd', 'Strawberry Lane', 'Willenhall', 'West Midlands', '', 'WV13 3NG', '01902 606226', '01902 634607', 'www.zincast.co.uk', 'phorton@zincast.co.uk', 0, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3503, 'TestSupplier', 'TestAlias', 'addr1', 'addr2', 'addr3', 'addr4', 'PO5 4CD', '01234666666', '01234666555', 'aaa.com', 'a@a.com', 1, 1, 0, 1, 0, 0, 0, 0, NULL, NULL, '09:00:00', '17:00:00', '09:00:00', '17:00:00', '10:00:00', '17:00:00', '09:00:00', '17:00:00', '09:00:00', '16:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', NULL, NULL, 2, 1, 15, NULL, NULL),
(3504, 'testsupplier 2', 'test2', 'test2 address1', 'test 2 address2', 'test 2 address 3', 'test 2 address 4', 'P05TCDE', '020304050607', '020304050608', 'www.website.com', 'web@site.com', 0, 1, 1, 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `supplieradditionaltime`
--

CREATE TABLE `supplieradditionaltime` (
  `pkSupplierAdditionalID` int(11) NOT NULL,
  `fkSupplierID` int(11) DEFAULT NULL,
  `TimeType` tinyint(4) DEFAULT NULL COMMENT '1= Shutdown, 2 = collection , 3 = other',
  `StartPoint` varchar(9) DEFAULT NULL,
  `EndPoint` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='	';

--
-- Dumping data for table `supplieradditionaltime`
--

INSERT INTO `supplieradditionaltime` (`pkSupplierAdditionalID`, `fkSupplierID`, `TimeType`, `StartPoint`, `EndPoint`) VALUES
(1, 3503, 2, 'Tuesday', 'Tuesday'),
(2, 3503, 1, '1st Aug', '14th Aug'),
(3, 1001, 3, '3rd April', '3rd April');

-- --------------------------------------------------------

--
-- Table structure for table `supplierapproval`
--

CREATE TABLE `supplierapproval` (
  `fkSupplierID` smallint(6) UNSIGNED NOT NULL,
  `fkApprovalID` tinyint(4) UNSIGNED NOT NULL,
  `ExpiryDate` date DEFAULT NULL,
  `AddedByfkUserID` smallint(6) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='between supplier and approval, to record suppliers approval from master approval list';

--
-- Dumping data for table `supplierapproval`
--

INSERT INTO `supplierapproval` (`fkSupplierID`, `fkApprovalID`, `ExpiryDate`, `AddedByfkUserID`) VALUES
(2, 1, '2015-06-07', 1),
(2, 3, '2015-06-08', 1),
(1001, 3, NULL, 2),
(1001, 4, '2016-04-04', 2);

-- --------------------------------------------------------

--
-- Table structure for table `suppliercontact`
--

CREATE TABLE `suppliercontact` (
  `pkContactID` smallint(5) UNSIGNED NOT NULL,
  `fkSupplierID` smallint(5) UNSIGNED NOT NULL,
  `ContactName` varchar(45) DEFAULT NULL,
  `Phone` varchar(13) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Prime` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1 = yes',
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 17/3/16',
  `Deleted` tinyint(4) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suppliercontact`
--

INSERT INTO `suppliercontact` (`pkContactID`, `fkSupplierID`, `ContactName`, `Phone`, `Email`, `Prime`, `AddedByfkUserID`, `Deleted`) VALUES
(1, 1001, 'Cuthbert', NULL, 'cuthbert@aircraftmaterialsuk.com', 1, 4, 0),
(2, 1001, 'Edmond', NULL, 'ed.mond@edmondo.net', 0, 4, 1),
(3, 3504, 'Samuel', NULL, 's.pepys@suppliersrus.co.uk', 1, 4, 0),
(4, 3504, 'Tabitha', NULL, 't.tabb@hotmail.com', 0, 4, 0),
(5, 3503, 'Ted Barchard', NULL, 'tbarchard@btinternet.com', 0, 4, 0),
(6, 3503, 'Big Ted', NULL, 'tedtruncheon@bigstick.net', 1, 4, 0),
(7, 3503, 'Bigger Ted', '0800 88 88', 'a@b.com', 0, 2, 0),
(8, 3501, 'test contact', NULL, 'test.contact@supplier.com', 1, 4, 0),
(9, 3502, 'Testee McTest', NULL, 't.mctest@testicalls.co.uk', 1, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `suppliernadcap`
--

CREATE TABLE `suppliernadcap` (
  `fkSupplierID` smallint(5) UNSIGNED NOT NULL,
  `fkApprovalID` tinyint(3) UNSIGNED NOT NULL,
  `ExpiryDate` date DEFAULT NULL,
  `AddedByfkUserID` smallint(6) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='shows nadcap approvals for any given Supplier';

--
-- Dumping data for table `suppliernadcap`
--

INSERT INTO `suppliernadcap` (`fkSupplierID`, `fkApprovalID`, `ExpiryDate`, `AddedByfkUserID`) VALUES
(1, 9, NULL, NULL),
(2, 4, '2015-06-10', 2),
(2, 9, '2015-06-09', 2),
(4, 9, NULL, NULL),
(1001, 6, NULL, 2),
(1001, 9, '2016-04-04', 2);

-- --------------------------------------------------------

--
-- Table structure for table `suppliernote`
--

CREATE TABLE `suppliernote` (
  `pkSupplierNoteID` int(11) UNSIGNED NOT NULL,
  `fkSupplierID` int(11) UNSIGNED DEFAULT NULL,
  `Note` varchar(100) DEFAULT NULL,
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 29/2/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='notes per supplier on contacts screen';

--
-- Dumping data for table `suppliernote`
--

INSERT INTO `suppliernote` (`pkSupplierNoteID`, `fkSupplierID`, `Note`, `AddedByfkUserID`) VALUES
(1, 1, 'Has two Irish setters', NULL),
(2, 1, 'Loves football. Supports Arsenal!', NULL),
(3, 3503, 'a lovely note for a lovely supplier 3503!', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `supplierpersonnel`
--

CREATE TABLE `supplierpersonnel` (
  `pkSupplierPersonnelID` int(11) NOT NULL,
  `fkSupplierID` smallint(6) UNSIGNED DEFAULT NULL,
  `PersonnelTypeID` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1= Manu. (Direct), 2 = Manu. (indirect), 3 = Quality, 4 = Engineering, 5 = Other',
  `Number` tinyint(4) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='	';

--
-- Dumping data for table `supplierpersonnel`
--

INSERT INTO `supplierpersonnel` (`pkSupplierPersonnelID`, `fkSupplierID`, `PersonnelTypeID`, `Number`) VALUES
(1, 3503, 1, 15),
(2, 3503, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `suppliertreatment`
--

CREATE TABLE `suppliertreatment` (
  `fkSupplierID` smallint(5) UNSIGNED NOT NULL,
  `fkOperationID` int(11) UNSIGNED NOT NULL,
  `Quantity` smallint(5) UNSIGNED DEFAULT NULL,
  `Price` decimal(8,2) UNSIGNED DEFAULT NULL COMMENT 'these two fields defunct? 27/7/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='sure this defunct 17/8/16';

-- --------------------------------------------------------

--
-- Table structure for table `supplycondition`
--

CREATE TABLE `supplycondition` (
  `pkSupplyConditionID` tinyint(4) UNSIGNED NOT NULL,
  `Description` varchar(20) DEFAULT NULL,
  `Deleted` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1=yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplycondition`
--

INSERT INTO `supplycondition` (`pkSupplyConditionID`, `Description`, `Deleted`) VALUES
(1, 'Machine Complete', 0),
(2, 'Gearcut only', 0),
(3, 'Broached', 0),
(4, 'Ground', 0),
(5, 'Assembly', 0),
(6, 'Manufacture complete', 0);

-- --------------------------------------------------------

--
-- Table structure for table `templateapprovalcomments`
--

CREATE TABLE `templateapprovalcomments` (
  `pkTemplateApprovalCommentID` int(10) UNSIGNED NOT NULL,
  `fkOperationTemplateID` mediumint(9) UNSIGNED DEFAULT NULL,
  `Comment` varchar(80) DEFAULT NULL COMMENT 'chgd 3/3/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `templateapprovalcomments`
--

INSERT INTO `templateapprovalcomments` (`pkTemplateApprovalCommentID`, `fkOperationTemplateID`, `Comment`) VALUES
(1, 1, 'ABC123ST16b std approval comment'),
(2, 3, 'ABC123ST16b std approval comment');

-- --------------------------------------------------------

--
-- Table structure for table `templatemachining`
--

CREATE TABLE `templatemachining` (
  `fkOperationTemplateID` mediumint(8) UNSIGNED NOT NULL,
  `fkMachiningListID` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='link table to confirm what machining task is allocated to this template operation';

-- --------------------------------------------------------

--
-- Table structure for table `templatematerial`
--

CREATE TABLE `templatematerial` (
  `pkOperationTemplateID` mediumint(8) UNSIGNED NOT NULL,
  `fkMaterialSpecID` smallint(5) UNSIGNED NOT NULL,
  `fkRFQOID` smallint(5) UNSIGNED DEFAULT NULL COMMENT '19/7/16 shld be defunct!',
  `Description` varchar(45) DEFAULT NULL COMMENT 'shld be 20chars 6/4/16',
  `Size` decimal(7,3) UNSIGNED DEFAULT NULL,
  `fkShapeID` tinyint(3) UNSIGNED DEFAULT NULL,
  `PartLength` decimal(8,2) DEFAULT NULL,
  `Yield` tinyint(4) DEFAULT NULL,
  `fkApprovalID` tinyint(3) UNSIGNED DEFAULT NULL,
  `SupplyCondition` varchar(200) DEFAULT NULL COMMENT 'think is defunct 8/4/16',
  `TestPieces` tinyint(4) DEFAULT NULL,
  `FreeIssue` tinyint(3) UNSIGNED DEFAULT '0',
  `UnitID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'mm or inches: 1 or 2',
  `StdDeliveryDays` tinyint(4) DEFAULT NULL,
  `SPUfkUOMID` tinyint(4) DEFAULT NULL,
  `StdfkSupplierID` smallint(5) UNSIGNED DEFAULT NULL,
  `IssueInstruction` varchar(50) DEFAULT NULL,
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 8/4/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Material Requirement .\nExpected price is per unit length. Only a non zero quantity indicates allocated material.';

--
-- Dumping data for table `templatematerial`
--

INSERT INTO `templatematerial` (`pkOperationTemplateID`, `fkMaterialSpecID`, `fkRFQOID`, `Description`, `Size`, `fkShapeID`, `PartLength`, `Yield`, `fkApprovalID`, `SupplyCondition`, `TestPieces`, `FreeIssue`, `UnitID`, `StdDeliveryDays`, `SPUfkUOMID`, `StdfkSupplierID`, `IssueInstruction`, `AddedByfkUserID`) VALUES
(1, 13, NULL, 'S154D', '4.200', 1, '30.00', 3, 5, NULL, 0, 0, 2, 0, 4, NULL, 'ABC123ST16b issue instruction', 4);

-- --------------------------------------------------------

--
-- Table structure for table `templatenote`
--

CREATE TABLE `templatenote` (
  `pkTemplateNoteID` int(11) UNSIGNED NOT NULL,
  `fkOperationTemplateID` mediumint(8) UNSIGNED DEFAULT NULL COMMENT 'chgd 1/3/16',
  `Note` varchar(80) DEFAULT NULL,
  `fkNoteTypeID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1=material, 2=conformity, 3=internalmaterial, 4=engineering, 5=treatment, 6=internaltreatment, 7=inspectioncomments, 8 = part control. chgd 1/3/16. rmvd part control type 3/3/16. 9=internalReview 4/3/16',
  `CreateDate` date DEFAULT NULL COMMENT 'added 1/3/16',
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 1/3/16',
  `fkPartTemplateID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 4/4/16. not sure if two tables needed, or one dual purpose.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Operation Template Notes - now part template notes';

--
-- Dumping data for table `templatenote`
--

INSERT INTO `templatenote` (`pkTemplateNoteID`, `fkOperationTemplateID`, `Note`, `fkNoteTypeID`, `CreateDate`, `AddedByfkUserID`, `fkPartTemplateID`) VALUES
(1, 1, 'ABC123ST16b conf note', 2, '2016-11-16', 4, NULL),
(2, 1, 'ABC123ST16b int mat note', 1, '2016-11-16', 4, NULL),
(3, 2, 'ABC123ST16b int eng note', 4, '2016-11-16', 4, NULL),
(4, 3, 'ABC123ST16b int SC note', 6, '2016-11-16', 4, NULL),
(5, 1, 'ABC123ST16b int QA note', 9, '2016-11-16', 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `templatepocomments`
--

CREATE TABLE `templatepocomments` (
  `pkTemplatePOCommentID` int(10) UNSIGNED NOT NULL,
  `fkOperationTemplateID` mediumint(9) DEFAULT NULL,
  `Comment` varchar(80) DEFAULT NULL COMMENT 'chgd mbh'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `templatepocomments`
--

INSERT INTO `templatepocomments` (`pkTemplatePOCommentID`, `fkOperationTemplateID`, `Comment`) VALUES
(1, 1, 'ABC123ST16b std PO comment');

-- --------------------------------------------------------

--
-- Table structure for table `templateprocess`
--

CREATE TABLE `templateprocess` (
  `pkProcessID` int(11) UNSIGNED NOT NULL,
  `fkOperationTemplateID` mediumint(9) UNSIGNED DEFAULT NULL,
  `ProcessNote` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Processes on machining template operation only';

--
-- Dumping data for table `templateprocess`
--

INSERT INTO `templateprocess` (`pkProcessID`, `fkOperationTemplateID`, `ProcessNote`) VALUES
(1, 2, 'ABC123ST16b process 1 of 1');

-- --------------------------------------------------------

--
-- Table structure for table `templateremark`
--

CREATE TABLE `templateremark` (
  `pkTemplateRemarkID` int(10) UNSIGNED NOT NULL COMMENT 'ai 4/4/16',
  `fkPartTemplateID` smallint(5) UNSIGNED DEFAULT NULL,
  `Remark` varchar(80) DEFAULT NULL,
  `CreateDate` date DEFAULT NULL,
  `AddedByUserID` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='god knows where the original went.';

-- --------------------------------------------------------

--
-- Table structure for table `templatetreatment`
--

CREATE TABLE `templatetreatment` (
  `pkOperationTemplateID` mediumint(9) UNSIGNED NOT NULL,
  `fkTreatmentID` smallint(5) UNSIGNED DEFAULT NULL,
  `LeadTime` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'in days',
  `StandardSupplierID` smallint(6) DEFAULT NULL,
  `ApprovalComment` varchar(100) DEFAULT NULL COMMENT '27/7/16 now defunct; back to multi comments',
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 3/3/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `templatetreatment`
--

INSERT INTO `templatetreatment` (`pkOperationTemplateID`, `fkTreatmentID`, `LeadTime`, `StandardSupplierID`, `ApprovalComment`, `AddedByfkUserID`) VALUES
(3, 1, 0, NULL, NULL, 4);

-- --------------------------------------------------------

--
-- Table structure for table `todo`
--

CREATE TABLE `todo` (
  `pkTodoID` mediumint(9) UNSIGNED NOT NULL,
  `RaisedDate` date DEFAULT NULL,
  `fkActionTypeID` tinyint(4) UNSIGNED DEFAULT NULL,
  `RaisedByfkUserID` smallint(6) UNSIGNED DEFAULT NULL,
  `DeptID` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1=FO,2=Pur,3=PE,4=Eng,5=Qua,6=Sch,7=Sto,8=Fin, 10=IT, 11=Partcontrol',
  `fkWorkOrderID` mediumint(9) UNSIGNED DEFAULT NULL,
  `fkPartTemplateID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 1/3/16',
  `PartNumber` varchar(25) DEFAULT NULL,
  `fkEngineeringTemplateID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 3/3/16',
  `Complete` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '0 = no, 1 = yes',
  `CompletedByfkUserID` smallint(6) UNSIGNED DEFAULT NULL,
  `Description` varchar(30) DEFAULT NULL COMMENT 'added 1/3/16',
  `fkGRBID` int(10) UNSIGNED DEFAULT NULL COMMENT 'added 12/4/16 for quality'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='action list for users';

--
-- Dumping data for table `todo`
--

INSERT INTO `todo` (`pkTodoID`, `RaisedDate`, `fkActionTypeID`, `RaisedByfkUserID`, `DeptID`, `fkWorkOrderID`, `fkPartTemplateID`, `PartNumber`, `fkEngineeringTemplateID`, `Complete`, `CompletedByfkUserID`, `Description`, `fkGRBID`) VALUES
(1, '2016-11-11', 2, 4, 2, NULL, NULL, NULL, NULL, 1, 4, 'Issue Material', 49),
(2, '2016-11-11', 2, 4, 2, NULL, NULL, NULL, NULL, 1, 4, 'Issue Material', 49),
(3, '2016-11-16', 15, 4, 4, 1, NULL, 'ABC123ST16b/PT', NULL, 0, NULL, 'S154D MAT', NULL),
(4, '2016-11-16', 15, 4, 4, 1, NULL, 'ABC123ST16b/PT', NULL, 0, NULL, 'Heat Treatment SC', NULL),
(6, '2016-11-16', 1, 4, 5, 1, NULL, '', NULL, 1, 4, 'Inspect Goods received', 49),
(7, '2016-11-16', 2, 4, 2, 1, NULL, NULL, NULL, 1, 4, 'Issue Material', 49),
(8, '2016-11-16', 1, 4, 5, NULL, NULL, NULL, NULL, 1, 4, 'Inspect machine set', NULL),
(9, '2016-11-16', 1, 4, 5, 1, NULL, '', NULL, 1, 4, 'Inspect Goods received', 50),
(10, '2016-11-16', 2, 4, 2, 1, NULL, NULL, NULL, 0, 4, 'Issue Material', 50),
(11, '2016-11-16', 22, 4, 2, 1, NULL, '', NULL, NULL, NULL, 'Dispatch parts', 51),
(12, '2016-11-16', 22, 4, 2, 1, NULL, NULL, NULL, NULL, NULL, 'Dispatch parts', 51),
(13, '2016-11-17', 1, 4, 5, NULL, NULL, '', NULL, NULL, NULL, 'Inspect Goods received', 52),
(14, '2016-11-21', 15, 4, 4, 3, NULL, 'ABC123ST16b/PT', NULL, 0, NULL, 'S154D MAT', NULL),
(15, '2016-11-21', 15, 4, 4, 3, NULL, 'ABC123ST16b/PT', NULL, 0, NULL, 'Heat Treatment SC', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tool`
--

CREATE TABLE `tool` (
  `pkToolID` smallint(5) UNSIGNED NOT NULL,
  `GRBNumber` int(11) UNSIGNED DEFAULT NULL,
  `SerialNumber` varchar(20) DEFAULT NULL,
  `Description` varchar(25) DEFAULT NULL,
  `ToolStatus` tinyint(4) UNSIGNED DEFAULT NULL,
  `fkLocationID` smallint(5) UNSIGNED DEFAULT NULL,
  `fkTypeID` tinyint(3) UNSIGNED DEFAULT NULL,
  `fkSubTypeID` tinyint(3) UNSIGNED DEFAULT NULL,
  `fkPartID` smallint(5) UNSIGNED DEFAULT NULL,
  `InUseYN` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1 = Y, 2 = N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tool`
--

INSERT INTO `tool` (`pkToolID`, `GRBNumber`, `SerialNumber`, `Description`, `ToolStatus`, `fkLocationID`, `fkTypeID`, `fkSubTypeID`, `fkPartID`, `InUseYN`) VALUES
(1, 810072, 'JSC1036A', 'Caliper', 1, NULL, 1, 1, 1, NULL),
(2, 810073, 'ABC123', 'Bigger Caliper', 2, NULL, 1, 2, 1, NULL),
(3, 810001, '1000999', 'Ripper', 3, NULL, 2, 3, 1, NULL),
(4, 810023, '5422991A-2', 'Ripper', 1, NULL, 2, 4, 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `toolsubtype`
--

CREATE TABLE `toolsubtype` (
  `pkSubTypeID` tinyint(3) UNSIGNED NOT NULL,
  `SubType` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `toolsubtype`
--

INSERT INTO `toolsubtype` (`pkSubTypeID`, `SubType`) VALUES
(1, 'Bullnose'),
(2, 'Ballnose'),
(3, 'Tee slot'),
(4, 'Square'),
(5, 'Shell');

-- --------------------------------------------------------

--
-- Table structure for table `tooltype`
--

CREATE TABLE `tooltype` (
  `pkTypeID` tinyint(3) UNSIGNED NOT NULL,
  `TType` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tooltype`
--

INSERT INTO `tooltype` (`pkTypeID`, `TType`) VALUES
(1, 'End Mill'),
(2, 'Reamer');

-- --------------------------------------------------------

--
-- Table structure for table `trainingcourse`
--

CREATE TABLE `trainingcourse` (
  `pkCourseID` tinyint(3) UNSIGNED NOT NULL,
  `CourseName` varchar(12) DEFAULT NULL,
  `Anniversary` date DEFAULT NULL,
  `RefreshPeriod` tinyint(4) UNSIGNED NOT NULL COMMENT 'period in years'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `treatmentlist`
--

CREATE TABLE `treatmentlist` (
  `pkTreatmentID` smallint(5) UNSIGNED NOT NULL COMMENT 'chgd ai 2/3/16',
  `Description` varchar(20) DEFAULT NULL,
  `Deleted` tinyint(4) UNSIGNED DEFAULT '0',
  `DeletedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 2/3/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `treatmentlist`
--

INSERT INTO `treatmentlist` (`pkTreatmentID`, `Description`, `Deleted`, `DeletedByfkUserID`) VALUES
(1, 'Heat Treatment', 0, NULL),
(2, 'Thread Cutting', 0, NULL),
(3, 'Harden & Temper', 0, NULL),
(4, 'Gear Grinding', 0, NULL),
(5, 'Strip Copper', 0, NULL),
(6, 'Stress Relieve', 0, NULL),
(7, 'Grinding', 0, NULL),
(8, 'Machining', 0, NULL),
(9, 'Shot Peening', 0, NULL),
(10, 'Copper Plate', 0, NULL),
(11, 'Nitride', 0, NULL),
(12, 'Spark Erode', 0, NULL),
(13, 'Wire Erode', 0, NULL),
(14, 'Centreless Grinding', 0, NULL),
(15, 'Penetrative FD', 0, NULL),
(16, 'Oxygen Clean', 0, NULL),
(17, 'Engraving', 0, NULL),
(18, 'Pressure Test', 0, NULL),
(19, 'Thread Grinding', 0, NULL),
(20, 'Gun Drilling', 0, NULL),
(21, 'Welding', 0, NULL),
(22, 'Magnetic FD', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `treatmentnote`
--

CREATE TABLE `treatmentnote` (
  `pkNoteID` int(10) UNSIGNED NOT NULL,
  `fkTreatmentID` mediumint(8) UNSIGNED DEFAULT NULL,
  `fkSupplierID` smallint(5) UNSIGNED DEFAULT NULL,
  `Note` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `unitofmeasure`
--

CREATE TABLE `unitofmeasure` (
  `pkUOMID` tinyint(3) UNSIGNED NOT NULL,
  `Description` varchar(11) DEFAULT NULL,
  `Deleted` tinyint(4) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `unitofmeasure`
--

INSERT INTO `unitofmeasure` (`pkUOMID`, `Description`, `Deleted`) VALUES
(1, 'Bars', 0),
(2, 'Billets', 0),
(3, 'Litres', 0),
(4, 'Metres', 0),
(5, 'Millimetres', 0),
(6, 'Off', 0),
(7, 'Packs', 0),
(8, 'Pairs', 0),
(9, 'Pallets', 0),
(10, 'Plates', 0),
(11, 'R/Length', 0),
(12, 'Rolls', 0),
(13, 'Sets', 0),
(14, 'Sheets', 0),
(15, 'Tonnes', 0);

-- --------------------------------------------------------

--
-- Table structure for table `unittype`
--

CREATE TABLE `unittype` (
  `pkUnitTypeID` tinyint(3) UNSIGNED NOT NULL,
  `Unit` char(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='for measuring devices\n';

--
-- Dumping data for table `unittype`
--

INSERT INTO `unittype` (`pkUnitTypeID`, `Unit`) VALUES
(1, 'BA'),
(2, 'BSF'),
(3, 'BSW'),
(4, 'MM'),
(5, 'UN'),
(6, 'UNC'),
(7, 'UNEF'),
(8, 'UNF'),
(9, 'UNS'),
(10, 'ACME'),
(11, 'BSP'),
(12, 'BSPF'),
(13, 'GAS'),
(14, 'MJ'),
(15, 'NC'),
(16, 'NF'),
(17, 'NS'),
(18, 'UNJC'),
(19, 'UNJF'),
(20, 'W'),
(21, 'WHIT'),
(22, 'TURN'),
(23, 'MILL');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `pkUserID` smallint(6) UNSIGNED NOT NULL COMMENT 'chgd 26/3/16',
  `UName` varchar(10) DEFAULT NULL,
  `pwd` varchar(32) DEFAULT NULL,
  `FirstName` varchar(25) DEFAULT NULL,
  `LastName` varchar(25) DEFAULT NULL,
  `Address1` varchar(45) DEFAULT NULL,
  `Address2` varchar(40) DEFAULT NULL,
  `Address3` varchar(40) DEFAULT NULL,
  `Address4` varchar(30) DEFAULT NULL,
  `Postcode` char(8) DEFAULT NULL,
  `HomePhone` varchar(20) DEFAULT NULL,
  `MobilePhone` varchar(20) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `ChargeRate` decimal(5,2) UNSIGNED DEFAULT NULL,
  `SkillLevel` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1-5?',
  `Employed` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1 = Y, 2 = N',
  `Extension` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'Office extension number',
  `ReportsTo` smallint(6) UNSIGNED DEFAULT NULL COMMENT 'id of master user\nUN 28/1/16',
  `POLimit` smallint(6) UNSIGNED DEFAULT NULL COMMENT 'UN 28/1/16',
  `SpendLimit` smallint(6) UNSIGNED DEFAULT NULL COMMENT 'UN 28/1/16',
  `ForkOperator` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 23/2/16',
  `FirstAid` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 23/2/16',
  `LeaveDays` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 23/2/16',
  `Salary` mediumint(8) UNSIGNED DEFAULT NULL COMMENT 'added 23/2/16. chgd 10/5/16',
  `Hours` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 23/2/16',
  `Security` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 23/2/16',
  `ContractFileName` varchar(30) DEFAULT NULL COMMENT 'added 23/2/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`pkUserID`, `UName`, `pwd`, `FirstName`, `LastName`, `Address1`, `Address2`, `Address3`, `Address4`, `Postcode`, `HomePhone`, `MobilePhone`, `Email`, `ChargeRate`, `SkillLevel`, `Employed`, `Extension`, `ReportsTo`, `POLimit`, `SpendLimit`, `ForkOperator`, `FirstAid`, `LeaveDays`, `Salary`, `Hours`, `Security`, `ContractFileName`) VALUES
(1, 'bmullen', '5f4dcc3b5aa765d61d8327deb882cf99', 'Brian', 'Mullen', 'sxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxe', 'sxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxe', 'sxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxe', 'sxxxxxxxxxxxxxxxxxxxxxxxxxxxxe', 'AA11 1AA', '12345678909876543212', '12345678909876543212', 'sxxxxxxxxxxxxxxxxxxxxxxxxx@xxxxxxxxxxxxxxxxxxxxxxe', '999.99', 5, 1, 255, 2, 65432, 65432, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'btownsend', NULL, 'Barry', 'Townsend', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'jtownsend', NULL, 'Jo', 'Townsend', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'dphillips', '8d2fb689abcc4d7055ad21f4495085ac', 'David', 'Phillips', 'None', 'Of', 'Your', 'Business', 'SS13 1DJ', '01234 567890', '07973 166881', 'davidphillips@simbiis.com', '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'rjeremy', NULL, 'Richard', 'Jeremy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'mbowler', NULL, 'Marc', 'Bowler', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 'drogers', NULL, 'Dave', 'Rogers', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 'gwitney', NULL, 'Graham', 'Witney', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 'apoulton', NULL, 'Andrew', 'Poulton', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 'dhumphrey', NULL, 'Dave', 'Humphrey', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 'mwild', NULL, 'Mark', 'Wild', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(102, 'swilson', NULL, 'Sue', 'Wilson', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, 'cwelsford', NULL, 'Carole', 'Welsford', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, 'rhenman', NULL, 'Ross', 'Henman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, 'tjohnson', NULL, 'Tony', 'Johnson', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, 'lcaton', NULL, 'Lorraine', 'Caton', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, 'scrittell', NULL, 'Steve', 'Crittell', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, 'lwren', NULL, 'Lee', 'Wren', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, 'gsnow', NULL, 'George', 'Snow', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(131, 'kandrews', NULL, 'Keith', 'Andrews', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(133, 'abyrne', NULL, 'Alicia', 'Byrne', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(152, 'lsargean', NULL, 'Liam', 'Sargeant', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(153, 'dlong', '', 'Dave', 'Long', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(155, 'twilliam', NULL, 'Tony', 'Williams', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.01', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(156, 'NChung', NULL, 'Nguyen', 'Chung', 'Address 1', 'Address 2', 'Address 3', 'Address 4', 'Pcode', '0123', '0456', 'a@gmail.com', '999.99', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 1, NULL),
(255, 'SYSTEM', NULL, 'Kythera', 'System', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `usercourse`
--

CREATE TABLE `usercourse` (
  `fkUserID` smallint(5) UNSIGNED NOT NULL,
  `fkCourseID` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userdept`
--

CREATE TABLE `userdept` (
  `fkUserID` smallint(5) UNSIGNED NOT NULL,
  `fkDeptID` tinyint(3) UNSIGNED NOT NULL COMMENT 'chgd 12/02/16',
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 23/2/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='chgd to dept 12/02/16';

--
-- Dumping data for table `userdept`
--

INSERT INTO `userdept` (`fkUserID`, `fkDeptID`, `AddedByfkUserID`) VALUES
(1, 5, NULL),
(4, 1, NULL),
(4, 2, NULL),
(4, 3, NULL),
(4, 4, NULL),
(4, 5, NULL),
(4, 6, NULL),
(4, 7, NULL),
(4, 8, NULL),
(33, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userrole`
--

CREATE TABLE `userrole` (
  `fkUserID` smallint(5) UNSIGNED NOT NULL,
  `fkRoleID` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='think defunct 25/5/16';

--
-- Dumping data for table `userrole`
--

INSERT INTO `userrole` (`fkUserID`, `fkRoleID`) VALUES
(2, 11);

-- --------------------------------------------------------

--
-- Table structure for table `vat`
--

CREATE TABLE `vat` (
  `Rate` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='25/1/16 store vat rate';

--
-- Dumping data for table `vat`
--

INSERT INTO `vat` (`Rate`) VALUES
(20);

-- --------------------------------------------------------

--
-- Stand-in structure for view `viewinvoicereport`
-- (See below for the actual view)
--
CREATE TABLE `viewinvoicereport` (
`CreateDate` datetime
,`InvNo` int(11) unsigned
,`Total` decimal(13,2)
,`CustomerName` varchar(50)
,`pkWorkOrderID` mediumint(8) unsigned
,`Complete` varchar(3)
,`Quantity` smallint(5) unsigned
,`PartNumber` varchar(25)
,`CustomerOrderRef` varchar(25)
);

-- --------------------------------------------------------

--
-- Table structure for table `websiteaccess`
--

CREATE TABLE `websiteaccess` (
  `pkWebsiteID` tinyint(3) UNSIGNED NOT NULL COMMENT 'chgd ai 15/3/16',
  `SiteAddress` varchar(45) DEFAULT NULL,
  `Username` varchar(25) DEFAULT NULL,
  `SitePassword` varchar(15) DEFAULT NULL COMMENT 'chgd name 16/2/16',
  `Deleted` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 16/2/16',
  `AddedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 3/4/16',
  `DeletedByfkUserID` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'added 3/4/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `websiteaccess`
--

INSERT INTO `websiteaccess` (`pkWebsiteID`, `SiteAddress`, `Username`, `SitePassword`, `Deleted`, `AddedByfkUserID`, `DeletedByfkUserID`) VALUES
(1, 'www.eauditnet.com', 'esllse', 'quality', 0, NULL, NULL),
(2, 'www.web.site', 'user', 'pass', 0, 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wodocument`
--

CREATE TABLE `wodocument` (
  `pkWODocumentID` int(10) UNSIGNED NOT NULL,
  `fkWorkOrderID` mediumint(9) DEFAULT NULL,
  `fkDocumentDropTypeID` tinyint(4) DEFAULT NULL COMMENT 'chgd 27/1/16',
  `FileName` varchar(30) DEFAULT NULL,
  `Print` tinyint(4) DEFAULT NULL COMMENT '1=yes. added 27/1/16',
  `AddedByfkUserID` tinyint(4) DEFAULT NULL,
  `DateAdded` date DEFAULT NULL COMMENT 'added 1/2/16'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='holds ADD for each WO - shows on WO>Docs';

--
-- Dumping data for table `wodocument`
--

INSERT INTO `wodocument` (`pkWODocumentID`, `fkWorkOrderID`, `fkDocumentDropTypeID`, `FileName`, `Print`, `AddedByfkUserID`, `DateAdded`) VALUES
(1, 1, 1, '1479396572_TestStructure.jpg', 0, 4, '2016-11-20'),
(2, 1, 2, '1479396582_FAIR.JPG', 0, 4, '2016-11-20'),
(3, 1, 3, '1479396593_BA Engineering .jpg', 0, 4, '2016-11-20'),
(4, 3, 1, '1479725376_TestStructure.jpg', 0, 4, '2016-11-21'),
(5, 3, 2, '1479725386_FAIR.JPG', 0, 4, '2016-11-21'),
(6, 3, 3, '1479725415_BA Engineering .jpg', 0, 4, '2016-11-21');

-- --------------------------------------------------------

--
-- Table structure for table `womaterialnote`
--

CREATE TABLE `womaterialnote` (
  `pkMaterialNoteID` mediumint(9) UNSIGNED NOT NULL,
  `fkWorkOrderID` mediumint(9) UNSIGNED DEFAULT NULL COMMENT 'uns 3/5/16',
  `Note` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wooutvoice`
--

CREATE TABLE `wooutvoice` (
  `fkInvoiceID` int(11) UNSIGNED NOT NULL,
  `fkWorkOrderID` mediumint(8) UNSIGNED NOT NULL,
  `fkUserID` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wooutvoice`
--

INSERT INTO `wooutvoice` (`fkInvoiceID`, `fkWorkOrderID`, `fkUserID`) VALUES
(1, 1, NULL),
(2, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `workorder`
--

CREATE TABLE `workorder` (
  `pkWorkOrderID` mediumint(8) UNSIGNED NOT NULL,
  `fkLineItemID` mediumint(8) UNSIGNED DEFAULT NULL,
  `fkApprovalID` tinyint(3) UNSIGNED DEFAULT NULL,
  `Complete` tinyint(4) UNSIGNED DEFAULT NULL,
  `QuantityReqd` smallint(5) UNSIGNED DEFAULT NULL,
  `QuantityOvers` smallint(5) UNSIGNED DEFAULT NULL,
  `PriceEach` decimal(7,2) UNSIGNED DEFAULT NULL,
  `OrigDeliveryDate` date DEFAULT NULL,
  `NewDeliveryDate` date DEFAULT NULL,
  `ChangeReason` varchar(30) DEFAULT NULL,
  `FAIR` tinyint(4) UNSIGNED DEFAULT NULL,
  `ReviewStatus` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1 = in process, 2 = passed, 3 = failed',
  `RiskAssessment` tinyint(4) UNSIGNED DEFAULT NULL COMMENT '1 = Low, 2 = Med, 3 = High',
  `Live` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '1 = y, 2 = Cancelled/closed',
  `CreatedByfkUserID` smallint(6) UNSIGNED DEFAULT NULL,
  `PaperworkComplete` tinyint(4) UNSIGNED DEFAULT NULL COMMENT 'added 21/1/16',
  `FastTrack` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 18/2/16',
  `Tooling` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 25/4/16',
  `QAEquipment` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 25/4/16',
  `CorrectApproval` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 25/4/16',
  `PreCancel` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'added 7/5/16 - 2 part cancel, 1=ok'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `workorder`
--

INSERT INTO `workorder` (`pkWorkOrderID`, `fkLineItemID`, `fkApprovalID`, `Complete`, `QuantityReqd`, `QuantityOvers`, `PriceEach`, `OrigDeliveryDate`, `NewDeliveryDate`, `ChangeReason`, `FAIR`, `ReviewStatus`, `RiskAssessment`, `Live`, `CreatedByfkUserID`, `PaperworkComplete`, `FastTrack`, `Tooling`, `QAEquipment`, `CorrectApproval`, `PreCancel`) VALUES
(1, 1, 2, 1, 45, 9, '5.85', '2016-11-16', NULL, '', 0, 2, 2, 1, 4, NULL, NULL, 1, 1, 1, NULL),
(2, 2, 3, 0, 55, 20, '5.85', '2016-11-17', NULL, '', 1, 2, 2, 0, 4, NULL, NULL, 1, 1, 1, NULL),
(3, 2, 3, 0, 23, 10, '5.85', '2016-11-21', NULL, '', 1, 2, 2, 1, 4, NULL, NULL, 1, 1, 1, NULL);

--
-- Triggers `workorder`
--
DELIMITER $$
CREATE TRIGGER `workorder_AINS` AFTER INSERT ON `workorder` FOR EACH ROW BEGIN
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `workordernote`
--

CREATE TABLE `workordernote` (
  `pkWONoteID` mediumint(9) NOT NULL,
  `Note` varchar(80) DEFAULT NULL,
  `fkWorkOrderID` mediumint(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='notes at WO level (tab 1 WOs)';

--
-- Dumping data for table `workordernote`
--

INSERT INTO `workordernote` (`pkWONoteID`, `Note`, `fkWorkOrderID`) VALUES
(1, 'ABC123ST16b WO note', 1);

-- --------------------------------------------------------

--
-- Table structure for table `workorderremark`
--

CREATE TABLE `workorderremark` (
  `fkWorkOrderID` mediumint(8) UNSIGNED NOT NULL,
  `fkRemarkID` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xcoordinate`
--

CREATE TABLE `xcoordinate` (
  `pkXCoordID` smallint(5) UNSIGNED NOT NULL COMMENT 'chgd 9/3/16',
  `fkStoreAreaID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT '9/2/16',
  `XCoord` tinyint(3) UNSIGNED DEFAULT NULL,
  `Deleted` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='extension of stores location. 7/3/16. chgd 9/2/16';

--
-- Dumping data for table `xcoordinate`
--

INSERT INTO `xcoordinate` (`pkXCoordID`, `fkStoreAreaID`, `XCoord`, `Deleted`) VALUES
(1, 1, 1, 0),
(2, 1, 2, 0),
(3, 1, 3, 1),
(4, 1, 4, 0),
(5, 1, 5, 1),
(6, 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ycoordinate`
--

CREATE TABLE `ycoordinate` (
  `pkYCoordID` smallint(5) UNSIGNED NOT NULL,
  `fkStoreAreaID` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'chgd 9/3/16',
  `YCoord` char(1) DEFAULT NULL,
  `Deleted` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='extension of stores location. 7/3/16';

--
-- Dumping data for table `ycoordinate`
--

INSERT INTO `ycoordinate` (`pkYCoordID`, `fkStoreAreaID`, `YCoord`, `Deleted`) VALUES
(1, 1, 'A', 1),
(2, 1, 'B', 0),
(3, 1, 'C', 1),
(4, 1, 'D', 0),
(5, 1, 'E', 0),
(6, 2, 'E', 0);

-- --------------------------------------------------------

--
-- Structure for view `exportcustomers`
--
DROP TABLE IF EXISTS `exportcustomers`;

CREATE ALGORITHM=UNDEFINED DEFINER=`david`@`localhost` SQL SECURITY DEFINER VIEW `exportcustomers`  AS  select `customer`.`FAO` AS `FAO`,`customer`.`CustomerName` AS `ClientName`,`customer`.`Address1` AS `Address1`,`customer`.`Address2` AS `Address2`,`customer`.`Address3` AS `Address3`,`customer`.`Address4` AS `Address4`,`customer`.`Postcode` AS `Postcode` from `customer` ;

-- --------------------------------------------------------

--
-- Structure for view `viewinvoicereport`
--
DROP TABLE IF EXISTS `viewinvoicereport`;

CREATE ALGORITHM=UNDEFINED DEFINER=`david`@`localhost` SQL SECURITY DEFINER VIEW `viewinvoicereport`  AS  select `d`.`CreateDate` AS `CreateDate`,`d`.`pkDocumentID` AS `InvNo`,((`wo`.`QuantityReqd` * `wo`.`PriceEach`) + 0.00) AS `Total`,`c`.`CustomerName` AS `CustomerName`,`wo`.`pkWorkOrderID` AS `pkWorkOrderID`,if((isnull(`wo`.`Complete`) or (`wo`.`Complete` <> 1)),'No','Yes') AS `Complete`,`wo`.`QuantityReqd` AS `Quantity`,`p`.`PartNumber` AS `PartNumber`,`co`.`CustomerOrderRef` AS `CustomerOrderRef` from (((((`document` `d` join `workorder` `wo` on((`d`.`fkWorkOrderID` = `wo`.`pkWorkOrderID`))) join `lineitem` `li` on((`wo`.`fkLineItemID` = `li`.`pkLineItemID`))) join `parttemplate` `p` on((`li`.`fkPartTemplateID` = `p`.`pkPartTemplateID`))) join `customerorder` `co` on((`li`.`fkOrderID` = `co`.`pkOrderID`))) join `customer` `c` on((`co`.`fkCustomerID` = `c`.`pkCustomerID`))) where (`d`.`fkDocumentTypeID` = 10) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acknowledgementnote`
--
ALTER TABLE `acknowledgementnote`
  ADD PRIMARY KEY (`pkAcknowNoteID`);

--
-- Indexes for table `actiontype`
--
ALTER TABLE `actiontype`
  ADD PRIMARY KEY (`pkActionTypeID`);

--
-- Indexes for table `additionaltime`
--
ALTER TABLE `additionaltime`
  ADD PRIMARY KEY (`pkAdditionalID`);

--
-- Indexes for table `allocated`
--
ALTER TABLE `allocated`
  ADD PRIMARY KEY (`fkOperationID`,`fkGRBID`);

--
-- Indexes for table `approval`
--
ALTER TABLE `approval`
  ADD PRIMARY KEY (`pkApprovalID`);

--
-- Indexes for table `approvalarea`
--
ALTER TABLE `approvalarea`
  ADD PRIMARY KEY (`pkApprovalAreaID`);

--
-- Indexes for table `approvallevel`
--
ALTER TABLE `approvallevel`
  ADD PRIMARY KEY (`pkApprovalLevelID`);

--
-- Indexes for table `approvalreq`
--
ALTER TABLE `approvalreq`
  ADD PRIMARY KEY (`pkApprovalReqID`);

--
-- Indexes for table `associatedpartdoc`
--
ALTER TABLE `associatedpartdoc`
  ADD PRIMARY KEY (`pkAPDID`);

--
-- Indexes for table `branchlocation`
--
ALTER TABLE `branchlocation`
  ADD PRIMARY KEY (`pkBranchID`);

--
-- Indexes for table `businessshutdown`
--
ALTER TABLE `businessshutdown`
  ADD PRIMARY KEY (`pkShutdownID`);

--
-- Indexes for table `calibrate`
--
ALTER TABLE `calibrate`
  ADD PRIMARY KEY (`fkSupplierID`,`fkMeasuringDeviceID`);

--
-- Indexes for table `calibrationparameter`
--
ALTER TABLE `calibrationparameter`
  ADD PRIMARY KEY (`pkParameterID`);

--
-- Indexes for table `calibrationreading`
--
ALTER TABLE `calibrationreading`
  ADD PRIMARY KEY (`pkReadingID`);

--
-- Indexes for table `certreqs`
--
ALTER TABLE `certreqs`
  ADD PRIMARY KEY (`pkCertReqID`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`pkCommentID`);

--
-- Indexes for table `contingency`
--
ALTER TABLE `contingency`
  ADD PRIMARY KEY (`pkContingencyID`);

--
-- Indexes for table `courier`
--
ALTER TABLE `courier`
  ADD PRIMARY KEY (`pkCourierID`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`pkCustomerID`);

--
-- Indexes for table `customeradditionaltime`
--
ALTER TABLE `customeradditionaltime`
  ADD PRIMARY KEY (`pkAdditionalID`);

--
-- Indexes for table `customercontact`
--
ALTER TABLE `customercontact`
  ADD PRIMARY KEY (`pkContactID`);

--
-- Indexes for table `customernote`
--
ALTER TABLE `customernote`
  ADD PRIMARY KEY (`pkCustomerNoteID`);

--
-- Indexes for table `customerorder`
--
ALTER TABLE `customerorder`
  ADD PRIMARY KEY (`pkOrderID`);

--
-- Indexes for table `customerordernote`
--
ALTER TABLE `customerordernote`
  ADD PRIMARY KEY (`pkOrderNoteID`);

--
-- Indexes for table `customerpersonnel`
--
ALTER TABLE `customerpersonnel`
  ADD PRIMARY KEY (`pkCustomerPersonnelID`);

--
-- Indexes for table `deliverycomment`
--
ALTER TABLE `deliverycomment`
  ADD PRIMARY KEY (`pkDeliveryCommentID`);

--
-- Indexes for table `deliverycontact`
--
ALTER TABLE `deliverycontact`
  ADD PRIMARY KEY (`pkDeliveryContactID`);

--
-- Indexes for table `deliverypoint`
--
ALTER TABLE `deliverypoint`
  ADD PRIMARY KEY (`pkDeliveryPointID`);

--
-- Indexes for table `dept`
--
ALTER TABLE `dept`
  ADD PRIMARY KEY (`pkDeptID`);

--
-- Indexes for table `deviation`
--
ALTER TABLE `deviation`
  ADD PRIMARY KEY (`pkDeviationID`);

--
-- Indexes for table `deviationtype`
--
ALTER TABLE `deviationtype`
  ADD PRIMARY KEY (`pkDeviationTypeID`);

--
-- Indexes for table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`pkDeviceID`);

--
-- Indexes for table `devicelocation`
--
ALTER TABLE `devicelocation`
  ADD PRIMARY KEY (`pkLocationID`);

--
-- Indexes for table `devicetype`
--
ALTER TABLE `devicetype`
  ADD PRIMARY KEY (`pkDeviceTypeID`);

--
-- Indexes for table `deviceuse`
--
ALTER TABLE `deviceuse`
  ADD PRIMARY KEY (`pkUseID`);

--
-- Indexes for table `dispatch`
--
ALTER TABLE `dispatch`
  ADD PRIMARY KEY (`pkDispatchID`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`pkDocumentID`);

--
-- Indexes for table `documentdroptype`
--
ALTER TABLE `documentdroptype`
  ADD PRIMARY KEY (`pkDocumentDropTypeID`);

--
-- Indexes for table `documenttype`
--
ALTER TABLE `documenttype`
  ADD PRIMARY KEY (`pkDocumentTypeID`);

--
-- Indexes for table `drawing`
--
ALTER TABLE `drawing`
  ADD PRIMARY KEY (`pkDrawingID`);

--
-- Indexes for table `engineeringtemplate`
--
ALTER TABLE `engineeringtemplate`
  ADD PRIMARY KEY (`pkEngineeringTemplateID`);

--
-- Indexes for table `exchangerate`
--
ALTER TABLE `exchangerate`
  ADD PRIMARY KEY (`pkExchangeRateID`);

--
-- Indexes for table `externalreject`
--
ALTER TABLE `externalreject`
  ADD PRIMARY KEY (`pkExternalRejectID`);

--
-- Indexes for table `fairreqs`
--
ALTER TABLE `fairreqs`
  ADD PRIMARY KEY (`pkFAIRReqs`);

--
-- Indexes for table `fat`
--
ALTER TABLE `fat`
  ADD PRIMARY KEY (`pkFatID`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fixture`
--
ALTER TABLE `fixture`
  ADD PRIMARY KEY (`pkFixtureID`);

--
-- Indexes for table `floorlocation`
--
ALTER TABLE `floorlocation`
  ADD PRIMARY KEY (`pkFloorLocationID`);

--
-- Indexes for table `freeformnote`
--
ALTER TABLE `freeformnote`
  ADD PRIMARY KEY (`pkFreeformNoteID`);

--
-- Indexes for table `goodsdocument`
--
ALTER TABLE `goodsdocument`
  ADD PRIMARY KEY (`pkDelDocID`);

--
-- Indexes for table `goodsin`
--
ALTER TABLE `goodsin`
  ADD PRIMARY KEY (`pkGRBID`);

--
-- Indexes for table `holiday`
--
ALTER TABLE `holiday`
  ADD PRIMARY KEY (`pkHolidayID`);

--
-- Indexes for table `inspection`
--
ALTER TABLE `inspection`
  ADD PRIMARY KEY (`pkInspectionID`);

--
-- Indexes for table `inspectiondevice`
--
ALTER TABLE `inspectiondevice`
  ADD PRIMARY KEY (`pkDeviceID`);

--
-- Indexes for table `inspectiontype`
--
ALTER TABLE `inspectiontype`
  ADD PRIMARY KEY (`pkTypeID`);

--
-- Indexes for table `inspectpause`
--
ALTER TABLE `inspectpause`
  ADD PRIMARY KEY (`pkPauseID`);

--
-- Indexes for table `instruction`
--
ALTER TABLE `instruction`
  ADD PRIMARY KEY (`InstructionID`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`pkInvoiceID`);

--
-- Indexes for table `invoiceaddress`
--
ALTER TABLE `invoiceaddress`
  ADD PRIMARY KEY (`pkInvoiceAddressID`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`pkItemID`);

--
-- Indexes for table `lineitem`
--
ALTER TABLE `lineitem`
  ADD PRIMARY KEY (`pkLineItemID`);

--
-- Indexes for table `machine`
--
ALTER TABLE `machine`
  ADD PRIMARY KEY (`pkMachineID`);

--
-- Indexes for table `machinemaintenance`
--
ALTER TABLE `machinemaintenance`
  ADD PRIMARY KEY (`pkMaintenanceID`);

--
-- Indexes for table `machinemaintparam`
--
ALTER TABLE `machinemaintparam`
  ADD PRIMARY KEY (`pkMachineID`);

--
-- Indexes for table `machineset`
--
ALTER TABLE `machineset`
  ADD PRIMARY KEY (`pkSetID`);

--
-- Indexes for table `machinetraining`
--
ALTER TABLE `machinetraining`
  ADD PRIMARY KEY (`fkUserID`);

--
-- Indexes for table `machininglist`
--
ALTER TABLE `machininglist`
  ADD PRIMARY KEY (`pkMachiningListID`);

--
-- Indexes for table `machiningtype`
--
ALTER TABLE `machiningtype`
  ADD PRIMARY KEY (`pkMachiningTypeID`);

--
-- Indexes for table `maintenanceparameter`
--
ALTER TABLE `maintenanceparameter`
  ADD PRIMARY KEY (`pkParameterID`);

--
-- Indexes for table `manufacturer`
--
ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`pkManufacturerID`);

--
-- Indexes for table `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`pkMaterialID`),
  ADD KEY `uom_idx` (`fkUOMID`),
  ADD KEY `MatType_idx` (`fkMaterialTypeID`),
  ADD KEY `approval_idx` (`fkApprovalLevelID`);

--
-- Indexes for table `materialinspect`
--
ALTER TABLE `materialinspect`
  ADD PRIMARY KEY (`fkMaterialID`,`fkInspectionID`);

--
-- Indexes for table `materiallocation`
--
ALTER TABLE `materiallocation`
  ADD PRIMARY KEY (`fkMaterialID`,`fkStoreAreaID`,`fkXCoordID`,`fkYCoordID`);

--
-- Indexes for table `materialnote`
--
ALTER TABLE `materialnote`
  ADD PRIMARY KEY (`pkMaterialNoteID`);

--
-- Indexes for table `materialspec`
--
ALTER TABLE `materialspec`
  ADD PRIMARY KEY (`pkMaterialSpecID`);

--
-- Indexes for table `materialtype`
--
ALTER TABLE `materialtype`
  ADD PRIMARY KEY (`pkMaterialTypeID`);

--
-- Indexes for table `measuringdevice`
--
ALTER TABLE `measuringdevice`
  ADD PRIMARY KEY (`pkDeviceID`);

--
-- Indexes for table `measuringdevicetype`
--
ALTER TABLE `measuringdevicetype`
  ADD PRIMARY KEY (`pkMeasuringDeviceTypeID`);

--
-- Indexes for table `nadcapapproval`
--
ALTER TABLE `nadcapapproval`
  ADD PRIMARY KEY (`pkApprovalID`);

--
-- Indexes for table `operatingprocedure`
--
ALTER TABLE `operatingprocedure`
  ADD PRIMARY KEY (`pkOperatingProcedureID`);

--
-- Indexes for table `operation`
--
ALTER TABLE `operation`
  ADD PRIMARY KEY (`pkOperationID`),
  ADD KEY `fkOperationTypeID_idx` (`fkOperationTypeID`),
  ADD KEY `fkWorkOrderID_idx` (`fkWorkOrderID`);

--
-- Indexes for table `operationapprovalcomments`
--
ALTER TABLE `operationapprovalcomments`
  ADD PRIMARY KEY (`pkApprovalCommentID`);

--
-- Indexes for table `operationdocument`
--
ALTER TABLE `operationdocument`
  ADD PRIMARY KEY (`pkDocumentID`),
  ADD KEY `DocsToMaterial` (`fkOperationID`);

--
-- Indexes for table `operationmachine`
--
ALTER TABLE `operationmachine`
  ADD PRIMARY KEY (`fkOperationID`,`fkMachineID`);

--
-- Indexes for table `operationmachining`
--
ALTER TABLE `operationmachining`
  ADD PRIMARY KEY (`fkOperationID`,`fkMachiningListID`);

--
-- Indexes for table `operationmaterial`
--
ALTER TABLE `operationmaterial`
  ADD PRIMARY KEY (`pkOperationID`,`fkMaterialSpecID`);

--
-- Indexes for table `operationnote`
--
ALTER TABLE `operationnote`
  ADD PRIMARY KEY (`pkOperationNoteID`);

--
-- Indexes for table `operationorderitem`
--
ALTER TABLE `operationorderitem`
  ADD PRIMARY KEY (`fkOperationID`,`fkItemID`);

--
-- Indexes for table `operationpo`
--
ALTER TABLE `operationpo`
  ADD PRIMARY KEY (`fkOperationID`,`fkItemID`);

--
-- Indexes for table `operationprocess`
--
ALTER TABLE `operationprocess`
  ADD PRIMARY KEY (`pkProcessID`);

--
-- Indexes for table `operationtemplate`
--
ALTER TABLE `operationtemplate`
  ADD PRIMARY KEY (`pkOperationTemplateID`);

--
-- Indexes for table `operationtreatment`
--
ALTER TABLE `operationtreatment`
  ADD PRIMARY KEY (`pkOperationID`);

--
-- Indexes for table `operationtype`
--
ALTER TABLE `operationtype`
  ADD PRIMARY KEY (`pkOperationTypeID`);

--
-- Indexes for table `opmachineinspect`
--
ALTER TABLE `opmachineinspect`
  ADD PRIMARY KEY (`fkOperationID`,`fkMachineID`,`fkInspectionID`);

--
-- Indexes for table `opmachineprogram`
--
ALTER TABLE `opmachineprogram`
  ADD PRIMARY KEY (`fkOperationID`,`fkMachineID`,`fkProgramNumber`);

--
-- Indexes for table `opsection`
--
ALTER TABLE `opsection`
  ADD PRIMARY KEY (`pkOpSectionID`),
  ADD KEY `SectionToProcedure_idx` (`fkOperatingProcedureID`);

--
-- Indexes for table `opsubsection`
--
ALTER TABLE `opsubsection`
  ADD PRIMARY KEY (`pkOpSubSectionID`),
  ADD KEY `SubSectionToSection_idx` (`fkOpSectionID`);

--
-- Indexes for table `optmpltmachineprogram`
--
ALTER TABLE `optmpltmachineprogram`
  ADD PRIMARY KEY (`pkMachineProgramID`);

--
-- Indexes for table `orderdoc`
--
ALTER TABLE `orderdoc`
  ADD PRIMARY KEY (`pkOrderDocID`);

--
-- Indexes for table `orderitem`
--
ALTER TABLE `orderitem`
  ADD PRIMARY KEY (`pkItemID`);

--
-- Indexes for table `ourapproval`
--
ALTER TABLE `ourapproval`
  ADD PRIMARY KEY (`pkApprovalID`);

--
-- Indexes for table `outvoice`
--
ALTER TABLE `outvoice`
  ADD PRIMARY KEY (`pkInvoiceID`);

--
-- Indexes for table `outvoicecredit`
--
ALTER TABLE `outvoicecredit`
  ADD PRIMARY KEY (`pkCreditID`);

--
-- Indexes for table `overheadgroup`
--
ALTER TABLE `overheadgroup`
  ADD PRIMARY KEY (`pkGroupID`);

--
-- Indexes for table `overheads`
--
ALTER TABLE `overheads`
  ADD PRIMARY KEY (`pkItemID`);

--
-- Indexes for table `partreviewtask`
--
ALTER TABLE `partreviewtask`
  ADD PRIMARY KEY (`pkTaskID`);

--
-- Indexes for table `parttemplate`
--
ALTER TABLE `parttemplate`
  ADD PRIMARY KEY (`pkPartTemplateID`);

--
-- Indexes for table `parttemplatenote`
--
ALTER TABLE `parttemplatenote`
  ADD PRIMARY KEY (`pkPartTemplateNoteID`);

--
-- Indexes for table `pause`
--
ALTER TABLE `pause`
  ADD PRIMARY KEY (`pkPauseID`);

--
-- Indexes for table `pausereason`
--
ALTER TABLE `pausereason`
  ADD PRIMARY KEY (`pkReasonID`);

--
-- Indexes for table `pocomment`
--
ALTER TABLE `pocomment`
  ADD PRIMARY KEY (`pkPOCommentID`);

--
-- Indexes for table `poinvoice`
--
ALTER TABLE `poinvoice`
  ADD PRIMARY KEY (`fkInvoiceID`,`fkItemID`);

--
-- Indexes for table `pon`
--
ALTER TABLE `pon`
  ADD PRIMARY KEY (`pkPOnID`);

--
-- Indexes for table `ponote`
--
ALTER TABLE `ponote`
  ADD PRIMARY KEY (`pkNoteID`);

--
-- Indexes for table `posupplier`
--
ALTER TABLE `posupplier`
  ADD PRIMARY KEY (`fkPurchaseOrderID`);

--
-- Indexes for table `procedure`
--
ALTER TABLE `procedure`
  ADD PRIMARY KEY (`pkProcedureID`);

--
-- Indexes for table `proceduretraining`
--
ALTER TABLE `proceduretraining`
  ADD PRIMARY KEY (`fkUserID`);

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`pkRateID`);

--
-- Indexes for table `regrind`
--
ALTER TABLE `regrind`
  ADD PRIMARY KEY (`pkRegrindID`);

--
-- Indexes for table `rejectgrb`
--
ALTER TABLE `rejectgrb`
  ADD PRIMARY KEY (`pkRejectID`);

--
-- Indexes for table `rejectinvoice`
--
ALTER TABLE `rejectinvoice`
  ADD PRIMARY KEY (`pkRejectID`);

--
-- Indexes for table `remark`
--
ALTER TABLE `remark`
  ADD PRIMARY KEY (`pkRemarkID`);

--
-- Indexes for table `requirement`
--
ALTER TABLE `requirement`
  ADD PRIMARY KEY (`pkRequirementID`),
  ADD KEY `OpSection_idx` (`fkOpSectionID`);

--
-- Indexes for table `rfqn`
--
ALTER TABLE `rfqn`
  ADD PRIMARY KEY (`pkRFQnID`);

--
-- Indexes for table `rfqorder`
--
ALTER TABLE `rfqorder`
  ADD PRIMARY KEY (`pkRFQOID`);

--
-- Indexes for table `rfqosupplier`
--
ALTER TABLE `rfqosupplier`
  ADD PRIMARY KEY (`fkRFQOID`,`fkSupplierID`);

--
-- Indexes for table `rfqprogress`
--
ALTER TABLE `rfqprogress`
  ADD PRIMARY KEY (`pkProgressID`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`pkRoleID`);

--
-- Indexes for table `safetytask`
--
ALTER TABLE `safetytask`
  ADD PRIMARY KEY (`pkTaskID`);

--
-- Indexes for table `safetytaskcheck`
--
ALTER TABLE `safetytaskcheck`
  ADD PRIMARY KEY (`pkTaskCheckID`);

--
-- Indexes for table `screen`
--
ALTER TABLE `screen`
  ADD PRIMARY KEY (`pkScreenID`);

--
-- Indexes for table `screenrole`
--
ALTER TABLE `screenrole`
  ADD PRIMARY KEY (`fkScreenID`,`fkRoleID`),
  ADD KEY `Screen_idx` (`fkScreenID`),
  ADD KEY `Role_idx` (`fkRoleID`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`pkServiceID`);

--
-- Indexes for table `shape`
--
ALTER TABLE `shape`
  ADD PRIMARY KEY (`pkShapeID`);

--
-- Indexes for table `specification`
--
ALTER TABLE `specification`
  ADD PRIMARY KEY (`pkSpecificationID`);

--
-- Indexes for table `spud`
--
ALTER TABLE `spud`
  ADD PRIMARY KEY (`pkID`);

--
-- Indexes for table `stdconformitynote`
--
ALTER TABLE `stdconformitynote`
  ADD PRIMARY KEY (`pkConformityNoteID`);

--
-- Indexes for table `storearea`
--
ALTER TABLE `storearea`
  ADD PRIMARY KEY (`pkStoreAreaID`);

--
-- Indexes for table `storelocation`
--
ALTER TABLE `storelocation`
  ADD PRIMARY KEY (`pkLocationID`);

--
-- Indexes for table `sundry`
--
ALTER TABLE `sundry`
  ADD PRIMARY KEY (`pkSundryID`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`pkSupplierID`);

--
-- Indexes for table `supplieradditionaltime`
--
ALTER TABLE `supplieradditionaltime`
  ADD PRIMARY KEY (`pkSupplierAdditionalID`);

--
-- Indexes for table `supplierapproval`
--
ALTER TABLE `supplierapproval`
  ADD PRIMARY KEY (`fkSupplierID`,`fkApprovalID`);

--
-- Indexes for table `suppliercontact`
--
ALTER TABLE `suppliercontact`
  ADD PRIMARY KEY (`pkContactID`);

--
-- Indexes for table `suppliernadcap`
--
ALTER TABLE `suppliernadcap`
  ADD PRIMARY KEY (`fkSupplierID`,`fkApprovalID`);

--
-- Indexes for table `suppliernote`
--
ALTER TABLE `suppliernote`
  ADD PRIMARY KEY (`pkSupplierNoteID`);

--
-- Indexes for table `supplierpersonnel`
--
ALTER TABLE `supplierpersonnel`
  ADD PRIMARY KEY (`pkSupplierPersonnelID`);

--
-- Indexes for table `suppliertreatment`
--
ALTER TABLE `suppliertreatment`
  ADD PRIMARY KEY (`fkSupplierID`,`fkOperationID`),
  ADD KEY `SupplierToTreatment_idx` (`fkSupplierID`),
  ADD KEY `Treatment_idx` (`fkOperationID`);

--
-- Indexes for table `supplycondition`
--
ALTER TABLE `supplycondition`
  ADD PRIMARY KEY (`pkSupplyConditionID`);

--
-- Indexes for table `templateapprovalcomments`
--
ALTER TABLE `templateapprovalcomments`
  ADD PRIMARY KEY (`pkTemplateApprovalCommentID`);

--
-- Indexes for table `templatemachining`
--
ALTER TABLE `templatemachining`
  ADD PRIMARY KEY (`fkOperationTemplateID`);

--
-- Indexes for table `templatematerial`
--
ALTER TABLE `templatematerial`
  ADD PRIMARY KEY (`pkOperationTemplateID`,`fkMaterialSpecID`);

--
-- Indexes for table `templatenote`
--
ALTER TABLE `templatenote`
  ADD PRIMARY KEY (`pkTemplateNoteID`);

--
-- Indexes for table `templatepocomments`
--
ALTER TABLE `templatepocomments`
  ADD PRIMARY KEY (`pkTemplatePOCommentID`);

--
-- Indexes for table `templateprocess`
--
ALTER TABLE `templateprocess`
  ADD PRIMARY KEY (`pkProcessID`);

--
-- Indexes for table `templateremark`
--
ALTER TABLE `templateremark`
  ADD PRIMARY KEY (`pkTemplateRemarkID`);

--
-- Indexes for table `templatetreatment`
--
ALTER TABLE `templatetreatment`
  ADD PRIMARY KEY (`pkOperationTemplateID`);

--
-- Indexes for table `todo`
--
ALTER TABLE `todo`
  ADD PRIMARY KEY (`pkTodoID`);

--
-- Indexes for table `tool`
--
ALTER TABLE `tool`
  ADD PRIMARY KEY (`pkToolID`);

--
-- Indexes for table `toolsubtype`
--
ALTER TABLE `toolsubtype`
  ADD PRIMARY KEY (`pkSubTypeID`);

--
-- Indexes for table `tooltype`
--
ALTER TABLE `tooltype`
  ADD PRIMARY KEY (`pkTypeID`),
  ADD UNIQUE KEY `pkToolTypeID_UNIQUE` (`pkTypeID`);

--
-- Indexes for table `trainingcourse`
--
ALTER TABLE `trainingcourse`
  ADD PRIMARY KEY (`pkCourseID`);

--
-- Indexes for table `treatmentlist`
--
ALTER TABLE `treatmentlist`
  ADD PRIMARY KEY (`pkTreatmentID`);

--
-- Indexes for table `treatmentnote`
--
ALTER TABLE `treatmentnote`
  ADD PRIMARY KEY (`pkNoteID`);

--
-- Indexes for table `unitofmeasure`
--
ALTER TABLE `unitofmeasure`
  ADD PRIMARY KEY (`pkUOMID`);

--
-- Indexes for table `unittype`
--
ALTER TABLE `unittype`
  ADD PRIMARY KEY (`pkUnitTypeID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`pkUserID`);

--
-- Indexes for table `usercourse`
--
ALTER TABLE `usercourse`
  ADD PRIMARY KEY (`fkUserID`,`fkCourseID`);

--
-- Indexes for table `userdept`
--
ALTER TABLE `userdept`
  ADD PRIMARY KEY (`fkUserID`,`fkDeptID`),
  ADD KEY `User_idx` (`fkUserID`),
  ADD KEY `Role_idx` (`fkDeptID`);

--
-- Indexes for table `userrole`
--
ALTER TABLE `userrole`
  ADD PRIMARY KEY (`fkUserID`,`fkRoleID`),
  ADD KEY `User_idx` (`fkUserID`),
  ADD KEY `Role_idx` (`fkRoleID`);

--
-- Indexes for table `websiteaccess`
--
ALTER TABLE `websiteaccess`
  ADD PRIMARY KEY (`pkWebsiteID`);

--
-- Indexes for table `wodocument`
--
ALTER TABLE `wodocument`
  ADD PRIMARY KEY (`pkWODocumentID`);

--
-- Indexes for table `womaterialnote`
--
ALTER TABLE `womaterialnote`
  ADD PRIMARY KEY (`pkMaterialNoteID`);

--
-- Indexes for table `workorder`
--
ALTER TABLE `workorder`
  ADD PRIMARY KEY (`pkWorkOrderID`);

--
-- Indexes for table `workordernote`
--
ALTER TABLE `workordernote`
  ADD PRIMARY KEY (`pkWONoteID`);

--
-- Indexes for table `workorderremark`
--
ALTER TABLE `workorderremark`
  ADD PRIMARY KEY (`fkWorkOrderID`,`fkRemarkID`),
  ADD KEY `WorkOrderToRemark_idx` (`fkWorkOrderID`),
  ADD KEY `Remark_idx` (`fkRemarkID`);

--
-- Indexes for table `xcoordinate`
--
ALTER TABLE `xcoordinate`
  ADD PRIMARY KEY (`pkXCoordID`);

--
-- Indexes for table `ycoordinate`
--
ALTER TABLE `ycoordinate`
  ADD PRIMARY KEY (`pkYCoordID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acknowledgementnote`
--
ALTER TABLE `acknowledgementnote`
  MODIFY `pkAcknowNoteID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `actiontype`
--
ALTER TABLE `actiontype`
  MODIFY `pkActionTypeID` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `approval`
--
ALTER TABLE `approval`
  MODIFY `pkApprovalID` tinyint(4) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'chg to auto 5/2/16', AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `approvallevel`
--
ALTER TABLE `approvallevel`
  MODIFY `pkApprovalLevelID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `associatedpartdoc`
--
ALTER TABLE `associatedpartdoc`
  MODIFY `pkAPDID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `branchlocation`
--
ALTER TABLE `branchlocation`
  MODIFY `pkBranchID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `businessshutdown`
--
ALTER TABLE `businessshutdown`
  MODIFY `pkShutdownID` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `calibrationparameter`
--
ALTER TABLE `calibrationparameter`
  MODIFY `pkParameterID` tinyint(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `calibrationreading`
--
ALTER TABLE `calibrationreading`
  MODIFY `pkReadingID` smallint(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `pkCommentID` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `contingency`
--
ALTER TABLE `contingency`
  MODIFY `pkContingencyID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `courier`
--
ALTER TABLE `courier`
  MODIFY `pkCourierID` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `pkCustomerID` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '15/2/16 chgd to ai', AUTO_INCREMENT=3408;
--
-- AUTO_INCREMENT for table `customeradditionaltime`
--
ALTER TABLE `customeradditionaltime`
  MODIFY `pkAdditionalID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'medint 24/3/16', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customercontact`
--
ALTER TABLE `customercontact`
  MODIFY `pkContactID` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customernote`
--
ALTER TABLE `customernote`
  MODIFY `pkCustomerNoteID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'medint 24/3/16', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customerorder`
--
ALTER TABLE `customerorder`
  MODIFY `pkOrderID` mediumint(9) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customerordernote`
--
ALTER TABLE `customerordernote`
  MODIFY `pkOrderNoteID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customerpersonnel`
--
ALTER TABLE `customerpersonnel`
  MODIFY `pkCustomerPersonnelID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `deliverycomment`
--
ALTER TABLE `deliverycomment`
  MODIFY `pkDeliveryCommentID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `deliverycontact`
--
ALTER TABLE `deliverycontact`
  MODIFY `pkDeliveryContactID` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `deliverypoint`
--
ALTER TABLE `deliverypoint`
  MODIFY `pkDeliveryPointID` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `deviation`
--
ALTER TABLE `deviation`
  MODIFY `pkDeviationID` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `deviationtype`
--
ALTER TABLE `deviationtype`
  MODIFY `pkDeviationTypeID` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `device`
--
ALTER TABLE `device`
  MODIFY `pkDeviceID` tinyint(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `deviceuse`
--
ALTER TABLE `deviceuse`
  MODIFY `pkUseID` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dispatch`
--
ALTER TABLE `dispatch`
  MODIFY `pkDispatchID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '22/3/16 ai', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `documentdroptype`
--
ALTER TABLE `documentdroptype`
  MODIFY `pkDocumentDropTypeID` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `drawing`
--
ALTER TABLE `drawing`
  MODIFY `pkDrawingID` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `engineeringtemplate`
--
ALTER TABLE `engineeringtemplate`
  MODIFY `pkEngineeringTemplateID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `exchangerate`
--
ALTER TABLE `exchangerate`
  MODIFY `pkExchangeRateID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `externalreject`
--
ALTER TABLE `externalreject`
  MODIFY `pkExternalRejectID` smallint(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fat`
--
ALTER TABLE `fat`
  MODIFY `pkFatID` tinyint(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fixture`
--
ALTER TABLE `fixture`
  MODIFY `pkFixtureID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `floorlocation`
--
ALTER TABLE `floorlocation`
  MODIFY `pkFloorLocationID` tinyint(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `goodsdocument`
--
ALTER TABLE `goodsdocument`
  MODIFY `pkDelDocID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `goodsin`
--
ALTER TABLE `goodsin`
  MODIFY `pkGRBID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `inspection`
--
ALTER TABLE `inspection`
  MODIFY `pkInspectionID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'chgd to auto 22/2/16. chgd to bigint', AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `inspectiontype`
--
ALTER TABLE `inspectiontype`
  MODIFY `pkTypeID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `inspectpause`
--
ALTER TABLE `inspectpause`
  MODIFY `pkPauseID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `pkInvoiceID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lineitem`
--
ALTER TABLE `lineitem`
  MODIFY `pkLineItemID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'made auto 20/1/16', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `machine`
--
ALTER TABLE `machine`
  MODIFY `pkMachineID` tinyint(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `machinemaintenance`
--
ALTER TABLE `machinemaintenance`
  MODIFY `pkMaintenanceID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `machineset`
--
ALTER TABLE `machineset`
  MODIFY `pkSetID` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `machiningtype`
--
ALTER TABLE `machiningtype`
  MODIFY `pkMachiningTypeID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'chgd ai 2/3/16', AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `material`
--
ALTER TABLE `material`
  MODIFY `pkMaterialID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `materialspec`
--
ALTER TABLE `materialspec`
  MODIFY `pkMaterialSpecID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `materialtype`
--
ALTER TABLE `materialtype`
  MODIFY `pkMaterialTypeID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `nadcapapproval`
--
ALTER TABLE `nadcapapproval`
  MODIFY `pkApprovalID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `operation`
--
ALTER TABLE `operation`
  MODIFY `pkOperationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `operationapprovalcomments`
--
ALTER TABLE `operationapprovalcomments`
  MODIFY `pkApprovalCommentID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `operationnote`
--
ALTER TABLE `operationnote`
  MODIFY `pkOperationNoteID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `operationprocess`
--
ALTER TABLE `operationprocess`
  MODIFY `pkProcessID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `operationtemplate`
--
ALTER TABLE `operationtemplate`
  MODIFY `pkOperationTemplateID` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `operationtype`
--
ALTER TABLE `operationtype`
  MODIFY `pkOperationTypeID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `optmpltmachineprogram`
--
ALTER TABLE `optmpltmachineprogram`
  MODIFY `pkMachineProgramID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `orderdoc`
--
ALTER TABLE `orderdoc`
  MODIFY `pkOrderDocID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `orderitem`
--
ALTER TABLE `orderitem`
  MODIFY `pkItemID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'chgd to auto 29/1/16', AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ourapproval`
--
ALTER TABLE `ourapproval`
  MODIFY `pkApprovalID` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `outvoice`
--
ALTER TABLE `outvoice`
  MODIFY `pkInvoiceID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `outvoicecredit`
--
ALTER TABLE `outvoicecredit`
  MODIFY `pkCreditID` mediumint(9) NOT NULL AUTO_INCREMENT COMMENT 'update 22/1/16';
--
-- AUTO_INCREMENT for table `partreviewtask`
--
ALTER TABLE `partreviewtask`
  MODIFY `pkTaskID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `parttemplate`
--
ALTER TABLE `parttemplate`
  MODIFY `pkPartTemplateID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ai 5/4/16', AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `parttemplatenote`
--
ALTER TABLE `parttemplatenote`
  MODIFY `pkPartTemplateNoteID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pause`
--
ALTER TABLE `pause`
  MODIFY `pkPauseID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pausereason`
--
ALTER TABLE `pausereason`
  MODIFY `pkReasonID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pocomment`
--
ALTER TABLE `pocomment`
  MODIFY `pkPOCommentID` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'chgd from smallint 1/2/16', AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pon`
--
ALTER TABLE `pon`
  MODIFY `pkPOnID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ponote`
--
ALTER TABLE `ponote`
  MODIFY `pkNoteID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'chgd auto 27/7/16', AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `procedure`
--
ALTER TABLE `procedure`
  MODIFY `pkProcedureID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `pkRateID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `rejectgrb`
--
ALTER TABLE `rejectgrb`
  MODIFY `pkRejectID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rejectinvoice`
--
ALTER TABLE `rejectinvoice`
  MODIFY `pkRejectID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `remark`
--
ALTER TABLE `remark`
  MODIFY `pkRemarkID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfqn`
--
ALTER TABLE `rfqn`
  MODIFY `pkRFQnID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `rfqorder`
--
ALTER TABLE `rfqorder`
  MODIFY `pkRFQOID` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `rfqprogress`
--
ALTER TABLE `rfqprogress`
  MODIFY `pkProgressID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shape`
--
ALTER TABLE `shape`
  MODIFY `pkShapeID` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `spud`
--
ALTER TABLE `spud`
  MODIFY `pkID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stdconformitynote`
--
ALTER TABLE `stdconformitynote`
  MODIFY `pkConformityNoteID` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `storearea`
--
ALTER TABLE `storearea`
  MODIFY `pkStoreAreaID` tinyint(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sundry`
--
ALTER TABLE `sundry`
  MODIFY `pkSundryID` tinyint(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `pkSupplierID` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3505;
--
-- AUTO_INCREMENT for table `supplieradditionaltime`
--
ALTER TABLE `supplieradditionaltime`
  MODIFY `pkSupplierAdditionalID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `suppliercontact`
--
ALTER TABLE `suppliercontact`
  MODIFY `pkContactID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `suppliernote`
--
ALTER TABLE `suppliernote`
  MODIFY `pkSupplierNoteID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `supplierpersonnel`
--
ALTER TABLE `supplierpersonnel`
  MODIFY `pkSupplierPersonnelID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `supplycondition`
--
ALTER TABLE `supplycondition`
  MODIFY `pkSupplyConditionID` tinyint(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `templateapprovalcomments`
--
ALTER TABLE `templateapprovalcomments`
  MODIFY `pkTemplateApprovalCommentID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `templatenote`
--
ALTER TABLE `templatenote`
  MODIFY `pkTemplateNoteID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `templatepocomments`
--
ALTER TABLE `templatepocomments`
  MODIFY `pkTemplatePOCommentID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `templateprocess`
--
ALTER TABLE `templateprocess`
  MODIFY `pkProcessID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `templateremark`
--
ALTER TABLE `templateremark`
  MODIFY `pkTemplateRemarkID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ai 4/4/16';
--
-- AUTO_INCREMENT for table `todo`
--
ALTER TABLE `todo`
  MODIFY `pkTodoID` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tool`
--
ALTER TABLE `tool`
  MODIFY `pkToolID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `toolsubtype`
--
ALTER TABLE `toolsubtype`
  MODIFY `pkSubTypeID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tooltype`
--
ALTER TABLE `tooltype`
  MODIFY `pkTypeID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `trainingcourse`
--
ALTER TABLE `trainingcourse`
  MODIFY `pkCourseID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `treatmentlist`
--
ALTER TABLE `treatmentlist`
  MODIFY `pkTreatmentID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'chgd ai 2/3/16', AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `treatmentnote`
--
ALTER TABLE `treatmentnote`
  MODIFY `pkNoteID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `unitofmeasure`
--
ALTER TABLE `unitofmeasure`
  MODIFY `pkUOMID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `pkUserID` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'chgd 26/3/16', AUTO_INCREMENT=256;
--
-- AUTO_INCREMENT for table `websiteaccess`
--
ALTER TABLE `websiteaccess`
  MODIFY `pkWebsiteID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'chgd ai 15/3/16', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wodocument`
--
ALTER TABLE `wodocument`
  MODIFY `pkWODocumentID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `womaterialnote`
--
ALTER TABLE `womaterialnote`
  MODIFY `pkMaterialNoteID` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `workorder`
--
ALTER TABLE `workorder`
  MODIFY `pkWorkOrderID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `workordernote`
--
ALTER TABLE `workordernote`
  MODIFY `pkWONoteID` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `xcoordinate`
--
ALTER TABLE `xcoordinate`
  MODIFY `pkXCoordID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'chgd 9/3/16', AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ycoordinate`
--
ALTER TABLE `ycoordinate`
  MODIFY `pkYCoordID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
