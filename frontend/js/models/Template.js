var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Template(options) {
    this.PartNumber = '';
    this.PartIssue = '';
    this.Description = '';
    this.LeadDays = 0;
    this.Price = 0;
    this.DrawingNumber = '';
    this.Issue = '';
    this.ApprovalID = null;
    this.AssemblyQty = 0;
    this.PartTemplateID = null;

    BaseModel.call(this, options);
}

inherits(Template, BaseModel);

Template.prototype.setApprovalID = function(value) {
	this.ApprovalID = value;
};

Template.prototype.setPartTemplateID = function(value) {
	this.PartTemplateID = value;
};

Template.prototype.getPartNumber = function() {
	return this.PartNumber;
};

Template.prototype.getPartIssue = function() {
	return this.PartIssue;
};

Template.prototype.getDescription = function() {
	return this.Description;
};

Template.prototype.getLeadDays = function() {
	return this.LeadDays;
};

Template.prototype.getPrice = function() {
	return this.Price;
};

Template.prototype.getDrawingNumber = function() {
	return this.DrawingNumber;
};

Template.prototype.getIssue = function() {
	return this.Issue;
};

Template.prototype.getApprovalID = function() {
	return this.ApprovalID;
};

Template.prototype.getAssemblyQty = function() {
	return this.AssemblyQty;
};

Template.prototype.getPartTemplateID = function() {
	return this.PartTemplateID;
};

module.exports = Template;
