(function(app) {
    app.controller('ProductionScheduleMachineCtrl', ProductionScheduleMachineCtrl);

    ProductionScheduleMachineCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function ProductionScheduleMachineCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {
        var machineAlias;

        $scope.resetOperationStatus();

        // Conditional to active class
        $rootScope.machineParams = machineAlias = $stateParams.machine;

        /**
         * Sort Table
         */
        $scope.sortTable = [
            { title: 'WO No.', sortBy: 'WorkOrderID' },
            { title: 'CO No.', sortBy: 'OrderID' },
            { title: 'Part Number', sortBy: 'PartNumber' },
            { title: 'Qty', sortBy: 'Qty' },
            { title: 'Operation', sortBy: 'Operation' },
            { title: 'DD Start', sortBy: 'DDStart' },
            { title: 'Start', sortBy: 'StartDate' },
            { title: 'Finish', sortBy: 'Finish' },
            { title: 'DD Finish', sortBy: 'DDFinish' },
            { title: 'Resource', sortBy: 'Resource' },
            { title: 'Progress', sortBy: 'OpNo' },
        ];
        $scope.sortBy = $scope.sortTable[0].sortBy;
        $scope.changeSortBy = function(item) {
            $scope.sortBy = item.sortBy;
        };

        /**
         * get Owner Machine
         */
        if ($scope.ProductionSchedule === undefined) {
            $scope.getAllMachine();
        }
        $scope.$watch('ProductionSchedule', function(ProductionSchedule) {
            if (ProductionSchedule !== undefined) {
                $scope.OwnerMachine = [];
                ProductionSchedule.forEach(function(value, key) {
                    if (value.selected !== undefined) {
                        delete value.selected;
                    }
                    value.MachineAlias = value.Machine.replace(/\s+/g, '-').toLowerCase();
                    if (value.MachineAlias === machineAlias) {
                        $scope.OwnerMachine.push(value);
                    }
                });
            }
        });

        /**
         * the function working when click Progess a tag
         */
        $scope.selectItemByOpNo = function(OpNo, OwnerMachine) {
            // var ItemByOpNo = _.find(OwnerMachine, function(item) {
            //     return item.OpNo === OpNo;
            // });
            // if (ItemByOpNo !== undefined) {
            //     if (ItemByOpNo.selected !== undefined) {
            //         delete ItemByOpNo.selected;
            //     } else {
            //         ItemByOpNo.selected = true;
            //     }
            // }
        };
    }

})(angular.module('app.components.department.production-schedule.machine', []));
