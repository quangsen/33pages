var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Staff(options) {
    this.Address1 = '';
    this.Address2 = '';
    this.Address3 = '';
    this.Address4 = '';
    this.ChargeRate = 0;
    this.Email = '';
    this.Employed = 0;
    this.Extension = 0;
    this.FirstAid = 0;
    this.FirstName = '';
    this.ForkOperator = 0;
    this.HomePhone = '';
    this.Hours = 0;
    this.LastName = '';
    this.LeaveDays = 0;
    this.MobilePhone = '';
    this.POLimit = 0;
    this.PayrollNumber = '';
    this.Postcode = '';
    this.ReportsTo = 0;
    this.Salary = 0;
    this.Security = 1;
    this.SkillLevel = 0;
    this.SpendLimit = 0;

    BaseModel.call(this, options);
}

inherits(Staff, BaseModel);

Staff.prototype.getAddress1 = function() {
    return this.Address1;
};

Staff.prototype.getAddress2 = function() {
    return this.Address2;
};

Staff.prototype.getAddress3 = function() {
    return this.Address3;
};

Staff.prototype.getAddress4 = function() {
    return this.Address4;
};

Staff.prototype.getChargeRate = function() {
    return parseFloat(this.ChargeRate);
};

Staff.prototype.getEmail = function() {
    return this.Email;
};

Staff.prototype.getEmployed = function() {
    return parseInt(this.Employed);
};

Staff.prototype.getExtension = function() {
    return parseInt(this.Extension);
};

Staff.prototype.getFirstAid = function() {
    return parseInt(this.FirstAid);
};

Staff.prototype.getFirstName = function() {
    return this.FirstName;
};

Staff.prototype.getForkOperator = function() {
    return parseInt(this.ForkOperator);
};

Staff.prototype.getHomePhone = function() {
    return this.HomePhone;
};

Staff.prototype.getHours = function() {
    return parseInt(this.Hours);
};

Staff.prototype.getLastName = function() {
    return this.LastName;
};

Staff.prototype.getLeaveDays = function() {
    return parseInt(this.LeaveDays);
};

Staff.prototype.getMobilePhone = function() {
    return this.MobilePhone;
};

Staff.prototype.getPOLimit = function() {
    return parseInt(this.POLimit);
};

Staff.prototype.getPayrollNumber = function() {
    return this.PayrollNumber;
};

Staff.prototype.getPostcode = function() {
    return this.Postcode;
};

Staff.prototype.getReportsTo = function() {
    return parseInt(this.ReportsTo);
};

Staff.prototype.getSalary = function() {
    return parseInt(this.Salary);
};

Staff.prototype.getSecurity = function() {
    return parseInt(this.Security);
};

Staff.prototype.getSkillLevel = function() {
    return parseInt(this.SkillLevel);
};

Staff.prototype.getSpendLimit = function() {
    return parseInt(this.SpendLimit);
};

module.exports = Staff;
