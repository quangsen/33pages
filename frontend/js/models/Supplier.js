var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Supplier(options) {
    this.Address1 = '';
    this.Address2 = '';
    this.Address3 = '';
    this.Address4 = '';
    this.Alias = '';
    this.CalibrationSupplier = 0;
    this.Email = '';
    this.Facsimile = '';
    this.MaterialSupplier = 0;
    this.MinOrderCharge = 0;
    this.MiscSupplier = 0;
    this.PaymentTerms = '';
    this.Phone = '';
    this.Postcode = '';
    this.Supplier = '';
    this.ToolingSupplier = 0;
    this.TreatmentSupplier = 0;
    this.Website = '';
    this.Current = 0;

    BaseModel.call(this, options);
}

inherits(Supplier, BaseModel);

Supplier.prototype.setPaymentTerms = function(value) {
    this.PaymentTerms = value;
};

Supplier.prototype.getAddress1 = function() {
    return this.Address1;
};

Supplier.prototype.getAddress2 = function() {
    return this.Address2;
};

Supplier.prototype.getAddress3 = function() {
    return this.Address3;
};

Supplier.prototype.getAddress4 = function() {
    return this.Address4;
};

Supplier.prototype.getAlias = function() {
    return this.Alias;
};

Supplier.prototype.getCalibrationSupplier = function() {
    return this.CalibrationSupplier;
};

Supplier.prototype.getEmail = function() {
    return this.Email;
};

Supplier.prototype.getFacsimile = function() {
    return this.Facsimile;
};

Supplier.prototype.getMaterialSupplier = function() {
    return this.MaterialSupplier;
};

Supplier.prototype.getMinOrderCharge = function() {
    return this.MinOrderCharge;
};

Supplier.prototype.getMiscSupplier = function() {
    return this.MiscSupplier;
};

Supplier.prototype.getPaymentTerms = function() {
    return this.PaymentTerms;
};

Supplier.prototype.getPhone = function() {
    return this.Phone;
};

Supplier.prototype.getPostcode = function() {
    return this.Postcode;
};

Supplier.prototype.getSupplier = function() {
    return this.Supplier;
};

Supplier.prototype.getToolingSupplier = function() {
    return this.ToolingSupplier;
};

Supplier.prototype.getTreatmentSupplier = function() {
    return this.TreatmentSupplier;
};

Supplier.prototype.getWebsite = function() {
    return this.Website;
};

Supplier.prototype.getCurrent = function() {
    return this.Current;
};

module.exports = Supplier;
