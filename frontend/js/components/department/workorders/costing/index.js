(function(app) {
    app.controller('WorkOrderCostingCtrl', WorkOrderCostingCtrl);
    WorkOrderCostingCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API'];

    function WorkOrderCostingCtrl($rootScope, $scope, $stateParams, API) {
        var WorkOrderID = $rootScope.WorkOrderID = $stateParams.WorkOrderID;
        if (WorkOrderID !== undefined && WorkOrderID !== '') {
            if ($scope.WorkOrderDetail === undefined) {
                $scope.getWorkOrderInfo(WorkOrderID);
            }

            /**
             * get Work Order Labour
             */
            API.procedure.post('ProcGetWorkOrderLabour', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    var PartTotalSet = 0;
                    var PartTotalRun = 0;
                    var GrandTime = 0;
                    var GrandCost = 0;
                    var GrandQty = null;
                    var WOTime = 0;
                    var WOCost = 0;
                    var WOQty = 0;
                    var TotalTreatments = 0;

                    console.log(result);
                    var MinQty = _.minBy(result, function(item) {
                        if (!_.isNil(item.Qty)) {
                            return item;
                        }
                    });
                    MinQty = parseInt(MinQty.Qty);
                    console.log(MinQty);

                    result.forEach(function(value, key) {
                        var pattern = /\d+/;
                        if (pattern.test(value.SetTime)) {
                            value.SetTime = parseFloat(value.SetTime);
                        }
                        if (pattern.test(value.RunTime)) {
                            value.RunTime = parseFloat(value.RunTime);
                        }
                        if (pattern.test(value.TimeTaken)) {
                            value.TimeTaken = parseFloat(value.TimeTaken);
                        }
                        if (value.SetTime) {
                            if (value.SetTime > 0) {
                                value.SetTime = $rootScope.timeFormat(value.SetTime, 'h', -1);
                            }
                            PartTotalSet += parseFloat(value.SetTime);
                        }
                        if (value.RunTime) {
                            if (value.RunTime > 0) {
                                value.RunTime = $rootScope.timeFormat(value.RunTime, 'h', -1);
                            }
                            PartTotalRun += parseFloat(value.RunTime);
                        }
                        if (value.TimeTaken) {
                            result[key].DisplayTimeTaken = $rootScope.timeFormat(value.TimeTaken, 'auto', -1, true);
                            value.TimeTaken = $rootScope.timeFormat(value.TimeTaken, 'auto', -1);
                            GrandTime += parseFloat(value.TimeTaken);
                        }
                        if (value.Cost) {
                            GrandCost += parseFloat(value.Cost);
                        }
                        if (value.Qty !== '') {
                            if (GrandQty === null) {
                                GrandQty = parseInt(value.Qty);
                            } else {
                                if (GrandQty > parseInt(value.Qty)) {
                                    GrandQty = parseInt(value.Qty);
                                }
                            }
                        }

                        if (!_.isNil(value.QuantityReqd)) {
                            WOQty += parseInt(value.QuantityReqd);
                        }

                        if (!_.isNil(value.TreatmentCost)) {
                            TotalTreatments += parseFloat(value.TreatmentCost);
                        }
                    });

                    $scope.PartTotalSet = PartTotalSet;
                    $scope.PartTotalRun = PartTotalRun;

                    $scope.GrandTime = $rootScope.timeFormat(GrandTime, 'auto', -1, true);
                    // format 12.08
                    $scope.GrandCost = Math.round(GrandCost * 100) / 100;
                    $scope.GrandQty = GrandQty;

                    $scope.PerPartTime = $rootScope.timeFormat(GrandTime / GrandQty, 'auto', -1, true);
                    $scope.PerPartCost = Math.round((GrandCost / GrandQty) * 100) / 100;

                    $scope.WOQty = parseInt(result[0].QuantityReqd);
                    $scope.WOTime = $rootScope.timeFormat((GrandTime / MinQty) * $scope.WOQty, 'auto', -1, true);
                    $scope.WOCost = Math.round(((GrandCost / MinQty) * $scope.WOQty) * 100) / 100;
                    $scope.TotalTreatments = TotalTreatments;
                    $scope.WorkOrderLabour = result;
                })
                .catch(function(error) {
                    throw error;
                });
        } else {
            var state = 'department.workorders';
            $rootScope.redirectDept(state);
            return false;
        }

    }
})(angular.module('app.components.department.workorders.costing', []));
