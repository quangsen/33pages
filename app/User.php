<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $table      = 'user';
    protected $primaryKey = 'pkUserID';
    public $timestamps    = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'pkUserID', 'UName', 'Email', 'pwd', 'FirstName', 'LastName', 'Address1', 'Address2', 'Address3', 'Address4', 'Postcode', 'HomePhone', 'MobilePhone', 'ChargeRate', 'SkillLevel', 'Employed', 'Extension', 'ReportsTo', 'POLimit', 'SpendLimit',
    // ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'pwd', 'remember_token',
    ];
}
