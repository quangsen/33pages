// FO / Admin
require('./home/');
require('./admin-reference/');
require('./customer/');
require('./customer-order/');
require('./workorders/');

// Quality Dept
require('./inspection-schedule/');
require('./remarks-comments/');

// Stores Dept
require('./goods-in/');
require('./material-issue/');
require('./delivery-despatch/');
require('./store-admin/');

// Production Dept
require('./production-schedule/');
require('./machine-control/');
require('./wo-detail/');

// Engineering Dept
require('./machine-list/');

// Finance - HR Dept
require('./staff-room/');
require('./finance-admin/');

// Purchasing Dept
require('./purchase-order/');
require('./supply-chain/');

// Planning / Estimating
require('./operations-treatment-admin/');
require('./part-control/');
require('./part-history/');
require('./engineering-template/');

(function(app) {
    app.controller('DepartmentCtrl', DepartmentCtrl);
    DepartmentCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'Notification', 'API'];

    function DepartmentCtrl($rootScope, $scope, $state, $stateParams, Notification, API) {
        var DeptID = $stateParams.DeptID;

        // Front Office
        $scope.LeftMenu = [{
            state: 'department.admin_reference',
            title: 'Admin references'
        }, {
            state: 'department.customer.main',
            title: 'Customer Center'
        }, {
            state: 'department.workorders',
            title: 'Work Orders'
        }, {
            state: 'department.customer-order',
            title: 'Customer Orders'
        }, {
            state: 'department.purchase-order',
            title: 'Purchase Order'
        }, ];

        // Production Dept
        if (DeptID == "1") {
            $scope.LeftMenu = [{
                state: 'department.production-schedule.material-issue',
                title: 'Production Schedule'
            }, {
                state: 'department.machine-control',
                title: 'Machine Control'
            }, ];
        }

        // Store Dept
        if (DeptID == "2") {
            $scope.LeftMenu = [{
                state: 'department.goods-in',
                title: 'Goods In'
            }, {
                state: 'department.material-issue',
                title: 'Material Issue'
            }, {
                state: 'department.delivery-despatch',
                title: 'Delivery Despatch'
            }, {
                state: 'department.store-admin',
                title: 'Admin'
            }, ];
        }

        // Engineering Dept
        if (DeptID == "3") {
            $scope.LeftMenu = [{
                state: 'department.machine-list',
                title: 'Machine List'
            }, ];
        }

        // Purchasing Dept
        if (DeptID == "4") {
            $scope.LeftMenu = [{
                state: 'department.purchase-order',
                title: 'Purchase Order'
            }, {
                state: 'department.supply-chain.main',
                title: 'Supply Chain'
            }, ];
        }

        // Quality Dept
        if (DeptID == "5") {
            $scope.LeftMenu = [{
                state: 'department.inspection-schedule.main',
                title: 'Inspection Schedule'
            }, {
                state: 'department.remarks-comments',
                title: 'Remarks Comments'
            }, ];
        }

        // Planning / Estimating
        if (DeptID == "6") {
            $scope.LeftMenu = [{
                state: 'department.production-schedule.material-issue',
                title: 'Production Schedule'
            }, {
                state: 'department.operations-treatment-admin',
                title: 'Admin'
            }, {
                state: 'department.part-control.part-list',
                title: 'Part control'
            }, ];
        }

        // Finance - HR Dept
        if (DeptID == "8") {
            $scope.LeftMenu = [{
                state: 'department.staff-room',
                title: 'Staff Room'
            }, {
                state: 'department.finance-admin',
                title: 'Admin'
            }, ];
        }

        $scope.redirectHome = function() {
            $state.go('department.home');
        };

    }
})(angular.module('app.components.department', [
    'app.template',
    'app.components.department.home',
    'app.components.department.admin-reference',
    'app.components.department.customer',
    'app.components.department.customer-order',
    'app.components.department.purchase-order',
    'app.components.department.workorders',
    'app.components.department.inspection-schedule',
    'app.components.department.remarks-comments',
    'app.components.department.goods-in',
    'app.components.department.material-issue',
    'app.components.department.delivery-despatch',
    'app.components.department.store-admin',
    'app.components.department.production-schedule',
    'app.components.department.wo-detail',
    'app.components.department.machine-control',
    'app.components.department.machine-list',
    'app.components.department.staff-room',
    'app.components.department.finance-admin',
    'app.components.department.supply-chain',
    'app.components.department.operations-treatment-admin',
    'app.components.department.part-control',
    'app.components.department.part-history',
    'app.components.department.engineering-template',
]));
