var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Customer(options) {
    this.pkCustomerID = '';
    this.CustomerSageCode = '';
    this.CustomerName = '';
    this.Alias = '';
    this.Address1 = '';
    this.Address2 = '';
    this.Address3 = '';
    this.Address4 = '';
    this.Postcode = '';
    this.Phone = '';
    this.Facsimile = '';
    this.Website = '';
    this.Email = '';
    this.InvAddress1 = '';
    this.InvAddress2 = '';
    this.InvAddress3 = '';
    this.InvAddress4 = '';
    this.InvPostcode = '';
    this.VATRate = 0;
    this.FDApproval = 0;
    this.MDApproval = 0;
    this.GMApproval = 0;
    this.CreditLimit = 0;
    this.MarkUpQuote = 0;
    this.PaymentTerms = null;
    this.PaymentRunFrequency = null;
    this.fkApprovalID = null;
    this.DeliveryCharge = 0;
    this.FinanceCode = '';
    this.IrishCurrency = 0;
    this.USCurrency = 0;
    this.EuroCurrency = 0;
    this.Day1Open = '';
    this.Day1Close = '';
    this.Day2Open = '';
    this.Day2Close = '';
    this.Day3Open = '';
    this.Day3Close = '';
    this.Day4Open = '';
    this.Day4Close = '';
    this.Day5Open = '';
    this.Day5Close = '';
    this.Day6Open = '';
    this.Day6Close = '';
    this.Day7Open = '';
    this.Day7Close = '';
    this.Deleted = 0;
    this.FAO = '';
    this.DeliveryGrace = 0;

    BaseModel.call(this, options);
}

inherits(Customer, BaseModel);

Customer.prototype.setOptions = function(options) {
    for (var key in options) {
        this[key] = options[key];
    }
    return this;
};

Customer.prototype.getCustomerId = function() {
    return this.pkCustomerID;
};

Customer.prototype.getCustomerSageCode = function() {
    return this.CustomerSageCode;
};

Customer.prototype.getCustomerName = function() {
    return this.CustomerName;
};

Customer.prototype.getAlias = function() {
    return this.Alias;
};

Customer.prototype.getAddress1 = function() {
    return this.Address1;
};

Customer.prototype.getAddress2 = function() {
    return this.Address2;
};

Customer.prototype.getAddress3 = function() {
    return this.Address3;
};

Customer.prototype.getAddress4 = function() {
    return this.Address4;
};

Customer.prototype.getPostcode = function() {
    return this.Postcode;
};

Customer.prototype.getPhone = function() {
    return this.Phone;
};

Customer.prototype.getFacsimile = function() {
    return this.Facsimile;
};

Customer.prototype.getWebsite = function() {
    return this.Website;
};

Customer.prototype.getEmail = function() {
    return this.Email;
};

Customer.prototype.getInvAddress1 = function() {
    return this.InvAddress1;
};

Customer.prototype.getInvAddress2 = function() {
    return this.InvAddress2;
};

Customer.prototype.getInvAddress3 = function() {
    return this.InvAddress3;
};

Customer.prototype.getInvAddress4 = function() {
    return this.InvAddress4;
};

Customer.prototype.getInvPostcode = function() {
    return this.InvPostcode;
};

Customer.prototype.getFDApproval = function() {
    return this.FDApproval;
};

Customer.prototype.getMDApproval = function() {
    return this.MDApproval;
};

Customer.prototype.getGMApproval = function() {
    return this.GMApproval;
};

Customer.prototype.getCreditLimit = function() {
    return this.CreditLimit;
};

Customer.prototype.getMarkUpQuote = function() {
    return this.MarkUpQuote;
};

Customer.prototype.getPaymentTerms = function() {
    return this.PaymentTerms;
};

Customer.prototype.getPaymentRunFrequency = function() {
    return this.PaymentRunFrequency;
};

Customer.prototype.getFkApprovalID = function() {
    return this.fkApprovalID;
};

Customer.prototype.getDeliveryCharge = function() {
    return this.DeliveryCharge;
};

Customer.prototype.getFinanceCode = function() {
    return this.FinanceCode;
};

Customer.prototype.getIrishCurrency = function() {
    return this.IrishCurrency;
};

Customer.prototype.getUSCurrency = function() {
    return this.USCurrency;
};

Customer.prototype.getEuroCurrency = function() {
    return this.EuroCurrency;
};

Customer.prototype.getDay1Open = function() {
    return this.Day1Open;
};

Customer.prototype.getDay1Close = function() {
    return this.Day1Close;
};

Customer.prototype.getDay2Open = function() {
    return this.Day2Open;
};

Customer.prototype.getDay2Close = function() {
    return this.Day2Close;
};

Customer.prototype.getDay3Open = function() {
    return this.Day3Open;
};

Customer.prototype.getDay3Close = function() {
    return this.Day3Close;
};

Customer.prototype.getDay4Open = function() {
    return this.Day4Open;
};

Customer.prototype.getDay4Close = function() {
    return this.Day4Close;
};

Customer.prototype.getDay5Open = function() {
    return this.Day5Open;
};

Customer.prototype.getDay5Close = function() {
    return this.Day5Close;
};

Customer.prototype.getDay6Open = function() {
    return this.Day6Open;
};

Customer.prototype.getDay6Close = function() {
    return this.Day6Close;
};

Customer.prototype.getDay7Open = function() {
    return this.Day7Open;
};

Customer.prototype.getDay7Close = function() {
    return this.Day7Close;
};

Customer.prototype.getDeleted = function() {
    return this.Deleted;
};


Customer.prototype.setPaymentTerms = function(value) {
    this.PaymentTerms = value;
    return this;
};


Customer.prototype.setPaymentRunFrequency = function(value) {
    this.PaymentRunFrequency = value;
    return this;
};

Customer.prototype.setApprovalLevel = function(value) {
    this.fkApprovalID = value;
    return this;
};

module.exports = Customer;
