require('./main/');
require('./supply-contact/');
require('./supply-order-history/');
require('./supply-performance/');

var Supplier = require('../../../models/Supplier');

(function(app) {

    app.controller('SupplyChainCtrl', SupplyChainCtrl);
    SupplyChainCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function SupplyChainCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {
        $scope.SupplierDetail = new Supplier();

        $scope.getSupplyInfo = function(SupplierID) {
            
            API.procedure.post('ProcGetSupplierDetail', {
                    SupplierID: SupplierID
                })
                .then(function(result) {
                    $scope.SupplierDetail = new Supplier(result[0]);

                    // Selected Payment term drop down
                    $scope.PaymentTerms = _.find($rootScope.ListPaymentTerms, function(item) {
                        return parseInt(item.value) === parseInt($scope.SupplierDetail.PaymentTerms);
                    });
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * Save Or Update Supplier
         */
        $scope.SaveOrUpdateSupplier = function(SupplierDetail) {
            if ($stateParams === null || $stateParams === undefined) {

                // Add Supplier
                API.procedure.post('ProcAddNewSupplier', {
                        UserID: $rootScope.user.getId(),
                        Supplier: SupplierDetail.getSupplier(),
                        Alias: SupplierDetail.getAlias(),
                        Address1: SupplierDetail.getAddress1(),
                        Address2: SupplierDetail.getAddress2(),
                        Address3: SupplierDetail.getAddress3(),
                        Address4: SupplierDetail.getAddress4(),
                        Postcode: SupplierDetail.getPostcode(),
                        Phone: SupplierDetail.getPhone(),
                        Facsimile: SupplierDetail.getFacsimile(),
                        Website: SupplierDetail.getWebsite(),
                        Email: SupplierDetail.getEmail(),
                        MinOrderCharge: parseInt(SupplierDetail.getMinOrderCharge()),
                        PaymentTerms: parseInt(SupplierDetail.getPaymentTerms()),
                        MaterialSupplier: SupplierDetail.getMaterialSupplier(),
                        TreatmentSupplier: SupplierDetail.getTreatmentSupplier(),
                        CalibrationSupplier: SupplierDetail.getCalibrationSupplier(),
                        ToolingSupplier: SupplierDetail.getToolingSupplier(),
                        MiscSupplier: SupplierDetail.getMiscSupplier(),
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            } else {

                // Update Supplier
                API.procedure.post('ProcUpdateSupplierDetail', {
                        UserID: $rootScope.user.getId(),
                        SupplierID: $stateParams.SupplierID,
                        Supplier: SupplierDetail.getSupplier(),
                        Alias: SupplierDetail.getAlias(),
                        Address1: SupplierDetail.getAddress1(),
                        Address2: SupplierDetail.getAddress2(),
                        Address3: SupplierDetail.getAddress3(),
                        Address4: SupplierDetail.getAddress4(),
                        Postcode: SupplierDetail.getPostcode(),
                        Phone: SupplierDetail.getPhone(),
                        Facsimile: SupplierDetail.getFacsimile(),
                        Website: SupplierDetail.getWebsite(),
                        Email: SupplierDetail.getEmail(),
                        MinOrderCharge: parseInt(SupplierDetail.getMinOrderCharge()),
                        PaymentTerms: parseInt(SupplierDetail.getPaymentTerms()),
                        MaterialSupplier: SupplierDetail.getMaterialSupplier(),
                        TreatmentSupplier: SupplierDetail.getTreatmentSupplier(),
                        CalibrationSupplier: SupplierDetail.getCalibrationSupplier(),
                        ToolingSupplier: SupplierDetail.getToolingSupplier(),
                        MiscSupplier: SupplierDetail.getMiscSupplier(),
                        Current: SupplierDetail.getCurrent()
                    })
                    .then(function(result) {
                        Notification.show('success', 'Update Supplier success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.redirectCustomer = function() {
            $window.history.back();
        };
    }

})(angular.module('app.components.department.supply-chain', [
    'app.components.department.supply-chain.main',
    'app.components.department.supply-chain.supply-contact',
    'app.components.department.supply-chain.supply-order-history',
    'app.components.department.supply-chain.supply-performance',
]));
