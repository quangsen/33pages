(function(app) {

    app.controller('PurchaseOrderRfqCtrl', PurchaseOrderRfqCtrl);
    PurchaseOrderRfqCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', 'Notification', '$window'];

    function PurchaseOrderRfqCtrl($rootScope, $scope, API, $state, $stateParams, Notification, $window) {

        $rootScope.PurchaseOrderDetail = $stateParams;
        if ($rootScope.PurchaseOrderDetail.RFQNo !== undefined) {

            /**
             * Sort Table
             */
            $scope.sortTable = [
                { title: 'Supplier', sortBy: 'SupplierID.SupplierName' },
                { title: 'Total Cost', sortBy: 'TotalCost' },
                { title: 'Delivery', sortBy: 'DeliveryPrice' },
            ];
            $scope.sortBy = $scope.sortTable[0].sortBy;
            $scope.changeSortBy = function(item) {
                $scope.sortBy = item.sortBy;
            };

            /**
             * get ESL Contact drop-down
             */
            API.procedure.get('ProcContactList')
                .then(function(result) {
                    $scope.ContactList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.getSupplierList = function() {
                API.procedure.get('ProcSupplierList')
                    .then(function(result) {
                        $scope.SupplierList = result;

                        /**
                         * get Selected Suppliers
                         */
                        API.procedure.post('ProcGetSelectedSuppliers', {
                                RFQOID: $stateParams.RFQNo
                            })
                            .then(function(result) {
                                if (result.length > 0) {
                                    result.forEach(function(value) {
                                        if (value.TotalCost !== null && value.TotalCost !== "") {
                                            value.TotalCost = parseFloat(value.TotalCost);
                                        }
                                        if (value.DeliveryPrice !== null && value.DeliveryPrice !== "") {
                                            value.DeliveryPrice = parseFloat(value.DeliveryPrice);
                                        }
                                    });
                                    $scope.SelectedSuppliers = result;

                                    // Selected Suppliers with SupplierList
                                    $scope.SupplierList.forEach(function(value) {
                                        $scope.SelectedSuppliers.forEach(function(val) {
                                            if (val.SupplierID === value.SupplierID) {
                                                val.SupplierID = value;
                                            }
                                        });
                                    });

                                    // Set selected for first item
                                    $scope.SelectedSuppliers[0].selected = true;
                                }
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };
            $scope.getSupplierList();

            $scope.generateEmail = function() {
                var selected = _.find($scope.SelectedSuppliers, { selected: true });
                if (_.isUndefined(selected)) {
                    Notification.show('warning', 'Please select Supplier');
                    return false;
                }
                if ($scope.Contact === undefined) {
                    Notification.show('warning', 'Please select Contact');
                    return false;
                }

                API.procedure.post('ProcGetSupplierEmail', {
                        RFQOID: $stateParams.RFQNo,
                        SupplierID: parseInt(selected.SupplierID.SupplierID)
                    }).then(function(response) {
                        if (response.length === 0) {
                            Notification.show('warning', 'This supplier do not have contact, please add contact for supplier');
                            return false;
                        } else {
                            var subject = "Quote required. Reference " + response[0].pkRFQnID;
                            var formattedBody = "Dear " + response[0].ContactName + ",\n\nplease provide a quotation for the following.\n\nRegards, " + $scope.Contact.UName + ".";
                            var mailToLink = "mailto:" + response[0].Email + "?subject=" + subject + "&body=" + encodeURIComponent(formattedBody);
                            $window.location.href = mailToLink;
                        }
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.RaiseRFQ = function() {
                API.procedure.post('ProcRaiseRFQ', {
                        UserID: $rootScope.user.getId(),
                        RFQOID: $stateParams.RFQNo
                    })
                    .then(function(result) {
                        Notification.show('success', 'Raise RFQ success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * AddRFQ
             */
            $scope.AddRFQ = function(itemAddRFQ) {
                if (itemAddRFQ === undefined) {
                    Notification.show('warning', 'Please enter full information!');
                    return false;
                }
                if (itemAddRFQ.SupplierID === undefined) {
                    Notification.show('warning', 'Please select Supplier!');
                    return false;
                }

                var TotalCostRegex = /^\d{0,9}(\.\d{1,2})?$/;
                var DeliveryPriceRegex = /^\d{0,3}(\.\d{1,2})?$/;
                if (!TotalCostRegex.test(itemAddRFQ.TotalCost)) {
                    Notification.show('warning', 'Total Cost must be decimal (9,2) !');
                    return false;
                }
                if (!DeliveryPriceRegex.test(itemAddRFQ.DeliveryPrice)) {
                    Notification.show('warning', 'Delivery Price must be decimal (3,2) !');
                    return false;
                }

                // AddRFQSupplier
                API.procedure.post('ProcAddRFQSupplier', {
                        UserID: $rootScope.user.getId(),
                        RFQOID: $stateParams.RFQNo,
                        SupplierID: itemAddRFQ.SupplierID.SupplierID,
                        TotalCost: itemAddRFQ.TotalCost,
                        DeliveryPrice: itemAddRFQ.DeliveryPrice
                    })
                    .then(function(result) {
                        Notification.show('success', 'Add RFQSupplier success!');
                        $scope.getSupplierList();
                        $scope.itemAddRFQ = null;
                    })
                    .catch(function(error) {
                        Notification.show('warning', 'RFQSupplier added!');
                    });
            };

        } else {
            var state = 'department.purchase-order';
            $rootScope.redirectDept(state);
        }
    }

})(angular.module('app.components.department.purchase-order.rfq', []));
