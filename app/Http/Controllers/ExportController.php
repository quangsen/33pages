<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Http\Controllers\ApiController;
use App\Procedure\Procedure;
use Illuminate\Http\Request;
use PHPExcel;
use PHPExcel_Writer_Excel2007;

class ExportController extends ApiController {

	private $_pathUpload;
	private $_creator;
	private $_title;

	public function __construct() {
		$this->_creator    = 'Kythera';
		$this->_title      = 'ExportCustomer.xlsx';
		$this->_pathUpload = 'resources/uploads/excels/';
	}

	public function export(Request $request) {
		$data = $request->only('userName', 'procedureName', 'title');
		if (!empty($data['procedureName'])) {
			$dataExport = Procedure::name($data['procedureName'])->run();
			$path       = $this->_exportData($dataExport);
			return $this->response($path);
		} else {
			throw new ApiException("Procedure name is missing", 1);
		}
	}

	private function _exportData($customers) {
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator($this->_creator);
		$objPHPExcel->getProperties()->setTitle($this->_title);
		$objPHPExcel->getProperties()->setSubject($this->_title);

		$charactes = 'ABCDEFGHIJKLMNOPQRSTVWXYZ';
		$columns   = [];

		/**
		 * Create Heading
		 */
		$columns = ['FAO', 'ClientName', 'Address1', 'Address2', 'Address3', 'Address4', 'Postcode'];
		foreach ($columns as $key => $column) {
			$cell = $charactes[$key] . '1';
			$objPHPExcel->getActiveSheet()->SetCellValue($cell, $column);
		}
		$rowCount = 2;
		foreach ($customers as $key => $customer) {
			foreach ($columns as $key2 => $column) {
				$cell = $charactes[$key2] . $rowCount;
				$objPHPExcel->getActiveSheet()->SetCellValue($cell, $customer[$column]);
			}
			$rowCount++;
		}
		$fileName = time() . "_{$this->_title}";
		if (!file_exists($this->_pathUpload)) {
			mkdir($this->_pathUpload, 0777);
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($this->_pathUpload . $fileName);
		return $this->_pathUpload . $fileName;
	}
}
