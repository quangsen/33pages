<?php

namespace App\Http\Controllers;

use App\Procedure\Procedure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ProcedureController extends ApiController
{
    /**
     * run procedure without params
     *
     * @param string $name
     * @return mixed
     */
    public function get($name)
    {
        $params = Input::all();
        if (empty($params)) {
            $data = Procedure::name($name)->run();
        } else {
            $data = Procedure::name($name)->with($params)->run();
        }
        return $this->response($data);
    }

    /**
     * run procedure with params
     *
     * @param string $name, 
     * @param array $params
     */
    public function post($name, Request $request)
    {
        $params = $request->all();
        $data   = Procedure::name($name)->with($params)->run();
        return $this->response($data);
    }
}
