(function(app) {

    app.controller('PurchaseOrderInvoiceCtrl', PurchaseOrderInvoiceCtrl);
    PurchaseOrderInvoiceCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', 'Notification'];

    function PurchaseOrderInvoiceCtrl($rootScope, $scope, API, $state, $stateParams, Notification) {
        $rootScope.PurchaseOrderDetail = $stateParams;

        if ($rootScope.PurchaseOrderDetail.RFQNo !== undefined) {

            /**
             * get un-invoiced Purchases
             */
            $scope.getUnpaidPurchases = function() {
                API.procedure.get('ProcGetUnpaidPurchases')
                    .then(function(result) {
                        $scope.UnpaidPurchases = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * get POInvoices
             */
            $scope.getPOInvoices = function() {
                API.procedure.post('ProcGetPOInvoices', {
                        RFQOID: $stateParams.RFQNo
                    })
                    .then(function(result) {
                        $scope.POInvoices = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };
            $scope.getUnpaidPurchases();
            $scope.getPOInvoices();

            /**
             * get AllInvoiceList
             */
            API.procedure.get('ProcGetAllInvoiceList')
                .then(function(result) {
                    $scope.AllInvoiceList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * Select GRB
             */
            $scope.calculateInvoiceAndAddGRB = function(UnpaidPurchases) {
                $scope.InvoiceTotal = 0;
                var linkPurchases = _.filter(UnpaidPurchases, _.matches({ 'Link': 1 }));
                if (linkPurchases.length > 0) {
                    linkPurchases.forEach(function(value) {
                        $scope.InvoiceTotal += value.ItemNo * value.Qty;
                        if ($scope.newInvoiceID === undefined) {
                            Notification.show('warning', 'Please DROP invoice first');
                            return false;
                        }
                        if ($scope.newInvoiceID > 0) {
                            API.procedure.post('ProcAddGRBInvoice', {
                                    UserID: $rootScope.user.getId(),
                                    InvoiceID: $scope.newInvoiceID,
                                    GRBID: value.fkGRBID
                                })
                                .then(function(result) {
                                    Notification.show('success', 'AddGRBInvoice success');
                                })
                                .catch(function(error) {
                                    throw error;
                                });
                        }
                    });
                }
            };

            /**
             * the function working when click 'invoice ok' checkbox button
             */
            $scope.refreshTwoTable = function(InvoiceOK) {
                $scope.getUnpaidPurchases();
                $scope.getPOInvoices();
            };

            $scope.RmvGRBInvoice = function(InvoiceOK) {
                if (InvoiceOK === 1) {
                    if ($scope.newInvoiceID === undefined) {
                        Notification.show('warning', 'Please DROP invoice first');
                        return false;
                    }
                    var UnpaidPurchase = _.find($scope.UnpaidPurchases, { 'selected': true });
                    if (UnpaidPurchase === undefined) {
                        Notification.show('warning', 'Please select UnpaidPurchase item');
                        return false;
                    }

                    if ($scope.newInvoiceID > 0) {
                        API.procedure.post('ProcRmvGRBInvoice', {
                                UserID: $rootScope.user.getId(),
                                InvoiceID: $scope.newInvoiceID,
                                GRBID: UnpaidPurchase.fkGRBID
                            })
                            .then(function(result) {
                                if (parseInt(result[0].Result) === 0) {
                                    Notification.show('warning', 'GRBInvoice not remove');
                                } else {
                                    Notification.show('success', 'Remove GRBInvoice success');
                                    _.remove($scope.UnpaidPurchases, UnpaidPurchase);
                                }
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    }
                }
            };

            /**
             * Raise Reject
             */
            $scope.RaiseReject = function(Raise) {
                if (Raise === undefined) {
                    Notification.show('warning', 'Please select Invoice and Reason!');
                    return false;
                }
                if (Raise.Invoice === undefined) {
                    Notification.show('warning', 'Please select Invoice!');
                    return false;
                }
                if (Raise.Reason === undefined) {
                    Notification.show('warning', 'Please select Reason!');
                    return false;
                }

                API.procedure.post('ProcRaiseReject', {
                        UserID: $rootScope.user.getId(),
                        InvoiceID: Raise.Invoice.InvoiceID,
                        ReasonID: Raise.Reason
                    })
                    .then(function(result) {
                        Notification.show('success', 'Raise Reject success!');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * RaiseAdjustment
             */
            $scope.RaiseAdjustment = function(Raise) {
                if (Raise === undefined) {
                    Notification.show('warning', 'Please select Invoice, Debit account and Reason!');
                    return false;
                }
                if (Raise.Invoice === undefined) {
                    Notification.show('warning', 'Please select Invoice!');
                    return false;
                }
                var RegexDebitAmount = /^\d{1,5}\.\d{1,2}$/;
                if (!RegexDebitAmount.test(Raise.DebitAmount)) {
                    Notification.show('warning', 'Debit amount must be decimal (7,2) !');
                    return false;
                }
                if (Raise.Reason === undefined) {
                    Notification.show('warning', 'Please select Reason!');
                    return false;
                }

                API.procedure.post('ProcRaiseAdjustment', {
                        UserID: $rootScope.user.getId(),
                        InvoiceID: Raise.Invoice.InvoiceID,
                        ReasonID: Raise.Reason,
                        DebitAmount: Raise.DebitAmount
                    })
                    .then(function(result) {
                        Notification.show('success', 'Raise Adjustment success!');
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * get NextRejectNo
             */
            API.procedure.get('ProcGetNextRejectNo')
                .then(function(result) {
                    $scope.NextRejectNo = result[0].RejectNoteID;
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.selectedUnpaidPurchases = function(UnpaidPurchase) {
                if ($scope.DropInvoice === undefined) {
                    $scope.DropInvoice = {};
                } else {
                    $scope.DropInvoice.InvoiceOK = 0;
                }
            };

            $scope.beforeUpload = function(file) {
                var regexDecimal92 = /^(\d{1,9}|\d{1,8}\.\d{1}|\d{1,7}\.\d{1,2})$/;
                if ($scope.DropInvoice === undefined) {
                    $scope.DropInvoice = {};
                }
                if ($scope.DropInvoice.ActualTotal !== undefined && $scope.DropInvoice.ActualTotal !== '') {
                    if (!regexDecimal92.test($scope.DropInvoice.ActualTotal)) {
                        Notification.show('warning', 'Actual total must be decimal(9,2)');
                        return false;
                    }
                } else {
                    $scope.DropInvoice.ActualTotal = 0;
                }
                if ($scope.DropInvoice.InvoiceOK === undefined) {
                    $scope.DropInvoice.InvoiceOK = 0;
                }
            };

            $scope.uploadSuccess = function(response) {
                var selectedUnpaidPurchases = _.find($scope.UnpaidPurchases, { 'selected': true });

                if ($scope.DropInvoice.InvoiceNo === undefined | $scope.DropInvoice.InvoiceNo === '') {
                    Notification.show('warning', 'Please enter InvoiceNo');
                    return false;
                }
                API.procedure.post('ProcDropInvoice', {
                        UserID: $rootScope.user.getId(),
                        FileName: response.data,
                        InvoiceNo: $scope.DropInvoice.InvoiceNo,
                        ActualTotal: $scope.DropInvoice.ActualTotal,
                        InvoiceOK: $scope.DropInvoice.InvoiceOK
                    })
                    .then(function(result) {
                        $scope.newInvoiceID = parseInt(result[0].InvoiceID);
                        if ($scope.newInvoiceID < 0) {
                            Notification.show('warning', 'Invoice already drop');
                        } else {
                            Notification.show('success', 'Drop invoice success');
                        }
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.redirectGoodsIn = function(GRBNo) {
                var state = 'department.goods-in';
                var params = {
                    GRBNo: GRBNo
                };
                $rootScope.redirectDept(state, params);
            };

        } else {
            var state = 'department.purchase-order';
            $rootScope.redirectDept(state);
        }
    }

})(angular.module('app.components.department.purchase-order.invoice', []));
