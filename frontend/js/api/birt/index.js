require('../birtResource');
(function(app) {

	app.service('BirtService', BirtService);
    BirtService.$inject = ['BirtResource', '$q'];

    function BirtService(BirtResource, $q) {

        this.post = function(birt, params) {
            var defferred = $q.defer();
            params = params || {};
            BirtResource.post({
                name: birt
            }, params, function(response) {
                defferred.resolve(response.data);
            }, function(error) {
                defferred.reject(error);
            });
            return defferred.promise;
        };
    }

})(angular.module('app.api.birt', [
	'app.api.birtResource'
]));