(function(app) {

    app.controller('EngineeringMaterialCtrl', EngineeringMaterialCtrl);
    EngineeringMaterialCtrl.$inject = ['$rootScope', '$scope', 'API', 'Notification', '$state', '$stateParams', '$window'];

    function EngineeringMaterialCtrl($rootScope, $scope, API, Notification, $state, $stateParams, $window) {
        var EngineeringTemplateID = $scope.EngineeringTemplateID;
        var WorkOrderID = $scope.WorkOrderID;
        var OperationID = $stateParams.OperationID;
        var OperationTemplateID = $stateParams.OperationTemplateID;
        var hasOperationID = false;

        if (OperationID !== undefined && OperationID !== '' && OperationID !== null) {
            hasOperationID = true;
        }
        if (OperationTemplateID !== undefined && OperationTemplateID !== '' && OperationTemplateID !== null) {
            hasOperationID = true;
        }

        if (!hasOperationID) {
            $rootScope.ApprovalComments = undefined;
            $rootScope.POComments = undefined;
            $rootScope.ConformityNotes = undefined;
            $rootScope.MaterialNotes = undefined;
        }

        // Selected Unit drop-down
        $rootScope.$on('EngineeringSelectedUnit', function(evt, unit) {
            $scope.Unit = _.find($scope.UnitList, function(item) {
                return parseInt(item.value) === parseInt(unit);
            });
        });

        // Selected Shape drop-down
        $rootScope.$on('EngineeringSelectedShape', function(evt, shape) {
            $scope.Shape = _.find($scope.MaterialShapes, function(item) {
                return parseInt(item.pkShapeID) === parseInt(shape);
            });
        });

        // Selected Approval drop-down
        $rootScope.$on('EngineeringSelectedApproval', function(evt, approval) {
            $scope.Approval = _.find($scope.MaterialLists, function(item) {
                return parseInt(item.ApprovalID) === parseInt(approval);
            });
        });

        // Selected SPU drop-down
        $rootScope.$on('EngineeringSelectedSPU', function(evt, spu) {
            $scope.SPU = _.find($scope.UOMList, function(item) {
                return item.pkUOMID === spu;
            });
        });

        // Selected Supplier drop-down
        $rootScope.$on('EngineeringSelectedSupplier', function(evt, supplier) {
            $scope.Supplier = _.find($scope.StdSuppliers, function(item) {
                return item.StdSupplierID === supplier;
            });
        });

        $scope.$on('Engineering_PopulateSpecID', function(event, args) {
            $scope.MaterialSpecID = parseInt(args.SpecID);
        });

        $scope.DescriptionSelected = function(item, model) {
            $scope.MaterialSpecID = parseInt(item.SpecID);
            $scope.changeTemplateMaterialInfo();
        };

        API.procedure.post('ProcSearchApprovalComments', {
                CommentSearch: ''
            })
            .then(function(result) {
                $scope.SearchApprovalComments = result;
            })
            .catch(function(error) {
                throw error;
            });

        API.procedure.post('ProcSearchPOComments', {
                CommentSearch: ''
            })
            .then(function(result) {
                $scope.SearchPOComments = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.addApprovalComment = function(Comment) {
            $rootScope.$broadcast('updateThead');
            if ($rootScope.ApprovalComments === undefined) {
                $rootScope.ApprovalComments = [];
            }
            var data;
            if (Comment !== undefined && Comment !== "") {
                data = {
                    Comment: Comment
                };
                $rootScope.ApprovalComments.unshift(data);
                $scope.ApprovalComment = undefined;

                if (OperationID !== undefined && OperationID !== "") {
                    $rootScope.addRootApprovalComment(data, OperationID, 'material');
                }
                if (OperationTemplateID !== undefined && OperationTemplateID !== "") {
                    $rootScope.addRootApprovalComment(data, OperationTemplateID, 'material');
                }
            }
        };

        $scope.addPOComment = function(Comment) {
            $rootScope.$broadcast('updateThead');
            if ($rootScope.POComments === undefined) {
                $rootScope.POComments = [];
            }
            var data;
            if (Comment !== undefined && Comment !== "") {
                data = {
                    Comment: Comment
                };
                $rootScope.POComments.unshift(data);
                $scope.POComment = undefined;

                if (OperationID !== undefined && OperationID !== "") {
                    $rootScope.addRootPOComment(data, OperationID);
                }
                if (OperationTemplateID !== undefined && OperationTemplateID !== "") {
                    $rootScope.addRootPOComment(data, OperationTemplateID);
                }
            }
        };

        $scope.MaterialSpecSelected = function(MaterialSpec) {
            $scope.MaterialSpec = MaterialSpec;
        };

        $scope.addTemplateMaterial = function(TemplateMaterial) {
            if (hasOperationID !== true) {
                Notification.show('warning', 'Please select Operation');
                return false;
            }
            if ($scope.MaterialSpecID === undefined) {
                $scope.MaterialSpecID = null;
            }
            if (EngineeringTemplateID !== undefined) {
                API.procedure.post('ProcAddTemplateMaterial', {
                        UserID: $rootScope.user.getId(),
                        OperationTemplateID: OperationTemplateID,
                        MaterialSpecID: $scope.MaterialSpecID,
                        Description: TemplateMaterial.getDescription(),
                        Size: TemplateMaterial.getSize(),
                        ShapeID: TemplateMaterial.getShapeID(),
                        PartLength: TemplateMaterial.getPartLength(),
                        Yield: TemplateMaterial.getYield(),
                        ApprovalID: TemplateMaterial.getApprovalID(),
                        IssueInstruction: TemplateMaterial.getIssueInstruction(),
                        TestPieces: TemplateMaterial.getTestPieces(),
                        FreeIssue: TemplateMaterial.getFreeIssue(),
                        UnitID: TemplateMaterial.getUnitID(),
                        StdDelDays: TemplateMaterial.getStdDeliveryDays(),
                        SPUID: TemplateMaterial.getSPUID(),
                        StdSupplierID: TemplateMaterial.getStdSupplierID()
                    })
                    .then(function(result) {
                        Notification.show('success', 'Add Template Material success');
                    })
                    .catch(function(error) {
                        Notification.show('warning', 'Template Material added');
                    });
            }

            if (WorkOrderID !== undefined) {
                API.procedure.post('ProcAddWOTemplateMaterial', {
                        UserID: $rootScope.user.getId(),
                        OperationID: OperationID,
                        MaterialSpecID: $scope.MaterialSpecID,
                        Description: TemplateMaterial.getDescription(),
                        Size: TemplateMaterial.getSize(),
                        ShapeID: TemplateMaterial.getShapeID(),
                        PartLength: TemplateMaterial.getPartLength(),
                        Yield: TemplateMaterial.getYield(),
                        ApprovalID: TemplateMaterial.getApprovalID(),
                        IssueInstruction: TemplateMaterial.getIssueInstruction(),
                        TestPieces: TemplateMaterial.getTestPieces(),
                        FreeIssue: TemplateMaterial.getFreeIssue(),
                        UnitID: TemplateMaterial.getUnitID(),
                        StdDelDays: TemplateMaterial.getStdDeliveryDays(),
                        SPUID: TemplateMaterial.getSPUID(),
                        StdSupplierID: TemplateMaterial.getStdSupplierID()
                    })
                    .then(function(result) {
                        Notification.show('success', 'Add Template Material success');
                    })
                    .catch(function(error) {
                        Notification.show('warning', 'Template Material added');
                    });
            }
        };

        $scope.AddConformityNote = function(Note) {
            $rootScope.$broadcast('updateThead');
            if ($rootScope.ConformityNotes === undefined) {
                $rootScope.ConformityNotes = [];
            }
            var data;
            if (Note !== undefined && Note !== "") {
                data = {
                    Note: Note
                };
                $rootScope.ConformityNotes.unshift(data);
                $scope.ConformityNote = undefined;

                if (OperationID !== undefined && OperationID !== "") {
                    $rootScope.addRootConformityNote(data, OperationID);
                }
                if (OperationTemplateID !== undefined && OperationTemplateID !== "") {
                    $rootScope.addRootConformityNote(data, OperationTemplateID);
                }
            }
        };

        $scope.AddMaterialNote = function(Note) {
            $rootScope.$broadcast('updateThead');
            if ($rootScope.MaterialNotes === undefined) {
                $rootScope.MaterialNotes = [];
            }
            var data;
            if (Note !== undefined && Note !== "") {
                data = {
                    Note: Note
                };
            }
            $rootScope.MaterialNotes.unshift(data);
            $scope.MaterialNote = undefined;
            if (OperationID !== undefined && OperationID !== "") {
                $rootScope.addRootMaterialNote(data, OperationID);
            }
            if (OperationTemplateID !== undefined && OperationTemplateID !== "") {
                $rootScope.addRootMaterialNote(data, OperationTemplateID);
            }
        };

        $scope.changeTemplateMaterialInfo = function() {
            $rootScope.Engineering_MaterialTab_TemplateMaterialInfo = $scope.TemplateMaterialInfo;
        };

        $scope.mailTo = function(data) {
            var hasDescription;
            var hasSize;
            var hasUnit;
            var hasShape;
            var hasSPU;
            var hasApproval;
            if (data.Description !== '' && data.Description !== undefined) {
                hasDescription = true;
            }
            if (data.Size !== undefined && data.Size !== '') {
                hasSize = true;
            }
            if (data.UnitID !== null && data.UnitID !== undefined) {
                hasUnit = true;
            }
            if (data.ShapeID !== null && data.ShapeID !== undefined) {
                hasShape = true;
            }
            if (data.SPUID !== null && data.SPUID !== undefined) {
                hasSPU = true;
            }
            if (data.ApprovalID !== null && data.ApprovalID !== undefined) {
                hasApproval = true;
            }
            if (hasDescription && hasSize && hasUnit && hasShape && hasSPU && hasApproval) {
                var Unit = _.find($scope.UnitList, function(item) {
                    return parseInt(item.value) === parseInt(data.UnitID);
                });
                if (!_.isUndefined(Unit)) {
                    Unit = Unit.name;
                }

                var Shape = _.find($scope.MaterialShapes, function(item) {
                    return parseInt(item.pkShapeID) === parseInt(data.ShapeID);
                });
                if (!_.isUndefined(Shape)) {
                    Shape = Shape.Description;
                }

                var Approval = _.find($scope.MaterialLists, function(item) {
                    return parseInt(item.ApprovalID) === parseInt(data.ApprovalID);
                });
                if (!_.isUndefined(Approval)) {
                    Approval = Approval.Description;
                }

                var SPU = _.find($scope.UOMList, function(item) {
                    return parseInt(item.pkUOMID) === parseInt(data.SPUID);
                });
                if (!_.isUndefined(SPU)) {
                    SPU = SPU.Description;
                }
                var firstRow = "Please provide a quotation for the following:";
                var secondRow = data.Description + " @ " + data.Size + Unit + " " + Shape + " " + SPU + ", to " + Approval + " Approval";
                var thirdRow = "Thank you.";
                var formattedBody = firstRow + "\n\n" + secondRow + "\n\n" + thirdRow;
                var mailToLink = "mailto:simbiisltd@gmail.com?body=" + encodeURIComponent(formattedBody);
                $window.location.href = mailToLink;
            } else {
                Notification.show('warning', 'Please select Description, Size, Unit, Shape, SPU and Approval');
                return false;
            }
        };
    }

})(angular.module('app.components.department.engineering-template.material', []));
