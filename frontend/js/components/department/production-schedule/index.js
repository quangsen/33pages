require('./material-issue/');
require('./subcon/');
require('./machine/');

(function(app) {
    app.controller('ProductionScheduleCtrl', ProductionScheduleCtrl);

    ProductionScheduleCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function ProductionScheduleCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {

        $scope.showBegin = true;
        $scope.showPause = true;
        $scope.showComplete = true;
        $scope.showBeginHighlight = !$scope.showBegin;
        $scope.showPauseHighlight = !$scope.showPause;

        function displayButton(OpStatus) {
            OpStatus = parseInt(OpStatus);
            if (OpStatus === 0) {
                $scope.showBegin = true;
                $scope.showPause = true;
                $scope.showComplete = true;
                $scope.showBeginHighlight = !$scope.showBegin;
                $scope.showPauseHighlight = !$scope.showPause;
            }
            if (OpStatus === 1) {
                $scope.showBegin = false;
                $scope.showPause = true;
                $scope.showComplete = true;
                $scope.showBeginHighlight = !$scope.showBegin;
                $scope.showPauseHighlight = !$scope.showPause;
            }
            if (OpStatus === 2) {
                $scope.showBegin = true;
                $scope.showPause = false;
                $scope.showComplete = true;
                $scope.showBeginHighlight = !$scope.showBegin;
                $scope.showPauseHighlight = !$scope.showPause;
            }
        }

        function showMessagesOperator(OpStatus, buttonType) {
            OpStatus = parseInt(OpStatus);
            if (OpStatus === 0) {
                if (buttonType !== 'Play') {
                    Notification.show('warning', 'You can only click Play button');
                    return false;
                } else {
                    return true;
                }
            }
            if (OpStatus === 1) {
                if (buttonType === 'Play') {
                    Notification.show('warning', 'You can click Pause or Complete button');
                    return false;
                } else {
                    return true;
                }
            }
            if (OpStatus === 2) {
                if (buttonType !== 'Pause') {
                    Notification.show('warning', 'You can only click Pause button');
                    return false;
                } else {
                    return true;
                }
            }
        }

        /**
         * get data table of Material Issue tab
         */
        API.procedure.get('ProcGetProductionScheduleMI')
            .then(function(result) {
                if (result.length > 0) {
                    result.forEach(function(value) {
                        if (value.OrderID !== null && value.OrderID !== "") {
                            value.OrderID = parseFloat(value.OrderID);
                        }
                        if (value.Qty !== null && value.Qty !== "") {
                            value.Qty = parseFloat(value.Qty);
                        }
                        if (value.Ops !== null && value.Ops !== "") {
                            value.Ops = parseFloat(value.Ops);
                        }
                    });
                }
                $scope.ProductionScheduleMI = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * get data table of Subcon Tab
         */
        API.procedure.get('ProcGetProductionScheduleSC')
            .then(function(result) {
                if (result.length > 0) {
                    result.forEach(function(value) {
                        if (value.OrderID !== null && value.OrderID !== "") {
                            value.OrderID = parseFloat(value.OrderID);
                        }
                        if (value.Qty !== null && value.Qty !== "") {
                            value.Qty = parseFloat(value.Qty);
                        }
                        if (value.OpNo !== null && value.OpNo !== "") {
                            value.OpNo = parseFloat(value.OpNo);
                        }
                        if (value.LastOp !== null && value.LastOp !== "") {
                            value.LastOp = parseFloat(value.LastOp);
                        }
                    });
                }
                $scope.ProductionScheduleSC = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * get All Machine
         * each Machine is display in a tab
         */
        $scope.getAllMachine = function() {
            API.procedure.get('ProcGetProductionSchedule')
                .then(function(result) {
                    if (result.length > 0) {
                        result.forEach(function(value) {
                            if (value.OrderID !== null && value.OrderID !== "") {
                                value.OrderID = parseFloat(value.OrderID);
                            }
                            if (value.Qty !== null && value.Qty !== "") {
                                value.Qty = parseFloat(value.Qty);
                            }
                            if (value.Ops !== null && value.Ops !== "") {
                                value.Ops = parseFloat(value.Ops);
                            }
                        });
                    }
                    $scope.ProductionSchedule = result;
                    $scope.MachineTab = angular.copy($scope.ProductionSchedule);
                    $scope.MachineTab = generateMachineTab($scope.MachineTab);
                })
                .catch(function(error) {
                    throw error;
                });
        };
        $scope.getAllMachine();

        function generateMachineTab(machineTab) {
            if (machineTab !== undefined && machineTab.length > 0) {
                var machineAliasArray = [];
                var repeatMachine = [];
                machineTab.forEach(function(value, key) {
                    machineTab[key].MachineAlias = value.Machine.replace(/\s+/g, '-').toLowerCase();
                    if (parseInt(machineAliasArray.indexOf(machineTab[key].MachineAlias)) === -1) {
                        machineAliasArray.push(machineTab[key].MachineAlias);
                    } else {
                        repeatMachine.push(value);
                    }
                });
                if (repeatMachine.length > 0) {
                    repeatMachine.forEach(function(value) {
                        _.pull(machineTab, value);
                    });
                }
            }
            return machineTab;
        }

        /**
         * get Reason drop-down
         */
        $scope.Reasons = [
            { name: 'Staff', value: 1 },
            { name: 'Gauges', value: 2 },
            { name: 'Failed', value: 3 },
        ];

        /**
         * get Resource drop-down
         */
        API.procedure.get('ProcGetResourceList')
            .then(function(result) {
                $scope.ResourceList = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.checkSelectOperation = function() {
            var OperationMC = _.find($scope.ProductionSchedule, ({ 'selected': true }));
            var OperationMI = _.find($scope.ProductionScheduleMI, ({ 'selected': true }));
            var OperationSC = _.find($scope.ProductionScheduleSC, ({ 'selected': true }));
            if (OperationMI === undefined && OperationSC === undefined && OperationMC === undefined) {
                Notification.show('warning', 'Please select Operation!');
                return false;
            }
            if (OperationMC !== undefined) {
                Operation = OperationMC;
            }
            if (OperationMI !== undefined) {
                Operation = OperationMI;
            }
            if (OperationSC !== undefined) {
                Operation = OperationSC;
            }
            return Operation;
        };

        $scope.checkSelectMachineOperation = function() {
            var selectMachineOperation = _.find($scope.ProductionSchedule, { 'selected': true });
            if (selectMachineOperation === undefined) {
                Notification.show('warning', 'Please select Machine Operation!');
                return false;
            }
            return selectMachineOperation;
        };

        $scope.addResource = function(UserResource) {
            var selectMachineOperation = $scope.checkSelectMachineOperation();
            if (selectMachineOperation) {
                if (UserResource === undefined) {
                    Notification.show('warning', 'Please select Allocate secondary resource!');
                    return false;
                }
                API.procedure.post('ProcAllocateResource', {
                        UserID: $rootScope.user.getId(),
                        OperationID: parseInt(selectMachineOperation.OperationID),
                        ResourceUserID: UserResource.UserID
                    })
                    .then(function(result) {
                        Notification.show('success', 'Add Resource success');
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.resetOperationStatus = function() {
            $scope.OperationStatus = null;
        };

        $scope.OperationSelected = function(Operation) {
            if (Operation.selected === true) {
                $scope.getOperationStatus(Operation.OperationID);
            } else {
                $scope.OperationStatus = null;
            }
        };

        $scope.NextProgress = function(array, item) {
            if (item.OpNo < item.LastOp) {
                item.OpNo++;
            }
        };

        $scope.BeginOperator = function(SetOrRun) {
            var selectMachineOperation = $scope.checkSelectMachineOperation();
            if (selectMachineOperation) {
                var permission = showMessagesOperator($scope.OperationStatus.OpStatus, 'Play');
                if (permission) {
                    if (SetOrRun === undefined) {
                        Notification.show('warning', 'Please select set or run!');
                        return false;
                    }
                    if (parseInt($scope.OperationStatus.OpStatus) === 0) {
                        API.procedure.post('ProcBeginOperation', {
                                UserID: $rootScope.user.getId(),
                                OperationID: selectMachineOperation.OperationID,
                            })
                            .then(function(result) {
                                if (result.length > 0) {
                                    if (result[0].ResultMsg) {
                                        if (parseInt(result[0].Result) === -1) {
                                            Notification.show('warning', result[0].ResultMsg);
                                        } else {
                                            Notification.show('success', result[0].ResultMsg);
                                        }
                                    } else {
                                        Notification.show('success', 'Begin Operation success');
                                    }
                                    $scope.getOperationStatus(selectMachineOperation.OperationID);
                                }
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    }
                }
            }
        };

        $scope.PauseOperator = function(Reason) {
            var selectMachineOperation = $scope.checkSelectMachineOperation();
            if (selectMachineOperation) {
                var permission = showMessagesOperator($scope.OperationStatus.OpStatus, 'Pause');
                if (permission) {
                    if (Reason === undefined) {
                        Notification.show('warning', 'Please select Reason!');
                        return false;
                    }
                    if (parseInt($scope.OperationStatus.OpStatus) === 1 || parseInt($scope.OperationStatus.OpStatus) === 2) {
                        API.procedure.post('ProcPauseOperation', {
                                UserID: $rootScope.user.getId(),
                                OperationID: selectMachineOperation.OperationID,
                                MachineID: selectMachineOperation.MachineID,
                                ReasonID: Reason.value,
                            })
                            .then(function(result) {
                                Notification.show('success', result[0].ResultMsg);
                                $scope.getOperationStatus(selectMachineOperation.OperationID);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    }
                }
            }
        };

        $scope.CompleteOperator = function() {
            var selectMachineOperation = $scope.checkSelectMachineOperation();
            if (selectMachineOperation) {
                var permission = showMessagesOperator($scope.OperationStatus.OpStatus, 'Stop');
                if (permission) {
                    API.procedure.post('ProcCompleteOperation', {
                            UserID: $rootScope.user.getId(),
                            OperationID: selectMachineOperation.OperationID,
                        })
                        .then(function(result) {
                            Notification.show('success', result[0].ResultMsg);
                            $scope.getOperationStatus(selectMachineOperation.OperationID);
                            // $scope.getAllMachine();
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            }
        };

        $scope.getOperationStatus = function(OperationID) {
            API.procedure.post('ProcGetOperationStatus', {
                    OperationID: OperationID
                })
                .then(function(result) {
                    $scope.OperationStatus = result[0];
                    displayButton($scope.OperationStatus.OpStatus);
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.refreshProduction = function() {
            $scope.getAllMachine();
        };

        /**
         * Redirect user to WorkOrder-Detail screen
         */
        $scope.ViewWO = function() {
            var selectOperation = $scope.checkSelectOperation();
            if (selectOperation) {
                var state = 'department.wo-detail';
                var params = {
                    WorkOrderID: selectOperation.WorkOrderID
                };
                $scope.redirectDept(state, params);
            }
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };

        $scope.openBIRT = function() {
            $rootScope.openTabBirt('ScheduleClash');
        };
    }

})(angular.module('app.components.department.production-schedule', [
    'app.components.department.production-schedule.material-issue',
    'app.components.department.production-schedule.subcon',
    'app.components.department.production-schedule.machine'
]));
