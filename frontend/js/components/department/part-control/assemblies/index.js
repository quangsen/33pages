(function(app) {

    app.controller('PartAssemblyCtrl', PartAssemblyCtrl);
    PartAssemblyCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification'];

    function PartAssemblyCtrl($rootScope, $scope, $state, $stateParams, API, Notification) {
        var PTemplateID = $stateParams.PTemplateID;
        if (PTemplateID !== undefined && PTemplateID !== '') {

            API.procedure.post('ProcGetPartAssemblies', {
                    PTemplateID: PTemplateID
                })
                .then(function(result) {
                    $scope.PartAssemblies = result;
                })
                .catch(function(error) {
                    throw error;
                });

            API.procedure.get('ProcPartNumberList')
                .then(function(result) {
                    $scope.PartNumberList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            $scope.PartLinkToMaster = function(Qty, MasterPartNo) {
                if (Qty === undefined | Qty === '') {
                    Notification.show('warning', 'Please enter Qty');
                    return false;
                }

                var numberRegex = /^\d+$/;
                if (!numberRegex.test(Qty)) {
                    Notification.show('warning', 'Qty must be number');
                    return false;
                }

                if (MasterPartNo === undefined) {
                    Notification.show('warning', 'Please select Master Part No');
                    return false;
                }

                API.procedure.post('ProcPartLinkToMaster', {
                        UserID: $rootScope.user.getId(),
                        Qty: Qty,
                        PTemplateID: PTemplateID,
                        MasterPartTemplateID: MasterPartNo.PartTemplateID
                    })
                    .then(function(result) {
                        Notification.show('success', 'Part link to Master success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

        } else {
            $state.go('department.part-control.part-list');
        }
    }

})(angular.module('app.components.department.part-control.assemblies', []));
