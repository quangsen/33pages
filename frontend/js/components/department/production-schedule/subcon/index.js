(function(app) {
    app.controller('ProductionScheduleSubconCtrl', ProductionScheduleSubconCtrl);

    ProductionScheduleSubconCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification'];

    function ProductionScheduleSubconCtrl($rootScope, $scope, $state, $stateParams, API, Notification) {

        $scope.resetOperationStatus();
        
        // Conditional to active class
        $rootScope.machineParams = null;

        /**
         * Sort Table
         */
        $scope.sortTable = [
            { title: 'WO No.', sortBy: 'WorkOrderID' },
            { title: 'CO No.', sortBy: 'OrderID' },
            { title: 'Part Number', sortBy: 'PartNumber' },
            { title: 'Qty', sortBy: 'Qty' },
            { title: 'Operation', sortBy: 'Operation' },
            { title: 'DD Start', sortBy: 'DDStart' },
            { title: 'Start', sortBy: 'StartDate' },
            { title: 'Finish', sortBy: 'Finish' },
            { title: 'DD Finish', sortBy: 'DDFinish' },
            { title: 'Supplier', sortBy: 'Supplier' },
            { title: 'Progress', sortBy: 'OpNo' },
        ];
        $scope.sortBy = $scope.sortTable[0].sortBy;
        $scope.changeSortBy = function(item) {
            $scope.sortBy = item.sortBy;
        };
        
    }

})(angular.module('app.components.department.production-schedule.subcon', []));
