(function(app) {

    app.controller('CustomerMainCtrl', CustomerMainCtrl);

    CustomerMainCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API', '$state', 'Notification'];

    function CustomerMainCtrl($rootScope, $scope, $stateParams, API, $state, Notification) {
    	$scope.resetStatusButton();
        $scope.clearInfoCustomer();
    }

})(angular.module('app.components.customer.main', ['app.template']));
