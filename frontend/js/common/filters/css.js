(function(app) {

	app.filter('css', css);
	css.$inject = ['$sce'];

	function css($sce) {
		return function(html) {
            return $sce.trustAsHtml(html);
        };
	}

})(angular.module('app.common.filters.css', []));
