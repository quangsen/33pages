<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'UName'       => 'admin',
                'Email'       => 'admin@gmail.com',
                'pwd'         => md5('password'),
                'FirstName'   => 'Admin',
                'LastName'    => 'Admin',
                'Address1'    => 'Ha Noi vietnam',
                'Postcode'    => '41000',
                'MobilePhone' => '0123456789',
                'SkillLevel'  => 1,
                'Employed'    => 2,
            ],
        ];
        foreach ($users as $v) {
            User::create($v);
        };
    }
}
