(function(app) {

    app.controller('PartReviewTasksCtrl', PartReviewTasksCtrl);
    PartReviewTasksCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification'];

    function PartReviewTasksCtrl($rootScope, $scope, $state, $stateParams, API, Notification) {
        var PTemplateID = $stateParams.PTemplateID;
        if (PTemplateID !== undefined && PTemplateID !== '') {

            /**
             * Get Task Type drop-down
             */
            API.procedure.get('ProcGetActionTypeList')
                .then(function(result) {
                    $scope.TypeList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get data of table
             */
            API.procedure.post('ProcGetPartReviewTasks', {
                    PTemplateID: PTemplateID
                })
                .then(function(result) {
                    $scope.$watch('TypeList', function(TypeList) {
                        if (TypeList !== undefined) {
                            result.forEach(function(value, key) {
                                var TaskType = _.find(TypeList, function(item) {
                                    return item.ActionTypeID === value.fkActionTypeID;
                                });
                                result[key].TaskType = TaskType.ActionType;
                            });
                            $scope.PartReviewTasks = result;
                        }
                    });
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * Create a review
             */
            $scope.CreateReview = function(Review) {
                if (Review === undefined) {
                    Notification.show('warning', 'Please enter Review and select Task Type');
                    return false;
                }
                if (Review.Description === undefined | Review.Description === '') {
                    Notification.show('warning', 'Please enter Review');
                    return false;
                }
                if (Review.Type === undefined) {
                    Notification.show('warning', 'Please select Task Type');
                    return false;
                }
                if (Review.FO === undefined) {
                    Review.FO = 0;
                }
                if (Review.Pu === undefined) {
                    Review.Pu = 0;
                }
                if (Review.PE === undefined) {
                    Review.PE = 0;
                }
                if (Review.En === undefined) {
                    Review.En = 0;
                }
                if (Review.Qu === undefined) {
                    Review.Qu = 0;
                }
                if (Review.SP === undefined) {
                    Review.SP = 0;
                }
                if (Review.St === undefined) {
                    Review.St = 0;
                }
                if (Review.Fi === undefined) {
                    Review.Fi = 0;
                }

                API.procedure.post('ProcAddPartReviewTask', {
                        PTemplateID: PTemplateID,
                        UserID: $rootScope.user.getId(),
                        Description: Review.Description,
                        ActionTypeID: Review.Type.ActionTypeID,
                        FO: Review.FO,
                        Pu: Review.Pu,
                        PE: Review.PE,
                        En: Review.En,
                        Qu: Review.Qu,
                        SP: Review.SP,
                        St: Review.St,
                        Fi: Review.Fi,
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.CloseReview = function(PartReviewTasks) {
                var selectedReview = _.find(PartReviewTasks, function(item) {
                    return item.selected === true;
                });

                API.procedure.post('ProcCloseTask', {
                        UserID: $rootScope.user.getId(),
                        TaskID: selectedReview.TaskID
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };
        }
    }

})(angular.module('app.components.department.part-control.part-review-tasks', []));
