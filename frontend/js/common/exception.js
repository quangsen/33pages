(function(app) {
    app.config(['$logProvider', '$provide', function($logProvider, $provide) {
        $logProvider.debugEnabled(true);

        $provide.decorator('$exceptionHandler', [
            '$log',
            '$delegate',
            '$injector',
            function($log, $delegate, $injector) {
                return function(exception) {
                    $log.debug('Exception handler:', exception);
                    var $rootScope = $injector.get("$rootScope");
                    $rootScope.$broadcast('onError', exception);
                };
            }
        ]);
    }]);
    /*
     * Show Error message
     */
    app.run(errorHandler);

    errorHandler.$inject = ['$rootScope', 'Notification'];

    function errorHandler($rootScope, Notification) {
        $rootScope.$on('onError', function(e, err) {
            if (!$rootScope.hasError) {
                $rootScope.hasError = true;
            }
            if (!_.isNil(err.data) && !_.isNil(err.data.error)) {
                Notification.show('warning', err.data.error, 10000);
            } else {
                Notification.show('warning', err, 10000);
            }
        });
    }
})(angular.module('app.common.exceptionHandler', []));
