(function(app) {
    app.controller('WorkOrderMaterialIssueCtrl', WorkOrderMaterialIssueCtrl);
    WorkOrderMaterialIssueCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API', '$state', 'Notification'];

    function WorkOrderMaterialIssueCtrl($rootScope, $scope, $stateParams, API, $state, Notification) {
        var WorkOrderID = $rootScope.WorkOrderID = $stateParams.WorkOrderID;
        if (WorkOrderID !== undefined && WorkOrderID !== '') {
            if ($scope.WorkOrderDetail === undefined) {
                $scope.getWorkOrderInfo(WorkOrderID);
            }

            /**
             * Sort Table
             */
            $scope.sortTable = [
                { title: 'GRB No.', sortBy: 'GRBNo', class: 'col-xs-1 no-padding' },
                { title: 'Spec', sortBy: 'Spec', class: 'col-xs-1 no-padding' },
                { title: 'Size', sortBy: 'Size', class: 'col-xs-1 no-padding' },
                { title: 'units', sortBy: 'Units', class: 'col-xs-1 no-padding' },
                { title: 'Shape', sortBy: 'Shape', class: 'col-xs-1 no-padding' },
                { title: 'Release', sortBy: 'ReleaseLevel', class: 'col-xs-1 no-padding' },
                { title: 'Purch. Lgth', sortBy: 'PurchaseLength', class: 'col-xs-1 no-padding' },
                { title: 'Date in', sortBy: 'DateIn', class: 'col-xs-2 no-padding' },
                { title: 'Ord. Qty', sortBy: 'OrderQty', class: 'col-xs-1 no-padding' },
                { title: 'In Stk', sortBy: 'InStock', class: 'col-xs-1 no-padding' },
                { title: 'Locn', sortBy: 'Location', class: 'col-xs-1 no-padding' },
            ];
            $scope.sortBy = $scope.sortTable[0].sortBy;
            $scope.changeSortBy = function(item) {
                $scope.sortBy = item.sortBy;
            };

            /**
             * get GRB table
             */
            API.procedure.post('ProcSearchMaterialInStock', {
                    GRBNo: null,
                    Spec: '',
                    Size: null
                })
                .then(function(result) {
                    $scope.MaterialInStock = result;
                    if ($scope.MaterialInStock.length > 0) {
                        $scope.MaterialInStock.forEach(function(value) {
                            if (value.GRBNo !== null && value.GRBNo !== "") {
                                value.GRBNo = parseInt(value.GRBNo);
                            }
                            if (value.Size !== null && value.Size !== "") {
                                value.Size = parseFloat(value.Size);
                            }
                            if (value.PurchaseLength !== null && value.PurchaseLength !== "") {
                                value.PurchaseLength = parseInt(value.PurchaseLength);
                            }
                            if (value.OrderQty !== null && value.OrderQty !== "") {
                                value.OrderQty = parseInt(value.OrderQty);
                            }
                            if (value.InStock !== null && value.InStock !== "") {
                                value.InStock = parseInt(value.InStock);
                            }
                        });
                    }
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Unit drop-down
             */
            $scope.Units = [
                { name: 'millimetres', value: 1 },
                { name: 'metres', value: 2 }
            ];

            /**
             * get Shape drop-down
             */
            API.procedure.get('ProcGetMaterialShapes')
                .then(function(result) {
                    $scope.MaterialShapes = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Approval drop-down
             */
            API.procedure.get('ProcGetAllApprovalList')
                .then(function(result) {
                    $scope.AllApprovalList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get SPU drop-down
             */
            API.procedure.get('ProcGetUOMList')
                .then(function(result) {
                    $scope.UOMList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get Overview information of Work Order
             */
            API.procedure.post('ProcGetWOMaterialDetail', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    if (result.length > 0) {
                        $scope.WOMaterialDetail = result[0];

                        // Selected Units drop-down
                        $scope.WOMaterialDetail.UnitID = _.find($scope.Units, function(value) {
                            return value.value === $scope.WOMaterialDetail.UnitID;
                        });

                        // Selected Shape drop-down
                        $scope.$watch('MaterialShapes', function(MaterialShape) {
                            $scope.WOMaterialDetail.fkShapeID = _.find(MaterialShape, function(value) {
                                return value.pkShapeID === $scope.WOMaterialDetail.fkShapeID;
                            });
                        });

                        // Selected Approval drop-down
                        $scope.$watch('AllApprovalList', function(ApprovalList) {
                            $scope.WOMaterialDetail.ApprovalID = _.find(ApprovalList, function(value) {
                                return value.pkApprovalID === $scope.WOMaterialDetail.ApprovalID;
                            });
                        });

                        // Selected SPU drop-down
                        $scope.$watch('UOMList', function(UOM) {
                            $scope.WOMaterialDetail.SPUID = _.find(UOM, function(value) {
                                return value.pkUOMID === $scope.WOMaterialDetail.SPUID;
                            });
                        });
                    }
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * get WODetail
             */
            API.procedure.post('ProcGetWODetail', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.WODetail = result[0];
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * Issue Material For WO
             */
            $scope.IssueMaterialForWO = function() {
                API.procedure.post('ProcIssueMaterialForWO', {
                        UserID: $rootScope.user.getId(),
                        PartNumber: $scope.WODetail.PartNumber,
                        WorkOrderID: WorkOrderID
                    })
                    .then(function(result) {
                        Notification.show('success', 'Store issue material success !');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * Search Material and populate GRB table
             */
            $scope.SearchMaterial = function(GRBNo, Spec, Size) {
                if (GRBNo === undefined) {
                    GRBNo = null;
                }
                if (Spec === undefined) {
                    Spec = null;
                }
                if (Size === undefined) {
                    Size = null;
                }
                API.procedure.post('ProcSearchMaterialInStock', {
                        GRBNo: GRBNo,
                        Spec: Spec,
                        Size: Size
                    })
                    .then(function(result) {
                        $scope.MaterialInStock = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * Reserve Material
             * 
             * checkQty & isStore use to show hide Qty & Store text in View
             */
            $scope.checkQty = false;
            $scope.isStore = false;
            $scope.ReserveMaterial = function(MaterialInStock, QtyReserve) {
                if (MaterialInStock === undefined) {
                    Notification.show('warning', 'Please search then select GRBNo');
                    return false;
                }
                if (QtyReserve === undefined || QtyReserve === '') {
                    Notification.show('warning', 'Please enter Qty reserve');
                    return false;
                }
                var isSelect = false;
                $scope.isStore = true;
                MaterialInStock.forEach(function(value) {
                    if (value.selected === true && value.selected !== undefined) {
                        isSelect = true;
                        if (value.OrderQty >= QtyReserve) {
                            $scope.checkQty = false;
                            API.procedure.post('ProcReserveMaterial', {
                                    UserID: $rootScope.user.getId(),
                                    GRBNo: value.GRBNo,
                                    WorkOrderID: $rootScope.WorkOrderID,
                                    Qty: QtyReserve
                                })
                                .then(function(result) {
                                    Notification.show('success', 'Reserve Material success');
                                })
                                .catch(function(error) {
                                    Notification.show('warning', 'Work Order Reserved');
                                });
                        } else {
                            $scope.checkQty = true;
                        }
                    }
                });
                if (isSelect === false) {
                    Notification.show('warning', 'Please select Material!');
                    return false;
                }
            };

            /**
             * get Notes table
             */
            API.procedure.post('ProcGetIntMaterialNotes', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.IntMaterialNotes = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * the function working when press Enter key on Note input
             */
            $scope.AddMaterialNote = function(MaterialNote) {
                if (MaterialNote === undefined | MaterialNote === '') {
                    Notification.show('warning', 'Please enter Note');
                    return false;
                }
                API.procedure.post('ProcAddIntMaterialNote', {
                        UserID: $rootScope.user.getId(),
                        WorkOrderID: WorkOrderID,
                        Note: MaterialNote
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.redirectPurchaseOrderPage = function() {
                var DeptID = $stateParams.DeptID;
                var params = {
                    DeptID: DeptID
                };
                var state = 'department.purchase-order';
                $rootScope.redirectDept(state, params);
            };

        } else {
            var state = 'department.workorders';
            $rootScope.redirectDept(state);
            return false;
        }

    }
})(angular.module('app.components.department.workorders.material-issue', []));
