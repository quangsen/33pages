(function(app) {
    app.directive('searchInput', ['API', function(API) {
        return {
            strict: 'E',
            scope: {
                procedure: '@',
                searchKey: '@',
                onSelect: '=',
                displayKey: '@',
                otherField: '=',
                searchInputModel: '=',
            },
            templateUrl: '/frontend/js/common/partials/searchInput.tpl.html',
            link: function(scope, element, attrs) {
                scope.PartNoList = [];
                scope.clearSearch = function(searchInputModel) {
                    if (scope.searchInputModel !== undefined) {
                        scope.searchInputModel = undefined;
                    }
                };
                scope.SearchInput = function(value) {
                    var params = {};
                    params[scope.searchKey] = value;
                    if (scope.otherField !== undefined && typeof scope.otherField == 'object') {
                        for (var key in scope.otherField) {
                            params[key] = scope.otherField[key];
                        }
                    }
                    return API.procedure.post(scope.procedure, params)
                        .then(function(result) {
                            if (!Array.isArray(result)) {
                                throw ('response data must be an array');
                            }
                            if (!_.has(result[0], scope.displayKey)) {
                                var displayKey = _.camelCase(scope.displayKey);
                                var str = scope.displayKey;
                                scope.display = displayKey;
                                var split;
                                var strpos;
                                var items;
                                var index;
                                value = _.omit(value, displayKey);
                                _(result).forEach(function(value) {
                                    items = [];
                                    index = 0;
                                    for (var key in value) {
                                        if (key !== '//' && key !== displayKey) {
                                            strpos = str.indexOf(key);
                                            if (strpos > -1) {
                                                items.push({ key: key, strpos: strpos });
                                            }
                                        }
                                    }
                                    items = _.orderBy(items, strpos);
                                    _(items).forEach(function(item) {
                                        value[displayKey] = value[displayKey] || '';
                                        value[displayKey] += str.substring(index, item.strpos) + value[item.key];
                                        index = item.strpos + item.key.length;
                                    });
                                });
                            } else {
                                scope.display = scope.displayKey;
                            }
                            return result;
                        })
                        .catch(function(error) {
                            console.log(error);
                        });
                };
            },
        };
    }]);
})(angular.module('app.common.directives.searchInput', ['app.template']));
