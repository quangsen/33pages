(function(app) {

    app.controller('InspectionCtrl', InspectionCtrl);
    InspectionCtrl.$inject = ['$rootScope', '$scope', 'API', '$stateParams', 'Notification', '$state', '$window'];

    function InspectionCtrl($rootScope, $scope, API, $stateParams, Notification, $state, $window) {
        var OperationID = $scope.OperationIDSelected = $stateParams.OperationID;
        var WorkOrderID = $scope.WorkOrderID = $stateParams.WorkOrderID;
        var PartNumber = $scope.PartNumber = $stateParams.PartNumber;
        var GRBID = $scope.GRBID = $stateParams.GRBID;

        /**
         * get Staff drop-down
         */
        $scope.Staffs = [
            { name: 'Staff', value: 1 },
            { name: 'Gauges', value: 2 },
            { name: 'Failed', value: 3 },
        ];

        var regexNumber = /^\d+$/;

        /**
         * get Pause Reason drop-down
         */
        $scope.Reasons = [
            { name: 'Staff', value: 1 },
            { name: 'Gauges', value: 2 },
            { name: 'Failed', value: 3 },
        ];

        /**
         * get Reason drop-down
         */
        $scope.ScrappedReasons = [
            { name: 'Machine breakdown', value: 1 },
            { name: 'Tool worn', value: 2 },
            { name: 'Operator error', value: 3 },
            { name: 'Material hardness', value: 4 },
        ];

        /**
         * get Reject reason drop-down
         */
        $scope.RejectReasons = [
            { name: 'Damaged', value: 1 },
            { name: 'Material Issue', value: 2 },
            { name: 'Quantity Issue', value: 3 },
            { name: 'Dimensional Issue', value: 4 },
            { name: 'Treatment Issue', value: 5 },
            { name: 'Paperwork Issue', value: 6 },
        ];

        /**
         * Execute Inspection
         */
        if (OperationID !== undefined) {

            $scope.getInspectOpList = function(OperationID) {
                API.procedure.post('ProcGetInspectOpList', {
                        WorkOrderID: WorkOrderID,
                        OperationID: OperationID
                    })
                    .then(function(result) {
                        $scope.InspectOpList = result;
                        var SelectedOperation = _.find($scope.InspectOpList, function(value) {
                            return parseInt(value.OperationID) === parseInt(OperationID);
                        });
                        if (SelectedOperation !== undefined) {
                            SelectedOperation.selected = true;
                        }
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $scope.getOperationInfo = function(OperationID) {

                /**
                 * get data of Operation table
                 */
                $scope.getInspectOpList(OperationID);

                /**
                 * get data Overview of Operation
                 */
                API.procedure.post('ProcGetOpInspectionDetail', {
                        OperationID: OperationID
                    })
                    .then(function(result) {
                        if (result.length > 0) {
                            $scope.OpInspectionDetail = result[0];
                            $scope.MaterialInspectionID = result[0].InspectionID;
                            if (result.length > 0) {

                                // Selected Reason drop-down
                                if ($scope.OpInspectionDetail.ReasonID !== undefined && $scope.OpInspectionDetail.ReasonID !== null) {
                                    $scope.OpInspectionDetail.ReasonID = _.find($scope.ScrappedReasons, function(item) {
                                        return parseInt(item.value) === parseInt($scope.OpInspectionDetail.ReasonID);
                                    });
                                }
                            }
                        }
                    })
                    .catch(function(error) {
                        throw error;
                    });

                /**
                 * get Previous Internal Scrap table
                 */
                API.procedure.post('ProcGetOpInspectionPISI', {
                        OperationID: OperationID
                    })
                    .then(function(result) {
                        $scope.OpInspectionPISI = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });

                /**
                 * get External Reject Preventive table
                 */
                API.procedure.post('ProcGetOpInspectionERPA', {
                        OperationID: OperationID
                    })
                    .then(function(result) {
                        $scope.OpInspectionERPA = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });

                /**
                 * get Checking equipment table
                 */
                API.procedure.post('ProcGetOpInspectionCE', {
                        OperationID: OperationID
                    })
                    .then(function(result) {
                        $scope.OpInspectionCE = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });

                /**
                 * get Operator Notes table
                 */
                API.procedure.post('ProcGetOpInspectionON', {
                        OperationID: OperationID
                    })
                    .then(function(result) {
                        $scope.OpInspectionON = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });

                /**
                 * get Processes table
                 */
                API.procedure.post('ProcGetOpInspectionProcesses', {
                        OperationID: OperationID
                    })
                    .then(function(result) {
                        $scope.OpInspectionProcesses = result;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };
            $scope.getOperationInfo(OperationID);

            /**
             * the function working when user select item in Operation table
             */
            $scope.SelectedOperation = function(Operation) {
                var state = 'department.inspection-schedule.inspection';
                var params = {
                    OperationID: Operation.OperationID
                };
                $rootScope.redirectDept(state, params);
            };

            /**
             * Update Operation detail
             */
            $scope.UpdateOperationDetail = function(OpInspectionDetail) {
                if ($scope.MaterialInspectionID) {
                    if (OpInspectionDetail === undefined) {
                        OpInspectionDetail = {};
                    }
                    if (!OpInspectionDetail.InspectInfo) {
                        OpInspectionDetail.InspectInfo = '';
                    }
                    if (!OpInspectionDetail.QtyInspected || !regexNumber.test(OpInspectionDetail.QtyInspected)) {
                        OpInspectionDetail.QtyInspected = '';
                    }
                    if (!OpInspectionDetail.QtyScrapped || !regexNumber.test(OpInspectionDetail.QtyScrapped)) {
                        OpInspectionDetail.QtyScrapped = '';
                    }
                    if (!OpInspectionDetail.ReasonID) {
                        ReasonID = null;
                    } else {
                        ReasonID = OpInspectionDetail.ReasonID.value;
                    }
                    API.procedure.post('ProcUpdateOpInspectionDetail', {
                            UserID: $rootScope.user.getId(),
                            InspectionID: $scope.MaterialInspectionID,
                            InspectionDetail: OpInspectionDetail.InspectInfo,
                            QtyInspected: OpInspectionDetail.QtyInspected,
                            QtyScrapped: OpInspectionDetail.QtyScrapped,
                            ReasonID: ReasonID
                        })
                        .then(function(result) {
                            $scope.getInspectOpList(OperationID);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            };
        }

        $scope.addOperatorNote = function(OperatorNote) {
            if ($scope.OperationIDSelected === undefined) {
                Notification.show('warning', 'Please select Operation table');
                return false;
            }
            if (OperatorNote === undefined | OperatorNote === '') {
                Notification.show('warning', 'Please enter Operator Note');
                return false;
            }

            API.procedure.post('ProcAddOperatorNote', {
                    UserID: $rootScope.user.getId(),
                    OperationID: $scope.OperationIDSelected,
                    Note: OperatorNote
                })
                .then(function(result) {
                    Notification.show('success', 'Add operator note success');
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.openBIRT = function() {
            var params = {};
            if ($scope.RejectID !== undefined) {
                params.RejectNoteID = $scope.RejectID;
            } else {
                params.RejectNoteID = 1;
            }
            $rootScope.openTabBirt('InspectRejectAdvice', params);
        };

        $scope.addProcessNote = function(ProcessNote) {
            if ($scope.OperationIDSelected === undefined) {
                Notification.show('warning', 'Please select Operation');
                return false;
            }
            if (ProcessNote === undefined | ProcessNote === '') {
                Notification.show('warning', 'Please enter Process Note');
                return false;
            }

            API.procedure.post('ProcAddProcessNote', {
                    UserID: $rootScope.user.getId(),
                    OperationID: $scope.OperationIDSelected,
                    ProcessNote: ProcessNote
                })
                .then(function(result) {
                    Notification.show('success', 'Add process note success');
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * Execute Goods in Mode
         */
        if (GRBID !== undefined) {

            /**
             * get Approval drop-down
             */
            API.procedure.get('ProcGetApprovalList')
                .then(function(result) {
                    $scope.ApprovalList = result;
                })
                .catch(function(error) {
                    throw error;
                });

            API.procedure.post('ProcGetInspectionDetail', {
                    GRBID: GRBID
                })
                .then(function(result) {
                    if (result.length > 0) {
                        $scope.InspectionDetail = result[0];
                        $scope.MaterialInspectionID = result[0].InspectionID;

                        // Selected Approval drop-down
                        $scope.InspectionDetail.ApprovalTypeID = _.find($scope.ApprovalList, function(item) {
                            return item.pkApprovalID === $scope.InspectionDetail.ApprovalTypeID;
                        });
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        }

        /**
         * the function working when user click Begin button
         */
        $scope.BeginInspection = function() {

            /**
             * if has OperationID execute ProcBeginOpInspection
             * else has GRBID execute ProcBeginMaterialInspection
             */
            if (OperationID !== undefined) {
                API.procedure.post('ProcBeginOpInspection', {
                        UserID: $rootScope.user.getId(),
                        OperationID: OperationID
                    })
                    .then(function(result) {
                        if (result.length > 0) {
                            if (parseInt(result[0].Result) === -1) {
                                Notification.show('warning', result[0].MsgResult);
                            } else {
                                Notification.show('success', result[0].MsgResult);
                                $scope.MaterialInspectionID = result[0].Last_Insert_ID;
                            }
                        }
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }

            if (GRBID !== undefined) {

                API.procedure.post('ProcBeginMaterialInspection', {
                        UserID: $rootScope.user.getId(),
                        GRBID: GRBID
                    })
                    .then(function(result) {
                        if (result.length > 0) {
                            if (result[0].MsgResult && !result[0].Result) {
                                Notification.show('success', result[0].MsgResult);
                                $scope.MaterialInspectionID = result[0].Last_Insert_ID;
                            }
                            if (result[0].MsgResult && parseInt(result[0].Result) === -1) {
                                Notification.show('warning', result[0].MsgResult);
                            }
                        }
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        var regexError = /error/;
        var patternError = new RegExp(regexError, "i");

        /**
         * the function working when click Pause button
         * if begin not click first, NO pause OR Stop is allowed
         */
        $scope.PauseInspection = function() {
            if ($scope.PauseReason === undefined) {
                Notification.show('warning', 'Please select Pause Reason');
                return false;
            }

            if (!$scope.MaterialInspectionID) {
                $scope.MaterialInspectionID = null;
            }
            API.procedure.post('ProcPauseInspection', {
                    UserID: $rootScope.user.getId(),
                    InspectionID: $scope.MaterialInspectionID,
                    ReasonID: $scope.PauseReason.value
                })
                .then(function(result) {
                    if (result.length > 0) {
                        if (patternError.test(result[0].MsgResult)) {
                            Notification.show('warning', result[0].MsgResult);
                        } else {
                            Notification.show('success', result[0].MsgResult);
                        }
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * the function working when user click Complete button
         */
        $scope.CompleteInspection = function() {
            if (!$scope.MaterialInspectionID) {
                $scope.MaterialInspectionID = null;
            }
            API.procedure.post('ProcCompleteInspection', {
                    UserID: $rootScope.user.getId(),
                    InspectionID: $scope.MaterialInspectionID
                })
                .then(function(result) {
                    if (result.length > 0) {
                        if (patternError.test(result[0].MsgResult)) {
                            Notification.show('warning', result[0].MsgResult);
                        } else {
                            Notification.show('success', result[0].MsgResult);
                        }
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * the function working when click Raise PO button
         */
        $scope.RaisePO = function(POQty) {
            if ($scope.OperationIDSelected === undefined) {
                Notification.show('warning', 'Please select Operation');
                return false;
            }
            if (POQty === undefined | POQty === '') {
                Notification.show('warning', 'Please enter PO Qty');
                return false;
            }
            if (!regexNumber.test(POQty)) {
                Notification.show('warning', 'Sub-con PO Qty must be number');
                return false;
            }

            API.procedure.post('ProcRaisePO', {
                    UserID: $rootScope.user.getId(),
                    OperationID: $scope.OperationIDSelected,
                    POQty: POQty
                })
                .then(function(result) {
                    Notification.show('success', 'Raise PO success');
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.UpdateInspectionDetail = function(InspectionDetail, type) {
            var RejectReasonID;
            if (type === 'allok') {
                $scope.InspectionDetail.RejectReasonID = undefined;
                RejectReasonID = null;
                $scope.InspectionDetail.QtyRejected = null;
            }
            if (type === 'allrejected') {
                $scope.InspectionDetail.QtyRejected = null;
            }
            if (type === 'rejected') {
                $scope.InspectionDetail.AllOK = null;
            }
            if (type === 'rejected' || type === 'allrejected' || type === 'rejectReason') {
                if (InspectionDetail.RejectReasonID === undefined || InspectionDetail.RejectReasonID === '') {
                    Notification.show('warning', 'Please select Reject Reason');
                    return false;
                }
                RejectReasonID = InspectionDetail.RejectReasonID.value;
            }
            if ($scope.GRBID !== undefined && $scope.GRBID !== null) {
                if (InspectionDetail.ApprovalTypeID === undefined) {
                    InspectionDetail.ApprovalTypeID = null;
                }
                API.procedure.post('ProcUpdateInspectionDetail', {
                        UserID: $rootScope.user.getId(),
                        GRBID: parseInt($scope.GRBID),
                        AllOK: InspectionDetail.AllOK,
                        QtyRejected: parseInt(InspectionDetail.QtyRejected),
                        RejectReasonID: RejectReasonID
                    })
                    .then(function(result) {
                        if (result.length > 0) {
                            $scope.RejectID = result[0].RejectID;
                        }
                        Notification.show('success', 'Update Inspection Detail success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            } else {
                Notification.show('warning', 'This function only use for MaterialInspection');
                return false;
            }
        };

        $scope.ViewADD = function() {
            API.procedure.post('ProcViewADD', {
                    WorkOrderID: $stateParams.WorkOrderID
                })
                .then(function(result) {
                    if (result.length > 0) {
                        var fileName = result[0].FileName;
                        $scope.showImage = $rootScope.PathImageUpload + fileName;
                        $window.open($scope.showImage, 'blank_');
                    } else {
                        Notification.show('warning', 'Image of Work Order is not avaiable');
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        };

        /**
         * if no exists OperationID & GRBID then redirect to inspection schedule
         */
        if (OperationID === undefined && GRBID === undefined) {
            $state.go('department.inspection-schedule.main');
        }

        $scope.redirectBack = function() {
            $window.history.back();
        };
    }

})(angular.module('app.components.department.inspection-schedule.inspection', []));
