(function(app) {
    app.controller('DeptHomeCtrl', DeptHomeCtrl);
    DeptHomeCtrl.$inject = ['$rootScope', '$scope', '$state', 'Notification', 'API', '$stateParams'];

    function DeptHomeCtrl($rootScope, $scope, $state, Notification, API, $stateParams) {

        var DeptID = $stateParams.DeptID;
        $rootScope.$watch('user', function(user) {
            if (user !== undefined) {
                API.procedure.post('ProcGetTodoList', {
                        DeptID: DeptID
                    })
                    .then(function(result) {
                        $scope.TodoList = result;
                        if ($scope.TodoList.length > 0) {
                            $scope.TodoList.forEach(function(value) {
                                if (value.Qty !== null && value.Qty !== "") {
                                    value.Qty = parseInt(value.Qty);
                                }
                            });
                        }
                    })
                    .catch(function(error) {
                        throw (error);
                    });
            }
        });

        /**
         * Sort Table
         */
        $scope.sortTable = [
            { title: 'Age', sortBy: 'Age', class: 'col-xs-3 no-padding' },
            { title: 'Action', sortBy: 'ActionType', class: 'col-xs-1 no-padding' },
            { title: 'Customer', sortBy: 'CustomerName', class: 'col-xs-3 no-padding' },
            { title: 'Qty', sortBy: 'Qty', class: 'col-xs-1 no-padding' },
            { title: 'WO No.', sortBy: 'WONo', class: 'col-xs-1 no-padding' },
            { title: 'Part Number', sortBy: 'PartNO', class: 'col-xs-2 no-padding' },
            { title: 'Complete', sortBy: 'Age', class: 'col-xs-1 no-padding' },
        ];
        $scope.sortBy = $scope.sortTable[0].sortBy;
        $scope.changeSortBy = function(item) {
            $scope.sortBy = item.sortBy;
        };

        $scope.CompleteTodo = function(Todo) {
            API.procedure.post('ProcCompleteTodo', {
                    TodoID: Todo.TodoID,
                    UserID: $rootScope.user.getId(),
                })
                .then(function(result) {
                    _.remove($scope.TodoList, Todo);
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.redirectPage = function(item) {
            var params = {};
            var action = item.ActionType;
            action = _.camelCase(action);
            action = _.toLower(action);

            switch (action) {
                case "review":
                case "plan":
                case "planchg":
                case "estimate":
                case "nre":
                    if (item.ETemplateID) {
                        params.ETemplateID = item.ETemplateID;
                        $rootScope.redirectDept('department.engineering-template.material', params);
                    }
                    break;
                case "grin":
                    if (item.GRBID) {
                        params.GRBID = item.GRBID;
                        $rootScope.redirectDept('department.inspection-schedule.inspection', params);
                    }
                    break;
                case 'issue':
                    if (item.GRBID) {
                        params.WorkOrderID = item.WONo;
                        params.GRBNo = item.GRBID;
                        $rootScope.redirectDept('department.material-issue', params);
                    }
                    break;
                case 'sc':
                    $rootScope.redirectDept('department.delivery-despatch');
                    break;
                case 'invoice':
                case 'outvoice':
                    if (item.WONo) {
                        params.WorkOrderID = item.WONo;
                        $rootScope.redirectDept('department.workorders.invoice', params);
                    }
                    break;
                case 'splitbatch':
                case 'schedule':
                    $rootScope.redirectDept('department.production-schedule.material-issue');
                    break;
                case 'cr':
                    if (item.ETemplateID) {
                        params.ETemplateID = item.ETemplateID;
                        $rootScope.redirectDept('department.engineering-template.review', params);
                    }
                    break;
                case 'purchase':
                case 'quote':
                case 'chasepo':
                    $rootScope.redirectDept('department.purchase-order');
                    break;
                case 'credit':
                    if (item.WONo) {
                        params.WONo = item.WONo;
                        $rootScope.redirectDept('department.workorders.main', params);
                    }
                    break;
                case 'service':
                    $rootScope.redirectDept('department.machine-control');
                    break;
                case 'dispatch':
                    $rootScope.redirectDept('department.delivery-despatch');
                    break;
                default:
                    console.log("don't exists action: " + action);
            }
        };

        $scope.logout = function() {
            API.user.logout()
                .then(function(response) {
                    $state.go('login');
                    $cookieStore.remove('token');
                })
                .catch(function(error) {
                    throw error;
                });
        };
    }
})(angular.module('app.components.department.home', []));
