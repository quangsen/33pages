var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Branch(options) {
    this.BranchName = '';
    this.Address1 = '';
    this.Address2 = '';
    this.Address3 = '';
    this.Address4 = '';
    this.Postcode = '';
    this.Phone = '';
    this.Fax = '';

    BaseModel.call(this, options);
}

inherits(Branch, BaseModel);

Branch.prototype.getBranchName = function() {
    return this.BranchName;
};

Branch.prototype.getAddress1 = function() {
    return this.Address1;
};

Branch.prototype.getAddress2 = function() {
    return this.Address2;
};

Branch.prototype.getAddress3 = function() {
    return this.Address3;
};

Branch.prototype.getAddress4 = function() {
    return this.Address4;
};

Branch.prototype.getPostcode = function() {
    return this.Postcode;
};

Branch.prototype.getPhone = function() {
    return this.Phone;
};

Branch.prototype.getFax = function() {
    return this.Fax;
};

module.exports = Branch;
