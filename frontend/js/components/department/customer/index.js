require('./main/');
require('./info/');
require('./contact/');
require('./order-history/');
require('./delivery-points/');
var Customer = require('../../../models/Customer.js');
(function(app) {

    app.controller('CustomerCtrl', CustomerCtrl);

    CustomerCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API', '$state', 'Notification', '$window'];

    function CustomerCtrl($rootScope, $scope, $stateParams, API, $state, Notification, $window) {
        var customer_id = $stateParams.customer_id;
        $scope.resetStatusButton = function() {

            /**
             * Use to show/hide Save, Update, Cancel button
             */
            $scope.showUpdateButton = false;
            $scope.showSaveCancelButton = false;
        };
        $scope.clearInfoCustomer = function() {

            /**
             * Clear information of Customer
             */
            $scope.CustomerInfo = null;
            $scope.PaymentTerm = null;
            $scope.PaymentRunFrequency = null;
            $scope.fkApprovalID = null;
        };
        $scope.resetStatusButton();
        $scope.clearInfoCustomer();

        /**
         * Use to save old value of Customer when User click Cancel button
         */
        var save_customer;

        $scope.ListPaymentTerms = [{
            value: 1,
            name: '30 days'
        }, {
            value: 2,
            name: '60 days'
        }, {
            value: 3,
            name: '90 days'
        }];


        /**
         * List select Payment Run Frequency
         */
        $scope.ListPaymentRunFrequency = [{
            value: 1,
            name: 'weekly'
        }, {
            value: 2,
            name: 'bi-monthly'
        }, {
            value: 3,
            name: 'monthly'
        }];

        /**
         * get All Customer
         */
        API.procedure.get('ProcCustomerList')
            .then(function(result) {
                $scope.CustomerList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * List AllApproval
         */
        API.procedure.get('ProcGetAllApprovalList')
            .then(function(result) {
                $scope.AllApprovalList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * Add Customer
         */
        $scope.addCustomer = function() {
            $scope.showUpdateButton = false;
            $scope.showSaveCancelButton = true;
            $scope.clearInfoCustomer();
            $scope.CustomerInfo = {};
        };

        /**
         * Cancel Add Customer
         */
        $scope.cancelAddCustomer = function() {
            if ($scope.CustomerInfo === undefined || $scope.CustomerInfo === null) {
                $scope.showUpdateButton = false;
                $scope.showSaveCancelButton = false;
            } else {
                $scope.showUpdateButton = true;
                $scope.showSaveCancelButton = false;
            }
        };

        /**
         * redirect Customer Order Page
         */
        $scope.redirectNewCO = function() {
            var state = 'department.customer-order';
            $rootScope.redirectDept(state);
        };

        /**
         * redirect Order History Page
         */
        $scope.redirectOrderHistoryPage = function() {
            if ($scope.CustomerInfo === null | $scope.CustomerInfo === undefined) {
                Notification.show('warning', 'Please select Customer');
                return false;
            }
            var state = 'department.customer.order-history';
            var params = {
                customer_id: $scope.CustomerInfo.pkCustomerID
            };
            $rootScope.redirectDept(state, params);
        };

        /**
         * redirect Customer Contact Page
         */
        $scope.redirectCustomerContactPage = function() {
            if ($scope.CustomerInfo === null | $scope.CustomerInfo === undefined) {
                Notification.show('warning', 'Please select Customer');
                return false;
            }
            var state = 'department.customer.contact';
            var params = {
                customer_id: $scope.CustomerInfo.pkCustomerID
            };
            $rootScope.redirectDept(state, params);
        };

        /**
         * redirect Delivery Points Page
         */
        $scope.redirectDeliveryPointsPage = function() {
            if ($scope.CustomerInfo === null | $scope.CustomerInfo === undefined) {
                Notification.show('warning', 'Please select Customer');
                return false;
            }
            var state = 'department.customer.delivery-points';
            var params = {
                customer_id: $scope.CustomerInfo.pkCustomerID
            };
            $rootScope.redirectDept(state, params);
        };

        $scope.exportCustomer = function() {
            API.global.post('exportExcel', {
                    userName: $rootScope.user.getUsername(),
                    procedureName: 'ViewAllCustomerDetail',
                    title: 'Export Customer'
                })
                .then(function(result) {
                    var form = $('<form method="GET" action="/' + result + '">');
                    $('body').append(form);
                    form.submit();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };

        /**
         * copy Address
         */
        $scope.copyAddress = function() {
            $scope.CustomerInfo.InvAddress1 = $scope.CustomerInfo.Address1;
            $scope.CustomerInfo.InvAddress2 = $scope.CustomerInfo.Address2;
            $scope.CustomerInfo.InvAddress3 = $scope.CustomerInfo.Address3;
            $scope.CustomerInfo.InvAddress4 = $scope.CustomerInfo.Address4;
            $scope.CustomerInfo.InvPostcode = $scope.CustomerInfo.Postcode;
        };


        $scope.UserSelected = function(data) {
            $scope.showUpdateButton = true;
            $scope.showSaveCancelButton = false;

            customer_id = data.pkCustomerID;
            $scope.getCustomerInfo(customer_id);
            $state.go('department.customer.info', {
                customer_id: customer_id
            });
        };

        $scope.getCustomerInfo = function(customer_id) {
            API.procedure.post('ProcGetCustomerDetail', {
                    CustomerID: customer_id
                })
                .then(function(result) {
                    if (result.length === 0) {
                        $state.go('department.home');
                    }
                    $scope.CustomerInfo = new Customer(result[0]);
                    save_customer = $scope.CustomerInfo;

                    $scope.PaymentTerm = _.find($scope.ListPaymentTerms, function(item) {
                        return parseInt(item.value) === parseInt($scope.CustomerInfo.getPaymentTerms());
                    });
                    $scope.PaymentRunFrequency = _.find($scope.ListPaymentRunFrequency, function(item) {
                        return parseInt(item.value) === parseInt($scope.CustomerInfo.getPaymentRunFrequency());
                    });

                    var days = [
                        { key: '1', name: 'Monday' },
                        { key: '2', name: 'Tuesday' },
                        { key: '3', name: 'Wednesday' },
                        { key: '4', name: 'Thursday' },
                        { key: '5', name: 'Friday' },
                        { key: '6', name: 'Staturday' },
                        { key: '7', name: 'Sunday' },
                    ];
                    $scope.days = days;

                    $scope.fkApprovalID = _.find($scope.AllApprovalList, function(item) {
                        return parseInt(item.pkApprovalID) === parseInt($scope.CustomerInfo.getFkApprovalID());
                    });

                    API.procedure.post('ProcGetOpeningHours', {
                            CustomerID: customer_id,
                        })
                        .then(function(result) {
                            for (var value in result[0]) {
                                if (result[0][value] === null || result[0][value] === '00:00:00') {
                                    result[0][value] = 'close';
                                } else {
                                    result[0][value] = result[0][value].slice(0, -3).replace(':', '');
                                }
                            }
                            $scope.CustomerInfo = $scope.CustomerInfo.setOptions(result[0]);
                        })
                        .catch(function(error) {
                            throw (error);
                        });

                    API.procedure.post('ProcGetCustomerDetail', {
                            CustomerID: customer_id
                        })
                        .then(function(result) {
                            $scope.CustomerInfo = $scope.CustomerInfo.setOptions(result[0]);
                        })
                        .catch(function(error) {
                            throw (error);
                        });
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.updateCustomer = function(Customer) {
            var data = {};
            for (var key in Customer) {
                var match = key.match(/^Day\d(Close|Open)$/);
                if (match !== null) {
                    if (Customer[key] === 'close' || Customer[key] === undefined) {
                        data[key] = '';
                    }
                    var regex = /^\d{4}$/;
                    if (!regex.test(Customer[key])) {
                        data[key] = '000000';
                    } else {
                        data[key] = Customer[key] + "00";
                    }
                }
            }
            var numberRegex = /^0|[1-9][0-9]*$/;
            var phoneRegex = /^[0-9_ ()]*$/g;
            var facsimileRegex = /^[0-9_ ()]*$/g;
            if (Customer.getFacsimile() !== '') {
                if (!facsimileRegex.test(Customer.getFacsimile())) {
                    Notification.show('warning', 'Facsimile format is invalid');
                    return false;
                }
            }

            if (Customer.getPhone() !== '') {
                if (!phoneRegex.test(Customer.getPhone())) {
                    Notification.show('warning', 'Phone format is invalid');
                    return false;
                }
            }
            if (Customer.getDeliveryCharge() === undefined || !numberRegex.test(Customer.getDeliveryCharge())) {
                Customer.DeliveryCharge = 0;
            }
            if ((Customer.getMarkUpQuote() === undefined) || !numberRegex.test(Customer.getMarkUpQuote())) {
                Customer.MarkUpQuote = 0;
            }
            if (Customer.getMarkUpQuote() > 255) {
                Notification.show('warning', 'Quote markup not be larger 255');
                return false;
            }

            API.procedure.post('ProcUpdateCustomerDetail', {
                    UserID: Customer.getCustomerId(),
                    CustomerID: customer_id,
                    CustomerSageCode: Customer.getFinanceCode(),
                    CustomerName: Customer.getCustomerName(),
                    Alias: Customer.getAlias(),
                    Address1: Customer.getAddress1(),
                    Address2: Customer.getAddress2(),
                    Address3: Customer.getAddress3(),
                    Address4: Customer.getAddress4(),
                    Postcode: Customer.getPostcode(),
                    Phone: Customer.getPhone(),
                    Facsimile: Customer.getFacsimile(),
                    Website: Customer.getWebsite(),
                    Email: Customer.getEmail(),
                    InvAddress1: Customer.getInvAddress1(),
                    InvAddress2: Customer.getInvAddress2(),
                    InvAddress3: Customer.getInvAddress3(),
                    InvAddress4: Customer.getInvAddress4(),
                    InvPostcode: Customer.getInvPostcode(),
                    FDApproval: Customer.getFDApproval(),
                    MDApproval: Customer.getMDApproval(),
                    GMApproval: Customer.getGMApproval(),
                    CreditLimit: Customer.getCreditLimit(),
                    MarkUpQuote: Customer.getMarkUpQuote(),
                    PaymentTerms: Customer.getPaymentTerms(),
                    PaymentRunFrequency: Customer.getPaymentRunFrequency(),
                    fkApprovalID: Customer.getFkApprovalID(),
                    DeliveryCharge: Customer.getDeliveryCharge(),
                    IrishCurrency: Customer.getIrishCurrency(),
                    USCurrency: Customer.getUSCurrency(),
                    EuroCurrency: Customer.getEuroCurrency(),
                    Day1Open: data.Day1Open,
                    Day1Close: data.Day1Close,
                    Day2Open: data.Day2Open,
                    Day2Close: data.Day2Close,
                    Day3Open: data.Day3Open,
                    Day3Close: data.Day3Close,
                    Day4Open: data.Day4Open,
                    Day4Close: data.Day4Close,
                    Day5Open: data.Day5Open,
                    Day5Close: data.Day5Close,
                    Day6Open: data.Day6Open,
                    Day6Close: data.Day6Close,
                    Day7Open: data.Day7Open,
                    Day7Close: data.Day7Close,
                    Deleted: Customer.getDeleted(),
                })
                .then(function(result) {
                    Notification.show('success', 'Update Successful');
                })
                .catch(function(error) {
                    throw (error);
                });
        };

        /**
         * Save Customer
         */
        $scope.saveCustomer = function(CustomerDetail, PaymentTerm, PaymentRunFrequency, fkApprovalID) {
            var CustomerInfo = new Customer(CustomerDetail);

            API.procedure.post('ProcAddNewCustomer', {
                    UserID: $rootScope.user.getId(),
                    CustomerSageCode: CustomerInfo.FinanceCode,
                    CustomerName: CustomerInfo.getCustomerName(),
                    Alias: CustomerInfo.getAlias(),
                    Address1: CustomerInfo.getAddress1(),
                    Address2: CustomerInfo.getAddress2(),
                    Address3: CustomerInfo.getAddress3(),
                    Address4: CustomerInfo.getAddress4(),
                    Postcode: CustomerInfo.getPostcode(),
                    Phone: CustomerInfo.getPhone(),
                    Facsimile: CustomerInfo.getFacsimile(),
                    Website: CustomerInfo.getWebsite(),
                    Email: CustomerInfo.getEmail(),
                    InvAddress1: CustomerInfo.getInvAddress1(),
                    InvAddress2: CustomerInfo.getInvAddress2(),
                    InvAddress3: CustomerInfo.getInvAddress3(),
                    InvAddress4: CustomerInfo.getInvAddress4(),
                    InvPostcode: CustomerInfo.getInvPostcode(),
                    FDApproval: CustomerInfo.getFDApproval(),
                    MDApproval: CustomerInfo.getMDApproval(),
                    GMApproval: CustomerInfo.getGMApproval(),
                    CreditLimit: CustomerInfo.getCreditLimit(),
                    MarkUpQuote: CustomerInfo.getMarkUpQuote(),
                    PaymentTerms: PaymentTerm.value,
                    PaymentRunFrequency: PaymentRunFrequency.value,
                    fkApprovalID: fkApprovalID.pkApprovalID,
                    DeliveryCharge: CustomerInfo.getDeliveryCharge(),
                    IrishCurrency: CustomerInfo.getIrishCurrency(),
                    EuroCurrency: CustomerInfo.getEuroCurrency(),
                    USCurrency: CustomerInfo.getUSCurrency()
                })
                .then(function(result) {
                    $state.reload();
                })
                .catch(function(error) {
                    Notification.show('warning', 'Finance Code has exist');
                    return false;
                });
        };
    }

    app.directive('currencyInput', function($filter, $browser) {
        return {
            require: 'ngModel',
            link: function($scope, $element, $attrs, ngModelCtrl) {
                var listener = function() {
                    var value = $element.val().replace(/,/g, '');
                    $element.val($filter('number')(value, false));
                };

                // This runs when we update the text field
                ngModelCtrl.$parsers.push(function(viewValue) {
                    return viewValue.replace(/,/g, '');
                });

                // This runs when the model gets updated on the scope directly and keeps our view in sync
                ngModelCtrl.$render = function() {
                    $element.val($filter('number')(ngModelCtrl.$viewValue, false));
                };

                $element.bind('change', listener);
                $element.bind('keydown', function(event) {
                    var key = event.keyCode;
                    // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                    // This lets us support copy and paste too
                    if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40))
                        return;
                    $browser.defer(listener); // Have to do this or changes don't get picked up properly
                });

                $element.bind('paste cut', function() {
                    $browser.defer(listener);
                });
            },
        };
    });

    app.directive('validNumber', function() {
        return {
            require: '?ngModel',
            link: function(scope, element, attrs, ngModelCtrl) {
                if (!ngModelCtrl) {
                    return;
                }

                ngModelCtrl.$parsers.push(function(val) {
                    if (angular.isUndefined(val)) {
                        val = '';
                    }

                    var clean = val.replace(/[^-0-9\.]/g, '');
                    var negativeCheck = clean.split('-');
                    var decimalCheck = clean.split('.');
                    if (!angular.isUndefined(negativeCheck[1])) {
                        negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                        clean = negativeCheck[0] + '-' + negativeCheck[1];
                        if (negativeCheck[0].length > 0) {
                            clean = negativeCheck[0];
                        }

                    }

                    if (!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                    }

                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });

                element.bind('keypress', function(event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });

})(angular.module('app.components.department.customer', [
    'app.components.customer.main',
    'app.components.customer.info',
    'app.components.customer.contact',
    'app.components.customer.order-history',
    'app.components.customer.delivery-points',
]));
