var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function TemplateMaterial(options) {
    this.Description = '';
    this.Size = 0;
    this.ShapeID = null;
    this.PartLength = 0;
    this.Yield = 0;
    this.ApprovalID = null;
    this.IssueInstruction = '';
    this.TestPieces = 0;
    this.FreeIssue = 0;
    this.UnitID = null;
    this.StdDeliveryDays = 0;
    this.SPUID = null;
    this.StdSupplierID = null;

    BaseModel.call(this, options);
}

inherits(TemplateMaterial, BaseModel);

TemplateMaterial.prototype.setShapeID = function(value) {
    this.ShapeID = value;
};

TemplateMaterial.prototype.setApprovalID = function(value) {
    this.ApprovalID = value;
};

TemplateMaterial.prototype.setUnitID = function(value) {
    this.UnitID = value;
};

TemplateMaterial.prototype.setSPUID = function(value) {
    this.SPUID = value;
};

TemplateMaterial.prototype.setStdSupplierID = function(value) {
    this.StdSupplierID = value;
};

TemplateMaterial.prototype.getDescription = function() {
    return this.Description;
};

TemplateMaterial.prototype.getSize = function() {
    return parseFloat(this.Size);
};

TemplateMaterial.prototype.getShapeID = function() {
    return this.ShapeID;
};

TemplateMaterial.prototype.getPartLength = function() {
    return this.PartLength;
};

TemplateMaterial.prototype.getYield = function() {
    return this.Yield;
};

TemplateMaterial.prototype.getApprovalID = function() {
    return this.ApprovalID;
};

TemplateMaterial.prototype.getIssueInstruction = function() {
    return this.IssueInstruction;
};

TemplateMaterial.prototype.getTestPieces = function() {
    return this.TestPieces;
};

TemplateMaterial.prototype.getFreeIssue = function() {
    return this.FreeIssue;
};

TemplateMaterial.prototype.getUnitID = function() {
    return this.UnitID;
};

TemplateMaterial.prototype.getStdDeliveryDays = function() {
    return this.StdDeliveryDays;
};

TemplateMaterial.prototype.getSPUID = function() {
    return this.SPUID;
};

TemplateMaterial.prototype.getStdSupplierID = function() {
    return this.StdSupplierID;
};

module.exports = TemplateMaterial;
