(function(app) {

    app.controller('SupplyMainCtrl', SupplyMainCtrl);
    SupplyMainCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', '$window'];

    function SupplyMainCtrl($rootScope, $scope, $state, $stateParams, API, Notification, $window) {
        var SupplierID = $stateParams.SupplierID;
        var dataFormat = 'DD/MM/YYYY';

        /**
         * Use to swap Update Save button though Supply Info
         */
        $scope.swapUpdateSave = false;
        var hasSupplierID = false;

        if (SupplierID !== undefined) {
            hasSupplierID = true;
            $scope.swapUpdateSave = true;
            $scope.getSupplyInfo(SupplierID);

            API.procedure.post('ProcGetSupplierApprovals', {
                    SupplierID: SupplierID
                })
                .then(function(result) {
                    $scope.SupplierApprovals = result;
                })
                .catch(function(error) {
                    throw error;
                });

            API.procedure.post('ProcGetSupplierNADCAPs', {
                    SupplierID: SupplierID
                })
                .then(function(result) {
                    $scope.SupplierNADCAPs = result;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        $scope.ViewSQ = function() {
            var check = $scope.checkSelectSupplier();
            if (check === true) {
                API.procedure.post('ProcViewSupplierQuestions', {
                        SupplierID: SupplierID
                    })
                    .then(function(result) {
                        var fileName = result[0].FileName;
                        if (fileName === null || fileName === '') {
                            Notification.show('warning', "File doesn't exist");
                            return false;
                        }
                        var file = $rootScope.PathImageUpload + fileName;
                        $window.open(file, 'blank_');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.SupplierSelected = function(item) {
            var state = 'department.supply-chain.main';
            var params = {
                SupplierID: item.SupplierID
            };
            $rootScope.redirectDept(state, params);
        };

        /**
         * get Iso Approval table
         */
        API.procedure.get('ProcGetApprovalList')
            .then(function(result) {
                $scope.ApprovalList = result;
            })
            .catch(function(error) {
                throw error;
            });

        /**
         * get Approval NADCAP drop-down
         */
        API.procedure.get('ProcGetNADCAPList')
            .then(function(result) {
                $scope.NADCAPList = result;
            })
            .catch(function(error) {
                throw error;
            });

        $scope.validateSupplier = function(Supplier) {
            var checkSupplier = $scope.checkSelectSupplier();
            if (checkSupplier !== false) {
                if (Supplier === undefined) {
                    Notification.show('warning', 'Please enter full information');
                    return false;
                }
                if (Supplier.ApprovalID === undefined) {
                    Notification.show('warning', 'Please select Approval');
                    return false;
                }
                if (Supplier.Expiry === undefined | Supplier.Expiry === '') {
                    Notification.show('warning', 'Please enter Expiry');
                    return false;
                }
                if (!$rootScope.validateDate(Supplier.Expiry, dataFormat)) {
                    Notification.show('warning', 'Expiry must be DD/MM/YYYY');
                    return false;
                }
                var ExpiryDate = Supplier.Expiry.split('/');
                var Expiry = ExpiryDate[2] + '/' + ExpiryDate[1] + '/' + ExpiryDate[0];
                return Expiry;
            }
        };

        /**
         * the function working when press Enter key on ISO Approval table
         */
        $scope.AddSupplierApproval = function(SupplierApproval) {
            var Expiry = $scope.validateSupplier(SupplierApproval);
            if (Expiry) {
                API.procedure.post('ProcAddSupplierApproval', {
                        UserID: $rootScope.user.getId(),
                        SupplierID: SupplierID,
                        ApprovalID: SupplierApproval.ApprovalID.pkApprovalID,
                        Expiry: Expiry
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.RmvSupplierApproval = function(SupplierApprovals) {
            var checkSupplier = $scope.checkSelectSupplier();
            if (checkSupplier !== false) {
                var selectedSupplier = _.find(SupplierApprovals, { 'selected': true });
                if (selectedSupplier === undefined) {
                    Notification.show('warning', 'Please select ISO Approval');
                    return false;
                }

                API.procedure.post('ProcRmvSupplierApproval', {
                        UserID: $rootScope.user.getId(),
                        SupplierID: SupplierID,
                        ApprovalID: selectedSupplier.ApprovalID
                    })
                    .then(function(result) {
                        _.remove(SupplierApprovals, selectedSupplier);
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        /**
         * the function working when press Enter key on NADCAP Approval table
         */
        $scope.AddSupplierNADCAP = function(SupplierNADCAP) {
            var Expiry = $scope.validateSupplier(SupplierNADCAP);
            if (Expiry) {
                API.procedure.post('ProcAddSupplierNADCAP', {
                        UserID: $rootScope.user.getId(),
                        SupplierID: SupplierID,
                        ApprovalID: SupplierNADCAP.ApprovalID.NADCAPApprovalID,
                        Expiry: Expiry
                    })
                    .then(function(result) {
                        $state.reload();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.RmvSupplierNADCAP = function(SupplierNADCAPs) {
            var checkSupplier = $scope.checkSelectSupplier();
            if (checkSupplier !== false) {
                var selectedNADCAP = _.find(SupplierNADCAPs, { 'selected': true });
                if (selectedNADCAP === undefined) {
                    Notification.show('warning', 'Please select Supplier NADCAP');
                    return false;
                }

                API.procedure.post('ProcRmvSupplierNADCAP', {
                        UserID: $rootScope.user.getId(),
                        SupplierID: SupplierID,
                        ApprovalID: selectedNADCAP.NADCAPID
                    })
                    .then(function(result) {
                        _.remove(SupplierNADCAPs, selectedNADCAP);
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        $scope.beforeUpload = function(file) {
            var checkSupplier = $scope.checkSelectSupplier();
            if (checkSupplier === false) {
                return false;
            } else {
                if ($scope.CompleteBy === undefined) {
                    Notification.show('warning', 'Please enter Completed by');
                    return false;
                }
            }
        };

        $scope.uploadSuccess = function(response) {
            API.procedure.post('ProcAddSupplierQuestions', {
                    UserID: $rootScope.user.getId(),
                    SupplierID: SupplierID,
                    FileName: response.data,
                    CompleteBy: $scope.CompleteBy
                })
                .then(function(result) {
                    Notification.show('success', 'Drop Supplier success!');
                })
                .catch(function(error) {
                    throw error;
                });
        };

        $scope.checkSelectSupplier = function() {
            if (!hasSupplierID) {
                Notification.show('warning', 'Please select Supplier');
                return false;
            }
            return true;
        };

        /**
         * redirect to Supply Chain Contact
         */
        $scope.redirectContact = function() {
            var checkSupplier = $scope.checkSelectSupplier();
            if (checkSupplier !== false) {
                var stateContact = 'department.supply-chain.supply-contact';
                var paramsContact = {
                    SupplierID: SupplierID
                };
                $rootScope.redirectDept(stateContact, paramsContact);
            }
        };

        /**
         * redirect Order History
         */
        $scope.redirectOrderHistory = function() {
            var checkSupplier = $scope.checkSelectSupplier();
            if (checkSupplier !== false) {
                var stateOrderHistory = 'department.supply-chain.supply-order-history';
                var paramsOrderHistory = {
                    SupplierID: SupplierID
                };
                $rootScope.redirectDept(stateOrderHistory, paramsOrderHistory);
            }
        };

        /**
         * redirect Performance
         */
        $scope.redirectPerformance = function() {
            var statePerformance = 'department.supply-chain.supply-performance';
            $rootScope.redirectDept(statePerformance);
        };

        /**
         * New Supplier
         */
        $scope.NewSupplier = function() {
            $scope.swapUpdateSave = false;
            $scope.SupplierDetail = null;
            var state = 'department.supply-chain.main';
            var params = {
                SupplierID: undefined
            };
            $rootScope.redirectDept(state, params);
        };

        /**
         * Check site
         */
        $scope.Checksite = function() {
            var url = 'https://www.eauditnet.com/eauditnet/ean/user/login.htm';
            var win = window.open(url, '_blank');
            win.focus();
        };

        $scope.redirectBack = function() {
            $window.history.back();
        };
    }

})(angular.module('app.components.department.supply-chain.main', []));
