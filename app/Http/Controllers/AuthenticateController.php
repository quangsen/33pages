<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Http\Controllers\ApiController;
use App\Procedure\Procedure;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use JWTAuth;

class AuthenticateController extends ApiController {
	public function authenticate(Request $request) {
		$credentials = $request->only('Username', 'UserPwd');
		$data        = Procedure::name('ProcUserLogin')->with($credentials)->run();
		if (count($data) == 0) {
			throw new ApiException("username or password is invalid", 0);
		} else {
			$user_id = $data[0]['UserID'];
			$user    = User::find($user_id);
			$token   = JWTAuth::fromUser($user);
			return $this->response($token);
		}
	}
	public function me() {
		if (!$user = JWTAuth::parseToken()->authenticate()) {
			return response()->json(['user_not_found'], 404);
		}
		$user->department = Procedure::name('ProcUserDept')->with(['UserID' => $user->pkUserID])->run();
		$user             = compact('user');
		return $this->response($user);
	}

	public function logout() {
		$token = JWTAuth::parseToken()->invalidate();
		return $this->response(['success' => true]);
	}
}
