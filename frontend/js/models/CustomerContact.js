var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function CustomerContact(options) {
    this.ContactID = '';
    this.ContactName = '';
    this.Email = '';
    this.Phone = '';
    this.Position = '';
    this.Extension = '';
    this.AcknowContact = 0;
    this.PrimaryContact = 0;

    BaseModel.call(this, options);
}

inherits(CustomerContact, BaseModel);

CustomerContact.prototype.getContactID = function() {
    return this.ContactID;
};

CustomerContact.prototype.getContactName = function() {
	return this.ContactName;
};

CustomerContact.prototype.getEmail = function() {
	return this.Email;
};

CustomerContact.prototype.getPhone = function() {
	return this.Phone;
};

CustomerContact.prototype.getPosition = function() {
	return this.Position;
};

CustomerContact.prototype.getExtension = function() {
	return parseInt(this.Extension);
};

CustomerContact.prototype.getAcknowContact = function() {
	return this.AcknowContact;
};

CustomerContact.prototype.getPrimaryContact = function() {
	return this.PrimaryContact;
};

module.exports = CustomerContact;
