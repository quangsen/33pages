function BaseModel(options) {
    this.bind(options);
}

BaseModel.prototype.bind = function(options) {
    options = options || {};
    for (var k in options) {
        var v = options[k];
        if (typeof v === 'function') {
            continue;
        }

        if (this.hasOwnProperty(k)) {
            if (v !== null && v !== undefined) {
                this[k] = v;
            }
        }
    }
};

BaseModel.prototype.getId = function() {
    if (!this.id) {
        return null;
    }
    return this.id;
};

module.exports = BaseModel;
