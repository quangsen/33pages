var gulp = require('gulp');
var watch = require('gulp-watch');
module.exports = function() {
    watch('frontend/js/**/*.js', function() {
        gulp.run([
            'jshint',
            'browserify'
        ]);
    });
    watch('frontend/css/**/*.css', function() {
        gulp.run([
            'minify_css'
        ]);
    });

    watch('frontend/js/**/*.tpl.html', function() {
        gulp.run([
            'html2js',
        ]);
    });

    gulp.watch('./frontend/**/*.scss', ['sass', 'minify_css']);
}
