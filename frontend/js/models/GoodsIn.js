var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function GoodsIn(options) {
    this.RcvdDate = null;
    this.MaterialTypeID = null;
    this.OrderID = null;
    this.LineItemID = null;
    this.POnID = null;
    this.ItemID = null;
    this.ETemplateID = null;
    this.WorkOrderNo = null;
    this.ReleaseNoteNo = null;
    this.ID = null;
    this.Type = null;
    this.Description = '';
    this.DeliveryNoteNo = '';
    this.PartNumber = '';
    this.QtyRcvd = 0;
    this.QtyManu = 0;
    this.Size = 0;
    this.UOMID = null;
    this.ShapeID = null;
    this.StoreAreaID = null;
    this.XLocation = '';
    this.YLocation = null;
    this.DirectIssue = null;
    this.ApprovalTypeID = null;
    this.RejectNoteID = null;

    BaseModel.call(this, options);
}

inherits(GoodsIn, BaseModel);

GoodsIn.prototype.setOptions = function(options) {
	for (var key in options) {
        this[key] = options[key];
    }
    return this;
};

GoodsIn.prototype.setMaterialTypeID = function(value) {
	this.MaterialTypeID = value;
};

GoodsIn.prototype.setOrderID = function(value) {
	this.OrderID = value;
};

GoodsIn.prototype.setLineItemID = function(value) {
	this.LineItemID = value;
};

GoodsIn.prototype.setPOnID = function(value) {
	this.POnID = value;
};

GoodsIn.prototype.setItemID = function(value) {
	this.ItemID = value;
};

GoodsIn.prototype.setETemplateID = function(value) {
	this.ETemplateID = value;
};

GoodsIn.prototype.setID = function(value) {
	this.ID = value;
};

GoodsIn.prototype.setType = function(value) {
	this.Type = value;
};

GoodsIn.prototype.setUOMID = function(value) {
	this.UOMID = value;
};

GoodsIn.prototype.setShapeID = function(value) {
	this.ShapeID = value;
};

GoodsIn.prototype.setStoreAreaID = function(value) {
	this.StoreAreaID = value;
};

GoodsIn.prototype.setXLocation = function(value) {
	this.XLocation = value;
};

GoodsIn.prototype.setYLocation = function(value) {
	this.YLocation = value;
};

GoodsIn.prototype.setApprovalTypeID = function(value) {
	this.ApprovalTypeID = value;
};

GoodsIn.prototype.setRejectNoteID = function(value) {
	this.RejectNoteID = value;
};

GoodsIn.prototype.getRcvdDate = function() {
    return this.RcvdDate;
};

GoodsIn.prototype.getMaterialTypeID = function() {
    return this.MaterialTypeID;
};

GoodsIn.prototype.getOrderID = function() {
    return this.OrderID;
};

GoodsIn.prototype.getLineItemID = function() {
    return this.LineItemID;
};

GoodsIn.prototype.getPOnID = function() {
    return this.POnID;
};

GoodsIn.prototype.getItemID = function() {
    return this.ItemID;
};

GoodsIn.prototype.getETemplateID = function() {
    return this.ETemplateID;
};

GoodsIn.prototype.getWorkOrderNo = function() {
    return this.WorkOrderNo;
};

GoodsIn.prototype.getReleaseNoteNo = function() {
    return this.ReleaseNoteNo;
};

GoodsIn.prototype.getID = function() {
    return this.ID;
};

GoodsIn.prototype.getType = function() {
    return this.Type;
};

GoodsIn.prototype.getDescription = function() {
    return this.Description;
};

GoodsIn.prototype.getDeliveryNoteNo = function() {
    return this.DeliveryNoteNo;
};

GoodsIn.prototype.getPartNumber = function() {
    return this.PartNumber;
};

GoodsIn.prototype.getQtyRcvd = function() {
    return this.QtyRcvd;
};

GoodsIn.prototype.getQtyManu = function() {
    return this.QtyManu;
};

GoodsIn.prototype.getSize = function() {
    return this.Size;
};

GoodsIn.prototype.getUOMID = function() {
    return this.UOMID;
};

GoodsIn.prototype.getShapeID = function() {
    return this.ShapeID;
};

GoodsIn.prototype.getStoreAreaID = function() {
    return this.StoreAreaID;
};

GoodsIn.prototype.getXLocation = function() {
    return this.XLocation;
};

GoodsIn.prototype.getYLocation = function() {
    return this.YLocation;
};

GoodsIn.prototype.getDirectIssue = function() {
    return this.DirectIssue;
};

GoodsIn.prototype.getApprovalTypeID = function() {
    return this.ApprovalTypeID;
};

GoodsIn.prototype.getRejectNoteID = function() {
    return this.RejectNoteID;
};

module.exports = GoodsIn;
