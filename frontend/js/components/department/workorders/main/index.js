(function(app) {
    app.controller('WorkOrderMainCtrl', WorkOrderMainCtrl);
    WorkOrderMainCtrl.$inject = ['$rootScope', '$scope', '$stateParams', '$state', 'API', 'Notification'];

    function WorkOrderMainCtrl($rootScope, $scope, $stateParams, $state, API, Notification) {
        var WorkOrderID = $rootScope.WorkOrderID = $stateParams.WorkOrderID;
        if (WorkOrderID !== undefined && WorkOrderID !== '') {
            if ($scope.WorkOrderDetail === undefined) {
                $scope.getWorkOrderInfo(WorkOrderID);
            }

            /**
             * get Work Order Notes
             */
            API.procedure.post('ProcGetWorkOrderNotes', {
                    WorkOrderID: WorkOrderID
                })
                .then(function(result) {
                    $scope.WorkOrderNotes = result;
                })
                .catch(function(error) {
                    throw error;
                });

            /**
             * Add Order Notes
             */
            $scope.AddOrderNotes = function(Notes) {
                if (Notes !== undefined) {
                    API.procedure.post('ProcAddWorkOrderNote', {
                            UserID: $rootScope.user.getId(),
                            WorkOrderID: WorkOrderID,
                            Note: Notes
                        })
                        .then(function(result) {
                            $state.reload();
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            };

            $scope.WOCancel = function() {
                API.procedure.post('ProcSetWOCancel', {
                        UserID: $rootScope.user.getId(),
                        WorkOrderID: WorkOrderID
                    })
                    .then(function(result) {
                        Notification.show('success', 'Set WorkOrder Cancel success');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /**
             * redirect to Customer Order page
             */
            $scope.redirectCOpage = function(CustomerOrderId, CustomerOrderRef) {
                var state = 'department.customer-order';
                var params = {};
                if (CustomerOrderRef !== undefined) {
                    params = {
                        CustomerOrderId: CustomerOrderId,
                        CustomerOrderRef: CustomerOrderRef
                    };
                }
                $rootScope.redirectDept(state, params);
            };

            $scope.redirectEngineering = function() {
                var state = 'department.engineering-template.material';
                var params = {
                    ETemplateID: undefined,
                    WorkOrderID: $stateParams.WorkOrderID
                };
                $rootScope.redirectDept(state, params);
            };

            /**
             * redirect to Production Schedule page
             */
            $scope.redirectPSpage = function() {
                var state = 'department.production-schedule.material-issue';
                $rootScope.redirectDept(state);
            };

            $scope.redirectMachineList = function(MachineName) {
                var state = 'department.machine-list';
                var params = {
                    MachineName: MachineName
                };
                $rootScope.redirectDept(state, params);
            };

        } else {
            var state = 'department.workorders';
            $rootScope.redirectDept(state);
            return false;
        }

    }
})(angular.module('app.components.department.workorders.main', []));
